@objects
    header							xpath		//h1[@class='col-xs-12 padding-0']
    subheader						xpath		//p[@class='col-xs-12 col-lg-8 col-lg-offset-2']
    banner							xpath		//div[@class='row row-band banner video-hero ng-scope']
    Logo							css			a.navbar-brand img
    selectcondition					xpath		//div[@class='col-lg-6 col-md-4 col-sm-4 col-xs-12 hidden-xs ']
    clearfixcars-*					xpath		//ul[@class='clearfix']/li
    clearfix						css			ul.clearfix
    bodystyle						css			h1#h1BodyStyle
    search							xpath		//input[@id='inputKeywordSearch']
    searchoption-*					xpath		//select[@id='search-make']/option
    navbarlist-*					xpath		//ul[@class='nav navbar-nav']/li
    activeslick-*					xpath		//li[contains(@class,'slick-active')]
    selectoptions					xpath		//div[@class='checkbox-group clearfix col-lg-6 col-md-7 col-sm-7 col-xs-12']
    navbar							xpath		//ul[@class='nav navbar-nav']
    bannertext						xpath		//h1[@class='col-xs-12 padding-0']/span
 	
= Autonation Home Page =

    @on Desktop        
	    @forEach [navbarlist-*] as itemName, next as nextItem
			${itemName}:
				left-of ${nextItem}
   				
    @on Desktop	
    	Logo:
    		left-of navbar
    		width 160px
    		height 36px
    		
		search:
			inside banner
			
		header:
			above subheader
			css font-size is "60px"
			css font-family starts "interstate-regular" 
			
		subheader:
			css font-size is "21px"
			css font-family starts "proximanova-regular" 
			
		selectcondition:
			aligned horizontally top selectoptions
		
		bannertext:
			text is "OPEN ROAD"
