# README #

A Hybrid Flexible Framework that is currently built on Selenium, TestNG and Data Driven approach

To Run:
clean compile exec:java test -DResFolder=resources

### What is this repository for? ###

- Under Construction -

### How do I get set up? ###

- Under Construction -

### Contribution guidelines ###

- Under Construction -

### Who do I talk to? ###

- Under Construction -

### Features ###

- Under Construction -

### ToDo ###

* Dynamic POM invocation using Specific flavor of a common POM. e.g., Mobile, Desktop, Team Specific pages, etc.

* Convert Test Data Workbook name to Class Name

* More customized Driver capabilities for Desktop

* Sub Iteration using Array of HasMap in Test Data Provider

* Code Obfuscation along with JAR packaging.

* Integrate Excel macro for dynamic thread count

* SQL helper functions

* Perfecto Device Queuing

* SQL DB DataProvider Iteration

* Dependency to check 404 on URLs

* Dependency to check pass/fail status on dependent tests

* Environment based Base URL