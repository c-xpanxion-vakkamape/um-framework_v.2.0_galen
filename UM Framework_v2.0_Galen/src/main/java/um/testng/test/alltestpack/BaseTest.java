package um.testng.test.alltestpack;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.DBConnection;
import um.testng.test.utilities.framework.ExcelReader;
import um.testng.test.utilities.framework.GridConfig;
import um.testng.test.utilities.framework.HipChatHelper;
import um.testng.test.utilities.framework.MailHelper;
import um.testng.test.utilities.framework.ReadAndReplace;
import um.testng.test.utilities.framework.TestDataProvider;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.enums.MessageFormat;

public class BaseTest {

	// ------------------------- JEEVA -----------------------------------
	public UMReporter classReport;

	@BeforeSuite(alwaysRun = true)
	public void presteps(ITestContext ctx) throws IOException {
		// DB Query creation 	
		String history_Prerequisite = ConfigProp.getPropertyValue("History_Prerequisite");
		if(history_Prerequisite.equalsIgnoreCase("Yes")){
			String dbQuery = ExcelReader.queryGenerator();
			System.out.println("The db query is "+dbQuery);		
			String jsonValue=DBConnection.jsonGenerator(dbQuery);
			ReadAndReplace.updateXML(jsonValue);
		}
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		System.out.println("Executing Pre-steps of the Test Suite");
		UMReporter.initReport(suiteName);		
		String appiumRunMode = ConfigProp.getPropertyValue("Run_Appium");
		System.out.println("The appium run node---"+appiumRunMode);
		if(appiumRunMode.equalsIgnoreCase("yes")){
			try{
				GridConfig.launchNodes();
			}catch(Exception e){
				System.out.println("The error message is "+e.getMessage());
				e.printStackTrace();
			}
		}
	}

	@AfterSuite(alwaysRun = true)
	public void poststeps(ITestContext ctx) {
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		System.out.println("Executing Post-steps of the Test Suite");
		UMReporter.endReport(suiteName);
		String hipchat_Prerequisite = ConfigProp.getPropertyValue("Hipchat_Prerequisite");
		if(hipchat_Prerequisite.equalsIgnoreCase("yes"))
				hipChat(ctx, suiteName);
		String mail_Prerequisite = ConfigProp.getPropertyValue("Mail_Prerequisite");
		if(mail_Prerequisite.equalsIgnoreCase("yes"))
				eMail(ctx, suiteName);
	}

	/*
	 * @AfterSuite(alwaysRun = true) public void hipChat1(ITestContext ctx) {
	 * System.out.println("Inside Hip chat Messenging"); HipChatHelper hipchat =
	 * new HipChatHelper("qCrymkPIFGkrT44XlXGKRkzRbssHBs3COSjTQ1iR");
	 * hipchat.messageUser("saravanan.mariappan@turner.com",
	 * "Test Run completed", true, MessageFormat.TEXT);
	 * 
	 * }
	 */

	public static void hipChat(ITestContext context, String suiteName) {

		System.out.println("Inside Hip chat Messenging");
		String apiKey = ConfigProp.getPropertyValue("APIKey");
		HipChatHelper hipchat = new HipChatHelper(apiKey);
//		String path = ConfigProp.getPropertyValue("SSPath");
		String path = UMReporter.reportPath;
		String toUser = ConfigProp.getPropertyValue("ToUser");

		if (suiteName.equalsIgnoreCase("Desktop")) {
			String statusMsg =  "Automation Desktop Test Results: Pass:" + UMReporter.passCountD + " :: FAIL:" + UMReporter.failCountD;
			hipchat.messageUser(toUser, statusMsg, true, MessageFormat.TEXT);
			hipchat.shareFile(toUser, statusMsg, path + "/UM_Execution_Result_Desktop.htm");
		} else {
			String statusMsg =  "Automation Mobile Test Results: Pass:" + UMReporter.passCountM + " :: FAIL:" + UMReporter.failCountM;
			hipchat.messageUser(toUser, statusMsg, true, MessageFormat.TEXT);
			hipchat.shareFile(toUser, statusMsg, path + "/UM_Execution_Result_Mobile.htm");
		}
	}

	public static void eMail(ITestContext ctx, String suiteName) {
		System.out.println("Inside eMailing API");
		String path = UMReporter.reportPath;
		System.out.println(path);
		if (suiteName.equalsIgnoreCase("Desktop")) {
			MailHelper.Send("Regression Report - Desktop", path + "/UM_Execution_Result_Desktop.htm", "", "");
		} else {
			MailHelper.Send("Regression Report - Mobile", path + "/UM_Execution_Result_Mobile.htm", "", "");
		}
	}

	@BeforeClass
	public void initParentRep(ITestContext ctx) {
		System.out.println("===============================================================================Before Class");
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		String testName = ctx.getCurrentXmlTest().getName();
		System.out.println("The suite name is ----"+suiteName);
		System.out.println("The test name is ----"+testName);

		if("Browser".equalsIgnoreCase(testName)){
			testName="";
		}
		String className = this.getClass().getSimpleName();
		classReport = new UMReporter(className + " <span class='device-name'> "+testName+"</span>", "Regression Test Set", suiteName);
	}

	@AfterClass
	public void endParentRepo(ITestContext ctx) {
		System.out.println("AfterClass");
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		classReport.endParent(suiteName);
	}

	@SuppressWarnings("unchecked")
	@BeforeMethod
	public void setUp(Object[] testArgs, ITestContext ctx, Method method) {
		String testName = method.getName();
		System.out.println("Executing test: " + testName);
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		System.out.println("The suite name is "+suiteName);
		ConfigProvider.init((Map<String, String>) testArgs[0]);
		TestDataProvider.init((Map<String, String>) testArgs[1]);
		UMReporter.initTest(testName,suiteName);
		String platform = ConfigProvider.getConfig("Platform").toUpperCase();
		UMReporter.assignCatogory(platform);
	}

	@AfterMethod
	public void tearUp(ITestContext context, Method method) {
		System.out.println("AfterMethod");
		DriverFactory.driverCleanUp();
		classReport.appendParent();
		ConfigProvider.endThreadLocal();
		TestDataProvider.endThreadLocal();
	}

	@DataProvider(name = "GlobalTestData", parallel = true)
	public static Object[][] GlobalTestData(Method method, ITestContext context) {
		System.out.println("INto DP");
		ArrayList<Map<String, String>> browArray = new ArrayList<Map<String, String>>();
		ArrayList<Map<String, String>> DataArray = new ArrayList<Map<String, String>>();

		String className = method.getDeclaringClass().getSimpleName();
		String methodName = method.getName();
		browArray = ExcelReader.getBrowsersDeviceComb(context.getCurrentXmlTest().getName(), className, methodName);
		DataArray = ExcelReader.getTestDataIterator(methodName, className);

		System.out.println("========" + browArray);
		System.out.println("========" + DataArray);
		int i = 0;
		int count = browArray.size() * DataArray.size();

		System.out.println(count + "Combinations");

		Object[][] test = new Object[count][2];

		for (Map<String, String> elem : browArray) {
			// for (Entry<String, String> entryD : Data.entrySet()) {
			for (Map<String, String> entryD : DataArray) {
				test[i][0] = elem;
				test[i][1] = entryD;
				i++;
			}
		}
		return test;
	}

}
