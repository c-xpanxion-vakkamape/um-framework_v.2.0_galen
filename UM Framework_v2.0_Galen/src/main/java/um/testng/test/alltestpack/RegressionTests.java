package um.testng.test.alltestpack;

import java.io.IOException;
import java.util.Map;
import org.apache.http.client.ClientProtocolException;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import um.testng.test.alltests.BaseTestMethods;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class RegressionTests extends BaseTest {

	@Test(dataProvider = "GlobalTestData", groups = {
			"Regression" }, description = "MODULE: New Navigation || QC ID:12599 || US Section Navigation Test")
	public void HeaderUSSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderUSSubSections();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"Regression" }, description = "MODULE: New Navigation || QC ID:12599 || WORLD Section Navigation Test")
	public void HeaderWorldSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderWorldSubSections();

		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || QC ID:12606 || TRAVEL Section Navigation Test")
	public void HeaderTravelSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderTravelSubSections();

	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || QC ID:12614 || MORE Section Navigation Test")
	public void HeaderMoreSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderMoreSubSections();

		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, enabled = false, description = "MODULE: New Navigation || QC ID:12604 || TECH Section Navigation Test")
	public void HeaderTechSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);

		try {
			test.testHeaderTechSubSections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || QC ID:12617 || INTL: FEATURES Section Navigation Test")
	public void HeaderINTLFeaturesSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		try {
			test.testHeaderINTLFeaturesSubSections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: Weather || QC Test ID: 13872,13908,13901 || Test Rail ID: C76108 || Weather Page Elements Validation, Weather Page Zip code Validation")
	public void weatherPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.weatherPageValidation();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: 01 HomePage || QC Test ID: 11822 || Home Page - Weather Card is Present")
	public void WeatherCardElem(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.testWeatherCardElem();
		else
			UMReporter.log(LogStatus.WARNING, "The Weather card is disabled in the JSON");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: 01 HomePage || QC Test ID: 11821  || Weather Page  Changing Temp from Fahrenheit to Celsius and Celsius to Fahrenheit")
	public void WeatherPageChangeTemp(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.testChangeTemp();
		else
			UMReporter.log(LogStatus.WARNING, "The Weather card is disabled in the JSON");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: 01 HomePage || QC Test ID: 11821  || Weather Page Current Condition")
	public void WeatherPageCurrent(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.testCurrentConditions();
		else
			UMReporter.log(LogStatus.WARNING, "The Weather card is disabled in the JSON");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: 13 Search || Home Page Search CNN Validation")
	public void FooterSearchBlank(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHomePageFooterSearchBlank();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: 13 Search || QC ID: 11908, 11680, 11683, 11681 || Home Page Search CNN Validation")
	public void FooterSearchValid(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHomePageFooterSearchValid("Search CNN");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || QC ID: 12633 || Home Page -> Gallery Leaf page Link")
	public void HomeGalleryLinks(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHomeGalLinks();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE: 13 Search || QC ID: 11678, 11684 || Home Page Search CNN Validation")
	public void HomePageHeaderSearchValid(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHomePageHeaderSearch("Search CNN");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"testVimeoPlayer" }, description = "MODULE: 01 HomePage || QC Test ID: 13583 || EMBED - CNN-23446 - Embed Vine")
	public void testEmVinePlayer(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		if (System.getProperty("environment").equals("SWEET") || System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.ValidatetestEmVinePlayer();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"testVRHomepageheaderfont" }, description = "MODULE:360 VIDEO || QC Test ID: 14167   || Verify New Fonts for Custom CSS ")
	public void testVRHomepageheaderfont(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVRheaderfont();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"testVimeoPlayer" }, description = "MODULE: 01 HomePage || QC Test ID: 13582 || EMBED - CNN-23447 - Embed Vimeo")
	public void testVimeoPlayer(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		if (System.getProperty("environment").equals("SWEET") || System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.ValidateVimeoPlayer();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Social Share || QC TEST ID : 13447 || Social Share page Validation")
	public void SocialShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.SocialShare();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12955 || FORMS - CNN-22576 - Basic Validations")
	public void FeedbackPageBasicValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackForm();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12955 ||  FORMS - CNN-22576 - Mandatory Validations")
	public void FeedbackPageMandatory(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormSubmitMandate();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12952 ||  FORMS Submission")
	public void FeedbackPageFormSubmit(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormSubmit();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12914 || EMAIL VALIDATION")
	public void FeedbackPageFormEmail(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormEmail();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12952 & 12963 ||  FORMS Submission & MetaData")
	public void FeedbackPageFormSubmitAc360(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormSubmit360();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12955 ||  FORMS - CNN-22576 - Mandatory Validations")
	public void FeedbackPageFormMandAc360(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormMandate360();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12914 || EMAIL VALIDATION")
	public void FeedbackPageFormEmail360(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormEmail360();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13145 || Live Blog header and paragraph : font styles")
	public void LiveFyreUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13238 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreItalicUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreItalicUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13241 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreOrderedListUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreOrderedListUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"Weather" }, description = "MODULE: 01 HomePage || QC Test ID: 11821  || Weather Page  Changing Temp from Fahrenheit to Celsius and Celsius to Fahrenheit")
	public void WeatherPageMaps(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.testWeatherMap();
		else
			UMReporter.log(LogStatus.WARNING, "The Weather card is disabled in the JSON");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Weather" }, description = "MODULE: 01 HomePage || QC ID: 11822 || Verify Zipcode validation in Weather page.")
	public void weatherpageZipcodeValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.weatherpageZipcodeValidation();
		else
			UMReporter.log(LogStatus.WARNING, "The Weather card is disabled in the JSON");

	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12955 ||  FORMS - CNN-22576 - Mandatory Validations")
	public void FeedbackPageFormMandStud(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormMandateStud();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12952 & 12964 ||  FORMS Submission & Metadata")
	public void FeedbackPageFormSubmitStud(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormSubmitStud();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Feedback" }, description = "MODULE: 34 - Feedback Form || QC Test ID: 12914 || EMAIL VALIDATION")
	public void FeedbackPageFormEmailStud(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedBackFormEmailStud();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "CNNGo",
			"FeedbackGo" }, description = "MODULE NAME : 04 SocialShare || QC ID: 13449 || SocialShare - FeedbackGo page validation")
	public void CNNFeedbackGo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.CNNFeedbackGo();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13243 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreUnOrderedListUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreUnOrderedListUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13242 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreStrikedTextUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreStrikedTextUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13245 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreBoldTextUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreBoldTextUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: Live Blog || QC TEST ID : 13246 || Live Blog Italic header and paragraph : font styles")
	public void LiveFyreQuotedTextUI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testLiveFyreQuotedTextUI();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13588 || Live Blog Plain Caption Validation")
	public void LiveFyrePlainCaption(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.LiveFyrePlainCaption();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13589 || Live Blog Headings Validation")
	public void LiveFyreHeadings(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.LiveFyreHeadings();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Section" }, description = "MODULE : 18 Shows || QC ID: 11814, 11645, 11888 || TV SHOWS PAGE")
	public void TvShowPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testTVShowValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Section" }, description = "SHOW LEAF PAGE : ElEMENTS")
	public void ShowLeafPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testShowLeafValidations();
	}

	@Test(dataProvider = "GlobalTestData", enabled = false, groups = { "Regression", "RegressionINTL",
			"Section" }, description = " MODULE: 24 Feed Cards || QC ID: 11586, 11585, 11580 - 11588 ||Feed card")
	public void FeedCard(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testFeedCard();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"ArticleGalFullWidth" }, description = "MODULE: 15 Embedded Elements || QC ID 11662,14048,14051 || Article: Article Full width gallery")
	public void ArticleGalFullWidth(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleFullWidthGallery();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Section" }, description = "MODULE: 26 Container Titles Clickable/Linkable || QC ID: 11566 || Section Page: Container and Zone elements")
	public void ContainerElements(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testContainerAndZoneElements();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", enabled = false, description = "MODULE:05 Video || QC Test ID: 12214 || PINNED VIDEO - CNNWE-22 - Right Rail Pinned Video- Fast Follow in Article Collection Page")
	public void ArticleColRightRailPinVideo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.ArticleColRightRailPinVideo();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"ArticleAutoPlayVideoPT" }, description = "Validating the video AUTO play")
	public void ArticleAutoPlayVideoPT(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.ArticleVideoautoplayed();
	}
	//////////////////////////////////////////////

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testArticlePages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: New Navigation || QC ID:12602 || ENTERTAINMENT Section Navigation Test")
	public void HeaderEntertainmentSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderEntertainmentSubSections();
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderOpinionsSubSection_DOM(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderOpinionsSubSections_DOM();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderOpinionsSubSection_INTL(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderOpinionsSubSections_INTL();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void testHeaderHealthSubSections_INTL(Map<String, String> elem, Map<String, String> eTest,
			ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderHealthSubSections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || QC ID:12613 || VIDEOS Section Navigation Test")
	public void HeaderVideoSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderVideoSubSections();

	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderINTLRegionsSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderINTLRegionsSubSections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderINTLSportSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderINTLSportSubSections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherCardCity(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherCardFnCity();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherCardZip(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherCardFnZip();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherCardToggle(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherCardFnToggle();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherCardRedirect(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherCardFnToggle();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Weatherpagevalidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.weatherPagePrefferedTemp();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void weatherpageImageValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.weatherpageImageValidation();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void weatherpageMakeDefaultValidation(Map<String, String> elem, Map<String, String> eTest,
			ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.weatherpageMakeDefaultValidation();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HomePageHeaderSearchInvalid(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHomePageHeaderSearchInvalid();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HomePageHeaderSearchBlank(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHomePageHeaderSearchBlank();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void weatherpageRecentLocationValidation(Map<String, String> elem, Map<String, String> eTest,
			ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.weatherpageRecentLocationValidation();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void FooterSearchInvalid(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHomePageFooterSearchInvalid();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HomeVideoLinks(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHomeVidLinkss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherPageOtherValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherPageOtherValidations();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void WeatherPageRecentLocation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		boolean weather = btm.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		try {
			if (weather == true)
				btm.testWeatherPageRecentLocation();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HomapgeHeaderPlusSymbol(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.launchpageeditionpicker();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Interactivebourdain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.interactivebourdain();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void MarketCard(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			try {
				btm.testMarketCard();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void MetaContent(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.MetaContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void testSoundCloudPlayer(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (System.getProperty("environment").equals("SWEET") || System.getProperty("environment").equals("REF")
				|| System.getProperty("environment").equalsIgnoreCase("PROD")) {
			try {
				btm.ValidateSoundCloudPlayer();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ValidateAdIZL(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if ((System.getProperty("environment").equals("PROD") || System.getProperty("environment").equals("REF")
				|| System.getProperty("environment").equals("STAGE"))
				&& (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))) {
			boolean enableIntelligentLoad = btm.ChecZonesJSON(System.getProperty("environment"),
					System.getProperty("edition"), "enableIntelligentLoad");
			boolean loadAllZonesLazy = btm.ChecZonesJSON(System.getProperty("environment"),
					System.getProperty("edition"), "loadAllZonesLazy");
			if (enableIntelligentLoad == loadAllZonesLazy) {

				try {
					btm.ValidateApolloADthirdZone();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");

			}
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Emailsubscription(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.Subscriptonnewsletter();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Validatesubscribebtn(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.Subscribebuttonvalidation();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ValidateSubscriptionFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI")) {

			try {
				btm.SubscribtionFBShare();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void SubscriptionTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI")) {

			try {
				btm.SubscriptionTwitterShare();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ValidateSubscriptionMoreShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.SubscriptionMoreshare();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void SubscriptionNavigation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (!ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			try {
				btm.SubscriptionNav();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"ArticleEmImageFullWidth" }, description = "MODULE: 15 Embedded Elements || QC ID: 11955 || Article Leaf Page with embedded static image page")
	public void ArticleEmImageFullWidth(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleEmImageFullWidth();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "Article Leaf Page with embedded Standard image page")
	public void ArticleEmImageStandard(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleStandardImg();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC ID: 11670 || 3rd Party elements : tweet")
	public void ArticleEmTw(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleEmTw();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC ID: 11670 || 3rd Party elements : tweet follow")
	public void ArticleEmTwFollow(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleEmTwFollow();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE: 15 Embedded Elements || QC ID 11666 ||Article Page Embedded video validation")
	public void ArticleEmVidFullWidth(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.ArticleEmbVideoPage();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 09 Leaf Page || QC ID: 11723 || Article Leaf Page Advertisement scenarios")
	public void ArticleLeafPageAds(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleLeafPageAds();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"ArticlePageEmbedInstagram" }, description = "MODULE: Embedded Elements || QC TEST ID : 12997,13584,13861 || Article Leaf Page with Embed Instagram")
	public void ArticlePageEmbedInstagram(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("PROD") || System.getProperty("environment").equals("STAGE")
				|| System.getProperty("environment").equals("REF")) {
			UMReporter.log(LogStatus.WARNING, "URL yet to be configured for AUTOMATION");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		} else {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmbedInstagram();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC ID: 11670,13584 || Article Page Instagram Image Validation")
	public void ArticlePageInstagram(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageInstagram();
		} else {
			UMReporter.log(LogStatus.INFO, "To be configured in REF and added");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 12 Share Button/Social Media || QC ID: 11691,11885,11930 || Article Leaf Page")
	public void ArticlePageSocialIcons(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticlePageSocialIcons();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11925 || Article Top Collections Page")
	public void ArticleColPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleCollectionsPage();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11925 || Article TOp collection : click PRev")
	public void ArticleColPlayerNavPrev(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleCollectionPlayerPrevNav();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11925 || Article TOp collection : click Next")
	public void ArticleColPlayerNavNext(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleCollectionPlayerNextNav();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Style",
			"Section" }, description = "MODULE: 29 Style || QC Test ID:  || International Style page")
	public void StyleLogoNavigation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testStyleLogoNavigation();
	}

	// Style page gallery elements
	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Style",
			"Section" }, description = "MODULE: 29 Style || QC Test ID: 12117 || International Style page: Gallery elements")
	public void StyleGallery(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testStyleGalleryValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Style",
			"Section" }, description = "MODULE: 29 Style || QC Test ID: 12117 || International Style page: click Prev and Next")
	public void StyleGalleryClickPrev(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testStyleGalleryPrevAndNext();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Style",
			"Section" }, description = "MODULE: 29 Style || QC Test ID: 12115, 13580 || Style Page Footer Validation")
	public void StylePageFooterValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.StylePageFooter();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Section",
			"Style" }, description = "MODULE: 31 - Outbrain || QC Test ID: 12992 || OUTBRIAN - CNNINTL-4154 - Style gallery Outbrain widget display")
	public void GalleryStyleOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.GalleryStyleOutbrain();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13093 || AD in the zone 4 of Video Landing Page")
	public void VideoOutbrainAD(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.videoOutbrainAD();

	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13040 || Video Landing Page : Zone 3 Layout")
	public void VideoZone3(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoZone3Validations();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13039 || Video Landing Page : Zone 5 Layout")
	public void VideoZone5(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoZone5Validations();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13040 || Video Landing Page : Zone 3 Layout")
	public void VideoZone3INTL(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoZone3INTLValidations();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13039 || Video Landing Page : Zone 5 Layout")
	public void VideoZone5INTL(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoZone5INTLValidations();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 17 Comments || QC ID: 12911 || Article Comments")
	public void ArticleCommentsValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Driver").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleComments();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 17 Comments || QC ID: 12911 || Article Comments")
	public void ArticleCommentsLoginWithFB(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Driver").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleCommentsLoginWithFb();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 17 Comments || QC ID: 12911 || Article Comments")
	public void ArticleCommentsLoginWithTwitter(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Driver").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleCommentsLoginWithTwitter();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 17 Comments || QC ID: 12911 || Article Comments")
	public void ArticleCommentsLoginWithGoogle(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Driver").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleCommentsLoginWithGoogle();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 17 Comments || QC ID: 12911 || Article Comments")
	public void ArticleCommentsLoginWithLinkedI(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!System.getProperty("environment").equals("PROD")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Driver").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleCommentsLoginWithLinkedIn();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "RefRegression",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC ID: 11670 || Embedded Elements - 3rd Party elements : Facebook")
	public void ArticleEmFb(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleEmFb();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "RefRegression",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC ID: 11661 || Article with Expandable image")
	public void ArticleEmImageExpandable(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleExpandableImg();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11735 || Article Leaf Page with page top image")
	public void ArticleEmPageTop(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticlePageTopImage();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"ArticleEmYoutube" }, description = "MODULE: 15 Embedded Elements || QC ID 11666, 11670,13585 || 3rd Party elements : youtube")
	public void ArticleEmYoutube(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleEmYoutube();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"adlock" }, description = "Long ARTICLE - CNNWE-73 - Short Article Ad Locking Article Leaf Page || QC ID : 11592,11691,11885,11930, 11738 ||Article Leaf Page")
	public void ArticleLeafLongAdLock(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (((System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD"))
				&& !ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testadLockLong();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 13590 || Long Article Ad Locking functionality check in  Article Leaf Page with Comment Section Turned On")
	public void ArticleLeafLongAdLockWithCommen(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (((System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD"))
				&& !ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testadLockLongWithComment();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "QC Test ID: 12223 || Article Leaf Page Outbrain validation")
	public void ArticleLeafOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleLeafOutbrain();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"adlock" }, description = "SHORT ARTICLE - CNNWE-73 - Short Article Ad Locking Article Leaf Page || QC ID : 11592,11691,11885,11930, 11738 ||Article Leaf Page")
	public void ArticleLeafShortAdLock(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (((System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD"))
				&& !ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleLeafShortAdLock();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC TEST ID : 12933 || Article Leaf Page with Embed Multiple Images")
	public void ArticlePageEmbedImages(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("PROD") || System.getProperty("environment").equals("STAGE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmbedImages();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: Embedded Elements || QC TEST ID : 12900  || Article Leaf Page with Embed show")
	public void ArticlePageEmbedShow(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if ((System.getProperty("environment").equals("PROD")) || (System.getProperty("environment").equals("STAGE"))) {
			UMReporter.log(LogStatus.INFO, "URL yet to be configured for AUTOMATION");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		} else {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmbedShow();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: Embedded Elements || QC TEST ID : 12947 || Article Leaf Page with Embed show")
	public void ArticlePageEmbedSpecial(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if ((System.getProperty("environment").equals("PROD")) || (System.getProperty("environment").equals("STAGE"))) {
			UMReporter.log(LogStatus.INFO, "URL yet to be configured for AUTOMATION");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		} else {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmbedSpecial();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 15 Embedded Elements || QC TEST ID : 12912 || Article Leaf Page with Embed Web Tag")
	public void ArticlePageEmbedWebTag(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("IE11")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmbedWebTag();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 12975 || ARTICLE -  CNNINTL-4261 - Formatting of bullet points differs from body text around them")
	public void ArticlePageFontStyles(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticlePageFontStyles();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11924, 14030, 14174 || Article Leaf Page with page top image")
	public void ArticlePageTopCollPageWidth(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		if (System.getProperty("environment").equals("REF"))
			test.testVideoCollValidationsPageWidth();
		else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11732 || Article Leaf Page with page top image")
	public void ArticlePageTopImgPageWidth(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		if (System.getProperty("environment").equals("REF"))
			test.testArticlePageTopImagePageWidth();
		else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"Article" }, description = "Article : Fact Box: Story Highlights")
	public void AuthorCardValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testAuthorCardValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: Article Leaf Page || QC TEST ID : 13751 || Verification of background Theme in a Facebook embed Article Page")
	public void ArticleWithEmbedFBTheme(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException {
		if (System.getProperty("environment").equals("REF") || System.getProperty("environment").equals("STAGE")
				|| System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleWithEmbedFBTheme();
		} else {
			UMReporter.log(LogStatus.INFO, "URL yet to be configured for AUTOMATION");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC ID : 11957, 11958 ||Embedded Video Collection")
	public void EmVideoCollValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testEmVideoCollValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC ID : 11957, 11958 ||Embedded Video Collection: when PREV is clicked")
	public void EmVideoCollPrevValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.tesEmVideoCollPrevNav();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC ID : 11957, 11958 ||Embedded Video Collection: when Next is clicked")
	public void EmVideoCollNextValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.tesEmVideoCollNextNav();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 25 Fact Box || QC ID : 11568, 11579, 11573 - 11577 ||Article: FAct box")
	public void FactBoxValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testFactBoxValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 12 Share Button/Social Media || QC ID: 11888 || Special Page - Social Share icons")
	public void SpecialPageSocialIcons(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSpecialPageSocialIcons();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "RefRegression",
			"Article" }, description = "MODULE: 04 Section Fronts || QC ID: 13149 ||Sport Page  Embedded Elements - 3rd Party elements : Facebook")
	public void SportArticleEmFb(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSportArticleEmFb();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 12965 || MOBILE: Read More : Article Leaf Page ")
	public void StormTracker(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testStormTracker();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 13729 || SPECIAL PAGES - CNN-23589 - When Breaking News Banner is displayed then Facebook Messenger Icon is hidden in Special Pages")
	public void SpecialArticleMessenger(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((System.getProperty("environment").equals("REF") || System.getProperty("environment").equals("SWEET"))
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testSpecialArticleMessenger();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:07 Article Leaf Page || QC TEST ID : 11737, 11734 || Article Page Top Video - Auto Play")
	public void ArticleAutoPlayVid(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testArticleAutoPlayVideoPage();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE: 23 Pull Quote || QC ID: 11589 || Article: Pull Quote Validations")
	public void ArticlePullQuote(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testPullQuoteValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"CNNIFBInstantartilemetadata" }, description = "")
	public void CNNIFBInstantartilemetadata(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.CNNIFBInstantartilemetadata();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Login",
			"Social" }, description = "MODULE: 10 Sign in || QC ID 11711 || Validating Click HERE on my cnn page")
	public void ClickHere(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.clickHere();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Login",
			"Social" }, description = "MODULE: 10 Sign in || QC ID 11714 || CNN Login: Forgot Password with Valid Email id")
	public void ForgotPasswordMyCnn(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.forgotPasswordMyCnn();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Login",
			"Social" }, description = "MODULE: 10 Sign in || QC ID 11714 || CNN Login: Forgot Password with Invalid Email id")
	public void ForgotPasswordWithInvalidEmail(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.forgotPasswordwithInvalidEmail();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Gallery" }, description = "MODULE: 08 Gallery Leaf Page,09 Leaf Page || 11722, 11727, 11726 || Gallery Page Elements Validation")
	public void GalleryElementsAds(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testGalleryElementsAD();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Politics", "Article",
			"Social" }, description = "MODULE: 13 Global Header Navigation || QC ID: 12706,12705")
	public void PoliticsPageFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& (System.getProperty("environment").equals("PROD")
						|| System.getProperty("environment").equals("STAGE"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPoliticsPageFBShare();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Politics", "Article",
			"Social" }, description = "MODULE: 13 Global Header Navigation || QC ID: 12706,12705")
	public void PoliticsPageTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& (System.getProperty("environment").equals("PROD")
						|| System.getProperty("environment").equals("STAGE"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPoliticsPageTwitterShare();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:05 HomePage || QC Test ID: 14119 || TOS-CNNWE-608-HOPE Privacy policy and TOS")
	public void PrivacyPolicy(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException {
		if (System.getProperty("environment").equals("REF") && System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPrivacyPolicy();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"HomePage" }, description = "MODULE: 13 Search || QC ID: 11684 || Search 404 page")
	public void Search404Page(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSearch404Page();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article" })
	public void TravelArticle(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("PROD") || System.getProperty("environment").equals("STAGE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testTravelArticlePageSocialIcons();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Login",
			"Social" }, description = "MODULE: 10 Sign in || QC ID 11711 || Validating LinkedIn login on my cnn page")
	public void UpdateAccountDetails(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.updateAccountDetails();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RefRegression",
			"SocialIcons" }, description = "Politics Article Page Social Icon Validations")
	public void PoliticsSocialicons(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("PROD") || System.getProperty("environment").equals("STAGE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPoliticsArticlePageSocialIcons();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Branding" }, description = "MODULE:BRANDING || QC ID: 13906 || BRANDING - CNN-23736 - Video branding image and logo not displaying on video leaf ")
	public void ValidateBrandings(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		if (System.getProperty("environment").equals("REF") || System.getProperty("environment").equals("STAGE")
				|| System.getProperty("environment").equals("SWEET")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.ValidateBrandingBanner();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"WebPack" }, description = "MODULE 38:WebPack || QC ID:13906  || INDEX TAG - CNNWE-418 - Add noindex tag to index2 and index4 ")
	public void ValidateIndex2Tag(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testindex2tag();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"WebPack" }, description = "MODULE 38:WebPack || QC ID:13906  || INDEX TAG - CNNWE-418 - Add noindex tag to index2 and index4 ")
	public void ValidateIndex4Tag(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testindex4tag();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"WebPack" }, description = "MODULE:WebPack || QC ID: 13902 || WEBPACK - CNNWE-377 - Webpack metadata-header.js ")
	public void Validatedescaftershrunk(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.Validateshowdescftershrunk();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Validateverticalgalleryimagecut" }, description = "MODULE 38:Gallery || QC ID:14017  || GALLERY - CNN-23957 - Vertical Photo Galleries pulling wrong image cut")
	public void Validateverticalgalleryimagecut(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		if (System.getProperty("environment").equals("SWEET") || System.getProperty("environment").equals("STAGE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.ValidateVerticalgallery();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: TRIP ADVISOR || QC ID : 14059,14062 || TripAdvisor displays and redirect validation in CNN International Travel Section")
	public void tripAdvisor(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.tripAdvisor();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Section",
			"trip" }, description = "MODULE: 04_02 International || QC Test ID: 12998 || Ticket No: CNNINTL-4046  || TripAdvisor logo in footer of travel related pages")
	public void IntlTripAdvisor(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		if (System.getProperty("edition").equals("INTL"))
			test.testTripAdvisorValidations();
		else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "VideoLanding",
			"Video" }, description = "MODULE:05 Video || QC Test ID: 13007  || Video Landing Page - Share Bar Validations")
	public void VideoLandingShareMeta(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVideoShareMeta();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Social" }, description = "05 Video || TEST ID: 13006 - VideoLanding Twitter Share Validations")
	public void VideoLandingTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testVideoPageTwitterShare();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Social" }, description = "05 Video || TEST ID: 13006 -  VideoLanding FB Share Validations")
	public void VideoLandingFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testVideoPageFBShare();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC TEST ID : 12934")
	public void VideoLandingPinnedVideoFooter(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.VideoLandingPinnedVideoFooter();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC TEST ID : 12979 - To verify the Pinned video is not listed")
	public void VideoLandingPinnedVideoNotListe(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPinnedVideoNotListed();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE:05 Video || QC TEST ID : 12214, 13002, 12971")
	public void VideoLandingPinnedVideo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").toString().equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testVideoLandingPinnedVideo();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Video" }, description = "MODULE:05 Video || QC ID: 13488 || Verify Video Landing page Gradient")
	public void VideoLandingPageGradientCheck(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws Exception {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVideoLandingPageGradient();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Video" }, description = "MODULE:05 Video || QC ID: 13487 || Verify Zones in Video Landing page when Lazy Load Flag is enabled")
	public void VideoLandingPageLazyLoadZones(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws Exception {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVideoLandingPageLazyLoadZones();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13016 || Video Landing Page : Zone 5 Layout")
	public void VideoLandingZone1AD(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!ConfigProvider.getConfig("Platform").equals("Mob")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoZone1AD();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Regression",
			"Video" }, description = "MODULE: 05 - VIDEO || QC Test ID: 13165 || Video leaf Page : Embed Button")
	public void VideoLandingEmbedbtn(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.videoEmbedButton();
		} else {
			UMReporter.log(LogStatus.INFO, "Testing COMMENTS skipped for Production");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "VideoLeaf",
			"Video" }, description = "MODULE:05 Video || QC Test ID: 13018  || Video Landing Page - Digital Studios")
	public void VideoLanding3rdZone(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVideoLanding3rdZone();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE: SUBSCRIPTION || QC ID: 13461 || APPIA - CNNINTL-4322 - Style article not appearing correctly on iPhone 5 ")
	public void Stylepage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Mob")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.Appiastylemobile();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || STYLE Section Navigation Test")
	public void HeaderStyleSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderStyleSubSections();
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: 30 10Mins Free Preview || QC ID : 12144  || Video Playing or not")
	public void HP10MinPreview(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHP10MinPreview();
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: 30 10Mins Free Preview || QC ID : 13025,13029,12145 || Video Player elements and  X symbol")
	public void HP10MinPreviewPlay(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHP10MinPreviewPlay();
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: Video || QC ID : 13895 || Video Player elements after login validation")
	public void HP10MinPreviewPlayLogin(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHP10MinPreviewPlayLogin();
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: 30 10Mins Free Preview || QC ID : 13021 || Video Player elements : T1")
	public void HomePageHP10MinPreviewPlay(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("VIDEO") && System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHMHP10MinPreviewPlay();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device OR HP10 video is not configured for this homepage");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: 30 10Mins Free Preview || QC ID : 12155 || VIdeo Playing in devices and desktop in same browser")
	public void HomePageHP10Video(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("VIDEO") && System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHP10MinVideoOpens();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser (OR) HP10 video is not configured for this homepage");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"TripAdvisorServiceInfo", }, description = "MODULE: 04 ShareButton/SocialMedia || QC ID: 14058, 14063 || TRIP ADVISOR - CNNTRAVEL-3 - Deadline 12th Oct_Remove TripAdvisor embed styline and footer branding from travel on both DOM and INTL editions")
	public void TripAdvisorServiceInfo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		if ((System.getProperty("environment").equals("PROD")) || (System.getProperty("environment").equals("STAGE"))) {
			test.testTripAdvisorServiceInfo();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Stylecarouselpaginated" }, description = "MODULE:SECTION FRONT || QC ID: 14021 || TECH - CNN-23804 - Add Tech to CNN.com (dom) and update Tech subsection links (dom+intl) ")
	public void Stylecarouselpaginated(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.carouselpagination();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "360* Videos",
			"VRPage" }, description = "MODULE:360 VIDEO || QC ID: 14161, 14163 || To verify the Header and Footer Validations, Zone 1 Validations")
	public void VRDegree(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.VRDegree();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "360* Videos",
			"VRPage" }, description = "MODULE: 360 Video || QC ID: 14164, 14169, 14172 || To verify the Section Front Zones 2-4 Configurations and Advertisements")
	public void VRZones2to4(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVRZones2to4();
	}

	@Test(dataProvider = "GlobalTestData", groups = { /*
														 * "Regression",
														 * "RegressionINTL",
														 * "Article",
														 * "testVRVideoPage"
														 */ }, description = "360 VIDEO - CNNWE-718 - [360� Video] Special Article Page || TEST ID: 14165,14168")
	public void testVRVideoPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVRVideo();
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE:30 10mins Preveiew || QC TEST ID : 13036")
	public void ArticleHP10PinnedVideo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").equals("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHP10PinnedVideo();
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = {
			"10mins Preview" }, description = "MODULE: 30 10Mins Free Preview || QC ID : 12155 || verify whether page refreshs when video is playing")
	public void HomePageHP10Refresh(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if (System.getProperty("environment").equals("VIDEO") && System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHP10MinRefresh();
		} else {
			UMReporter.log(LogStatus.INFO, "HP10 video is not configured for this homepage");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { /*
														 * "Regression",
														 * "RegressionINTL",
														 * "Article",
														 * "testVRheaderfont"
														 */ }, description = "360 VIDEO - CNNWE-721 - [360� Video] New Fonts for Custom CSS || TEST ID: 14167")
	public void testVRheaderfont(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testVRheaderfont();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Section",
			"backslash" }, description = "MODULE: 04_01 Domestic - Politics || QC ID: 12937 || POLITICS - CNNWE-205 - Extra slash on politics section front ")
	public void LoadPagewithbackslash(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSectionPages();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Section",
			"backslash" }, description = "MODULE: 04_01 Domestic - Politics || QC ID: 12937 || POLITICS - CNNWE-205 - Extra slash on politics section front ")
	public void LoadPagewithbackslashPolitics(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSectionPagesPolitics();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Section" }, description = "MODULE: 19 Specials || QC ID: 12980 || Verify Special page descirption text color on dark theme")
	public void SpecialPageDarkTheme(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testSpecialPageDarkTheme();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL",
			"Sports" }, description = "MODULE: 04 Section Fronts 04_02 International || QC ID: 13067 || Sports Article Page should have Sport Ticker")
	public void SportsArticleValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((System.getProperty("environment").equals("PROD")) || System.getProperty("environment").equals("STAGE")
				|| (System.getProperty("environment").equals("REF"))
				|| (System.getProperty("environment").equals("TERRA"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testSportsStoryTicker();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL",
			"Sports" }, description = "MODULE: 04 Section Fronts 04_02 International || QC ID: 12991,12994 || Sports Ticker all URL Validations")
	public void SportsTickerValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testSportsTickerValidations();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Section",
			"parter" }, description = "MODULE: 04_02 International || QC Test ID: 12990 || Ticket No: TRIP ADVISOR - CNNINTL-4134 || Partner hotel widget Image Holder")
	public void IntlPartnerHotelWidget(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testPartnerHotelWidget();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header Entertainment Links")
	public void HeaderEntertainmentMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderEntertainmentMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header Health Links")
	public void HeaderHealthMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderHealthMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header BLEACHER Links")
	public void HeaderBleacherMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderBleacherMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header LIVING Links")
	public void HeaderLivingMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderLivingMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header Money Links")
	public void HeaderMoneyMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderMoneyMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || Verify Header MORE Links")
	public void HeaderMoreMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderMoreMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, priority = 2, description = "MODULE: New Navigation || QC ID: 12583 || Verify Header Logo & Nav Links")
	public void HeaderLogoNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
				&& (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderLogoNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header", "HomePage",
			"LastTen" }, priority = 2, description = "MODULE: New Navigation || QC ID: 13863 || Verify Header Logo & Nav Links in Static Pages")
	public void HeaderLogoNavStatic(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if ((System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD"))
				&& (!ConfigProvider.getConfig("Driver").equalsIgnoreCase("SAFARI"))
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderLogoNavStatic();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12586 || Verify Header Edition Picker")
	public void HeaderMenuNavFlyout(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderMenuNavFlyout();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header Opinions Links")
	public void HeaderOpinionsMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderOpinionsMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header Politics Links")
	public void HeaderPoliticsMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderPoliticsMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header STYLE Links")
	public void HeaderStyleMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderStyleMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header TECH Links")
	public void HeaderTechMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderTechMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || DOM: Verify Header TRAVEL")
	public void HeaderTravelMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderTravelMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header US Links")
	public void HeaderUSMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderUSMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header", "HomePage",
			"breaknewsBanner" }, priority = 1, description = "MODULE: New Navigation || QC ID: 12596,12611,12597,12612 || Verify Header US Links")
	public void HeaderUSMenuNavBreaking(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		if (System.getProperty("edition").equals("DOM"))
			test.testHeaderUSMenuNavBreaking();
		else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || Verify Header VIDEOS:DOM Links")
	public void HeaderVideosMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderVideosMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID:12596,12611 || Verify Header World Links")
	public void HeaderWorldMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderWorldMenuNav();

		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12597,12612 || Verify Header MORE Links")
	public void HeaderINTLFeaturesMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderINTLFeaturesMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12597,12612 || Verify Header MORE Links")
	public void HeaderINTLMoreMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderINTLMoreMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12597,12612 || Verify Header TRAVEL:INTL Links")
	public void HeaderINTLRegionsMenuNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderINTLRegionsMenuNav();
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void moneyNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.moneyNav();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void EmailsubscriptionCssValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.SubscribepageCSSvalidation();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	// hold
	@Test(dataProvider = "GlobalTestData")
	public void SectionPageAds(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {

			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testSectionPageAds();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	// hold

	@Test(dataProvider = "GlobalTestData")
	public void SectionPageAds_Intl(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {

			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testSectionPageAds();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void PoliticsSections(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testPoliticsHeaderElements();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void PoliticsPageFooterValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.PoliticsPageFooter();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderPoliticsSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHeaderPoliticsSubSections();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void PoliticsSectionOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE")
				|| System.getProperty("environment").equalsIgnoreCase("PROD")
						&& System.getProperty("edition").equalsIgnoreCase("DOM")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.politicsSectionOutbrain();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Validatepoliticssubscribebox(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testpoliticssubscribebox();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ContentNavValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testContentElements();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleMobileReadMore(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {

			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testArticleReadMore();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePageRegression(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testArticlePage();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePagepaidcontent(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")
				|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {

			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testpaidcontentoutbrain();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleGalleryElements(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testPageTopGallery();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleVidNoAutoPlay(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testArticleVideoPage();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleVideoLeafOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testArticleVideoLeafOutbrain();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleGalleryEmb(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testArticleGalleryEmb();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleGalleryLeafOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			try {
				btm.testArticleGalleryLeafOutbrain();
			} catch (Exception e) {
				UMReporter.log(LogStatus.SKIP,
						"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			}
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafBasicElements(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testVideoLeaf();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafVideoElements(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testVideoLeafVid();
		} catch (Exception e) {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafDynContent(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLeafPlayerDynamicContent();

		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");

		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPlayerNavPrev(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!ConfigProvider.getConfig("Platform").contains("PERFECTO")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLeafPlayerPrevNav();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");

		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPlayerNavNext(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (!ConfigProvider.getConfig("Platform").contains("PERFECTO")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLeafPlayerNextNav();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafOutbrain(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLeafOutbrain();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPageZone1AD(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.videoZone1AD();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPlayIcon(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		btm.testVideoPagePlayIcon();
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLandingPlayIcon(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		btm.testVideoLandingPlayIcon();
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafShareMeta(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		btm.testVideoShareMeta();
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("IE")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoPageFBShare();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("IE")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoPageTwitterShare();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void LeafPagePinnedVideoNotListed(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testPinnedVideoNotListed();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	// hold
	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPinnedVideo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLandingPinnedVideo();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafEmbeddedbtn(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.videoEmbedButton();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPageGradientCheck(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		btm.testVideoLeafPageGradient();

	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLeafPageLazyLoadZones(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		btm.testVideoLeafPageLazyLoadZones();
	}

	@Test(dataProvider = "GlobalTestData")
	public void InvalidEmailSubscription(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.invalidEmailSubscription();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ValidEmailSubscription(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.validEmailSubscription();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ExistingEmailSubscription(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.existingEmailSubscription();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void AppiaAds(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")
				&& System.getProperty("edition").equals("DOM")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testAppiaAds();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLandingDynContent(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("Safari")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testVideoLandingPlayerDynamicContent();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePageFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& ConfigProvider.getConfig("Browser").equalsIgnoreCase("Safari")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testArticlePageFBShare();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePinnedEmbedded(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("environment").equalsIgnoreCase("STAGE")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testArticlePinnedEmbedded();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePinnedVideo(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods btm = new BaseTestMethods(true);
			btm.testArticlePinnedVideo();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void QunatCastTag(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testQunatCastTag();
	}

	///
	@Test(dataProvider = "GlobalTestData")
	public void HeaderEditionValidations(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderEditionValidations();
	}

	@Test(dataProvider = "GlobalTestData")
	public void HomepageMutedWatchTv(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHomePageWatchTv();
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderBrokenLinks(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderBrokenLinks();
	}

	@Test(dataProvider = "GlobalTestData")
	public void FooterLinkValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testFooterSectionValidation();
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderMenuLinks(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderMenuLinks();
	}

	@Test(dataProvider = "GlobalTestData")
	public void HeaderSectionLinks(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderSectionLinks();
	}

	@Test(dataProvider = "GlobalTestData")
	public void BreakingnewsSocialicons(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("SWEET")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testBreakingnews();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void TimelineDarkTheme(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE") && System.getProperty("edition").equals("DOM")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testTimelineDarkTheme();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void TimelineLightTheme(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE") && System.getProperty("edition").equals("INTL")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testTimelineLightTheme();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void PageloadtimeNowhitepage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.CNNPageLoadWhitePagecheck();
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePinnedBeforeLoad(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testPinnedVideoNotListedd();
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLandingDescChange(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.VideoLandingDescChange();
	}

	@Test(dataProvider = "GlobalTestData")
	public void SearchIconValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testSearchIconValidation();
	}

	/*
	 * @Test(dataProvider = "GlobalTestData") public void
	 * ChangedLinknotlinkedtotile(Map<String, String> elem, Map<String, String>
	 * eTest, ITestContext ctx) { BaseTestMethods test = new
	 * BaseTestMethods(true); test.ChangedLinknotlinkedtotile(); }
	 */
	@Test(dataProvider = "GlobalTestData")
	public void Validatamphtmltag(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& System.getProperty("environment").equalsIgnoreCase("SWEET")) {

			BaseTestMethods test = new BaseTestMethods(true);
			test.Articlevideogalpagetag();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void LiveblogHeading(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equalsIgnoreCase("ENABLE")) {

			BaseTestMethods test = new BaseTestMethods(true);
			test.LiveblogHeader();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void Carousel360(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.Carousel360Videos();
	}

	@Test(dataProvider = "GlobalTestData")
	public void ArticleTimestampAllignment(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.ArticleTimestampAllignment();
	}

	// ------------
	@Test(dataProvider = "GlobalTestData", groups = { "Regression",
			"RegressionINTL" }, description = "MODULE: New Navigation || POLITICS Section Navigation Test")
	public void FooterPoliticsSubSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testFooterPoliticsSubSections();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Section" }, description = "MODULE: Live Blog || QC ID: 13849 || Verifying the Date dropdown box is broken on TV schedule page")
	public void TvSchedulePageDD(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testTvSchedulePageDDValidations();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Video" }, description = "MODULE: 06 Toolkit || QC Test ID: 11743,11744  || Gallery Leaf Page Outbrain validation")
	public void GalleryPagecaptioncheck(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testGalleryPageresizecaption();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13443, 13266, 13479 || Live Blog Image with Underlined Caption, Circle Shape Initial and Star(Like) Symbol Validation")
	public void LiveFyreImageUnderlineCaption(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.LiveFyreImageUnderlineCaption();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:LiveBlog ||  QC ID : 13143,13269,13237,13240,13239,13441 || Live blog social share icons")
	public void Liveblogvalidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleliveblog();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Live Blog || QC TEST ID : 13440 || Live Blog Image with Bold Caption ")
	public void LiveFyreImageBoldCaption(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.LiveFyreImageBoldCaption();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Article" }, description = "MODULE:Timeline ||  QC ID : 13270 || LiveBlog Footer Weather Section")
	public void LiveblogFooterWeatherCard(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("ENABLE")) {
			BaseTestMethods test = new BaseTestMethods(true);
			boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"),
					System.getProperty("edition"));
			if (weather == true)
				test.LiveblogFooterWeatherCard();
			else
				UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");

		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", enabled = false, groups = { /*
																		 * "Regression",
																		 * "Not Needed as per Jayesh Comments"
																		 */ }, description = "MODULE:20 Branding || QC TEST ID : 13244 || Expandable Top Banner Advertsement")
	public void TopBannerAdExpandable(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("edition").equals("DOM")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testTopBannerAdExpandable();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RegressionINTL",
			"Regression" }, description = "MODULE: 33 - ADS || QC Test ID: 13069 || Style Page : Appia and Yield Mo Ads")
	public void StyleArticleAppdiaAds(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")
				&& (System.getProperty("environment").equals("PROD")
						|| System.getProperty("environment").equals("STAGE"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testAppiaAds();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RefRegression",
			"Branding" }, description = "Special Page Branding")
	public void SpecialBranding(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testSpecialBranding();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "RefRegression", "Branding" }, description = "Video Branding")
	public void VideoBranding(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("REF")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testVideoBranding();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", enabled = false, groups = { /*
																		 * "Regression",
																		 * "RegressionINTL",
																		 * "HomePage",
																		 * "Search"
																		 * Commenting
																		 * this
																		 * as
																		 * Messenger
																		 * Icon
																		 * Removed
																		 * as
																		 * per
																		 * CNN-
																		 * 24633
																		 */ }, description = "MODULE:SUBSCRIPTION || QC ID: 13476 || Subscription Messenger Share validation ")
	public void ValidateSubscriptionMessengerShare(Map<String, String> elem, Map<String, String> eTest,
			ITestContext ctx) throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.SubscriptionMessengericon();
	}

	@Test(dataProvider = "GlobalTestData", enabled = false, groups = { /*
																		 * "Regression",
																		 * "RegressionINTL",
																		 * "Politics",
																		 * "Article",
																		 * "Social"
																		 * --
																		 * Commenting
																		 * this
																		 * as
																		 * email
																		 * Social
																		 * Icon
																		 * is
																		 * removed
																		 * from
																		 * Article
																		 * Pages
																		 * ..
																		 * CNN-
																		 * 24535
																		 */ }, description = "MODULE: 13 Global Header Navigation || QC ID: 12706,12705 ")
	public void PoliticsPageEmailShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if ((ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI"))
				&& (System.getProperty("environment").equals("PROD")
						|| System.getProperty("environment").equals("STAGE"))) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageEmailShare();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeRefTests", "SmokeTestsIntl",
			"SmokeRefTestsINTL", "Article",
			"Social" }, description = "12 Share Button/Social Media || TEST ID: 12930 & 11686")
	public void ArticlePageTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageTwitterShare();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE:13 SEARCH || QC ID: 13226 || SEARCH Sections")
	public void searchValidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.searchValidation();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE:SUBSCRIPTION || QC ID: 13465 || OurBrainOb validation ")
	public void oldbourdainvalidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.redirectbourdainpage();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE:SUBSCRIPTION || QC ID: 13468 || OutBrainOb validation ")
	public void Outbrainvalidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (System.getProperty("environment").equals("TERRA")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testOutbrainOb();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "Article",
			"Sectionsubsectionvalidation" }, description = "")
	public void Sectionsubsectionvalidation(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws ClientProtocolException, IOException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testsectionandsubsection();
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL",
			"Validatethercard" }, description = "MODULE 38:Weather || QC ID:14015  || WEATHER - CNN-24049 - Fix issue with disabling weather not removing it from the footer ")
	public void Validatethercard(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws IOException, InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		boolean weather = test.CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		if (weather == true)
			test.testWeatherCardpresent();
		else
			UMReporter.log(LogStatus.INFO, "The Weather card is disabled in the JSON");
	}

	@Test(dataProvider = "GlobalTestData", groups = { "Regression", "RegressionINTL", "HomePage",
			"Search" }, description = "MODULE:SUBSCRIPTION || QC ID: 13476 || Subscription Messenger Share validation ")
	public void ValidateSubscriptionMsgnrShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx)
			throws InterruptedException {
		BaseTestMethods test = new BaseTestMethods(true);
		test.SubscriptionMessengericon();
	}
	////////////Rajesh
}