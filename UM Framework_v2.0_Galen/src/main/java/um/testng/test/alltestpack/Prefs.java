package um.testng.test.alltestpack;

/**
 * Preferences/Settings. The structure is hierarchical so that default
 * preferences are provided but can be overridden by command-line, or via
 * another file. Preferences can also be overridden on a per-test basis,
 * provided that the preferences are being accessed from within a test.
 */
public class Prefs {
	// loads the proxy server on tests
	public static boolean useProxy = false;

	// injects Javascript into the HTML of the website to check for errors
	public static boolean testJS = false;

	// the development site URL
	public static String AUT_URL = "http://ref.next.cnn.com/";

	public static String ARTICLE_URL = "/2015/07/28/us/florida-missing-teens-boat/index.html";
	public static String articleVidUrl = "/2015/01/19/opinion/parini-jindal-comments-muslims/index.html";
	public static String articleEmVidUrl = "/2015/01/30/travel/feat-north-korean-airline-aviation-enthusiasts/index.html";
	public static String articleGalUrl = "/2015/04/24/health/infertility-advice/index.html";
	public static String articleGalEmUrl = "/2015/04/27/us/baltimore-freddie-gray-funeral/index.html";
	public static String SECTION_URL = "/tech";
	public static String GALLERY_REF_STAGE_URL = "/2014/11/07/asia/gallery/china-planes/index.html";
	public static String GALLERY_URL = "/2014/11/07/asia/gallery/china-planes/index.html";
	public static String shows_URL = "/shows/the-eighties";
	public static String special_URL = "/specials/cnn-heroes";
	public static String profile_URL = "/profiles/saeed-ahmed";
	public static String MONEY_URL = "http://money.cnn.com/";
	// public static String MONEY_INTL_URL =
	// "http://money.cnn.com/INTERNATIONAL/";
	public static String SPORTS_URL = "http://bleacherreport.com/";
	public static String SPORTS_URL_INTL = "/sport";
	public static String INTL_TRAVEL_URL = "http://travel.cnn.com/";
	public static String IREPORT_URL = "http://ireport.cnn.com/";
	public static String brokenUrl = "/thisisabrokenlink";
	public static String weatherUrl = "/weather";
	public static String videoUrl = "/videos/showbiz/2013/01/16/sbt-intv-viral-wedding-singer.hln/video/playlists/amazing-wedding-stories/";
	public static String articlePTImage = "/2015/07/22/opinions/human-trafficking-laws-u-s-/index.html";
	public static String articlePTVideo = "/2015/07/22/arts/china-ai-weiwei-passport-returned/index.html";
	public static String articlePTColl = "/2015/07/22/europe/uk-quran-birmingham-manuscript/index.html";
	public static String politicsURL = "/politics";
	public static String Style_URL = "/style";
	public static String articlePullQuote = "/2015/07/22/europe/fgm-uk-report/index.html";
	public static String article_embed_video = "/2015/11/25/asia/china-dinosaurs-special/";
	public static String search = "/search";
	// public static String articlePullQuoteRef = "";

	public static String LeafPage = "/videos/us/2015/01/15/erin-pkg-moos-pizza-delivery-car-dealership.cnn/video/playlists/wacky-world-of-jeanne-moos/";

	public static String articleEmFullImg = "/2015/07/28/us/florida-missing-teens-boat/index.html";
	public static String articleEmStdImg = "/2015/07/28/opinions/miller/index.html";
	public static String articleEmExpImg = "/2015/07/14/asia/bangladesh-sylhet-boy-lynched/?iid=ob_article_footer_expansion&iref=obinsite";
	public static String articleEmFullVid = "/2015/07/29/asia/north-korea-satellite-launch-station/index.html";
	public static String articleEmFullGal = "/2015/07/27/politics/bobby-jindal-strengthen-gun-laws/index.html";
	public static String ArticleEmFb = "/2015/07/29/us/utah-boy-book-plea/index.html";
	public static String ArticleEmTw = "/2015/04/20/living/dr-seuss-new-book-first-picture-feat/index.html";
	public static String ArticleEmYoutube = "/2015/07/28/travel/laguardia-airport-overhaul/index.html";
	public static String articleInstImage = "/2015/08/11/entertainment/robin-williams-bench-instagram-feat/";
	public static String ArticleEmTwFollow = "/2015/07/17/opinions/holloway-death-penalty-future/index.html";
	public static String ArticleComment = "/2015/05/08/opinions/yang-joss-whedon-feminism/index.html";
	public static String articleAuthorCard = "/2015/07/24/opinions/ghitis-trump-impact-america-image/?iid=ob_article_footer_expansion&iref=obnetwork";
	public static String articleWithEmbedFacebook = "/2016/06/28/entertainment/lena-dunham-kanye-west-famous-video/index.html";
	public static String ArticleFactBoxURL = "/2015/08/12/us/tucker-hipps-death/index.html";
	public static String tvScheduleUrl = "/tv/schedule/cnn";
	public static String ArticleEmColl = "/2015/02/20/health/new-virus-discovered/index.html";

	public static String layout_shows = "/shows/at-this-hour";

	public static String travel_page = "/travel/aviation";
	public static String articleAdLockShort = "/2015/12/28/entertainment/lemmy-motrhead-death/index.html";
	public static String articleAdLockLong = "/2015/11/13/health/fitness-trainer-tips-stay-motivated-exercise/index.html";
	public static String articleAdLockLongWithComment = "/2016/06/08/opinions/men-and-bystander-intervention-can-stop-rape-leslie-morgan-steiner/index.html";
	public static String mailsubscription_url = "/2015/10/30/tech/cnn-22464/index.html";
	public static String tripadvisor_url = "/2015/09/18/travel/insider-guide-mumbai/index.html";
	public static String feedback = "/feedback";
	public static String feedbackac360 = "/feedback/ac360";
	public static String feedbackstudent = "/feedback/studentnews";
	public static String gal16x9Full = "/2015/11/01/autos/toyota-s-fr-tokyo-motor-show/index.html";
	public static String gal16x9Body = "/2015/12/10/fashion/fashion-tribes-daniele-tamagni/index.html";
	public static String gal4x3Full = "/2016/01/03/arts/daisuke-takakura-clone-photographs/index.html";
	public static String videoLanding = "/videos";
	public static String article_embedwebtag = "/2016/04/27/health/automation-dont-modifymoms-who-need-mental-health-care-the-most-arent-getting-it-test/index.html";
	public static String article_embedwebtag_prod = "/2016/04/27/politics/primary-results-takeaways/index.html";
	public static String article_embedimages = "/2015/10/02/arts/instabul-biennial-armenian-genocide/index.html";
	public static String article_embedspecial = "/2016/04/27/health/automation-dont-modify-istanbul-explosion-in-central-square-kills-at-least-10/index.html";
	public static String article_content = "/2016/03/07/europe/europe-migrant-crisis-summit/";
	public static String article_embedshow = "/2016/04/26/health/automationdont-modify-americans-cutting-calories/index.html";
	public static String article_embedinstagram = "/2016/04/28/entertainment/automation-dont-modify-url-for-article-embed-instagram/index.html";
	public static String pinned_n_Embbeded = "/2016/04/27/us/severe-weather-central-united-states/index.html";
	public static String style_article = "/2016/04/27/arts/liu-bolin-invisible-man-gallery/index.html";
	public static String special_URL_darktheme = "/specials/asia/on-the-road-singapore";
	public static String hp10_URL = "/specials/hp10";
	public static String hp10_home_URL = "/";
	public static String hp10_pinned = "http://ref.next.cnn.com/2015/11/10/us/rm-hp10-pinned-player-test/index.html";
	public static String article_hktime = "/2016/04/11/opinions/women-equal-pay-day-carter/index.html";
	public static String storm_tracker_url = "/interactive/storm-tracker/";
	public static String VIDEO_360 = "/shows/the-eighties";
	public static String specialarticle_huaweilogo = "/2016/03/24/health/dennis-lo-dna-discovery/index.html";
	public static String LIVE_BLOG = "/2016/04/08/marty004-live-updates-someone-won-something-somewhere-/index.html";
	public static String topBannerAd_exp = "/?adsqa=status%3Dcnn_dps94";
	public static String ARTICLE_URL1 = "/2015/07/28/us/florida-missing-teens-boat/index.html";
	public static String articlePTGalleryPageWidth = "/2015/12/20/travel/cnnphotos-thin-line-spain-portugal-border/index.html";
	public static String articlePTAutoVideoPageWidth = "/2015/12/23/politics/amtrak-spending-sightseeing/index.html";
	// public static String AUT_URL = System.getProperty("testEnv") == null ?
	// "http://www.ref.next.cnn.com/" : "http://www.ref.next.cnn.com" ;
	public static String INTL_URL = System.getProperty("testEnv") == null ? "http://edition.ref.next.cnn.com/"
			: "http://edition.ref.next.cnn.com/";

	// sets the threshold for failing template tests because of browser
	// discrepencies - lower # = more strict
	public static int PIXEL_OFFSET = 20;

	/**
	 * REF URLs
	 */
	public static String articleUrl_Ref = "/2014/10/29/world/us-newest-allies-syrian-kurds/index.html";
	public static String articleautoplay_Ref = "/2014/10/29/world/us-newest-allies-syrian-kurds/index.html";
	public static String ArticleEmFb_Ref = "/2014/11/13/politics/republicans-immigration-shutdown/index.html";
	public static String videoleaf_Ref = "/videos/news/2015/02/23/romans-numeral-212-percent.cnnmoney/video/playlists/video-collection-test-04232015-0408-duplicate-2/";
	public static String articlePTImage_Ref = "/2015/08/26/business/regression-xmkpgwmvehimb-8252015-104444-duplicate-2/index.html";
	public static String articleEmGalUrl_Ref = "/2015/08/04/politics/test-politics-article/index.html";
	public static String factboxpullquote_Ref = "/2015/05/14/health/new-story-to-check-25-module-fact-box-layout/index.html";
	public static String articleEmImage_Ref = "/2014/11/05/politics/obama-what-now/index.html";
	public static String articlePTVideo_Ref = "/2014/11/05/politics/obama-what-now/index.html";
	public static String articlePTColl_Ref = "/2013/01/08/us/jimmy-carter---fast-facts/index.html";
	public static String ArticleEmTw_Ref = "/2015/08/26/cnnmoney/story-with-factbox--webtag/index.html";
	public static String ArticleEmYoutube_Ref = "/2014/10/23/health/ayahuasca-medicine-six-things/index.html";
	public static String ArticleEmColl_Ref = "/2014/11/10/travel/national-parks-fee-increase-grand-canyon/index.html";
	public static String articleEmStdImg_Ref = "/2015/06/11/rupert-murdoch-stepping-down/index.html";
	public static String articleEmFullGal_Ref = "/2015/07/31/entertainment/avlon-iowa-countdown-duplicate-2/index.html";
	public static String articleEmExpImg_Ref = "/2015/08/22/health/ivauh-esbwwsd-8222015-115454/index.html";
	public static String articleAuthorCard_Ref = "/2014/11/09/opinion/klass-flu-pandemic-world-war-i/index.html";
	public static String articleEmFullVid_Ref = "/2014/07/07/health/new-york-medical-marijuana/index.html ";
	// article url with standard and full width
	public static String article_ref = "/2016/03/02/style/cnn-23033/index.html";
	public static String articleWithEmbedFacebook_Ref = "/2016/08/31/entertainment/facebook-black-color-issue-t-light-and-t-dark/index.html";
	public static String Hero3_ref = "http://edition.ref.next.cnn.com/sport/equestrian";
	public static String Hero3NV_ref = "http://edition.cnn.com/sport/horse-racing";
	public static String Priority2_ref = "http://edition.ref.next.cnn.com/travel/destinations";
	public static String Priority2NV_ref = "http://edition.ref.next.cnn.com/asia";
	public static String FullBleed_ref = "http://edition.ref.next.cnn.com/americas";
	public static String FullBleedNV_ref = "http://edition.cnn.com/sport/sailing";
	public static String Priority1_ref = "http://edition.ref.next.cnn.com/opinions";
	public static String Priority1NV_ref = "http://edition.ref.next.cnn.com/china";
	public static String Balanced_ref = "http://edition.ref.next.cnn.com/travel/food-and-drink";
	public static String BalancedNV_ref = "http://edition.ref.next.cnn.com/travel/hotels";
	public static String Superhero_ref = "http://edition.ref.next.cnn.com/sport/football";
	public static String SuperheroNV_ref = "http://edition.ref.next.cnn.com/regions";
	public static String RightPriority1_ref = "http://edition.ref.next.cnn.com/travel";
	public static String RightPriority1NV_ref = "http://edition.ref.next.cnn.com/africa";
	public static String SingleColumn_ref = "http://edition.ref.next.cnn.com/sport/motorsport";
	public static String SingleColumnNV_ref = "http://edition.ref.next.cnn.com/entertainment";
	public static String Hero3Reverse_ref = "http://edition.ref.next.cnn.com/middle-east";
	public static String Hero3ReverseNV_ref = "http://edition.ref.next.cnn.com/sport/golf";
	public static String PriorityCenter_ref = "http://edition.ref.next.cnn.com/sport/skiing";
	public static String PriorityCenterNV_ref = "http://edition.ref.next.cnn.com/travel/aviation";

	/**
	 * Layout URLs
	 */
	public static String articlelayoutURL = "/2015/08/26/health/moms-girl-empowerment-clothing-parents/index.html";
	public static String featuredcomments = "http://ref.next.cnn.com/2015/02/10/health/syrian-president-to-bbc-i-get-updates-on-u-s--led-attacks-on-isis/index.html";

	/**
	 * BRANDING URLs
	 */
	public static String article_branding = "/2016/05/26/politics/hillary-clinton-donald-trump-attacks/index.html";
	public static String profile_branding = "/profiles/tricia-escobedo-profile";
	public static String gallery_branding = "/2014/05/13/world/gallery/dior-exhibition/index.html";
	public static String show_branding = "/shows/aa-1-test";
	public static String special_branding = "/specials/world/on-the-road-thailand";
	public static String video_branding = "/videos/international/2014/11/10/lead-obama-arrives-to-elaborate-show-in-china.cnn";

	/**
	 * Sponosor Layouts
	 */
	public static String card_sponsor = "http://stage.next.cnn.com/living?ntv_a=VqgBAAAAAAyToLA&prx_ro=s";
	public static String gallery_sponsor = "http://sponsorcontent.stage.next.cnn.com/cnn-next-test/article/thesonsofanarchyconanfanarthalloffame?prx_t=caQBADRwCAyToLA&prx_ro=s";
	public static String video_sponsor = "http://sponsorcontent.stage.next.cnn.com/cnn-next-test/article/why-you-need-to-have-the-talk?prx_t=VqgBAGz8CAyToLA&prx_ro=s";
	public static String ptimage_sponsor = "http://sponsorcontent.stage.next.cnn.com/cnn-next-test/article/why-you-need-to-have-the-talk?prx_t=1a8BAimwCAyToLA&prx_ro=s";
	public static String travel_sponsor = "/travel?ntv_a=F6QBAMxkCA8UMLA&prx_ro=s";

	public static String ptimage_pagewidth = "/2016/01/25/tech/automation-page-top-image--page-width/index.html";
	public static String ptcoll_pagewidth = "/2016/01/25/tech/automation-page-top-collections--page-width/index.html";

	public static String sports_article = "/2016/03/08/tennis/maria-sharapova-doping-questions-tennis/index.html";
	public static String sports_article_ref = "/2014/08/27/sport/tennis/bouchard-raonic-canadian-tennis-boom/index.html";
	public static String sports_article_terra = "/2015/11/01/tennis/tennis-radwanska-federer-nadal/index.html";
	public static String sport_article = "/2016/04/20/sport/freediving-alexey-molcanov-sea-mom/index.html";
	/**
	 * Politics Election URL's
	 */

	
	public static String ElectionHUBLanding = "/election";
	public static String Candidates = "/election/candidates";
	public static String exitPolls = "/election/results/exit-polls";
	public static String house_FullResults = "/election/results/house";
	public static String President_CountyResults = "/election/results/president";

	public static String articleAuthorURL_Ref = "/2016/05/12/entertainment/livefyre-smoke-testing-the-liveblog-slug/index.html";

	public static String Subscription = "/email/subscription";

	public static String socialShare = "/2016/06/02/us/ucla-shooting/index.html";

	public static final String feedback_go = "/feedback/go";

	public static final String gallerywhite_BG = "/2016/05/22/entertainment/billboard-music-awards-2016/index.html ";
	public static final String gallerywhiteOtherENV_BG = "/2016/08/09/entertainment/for-automation-dont-disturb---brussels-attack-isis/index.html";

	public static final String Longarticle = "/2016/06/08/opinions/men-and-bystander-intervention-can-stop-rape-leslie-morgan-steiner/index.html";

	public static String ElectionHUB = "/election/primaries";
	public static String candidate_summary = "/election/primaries/candidates/hillary-clinton";
	public static String primary_calendar = "/election/primaries/calendar/february";
	public static String candidate_state_summary = "/election/primaries/states/ga/";
	public static String candidate_state__county_results = "/election/primaries/counties/ia/Rep";
	public static String state_exit_entrance_poll_page = "/election/primaries/polls/ia/Rep";
	public static String republican_party = "/election/primaries/parties/republican";
	public static String democratic_party = "/election/primaries/parties/democrat";
	public static String exit_poll_hub = "/election/primaries/polls";
	public static String CoreResProcess = "/election/results/states/arizona/house/2";
	public static String Politics_events = "/election/events";
	public static String GovernorPage_Results = "/election/results/governor";
	public static String GPresidentialPage_Results = "/election/results/president";
	public static String Results_Page = "/election/results";
	public static String Senate_page = "/election/results/senate";
	public static String State_page = "/election/results/states";
	public static String ballotpage = "/election/results/ballot-measures";
	public static String housetpage = "/election/results/house";
	public static String President_page = "/election/results/exit-polls/indiana/governor";
	public static String PresidentExitpoll_Homepage = "/election/results/exit-polls";
	public static String sensatestatepage = "/election/results/states/alabama/senate";
	public static String governorstatepage = "/election/results/states/indiana/governor";

	public static String OldBourdain="/specials/travel/bourdain-special-page/index.html";
	
	public static final String sweetSoundCloudPlayer = "/2016/07/05/aviation/to-test-cnn-23448-ticket/index.html";
	public static final String RefSoundCloudPlayer = "/2016/07/07/tech/test-soundcloud-ticket-using-embed-link/index.html";
	public static final String ProdSoundCloudPlayer = "/2015/07/29/politics/dan-pfeiffer-meek-mill-drake-diss/index.html ";
	
	public static final String sweetVimeoPlayer = "/2016/07/07/travel/testing-cnn-23445-embed-youtube-article-leaf-page/index.html";
	public static final String RefVimeoPlayer = "/2016/07/08/us/test-embed-youtube-ticket-using-embed-link/index.html";
	public static String tripadvisor_Ref_url = "/2015/07/10/travel/tripadvisor-test-qa/index.html";
	public static final String RefAllEmbedded = "/2016/07/27/us/special-test-embed-youtube-ticket-using-embed-link-duplicate-2/index.html";
	public static String article_embedinstagram_ref = "/2016/08/24/arts/test-instagram-embed/index.html ";
	public static String EmbeddedLiveFyre_Ref = "/2016/09/22/travel/livefyre-embed-ref/index.html";
	public static String Apollo_ADS = "/?adsqa=status%3Dcnn-apollo-navpushdown_v2";
	public static String Apollo_ADSsid = "http://stage.next.cnn.com/videos/tv/2016/05/09/donald-trump-1987-interview-larry-king-live.cnn/video/playlists/cnn-1980s-video-vault";
	public static String Brandings_SWEET = "/videos/justice/2015/11/09/police-superintendent-jeremy-mardis-newday-intv.cnn/video/playlists/cnn-23158";
	public static String Brandings_REF = "/videos/health/2016/09/14/cms-14351.cnn";
	public static String Brandings_STAGE = "/videos/tech/2016/08/05/declassified-ep-8-hexagon-2.cnn";

	public static String Description_shrunk = "/specials/videos/digital-shorts";

	public static String index2html = "/index2.html";

	public static String index4html = "/index4.html";
	public static String travel= "/travel";
	public static String Animation = "http://enable.next.cnn.com/2016/09/20/app-style-section/hello-world---page-width/index.html";
	public static String Sweet_verticalgallery = "/2016/11/15/health/gallery/cnn-23957-vertical-gallery/index.html";
	public static String stage_verticalgallery = "/2016/02/28/entertainment/gallery/oscars-red-carpet-2016/index.html";
	public static String VR = "/vr";
	public static String tvshow = "/tv/shows";
	public static String tvallshows ="/specials/tv/all-shows";
	public static String Interactivebourdain = "/interactive/bourdain/";
	public static String style_carousel_paginated = "/specials/style/carousel-corrected";
	public static String VRVideopage = "/2017/02/27/vr/360-video-embed-2/index.html";
	public static String LIVE_BLOG_CAROUSEL="/2016/04/27/us/severe-weather-central-united-states/index.html";
	public static String Entertainent="/entertainment";

}