package um.testng.test.alltestpack;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonSimpleReader {

	public static Boolean parseCNNFeatures(String jsonText, String featureTag) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("features");
		// System.out.println("Into job structure, name: " +
		// structure1.get(featureTag));

		return (Boolean) structure1.get(featureTag);
	}

	public static Boolean parseVideosLandingPageIndex(String jsonText, String VideoIndexTag) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("section");
		JSONObject structure2 = (JSONObject) structure1.get("videos/index.html");
		return (Boolean) structure2.get(VideoIndexTag);
	}

	public static String parseVideoLandingIndexZones(String jsonText, String VideoIndexDim) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("section");
		JSONObject structure2 = (JSONObject) structure1.get("videos/index.html");
		JSONObject structure3 = (JSONObject) structure2.get("zones");
		JSONObject structure4 = (JSONObject) structure3.get("minWidth");
		JSONArray structure5 = (JSONArray) structure4.get(VideoIndexDim);
		return (String) structure5.toString();
	}

	public static Boolean parseVideosLeafPageIndex(String jsonText, String VideoIndexTag) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("video");
		JSONObject structure2 = (JSONObject) structure1.get("videos/index.html");
		return (Boolean) structure2.get(VideoIndexTag);
	}

	public static String parseVideoLeafIndexZones(String jsonText, String VideoIndexDim) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("video");
		JSONObject structure2 = (JSONObject) structure1.get("videos/index.html");
		JSONObject structure3 = (JSONObject) structure2.get("zones");
		JSONObject structure4 = (JSONObject) structure3.get("minWidth");
		JSONArray structure5 = (JSONArray) structure4.get(VideoIndexDim);
		return (String) structure5.toString();
	}

	public static Boolean parseCNNWeather(String jsonText, String featureTag) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("weather");
		// System.out.println("Into job structure, name: " +
		// structure1.get(featureTag));

		return (Boolean) structure1.get(featureTag);
	}

	public static Boolean parseCNNWZones(String jsonText, String featureTag) {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) jsonParser.parse(jsonText);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject structure = (JSONObject) jsonObject.get("data");
		JSONObject structure1 = (JSONObject) structure.get("section");
		JSONObject structure2 = (JSONObject) structure1.get("index.html");

		// System.out.println("Into job structure, name: " +
		// structure1.get(featureTag));

		return (Boolean) structure2.get(featureTag);
	}

}
