package um.testng.test.alltestpack;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class EnvironmentHelper {

//	public static String getURL() {
//		String Environment = System.getProperty("environment").toUpperCase();
//		String Edition = System.getProperty("edition").toUpperCase();
//		System.out.println("######" + Environment);
//		/*
//		 * if(Environment.equalsIgnoreCase("QA")){ return
//		 * ConfigProp.getPropertyValue("NBA_Teamsites"); }else
//		 * if(Environment.equalsIgnoreCase("NBA_League")){
//		 * System.out.println(ConfigProp.getPropertyValue("NBA_League")); return
//		 * ConfigProp.getPropertyValue("NBA_League");
//		 * 
//		 * }else if(Environment.equalsIgnoreCase("TBS")){ return
//		 * ConfigProp.getPropertyValue("TBS"); }else
//		 * if(Environment.equalsIgnoreCase("AutoNation")){ return
//		 * ConfigProp.getPropertyValue("AutoNation"); }else
//		 */
//		if (Environment.equalsIgnoreCase("PROD")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("PROD_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("PROD_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("STAGE")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("STAGE_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("STAGE_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("REF")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("REF_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("REF_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("ENABLE")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("ENABLE_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("ENABLE_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("SWEET")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("SWEET_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("SWEET_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("TERRA")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("TERRA_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("TERRA_INTL");
//			}
//		}else if (Environment.equalsIgnoreCase("VIDEO")) {
//			if (Edition.equalsIgnoreCase("DOM")) {
//				return ConfigProp.getPropertyValue("VIDEO_DOM");
//			} else {
//				return ConfigProp.getPropertyValue("VIDEO_INTL");
//			}
//		}
//		
//		else {
//			System.out.println(
//					"Seems to be you have mentioned your dreamy environment!!! Kindly check out the environment ");
//			UMReporter.log(LogStatus.FAIL,
//					"Seems to be you have mentioned your dreamy environment!!! Kindly check out the environment ");
//		}
//		return Environment;
//	}
	public static String getURL() {
		String Environment = System.getProperty("environment").toUpperCase();
		String Edition = System.getProperty("edition").toUpperCase();
		System.out.println("######" + System.getProperty("environment"));
		
		if (System.getProperty("environment").equalsIgnoreCase("PROD")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("PROD_DOM");
			} else {
				return ConfigProp.getPropertyValue("PROD_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("STAGE")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("STAGE_DOM");
			} else {
				return ConfigProp.getPropertyValue("STAGE_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("REF")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("REF_DOM");
			} else {
				return ConfigProp.getPropertyValue("REF_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("ENABLE")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("ENABLE_DOM");
			} else {
				return ConfigProp.getPropertyValue("ENABLE_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("SWEET")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("SWEET_DOM");
			} else {
				return ConfigProp.getPropertyValue("SWEET_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("TERRA")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("TERRA_DOM");
			} else {
				return ConfigProp.getPropertyValue("TERRA_INTL");
			}
		}else if (System.getProperty("environment").equalsIgnoreCase("VIDEO")) {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				return ConfigProp.getPropertyValue("VIDEO_DOM");
			} else {
				return ConfigProp.getPropertyValue("VIDEO_INTL");
			}
		}
		
		else {
			System.out.println(
					"Seems to be you have mentioned your dreamy environment!!! Kindly check out the environment ");
			UMReporter.log(LogStatus.FAIL,
					"Seems to be you have mentioned your dreamy environment!!! Kindly check out the environment ");
		}
		return Environment;
	}

}
