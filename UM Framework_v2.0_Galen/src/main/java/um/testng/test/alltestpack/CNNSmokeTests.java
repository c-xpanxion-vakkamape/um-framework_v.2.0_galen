package um.testng.test.alltestpack;

import java.util.Map;

import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltests.BaseTestMethods;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class CNNSmokeTests extends BaseTest {

	@Test(dataProvider = "GlobalTestData")
	public void ArticlePage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testArticlePages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void GalleryPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testGalleryPages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void MoneyPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testMoneyPage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void VideoLandingPage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testVideos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void videoleafpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testvideoleafpage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void articleemvidpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testarticleemvidpage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void brokenpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testbrokenpage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void profilepage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testprofilepage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void showspage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testshowpage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void specialpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testspecialspage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void sportpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testsportpage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void morepage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testmorepage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void entertainmentSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.entpagesection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ===jeeva====
	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void editionPicker(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.editionPicker( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 01 HomePage || Tests Home Page elements")
	public void HomePage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testHomePage( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 13 Search || QC Test ID: 11678 || Search functionality from the header")
	public void HomePageSearch(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.HomePageSearch( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page: with invalid credentials")
	public void LoginInvalidCredentials(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testLoginInvalidDetails( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void LoginSignIn(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testLoginSignIn( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void TOSBorder(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			try {
				btm.TOSBorder( );
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			// throw new SkipException(
			// "Skipping this Test as this is not Applicable to this Environment
			// or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void FeaturesSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.splpagesection( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void Sportsecfrontpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			try {
				btm.sportpagesection( );
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			// throw new SkipException(
			// "Skipping this Test as this is not Applicable to this Environment
			// or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void travelSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.travelpagesection( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void Intsecfrontpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			try {
				btm.testintsecfrontpage( );
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			UMReporter.log(LogStatus.INFO,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void Footers(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testFooters( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void FootersOtherElements(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testFootersOther( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE: 10 Sign in || QC Test ID: 11712, 11702, 11647, 11649, 11718 || Tests Login from Home Page")
	public void techSection(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.techpagesection( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// aruns
	@Test(dataProvider = "GlobalTestData", description = "MODULE:07 Article Leaf Page || QC Test ID: 11734, 11737, 11664 || Verify Article page with video and video loads correctly")
	public void articlevidpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testarticlevidpage( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", description = "MODULE:07 Article Leaf Page || QC Test ID: 11734, 11737, 11664 || Verify Article page with video and video loads correctly")
	public void articlegalpage(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.testarticlegalpage( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void StaticSkinnyNav(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods btm = new BaseTestMethods(true);
		try {
			btm.StaticSkinnyNav( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData")
	public void AdChoice(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		try {
			test.testAdChoice( );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeTestsIntl", "SmokeRefTests",
			"SmokeRefTestsINTL", "Footer", "FooterWeather",
			"LastTen" }, description = "MODULE:01 HomePage, 36 Timeline || QC Test ID: 13158,13270 || Verify Footer weather card tag")
	public void FooterWeather(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		// boolean weather =
		// test.CheckWeathercardJSON(System.getProperty("environment"),
		// System.getProperty("site"));
		// if (weather == true)
		test.testFooterWeather( );
		/*
		 * else Reporter.log("The Weather card is disabled in the JSON");
		 */
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeTestsIntl", "SmokeRefTests",
			"SmokeRefTestsINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12582 || Verify Header Live Tv")
	public void HeaderLiveTv(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testHeaderLiveTv( );
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeTestsIntl", "SmokeRefTests",
			"SmokeRefTestsINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12610 || Verify Header Search Elements")
	public void HeaderSearchElem(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderSearchElem( );
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeRefTests", "SmokeTestsIntl",
			"SmokeRefTestsINTL", "Header", "HomePage",
			"flyout" }, description = "MODULE: New Navigation || QC ID:12587 || Verify FLYOUT footer ")
	public void HeaderFlyoutFooterMenu(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderFlyoutFooterMenu( );

	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeTestsIntl", "SmokeRefTests",
			"SmokeRefTestsINTL", "Footer",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12640,12591,12609,12579 || Verify footer Edition Picker")
	public void FooterEdPick(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testFooterEdPick( );
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeTestsIntl", "SmokeRefTests",
			"SmokeRefTestsINTL", "Header",
			"HomePage" }, description = "MODULE: New Navigation || QC ID: 12578,12577,12591,12609,12579 || Verify Header Edition Picker")
	public void HeaderEdPick(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		BaseTestMethods test = new BaseTestMethods(true);
		test.testHeaderEdPick( );
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeRefTests", "SmokeTestsIntl",
			"SmokeRefTestsINTL", "Article",
			"Social" }, description = "12 Share Button/Social Media || TEST ID: 12930 & 11686")
	public void ArticlePageFBShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageFBShare( );
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "SmokeRefTests", "SmokeTestsIntl",
			"SmokeRefTestsINTL", "Article",
			"Social" }, description = "12 Share Button/Social Media || TEST ID: 12930 & 11686")
	public void ArticlePageTwitterShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {
		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& !ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticlePageTwitterShare( );
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	@Test(dataProvider = "GlobalTestData", groups = { "smokeTests", "testArticlePage", "SmokeTestsIntl",
			"SmokeRefTests", "SmokeRefTestsINTL",
			"Social" }, description = "MODULE: 12 Share Button/Social Media || Verify Article share options - Others")
	public void ArticleOtherShare(Map<String, String> elem, Map<String, String> eTest, ITestContext ctx) {

		if (ConfigProvider.getConfig("Platform").equals("Desktop")
				&& ConfigProvider.getConfig("Browser").equalsIgnoreCase("FIREFOX")) {
			BaseTestMethods test = new BaseTestMethods(true);
			test.testArticleOtherShare( );
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

}
