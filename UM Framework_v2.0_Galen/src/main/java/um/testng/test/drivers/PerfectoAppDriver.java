package um.testng.test.drivers;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.perfectomobile.selenium.MobileDriver;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class PerfectoAppDriver implements DriverInterface {
	AndroidDriver<WebElement> androidDriver = null;
	public WebDriver getNewDriver() {
		String deviceId = ConfigProvider.getConfig("Device_ID");
		String os = ConfigProvider.getConfig("OS");
		String host = ConfigProp.getPropertyValue("PerfectoHost");
		String username = ConfigProp.getPropertyValue("PerfectoUserName");
		String password = ConfigProp.getPropertyValue("PerfectoPassword");
		
		String appRelPath = ConfigProp.getPropertyValue("AppPath");		
		File appFile = new File(appRelPath);
		String appPath = appFile.getAbsolutePath();
		
		
		MobileDriver wda = new MobileDriver(host, username, password);
		String repLoc = wda.uploadMedia(appPath);
		wda.quit();
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("user", username);
		capabilities.setCapability("password", password);				
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceId);
		capabilities.setCapability(MobileCapabilityType.APP, repLoc);
//		capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, "");		
		capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.cnn.mobile.android.phone.ui.SplashActivity"); // com.turner.cnvideoapp 
		capabilities.setCapability("automationName", "appium");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, os);

		System.out.println(appPath);
		System.out.println(repLoc);
		try {
			androidDriver = new AndroidDriver<WebElement>(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);			
			UMReporter.log(LogStatus.INFO, "App driver is initiated successfully");
		} catch (Exception e) {
			e.printStackTrace();	
			UMReporter.log(LogStatus.ERROR, e.getMessage());
		}
//		return remteDriver;
		return (WebDriver)androidDriver;
	}
	
}