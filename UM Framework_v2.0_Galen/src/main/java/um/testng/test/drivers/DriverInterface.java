package um.testng.test.drivers;

import org.openqa.selenium.WebDriver;

public interface DriverInterface {
	WebDriver getNewDriver();
}
