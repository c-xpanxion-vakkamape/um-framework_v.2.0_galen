package um.testng.test.drivers;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import um.testng.test.utilities.framework.ConfigProp;

public class SauceDeskDriver implements DriverInterface {
	WebDriver remoteDriver = null;
	 public static final String URL = "https://" + ConfigProp.getPropertyValue("SauceUserName") + ":" + ConfigProp.getPropertyValue("SauceAccessKey") + "@ondemand.saucelabs.com:443/wd/hub";
	public WebDriver getNewDriver() {
	
			remoteDriver = getRemoteDriver();
	
		return remoteDriver;
		}
		public  WebDriver getRemoteDriver()
		{
			//String browserName = ConfigProvider.getConfig("Driver").split(":")[1].toUpperCase();
			DesiredCapabilities desiredCap = null;	
			 desiredCap = DesiredCapabilities.chrome();
			desiredCap.setCapability("platform", "Windows XP");
			desiredCap.setCapability("version", "43.0");
		    try{
			     remoteDriver = new RemoteWebDriver(new URL(URL), desiredCap);
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			System.out.println("Inside remote driver chrome ");
			return remoteDriver;
		}
		
}
