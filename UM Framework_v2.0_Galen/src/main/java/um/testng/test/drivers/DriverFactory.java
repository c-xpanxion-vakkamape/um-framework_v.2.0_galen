package um.testng.test.drivers;

import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class DriverFactory {
	private static ThreadLocal<WebDriver> currentDriver = new ThreadLocal<WebDriver>();

	public static WebDriver getCurrentDriver() {
		WebDriver driver = currentDriver.get();
		if(driver != null){
		return driver;
		}else{
			UMReporter.log(LogStatus.ERROR, "Driver Not Initialised");
			return null;
		}
	}
	public static void sauceDriverInit()
	{
		switch (ConfigProvider.getConfig("Driver").split(":")[1].toUpperCase()) {
	case "FIREFOX":
		currentDriver.set(new SauceDeskDriver().getNewDriver());
		break;	
	case "CHROME":
		currentDriver.set(new SauceDeskDriver().getNewDriver());
		break;	
	case "IE":
		currentDriver.set(new SauceDeskDriver().getNewDriver());
		break;	
		}
	}
	public static void driverInit() {
		switch (ConfigProvider.getConfig("Driver").toUpperCase()) {
		case "PERFECTO":
			currentDriver.set(new PerfectoDriver().getNewDriver());
			break;
		case "APPIUM":
			currentDriver.set(new AppiumDriver().getNewDriver());
			break;
		case "FIREFOX":
			currentDriver.set(new DeskDriver().getNewDriver());
			break;	
		case "CHROME":
			currentDriver.set(new DeskDriver().getNewDriver());
			break;	
		case "IE":
			currentDriver.set(new DeskDriver().getNewDriver());
			break;	
		case "APPIUMAPP":
			currentDriver.set(new AppiumAppDriver().getNewDriver());
			break;
		case "PERFECTOAPP":
			currentDriver.set(new PerfectoAppDriver().getNewDriver());
			break;
		case "SAUCE":
			currentDriver.set(new SauceDriver().getNewDriver());
			break;
		case "SAFARI":
			currentDriver.set(new DeskDriver().getNewDriver());
			break;
		default:
			System.out.println("Unknown Driver");
		}
	}

	public static void driverCleanUp() {
		WebDriver driver = currentDriver.get();
		if(driver != null){
		switch (ConfigProvider.getConfig("Driver").toUpperCase()) {
		case "APPIUMAPP":	
			AndroidDriver<?> k = (AndroidDriver<?>)DriverFactory.getCurrentDriver();
			k.closeApp();
			UMReporter.log(LogStatus.INFO, "App is closed successfully");
			break;
		default:
			getCurrentDriver().close();
			break;
		}		
		getCurrentDriver().quit();
		
	}
	currentDriver.remove();
}
}
