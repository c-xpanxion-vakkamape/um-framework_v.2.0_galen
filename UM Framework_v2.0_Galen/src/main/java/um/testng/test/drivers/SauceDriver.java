package um.testng.test.drivers;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;

public class SauceDriver implements DriverInterface {
	
	
	  public static final String URL = "https://" + ConfigProp.getPropertyValue("SauceUserName") + ":" + ConfigProp.getPropertyValue("SauceAccessKey") + "@ondemand.saucelabs.com:443/wd/hub";
	 
	WebDriver remoteDriver = null;
	public WebDriver getNewDriver() {
		if(ConfigProvider.getConfig("OS").equalsIgnoreCase("ios")){
		
			remoteDriver = getRemoteDriverIOS();
		}
		else if(ConfigProvider.getConfig("OS").equalsIgnoreCase("Android"))
		{
			remoteDriver = getRemoteDriverAndroid();
		}
		return remoteDriver;
	}
	public WebDriver getRemoteDriverAndroid() {
		String browserName = ConfigProvider.getConfig("Browser");
		System.out.println("Inside Sauce Android driver");
		System.out.println("Insde sauce " + browserName);
		    DesiredCapabilities capabilities = new DesiredCapabilities();
		    capabilities.setCapability("platformName", "Android");
		    capabilities.setCapability("deviceName", "Android GoogleAPI Emulator");
		    capabilities.setCapability("platformVersion", "5.0");
		    capabilities.setCapability("browserName", "Browser");
		    capabilities.setCapability("deviceOrientation", "portrait");
		 try{
		     remoteDriver = new AndroidDriver<>(new URL(URL), capabilities);
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		return remoteDriver;
	}
	public WebDriver getRemoteDriverIOS() {
		String browserName = ConfigProvider.getConfig("Browser");
		System.out.println("Inside Sauce iOS driver");
		System.out.println("Insde sauce " + browserName);
		    DesiredCapabilities capabilities = new DesiredCapabilities();
		    capabilities.setCapability("platformName", "iOS");
		    capabilities.setCapability("deviceName", "iPhone 5s");
		    capabilities.setCapability("platformVersion", "9.1");
		    capabilities.setCapability("browserName", "Safari");
		    capabilities.setCapability("deviceOrientation", "portrait");
		 try{
		     remoteDriver = new IOSDriver<>(new URL(URL), capabilities);
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
		return remoteDriver;
	}
}
