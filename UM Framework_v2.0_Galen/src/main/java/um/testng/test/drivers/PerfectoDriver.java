package um.testng.test.drivers;

import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.perfectomobile.selenium.MobileDriver;
import com.perfectomobile.selenium.api.IMobileDevice;
import com.perfectomobile.selenium.api.IMobileWebDriver;
import com.perfectomobile.selenium.options.MobileBrowserType;
import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class PerfectoDriver implements DriverInterface {
	MobileDriver wda = null;
	IMobileWebDriver domDriver = null;
	IMobileDevice device = null;
	WebDriver remoteDriver = null;

	public WebDriver getNewDriver() {
		String isRemote = ConfigProp.getPropertyValue("IsRemote");
		if ("yes".equalsIgnoreCase(isRemote)) {
			remoteDriver = getRemoteDriver();
			//return remoteDriver;
		} 
		return remoteDriver;
	}

	public IMobileDevice getDevice() {
		if (device == null) {
//			String deviceId = ConfigProp.getPropertyValue("DeviceId");
			String deviceId = ConfigProvider.getConfig("Device_ID");
			if (wda == null) {
				// String host = Configuration.getPropertyValue("Host");
				// String username = Configuration.getPropertyValue("UserName");
				// String password = Configuration.getPropertyValue("Password");
				// wda = new MobileDriver(host, username, password);
				wda = new MobileDriver();
			}			
			device = wda.getDevice(deviceId);
			UMReporter.log(LogStatus.INFO, "Taking device id -"+deviceId);
//			String inUse = device.getProperty("In use");
			device.open();
			device.home();
		}
		return device;
	}

	public WebDriver getRemoteDriver() {
		String browserName = "mobileOS";
		DesiredCapabilities capabilities = new DesiredCapabilities(browserName, "", Platform.ANY);
		String host = ConfigProp.getPropertyValue("PerfectoHost");
		String username = ConfigProp.getPropertyValue("PerfectoUserName");
		String password = ConfigProp.getPropertyValue("PerfectoPassword");
		String deviceId = ConfigProvider.getConfig("Device_ID");
		capabilities.setCapability("user", username);
		capabilities.setCapability("password", password);
		capabilities.setCapability("deviceName", deviceId);

		// Use the automationName capability to define the required framework -
		// Appium (this is the default) or PerfectoMobile.
		capabilities.setCapability("automationName", "PerfectoMobile");
		try {
			remoteDriver = new RemoteWebDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);			
//			System.out.println("########################33INSTANCE CREATED");			
		} catch (Exception e) {
			e.printStackTrace();
			UMReporter.log(LogStatus.ERROR,e.getMessage());
		}
		return remoteDriver;
	}
}