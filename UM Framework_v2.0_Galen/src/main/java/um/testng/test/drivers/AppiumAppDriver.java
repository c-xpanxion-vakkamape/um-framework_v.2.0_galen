package um.testng.test.drivers;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;

public class AppiumAppDriver implements DriverInterface {
	WebDriver remoteDriver = null;
	public WebDriver getNewDriver() {
		
		String grid_HN= ConfigProp.getPropertyValue("Appium_Grid_HostName");
		String grid_port= ConfigProp.getPropertyValue("Appium_Grid_Port");
		String host="http://"+grid_HN+":"+grid_port+"/wd/hub";
		System.out.println("appium driver host"+host);
//		String host = ConfigProp.getPropertyValue("AppiumHost");
		String deviceName = ConfigProvider.getConfig("Device_ID");
		String deviceId= ConfigProp.getPropertyValue(deviceName); 
		String os = ConfigProvider.getConfig("OS");
		String appRelPath = ConfigProp.getPropertyValue("AppPath");
		File appFile = new File(appRelPath);
		String appPath = appFile.getAbsolutePath();
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, os);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceId);
		capabilities.setCapability(MobileCapabilityType.APP, appPath);
		System.out.println(appPath);
		try {
			remoteDriver = new AndroidDriver<WebElement>(new URL(host), capabilities);
			UMReporter.log(LogStatus.INFO, "App driver is initiated successfully");
		} catch (Exception e) 
		{
			e.printStackTrace();	
			UMReporter.log(LogStatus.ERROR, e.getMessage());
		}
		return remoteDriver;
	}
	
	

}