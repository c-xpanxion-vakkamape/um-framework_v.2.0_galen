package um.testng.test.alltests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.SkipException;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltestpack.EnvironmentHelper;
import um.testng.test.alltestpack.JsonReader;
import um.testng.test.alltestpack.JsonSimpleReader;
import um.testng.test.alltestpack.Prefs;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.pom.elements.BasePage;
import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.HomePage;
import um.testng.test.pom.elements.HomePage.weatherType;
import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.pom.elements.SpecialPage;
import um.testng.test.pom.elements.TvPages;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.pom.elements.WeatherPage;
import um.testng.test.pom.elements.sportsPage;
import um.testng.test.pom.functions.ArticlePageFunctions;
import um.testng.test.pom.functions.CommomPageFunctions;
import um.testng.test.pom.functions.GalleryPageFunctions;
import um.testng.test.pom.functions.HomePageFunctions;
import um.testng.test.pom.functions.LeafPageFunctions;
import um.testng.test.pom.functions.MoneyPageFunctions;
import um.testng.test.pom.functions.PoliticsPageFunctions;
import um.testng.test.pom.functions.ProfilesPageFunctions;
import um.testng.test.pom.functions.SectionFrontsPageFunctions;
import um.testng.test.pom.functions.SpecialPageFunctions;
import um.testng.test.pom.functions.StylePageFunctions;
import um.testng.test.pom.functions.SubscriptionPageFunctions;
import um.testng.test.pom.functions.TvPagesFunctions;
import um.testng.test.pom.functions.VideoLandingPageFunctions;
import um.testng.test.pom.functions.VideoLeafPageFunctions;
import um.testng.test.pom.functions.WeatherPageFunctions;
import um.testng.test.pom.functions.sportsPageFunctions;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.TestDataProvider;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class BaseTestMethods {

	ArticlePageFunctions apf = new ArticlePageFunctions();
	GalleryPageFunctions gpf = new GalleryPageFunctions();
	MoneyPageFunctions mpf = new MoneyPageFunctions();
	VideoLandingPageFunctions vlpf = new VideoLandingPageFunctions();
	VideoLeafPageFunctions vpf = new VideoLeafPageFunctions();
	ProfilesPageFunctions ppf = new ProfilesPageFunctions();
	SpecialPageFunctions spf = new SpecialPageFunctions();
	sportsPageFunctions sprtpf = new sportsPageFunctions();
	SectionFrontsPageFunctions secpf = new SectionFrontsPageFunctions();
	String url;

	public BaseTestMethods(boolean condi) {
		if (condi) {
			DriverFactory.driverInit();
			UMReporter.log(LogStatus.INFO,
					"OS-" + ConfigProvider.getConfig("OS") + "Browser-" + ConfigProvider.getConfig("Browser"));
		} else {
			UMReporter.log(LogStatus.INFO, "No browser interaction");
		}
	}

	public void testArticlePages() {
		// Below code take the urls from excell
		// String secondUrl =TestDataProvider.getData("URLs");
		String url = EnvironmentHelper.getURL() + Prefs.ARTICLE_URL;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		apf.testArticlePages();
	}

	public void testGalleryPages() {
		if (System.getProperty("environment").equalsIgnoreCase("REF")) {
			String url = EnvironmentHelper.getURL() + Prefs.GALLERY_URL;
			WrapperMethods.enter_URL(url);
			WrapperMethods.checkallpageload();
			gpf.testGalleryPage();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	public void testMoneyPage() {
		String url = Prefs.MONEY_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		mpf.testMoneyPage();
	}

	public void testVideos() {
		String url = EnvironmentHelper.getURL() + Prefs.videoLanding;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vlpf.testVideolandingpage();
	}

	public void testvideoleafpage() {
		String url = EnvironmentHelper.getURL() + Prefs.videoUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vpf.testVideoleafgpage();
	}

	public void testarticleemvidpage() {
		String url = EnvironmentHelper.getURL() + Prefs.articleEmVidUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vpf.testarticleemvidpage();
	}

	public void testbrokenpage() {
		String url = EnvironmentHelper.getURL() + Prefs.brokenUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		apf.testbrokenpageurl();
	}

	public void testprofilepage() {
		String url = EnvironmentHelper.getURL() + Prefs.profile_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		ppf.testprofilepage();
	}

	public void testshowpage() {
		String url = EnvironmentHelper.getURL() + Prefs.shows_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		spf.testshowpage();
	}

	public void testspecialspage() {
		String url = EnvironmentHelper.getURL() + Prefs.special_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		spf.testshowpage();
	}

	public void testsportpage() {
		if (System.getProperty("environment").equalsIgnoreCase("PROD")) {

			String url = Prefs.SPORTS_URL;
			WrapperMethods.enter_URL(url);
			WrapperMethods.checkallpageload();
			sprtpf.testsportpage();
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");

		}
	}
	/////

	// ==================jeeva
	public static void closeTermsOfService(By li) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (!driver.findElement(li).isDisplayed()) {

		}
		try {
			wait.until(ExpectedConditions.elementToBeClickable(li));
		} catch (Exception e) {
			// error.addError("TEST");
		}
		try {
			// li.click();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", li);
			Reporter.log("INFO: Terms of Service was present & closed");
		} catch (Exception e) {
			Reporter.log("INFO: Terms of Service is not present");
		}

	}

	public void skipAdv() {
		try {
			WrapperMethods.moveToElementClick(HomePage.skip());
			UMReporter.log(LogStatus.PASS, "Ad is present and its closed");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Ad is not present");
		}
	}

	public static void testHomePage(String browser, String machineType, String edition) {
		try {
			WrapperMethods.moveToElementClick(HomePage.skip());
			UMReporter.log(LogStatus.PASS, "Ad is present and its closed");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Ad is not present");
		}

		WrapperMethods.verifyElement(HomePage.getTermsServiceConsentClose(), "Terms Service Consent ");

		WrapperMethods.click(HomePage.getTermsServiceConsentClose(), "Terms Service Consent");

		try {
			if (edition.equalsIgnoreCase("DOM")) {
				WrapperMethods.waitPresenceOfEleLocated(HomePage.verifyGoogleAd(), 20);
				WrapperMethods.waitPresenceOfEleLocated(HomePage.verifyGoogleAdCont1(), 20);
			} else if (edition.equalsIgnoreCase("INTL")) {
				WrapperMethods.waitPresenceOfEleLocated(HomePage.verifyGoogleAdCont3Inlt(), 20);
				WrapperMethods.waitPresenceOfEleLocated(HomePage.verifyGoogleAdContIntl(), 20);
			}
			UMReporter.log(LogStatus.PASS, "Google Adv is present.");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Google Adv not is present.");
		}
	}

	public static void preSteps() {
		try {
			if (WrapperMethods.isDisplayed(HomePage.skip())) {
				WrapperMethods.waitAndClick(HomePage.skip(), 30);
				UMReporter.log(LogStatus.PASS, "Skip Ad is present and skipped");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Skip Ad is not present");
		}

		try {
			if (WrapperMethods.isDisplayed(HomePage.getBreakingNewsClose())) {

				WrapperMethods.expectedResult(HomePage.getBreakingNewsClose(),
						"Breaking News button is present and its closed", "Breaking News button is not present ");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Breaking News button is not present");
		}

		try {
			if (WrapperMethods.isDisplayed(HomePage.getTermsServiceConsentClose())) {
				WrapperMethods.waitAndClick(HomePage.getTermsServiceConsentClose(), 30,
						"Cookie Consent was present and its closed", "Cookie Consent is not closed");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Cookie Consent was not present");
		}
	}

	// ========================================================================================================================//
	public void testHomePage() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		skipAdv();
		testHomePage(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void HomePageSearch() {
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		hpf.testHomePageSearch(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testLoginInvalidDetails() {
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(Prefs.AUT_URL + "login.html");
		WrapperMethods.checkallpageload();
		hpf.testLoginInvalidDetails(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testLoginSignIn() {
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(Prefs.AUT_URL + "mycnn");
		WrapperMethods.checkallpageload();
		hpf.testLoginSignIn(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void TOSBorder() {
		String browser = ConfigProvider.getConfig("Browser");
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		hpf.TOSBorder(browser, ConfigProvider.getConfig("Platform"), System.getProperty("edition"));
	}

	public void editionPicker() {
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		hpf.editionPicker(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void splpagesection() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		hpf.splpagesection(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testFooters() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		hpf.testFooters(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testFootersOther() {
		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		boolean weather = CheckWeathercardJSON(System.getProperty("environment"), System.getProperty("edition"));
		WrapperMethods.checkallpageload();
		hpf.testFootersOther(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"), weather);
	}

	public void testintsecfrontpage() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		hpf.testintsecfrontpage(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"), currentURL);
	}

	public void sportpagesection() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		hpf.sportpagesection(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void travelpagesection() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		hpf.travelpagesection(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void techpagesection() {
		HomePageFunctions hpf = new HomePageFunctions();
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		skipAdv();
		hpf.techpagesection(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public boolean CheckWeathercardJSON(String Environent, String site) {

		String Site = "";
		Boolean Test = false;
		if (site.equalsIgnoreCase("DOM")) {
			Site = "domestic";
		} else {
			Site = "international";
		}
		try {
			JSONObject test = JsonReader.readJsonFromUrl(
					"http://data.cnn.com/cfg/v1/" + Site + "/" + Environent.toLowerCase() + "/michonne.json");
			String jsonText = test.toString();
			Test = JsonSimpleReader.parseCNNWeather(jsonText, "enableInFooter");
			Reporter.log("Sample stest" + Test);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Test;
	}

	public void testarticlevidpage() {
		ArticlePageFunctions apf = new ArticlePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleVidUrl);
		WrapperMethods.checkallpageload();
		apf.testarticlevidpage(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testarticlegalpage() {
		GalleryPageFunctions gpf = new GalleryPageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleGalUrl);
		WrapperMethods.checkallpageload();
		gpf.testarticlegalpage(ConfigProvider.getConfig("Browser"), ConfigProvider.getConfig("Platform"),
				System.getProperty("edition"));
	}

	public void testmorepage() {

		String url = EnvironmentHelper.getURL();
		String secondUrl = TestDataProvider.getData(System.getProperty("edition"));
		System.out.println(url + secondUrl);
		WrapperMethods.enter_URL(url + secondUrl);
		WrapperMethods.checkallpageload();
		apf.testmorepage();
	}

	public void entpagesection() {

		String url = EnvironmentHelper.getURL();
		String secondUrl = TestDataProvider.getData("Data");
		if (!secondUrl.startsWith("/"))
			WrapperMethods.enter_URL(secondUrl);
		else
			WrapperMethods.enter_URL(url + secondUrl);
		WrapperMethods.checkallpageload();
		apf.testentpagesection(secondUrl);
	}

	public void StaticSkinnyNav() {
		if (System.getProperty("environment").equalsIgnoreCase("PROD")
				|| System.getProperty("environment").equalsIgnoreCase("STAGE")) {
			String url = EnvironmentHelper.getURL();
			String secondUrl = TestDataProvider.getData("Data");
			if (!secondUrl.startsWith("/"))
				WrapperMethods.enter_URL(secondUrl);
			else
				WrapperMethods.enter_URL(url + secondUrl);
			WrapperMethods.checkallpageload();
			apf.StaticSkinnyNav(url + secondUrl);
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}

	}

	public void testAdChoice() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();

		WebElement adchoice = WrapperMethods.getWebElement(HomePage.getAdChoice());
		if (adchoice.isDisplayed()) {
			UMReporter.log(LogStatus.PASS, "<BR>PASSED: Ad choice is Displayed<BR>");
			WrapperMethods.clickJavaScript(HomePage.getAdChoice());
			HomePage.wait(10);
			MethodDef.explicitWaitVisibility(HomePage.getAdChoiceFrame(), "Ad Choice Frame element is loaded",
					"Ad Choice Frame element is not loaded");

			if (!ConfigProvider.getConfig("Platform").equals("Perfecto")) {
				driver.switchTo().frame(WrapperMethods.getWebElement(HomePage.getAdChoiceFrame()));
				try {
					driver.switchTo().frame(WrapperMethods.getWebElement(HomePage.getAdChoiceFrame()));
					HomePage.wait(2);
				} catch (Exception E) {
				}
				try {
					WebElement adchoiceCon = driver.findElement(By.cssSelector(".headTitle"));
					if (adchoiceCon.isDisplayed() && adchoiceCon.getText().length() > 0) {
						UMReporter.log(LogStatus.PASS, "<BR>PASSED: Ad choice content is Displayed<BR>");
					} else {
						UMReporter.log(LogStatus.FAIL, "<BR>Failed: Ad choice content is not Displayed<BR>");
						// error.addError("Ad Choice content is not displayed");
					}
				} catch (Exception e) {
					UMReporter.log(LogStatus.FAIL, "<BR>Failed: Ad Choice element is not accessible<BR>");
					// error.addError("Ad Choice element is not accessible" +
					// e.getMessage());
				}
			}
		} else {
			UMReporter.log(LogStatus.FAIL, "<BR>Failed: Ad Choice is not displayed<BR>");
			// error.addError("Ad Choice is not displayed");
		}
		/*
		 * if (error.hasError()) { Assert.fail(error.getErrors()); }
		 */
	}

	public void testHeaderLiveTv() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		HomePageFunctions home = new HomePageFunctions();

		HomePageFunctions.headerLiveTv();

	}

	public void testFooterWeather() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		try {
			WrapperMethods.waitForVisiblity(HomePage.getFooterWeatherContent(), 60, "Footer Weather card is loaded",
					"Footer Weather card is not loaded in specified time");

			WrapperMethods.verifyElement(HomePage.getFooterWeatherContent(), "Footer Weather card");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getFooterWeatherContent()).getCssValue("padding-top")
							.equals("2px"),
					"The top padding for footer weather is present and the value is : " + WrapperMethods
							.getWebElement(HomePage.getFooterWeatherContent()).getCssValue("padding-top"),
					"The top padding for footer weather is not present");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getFooterWeatherContent()).getCssValue("padding-bottom")
							.equals("10px"),
					"The bottom padding for footer weather is present and the value is : " + WrapperMethods
							.getWebElement(HomePage.getFooterWeatherContent()).getCssValue("padding-bottom"),
					"The bottom padding for footer weather is not present");

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String Text = (String) executor.executeScript("return document.getElementsByTagName('html')[0].innerHTML");

			WrapperMethods.assertIsTrue(Text.contains("WebpackAssets"), "the WEBPACKASSETS is Present in page source",
					"the WEBPACKASSETS is not Present in page source");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "<BR>Weather card is not present in the footer<BR>");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}

	}

	public void testHeaderSearchElem() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		HomePageFunctions home = new HomePageFunctions();
		HomePageFunctions.headersearchElem();

	}

	public void testHeaderFlyoutFooterMenu() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		HomePageFunctions home = new HomePageFunctions();
		HomePageFunctions.headerFlyoutfooterMenu();

	}

	public void testHeaderEdPick() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		HomePageFunctions home = new HomePageFunctions();
		HomePageFunctions.headerEditionPicker();

	}

	public void testFooterEdPick() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		HomePageFunctions home = new HomePageFunctions();
		HomePageFunctions.footerEditionPicker();

	}

	public void testArticlePageFBShare() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();

		// LeafPageFunctions
		ArticlePageFunctions.fbShareValidations();

	}

	public void testArticlePageTwitterShare() {

		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();

		// LeafPageFunctions
		ArticlePageFunctions.TwShareValidations();

	}

	public void testArticleOtherShare() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		Actions actions = new Actions(driver);

		try {
			WrapperMethods.waitAndLocate(By.id("gigyaShareBar_4_gig_containerParent"), 30, "Share Bar is Loaded",
					"Share Bar is not Loaded");
			UMReporter.log(LogStatus.PASS, "<br>PASSED: Gigya Share bar is present</br>");
			WrapperMethods.waitForClickable(ArticlePage.getGigyaBarElem4(), 30,
					"Other Share element is Loaded and Clickable", "Other Share element is not Loaded");
			WrapperMethods.waitForVisiblity(ArticlePage.getGigyaBarElem4(), 30, "Other Share element is visible",
					"Other Share element is not visible");
			int width = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
			if (width > 600 && !ConfigProvider.getConfig("Browser").startsWith("IE")) {
				try {
					// ArticlePage.wait(5);
					if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI"))
						actions.moveToElement(WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem4())).click()
								.build().perform();
					else if (ConfigProvider.getConfig("Browser").startsWith("IE")
							&& !ConfigProvider.getConfig("Browser").equals("IE11")) {
						// ArticlePage.wait(5);
						JavascriptExecutor executor = (JavascriptExecutor) driver;
						executor.executeScript("arguments[0].click();",
								WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem4()));
					} else
						WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem4()).click();
					try {
						WrapperMethods.waitForVisiblity(ArticlePage.getGigyaBarOthers(), 30,
								"Others Share element popup window is visible",
								"Others Share element popup window is not visible");

					} catch (Exception e) {
						WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem4()).click();
						WrapperMethods.waitForVisiblity(ArticlePage.getGigyaBarOthers(), 30,
								"Others Share element popup window is visible",
								"Others Share element popup window is not visible");

					}
					// actions.moveToElement(article.getGigyaBarOthers(wf.go())).click().build().perform();
					// ArticlePage.wait(2);
					for (WebElement linkUrl : ArticlePage.getGigyaBarOtherLinks()) {
						ArticlePage.wait(5);
						// wait.until(ExpectedConditions.elementToBeClickable(linkUrl));
						WrapperMethods.assertIsTrue(linkUrl.isDisplayed(), "link URL is not displayed",
								"link URL is displayed");
						WrapperMethods.assertIsTrue(!linkUrl.getAttribute("provider").isEmpty(),
								"Share link has no text", "Share link has text");
						UMReporter.log(LogStatus.INFO,
								"<br>INFO: Test complete for LINK" + linkUrl.getAttribute("provider") + "</br>");
					}
				} catch (Exception e) {
					UMReporter.log(LogStatus.FAIL, "<br>FAILED: Error in Other Share Links </br>" + e.getMessage());
					UMReporter.log(LogStatus.INFO, e.getMessage());

				}
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "FAILED: Issue in the Gigya Share bar");
			UMReporter.log(LogStatus.INFO, E.getMessage());

		}

		/*
		 * if (error.hasError()) { Assert.fail(error.getErrors()); }
		 */
	}

	public void testHeaderOpinionsSubSections() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("dom_header_opinions").split(",");

		secpf.sectionsValidation("Opinion", Prefs.AUT_URL, domData);
	}

	public void testHeaderUSSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String[] domData;
		if (System.getProperty("edition").equalsIgnoreCase("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_us").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_us").split(",");

		secpf.sectionsValidation("U.S.", EnvironmentHelper.getURL(), domData);

	}

	public void testHeaderWorldSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_world").split(",");
		secpf.sectionsValidation("World", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderTravelSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String[] domData;
		if (System.getProperty("edition").equalsIgnoreCase("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_travel").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_travel").split(",");
		secpf.sectionsValidation("Travel", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderVideoSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String[] domData;
		if (System.getProperty("edition").equalsIgnoreCase("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_videos").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_videos").split(",");
		secpf.sectionsValidation("Video", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderMoreSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String[] domData;
		if (System.getProperty("edition").equalsIgnoreCase("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_more").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_more").split(",");

		secpf.sectionsValidation("More�", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderEntertainmentSubSections() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.SECTION_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();

		SectionFrontsPage section = new SectionFrontsPage();

		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_entertainment").split(",");

		else
			domData = ConfigProp.getPropertyValue("ed_header_entertainment").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Entertainment", Prefs.AUT_URL, domData);

	}

	public void testHeaderTechSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.SECTION_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();

		SectionFrontsPage section = new SectionFrontsPage();

		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_tech").split(",");

		else
			domData = ConfigProp.getPropertyValue("ed_header_tech").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Tech", Prefs.AUT_URL, domData);

	}

	public void testHeaderINTLFeaturesSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);

		String[] domData;
		domData = ConfigProp.getPropertyValue("ed_header_features").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Features", EnvironmentHelper.getURL(), domData);
	}

	public void weatherPageValidation() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		if (!ConfigProvider.getConfig("Platform").equals("Perfecto")) {
			WrapperMethods.page_scrollDown();
			WrapperMethods.scrollIntoViewByElement(
					By.xpath("//span[@class='el-weather__footer-location js-el-weather__footer-location']"));
			WeatherPage.wait(5);
			WrapperMethods.clickJavaScript(HomePage.getfooterWeather(), "Footer Weather is clicked ",
					"Footer Weather is not clicked");
			WeatherPage.wait(5);
			WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
			WeatherPageFunctions.weatherforecast();
			WeatherPageFunctions.testExtendedForecast();
			WeatherPageFunctions.weatherZip();
		}

	}

	public void testWeatherCardElem() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		if (System.getProperty("edition").equals("DOM") && ConfigProvider.getConfig("Platform").equals("Desktop")) {
			try {
				HomePageFunctions.validateWeatherCard();
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Weather card Element is not present");
			}
		} else
			UMReporter.log(LogStatus.INFO, " Weather card validation skipped for home page & Devices");
	}

	public void testChangeTemp() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		try {
			if (!ConfigProvider.getConfig("Platform").equals("Perfecto")) {
				WeatherPageFunctions.changeWeatherUnitType("celsius");
				WeatherPageFunctions.changeWeatherUnitType("fahrenheit");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing weather unit elements");
		}
	}

	public void testCurrentConditions() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		UMReporter.log(LogStatus.INFO, "Current Conditions");
		WrapperMethods.clickJavaScript(WeatherPage.getFaren());
		if (!ConfigProvider.getConfig("Platform").equals("Perfecto")) {
			WrapperMethods.verifyElement(WeatherPage.getCurrconditionTitle(), "Current conditions title ");
			WrapperMethods.verifyElement(WeatherPage.getWeatherIcon(), "Icon ");
			WrapperMethods.textNotEmpty(WeatherPage.getCurrDetails(), "Current Conditons details ");
		}

	}

	public void testHomePageFooterSearchBlank() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO,
					"<br><b><i><em>Info: Testing Home Page Footer Search CNN: Entering Blank Text</b></i></em>");
			WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());
			WrapperMethods.waitForPageLoaded(driver);
			Thread.sleep(3000);
			String content2 = "Your search did not match any documents.";
			WrapperMethods.contains_Text(HomePage.getSearchEmpty(), content2,
					" - Your search did not match message posted successfully .. ",
					" - Your search did not match message not posted successfully..");
		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing the Home Page Search CNN Section");
		}
	}

	public void testHomePageFooterSearchValid(String defaulttext) {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		try {
			UMReporter.log(LogStatus.INFO, "Testing Home Page Footer Search CNN: Entering valid Text");
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				String Searchtext = "Bill Gates";

				WrapperMethods.isDisplayed(HomePage.getSearchField(), "The search field is expanded",
						"The search field is not expanded");

				WrapperMethods.validatesearchfieldtext(defaulttext);

				try {
					WrapperMethods.sendkeys(HomePage.getHomeFooterSearchCNNBox(), Searchtext, "Entered the text ",
							"Not able to enter the text ");
				} catch (Exception ee) {
					WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchCNNBoxCC());
					WrapperMethods.sendkeys(HomePage.getHomeFooterSearchCNNBox(), Searchtext, "Entered the text ",
							"Not able to enter the text ");
				}

				WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());

				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".search-results_msg")));
				} catch (Exception e) {
					try {
						WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());
						wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".search-results_msg")));

					} catch (Exception E) {
						UMReporter.log(LogStatus.ERROR,
								"Page did not load after click/Click operation failed... Please check");
					}
				}

				HomePageFunctions.testSearchSummaryDesktop();
				HomePageFunctions.testSearchResults();

				WrapperMethods.contains_Text_Attribute(HomePage.getSearchField(), "value", Searchtext,
						"The Page has been launced with the derived results",
						"The page has not launced with the results expected"
								+ WrapperMethods.getWebElement(HomePage.getsearchlaunchpage()).getText());

				UMReporter.log(LogStatus.INFO, "====Radio button validation====");

				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.domesticsearchradio_btn(HomePage.getradiotext());
				} else {
					WrapperMethods.INTLsearchradio_btn(HomePage.getradiotext());
				}

			} else if (ConfigProvider.getConfig("Platform").equals("Mob")
					|| ConfigProvider.getConfig("Platform").equals("Tab")) {

				String Searchtext = "Bill Gates";

				WrapperMethods.sendkeys(HomePage.getHomeFooterSearchCNNBox(), Searchtext, "Entered the text ",
						"Not able to enter the text ");

				WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());
				HomePage.wait(5);
				WrapperMethods.waitForPageLoaded(driver);

				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".search-results_msg")));
				} catch (Exception e) {
					try {
						WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());
						wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".search-results_msg")));

					} catch (Exception E) {
						Reporter.log("Page did not load after click/Click operation failed... Please check");
					}
				}
				HomePageFunctions.testSearchSummaryDesktop();
				HomePageFunctions.testSearchResults();

				WrapperMethods.isDisplayed(HomePage.getSearchMobileSection(),
						"The section block is displayed in mobile search results",
						"The section block is not displayed in mobile search results");

				WrapperMethods.contains_Text_Attribute(HomePage.getsearchlaunchpage(), "value", Searchtext,
						"The Page has been launced with the derived results",
						"The page has not launced with the results expected"
								+ WrapperMethods.getWebElement(HomePage.getsearchlaunchpage()).getText());

				WrapperMethods.click(HomePage.getSearchMobileSection(), "Clicked the Search Mobile Section",
						"Not able to click the Search Mobile Section");
				WrapperMethods.isDisplayed(HomePage.getSearchMobileSectionExpand(), "The section block expanded",
						"The section block didnot expand");
				WrapperMethods.click(HomePage.getSearchMobileSection(), "Clicked the Search Mobile Section",
						"Not able to click the Search Mobile Section");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing the Home Page Search CNN Section:");
		}
	}

	public void testHomeVidLinks() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		WebElement vid = null;

		try {

			for (WebElement test : HomePage.getVidLinks()) {
				if (test.isDisplayed()) {
					vid = test;
					break;
				}
			}

			try {
				WrapperMethods.assertIsTrue(vid.isDisplayed(), "Video links are present",
						"Video links not present in home page");
			} catch (Exception E) {
				UMReporter.log(LogStatus.ERROR, "Video Element is not present in the page");
			}

			WrapperMethods.clickJavaScript(HomePage.gethomeVidLinks());

			WrapperMethods.waitForPageLoaded(driver);

			MethodDef.explicitWaitVisibility(HomePage.getVidLoad(), 20, "Video is Loaded", "Video is not Loaded");

			WrapperMethods.isDisplayed(HomePage.getVidLoad(), "Video page loaded", "Video page not loaded");

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing  Video element");
		}

	}

	public void testHomeGalLinks() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		WrapperMethods.preSteps();
		WebElement gal = null;

		try {

			for (WebElement test : HomePage.getIntlGalLinks()) {
				if (test.isDisplayed()) {
					gal = test;
					break;
				}
			}
			WrapperMethods.assertIsTrue(gal.isDisplayed(), "Gallery links are present",
					"Gallery links not present in home page");

			WrapperMethods.clickJavaScript(gal);
			HomePage.wait(5);
			WrapperMethods.isDisplayed(HomePage.getGalleryHeaderTitlestat(), "Gallery page loaded",
					"Gallery page not loaded");

		} catch (NullPointerException E) {
			UMReporter.log(LogStatus.WARNING, "Gallery links (Cards with Gallery link) are not present in home page");
		}

	}

	private boolean checkMobile(int windowWidth, WebDriver go) {
		if (windowWidth <= 800) {
			return true;
		} else {
			return false;
		}
	}

	public static void validatesearchfieldtext(String Searchtext) {
		WrapperMethods.contains_Text_Attribute(HomePage.getSearchField(), "placeholder", Searchtext,
				"The Search field has the default value: " + Searchtext,
				"The search Field does not contain the appropriate default text:"
						+ WrapperMethods.getWebElement(HomePage.getSearchField()).getAttribute("placeholder"));
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterSearch(), "placeholder", Searchtext,
				"The Bottom Search field has the default value: " + Searchtext,
				"The Bottom search Field does not contain the appropriate default text:"
						+ WrapperMethods.getWebElement(HomePage.getFooterSearch()).getAttribute("placeholder"));
	}

	public void testHomePageHeaderSearch(String defaulttext) {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			boolean ismob = checkMobile(Integer.parseInt(ConfigProvider.getConfig("Res_Width")), driver);
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				UMReporter.log(LogStatus.INFO, "<BR>====Home page Search Test Starts====");

				String Searchtext = "Ronald Trump";

				try {
					WrapperMethods.click(HomePage.getHeaderSearch(), "HeaderSearch is clicked",
							"HeaderSearch is not clicked");
				} catch (Exception e) {
					WrapperMethods.clickJavaScript(HomePage.getHeaderSearch(), "HeaderSearch is clicked",
							"HeaderSearch is not clicked");
				}
				WrapperMethods.isDisplayed(HomePage.getSearchField(), "The search field is expanded",
						"The search field is not expanded");

				validatesearchfieldtext(defaulttext);

				WrapperMethods.sendkeys(HomePage.getSearchField(), Searchtext, "Entered the text -",
						"Not able to enter the text - ");
				WrapperMethods.clickJavaScript(HomePage.getHomeHeaderSearchButton(),
						"Clicked the Search button in Home header",
						"Not able to click the Search button in Home header");
				UMReporter.log(LogStatus.INFO, "====Home page Search Text button is clicked====");
				Thread.sleep(2000);

				MethodDef.explicitWaitVisibility(HomePage.getcleartext(), "Clear Text is visible",
						"Clear Text is not visible");
				HomePageFunctions.testSearchSummaryDesktop();
				HomePageFunctions.testSearchResults();

				WrapperMethods.contains_Text_Attribute(HomePage.getsearchlaunchpage(), "value", Searchtext,
						"The Page has been launced with the derived results",
						"The page has not launced with the results expected"
								+ WrapperMethods.getWebElement(HomePage.getsearchlaunchpage()).getText());

				UMReporter.log(LogStatus.INFO, "====Radio button validation====");
				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.domesticsearchradio_btn(HomePage.getradiotext());
				} else {
					WrapperMethods.INTLsearchradio_btn(HomePage.getradiotext());
				}
			} else {
				UMReporter.log(LogStatus.INFO, "<BR>====Home page Search Test Strts====");
				WrapperMethods.click(HomePage.getNavMenu(), "Clicked the Navigation menu",
						"Not able to click the Navigation menu");

				validatesearchfieldtext(defaulttext);

				WrapperMethods.sendKeys(HomePage.getSearchField(), Keys.BACK_SPACE);
				WrapperMethods.sendkeysTextEnter(HomePage.getSearchField(), "Ronald Trump");
				UMReporter.log(LogStatus.PASS, "<br>PASSED: Search key is passed</br>");
				WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
				Thread.sleep(5000);
				HomePageFunctions.testSearchSummaryDesktop();
				HomePageFunctions.testSearchResults();

				WrapperMethods.contains_Text_Attribute(HomePage.getsearchlaunchpage(), "value", "Ronald Trump",
						"The Page has been launced with the derived results: "
								+ WrapperMethods.getWebElement(HomePage.getsearchlaunchpage()).getAttribute("value"),
						"The page has not launced with the results expected"
								+ WrapperMethods.getWebElement(HomePage.getsearchlaunchpage()).getAttribute("value"));
				WrapperMethods.click(HomePage.getSearchMobileSection(), "Clicked the SearchMobile section",
						"Not able to click the SearchMobile section");
				WrapperMethods.isDisplayed(HomePage.getSearchMobileSectionExpand(), "The section block expanded",
						"The section block didnot expand");
				WrapperMethods.click(HomePage.getSearchMobileSection(), "Clicked the SearchMobile section",
						"Not able to click the SearchMobile section");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing the Home Page Search CNN Section");
		}
	}

	public void ValidatetestEmVinePlayer() throws IOException {
		if (System.getProperty("environment").equals("SWEET")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.sweetVimeoPlayer);
		} else if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.RefVimeoPlayer);
		}

		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		HomePageFunctions.testEmVinePlayer();
	}

	public void testVRheaderfont() throws IOException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.VR);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		ArticlePageFunctions.testVRheaderfont();
	}

	public void ValidateVimeoPlayer() throws IOException {
		if (System.getProperty("environment").equals("SWEET")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.sweetVimeoPlayer);
		} else if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.RefVimeoPlayer);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		HomePageFunctions.testEmVimeoPlayer();
	}

	public void SocialShare() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.socialShare);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();

		WebDriverWait wait = new WebDriverWait(driver, 15);
		String brow = ConfigProvider.getConfig("Browser");

		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.cssSelector(".m-share__rail-top .js-gigya-sharebar.gigya-sharebar")));
		UMReporter.log(LogStatus.PASS, "<br>PASSED: Gigya Share bar is present</br>");
		try {
			UMReporter.log(LogStatus.INFO, "<BR>======The SocialShare facebook Page validation starts =====<BR>");

			ArticlePageFunctions.SocialShareValidation(brow);

			UMReporter.log(LogStatus.INFO, "<BR>======The SocialShare facebook Page validation ends=====<BR>");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing in SocialShare facebook Page");
		}
	}

	public void testFeedBackForm() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedback);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.validateFeedbackForm();
	}

	public void testFeedBackFormSubmitMandate() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedback);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackFormMand();
	}

	public void testFeedBackFormSubmit() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedback);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackForm();
	}

	public void testFeedBackFormEmail() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedback);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackEmail();
	}

	public void testFeedBackFormSubmit360() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackac360);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.feedbackForm360Details();
		CommomPageFunctions.submitFeedbackForm360();
	}

	public void testFeedBackFormMandate360() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackac360);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackFormMand360();
	}

	public void testFeedBackFormEmail360() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackac360);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackEmail360();
	}

	public void testLiveFyreUI() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();

		ArticlePage.wait(10);
		try {
			WrapperMethods.scrollIntoViewByElement(By.cssSelector(".s-element-content h2"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreHeading(driver));

			ArticlePageFunctions.validateLivefyreHeading();
			ArticlePageFunctions.validateLivefyreParagraph();

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing Livefyre");
		}

	}

	public void testLiveFyreItalicUI() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]//h2/i"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreItalicHeading(driver));
			ArticlePageFunctions.validateLivefyreItalicHeading();
			ArticlePageFunctions.validateLivefyreItalicParagraph();

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing Livefyre : Italic Text");
		}
	}

	public void testLiveFyreOrderedListUI() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]/ol/li"));

			WrapperMethods.moveToElement(ArticlePage.getLiveFyreOrderedList(driver));
			ArticlePageFunctions.validateLiveBlogOrderedList();

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing Livefyre : Ordered List");
		}
	}

	public void testWeatherMap() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			WeatherPageFunctions.testWeatherMaps();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Weather Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void weatherpageZipcodeValidation() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WeatherPageFunctions.weatherpageZipcodeValidation();

	}

	public void testFeedBackFormMandateStud() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackstudent);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackFormMandStud();

	}

	public void testFeedBackFormSubmitStud() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackstudent);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.feedbackFormStudDetails();
		CommomPageFunctions.submitFeedbackFormStud();

	}

	public void testFeedBackFormEmailStud() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedbackstudent);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.submitFeedbackEmailStud();
	}

	public void CNNFeedbackGo() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.feedback_go);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "======The FeedbackGo(CNN Go) Page validation starts =====");
			HomePageFunctions.CNNFeedbackGovalidation();
			UMReporter.log(LogStatus.INFO, "======The FeedbackGo(CNN Go) Page validation ends =====");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the FeedbackGo(CNN Go) Page");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testLiveFyreUnOrderedListUI() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]/ul/li"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreUnOrderedList(driver));
			ArticlePageFunctions.validateLiveBlogUnOrderedList();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Livefyre : UnOrdered List");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public void testLiveFyreStrikedTextUI() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]//h2/strike"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreStrikedHeading(driver));
			ArticlePageFunctions.validateLivefyreStrikedHeading();
			ArticlePageFunctions.validateLivefyreStrikedParagraph();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Livefyre : Striked Text");
		}
	}

	public void testLiveFyreBoldTextUI() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]//h2/b"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreBoldHeading(driver));
			ArticlePageFunctions.validateLivefyreBoldHeading();
			ArticlePageFunctions.validateLivefyreBoldParagraph();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Livefyre : bold Text");
		}
	}

	public void testLiveFyreQuotedTextUI() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]/blockquote"));
			WrapperMethods.moveToElement(ArticlePage.getLiveFyreQuotedText(driver));
			ArticlePageFunctions.validateLiveBlogQuotedText();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Livefyre : QuotedText");
		}
	}

	public void LiveFyrePlainCaption() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "<b>======The Live Blog Plain Caption Validation test starts=====</b>");
			ArticlePageFunctions.LiveFyrePlainCaption();
			UMReporter.log(LogStatus.INFO, "<b>======The Live Blog Plain Caption Validation test Ends=====</b>");

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR, "Error in accessing Live Blog Plain Caption validation");
		}

	}

	public void LiveFyreHeadings() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAuthorURL_Ref);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "<b>======The Live Blog Headings Validation test starts=====</b>");
			ArticlePageFunctions.LiveFyreHeadings();
			UMReporter.log(LogStatus.INFO, "<b>======The Live Blog Headings Validation test Ends=====<b>");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Live Blog Headings validation");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public void testTVShowValidations() {
		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.tvshow);
		} else if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.tvallshows);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			if (System.getProperty("edition").equals("DOM")) {
				UMReporter.log(LogStatus.INFO, "<b>Info: Shows  :DOM </b>");

				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(TvPages.getTVShowsTitle()).isDisplayed(),
						"the title is displayed:  " + WrapperMethods.getWebElement(TvPages.getTVShowsTitle()).getText(),
						"the title is not displayed");

				TvPagesFunctions.showsValidations();
			} else {
				UMReporter.log(LogStatus.INFO, "<b>Info: Shows  :INTL </b>");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(TvPages.getTVShowsINTLTitle()).isDisplayed(),
						"the title is displayed:  "
								+ WrapperMethods.getWebElement(TvPages.getTVShowsINTLTitle()).getText(),
						"the title is not displayed");
				TvPagesFunctions.showsValidations();
				for (WebElement elem : TvPages.getShowDescription()) {
					WrapperMethods.assertIsTrue(elem.isDisplayed(),
							"the show description is displayed and the description is :  " + elem.getText(),
							"the show description is not displayed");
				}
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Shows Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testShowLeafValidations() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.shows_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			Reporter.log("<br>INFO: Show leafPage Elements</br>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(TvPages.getShowLogo()).isDisplayed(),
					"the show logo is displayed ", "the show logo is not displayed ");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(TvPages.getShowFbIcon()).isDisplayed(),
					"the facebook social icon is displayed ", "the facebook social icon is not displayed ");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(TvPages.getShowFbIconHref()).getAttribute("href").contains("facebook"),
					"the facebook social icon has href and the href is :  "
							+ WrapperMethods.getWebElement(TvPages.getShowFbIconHref()).getAttribute("href"),
					"the facebook social icon doesnot have the href ");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(TvPages.getShowTwIcon()).isDisplayed(),
					"the twitter social icon is displayed ", "the twitter social icon is not displayed ");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(TvPages.getShowTwIconHref()).getAttribute("href").contains("twitter"),
					"the twitter social icon has href and the href is :  "
							+ WrapperMethods.getWebElement(TvPages.getShowTwIconHref()).getAttribute("href"),
					"the twitter social icon doesnot have the href ");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the show leaf elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testFeedCard() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.SECTION_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			SectionFrontsPageFunctions.basicFeedCardValidations();
			SectionFrontsPageFunctions.FeedcardElements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Feed card Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleFullWidthGallery() {
		{
			if (System.getProperty("environment").equals("REF")) {
				WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmFullGal_Ref);
			} else {
				WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmFullGal);
			}
			WrapperMethods.checkallpageload();
			WebDriver driver = DriverFactory.getCurrentDriver();
			WrapperMethods.waitForPageLoaded(driver);
			driver.manage().window().maximize();
			WrapperMethods.preSteps();
			GalleryPageFunctions.testheader();
			GalleryPageFunctions.CheckFullwodthGAlleryImageSize();

			try {
				GalleryPageFunctions.clickGalleryPrev();
				GalleryPageFunctions.clickGalleryNext();
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error accessing full Width gallery elements");
				UMReporter.log(LogStatus.INFO, E.getMessage());
			}
			try {
				int windowWidth = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
				boolean ismob = checkMobile(windowWidth, DriverFactory.getCurrentDriver());
				if (!ismob) {
					GalleryPageFunctions.galleryCaptionsOpenTest();
					GalleryPageFunctions.hideCaption();
					GalleryPageFunctions.showCaption();
				} else if (windowWidth > 599 && ConfigProvider.getConfig("Platform").equals("Desktop")) {
					GalleryPageFunctions.showCaption();
					GalleryPageFunctions.hideCaption();
				} else if (windowWidth > 599) {
					GalleryPageFunctions.hideCaption();
					GalleryPageFunctions.showCaption();
				} else {
					// if(!System.getProperty("deviceType").equals("Tab"))
					GalleryPageFunctions.showCaption();
					GalleryPageFunctions.hideCaption();
				}
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error in Accessing Gallery Elements");
				UMReporter.log(LogStatus.INFO, E.getMessage());
			}

		}

	}

	public void testContainerAndZoneElements() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.SECTION_URL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {

			SectionFrontsPageFunctions.testContainerElements();
			SectionFrontsPageFunctions.testZoneElements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Container Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void ArticleColRightRailPinVideo() throws ClientProtocolException, IOException {

		if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl_Ref);
		} else {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO,
					"<b>Info: Testing of Right Rail Pinned Video in Article Collection Page</b>");
			ArticlePage.wait(10);
			WebDriverWait waitvid = new WebDriverWait(DriverFactory.getCurrentDriver(), 60);
			waitvid.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					WebElement videoPlayer = null;

					try {
						videoPlayer = DriverFactory.getCurrentDriver().findElement(By.xpath("//video"));
					} catch (Exception e) {
						videoPlayer = DriverFactory.getCurrentDriver().findElement(By.xpath("//object"));
					}
					String enabled = videoPlayer.getAttribute("src");
					Reporter.log("<BR>" + enabled);
					if (enabled.contains("cnn") && !enabled.contains("blank.mp4")) {
						WrapperMethods.moveToElement(videoPlayer);
						MethodDef.passLog("CNN Video is now loaded");
						return true;
					} else
						return false;
				}
			});
			ArticlePage.wait(10);

			WrapperMethods.moveToElement(ArticlePage.getFooter());

			ArticlePage.wait(10);
			WrapperMethods.captureScreenshot();
		} catch (Exception E) {

			UMReporter.log(LogStatus.FAIL, "Error in accessing Right Rail Pinned Video element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void ArticleVideoautoplayed() {
		if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl_Ref);
		} else {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();

		WebDriverWait wait = new WebDriverWait(driver, 20);

		try {
			try {
				wait.until(ExpectedConditions
						.visibilityOf(WrapperMethods.getWebElement(ArticlePage.Nowplayingtheoplayer())));
			} catch (Exception e) {
				MethodDef.failLog("The Article Page Top Collection Video is not auto played - Please check",
						"The Article Page Top Collection Video is not auto played - Please check");
			}
			WrapperMethods.isDisplayed(ArticlePage.getTheoplayerPlay(), "the video is playing",
					"the video is not playing");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing theo player !!!");
		}
	}

	public void testHeaderOpinionsSubSections_DOM() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("dom_header_opinions").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Opinion", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderOpinionsSubSections_INTL() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("intl_header_opinions").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Opinion", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderHealthSubSections() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("dom_header_health").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Health", EnvironmentHelper.getURL(), domData);
	}

	public void testHeaderVideosSubSections() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_videos").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_videos").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Video", EnvironmentHelper.getURL(), domData);

	}

	public void testHeaderINTLRegionsSubSections() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("ed_header_regions").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Regions", EnvironmentHelper.getURL(), domData);

	}

	public void testHeaderINTLSportSubSections() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;

		domData = ConfigProp.getPropertyValue("ed_header_sport").split(",");

		SectionFrontsPageFunctions.sectionsValidation("Sport", EnvironmentHelper.getURL(), domData);

	}

	public void testWeatherCardFnCity() {
		// Creating custom Page instances
		// Load Page
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (System.getProperty("edition").equals("DOM")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				MethodDef.explicitWaitVisibility(HomePage.getWeatherCard(), "Weather card is present in the page",
						"Weather card is not present in the page");
				HomePageFunctions.validateWeatherCardFn("Chennai", weatherType.city, "Chennai, India", false);
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Weather card Element is not present");
				// error.addError("Weather card Element is not present");
			}
		} else

			UMReporter.log(LogStatus.INFO, "Weather card validation skipped for home page & Devices");
	}

	public void testWeatherCardFnZip() {
		// Creating custom Page instances
		// Test: Weather Card widget functionality from home page
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (System.getProperty("edition").equalsIgnoreCase("DOM")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				MethodDef.explicitWaitVisibility(HomePage.getWeatherCard(), "Weather card is present in the page",
						"Weather card is not present in the page");
				HomePageFunctions.validateWeatherCardFn("10001", weatherType.zip, "New York, NY", false);
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Weather card Element is not present");
				// error.addError("Weather card Element is not present");
			}
		} else
			UMReporter.log(LogStatus.INFO, "Weather card validation skipped for home page & Devices");
	}

	public void testWeatherCardFnToggle() {
		// Creating custom Page instances
		// Test: Weather Card widget functionality from home page
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (System.getProperty("edition").equalsIgnoreCase("DOM")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				MethodDef.explicitWaitVisibility(HomePage.getWeatherCard(), "Weather card is present in the page",
						"Weather card is not present in the page");
				HomePageFunctions.validateWeatherCardFn("Chennai", weatherType.city, "Chennai, India", true);
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Weather card Element is not present");
				// error.addError("Weather card Element is not present");
			}
		} else
			UMReporter.log(LogStatus.INFO, "Weather card validation skipped for home page & Devices");
	}

	public void testWeatherCardRedirect(int windowWidth, int windowHeight, String url) {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (System.getProperty("edition").equalsIgnoreCase("DOM")
				&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				MethodDef.explicitWaitVisibility(HomePage.getWeatherCard(), "Weather card is present in the page",
						"Weather card is not present in the page");
				HomePageFunctions.validateWeatherCardRedirect();
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Weather card Element is not present");
				// error.addError("Weather card Element is not present");
			}
		} else
			UMReporter.log(LogStatus.INFO, "Weather card validation skipped for home page & Devices");
	}

	public void weatherPagePrefferedTemp() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")
					&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				// if (!ConfigProvider.getConfig("Platform").equals("Perfecto"))
				// {
				WeatherPageFunctions.TestWeatherPagepRefafterRefresh();

			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Accessing Weather Elements" + e.getMessage());

		}
	}

	public void weatherpageImageValidation() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		WeatherPageFunctions.weatherpageImageValidation();
	}

	public void weatherpageMakeDefaultValidation() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		WeatherPageFunctions.weatherpageMakeDefaultValidation();
	}

	public void testHomePageHeaderSearchInvalid() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testHomePageHeaderSearchInvalid();
	}

	public void testHomePageHeaderSearchBlank() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testHomePageHeaderSearchBlank();
	}

	public void weatherpageRecentLocationValidation() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.weatherpageRecentLocationValidation();
	}

	public void testHomePageFooterSearchInvalid() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testHomePageFooterSearchInvalid();
	}

	public void testHomeVidLinkss() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testHomeVidLinks();
	}

	public void testWeatherPageOtherValidations() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.WeatherPageOtherValidations();
	}

	public void testWeatherPageRecentLocation() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.weatherUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testWeatherPageRecentLocation();
	}

	public void launchpageeditionpicker() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		SectionFrontsPageFunctions.editionpickersymbolcheck();
	}

	public void interactivebourdain() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Interactivebourdain);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.explorePartsValidations();
	}

	public void testMarketCard() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.basicMarketCardValidations();

	}

	public boolean ChecZonesJSON(String Environent, String site, String text) {

		String Site = "";
		Boolean Test = false;
		if (site.equalsIgnoreCase("DOM")) {
			Site = "domestic";
		} else {
			Site = "international";
		}
		try {
			JSONObject test = JsonReader.readJsonFromUrl(
					"http://data.cnn.com/cfg/v1/" + Site + "/" + Environent.toLowerCase() + "/zones.json");
			String jsonText = test.toString();
			Test = JsonSimpleReader.parseCNNWZones(jsonText, text);
			Reporter.log("Sample stest" + Test);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Test;
	}

	public void ValidateApolloADthirdZone() throws IOException, InterruptedException {
		// url = MethodDef.correctUrl(url);
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testAdpresentthirdzone();
	}

	public void MetaContent() {
		// Load Page
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			HomePageFunctions.metaContentValidation();

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Meta Content validation in Home page");
		}
	}

	public void ValidateSoundCloudPlayer() throws IOException {
		if (System.getProperty("environment").equals("SWEET"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.sweetSoundCloudPlayer);
		else if (System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.RefSoundCloudPlayer);
		else if (System.getProperty("environment").equals("PROD"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ProdSoundCloudPlayer);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.testSoundCloudPlayer();
	}

	public void Subscriptonnewsletter() throws IOException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.Validatesubscriptionmodule();
	}

	public void Subscribebuttonvalidation() throws IOException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.Validatesubscribebutton();
	}

	public void SubscribtionFBShare() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.CFacebooksharevalidation();
	}

	public void SubscriptionTwitterShare() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.CTwittersharevalidation();
	}

	public void SubscriptionMoreshare() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.Cmorevalidation();
	}

	public void SubscriptionNav() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.SubscriptonNav();
	}

	public void testArticleEmImageFullWidth() {
		if (!System.getProperty("environment").equalsIgnoreCase("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmFullImg);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmImage_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with Article with Standard Gallery Elements");
			ArticlePageFunctions.clickReadMore();
			WrapperMethods.verifyElement(ArticlePage.getImgFullWidth(), "Full Width Image");
			WrapperMethods.assertIsTrue(!ArticlePage.getImgFullWidthImg().getAttribute("src").isEmpty(),
					"Image Ref is present", "Image Ref is no present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the embedded element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleStandardImg() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmStdImg);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmStdImg_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with Article with Standard Gallery Elements");
			ArticlePageFunctions.clickReadMore();
			WrapperMethods.verifyElement(ArticlePage.getStandardImg(), "Standard Image");
			UMReporter.log(LogStatus.INFO,
					"The Photo height is: " + ArticlePage.getStandardImgSize().getCssValue("height"));
			UMReporter.log(LogStatus.INFO,
					"The Photo width is: " + ArticlePage.getStandardImgSize().getCssValue("width"));
			WrapperMethods.assertIsTrue(
					ArticlePage.getStandardImgSize().getCssValue("height").contains("169")
							&& ArticlePage.getStandardImgSize().getCssValue("width").equalsIgnoreCase("300px"),
					"The Photo Width and Height are as expected", "The Photo Width and Height are not as expected");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the embedded element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleEmTw() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmTw);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmTw_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with 3rd party elements");
			ArticlePageFunctions.clickReadMore();
			MethodDef.explicitWaitVisibility(ArticlePage.getTwWidgetFrame(), "Wait untill Twitter widget is present",
					"Twitter widget is not present");
			WrapperMethods.verifyElement(ArticlePage.getTwWidgetFrame(), "The Twitter widget");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article 3rd party Twitter elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleEmTwFollow() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmTwFollow);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmTw_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with 3rd party elements");
			ArticlePageFunctions.clickReadMore();
			WrapperMethods.verifyElement(ArticlePage.getTwFollowWidget(), "The Twitter follow widget");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article 3rd party Twitter elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void ArticleEmbVideoPage() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmFullVid);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmFullVid_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		ArticlePageFunctions.clickReadMore();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Embedded Video (Full Width) Page");
			ArticlePageFunctions.testArticleEmbFullVideo();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article Embedded Video (Full Width) Page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleLeafPageAds() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTImage);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTImage_Ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		// TEST: Section Ads
		try {
			// ArticlePageFunctions.adElements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.WARNING, "Error in accessing the Article Leaf Page Ad Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageEmbedInstagram() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedinstagram);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedinstagram_ref);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article With Embed Instagram Page");
			ArticlePageFunctions.validateEmbedInstagram();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article Page with Embed Instagram");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageInstagram() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleInstImage);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Page Instagram Image");
			ArticlePageFunctions.clickReadMore();
			WrapperMethods.verifyElement(ArticlePage.getInstagramImg(), "Instagram Image");
			MethodDef.brokenLinkValidation(ArticlePage.getInstagramImg().getAttribute("src"));
			WrapperMethods.switchToFrame("instagram-embed-0");
			WrapperMethods.verifyElement(ArticlePage.getInstagramFollow_btn(), "Instagram Follow Button");
			WrapperMethods.verifyElement(ArticlePage.getInstagramLogo(), "Instagram Logo");
			WrapperMethods.assertIsTrue(!ArticlePage.getInstagramLogo().getAttribute("href").isEmpty(),
					"Instagram Logo href is Present", "Instagram Logo href is not Present");
			MethodDef.brokenLinkValidation(ArticlePage.getInstagramLogo().getAttribute("href"));
			/*
			 * WrapperDef.assertIsDisplayed(error, article.getInstagram_Likes(),
			 * "Instagram Likes Button is Present",
			 * "Instagram Likes Button is not Present");
			 * 
			 * WrapperDef.assertIsDisplayed(error,
			 * article.getInstagram_Comments(),
			 * "Instagram Comments Section is Present",
			 * "Instagram Comments Section is not Present - Please check");
			 */
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article Page Instagram Image");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageSocialIcons() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Page Social Icons");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Facebook Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Twitter Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Share With Your Friends Icon");
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				UMReporter.log(LogStatus.INFO, "Testing Article Page Social Icons after scroll down");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski2(), "Facebook Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski3(), "Twitter Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski4(), "Share With Your Friends Icon");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article Page Social Icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleCollectionsPage() {
		if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl_Ref);
		} else {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();

			UMReporter.log(LogStatus.INFO, "<b>Info: Testing Article Page Header</b>");
			ArticlePageFunctions.testHeaderColl();
			ArticlePageFunctions.testArticleAuthor();
			ArticlePageFunctions.basicArticleVideoElemValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Top Collections Page Header element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleCollectionPlayerPrevNav() {

		if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl_Ref);
		} else {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
		try {
			WrapperMethods.clickJavaScript(ArticlePage.getArticleVideoCollNextCarousel());
			ArticlePageFunctions.basicArticleVideoElemValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Top Collections Page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleCollectionPlayerNextNav() {

		if (System.getProperty("environment").equals("REF")) {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl_Ref);
		} else {
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTColl);
		}
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
		try {
			WrapperMethods.clickJavaScript(ArticlePage.getArticleVideoCollPrevCarousel());
			ArticlePageFunctions.basicArticleVideoElemValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Top Collections element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void base(String url) {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
	}

	public void testStyleLogoNavigation() {
		base(Prefs.Style_URL);
		try {
			DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
		} catch (Exception e) {
		}
		try {
			UMReporter.log(LogStatus.INFO, "<b>INFO:Style Logo Navigation</b>");

			SectionFrontsPageFunctions.testStyleLogoNavigation();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Style logo  Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void StylePageFooter() {
		base(Prefs.Style_URL);
		try {
			DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing Style Page Footer Section</b>");

			SectionFrontsPageFunctions.footerTestingXpath(SectionFrontsPage.getStyleFooterLastLineLinks());
			SectionFrontsPageFunctions.footerTestingXpath(SectionFrontsPage.getStyleFooterTurnBroadCastLink());
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getPoliticsStyleFooterCopyright()).getText()
							.contains("CNN Sans � &"),
					"'CNN Sans � &' is added in Footer Copyright Text: " + WrapperMethods
							.getWebElement(SectionFrontsPage.getPoliticsStyleFooterCopyright()).getText(),
					"'CNN Sans � &' is not added in Footer Copyright Text: " + WrapperMethods
							.getWebElement(SectionFrontsPage.getPoliticsStyleFooterCopyright()).getText());

			SectionFrontsPageFunctions.testStyleFooterSocialElements();

			SectionFrontsPageFunctions.footerTestingXpath(SectionFrontsPage.getStyleFooterAboutLink());
			SectionFrontsPageFunctions.testStyleFooterTopLinkElements();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Style Page Footer Section");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testStyleGalleryValidations() {
		base(Prefs.Style_URL);
		try {
			DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
		} catch (Exception e) {
		}
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Gallery Elements</b>");
			StylePageFunctions.testStyleGalleryElements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Style Gallery Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testStyleGalleryPrevAndNext() {
		base(Prefs.Style_URL);
		try {
			DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
		} catch (Exception e) {

		}
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Gallery Prev</b>");
			WrapperMethods.clickJavaScript(SectionFrontsPage.getStylePageGalleryPrev());
			SectionFrontsPage.wait(5);
			SectionFrontsPageFunctions.StyleOuterGallery();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Style Gallery Prev Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Gallery Next</b>");
			WrapperMethods.clickJavaScript(SectionFrontsPage.getStylePageGalleryNext());
			SectionFrontsPage.wait(5);
			SectionFrontsPageFunctions.StyleOuterGallery();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Style Gallery Next Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void GalleryStyleOutbrain() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);

		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			StylePageFunctions.validateGalleryOutbrain();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Style Gallery Outbrain Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void videoOutbrainAD() {
		base(Prefs.videoLanding);

		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			LeafPageFunctions.validateOutbrainAD(DriverFactory.getCurrentDriver());
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}

	}

	public void videoZone3Validations() {
		base(Prefs.videoLanding);

		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			LeafPageFunctions.validateZone3(DriverFactory.getCurrentDriver());
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}
	}

	public void videoZone5Validations() {
		base(Prefs.videoLanding);

		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			LeafPageFunctions.validateZone5(DriverFactory.getCurrentDriver());
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}
	}

	public void videoZone3INTLValidations() {
		base(Prefs.videoLanding);
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			LeafPageFunctions.validateZone3INTL();
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}

	}

	public void videoZone5INTLValidations() {
		base(Prefs.videoLanding);
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			LeafPageFunctions.validateINTLVideoZone5();
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}

	}

	public void testArticleComments() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.testArticleComments();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article comments Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleCommentsLoginWithFb() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			WrapperMethods.moveToElement(ArticlePage.getLoginBar());
			WrapperMethods.click(ArticlePage.getLoggedOut(), "Logout Button");
			ArticlePage.wait(15);
		} catch (Exception e) {
		}
		WrapperMethods.verifyElement(ArticlePage.getCommentSignIn(), "The SignIN");
		WrapperMethods.clickJavaScript(ArticlePage.getCommentSignIn());
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-gigya-widget")));
		try {
			int count = 0;
			String wParentHandle = driver.getWindowHandle();
			UMReporter.log(LogStatus.INFO, "<b><i>Facebook Login Testing on My CNN Page</b></i>");
			WrapperMethods.clickJavaScript(ArticlePage.getMycnnFbLogin());
			UMReporter.log(LogStatus.INFO, "Facebook is clicked");
			ArticlePage.wait(15);
			WrapperMethods.waitForPageLoaded(driver);
			try {
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
					Reporter.log(driver.getTitle());
					if (driver.getTitle().toString().contains("Facebook")) {
						Reporter.log("<BR> Inside window" + driver.getTitle());
						WrapperMethods.sendKeys(ArticlePage.getMycnnFbEmail(), "turnercnn1@gmail.com");
						UMReporter.log(LogStatus.INFO, "username is passed");
						WrapperMethods.sendKeys(ArticlePage.getMycnnFbPass(), "fbookcnn1");
						UMReporter.log(LogStatus.INFO, "password is passed");
						if (ConfigProvider.getConfig("Platform").equals("Desktop"))
							WrapperMethods.click(ArticlePage.getMycnnFbLoginButton(), "FB Login Button");
						else
							WrapperMethods.click(ArticlePage.getMycnnFbLoginButtonDev(), "FB Login Button");
						driver.switchTo().window(wParentHandle);
					}
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "User is already logged in Facebook");
			}
		} catch (Exception e) {
		}
		ArticlePage.wait(20);
		ArticlePageFunctions.postArticleComments();
	}

	public void testArticleCommentsLoginWithTwitter() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			WrapperMethods.moveToElement(ArticlePage.getLoginBar());
			WrapperMethods.click(ArticlePage.getLoggedOut(), "Logout Button");
			ArticlePage.wait(15);
		} catch (Exception e) {
		}
		WrapperMethods.verifyElement(ArticlePage.getCommentSignIn(), "The SignIN");
		WrapperMethods.clickJavaScript(ArticlePage.getCommentSignIn());
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-gigya-widget")));

		try {
			int count = 0;
			String wParentHandle = driver.getWindowHandle();
			UMReporter.log(LogStatus.INFO, "<b><i><em>Twitter Login Testing on My CNN Page</b></i></em>");
			WrapperMethods.clickJavaScript(ArticlePage.getMycnnTwitterLogin());
			ArticlePage.wait(15);
			WrapperMethods.waitForPageLoaded(driver);
			try {
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
					Reporter.log(driver.getTitle());
					if (driver.getTitle().toString().contains("Twitter")) {
						Reporter.log("<BR> Inside window" + driver.getTitle());
						WrapperMethods.sendKeysJavaScript(ArticlePage.getMycnnTwitterUser(), "turnercnn1@gmail.com");
						WrapperMethods.sendKeysJavaScript(ArticlePage.getMycnnTwitterPass(), "tweetcnn1");
						WrapperMethods.click(ArticlePage.getMycnnTwitterLogIn(), "Twitter Login Button");
						MethodDef.passLog("Twitter Logged in");
						driver.switchTo().window(wParentHandle);
						Thread.sleep(20000);
					}
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "User is already logged in Twitter");
			}
		} catch (Exception e) {
		}
		ArticlePage.wait(20);
		ArticlePageFunctions.postArticleComments();
	}

	public void testArticleCommentsLoginWithGoogle() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			WrapperMethods.moveToElement(ArticlePage.getLoginBar());
			WrapperMethods.click(ArticlePage.getLoggedOut(), "Logout Button");
			ArticlePage.wait(15);
		} catch (Exception e) {
		}
		WrapperMethods.verifyElement(ArticlePage.getCommentSignIn(), "The SignIN");
		WrapperMethods.clickJavaScript(ArticlePage.getCommentSignIn());
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-gigya-widget")));
		try {
			int count = 0;
			String wParentHandle = driver.getWindowHandle();
			UMReporter.log(LogStatus.INFO, "<b><i><em>Google+ Login Testing on My CNN Page</b></i></em>");
			// Logging into Google+
			WrapperMethods.clickJavaScript(ArticlePage.getMycnnGoogleLogin());
			ArticlePage.wait(15);
			WrapperMethods.waitForPageLoaded(driver);
			try {
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
					Reporter.log(driver.getTitle());
					if (driver.getTitle().toString().contains("Google")) {
						Reporter.log("<BR> Inside window" + driver.getTitle());
						WrapperMethods.sendKeys(ArticlePage.getMycnnGoogleEmail(), "turnercnn1@gmail.com");
						WrapperMethods.click(ArticlePage.getMycnnGoogleNext(), "Google Next Button");
						WrapperMethods.sendKeys(ArticlePage.getMycnnGooglePass(), "cnnmail1");
						WrapperMethods.click(ArticlePage.getMycnnGoogleSignin(), "Google Sign in Button");
						driver.switchTo().window(wParentHandle);
						Thread.sleep(20000);
					}
				}
			}

			catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "User is already logged in Google");
			}
		} catch (Exception e) {
		}
		ArticlePage.wait(20);
		ArticlePageFunctions.postArticleComments();
	}

	public void testArticleCommentsLoginWithLinkedIn() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			WrapperMethods.moveToElement(ArticlePage.getLoginBar());
			WrapperMethods.click(ArticlePage.getLoggedOut(), "Logout Button");
			ArticlePage.wait(15);
		} catch (Exception e) {
		}
		WrapperMethods.verifyElement(ArticlePage.getCommentSignIn(), "The SignIN");
		WrapperMethods.clickJavaScript(ArticlePage.getCommentSignIn());
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-gigya-widget")));
		try {
			int count = 0;
			String wParentHandle = driver.getWindowHandle();
			UMReporter.log(LogStatus.INFO, "<b><i><em>LinkedIn Login Testing on My CNN Page</b></i></em>");
			WrapperMethods.clickJavaScript(ArticlePage.getMycnnLinkLogin());
			ArticlePage.wait(15);
			WrapperMethods.waitForPageLoaded(driver);
			try {
				for (String handle : driver.getWindowHandles()) {
					driver.switchTo().window(handle);
					Reporter.log(driver.getTitle());
					if (driver.getTitle().toString().contains("LinkedIn")) {
						Reporter.log("<BR> Inside window" + driver.getTitle());
						Reporter.log("<BR> Inside window" + driver.getTitle());
						WrapperMethods.sendKeys(ArticlePage.getMycnnLinkEmail(), "turnercnn1@gmail.com");
						WrapperMethods.sendKeys(ArticlePage.getMycnnLinkPass(), "linkcnn1");
						WrapperMethods.click(ArticlePage.getMycnnLinkSignIn(), "LinkedIn Signin");
						driver.switchTo().window(wParentHandle);
						Thread.sleep(20000);
					}
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "User is already logged in Linked In");
			}
		} catch (Exception e) {
		}
		ArticlePage.wait(20);
		ArticlePageFunctions.postArticleComments();
	}

	public void testArticleEmFb() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmFb);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmFb_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with 3rd party elements");
			MethodDef.explicitWaitVisibility(ArticlePage.getFbWidget(), "Wait untill FB Widget is loaded",
					"FB Widget is not loaded");
			WrapperMethods.verifyElement(ArticlePage.getFbWidget(), "The Facebook widget");
			WrapperMethods.assertIsTrue(ArticlePage.getFbWidget().getAttribute("src").contains("facebook"),
					"the facebook starts with " + ArticlePage.getFbWidget().getAttribute("src"),
					"the facebook will not starts with");
			MethodDef.brokenLinkValidation(ArticlePage.getFbWidget().getAttribute("src"));
			WrapperMethods.verifyElement(ArticlePage.getGigyaBar(), "Gigyabar");
			// WrapperMethods.verifyElement(ArticlePage.getGigyaBarMessenger(),
			// "Gigyabar Messenger Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Gigyabar Facebook icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Gigyabar Twitter icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Gigyabar Share icon");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article 3rd party Facebook elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleExpandableImg() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmExpImg);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmExpImg_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			int windowWidth = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
			boolean ismob = checkMobile(windowWidth, DriverFactory.getCurrentDriver());
			UMReporter.log(LogStatus.INFO, "Testing Article with Expand Gallery Elements");
			ArticlePageFunctions.clickReadMore();
			if (!ismob) {
				ArticlePageFunctions.testImgBeforeExpand();
				WrapperMethods.verifyElement(ArticlePage.getExpandImg(), "Expandable Image");

				/*
				 * WrapperMethods.clickJavaScript(ArticlePage.getExpandImg(
				 * driver), driver);
				 */
				// WrapperMethods.click(ArticlePage.getExpandImg(driver));
				WrapperMethods.clickJavaScript(ArticlePage.getExpandImg());
				ArticlePage.wait(5);
				ArticlePageFunctions.testImgAfterExpand();
				WrapperMethods.clickJavaScript(ArticlePage.getExpandClose());
				ArticlePage.wait(5);
				WrapperMethods.assertIsTrue(
						ArticlePage.getImgWidthAfterClose().getAttribute("style").contains("width: 300px;"),
						"The width is 300px:",
						"The width is not 300px: " + ArticlePage.getImgWidthAfterClose().getAttribute("style"));
			} else {
				WrapperMethods.verifyElement(ArticlePage.getBeforeExpandImgWidth(), "Expandable Image");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing emb element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageTopImage() {

		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTImage);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePTImage_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page with page top image");
			ArticlePageFunctions.testheader();
			ArticlePageFunctions.testArticleStaticPage();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with page top image element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleEmYoutube() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmYoutube);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmYoutube_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with 3rd party elements");
			ArticlePageFunctions.clickReadMore();
			// WebElement iframe =
			// driver.findElement(By.cssSelector("iframe[title='fb:post
			// Facebook Social Plugin'"));
			// driver.switchTo().frame(iframe);
			WrapperMethods.verifyElement(ArticlePage.getYoutubeWidget(), "The Youtube widget");
			WrapperMethods.contains_Text_Attribute(ArticlePage.getYoutubeWidget(), "src", "youtube",
					"The youtube starts with ", "The youtube will not starts with");
			String Url = driver.findElement(ArticlePage.getYoutubeWidget()).getAttribute("src");
			MethodDef.brokenLinkValidation(Url);
			int windowWidth = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
			if (windowWidth > 599) {
				// WrapperMethods.click(ArticlePage.getEmVideoElementPlayElemOB(driver));
				WrapperMethods.clickJavaScript(ArticlePage.getYoutubeWidget());
				ArticlePage.wait(10);
			}
			WrapperMethods.switchToFrame(By.cssSelector(".el-embed-youtube__content"));
			try {
				WebElement videoPlayer = MethodDef.getvideoObject();
				MethodDef.YoutubePlayervalidation(videoPlayer);
			} catch (Exception E) {
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article 3rd party Youtube elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testadLockLong() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAdLockLong);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - Long Article Ad Locking");
			ArticlePageFunctions.outbrainFirstSectionAdVerify();
			ArticlePageFunctions.outbrainSecondSectionAdVerify();
			ArticlePageFunctions.outbrainThirdZoneAdVerify();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Long Article Ad elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleLeafOutbrain() throws ClientProtocolException, IOException {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmStdImg);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmStdImg_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with Article Leaf Page Outbrain Elements");
			int windowWidth = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
			if (windowWidth > 1020)
				ArticlePageFunctions.testOutbrain();
			else
				ArticlePageFunctions.testOutbrainTab();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page Outbrain Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleLeafShortAdLock() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAdLockShort);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - Short Article Ad Locking");
			ArticlePageFunctions.outbrainFirstSectionAdVerify();
			ArticlePageFunctions.outbrainThirdZoneAdVerify();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page Short Article Ad elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageEmbedImages() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedimages);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Article With Multiple Embed Images");
			ArticlePageFunctions.validateMultipleEmbedImages();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with Multiple Embed Images");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testadLockLongWithComment() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAdLockLongWithComment);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - Long Article Ad Locking");
			ArticlePageFunctions.outbrainFirstSectionAdVerify();
			ArticlePageFunctions.outbrainSecondSectionAdVerify();
			ArticlePageFunctions.outbrainThirdZoneAdVerify();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Long Article Ad elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageEmbedShow() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedshow);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - With Embed show");
			ArticlePageFunctions.validateEmbedShowElements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with Embed Show Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageEmbedSpecial() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedspecial);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - With Embed Special");
			ArticlePageFunctions.validateEmbedSpecial();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with Embed Special Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageEmbedWebTag() {
		if (System.getProperty("environment").equals("STAGE") || System.getProperty("environment").equals("PROD"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedwebtag_prod);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_embedwebtag);

		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Leaf Page - With Embed WebTag");
			ArticlePageFunctions.validateEmbedWebTag();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with Embed WebTag Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageFontStyles() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_content);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.contentFontstyles();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Page with Embed Font Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testVideoCollValidationsPageWidth() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ptcoll_pagewidth);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Embedded video collection");
			ArticlePageFunctions.clickReadMore();
			ArticlePageFunctions.basicEmVideoCollElemValidations();
			WrapperMethods.clickJavaScript(ArticlePage.getEmVideoCollElement());
			ArticlePage.wait(5);
			MethodDef.explicitWaitVisibility(ArticlePage.getEmVideoCollNowPlay(),
					"Wait untill Now Playing Text Appears", "Now Playing text is not loaded");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmVideoCollNowPlay().getText().isEmpty(),
					"Now Playing text is correct" + ArticlePage.getEmVideoCollNowPlay().getText(),
					"Now Playing text is not correct" + ArticlePage.getEmVideoCollNowPlay().getText());
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Embedded Video Collection Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticlePageTopImagePageWidth() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ptimage_pagewidth);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Video Page");
			ArticlePageFunctions.testheader();
			ArticlePageFunctions.testArticleStaticPage();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Video Page Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testAuthorCardValidations() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAuthorCard);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleAuthorCard_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.basicAuthorCardValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Author Card Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleWithEmbedFBTheme() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleWithEmbedFacebook);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleWithEmbedFacebook_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing background Theme in a Facebook embed Article Page");
			WrapperMethods.verifyElement(ArticlePage.getFBWidget(), "embedded facebook widget");
			try {
				WrapperMethods.verifyElement(ArticlePage.getLightBackgroundTheme(),
						"The embedded facebook in Light background theme");
			} catch (Exception e) {
				WrapperMethods.verifyElement(ArticlePage.getDarkBackgroundTheme(),
						"The embedded facebook in Dark background theme");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Facebook embed Article Page");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testEmVideoCollValidations() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Embedded video collection");
			ArticlePageFunctions.clickReadMore();
			ArticlePageFunctions.basicEmVideoCollElemValidations();
			if ((ConfigProvider.getConfig("Browser").equalsIgnoreCase("FIREFOX"))
					|| (ConfigProvider.getConfig("Browser").startsWith("IE"))) {
				WrapperMethods.moveToElement(ArticlePage.getEmVideoCollElement());
				WrapperMethods.scrollUp();
				WrapperMethods.click(ArticlePage.getEmVideoCollElement(), "Embedded video Collection");
			} else {
				WrapperMethods.click(ArticlePage.getEmVideoCollElement(), "Embedded video Collection");
			}
			ArticlePage.wait(10);
			MethodDef.explicitWaitVisibility(ArticlePage.getEmVideoCollNowPlay(), "Wait untill Now Playing appears",
					"Now Playing not loaded");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmVideoCollNowPlay().getText().isEmpty(),
					"Now Playing text is correct" + ArticlePage.getEmVideoCollNowPlay().getText(),
					"Now Playing text is not correct" + ArticlePage.getEmVideoCollNowPlay().getText());
			MethodDef.explicitWaitVisibility(ArticlePage.getcolectionVideoplayer(),
					"Wait untill collection Video Player appears", "collection Video Player not loaded");
			WrapperMethods.assertIsTrue(ArticlePage.getcolectionVideoplayer().isDisplayed(),
					"The video is expanded after click", "The Video is Not Expanded after click - Please check");
			WrapperMethods.assertIsTrue(
					ArticlePage.getcolectionVideoplayer().getCssValue("width").equalsIgnoreCase("768px")
							&& ArticlePage.getcolectionVideoplayer().getCssValue("height").equalsIgnoreCase("432px"),
					"The video is of correct dimension as expected",
					"The Video has in-correct dimension - Please check");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Embedded Video Collection Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void tesEmVideoCollPrevNav() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.clickReadMore();
			UMReporter.log(LogStatus.INFO, "Testing Embedded video collection : Prev");
			WrapperMethods.clickJavaScript(ArticlePage.getEmVideoCollCarouselPrev());
			ArticlePage.wait(5);
			ArticlePageFunctions.basicEmVideoCollElemValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Embedded Video Collection Elements when clicked prev");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void tesEmVideoCollNextNav() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleEmColl_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.clickReadMore();
			UMReporter.log(LogStatus.INFO, "Testing Embedded video collection : Next");
			WrapperMethods.clickJavaScript(ArticlePage.getEmVideoCollCarouselNext());
			ArticlePage.wait(5);
			ArticlePageFunctions.basicEmVideoCollElemValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Embedded Video Collection Elements when clicked Next");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testFactBoxValidations() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ArticleFactBoxURL);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.factboxpullquote_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			Reporter.log("<br>Info: Testing with Fact box");
			ArticlePageFunctions.clickReadMore();
			WrapperMethods.verifyElement(ArticlePage.getFactBoxTitle(), "Fact box title");
			WrapperMethods.verifyElement(ArticlePage.getFactBoxContent(), "Fact box content");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing factbox elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testSpecialPageSocialIcons() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.special_URL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			Reporter.log("<br>Info: Testing Special Page Social Icons");
			WrapperMethods.verifyElement(HomePage.getSpecialPageFB(), "Facebook Icon");
			WrapperMethods.verifyElement(HomePage.getSpecialPageTW(), "Twitter Icon");
			WrapperMethods.verifyElement(HomePage.getSpecialPageInst(), "Instagram Icon");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page Social Icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testSportArticleEmFb() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.sport_article);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing with 3rd party elements");
			MethodDef.explicitWaitVisibility(ArticlePage.getFbWidget(), "Wait untill FB Widget is loaded",
					"FB Widget is not loaded");
			WrapperMethods.verifyElement(ArticlePage.getFbWidget(), "The Facebook widget");
			WrapperMethods.assertIsTrue(ArticlePage.getFbWidget().getAttribute("src").contains("facebook"),
					"the facebook starts with " + ArticlePage.getFbWidget().getAttribute("src"),
					"the facebook will not starts with");
			MethodDef.brokenLinkValidation(ArticlePage.getFbWidget().getAttribute("src"));
			WrapperMethods.verifyElement(ArticlePage.getGigyaBar(), "Gigyabar");
			/*
			 * WrapperMethods.verifyElement(ArticlePage.getGigyaBarMessenger(),
			 * "Gigyabar Messenger Icon");
			 */
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Gigyabar Facebook icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Gigyabar Twitter icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Gigyabar Share icon");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article 3rd party Facebook elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testStormTracker() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.storm_tracker_url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.stormTrackerValidations();
	}

	public void testSpecialArticleMessenger() {
		String currentURL = TestDataProvider.getData(System.getProperty("environment"));
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.verifyElement(ArticlePage.getBreakingNews(), "Breaking News Banner");
			UMReporter.log(LogStatus.INFO, "Scroll down the Page untill share icons are visible on left side");
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(2);
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(2);
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(2);
			WrapperMethods.verifyElement(ArticlePage.Sharebar(), "Social Share Bar");
			// WrapperMethods.verifyElement(ArticlePage.getGigyaBarMessenger(),
			// "Messenger Icon");
			// MethodDef.isVisible(ArticlePage.getGigyaBarMessenger());
			MethodDef.screenshot();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Messenger validation of Special Article Page");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testArticleAutoPlayVideoPage() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleVidUrl);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleautoplay_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Article Video Page");
			ArticlePageFunctions.testheader();
			ArticlePageFunctions.testArticleAuthor();
			ArticlePageFunctions.testArticleAutoPlayVideo();
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				WrapperMethods.isDisplayed(ArticlePage.getVideoPlayedAuto(), "the video is playing",
						"the video is not playing");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article AutoPlay Video Page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testPullQuoteValidations() {
		if (!System.getProperty("environment").equals("REF"))
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articlePullQuote);
		else
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.factboxpullquote_Ref);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.clickReadMore();
			ArticlePageFunctions.basicPullQuoteValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Pull Quote Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void CNNIFBInstantartilemetadata() throws IOException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		URL homepage = new URL(EnvironmentHelper.getURL());
		URLConnection hp = homepage.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(hp.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuilder pagesrc = new StringBuilder();
		while ((inputLine = in.readLine()) != null)
			pagesrc.append(inputLine);
		in.close();
		WrapperMethods.assertIsTrue(pagesrc.toString().contains("5550296508,18793419640"),
				"The Meta tag is displayed as expected", "The Meta tag in page source is not displayed as expected");
		WrapperMethods.assertIsTrue(pagesrc.toString().contains("fb:pages"),
				"The Meta tag fb:pages is displayed as expected",
				"The Meta tag fb:pages is not displayed as expected - Please check!!!");
	}

	public void clickHere() {
		String loginurl = EnvironmentHelper.getURL() + "/mycnn";
		MethodDef.navUrl(loginurl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
			MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			UMReporter.log(LogStatus.INFO, "User is already log in and hence logging out");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "User is not logged in");
		}
		try {
			WrapperMethods.verifyElement(HomePage.getClickHereButton(), "Click Here link");
			WrapperMethods.clickJavaScript(HomePage.getClickHereButton());
			WrapperMethods.contains_Text_Attribute(HomePage.getTermsOfService(), "href", "/terms",
					"Terms of Service has the hyperlink", "Terms of Service doesnt have the hyperlink");
			WrapperMethods.contains_Text_Attribute(HomePage.getPrivacyPolicy(), "href", "/privacy",
					"Privacy Policy has the hyperlink", "Privacy Policy doesnt have the hyperlink");
			WrapperMethods.clickJavaScript(HomePage.getRegisterDetailsSubmit());
			WrapperMethods.contains_Text(HomePage.getErrorMsg(), "This field is required",
					"Error Message should be displayed while submitting the form without entering the data and the error msg is : ",
					"Error Message is not displayed while submitting the form without entering the data.");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Click here");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void forgotPasswordMyCnn() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + "/mycnn");
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String loginurl = EnvironmentHelper.getURL() + "/mycnn";
		try {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
			MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			Reporter.log("User is already log in and hence logging out");
		} catch (Exception e) {
			Reporter.log("User is not logged in");
		}
		try {
			WrapperMethods.verifyElement(HomePage.getForgotPassword(), "Forgot Password link");

			WrapperMethods.clickJavaScript(HomePage.getForgotPassword());

			WrapperMethods.sendKeys(HomePage.getPassWordResetEmail(), "turnercnn1@gmail.com");

			WrapperMethods.clickJavaScript(HomePage.getPassWordResetSubmit());
			WrapperMethods.contains_Text(HomePage.getPasswordResetMessage(),
					"An email regarding your password change has been sent to your email address.",
					"Message is displayed after submitting the email id and the message is : ",
					"Message is not displayed after submitting the email id");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing ForgotPassword");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void forgotPasswordwithInvalidEmail() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + "/mycnn");
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		String loginurl = EnvironmentHelper.getURL() + "/mycnn";
		try {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
			MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			Reporter.log("User is already log in and hence logging out");
		} catch (Exception e) {
			Reporter.log("User is not logged in");
		}
		try {
			WrapperMethods.verifyElement(HomePage.getForgotPassword(), "Forgot Password link");

			WrapperMethods.clickJavaScript(HomePage.getForgotPassword());

			WrapperMethods.sendKeys(HomePage.getPassWordResetEmail(), "skfdjsjfdjgdfg");

			WrapperMethods.clickJavaScript(HomePage.getPassWordResetSubmit());
			WrapperMethods.contains_Text(HomePage.getPasswordResetInvalidEmailErrorMsg(),
					"There is no user with that username or email",
					"Error Message should be displayed after submitting the invalidemail id and error message is : ",
					"Error Message is not displayed after submitting the email id");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing ForgotPassword with Invalid Email");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testGalleryElementsAD() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.GALLERY_URL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {

			for (int i = 0; i < 6; i++) {
				WrapperMethods.clickJavaScript(GalleryPage.getGalleryNext());
				GalleryPage.wait(5);
			}
			MethodDef.explicitWaitVisibility(GalleryPage.adOverlay(), "Wait untill Ad is loaded", "Ad is not loaded");
			WrapperMethods.verifyElement(GalleryPage.adOverlay(), "After 6 clicks Ad is ");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Gallery Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testPoliticsPageFBShare() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_branding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		ArticlePageFunctions.fbShareValidations();
	}

	public void testPoliticsPageTwitterShare() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_branding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		ArticlePageFunctions.TwShareValidations();
	}

	public void testPrivacyPolicy() throws IOException {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.testPrivacyPolicy(currentURL);
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Privacy Policy Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testSearch404Page() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + "/abcdgasds");
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Search 404 page  ");
			HomePageFunctions.validateSearch404elements();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Search 404 Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void updateAccountDetails() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		String loginurl = EnvironmentHelper.getURL() + "/mycnn";
		WrapperMethods.enter_URL(loginurl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
			MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			UMReporter.log(LogStatus.INFO, "User is already log in and hence logging out");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "User is not logged in");
		}
		try {
			MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
					&& ConfigProvider.getConfig("Platform").equals("Desktop"))
				WrapperMethods.moveToElement(HomePage.getLoginEmail());

			WebElement LoginEmail = driver.findElement(HomePage.getLoginEmail());
			LoginEmail.clear();
			WrapperMethods.sendKeys(HomePage.getLoginEmail(), "cnnweqa@gmail.com");
			WebElement LoginPass = driver.findElement(HomePage.getLoginPass());
			LoginPass.clear();
			if (ConfigProvider.getConfig("Browser").startsWith("IE")
					|| !ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WrapperMethods.sendKeys(HomePage.getLoginPass(), "cnnweqa");
				// HomePage.getLoginPass().sendKeys(Keys.ENTER);
				WrapperMethods.click(HomePage.getLoginButton(), "Login Button");
			} else if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
				WrapperMethods.sendKeys(HomePage.getLoginPass(), "cnnweqa");
				WrapperMethods.click(HomePage.getLoginButton(), "Login Button");
			} else {
				WrapperMethods.moveToElement(HomePage.getLoginPass());
				WrapperMethods.sendKeys(HomePage.getLoginPass(), "cnnweqa");
				WrapperMethods.moveToElementClick(HomePage.getLoginButton());
			}
			HomePage.wait(20);
			UMReporter.log(LogStatus.INFO, "Updating the First Name and Click Save");
			String text = "Automation TEST COMMENT" + RandomStringUtils.randomNumeric(3);
			WebElement MycnnAccName = driver.findElement(HomePage.getMycnnAccName());
			MycnnAccName.clear();
			WrapperMethods.sendKeys(HomePage.getMycnnAccName(), text);
			WrapperMethods.clickJavaScript(HomePage.getMycnnSave());
			HomePage.wait(10);
			WrapperMethods.contains_Text(HomePage.getMycnnAccDetailsError(), "Profile updated",
					"Update Account Details is working fine: ", "Update Account Details is not working");
			// Logout
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Updating account details");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testTravelArticlePageSocialIcons() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleEmVidUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Travel Article Page Social Icons");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Facebook Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Twitter Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Share With Your Friends Icon");
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				UMReporter.log(LogStatus.INFO, "Testing Travel Article Page Social Icons after scroll down");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski2(), "Facebook Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski3(), "Twitter Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski4(), "Share With Your Friends Icon");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Travel Article Page Social Icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testPoliticsArticlePageSocialIcons() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.article_branding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "Testing Politics Article Page Social Icons");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Facebook Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Twitter Icon");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Share With Your Friends Icon");
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				WrapperMethods.page_scrollDown();
				ArticlePage.wait(2);
				UMReporter.log(LogStatus.INFO, "Testing Politics Article Page Social Icons after scroll down");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski2(), "Facebook Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski3(), "Twitter Icon");
				WrapperMethods.verifyElement(ArticlePage.getGigyaBarElemski4(), "Share With Your Friends Icon");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Politics Article Page Social Icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void ValidateBrandingBanner() throws IOException, InterruptedException {
		String currentURL = TestDataProvider.getData(System.getProperty("environment"));
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.testBrandingBanner();

	}

	public void testindex2tag() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.index2html);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver go = DriverFactory.getCurrentDriver();
		WrapperMethods.assertIsTrue(go.getPageSource().contains("<meta content=\"noindex\""),
				"The Meta Content is present as expected", "The Meta Content is not present - Unexpected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("name=\"robots\""),
				"The Name Content is present as expected", "The Name Content is not present as expected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("<meta name=\"googlebot\""),
				"The Meta Name is present as expected", "The Meta Name is not present - Unexpected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("content=\"noindex\""),
				"The Content is present as expected", "The Content is not present - Unexpected");
	}

	public void testindex4tag() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.index4html);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		WebDriver go = DriverFactory.getCurrentDriver();
		WrapperMethods.assertIsTrue(go.getPageSource().contains("<meta content=\"noindex\""),
				"The Meta Content is present as expected", "The Meta Content is not present - Unexpected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("name=\"robots\""),
				"The Name Content is present as expected", "The Name Content is not present as expected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("<meta name=\"googlebot\""),
				"The Meta Name is present as expected", "The Meta Name is not present - Unexpected");
		WrapperMethods.assertIsTrue(go.getPageSource().contains("content=\"noindex\""),
				"The Content is present as expected", "The Content is not present - Unexpected");
	}

	public void Validateshowdescftershrunk() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Description_shrunk);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.testshowdescafterresize();

	}

	public void ValidateVerticalgallery() throws IOException, InterruptedException {
		String currentURL = TestDataProvider.getData(System.getProperty("environment"));
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			ArticlePageFunctions.testimagecutverticalgallery();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Gallery page elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void tripAdvisor() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.travel);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		CommomPageFunctions.tripAdvisorVal();
	}

	public void testTripAdvisorValidations() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.travel_page);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			SectionFrontsPageFunctions.tripAdvisor();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Trip advisor elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testVideoPageTwitterShare() {
		base(Prefs.videoLanding);
		VideoLeafPageFunctions.TwShareValidations();
	}

	public void testVideoPageFBShare() {
		base(Prefs.videoLanding);
		VideoLeafPageFunctions.fbShareValidations();
	}

	public void VideoLandingPinnedVideoFooter() {
		base(Prefs.videoLanding);
		try {
			MethodDef.explicitWaitVisibility(VideoLeafPage.getTheoVideoElement(), 25,
					"Able to visible the Theo Video element", "Not able to visible the Theo video element");
			try {
				WebElement videoPlayer = MethodDef.getvideoObject();
				MethodDef.vidPlayerBasicValidations(videoPlayer);
			} catch (Exception E) {

			}

			ArticlePageFunctions.commonPinnedVideoValidation();

			WrapperMethods.scrollToPageBottom();
			MethodDef.isNotVisible(VideoLeafPage.getVideoElementPinned());

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Pinned Video Page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testPinnedVideoNotListed() {
		base(Prefs.videoLanding);

		try {
			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			WrapperMethods.click(ArticlePage.getVideoElementPinned(), "Clicked on Video Pinned Element",
					"Not able to click on video pinned element..");
			UMReporter.log(LogStatus.WARNING,
					"Testing Of Pinned Video - Negative scenarios is skipped as the player started");
		} catch (Exception EE) {
			UMReporter.log(LogStatus.PASS,
					"Pinned Video is not displayed - when the scrolling is done before Video load");
		}
	}

	public void testVideoLandingPinnedVideo() {
		base(Prefs.videoLanding);

		try {
			try {
				WebElement videoPlayer = MethodDef.getvideoObject();
				MethodDef.vidPlayerBasicValidations(videoPlayer);
			} catch (Exception E) {

			}
			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			ArticlePageFunctions.pinnedVideoValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Pinned Video Page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testVideoLandingPageGradient() {

		base(Prefs.videoLanding);
		VideoLeafPageFunctions.zoneGradientCheck();
	}

	public void testVideoLandingPageLazyLoadZones() {

		base(Prefs.videoLanding);
		try {
			Boolean read = MethodDef.getJsonVideoLandingLazyLoadFlag();
			if (read) {

				UMReporter.log(LogStatus.PASS, "Lazy Load Flag is set as TRUE");
				VideoLeafPageFunctions.VideoLandingPageLazyLoadZonesValidation();

			} else
				UMReporter.log(LogStatus.WARNING, "Lazy Load Flag is set as FALSE");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Video Landing Page Lazy Load Zones Validation");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public void videoZone1AD() {

		base(Prefs.videoLanding);
		VideoLeafPage.wait(10);
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WrapperMethods.isDisplayed(VideoLeafPage.getZone1DesktopAD(), "AD is displayed in the 1st Zone",
						"AD is not displayed in the 1st Zone");
			} else if (ConfigProvider.getConfig("Platform").equals("Tab")
					&& Integer.parseInt(ConfigProvider.getConfig("Res_Width").toString()) > 800) {
				WrapperMethods.isDisplayed(VideoLeafPage.getZone1TabAD(), "AD is displayed in the 1st Zone",
						"AD is not displayed in the 1st Zone");
			} else {
				UMReporter.log(LogStatus.INFO,
						"INFO: AD will not be displayed for MOBILES and TABS of Screen Size lesser than 800");
			}
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}

	}

	public void videoEmbedButton() {

		base(Prefs.videoLanding);
		VideoLeafPage.wait(25);
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				MethodDef.explicitWaitVisibility(VideoLeafPage.getVideoEmbed(), "The Embedded button is visible",
						"The Embedded button is not visible");
				WrapperMethods.isDisplayed(VideoLeafPage.getVideoEmbed(), "The Embedded button is displayed",
						"The Embedded button is not displayed");
				VideoLeafPageFunctions.validateEmbeddedbutton();
			}
		}
	}

	public void testVideoLanding3rdZone() {
		base(Prefs.videoLanding);
		Boolean unficVid = MethodDef.getJsonVideoUnificationFlag();
		if (unficVid) {

			// // Pre Steps
			// try {
			// MethodDef.cnnConsent(wf.go(), error,
			// BasePage.getTermsConsentClose(wf.go()));
			// } catch (Exception E) {
			// }
			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			MethodDef.explicitWaitVisibility(VideoLeafPage.get3rdSection(), "3rd Section is visible",
					"3rd Section is not visible");
			WrapperMethods.assertHasAttribute(WrapperMethods.getWebElement(VideoLeafPage.get3rdSection()), "id",
					"recommended-digital-studios", "Video 3rd Zone has Digital Studios Section",
					"Video 3rd Zone doesn't have Digital Studios Section");

		} else {
			UMReporter.log(LogStatus.WARNING, "Video Unification Code is not Enabled for this Environment");
		}
	}

	public void Appiastylemobile() {
		base(Prefs.videoLanding);

		try {
			UMReporter.log(LogStatus.INFO, "<B>======The Appia Style Page mobile web automation starts=====</B>");
			SectionFrontsPageFunctions.StyleAppiavalidation();
			UMReporter.log(LogStatus.INFO, "<B>======The Appia Style Page mobile web automation ends=====</B>");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Appia Style Page mobile web elements ");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public void testHeaderStyleSubSections() {

		base(Prefs.Style_URL);
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_style").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_style").split(",");

		StylePageFunctions.styleValidation(Prefs.AUT_URL, domData,
				Integer.parseInt(ConfigProvider.getConfig("Res_Width").toString()));
	}

	public void testHP10MinPreview() {
		base(Prefs.hp10_URL);
		CommomPageFunctions.testHp10Element();
	}

	public void testHP10MinPreviewPlay() {
		base(Prefs.hp10_URL);
		CommomPageFunctions.testHp10ElementPlay();
	}

	public void testHP10MinPreviewPlayLogin() {
		base(Prefs.hp10_URL);
		CommomPageFunctions.testHp10ElementPlayLogin();
	}

	public void testHMHP10MinPreviewPlay() {
		base(Prefs.hp10_home_URL);
		CommomPageFunctions.testHp10ElementPlay();
	}

	public void testHP10MinVideoOpens() {
		base(Prefs.hp10_home_URL);
		CommomPageFunctions.testHp10VideoOpensInSameBrowser();
	}

	public void testTripAdvisorServiceInfo() {
		if (!System.getProperty("environment").equals("REF")) {
			base(Prefs.tripadvisor_url);
		} else {
			base(Prefs.tripadvisor_Ref_url);
		}

		BasePage.wait(10);
		try {
			UMReporter.log(LogStatus.INFO, "Testing Trip Advisor Service info widget in the story");

			WrapperMethods.isDisplayed(ArticlePage.getTripAdvisorServiceInfo(),
					"Trip Advisor Service info widget is  displayed",
					"Trip Advisor Service info widget is not displayed");

			WrapperMethods.assertIsTrue(ArticlePage.EmbeddedTagTripAdvisors().size() == 0,
					"The Embedded WebTag is not presentis the Page as expected",
					"The embedded WebTag is present in the page - UnExpexted");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Trip Advisor Service info widget element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void carouselpagination() {
		if (System.getProperty("environment").equals("REF")) {
			base(Prefs.style_carousel_paginated);
		} else {
			throw new SkipException(
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
		base(Prefs.tripadvisor_url);
		BasePage.wait(5);

		try {
			UMReporter.log(LogStatus.INFO,
					"<B>======The Style page carousel pagination web automation starts=====</B>");
			SectionFrontsPageFunctions.carouselpaginationBGvalidation();
			UMReporter.log(LogStatus.INFO, "<B>======The Style page carousel pagination web automation ends=====</B>");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Appia Style Page mobile web elements ");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public void VRDegree() throws IOException {
		base(Prefs.VR);
		try {
			VideoLeafPageFunctions.vrPageValidations();
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing VR page  elements ");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public void testVRZones2to4() throws IOException {
		base(Prefs.VR);
		try {
			VideoLeafPageFunctions.VRZones2to4Validations();
		} catch (Exception e) {

			UMReporter.log(LogStatus.FAIL, "Error in accessing VR page zones 2 to 4 elements");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public void testVRVideo() throws IOException {
		base(Prefs.VRVideopage);
		ArticlePageFunctions.VRPlayervalidation();

	}

	public void testHP10PinnedVideo() {
		WrapperMethods.enter_URL(Prefs.hp10_pinned);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "TESING: Pinned");
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(), "HP10FreePreview Play Button is clicked",
					"HP10FreePreview Play Button is not able to click");

			MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), 30, "hp10FreePreviewSpinner is visible",
					"hp10FreePreviewSpinner is not visible");

			BasePage.wait(180);

			WrapperMethods.page_scrollDown();

			CommomPageFunctions.pinnedVidLocation();
		} catch (Exception e) {

		}
	}

	public void testHP10MinRefresh() {
		String url = MethodDef.correctUrl(Prefs.hp10_home_URL);
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		CommomPageFunctions.testHP10MinRefresh();

	}

	public void testSectionPages() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		Boolean errorFoundBrokenLinks = false;
		Boolean errorFoundJavaScripts = false;
		errorFoundJavaScripts = validateJSErrors();
		if (errorFoundJavaScripts || errorFoundBrokenLinks) {
			UMReporter.log(LogStatus.FAIL, "JavaScript Errors found on this page");
		}
	}

	public void testSectionPagesPolitics() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		Boolean errorFoundBrokenLinks = false;
		Boolean errorFoundJavaScripts = false;
		errorFoundJavaScripts = validateJSErrors();
		if (errorFoundJavaScripts || errorFoundBrokenLinks) {
			UMReporter.log(LogStatus.FAIL, "JavaScript Errors found on this page");
		}
	}

	public boolean validateJSErrors() {
		boolean errorFoundJavaScripts = false;
		ExpectedCondition e = new ExpectedCondition() {
			public Boolean apply(Object d) {
				try {
					JavascriptExecutor js = (JavascriptExecutor) d;
					Boolean isReady = (Boolean) js.executeScript("return AdfPage.PAGE.isSynchronizedWithServer()");
					return isReady;
				} catch (WebDriverException e) {
					// somthingwrongbutidontcare
					UMReporter.log(LogStatus.WARNING,
							"JAVA SCRIPT is not synchronised with the server" + e.getMessage());
					return false;
				}
			}
		};

		/*
		 * try { List<JavaScriptError> jsErrors = JavaScriptError
		 * .readErrors(wf.go()); if (!jsErrors.isEmpty()) { Reporter.log(
		 * "<br>FAILED: Java Script Errors found " + jsErrors.toString() +
		 * "</br>"); errorFoundJavaScripts = true; } } catch (Exception d) {
		 * d.printStackTrace(); Reporter.log(d.getMessage()); }
		 */
		if (!errorFoundJavaScripts) {
			UMReporter.log(LogStatus.PASS, "No JavaScript Errors found on this page");
		}
		return errorFoundJavaScripts;
	}

	public void testSpecialPageDarkTheme() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.special_URL_darktheme);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		Boolean errorFoundJavaScripts = false;
		/*
		 * try { List<JavaScriptError> jsErrors = JavaScriptError
		 * .readErrors(wf.go()); if (!jsErrors.isEmpty()) { Reporter.log(
		 * "<br>FAILED: Java Script Errors found " + jsErrors.toString() +
		 * "</br>");
		 * 
		 * }
		 * 
		 * } catch (Exception d) { d.printStackTrace();
		 * Reporter.log(d.getMessage()); }
		 */
		if (!errorFoundJavaScripts) {
			UMReporter.log(LogStatus.PASS, "No JavaScript Errors found on this page Money page");
		}
		try {
			WrapperMethods.isDisplayed(SpecialPage.getSpecialPageZone1(), "Zone1 is displayed",
					"Zone1 is not displayed");
			/*
			 * String classValue =
			 * SpecialPage.getSpecialPageZone1().getAttribute("class");
			 * WrapperMethods.assertIsTrue(classValue.contains("t-dark"),
			 * "The container theme is dark",
			 * "The theme of the container is not dark");
			 */
			WrapperMethods.contains_Text_Attribute(SpecialPage.getSpecialPageZone1(), "class", "t-dark",
					"The container theme is dark", "The theme of the container is not dark");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getCssValue(SpecialPage.getSpecialPageBlueLinkText(), "color")
							.equals("rgba(36, 131, 179, 1)"),
					"The color of the link text is blue and the value is  : "
							+ WrapperMethods.getCssValue(SpecialPage.getSpecialPageBlueLinkText(), "color"),
					"The color of the link text is not blue "
							+ WrapperMethods.getCssValue(SpecialPage.getSpecialPageBlueLinkText(), "color"));
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in testing the special Page Dark theme : Link Text");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testSportsStoryTicker() {
		String currentURL = TestDataProvider.getData(System.getProperty("environment"));
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		if ((System.getProperty("edition").equals("INTL"))) {
			try {
				WrapperMethods.isDisplayed(sportsPage.getSportTicker(),
						"The sport ticker is displayed for the sport article page",
						"The sport ticker is not displayed for the sport article page");
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error in accessing the SPORTS Ticker elements");
				UMReporter.log(LogStatus.INFO, E.getMessage());
			}
		} else {
			UMReporter.log(LogStatus.INFO, "Sports Ticker is not applicable for DOMESTIC SITE");
		}
	}

	public void testSportsTickerValidations() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.SPORTS_URL_INTL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		;
		if ((System.getProperty("edition").equals("INTL"))) {
			try {
				sportsPageFunctions.sportsTickerURLValidations();
				sportsPageFunctions.sportsTickerURL();
				if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
					UMReporter.log(LogStatus.INFO, "color before mouse hover on sport ticker");
					WrapperMethods.assertIsTrue(
							WrapperMethods.getCssValue(sportsPage.getSportTickerSection(), "color")
									.equals("rgba(255, 255, 255, 1)"),
							"The color of sport section before  hover and the color is black :  "
									+ WrapperMethods.getCssValue(sportsPage.getSportTickerSection(), "color"),
							"color is not black before hover"
									+ WrapperMethods.getCssValue(sportsPage.getSportTickerSection(), "color"));

					Thread.sleep(2000);
					UMReporter.log(LogStatus.INFO, "Color Change to RED on mouse hover on sport ticker");
					WrapperMethods.moveToElement(sportsPage.getSportTickerArticle());
					WrapperMethods.assertIsTrue(
							WrapperMethods.getCssValue(sportsPage.getSportTickerArticle(), "color")
									.equals("rgba(204, 0, 0, 1)"),
							"The color of sport article changed to red on hover and the color is red : "
									+ WrapperMethods.getCssValue(sportsPage.getSportTickerArticle(), "color"),
							"The color is not changed");
				}

			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error in accessing the SPORTS Ticker elements");
				UMReporter.log(LogStatus.INFO, E.getMessage());
			}
		} else {
			UMReporter.log(LogStatus.INFO, "Sports Ticker is not applicable for DOMESTIC SITE");
		}

	}

	public void testPartnerHotelWidget() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.travel_page);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			SectionFrontsPageFunctions.partnerhotelValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Partner Hotel Widget elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public void testHeaderEntertainmentMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavEntertainMenu();
	}

	public void testHeaderHealthMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavHealthMenu();
	}

	public void testHeaderBleacherMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavBleacherMenu();
	}

	public void testHeaderLivingMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavLivingMenu();
	}

	public void testHeaderMoneyMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavMoneyMenu();
	}

	public void testHeaderMoreMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavMoreMenu();
	}

	public void testHeaderLogoNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNav();
	}

	public void testHeaderLogoNavStatic() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + TestDataProvider.getData(System.getProperty("edition")));
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNav();
	}

	public void testHeaderMenuNavFlyout() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerNavFlyOut();
	}

	public void testHeaderOpinionsMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavOpinionsMenu();
	}

	public void testHeaderPoliticsMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavPoliticsMenu();
	}

	public void testHeaderTechMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavTechMenu();
	}

	public void testHeaderStyleMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavStyleMenu();
	}

	public void testHeaderTravelMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavTravelMenu();
	}

	public void testHeaderUSMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavUSMenu();
	}

	public void testHeaderUSMenuNavBreaking() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavUSMenu();
	}

	public void testHeaderVideosMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavVideosMenu();
	}

	public void testHeaderWorldMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavWorldMenu();
	}

	public void testHeaderINTLFeaturesMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderINTLFeaturesNavMenu();
	}

	public void testHeaderINTLRegionsMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderINTLRegionsNavMenu();
	}

	public void testHeaderINTLMoreMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderINTLNavMoreMenu();
	}

	public void moneyNav() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.moneyNav();
	}

	public void SubscribepageCSSvalidation() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Subscription);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.ValidatesubscribtionCSSproperties();
	}

	public void testSectionPageAds() throws IOException, InterruptedException {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		SectionFrontsPageFunctions.adElements();
	}

	public void testPoliticsHeaderElements() throws IOException, InterruptedException {

		HomePageFunctions hpf = new HomePageFunctions();
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.clickJavaScript(SectionFrontsPage.getHam());
		}

		try {

			PoliticsPageFunctions.testPoliticsHeaderElementsGeneral();
			PoliticsPageFunctions.headerTesting(PoliticsPageFunctions.politicsheaderLinks());
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				UMReporter.log(LogStatus.INFO, "Validations after Scroll down the Page...");
				JavascriptExecutor jse = (JavascriptExecutor) DriverFactory.getCurrentDriver();
				jse.executeScript("scroll(0, 250)");
				PoliticsPageFunctions.testPoliticsHeaderElementsGeneral();
				PoliticsPageFunctions.headerTesting(PoliticsPageFunctions.politicsheaderLinks());
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Politics header Elements");
		}
	}

	public void PoliticsPageFooter() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		PoliticsPageFunctions.politicsFooter();
	}

	public void testHeaderPoliticsSubSections() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		String[] domData;
		if (System.getProperty("site").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_politics").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_politics").split(",");

		PoliticsPageFunctions.politicsValidation(domData);

	}

	public void politicsSectionOutbrain() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.outbrainValidate();
	}

	public void testpoliticssubscribebox() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		PoliticsPageFunctions.testEmailpolitcshome();
	}

	public void testContentElements() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL1);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testContentNavigationElements();
	}

	public void testArticleReadMore() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL1);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.articleReadMore();
	}

	public void testArticlePage() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL1);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testheader();
		ArticlePageFunctions.testArticleAuthor();

	}

	public void testpaidcontentoutbrain() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL1);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testarticleoutbrain();

	}

	public void testPageTopGallery() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleGalUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testarticlepagetopgallery();

	}

	public void testArticleVideoPage() throws IOException, InterruptedException {
		String url;
		if (!System.getProperty("environment").equals("REF"))
			url = Prefs.articlePTVideo;
		else
			url = Prefs.articleEmFullGal_Ref;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testArticleVideoPage();

	}

	public void testArticleVideoLeafOutbrain() throws IOException, InterruptedException {
		if (!System.getProperty("environment").equals("REF"))
			url = Prefs.articlePTVideo;
		else
			url = Prefs.articleEmFullGal_Ref;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			ArticlePageFunctions.testOutbrain();
		} else {
			ArticlePageFunctions.testOutbrainTab();

		}

	}

	public void testArticleGalleryEmb() throws IOException, InterruptedException {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleGalEmUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		ArticlePageFunctions.testArticleGalleryEmbedded();

	}

	public void testArticleGalleryLeafOutbrain() throws ClientProtocolException, IOException {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.articleGalEmUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		UMReporter.log(LogStatus.INFO, "Testing with Gallery Leaf Page Outbrain Elements");

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
			ArticlePageFunctions.testOutbrain();
		else
			ArticlePageFunctions.testOutbrainTab();

	}

	public void testVideoLeaf() {

		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		VideoLeafPageFunctions.basicVideoValidations();
	}

	public void testVideoLeafVid() {

		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		VideoLeafPageFunctions.videoleafvideo();
	}

	public void testVideoLeafPlayerDynamicContent() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.basicVideoElemDynamicContent();

	}

	public void testVideoLeafPlayerPrevNav() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.testVideoLeafPlayerPrevNav();

	}

	public void testVideoLeafPlayerNextNav() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.testVideoLeafPlayerNextNav();

	}


	public void testVideoLeafOutbrain() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.testVideoLeafOutbrain();

	}


	public void testVideoPagePlayIcon() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.basicVideoPlayIcon();

	}

	public void testVideoLandingPlayIcon() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoLanding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.basicVideoPlayIcon();
	}

	public void testVideoShareMeta() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.videoleaf_Ref;
		else
			url = Prefs.LeafPage;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.testVideoShareMeta();
	}




	public void testVideoLeafPageGradient() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		// WrapperDef.videoObjectCheck(wf.go(), error);

		VideoLeafPageFunctions.zoneGradientCheck();
	}

	public void testVideoLeafPageLazyLoadZones() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoUrl);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		// WrapperDef.videoObjectCheck(wf.go(), error);

		VideoLeafPageFunctions.VideoLeafPageLazyLoadZonesValidation();
	}

	public void invalidEmailSubscription() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.mailsubscription_url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		// WrapperDef.videoObjectCheck(wf.go(), error);
		SubscriptionPageFunctions.invalidEmailSubscription();
	}

	public void validEmailSubscription() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.mailsubscription_url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		// WrapperDef.videoObjectCheck(wf.go(), error);
		SubscriptionPageFunctions.validEmailSubscription();
	}

	public void existingEmailSubscription() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.mailsubscription_url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		// WrapperDef.videoObjectCheck(wf.go(), error);
		SubscriptionPageFunctions.existingEmailSubscription();
	}

	public void testAppiaAds() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.travel_page);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		SectionFrontsPageFunctions.testAppiaAds();
	}

	public void testVideoLandingPlayerDynamicContent() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoLanding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		VideoLeafPageFunctions.basicVideoElemDynamicContent();

	}

	public void testQunatCastTag() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoLanding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		if (System.getProperty("edition").equals("INTL")) {
			SectionFrontsPageFunctions.intlQuancastTag();
		} else {
			SectionFrontsPageFunctions.domQuancastTag();
		}

	}

	public void testArticlePinnedEmbedded() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.pinned_n_Embbeded);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			MethodDef.explicitWaitVisibility(ArticlePage.getVideoElement(), 20, "Video element is visible",
					"Video element is not visible");
			try {
				WebElement videoPlayer = MethodDef.getvideoObject();
				MethodDef.vidPlayerBasicValidations(videoPlayer);
			} catch (Exception E) {
			}

			VideoLandingPageFunctions.commonPinnedVideoValidation();
			VideoLandingPageFunctions.pinnedVidLocation();
			VideoLandingPageFunctions.clickFirstEmbVideo();
			VideoLandingPageFunctions.embVideoposition();
			VideoLandingPageFunctions.pinnedVidLocation();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Pinned Video Page element");
		}

	}

	public void testArticlePinnedVideo() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.articleautoplay_Ref;
		else
			url = Prefs.articleVidUrl;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			MethodDef.explicitWaitVisibility(ArticlePage.getVideoElement(), 20, "Video element is present",
					"Video element is not present");
			try {
				WebElement videoPlayer = MethodDef.getvideoObject();
				MethodDef.vidPlayerBasicValidations(videoPlayer);
			} catch (Exception E) {

			}
			VideoLandingPageFunctions.pinnedVideoValidations();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Pinned Video Page element");
		}

	}

	public void testHeaderEditionValidations() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		HomePageFunctions.headerEditionPickerValidations();
	}

	public void testHomePageWatchTv() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		HomePageFunctions.testHomePageWatchTv();
	}

	public void testHeaderBrokenLinks() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		try {
			MethodDef.brokenLinkValidation(HomePage.getHeaderAllNavSections());
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Failed: Error accessing the header item" + e.getMessage());
		}
	}

	public void testFooterSectionValidation() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		HomePageFunctions.testFooterSectionValidation();
	}

	public void testHeaderMenuLinks() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		try {
			WrapperMethods.click(HomePage.getNavMenu(), "Nav Menu");
			for (WebElement elem : HomePage.getHeaderMenuLinks()) {

				MethodDef.brokenLinkValidation(elem.getAttribute("href"));
			}

			for (WebElement secelem : HomePage.getSectionLinks()) {
				MethodDef.brokenLinkValidation(secelem.getAttribute("href"));
			}

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Failed: Error accessing the header item");
		}
	}

	public void testHeaderSectionLinks() {
		// Creating custom Page instances
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		try {
			WrapperMethods.click(HomePage.getNavMenu(), "Nav Menu");
			for (WebElement secelem : HomePage.getSectionLinks()) {
				MethodDef.brokenLinkValidation(secelem.getAttribute("href"));
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Failed: Error accessing the header item");
		}
	}

	public void testBreakingnews() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		try {
			HomePageFunctions.breakingnewsvalidation();

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "The breaking news is not displayed");
		}
	}

	public void testTimelineDarkTheme() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		HomePageFunctions.testTimelineDarkTheme();
	}

	public void testTimelineLightTheme() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		HomePageFunctions.testTimelineLightTheme();
	}

	public void CNNPageLoadWhitePagecheck() {

		MethodDef.navUrl(EnvironmentHelper.getURL());

		WrapperMethods.checkallpageload();
		UMReporter.log(LogStatus.PASS, "The Page didn't flash any White screen on Launch");

	}

	public void testPinnedVideoNotListedd() {
		if (System.getProperty("environment").equals("REF"))
			url = Prefs.articleautoplay_Ref;
		else
			url = Prefs.articleVidUrl;

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + url);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		ArticlePageFunctions.testPinnedVideoNotListed();
	}

	public void VideoLandingDescChange() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.videoLanding);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		VideoLeafPageFunctions.valiDesc();
	}

	public void testSearchIconValidation() {

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.Entertainent);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		ArticlePageFunctions.SearchIconValidation();
	}

	public void ChangedLinknotlinkedtotile() {
		UMReporter.log(LogStatus.INFO, "The Changed Link which are not linked to tiles validation Starts");

		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.politicsURL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		UMReporter.log(LogStatus.INFO, "The Changed Link which are not linked to tiles validation ends");
	}

	public void Articlevideogalpagetag() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		VideoLeafPageFunctions.checkarticlevideogallerypage();

	}

	public void LiveblogHeader() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		ArticlePageFunctions.LiveblogHeaderVal();

	}

	public void Carousel360Videos() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		VideoLeafPageFunctions.Video360Carousel();

	}

	public void ArticleTimestampAllignment() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		ArticlePageFunctions.ArticleTimestampAlignment();

	}

	public void ArticleSpeakableParagraph() {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		ArticlePageFunctions.ArticleSpeakableTimestamp();

	}

	public void liveFyreCarousel() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.LIVE_BLOG_CAROUSEL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		ArticlePageFunctions.liveFyreCarousel();
	}

	public void SearchRadioButton() {
		String currentURL = TestDataProvider.getData("Data");
		String CurrentKey = TestDataProvider.getData("Key");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}

		try {
			UMReporter.log(LogStatus.INFO, "The Search page radio button validation starts here");

			if ((EnvironmentHelper.getURL() + currentURL).contains("entertainment"))
				HomePageFunctions.SearchEntertainmentRadiobuttonvalidation(CurrentKey);
			else if (!(EnvironmentHelper.getURL() + currentURL).contains("style"))
				HomePageFunctions.SearchRadiobuttonvalidation(CurrentKey);
			else
				HomePageFunctions.SearchStyleRadiobuttonvalidation(CurrentKey);

			UMReporter.log(LogStatus.INFO, "The Search page radio button validation ends here");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing FB Subscription Elements");
		}

	}

	public void testGalleryCaptions() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.GALLERY_URL);
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		GalleryPageFunctions.testGalleryCaptions();
	}

	public void loginValidationGoogle() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		try {
			MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		} catch (Exception E) {
		}
		ArticlePageFunctions.loginValidationGoogle();

	}

	public void Gallerypagewhitepagecheck() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		try {
			UMReporter.log(LogStatus.INFO, "The White Background gallery page validation starts here");

			ArticlePageFunctions.checkwhiteBGgallerypage();
			UMReporter.log(LogStatus.INFO, "The White Background gallery page validation ends here");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Gallery page elements ");
		}

	}

	public void testHeaderINTLVideosMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderINTLNavVideosMenu();

	}

	public void testHeaderINTLSportMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();

		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderINTLNavSportMenu();

	}

	public void testHeaderINTLTravelMenuNav() {
		WrapperMethods.enter_URL(EnvironmentHelper.getURL());
		WrapperMethods.checkallpageload();
		WrapperMethods.preSteps();
		HomePageFunctions.headerRedLogo();
		HomePageFunctions.headerHeaderNavINTLTravelMenu();

	}

	public void testFooterPoliticsSubSections() {
		base(Prefs.politicsURL);
		PoliticsPageFunctions.politicsFooterValidation();
	}
	public void testTvSchedulePageDDValidations() {

		base(Prefs.tvScheduleUrl);
		TvPagesFunctions.scheduleDDValidations();
		WrapperMethods.click(TvPages.getTVScheduleDropDown(), "Clicked on TV Schedule Dropdown",
				"Not able to Clicked on TV Schedule Dropdown");
	}
	public void testGalleryPageresizecaption() {
		base(Prefs.GALLERY_REF_STAGE_URL);
		UMReporter.log(LogStatus.INFO, "<B>Info :Article Page Window Resize caption check </B>");
		ArticlePageFunctions.validategalleryCaptionafterresize();
	}
	public void LiveFyreImageUnderlineCaption() {
		base(Prefs.LIVE_BLOG);
		UMReporter.log(LogStatus.INFO,
				"<B>======The Live Blog Imge with Underlined Caption validation test starts=====</B>");
		ArticlePageFunctions.LiveFyreImageUnderlineCaption();
		UMReporter.log(LogStatus.INFO,
				"<B>======The Live Blog Imge with Underlined Caption validation test Ends=====</B>");

	}
	
	public void testArticleliveblog() {
		base(Prefs.LIVE_BLOG);
		WebDriver d = DriverFactory.getCurrentDriver();
		WrapperMethods.scrollDown();
		WrapperMethods.moveToElement(ArticlePage.getLiveFyreHeading(d));
		ArticlePageFunctions.articleliveblog();

		ArticlePageFunctions.liveBlogSocialShareButtonText();

		ArticlePageFunctions.liveBlogAvatarLogo();

		WrapperMethods.scrollDown();

		WrapperMethods.scrollDown();

		WrapperMethods.scrollDown1000(d);

		WrapperMethods.moveToElement(ArticlePage.liveBlogImageCaption(d));

		ArticlePageFunctions.liveBlogImageCaption();
	}
	
	public void LiveFyreImageBoldCaption() {
		base(Prefs.LIVE_BLOG);
		UMReporter.log(LogStatus.INFO, "<B>======The Live Blog Imge with Bold Caption validation test starts=====</B>");
		ArticlePageFunctions.LiveFyreImageBoldCaption();
		UMReporter.log(LogStatus.INFO, "<B>======The Live Blog Imge with Bold Caption validation test Ends=====</B>");

	}
	
	public void LiveblogFooterWeatherCard() {
		base(Prefs.LIVE_BLOG);
		UMReporter.log(LogStatus.INFO, "<B>======The Live Blog Footer Weather Section test starts=====</B>");

		ArticlePageFunctions.LiveblogFooterWeatherCard();

		UMReporter.log(LogStatus.INFO, "<B>======The Live Blog Footer Weather Section test ends=====</B>");
	}
	public void testTopBannerAdExpandable() {

		base(Prefs.topBannerAd_exp);
		try {

			WrapperMethods.switchToFrame(HomePage.getFrameElement());
			WrapperMethods.SleepSeconds(10);
			UMReporter.log(LogStatus.INFO, "<b>Expandable Top Banner Ad validations: </b>");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getNormalTopBannerAd()).getAttribute("style")
							.contains("block"),
					"The Expandable Top Banner Ad is displayed with Expand button",
					"The Expandable Top Banner Ad is not displayed with Expand button");
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.isDisplayed(HomePage.getClickThroughButton(),
						"Click Through Button is displayed in the Ad",
						"Click Through Button is not displayed in the Ad");
			}
			WrapperMethods.isDisplayed(HomePage.getExpandButton(), "Expandable Button is displayed in the Ad",
					"Expandable Button is not displayed in the Ad");
			UMReporter.log(LogStatus.INFO, "<b>Click on Expandable Button in the Top Banner Ad: </b>");
			WrapperMethods.click(HomePage.getExpandButton(), "Clicked Expand Button",
					"Not able to click Expand Button");
			WrapperMethods.SleepSeconds(5);
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.getExpandedTopBannerAd())
					.getAttribute("style").contains("block"), "Top Banner Ad is expanded",
					"Top Banner Ad is not expanded");
			WrapperMethods.isDisplayed(HomePage.getCloseButton(), "Close Button is displayed in the Ad",
					"Close Button is not displayed in the Ad");
			UMReporter.log(LogStatus.INFO, "<b> Click on Close Button in the Top Banner Ad: </b>");
			WrapperMethods.click(HomePage.getCloseButton(), "Clicked the Close button of ExpandedTopBannerAd",
					"Not able to click the Close button of ExpandedTopBannerAd");
			WrapperMethods.SleepSeconds(5);
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getNormalTopBannerAd()).getAttribute("style")
							.contains("block"),
					"The Expandable Top Banner Ad is displayed with Expand button",
					"The Expandable Top Banner Ad is displayed with Expand button");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "<b><u>Error in accessing Expandable Top Banner Ad </u></b>");
			UMReporter.log(LogStatus.FAIL, E.getMessage());
		}
	}
	public void testSpecialBranding() {
		base(Prefs.special_branding);
		SpecialPageFunctions.testBranding();
	}

	public void testVideoBranding() {
		base(Prefs.video_branding);
		Reporter.log("Info: Special Pages Branding");
		VideoLeafPageFunctions.testBranding();
	}
	
	public void SubscriptionMessengericon() throws InterruptedException {

		base(Prefs.GALLERY_REF_STAGE_URL);
		UMReporter.log(LogStatus.INFO, "<B>======The FB Subscription validation starts starts=====</B>");

		HomePageFunctions.CMessengervalidation(HomePage.SubscriptionMessengerr().get(1));

		UMReporter.log(LogStatus.INFO, "<B>======The FB Subscription Validation ends=====</B>");

	}
	public void testArticlePageEmailShare() {
		base(Prefs.article_branding);
		ArticlePageFunctions.emailShareValidations();
	}
	public void searchValidation() {
		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {

			String currentURL = TestDataProvider.getData("Data_dom");
			if (!currentURL.equalsIgnoreCase("NULL")) {
				WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
				WrapperMethods.checkallpageload();
				WebDriver driver = DriverFactory.getCurrentDriver();
				WrapperMethods.waitForPageLoaded(driver);
				driver.manage().window().maximize();
				WrapperMethods.preSteps();
				HomePageFunctions.searchValidation(currentURL);
			}

		} else if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			String currentURL = TestDataProvider.getData("Data_intl");
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
			WrapperMethods.checkallpageload();
			WebDriver driver = DriverFactory.getCurrentDriver();
			WrapperMethods.waitForPageLoaded(driver);
			driver.manage().window().maximize();
			WrapperMethods.preSteps();
			HomePageFunctions.searchValidation(currentURL);
		}

	}
	public void redirectbourdainpage() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		Reporter.log("<BR>The Bourdain page redirect validation starts</BR>");
		TvPagesFunctions.RedirectOldBourdainPage(TestDataProvider.getData("Data_redirect"));
		Reporter.log("<BR>The Bourdain page redirect validation Ends</BR>");
	}
	public void testOutbrainOb() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(currentURL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();

		HomePageFunctions.validateOutbrainOb(TestDataProvider.getData("value"), TestDataProvider.getData("datacut"));
	}

	public void testsectionandsubsection() throws IOException {

		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		UMReporter.log(LogStatus.INFO,"<b>Header navigation starts here</b>");
		MethodDef.brokenLinkValidation(SectionFrontsPage.getHeaderAllSections());
		UMReporter.log(LogStatus.INFO,"<b>Footer navigation starts here</b>");
		MethodDef.brokenLinkValidation(SectionFrontsPage.getFooterAllSections());
	}

	public void testWeatherCardpresent() {
		if (System.getProperty("environment").equals("REF")){
			String currentURL = TestDataProvider.getData("Data_REF");
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
			WrapperMethods.checkallpageload();
			WebDriver driver = DriverFactory.getCurrentDriver();
			WrapperMethods.waitForPageLoaded(driver);
			driver.manage().window().maximize();
			WrapperMethods.preSteps();
		}
		
		else{
			String currentURL = TestDataProvider.getData("Data");
			WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
			WrapperMethods.checkallpageload();
			WebDriver driver = DriverFactory.getCurrentDriver();
			WrapperMethods.waitForPageLoaded(driver);
			driver.manage().window().maximize();
			WrapperMethods.preSteps();
		}
		WrapperMethods.isDisplayed(HomePage.getFooterWeatherContent(),
				"The Weather card is preent as expected after checking the enableweather condition to be set true",
				"The Weather card is not present even when the enablefooter is set true - unexpected ");

	}
	
	public void replayValidation() {
		String currentURL = TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + currentURL);
		WrapperMethods.checkallpageload();
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.waitForPageLoaded(driver);
		driver.manage().window().maximize();
		WrapperMethods.preSteps();
		VideoLandingPageFunctions.replayValidation();
	}
}