package um.testng.test.pom.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class WeatherPage extends BasePage {

	static WebDriver driver = DriverFactory.getCurrentDriver();

	/**
	 * Waits for the page to finish loading before continuing.
	 */
	/*
	 * @Override protected void waitForLoad() { wait.until(new
	 * ExpectedCondition<WebElement>() {
	 * 
	 * @Override public static By apply(WebDriver d) { if (isElementExists(By
	 * .cssSelector(".zn-header__text-page_header"))) { Reporter.log(
	 * "PASSED: TV Page is Loaded."); } return d.findElement(By
	 * .cssSelector(".zn-header__text-page_header")); } }); }
	 */

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	/**
	 * Current Selected city
	 */
	public static By getCurrentLoc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text");
	}

	/**
	 * Current Selected city
	 */
	public static By getCurrentLocStat() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "zn-header__text");
	}

	/**
	 * Set Weather format to Farenheit
	 */
	public static By getFaren() {
		return WrapperMethods.locatorValue(Locators.CSS, "button[data-temp=\"fahrenheit\"]");
	}

	/**
	 * Set Weather format to Celsius
	 */
	public static By getCelsi() {
		return WrapperMethods.locatorValue(Locators.CSS, "button[data-temp=\"celsius\"]");
	}

	/**
	 * Get Forecast button
	 */
	public static By getForecast() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "weather__local-query__search-button");
	}

	/**
	 * Input Location City for Weather Information
	 */
	public static By getForecastsearch() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#weather__local-query-form > span.twitter-typeahead > input[name=\"weather-local-search\"]");
	}

	/**
	 * Forecast drop down in the Weather Page
	 */
	public static By getForecastdrop() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "tt-dataset-user-location");
	}

	/**
	 * Header: Weather Location
	 */
	public static By getheaderLoc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".location");
	}

	/**
	 * Header: Weather Temperature
	 */
	public static By getheaderTemp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-current > span");
	}

	/**
	 * Header: Weather Temperature Farenheit
	 */
	public static By getheaderTempF() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-current > span[data-temptype=\"fahrenheit\"]");
	}

	/**
	 * Header: Weather Temperature celsius
	 */
	public static By getheaderTempC() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-current > span[data-temptype=\"celsius\"]");
	}

	/**
	 * Convert Temperature to celsius Button
	 */
	public static By getButtonTempC() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"button.el-weather__local-query__temp-toggle-button[data-temp='celsius']");
	}

	/**
	 * Current Temperature in the Weather Page of the selected city
	 */
	public static By getCurrTemp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__conditions-temp--current span");
	}

	/**
	 * Current Temperature in the Weather Page of the selected city - Hi & Lo
	 */
	public static By getTempRange() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "weather__conditions-temp--range");
	}

	/**
	 * Current condition Title in the Weather Page
	 */
	public static By getCurrconditionTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__details h3");
	}

	/**
	 * Current Temperature in the Weather Page of the selected city - Detailed
	 * Information
	 */
	public static By getCurrDetails() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".weather__conditions--current > div:nth-child(1) > ul:nth-child(4)");
	}

	/**
	 * Extended Forecast Title in the Weather Page
	 */
	public static By getExtendedForecastTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cn--expandable h2");
	}

	/**
	 * Extended Forecast Day in the Weather Page
	 */
	public static By getExtForecastDay() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__conditions--future-extended h4");
	}

	/**
	 * Extended Forecast Temperature of the selected city in the Weather Page
	 */
	public static By getExtForecastTemp() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".weather__conditions--future-extended div.weather__conditions-temp");
	}

	/**
	 * Weather Icon of the selected city in the Weather Page
	 */
	public static By getWeatherIcon() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "weather__conditions-short-icon");
	}

	/**
	 * Set Default Button to set a city as default option
	 */
	public static By getDefaultButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.weather__stored-default:nth-child(1)");
	}

	/**
	 * list of Temperature in Farenheit format in the Weather Page
	 */
	public List<WebElement> getTempFarenheit(WebDriver d) {
		return d.findElements(By.cssSelector(".weather__conditions-temp span.js-temp"));
	}

	/**
	 * list of Temperature in celcius format in the Weather Page
	 */
	public List<WebElement> getTempCelsius(WebDriver d) {
		return d.findElements(By.cssSelector(".weather__conditions-temp span.js-temp"));
	}

	/**
	 * Weather Map Title in the Weather Page
	 */
	public static By getWeatherMapTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cn-grid--small h2");
	}

	/**
	 * Weather satellite Map in the Weather Page
	 */
	public static By getWeatherSatelliteMap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__localmap--satellite");
	}

	/**
	 * Weather temperature Map in the Weather Page
	 */
	public static By getWeatherTemperatureMap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__localmap--temperature");
	}

	/**
	 * Weather forecast Map in the Weather Page
	 */
	public static By getWeatherForecastMap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__localmap--forecast");
	}

	/**
	 * Weather Current Radar in the Weather Page
	 */
	public static By getWeatherCurrentRadar() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__localmap--radar");
	}

	/**
	 * Weather Satellite Map Image in the Weather Page
	 */
	public static WebElement getWeatherSatelliteMapSrc() {
		return driver.findElement(By.cssSelector(".weather__localmap--satellite img"));

	}

	/**
	 * Weather Temperature Map Image in the Weather Page
	 */
	public static WebElement getWeatherTemperatureMapSrc() {
		return driver.findElement(By.cssSelector(".weather__localmap--temperature img"));
	}

	/**
	 * Weather Forecast Map Image in the Weather Page
	 */
	public static WebElement getWeatherForecastMapSrc() {
		return driver.findElement(By.cssSelector(".weather__localmap--forecast img"));
	}

	/**
	 * Weather Current Radar Image in the Weather Page
	 */
	public static WebElement getWeatherCurrentRadarSrc() {
		return driver.findElement(By.cssSelector(".weather__localmap--radar img"));
	}

	public static By getWeatherRecentLocationBlock() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul/*[contains(.,'Recent Locations')]");
	}

	public static By getfarenheitbutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@data-temp='fahrenheit']");
	}

	public static By getcelsiousbutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@data-temp='celsius']");
	}

	public static By gettemperature() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='js-current weather__conditions-temp--current']/span[@class='js-temp']");
	}

	public static By getRecentLocationTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[contains(text(), 'Recent Locations')]");
	}

	public static By getRecentLocationLane() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//ul[contains(@class, 'cn cn-list-hierarchical-xs cn--idx-2 cn-container')]");
	}

	public static List<WebElement> getRecentLocationItems() {
		return driver.findElements(By.xpath("//div[@class='weather__stored-locations']/div[*]/h4"));
	}

	public static List<WebElement> getRecentLocationItem() {
		return driver.findElements(By.xpath("//div[@class='weather__stored-locations']/div[*]"));
	}

	public static By getEnteredLoc() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='zn-top ']//h2");
	}

	public static By getCurrentCondition() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__conditions weather__conditions--current']");
	}

	public static By getCurrentConditionCloudImg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__conditions weather__conditions--current']//div[@class='weather__conditions-short']/span[1]");
	}

	public static By getCurrentConditionCloudText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__conditions weather__conditions--current']//div[@class='weather__conditions-short']/span[2]");
	}

	public static By getCurrentConditioImg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__conditions weather__conditions--current']//div[@class='weather__conditions-short']");
	}

	public static List<WebElement> getExtForecastCloudTexts() {
		return driver
				.findElements(By.xpath("//ul[contains(@class,'cn cn-list-xs cn--idx-1 cn-container_')]/li[*]//h4"));
	}

	public static List<WebElement> getExtForecastCloudImgs() {
		return driver.findElements(By.xpath(
				"//ul[contains(@class,'cn cn-list-xs cn--idx-1 cn-container_')]/li[*]//div[@class='weather__conditions-short']"));
	}

	public static List<WebElement> getRecentLocImgs() {
		return driver.findElements(
				By.xpath("//div[@class='weather__stored-locations']/div[*]//div[@class='weather__conditions-short']"));
	}

	public static WebElement getCurrentLocation() {
		return driver.findElement(By.xpath("//div[@class='l-container zn-top__label']//h2"));
	}

	public static By weatherlogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='zn-header__text-page_header']");
	}

	public static List<WebElement> RecentLocationlist() {
		return driver.findElements(By.xpath("//div[@class='weather__stored-locations']/div"));
	}

	public static By getTopAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_bnr_atf_01");
	}

	public static List<WebElement> getTopAdv() {
		return driver.findElements(By.cssSelector("#ad_bnr_atf_01"));
	}

	public static By getRightRailAd1() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_rect_atf_02");
	}

	public static By getRightRailAd2() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_rect_btf_01");
	}

	public static By getBottomAd1() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_bnr_btf_02");
	}

	public static By getBottomAd2() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_bnr_btf_04");
	}

	public static By getBottomAd3() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_bnr_btf_01");
	}

	public static List<WebElement> getWeatherParameters() {
		return driver.findElements(By.cssSelector(".weather__details li"));
	}

	public static By getLocationTextBox() {
		return WrapperMethods.locatorValue(Locators.CSS, ".twitter-typeahead input");
	}

	public static By getWeatherPageCurrentTemp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-current.weather__conditions-temp--current");
	}

	public static By getWeatherPageFooterTemp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-temperature");
	}

	public static By getWeatherMapsSection() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section.zn-local-maps .l-container.zn__background--content-relative");
	}

	public static By getTextBox() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//input[@class='js-weather__local-query__search weather__local-query__search tt-input']");
	}

	public static List<WebElement> getTemCelsius() {
		return WrapperMethods.locateElements(Locators.CSS, ".weather__conditions-temp span.js-temp");
	}
}