package um.testng.test.pom.functions;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.BasePage;
import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class SectionFrontsPageFunctions {

	public static void checkSectionEntertainmentDropDown(By currentsection, String Section) {

		WrapperMethods.contains_Text_Attribute(currentsection, "href", Section, "Section name is correct",
				"Section name is not correct");

	}

	public static void checkSectionDropDown(By currentsectionDropdown, By currentSubsection) {

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			try {
				WrapperMethods.verifyElement(currentsectionDropdown, "DropDown Icon");
				WrapperMethods.clickJavaScript(currentsectionDropdown);

				MethodDef.explicitWaitVisibility(currentSubsection, 10, "Current subsection is present",
						"Current Subsection is not present");

				WrapperMethods.verifyElement(currentsectionDropdown, "DropDown Icon");
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error accessing the Drop Down Section");
			}
		}
	}

	public static void checkSectionContent(By sectionContent) {
		WrapperMethods.verifyElement(sectionContent, "Page Content");
	}

	public static void sectionsValidation(String secname, String baseurl, String[] domData) {
		int i = 0;

		for (String temp : domData) {

			String[] headItems = temp.split("~");

			if (!headItems[0].equals("Live TV \u2022") && !headItems[0].equals("Innovation Nation")
					&& !headItems[0].equals("Pressroom") && !headItems[0].equals("CNN Partner Hotels")
					&& !headItems[0].equals("Partner Hotels") && !headItems[0].equals("TV Schedule")
					&& !headItems[0].equals("HLN")) {

				UMReporter.log(LogStatus.INFO, "Element under test " + headItems[0]);

				String newurl = baseurl + headItems[1];
				MethodDef.navUrl(newurl);

				try {
					if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Perfecto")
							|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Appium")) {
						MethodDef.perfectoLoad();
						MethodDef.explicitWaitVisibility(BasePage.getSubSecPlusIcon(),
								"Sub section '+' icon is visible", "Sub section '+' icon is not visible");
						WrapperMethods.verifyElement(BasePage.getSubSecPlusIcon(), "SubSection Plus Icon");
						WrapperMethods.click(BasePage.getSubSecPlusIcon(), "Sub section '+' icon is clicked",
								"Sub section '+' icon is not clicked");
					}
				} catch (Exception E) {
					UMReporter.log(LogStatus.FAIL, "Error in accessing section dropdown " + headItems[0]);
				}

				try {
					MethodDef.explicitWaitVisibility(SectionFrontsPage.getCurrentsectionSelect(),
							"Current section Radio buton is visible", "Current section Radio buton is not visible");
					BasicSectionValidation(secname, BasePage.getCnnRedLogo(), SectionFrontsPage.getCurrentsection(),
							SectionFrontsPage.getCurrentsectionSelect(), BasePage.getNavColorStrip());
				} catch (Exception E) {
					UMReporter.log(LogStatus.FAIL, "Error in accessing section Elements for " + headItems[0]);
				}
				if (headItems[2].equals("image")) {
					try {
						SectionImageValidation(BasePage.getSubSectionImage());
					} catch (Exception E) {
						UMReporter.log(LogStatus.FAIL, "Error in accessing Image for section " + headItems[0]);
					}

				} else {
					int j = 0;
					try {
						if (headItems[0].trim().contains("Leadership")) {
							SectionTitleleadershipValidation(SectionFrontsPage.getrofileandleadershiptitle(),
									headItems[2]);
						} else {
							SectionTitleValidation(BasePage.getSubSectionAltTitle(),
									SectionFrontsPage.getSubSectionTitle(), headItems[2]);
						}
						j++;
					} catch (Exception E) {

					}

					if (j != 1) {
						UMReporter.log(LogStatus.FAIL, "SECTION TITLE IS NOT PRESENT");
					}
				}
			}
			i++;
		}
	}

	public static void BasicSectionValidation(String secname, By cnnRedLogo, By currentsection, By currentsectionSelect,
			By navcolorstrip) {
		// WebDriver driver = DriverFactory.getCurrentDriver();

		// String secname = driver.findElement(SecBy).getText();
		// Logo
		WrapperMethods.verifyElement(cnnRedLogo, "CNN logo");

		// Section Name
		WrapperMethods.contains_Text(currentsection, secname, "The Section name is displayed ",
				"The Section name is not displayed");

		// Sub section Selection
		WrapperMethods.verifyElement(currentsectionSelect, "The Current Section");

		WrapperMethods.verifyElement(navcolorstrip, "Color Nav Strip");

		WrapperMethods.verifyCssValue(navcolorstrip, "background-color", "rgba(204, 0, 0, 1)",
				"The Background color for color strip");
	}

	public static void SectionImageValidation(By subSectionImage) {
		WrapperMethods.verifyElement(subSectionImage, "The SubSection Image");
	}

	public static void SectionTitleValidation(By subSectionTitle1, By subSectionTitle2, String titleName) {

		WrapperMethods.contains_Text(subSectionTitle1, subSectionTitle2, titleName,
				"SubSection Text is correct and the text is", "SubSection Text is not correct");

	}

	public static void SectionTitleleadershipValidation(By subSectionTitle, String titleName) {
		WrapperMethods.contains_Text(subSectionTitle, titleName, "SubSection Text is correct ",
				"SubSection Text is not correct");

	}

	public static void basicFeedCardValidations() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Feed card Visibility</b>");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getFeedCard()).isDisplayed(),
					"Feed card is visible", "Feed card is not visible");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getFeedCardTitle()).isDisplayed(),
					"Feed card is visible:"
							+ WrapperMethods.getWebElement(SectionFrontsPage.getFeedCardTitle()).getText(),
					"Feed card is not visible");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Feed card ");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void FeedcardElements() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Feed card Elements</b>");
			for (WebElement elem : SectionFrontsPage.getFeedCardList()) {
				WrapperMethods.assertIsTrue(elem.isDisplayed(), "Feed card Item is visible",
						"Feed card Item is not visible");
				WrapperMethods.assertIsTrue(!elem.getText().isEmpty(), "Feed card text is visible" + elem.getText(),
						"Feed card text is not visible" + elem.getText());
			}

			for (WebElement imgelem : SectionFrontsPage.getFeedCardImg()) {
				WrapperMethods.assertIsTrue(imgelem.isDisplayed(), "Feed card Image is visible",
						"Feed card Image is not visible");
			}
			for (WebElement overlayelem : SectionFrontsPage.getFeedCardOverlay()) {
				WrapperMethods.assertIsTrue(overlayelem.isDisplayed(), "Overlay Image is visible",
						"Overlay Image is not visible");
			}
			int feedcount = SectionFrontsPage.getFeedCardList().size();
			int overlayimgcount = SectionFrontsPage.getFeedCardOverlay().size();
			WrapperMethods.assertIsTrue(feedcount == overlayimgcount,
					"the no.of images are equal" + " to no.of overlays" + overlayimgcount,
					"there is a mismatch in the images and the overlays" + overlayimgcount + feedcount);

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Feed card basic Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testContainerElements() {

		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Container Elements</b>");
			if (!WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedContainer()).isDisplayed()) {
				UMReporter.log(LogStatus.FAIL, "None of the containers are having the hyperlink");
			} else {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedContainer()).isDisplayed(),
						"the container is having the hyperlink", "the container is not having the hyperlink");
				WrapperMethods.assertIsTrue(
						!WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedContainer()).getAttribute("href")
								.isEmpty(),
						"the hyperlink of the container exists and it is :" + WrapperMethods
								.getWebElement(SectionFrontsPage.getHyperlinkedContainer()).getAttribute("href"),
						"this container doesnt have the hyperlink");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the container elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testZoneElements() {

		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Zone Elements</b>");
			if (!WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedZone()).isDisplayed()) {
				UMReporter.log(LogStatus.FAIL, "None of the Zone are having the hyperlink");
			} else {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedZone()).isDisplayed(),
						"the Zone is having the hyperlink", "the Zone is not having the hyperlink");
				WrapperMethods.assertIsTrue(
						!WrapperMethods.getWebElement(SectionFrontsPage.getHyperlinkedZoneHref()).getAttribute("href")
								.isEmpty(),
						"the hyperlink of the Zone exists and it is :" + WrapperMethods
								.getWebElement(SectionFrontsPage.getHyperlinkedZoneHref()).getAttribute("href"),
						"this Zone doesnt have the hyperlink");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Zone elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void editionpickersymbolcheck() {

		try {

			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
				UMReporter.log(LogStatus.INFO, "Click on Hamburger");
				WrapperMethods.getWebElement(SectionFrontsPage.getNavMenu()).click();

				WrapperMethods.verifyElement(SectionFrontsPage.getMobHeadereditionpicsymbol(), "The Plus sign ");

			} else {
				WrapperMethods.verifyElement(SectionFrontsPage.getheadereditionpicsymbol(), "The Plus sign ");

			}
		} catch (Exception e) {
		}
	}

	public static void testStyleLogoNavigation() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Social Elements</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getStyleLogo()).isDisplayed(),
					"the Style logo is displayed", "the Style logo is not displayed");

			WrapperMethods.click(SectionFrontsPage.getStyleLogo(), "Style Logo");

			SectionFrontsPage.wait(15);

			String title = DriverFactory.getCurrentDriver().getCurrentUrl();

			WrapperMethods.assertIsTrue(title.endsWith("/style"),
					"If we click the style logo the page is redirected to style home page",
					"If we click the style logo the page it is not redirected to style home page");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleCnnRedLogo()).isDisplayed(),
					"the Style logo is displayed", "the Style logo is not displayed");

			try {
				DriverFactory.getCurrentDriver().findElement(By.xpath("//button[@class='sp_message_dismiss']")).click();
			} catch (Exception e) {

			}
			WrapperMethods.click(SectionFrontsPage.getStyleCnnRedLogo(), "CNN Red Logo");
			SectionFrontsPage.wait(15);
			String title_home = DriverFactory.getCurrentDriver().getCurrentUrl();

			WrapperMethods.assertIsTrue((title_home.endsWith("cnn.com/") || title_home.endsWith("cnn.com")),
					"If we click the cnn logo the page is redirected to cnn home page",
					"If we click the cnn logo the page it is not redirected to cnn home page");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Style Logo navigation");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void footerTestingXpath(Map<String, List<String>> footerLinks) {
		Map<String, List<String>> map;
		map = footerLinks;
		MethodDef.splitHrefValidationXpath(map.entrySet());
	}

	public static void testStyleFooterSocialElements() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Page Footer Social Icons Validation</b>");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterSocialIcons()).isDisplayed(),
					"The social icons are displayed", "The social icons are not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterFbIcon()).isDisplayed(),
					"The social icon facebook is displayed", "The social icon facebook is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterFbIcon()).getAttribute("href")
							.contains("facebook"),
					"The social icon facebook href is correct " + WrapperMethods
							.getWebElement(SectionFrontsPage.getStyleFooterFbIcon()).getAttribute("href"),
					"The social icon facebook href is not correct");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterTwIcon()).isDisplayed(),
					"The social icon twitter is displayed", "The social icon twitter is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterTwIcon()).getAttribute("href")
							.contains("twitter"),
					"The social icon twitter href is correct " + WrapperMethods
							.getWebElement(SectionFrontsPage.getStyleFooterTwIcon()).getAttribute("href"),
					"The social icon twitter href is not correct");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterInstIcon()).isDisplayed(),
					"The social icon instagram is displayed", "The social icon instagram is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterInstIcon()).getAttribute("href")
							.contains("instagram"),
					"The social icon instagram href is correct " + WrapperMethods
							.getWebElement(SectionFrontsPage.getStyleFooterInstIcon()).getAttribute("href"),
					"The social icon instagram href is not correct");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Style Page Footer Social Icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testStyleFooterTopLinkElements() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Page Footer Top Link Validation</b>");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterTopLinkcons()).isDisplayed(),
					"The icons Top Link is displayed", "The icons Top Link is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleFooterTopLinkcons()).getAttribute("href")
							.contains("#"),
					"The icons Top Link href is correct " + WrapperMethods
							.getWebElement(SectionFrontsPage.getStyleFooterTopLinkcons()).getAttribute("href"),
					"The icons Top Link href is not correct");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Style Page Footer Top Link Icon");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void StyleOuterGallery() {
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Style Section Page</b>");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemImg()).isDisplayed(),
					"the Image is displayed for the gallery item", "the Image is displayed for the gallery item");
			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemImgHref()).getAttribute("href")
							.isEmpty(),
					"the Image href is displayed for the gallery item:  " + WrapperMethods
							.getWebElement(SectionFrontsPage.getGalleryItemImgHref()).getAttribute("href"),
					"the Image href is not displayed for the gallery item");

			UMReporter.log(LogStatus.INFO, "<b>Info: Style Outer Gallery Elements</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGallery()).isDisplayed(),
					"the Gallery is displayed", "the Gallery is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getStyleOuterGallery()).isDisplayed(),
					"the Image is displayed for the gallery item", "the Image is displayed for the gallery item");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(SectionFrontsPage.getOuterGalleryItemImgHref()).getAttribute("href")
							.isEmpty(),
					"the Image href is displayed for the gallery item:  " + WrapperMethods
							.getWebElement(SectionFrontsPage.getOuterGalleryItemImgHref()).getAttribute("href"),
					"the Image href is not displayed for the gallery item");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.getOuterGalleryItemContentHeadline()).isDisplayed(),
					"the Gallery's content headline is displayed : " + WrapperMethods
							.getWebElement(SectionFrontsPage.getOuterGalleryItemContentHeadline()).getText(),
					"the Gallery's content headline is not displayed");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(SectionFrontsPage.getOuterGalleryItemContentHeadlineHref())
							.getAttribute("href").isEmpty(),
					"the Gallery's content headline href is correct:    "
							+ WrapperMethods.getWebElement(SectionFrontsPage.getOuterGalleryItemContentHeadlineHref())
									.getAttribute("href"),
					"the Gallery's content headline href is not correct");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Style Outer  Gallery elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void tripAdvisor() {
		try {
			WrapperMethods.isDisplayed(SectionFrontsPage.getTripAdvisorBlock(), "Trip Advisor Block is present",
					"Trip Advisor Block is not present");
			WrapperMethods.isDisplayed(SectionFrontsPage.getPartnerHotelPlaceholderImage(),
					"Partner Hotel Widget image is present", "Partner Hotel Widget image is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Trip Advisor block");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void StyleAppiavalidation() {
		WrapperMethods.assertIsTrue(SectionFrontsPage.AppiaStylepageElement().size() == 0,
				"The Appia element is not found in style page as expected",
				"The appia is found in Style Page - UnExpected");
	}

	public static void carouselpaginationBGvalidation() {

		WrapperMethods.isDisplayed(SectionFrontsPage.carouselpagination(),
				"The Carousel Pagination is displayed as expected", "The Carousel Pagination is not displayed");

		if (DriverFactory.getCurrentDriver().toString().equalsIgnoreCase("chrome")) {
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.carouselpaginationBackground())
							.getCssValue("background-color").equalsIgnoreCase("rgba(38, 38, 38, 0.85098)"),
					"The Pagination background is as expected",
					"The Pagination background is not as expected"
							+ WrapperMethods.getWebElement(SectionFrontsPage.carouselpaginationBackground())
									.getCssValue("background-color"));
		} else {
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(SectionFrontsPage.carouselpaginationBackground())
							.getCssValue("background-color").equalsIgnoreCase("rgba(38, 38, 38, 0.85)"),
					"The Pagination background is as expected",
					"The Pagination background is not as expected"
							+ WrapperMethods.getWebElement(SectionFrontsPage.carouselpaginationBackground())
									.getCssValue("background-color"));
		}

	}

	public static void partnerhotelValidations() {
		try {
			WrapperMethods.isDisplayed(SectionFrontsPage.getTripAdvisorBlock(), "Partner Hotel Widget is present",
					"Partner Hotel Widget is not present");
			UMReporter.log(LogStatus.INFO, "PLACE HOLDER IMAGE BEFORE THE PARTNER HOTEL WIDGET LOADS");
			WrapperMethods.isDisplayed(SectionFrontsPage.getPartnerHotelPlaceholderImage(),
					"Partner Hotel Widget image is present", "Partner Hotel Widget image is not present");
			WrapperMethods.isDisplayed(SectionFrontsPage.getPartnerHotelPlaceholderImage(),
					"The Partner Hotel Widget image has the valid src and the src is : "
							+ WrapperMethods.getAttribute(SectionFrontsPage.getPartnerHotelPlaceholderImage(), "src"),
					"The Partner Hotel Widget logo doesnot have the valid src");
			MethodDef.brokenLinkValidation(
					WrapperMethods.getAttribute(SectionFrontsPage.getPartnerHotelPlaceholderImage(), "src"));
			/*
			 * try { WrapperMethods.moveToElement(SectionFrontsPage.
			 * getPartnerHotelPlaceholderImage()); WrapperMethods.scrollDown();
			 * WrapperMethods.scrollUp();
			 * WrapperMethods.isDisplayed(SectionFrontsPage.getPartnerHotelImage
			 * (), "Partner Hotel Widget image is present",
			 * "Partner Hotel Widget image is not present");
			 * WrapperMethods.isDisplayed(SectionFrontsPage.getPartnerHotelImage
			 * (),
			 * "The Partner Hotel Widget image has the valid src and the src is : "
			 * + WrapperMethods.getAttribute(SectionFrontsPage.
			 * getPartnerHotelImage(), "src"),
			 * "The Partner Hotel Widget logo doesnot have the valid src");
			 * MethodDef.brokenLinkValidation(
			 * WrapperMethods.getAttribute(SectionFrontsPage.
			 * getPartnerHotelImage(), "src"));
			 * WrapperMethods.isDisplayed(SectionFrontsPage.
			 * getPartnerHotelTripAdvisor(),
			 * "Partner Hotel Widget image is present",
			 * "Partner Hotel Widget image is not present"); } catch (Exception
			 * e) { }
			 */
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Trip Advisor block");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}


	public static void adElements() throws InterruptedException {
		Thread.sleep(20000);

		int footerad = 0;
		int zonead = 0;
		int headerad = 0;
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				// header Ad
				for (WebElement elem : SectionFrontsPage.topBannerAd()) {
					if (elem.isDisplayed())
						headerad++;
				}
				if (headerad == 0) {
					UMReporter.log(LogStatus.WARNING, "No header ad is displayed");
					// error.addError("No header ad is displayed");
				} else
					UMReporter.log(LogStatus.PASS, "Header Ad is displayed");

				// Zone Ad
				List<WebElement> zoneadelem = null;
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
					zoneadelem = SectionFrontsPage.zoneAdDesktop();
				else if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet"))
					zoneadelem = SectionFrontsPage.zoneAdTab();
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile"))
					zoneadelem = SectionFrontsPage.zoneAdMob();
				for (WebElement elem : zoneadelem) {
					if (elem.isDisplayed())
						zonead++;
				}
				if (zonead == 0) {
					UMReporter.log(LogStatus.WARNING, "No zone ad is displayed");

					// error.addError("No zone ad is displayed");
				} else
					UMReporter.log(LogStatus.PASS, "zone Ad is displayed: Number of Zone ads:");

				// Footer Ad
				for (WebElement elem : SectionFrontsPage.footerBannerAd()) {
					if (elem.isDisplayed())
						footerad++;
				}
				if (footerad == 0) {
					UMReporter.log(LogStatus.WARNING, "No footer ad is displayed");
					// error.addError("No footer ad is displayed");
				} else
					UMReporter.log(LogStatus.PASS, "Footer Ad is displayed");
			} catch (Exception E) {
				UMReporter.log(LogStatus.ERROR, E.getMessage());
			}
		}
	}

	public static void testAppiaAds() {

		UMReporter.log(LogStatus.INFO, "APPIA and Yield Mo ADS  in Edition");

		try {
			WrapperMethods.verifyElement(SectionFrontsPage.getAppiaAD(), "Appia Ad ");

			WrapperMethods.verifyCssValue(SectionFrontsPage.getAppiaAD(), "padding-bottom", "20px",
					"The padding for the appia ad container");

			WrapperMethods.verifyCssValue(SectionFrontsPage.getAppiaAD(), "padding-top", "20px",
					"The padding for the appia ad container ");

			WrapperMethods.verifyCssValue(SectionFrontsPage.getAppiaAD(), "text-align", "center",
					"appia ad container text is aligned and ");
			try {
				WrapperMethods.verifyElement(SectionFrontsPage.getYieldMoAd(), "YieldMo Ad ");
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "Yield Mo Ads are not configured");

			}

		} catch (Exception e) {
			WrapperMethods.verifyElement(SectionFrontsPage.getYieldMoAd(), "YieldMo Ad ");

		}
	}

	public static void intlQuancastTag() {
		try {
			UMReporter.log(LogStatus.INFO, "Quantcast tag in Edition");
			JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getCurrentDriver();
			String Text = (String) executor.executeScript("return document.getElementsByTagName('html')[0].innerHTML");

			WrapperMethods.assertIsTrue(Text.contains("quantserve.com/aquant.js"),
					"Quantcast is Present in page source", "Quantcast is not Present in page source");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing  Quantcast tag");
		}

	}

	public static void domQuancastTag() {
		try {
			UMReporter.log(LogStatus.INFO, "Quantcast tag in Edition");
			JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getCurrentDriver();
			String Text = (String) executor.executeScript("return document.getElementsByTagName('html')[0].innerHTML");

			WrapperMethods.assertIsTrue(!Text.contains("quantserve.com/aquant.js"),
					"Quantcast tag will not be Present in page source for Domestic site",
					"Quantcast is Present in page source for Domestic site");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing  Quantcast tag");
		}

	}

}