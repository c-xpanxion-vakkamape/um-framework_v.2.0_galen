package um.testng.test.pom.functions;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.TvPages;
import um.testng.test.pom.elements.VideoLandingPage;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class VideoLeafPageFunctions {

	public void testVideoleafgpage() {

		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			WrapperMethods.videoObjectCheck();

			CommomPageFunctions.basicLandingPageElements();
		} else
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
	}

	public void testarticleemvidpage() {
		// Creating custom Page instances
		WebDriver driver = DriverFactory.getCurrentDriver();

		// Page Load

		WebDriverWait wait = new WebDriverWait(driver, 15);

		CommomPageFunctions.clickReadMore();

		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}

		try {
			MethodDef.explicitWaitClickable(VideoLeafPage.getEmVideoElement(), "Video embedded element is cicked",
					"Video embedded element is not cicked");
			UMReporter.log(LogStatus.PASS, "Video element is Present");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Video element is not Present");
		}

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			MethodDef.explicitWaitClickable(VideoLeafPage.getEmVideoElementPlayElemOB(),
					"Video embedded element Play is cicked", "Video embedded element Play is not cicked");
			WrapperMethods.click(VideoLeafPage.getEmVideoElementPlayElemOB(),"play element");

		}

		WrapperMethods.videoObjectCheck();

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				WrapperMethods.moveToElement(VideoLeafPage.getEmVideoElementPlayClose());
				WrapperMethods.scrollUp();
				MethodDef.explicitWaitClickable(VideoLeafPage.getEmVideoElementPlayClose(),
						"Video embedded Play/close is cicked", "Video embedded element Play/close is not cicked");
				WrapperMethods.click(VideoLeafPage.getEmVideoElementPlayClose(), "Video embedded Play/close");
				UMReporter.log(LogStatus.PASS, "Close Link in the Embedded Video is Clickable");
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Close Link in the Embedded Video is not Clickable");
			}
		}
	}

	public static void TwShareValidations() {
		try {
			clickTwShare();
			twShareValidations();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	private static void clickTwShare() {
		WrapperMethods.Explicit_Wait(VideoLeafPage.getTWShare(),5,"Waited for Twitter share icon","Twitter share icon until 5 seconds");
		WrapperMethods.click(VideoLeafPage.getTWShare(),"Clicked Twitter share icon","Not able to click the Twitter share icon");
	}
	public static void twShareValidations() {
		try {
			ArticlePage.wait(10);
			WebDriver go =  DriverFactory.getCurrentDriver();
			String parentHandle = go.getWindowHandle();
			int twCount = 0;
			for (String handle : go.getWindowHandles()) {
				go.switchTo().window(handle);
				UMReporter.log(LogStatus.INFO,"<B>INSIDE WINDOW: " + go.getTitle() + "</B>");
				if (go.getTitle().toString().contains("Twitter")) {
					UMReporter.log(LogStatus.INFO,"<b>PASSED Twitter Page is opened</b>");
					try {
						WrapperMethods.assertIsTrue(VideoLeafPage.getTWContent().contains("cnn"),
								"CNN Text is not present in Twitter share", "CNN Text is present in Twitter share");
						WrapperMethods.assertIsTrue(VideoLeafPage.getTWContent().contains("http://"),
								"URL is not present in Twitter share", "URL is present in Twitter Share");
						go.close();
						// article.wait(2);
						UMReporter.log(LogStatus.PASS,"<b>PASSED: Twitter Share/Element is loading correctly</b>");
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL,"Twitter Share/Element is not loading correctly" );
						UMReporter.log(LogStatus.INFO, e.getMessage());
					}
					twCount++;
				}
			}
			go.switchTo().window(parentHandle);
			if (twCount == 0) {
				UMReporter.log(LogStatus.FAIL,"Issue in accessing Twitter Share window");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL,"Error accessing FB share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	public static void fbShareValidations() {
		try {
			VideoLeafPage.wait(20);
			clickfbShare();
			FBShareValidations();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	private static void clickfbShare() {
		WrapperMethods.Explicit_Wait(VideoLeafPage.getFBShare(),8,"Waited for Facebook share icon","Facebook share icon until 8 seconds");
		WrapperMethods.click(VideoLeafPage.getFBShare(),"Clicked Facebook share icon","Not able to click the Facebook share icon");
	}
	public static void FBShareValidations() {
		try {
			ArticlePage.wait(10);
			WebDriver go =  DriverFactory.getCurrentDriver();
			String parentHandle = go.getWindowHandle();
			int fbCount = 0;
			for (String handle : go.getWindowHandles()) {
				go.switchTo().window(handle);
				UMReporter.log(LogStatus.INFO,"<B>INSIDE WINDOW: " + go.getTitle() + "</B>");
				if (go.getTitle().toString().contains("Facebook")) {
					UMReporter.log(LogStatus.PASS,"PASSED Facebook Page is opened");
					WrapperMethods.Explicit_Wait(ArticlePage.getFbEmail(), 15,"Waited for Email icon visibility","Not able to visible to Email for 15 seconds");
					WrapperMethods.click(ArticlePage.getFbEmail(), "Clicked the Email username text box", "Not able to Click the Email usernametext box");
					WrapperMethods.clear(ArticlePage.getFbEmail());
					WrapperMethods.sendkeys(ArticlePage.getFbEmail(), "turnercnn1@gmail.com", "Entered the userid-turnercnn1@gmail.com", "Not able to enter the user id");

					
					WrapperMethods.click(ArticlePage.getFbPass(), "Clicked the Email password text box", "Not able to Click the Email password text box");
					WrapperMethods.clear(ArticlePage.getFbPass());
					WrapperMethods.sendkeys(ArticlePage.getFbPass(), "fbookcnn1", "Entered the Password", "Not able to enter the Password");

					ArticlePage.wait(2);
					
					WrapperMethods.click(ArticlePage.getFbLogin(),"Clicked the Facebook Login button","Not able to Click the Facebook Login button");
					WrapperMethods.Explicit_Wait(ArticlePage.getFbShareElem(), 20,"Waited untill FB share element is present","Not able to visible the FB share element ");
					MethodDef.passLog("Facebook Share/Element is loading correctly");
					WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.getFbTitle()).getAttribute("value").endsWith("/videos"),
							"FB share Title Loaded", "FB share Title not loaded");
					fbCount++;
				}
			}
			go.switchTo().window(parentHandle);
			if (fbCount == 0) {
				UMReporter.log(LogStatus.FAIL, "Issue in accessing FB Share window");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	public static void zoneGradientCheck() {
		try {
			int i = 0;
			for (WebElement ZoneTopGradient : VideoLeafPage.getZoneTopGradient()) {
				i++;
				WrapperMethods.assertIsTrue( ZoneTopGradient.getCssValue("min-height").equals("55px"),
						"The Gradient on top of the zone" + i + " is present and having a min-height of 55px",
						"The Gradient on top of the zone" + i + " is not having a min-height of 55px");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Zone Gradient Check");
			UMReporter.log(LogStatus.INFO,e.getMessage());
		}
	}
	
	public static void VideoLandingPageLazyLoadZonesValidation() {

		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {

			UMReporter.log(LogStatus.INFO,"<b> Verify the zone details enabled for 800 under videos/index.html </b>");
			String Zone1 = MethodDef.getJsonVideoLandingZones( "800");
			String Zone_800[] = Zone1.split(",");
			UMReporter.log(LogStatus.INFO,"<b>Zone Details Enabled for 800 are: </b>");
			for (int i = 0; i < Zone_800.length; i++) {

				UMReporter.log(LogStatus.PASS,(i + 1) + ". " + Zone_800[i]+"<br>");
			}
			UMReporter.log(LogStatus.INFO,"<b> The list of zones present in the Desktop are:  </b>");
			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.PASS,zone.getAttribute("id") + "<br>");
			}
			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_800.length,
					"The number of zones present in the Desktop equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Desktop not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());

		} else if (ConfigProvider.getConfig("Platform").equals("Tab")) {
			UMReporter.log(LogStatus.INFO,"<b> Verify the zone details enabled for 640 under videos/index.html </b>");
			String Zone2 = MethodDef.getJsonVideoLandingZones( "640");
			String Zone_640[] = Zone2.split(",");
			UMReporter.log(LogStatus.INFO,"<br> Zone Details Enabled for 640 are: <br>");

			for (int i = 0; i < Zone_640.length; i++) {
				UMReporter.log(LogStatus.PASS,(i + 1) + ". " + Zone_640[i] + "<br>");
			}
			UMReporter.log(LogStatus.INFO,"<b>The list of zones present in the Tab are:  <br>");
			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.PASS,zone.getAttribute("id") + "<br>");
			}
			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_640.length,
					"The number of zones present in the Tab equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Tab not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());

		} else {
			UMReporter.log(LogStatus.INFO,"<b> Verify the zone details enabled for 0 under videos/index.html </b>");
			String Zone3 = MethodDef.getJsonVideoLandingZones( "0");
			String Zone_0[] = Zone3.split(",");
			UMReporter.log(LogStatus.INFO,"<b> Zone Details Enabled for 0 are: </b>");

			for (int i = 0; i < Zone_0.length; i++) {

				UMReporter.log(LogStatus.PASS,(i + 1) + ". " + Zone_0[i] + "<br>");
			}
			UMReporter.log(LogStatus.INFO,"<b> The list of zones present in the Mobile are:  </b>");
			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.INFO,zone.getAttribute("id") + "<br>");
			}
			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_0.length,
					"The number of zones present in the Mobile equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Mobile not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());
		}
	}
	public static void validateEmbeddedbutton() {

		try
		{
			WrapperMethods.clickJavaScript(VideoLeafPage.getVideoEmbed());
			System.out.println("first click");
			
			MethodDef.explicitWaitVisibility(VideoLeafPage.embeddedtextbox(),"Embedded Text Box is visible","Embedded Text Box is not visible");
					
//			WebDriverWait waitt = new WebDriverWait(DriverFactory.getCurrentDriver(), 5);
//		waitt.until(ExpectedConditions.visibilityOf(leaf.embeddedtextbox(go)));
		}
		catch(Exception e)
		{
			WrapperMethods.click(VideoLeafPage.getVideoEmbed(),"Video Embedded");
			System.out.println("second click");
		}
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.embeddedtextbox()).getAttribute("value")
						.contains("iframe"),
				"Iframe element is available in the text value",
				"Iframe tag is not available in the text value");
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.embeddedtextbox()).getAttribute("value")
						.contains("width"),
				"width attribute is available in the text value",
				"width attribute is not available in the text value");
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.embeddedtextbox()).getAttribute("value")
						.contains("height"),
				"height attribute is available in the text value",
				"height attribute is not available in the text value");
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.embeddedtextbox()).getAttribute("value").contains("416"),
				"width attribute value is 416 in the text value",
				"width attribute value is not as expected in the text value");
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.embeddedtextbox()).getAttribute("value").contains("234"),
				"height attribute value  is 234 in the text value",
				"height attribute value  is not as expected in the text value");
	}	
	public static void vrPageValidations() throws IOException {
		WrapperMethods.isDisplayed(VideoLeafPage.getVRPageCopyrights(), "Copy Right msg is displayed",
				"Copy Right msg is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getVRPageCopyrightsTB().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getLegalCopyRight(), "Legal Font Copy Right msg is displayed",
				"Legal Font Copy Right msg is not displayed");

		WrapperMethods.isDisplayed(VideoLeafPage.getTos(), "Terms of services msg is displayed",
				"Terms of services msg is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getTos().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getPrivGuid(), "Privacy Guidelines msg is displayed",
				"Privacy Guidelines msg is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getPrivGuid().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getHomeLink(), "CNN Home link is displayed",
				"CNN Home link is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getHomeLinkCNN().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getShowMoreVRVid(), "Get Show More VRVideo is displayed",
				"Get Show More VRVideo is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getShowMoreVRVid().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getMoreVRVid(), "More VRVideo Header link is displayed",
				"More VRVideo Header link is not displayed");
		MethodDef.brokenLinkValidation(VideoLeafPage.getMoreVRVid().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getZoneone(), "Zone 1 is displayed in CNN VR Page",
				"Zone 1 is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getZoneoneHeadline(), "Headline is displayed in Zone 1",
				"Headline is not displayed in Zone 1");
		MethodDef.brokenLinkValidation(VideoLeafPage.getZoneoneHeadline().toString());

		WrapperMethods.assertIsTrue(VideoLeafPage.getMoreVRVideos().size() >= 0,
				"Totally " + VideoLeafPage.getMoreVRVideos().size() + " videos are displayed in grid.",
				"Videos are not displayed under More VR Videos section.");

		for (int i = 0; i < VideoLeafPage.getImages().size(); i++) {
			WrapperMethods.isDispWE(VideoLeafPage.getImages().get(i),
					"No:" + (i + 1) + " - image is displayed in grid.", "No:" + (i + 1) + " - image is not displayed!");
			MethodDef.brokenLinkValidation(VideoLeafPage.getImagesLink().get(i).toString());

			WrapperMethods.isDispWE(VideoLeafPage.getImagesHeadlineLink().get(i),
					"Headline under No:" + (i + 1) + " image is displayed in grid.",
					"Headline under No:" + (i + 1) + " image is not displayed in grid.");
			MethodDef.brokenLinkValidation(VideoLeafPage.getImagesHeadlineLink().get(i).toString());
			Reporter.log("<br></br>");
		}

		try {
			WrapperMethods.click(VideoLeafPage.getZoneoneHeadline(),"ZoneOneHeadline is clicked","Not able to click the ZoneOneHeadline.");
			UMReporter.log(LogStatus.PASS,"Clicked the Headline in Zone 1");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL,"Not able to click the Headline in Zone-1");
		}

		WrapperMethods.isDisplayed(VideoLeafPage.getVRVideo(), "Fave-vr-360-video is displayed in Article page",
				"Fave-vr-360-video is not displayed in Article page");
	}
	
	public static void VRZones2to4Validations() throws IOException {

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone2(), "Zone 2 is displayed in CNN VR Page",
				"Zone 2 is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone3(), "Zone 3 is displayed in CNN VR Page",
				"Zone 3 is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone4(), "Zone 4 is displayed in CNN VR Page",
				"Zone 4 is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone2Headline(), "Headline is displayed in Zone 2",
				"Headline is not displayed in Zone 2");
		MethodDef.brokenLinkValidation(VideoLeafPage.getVRZone2Headline().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone3Headline(), "Headline is displayed in Zone 3",
				"Headline is not displayed in Zone 3");
		MethodDef.brokenLinkValidation(VideoLeafPage.getVRZone3Headline().toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone4Headline(), "Headline is displayed in Zone 4",
				"Headline is not displayed in Zone 4");
		MethodDef.brokenLinkValidation(VideoLeafPage.getVRZone4Headline( ).toString());

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone2BackgroundImage( ),
				"Zone 2 Background Image is displayed in CNN VR Page",
				"Zone 2 Background Image is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone3BackgroundImage( ),
				"Zone 3 Background Image is displayed in CNN VR Page",
				"Zone 3 Background Image is not displayed in CNN VR Page");

		WrapperMethods.isDisplayed(VideoLeafPage.getVRZone4BackgroundImage( ),
				"Zone 4 Background Image is displayed in CNN VR Page",
				"Zone 4 Background Image is not displayed in CNN VR Page");

		WrapperMethods.assertIsTrue(VideoLeafPage.getVRPageAds().size() == 0,
				"Advertisements are not displayed in CNN VR Page",
				"Advertisements are displayed in CNN VR Page - Unexpected Check Manually");
	}
	public static void basicVideoValidations() {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHead(), "Leaf Page title");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoShowName(), "Show Name ");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoSource(), "Source Element is not empty",
					"Source Element is empty");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoSourceUrl(), "Source Name ");
			WrapperMethods.textAttributeNotEmpty(VideoLeafPage.getVideoSourceUrl(), "href",
					"Source Name is hyperlinked");

			MethodDef.explicitWaitVisibility(VideoLeafPage.getVideoEmbed(), 30, "Video Embedded element is visible",
					"Video Embedded element is not visible");

			WrapperMethods.verifyElement(VideoLeafPage.getVideoEmbed(), "Embed element ");

		} else {
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHeadM(), "Leaf Page title");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoSourceM(), "Source Element ");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoSourceUrlM(), "Source Name ");
			WrapperMethods.textAttributeNotEmpty(VideoLeafPage.getVideoSourceUrlM(), "href",
					"Source Name  hyperlinked");
		}

	}

	public static void videoleafvideo() {
		UMReporter.log(LogStatus.INFO, "Testing On video Title in Navigation Bar After scrolling");

		WrapperMethods.page_scrollDown();
		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.verifyElement(VideoLeafPage.getArticletitle(),
					"the title in the navigation bar after scrolling and the title is ");
		} else {
			WrapperMethods.verifyElement(VideoLeafPage.getarticletitle(),
					"the title in the navigation bar after scrolling and the title is");
		}

		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Deskop")) {
			WrapperMethods.click(VideoLeafPage.getVideoPlayElementBy(), "Video Play Element");
		}
		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			basicVideoElemValidations();
			if (System.getProperty("deviceType").equals("Tab")) {
				WrapperMethods.click(VideoLeafPage.vidPlayButton(), "Video Play button");
			}
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoCollNowPlay(), "Now Playing text is not empty",
					"Now Playing text is empty");
		}
	}

	public static void basicVideoElemValidations() {

		/*
		 * if (!System.getProperty("deviceType").equals("Deskop")) {
		 * WrapperDef.click(leaf.getVideoPlayElement(go)); }
		 */
		WrapperMethods.textNotEmpty(VideoLeafPage.getVideoCollTitle(), "Video Collection has title",
				"Video Collection has no title");
		try {
			Pattern p = Pattern.compile("[. (][\\d]+[ ][V][i][d][e][o][s][)]");
			Matcher m = p.matcher(WrapperMethods.getWebElement(VideoLeafPage.getVideoCollTitle()).getText());
			if (m.find()) {
				UMReporter.log(LogStatus.PASS, "Total Videos is listed in the text" + m.group());

			} else {
				UMReporter.log(LogStatus.PASS, "Total Videos is not listed in the text");
			}
		} catch (Exception e) {
			MethodDef.failLog(e.getMessage(), "Error in accessing number of videos text");
		}

		WrapperMethods.verifyElement(VideoLeafPage.getVideoCollCarousel(), "Video Carousel ");

		WrapperMethods.verifyElement(VideoLeafPage.getVideoCollPrevCarousel(), "Prev Owl Carousel ");

		WrapperMethods.verifyElement(VideoLeafPage.getVideoCollNextCarousel(), "Next Owl Carousel ");

		WrapperMethods.verifyElement(VideoLeafPage.getVideoCollTotalTime(), "Total Time ");

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.textAttributeNotEmpty(VideoLeafPage.getVideoCollTotalTime(), "data-post-text", "Total Time");
			WrapperMethods.verifyElement(VideoLeafPage.getVideoCollVideoElem(), "Video element ");
		}
		WrapperMethods.textAttributeNotEmpty(VideoLeafPage.getVideoCollImgSrc(), "src", "Img Src");
		MethodDef.brokenLinkValidation(
				WrapperMethods.getWebElement(VideoLeafPage.getVideoCollImgSrc()).getAttribute("src"));

		WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHeadLines(), "Vid Headline ");

		WrapperMethods.verifyElement(VideoLeafPage.getVideoPlayElement(), "Video element ");

	}

	public static void basicVideoElemDynamicContent() {

		String title = WrapperMethods.getWebElement(VideoLeafPage.getVideoHead()).getText();

		String desc = WrapperMethods.getWebElement(VideoLeafPage.getVideoDescription()).getText();
		int count = 0;
		List<WebElement> elems = null;

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
			elems = VideoLeafPage.getVideoCollVideoElements();
		else
			elems = VideoLeafPage.getVideoCollVideoElemDevice();

		for (WebElement elem : elems) {
			if (count > 0) {
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
					WrapperMethods.click(elem, "Video Collection Element is clicked");
					WrapperMethods.clickJavaScript(elem);
				} else
					WrapperMethods.click(elem, "Video Collection Element is clicked");

				// WrapperDef.waitForPageLoaded(go);

				// leaf.wait(20);

				basicVideoElemValidations();

				WrapperMethods.assertIsTrue(
						!WrapperMethods.getWebElement(VideoLeafPage.getVideoHead()).getText().equals(title),
						"Title changed" + "<br>" + WrapperMethods.getWebElement(VideoLeafPage.getVideoHead()).getText()
								+ " ||Previously|| " + title,
						"Title Didn't change" + WrapperMethods.getWebElement(VideoLeafPage.getVideoHead()).getText());

				WrapperMethods.assertIsTrue(
						!WrapperMethods.getWebElement(VideoLeafPage.getVideoDescription()).getText().equals(desc),
						"Description changed" + "<br>"
								+ WrapperMethods.getWebElement(VideoLeafPage.getVideoDescription()).getText()
								+ " ||Previously|| " + desc,
						"Description Didn't change"
								+ WrapperMethods.getWebElement(VideoLeafPage.getVideoDescription()).getText());
				break;
			}
			count++;
		}
	}

	public static void testVideoLeafPlayerPrevNav() {
		// Creating custom Page instances
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
					|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
				WrapperMethods.clickJavaScript(VideoLeafPage.getVideoCollNextCarousel());
				basicVideoElemValidations();
			} else {
				UMReporter.log(LogStatus.INFO, "In Devices Next/Prev link wont displayed");

				WrapperMethods.verifyElement(VideoLeafPage.getMoreBtn(), "See More Button");

				WrapperMethods.verifyElement(VideoLeafPage.getMoreTxt(), "See More Text in Button ");
				int totalVid = VideoLeafPage.getVideosCarousel().size();
				try {
					WrapperMethods.clickJavaScript(VideoLeafPage.getMoreTxt());
				} catch (Exception e) {
					WrapperMethods.getWebElement(VideoLeafPage.getMoreTxt()).click();
				}
				// leafpage.wait(5);
				WrapperMethods.page_scrollDown();
				WrapperMethods.assertIsTrue((VideoLeafPage.getVideosCarousel().size()) > totalVid,
						"After clicked the see more button videos showing more in carousel, before-" + totalVid
								+ " Now -" + VideoLeafPage.getVideosCarousel().size(),
						"After clicked the see more button videos are not showing more in carouselbefore-" + totalVid
								+ " Now -" + VideoLeafPage.getVideosCarousel().size());
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.INFO, "Error in accessing Leaf Page element");
		}
	}

	public static void testVideoLeafPlayerNextNav() {
		// Creating custom Page instances
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.clickJavaScript(VideoLeafPage.getVideoCollPrevCarousel());
				basicVideoElemValidations();
			} else {
				UMReporter.log(LogStatus.INFO, "In Devices Next/Prev link wont displayed");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Leaf Page element");

		}
	}

	public static void testShowLeafValidations() {
		// Creating custom Page instances

		Reporter.log("<br>INFO: Show leafPage Elements</br>");
		WrapperMethods.verifyElement(TvPages.getShowLogo(), "the show logo");
		WrapperMethods.verifyElement(TvPages.getShowFbIcon(), "the facebook social icon");
		WrapperMethods.contains_Text_Attribute(TvPages.getShowFbIconHref(), "href", "facebook",
				"the facebook social icon has href", "the facebook social icon doesnot have the href");

		WrapperMethods.verifyElement(TvPages.getShowTwIcon(), "the twitter social icon ");
		WrapperMethods.contains_Text_Attribute(TvPages.getShowTwIconHref(), "href", "twitter",
				"the twitter social icon has href", "the twitter social icon doesnot have the href");
	}

	public static void testVideoLeafOutbrain() {

		UMReporter.log(LogStatus.INFO, "Testing with Video Leaf Page Outbrain Elements");

		testOutbrain();

	}

	public static void testOutbrain() {

		WrapperMethods.verifyElement(ArticlePage.getPaidContent(), "Paid Content Container in the Video Leaf Page");

		WrapperMethods.verifyElement(ArticlePage.getRecommendedForYou(),
				"Recommended For You Container in the Video Leaf Page");

		WrapperMethods.assertIsTrue(!WrapperMethods.getWebElements(ArticlePage.getPaidContentHeadlines()).isEmpty(),
				"Paid Content Headlines are displayed", "Paid Content Headlines are not displayed");

		WrapperMethods.assertIsTrue(
				!WrapperMethods.getWebElements(ArticlePage.getRecommendedForYouHeadlines()).isEmpty(),
				"Recommended For You Headlines are displayed", "Recommended For You Headlines are not displayed");

		WrapperMethods.verifyElement(

				ArticlePage.getMoreFromCNNOutbrain(), "Recommended by Outbrain link under the Paid Content");
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			UMReporter.log(LogStatus.INFO, "Click on Recommended by Outbrain link");
			WrapperMethods.clickJavaScript(ArticlePage.getMoreFromCNNOutbrain());
			// leaf.wait(25);
			WrapperMethods.switchToFrame("ob_iframe_modal");

			WrapperMethods.verifyElement(ArticlePage.getOutbrainLogoLeaf(), "Outbrain dialogue box ");
			UMReporter.log(LogStatus.INFO, "Click on Close button of Outbrain dialogue box");

			WrapperMethods.switchToDefaultContent();

			if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
					|| ConfigProvider.getConfig("Browser").equalsIgnoreCase("FIREFOX")) {

				WrapperMethods.clickJavaScript(ArticlePage.getOutbrainCloseButton());

			} else {
				WrapperMethods.moveToElementClick(ArticlePage.getOutbrainCloseButtonTab());
			}
			// leaf.wait(5);
			WrapperMethods.verifyElement(ArticlePage.getMoreFromCNNOutbrain(), "Outbrain dialogue box ");
		}

	}

	public static void videoZone1AD() {

		// leafpage.wait(10);
		Boolean read = MethodDef.getJsonVideoUnificationFlag();
		if (read) {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.verifyElement(VideoLeafPage.getZone1DesktopAD(), "AD in the 1st Zone");
			} else if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
				WrapperMethods.verifyElement(VideoLeafPage.getZone1TabAD(), "AD in the 1st Zone");
			} else {
				UMReporter.log(LogStatus.INFO,
						"AD will not be displayed for MOBILES and TABS of Screen Size lesser than 800");
			}
		} else {
			UMReporter.log(LogStatus.WARNING, "Enable Unification Video Flag is set as FALSE");
		}

	}

	public static void basicVideoPlayIcon() {
		try {

			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
				basicVideoPlayIconChk(true);
			else
				basicVideoPlayIconChk(false);
			UMReporter.log(LogStatus.INFO, "Clicking on the 3rd Video");
			int count = 0;
			List<WebElement> elems = null;
			if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile"))
				elems = VideoLeafPage.getVideoCollVideoElements();
			else
				elems = VideoLeafPage.getVideoCollVideoElemDevice();
			for (WebElement elem : elems) {
				if (count == 3) {
					if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
						// WrapperDef.click(elem);
						WrapperMethods.clickJavaScript(elem);
						break;
					} else
						count++;
					continue;
				}
			}
			WrapperMethods.waitForPageLoaded();

			// leaf.wait(10);

			// if (!System.getProperty("deviceType").equals("Mob")) {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				// WrapperDef.click(leaf.getVideoCollPrevCarousel(go));
			} else {
				try {
					WrapperMethods.click(VideoLeafPage.vidPlayButtonM(), "Video Play Button");
				} catch (Exception e) {
					WrapperMethods.click(VideoLeafPage.vidPlayButton(), "Video Play Button");
				}
				// leaf.wait(5);
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
					WrapperMethods.click(VideoLeafPage.getVideoCollPrevCarousel(), "Previous carousel");
				}
			}
			basicVideoPlayIconChk(true);

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Video Elements");
		}
	}

	public static void basicVideoPlayIconChk(boolean b) {

		try {
			int imglen = 0;
			int playIcon = 0;
			if (ConfigProvider.getConfig("Platform").equals("Mobile")) {
				playIcon = VideoLeafPage.getVideoCollTotalTimesMob().size();
				imglen = VideoLeafPage.getVideoCollImgSrcsMob().size();
				try {
					WrapperMethods.click(VideoLeafPage.vidPlayButtonM(), "Video Play button");
				} catch (Exception e) {
					WrapperMethods.click(VideoLeafPage.vidPlayButton(), "Video Play button");
				}
			} else {
				imglen = VideoLeafPage.getVideoCollImgSrcs().size();
				playIcon = VideoLeafPage.getVideoCollTotalTimes().size();
			}
			int nowPlay = VideoLeafPage.getVideoCollNowPlays().size();
			if (b) {
				if (nowPlay == 1)
					UMReporter.log(LogStatus.PASS, "Now play icon is present correctly");
				else {
					UMReporter.log(LogStatus.FAIL, "Now play icon is present multiple/ maybe missing");

				}
			}

			boolean t;
			if (ConfigProvider.getConfig("Platform").equals("Mobile")) {
				t = (imglen > 1) && (imglen == playIcon);
			} else {
				t = (imglen > 1) && (imglen == playIcon + 1);
			}
			if (nowPlay == 0 && !ConfigProvider.getConfig("Platform").equals("Mobile"))
				t = imglen > 1 && imglen == playIcon;
			if (t)
				UMReporter.log(LogStatus.PASS, "Play Icons are present in the carousel items");
			else {
				UMReporter.log(LogStatus.FAIL, "Play Icons are wrong/ maybe missing");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Video Elements");
		}
	}

	public static void testVideoShareMeta() {
		Boolean unficVid = MethodDef.getJsonVideoUnificationFlag();

		if (unficVid) {

			try {
				// Thread.sleep(5000);
				if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
					try {
						WrapperMethods.click(VideoLeafPage.vidPlayButtonM(), "Video Play button is clicked");
					} catch (Exception e) {
						WrapperMethods.click(VideoLeafPage.vidPlayButton(), "Video Play button is clicked");
					}
				}
				MethodDef.explicitWaitVisibility(VideoLeafPage.getCurrentVideoElem(), 60,
						"The Video element is visible", "The Video element is not visible");

				try {
					if (WrapperMethods.getWebElement(VideoLeafPage.getVidLeftShare()).isDisplayed()) {
						if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
							UMReporter.log(LogStatus.PASS, "Left rail Share icon is present");
						} else {
							UMReporter.log(LogStatus.FAIL, "Left rail Share icon is present");
						}

					} else {
						if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
							UMReporter.log(LogStatus.FAIL, "Left rail Share icon is present");
						} else {
							UMReporter.log(LogStatus.PASS, "Left rail Share icon is not present");
						}
					}
				} catch (Exception E) {
					UMReporter.log(LogStatus.PASS, "Left rail Share icon is not present");
				}

				WrapperMethods.contains_Text_Attribute(VideoLeafPage.getVidShare(), "data-link",
						WrapperMethods.getWebElement(VideoLeafPage.getCurrentVideoElem())
								.getAttribute("data-vr-contentbox"),
						"CORRECT Metadata is present in video Share", "Wrong Metadata is present in video Share");

			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error Accessing Video Elements");

			}
		} else {
			UMReporter.log(LogStatus.WARNING, "Video Unification Code is not Enabled for this Environment");
		}
	}



	public static void VideoLeafPageLazyLoadZonesValidation() {

		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {

			UMReporter.log(LogStatus.INFO, "Verify the zone details enabled for 800 under videos/index.html");
			String Zone1 = MethodDef.getJsonVideoLeafZones("800");
			String Zone_800[] = Zone1.split(",");
			UMReporter.log(LogStatus.INFO, "Zone Details Enabled for 800 are:");
			for (int i = 0; i < Zone_800.length; i++) {

				UMReporter.log(LogStatus.INFO, (i + 1) + ". " + Zone_800[i]);
			}
			Reporter.log("<br> The list of zones present in the Desktop are:  <br>");
			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.INFO, zone.getAttribute("id"));

			}

			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_800.length,
					"The number of zones present in the Desktop equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Desktop not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());

		} else if (ConfigProvider.getConfig("Platform").equals("Tab")) {
			UMReporter.log(LogStatus.INFO, "Verify the zone details enabled for 640 under videos/index.html");

			String Zone2 = MethodDef.getJsonVideoLeafZones("640");
			String Zone_640[] = Zone2.split(",");
			UMReporter.log(LogStatus.INFO, "Zone Details Enabled for 640 are:");

			for (int i = 0; i < Zone_640.length; i++) {
				UMReporter.log(LogStatus.INFO, (i + 1) + ". " + Zone_640[i]);
			}
			UMReporter.log(LogStatus.INFO, "The list of zones present in the Tab are:");

			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.INFO, zone.getAttribute("id"));

			}
			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_640.length,
					"The number of zones present in the Tab equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Tab not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());

		} else {
			UMReporter.log(LogStatus.INFO, "Verify the zone details enabled for 0 under videos/index.html");

			String Zone3 = MethodDef.getJsonVideoLeafZones("0");
			String Zone_0[] = Zone3.split(",");
			UMReporter.log(LogStatus.INFO, "Zone Details Enabled for 0 are:");

			for (int i = 0; i < Zone_0.length; i++) {
				UMReporter.log(LogStatus.INFO, (i + 1) + ". " + Zone_0[i]);
			}
			Reporter.log("<br> The list of zones present in the Mobile are:  <br>");
			for (WebElement zone : VideoLeafPage.getZones()) {
				UMReporter.log(LogStatus.INFO, zone.getAttribute("id"));

			}
			WrapperMethods.assertIsTrue(VideoLeafPage.getZones().size() == Zone_0.length,
					"The number of zones present in the Mobile equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size(),
					"The number of zones present in the Mobile not equals the number of zones specified in Json for 640 under videos/index.html: "
							+ VideoLeafPage.getZones().size());
		}

	}

	public static void valiDesc() {

		WrapperMethods.verifyElement(VideoLeafPage.getPreloadad(), "PreLoad Adv ");
		// leaf.wait(20);
		WrapperMethods.verifyElement(VideoLeafPage.getVideoCollNextCarousel(), "Next Owl Carousel ");
		WrapperMethods.verifyElement(VideoLeafPage.gettitle(), "Title is displayed ");
		String title = WrapperMethods.getWebElement(VideoLeafPage.gettitle()).getText();
		if (ConfigProvider.getConfig("deviceType").equalsIgnoreCase("Desktop")) {
			Dimension dimension = new Dimension(1030, 600);
			DriverFactory.getCurrentDriver().manage().window().setSize(dimension);
			;
			// leaf.wait(5);
			try {
				WrapperMethods.click(VideoLeafPage.getsecvideo(), "Section video");
			} catch (Exception e) {
				WrapperMethods.clickJavaScript(VideoLeafPage.getsecvideo());
			}
		} else {
			Dimension dimension = new Dimension(320, 350);
			DriverFactory.getCurrentDriver().manage().window().setSize(dimension);

			try {
				WrapperMethods.click(VideoLeafPage.getsecvideoDev(), "Section video");
			} catch (Exception e) {
				WrapperMethods.clickJavaScript(VideoLeafPage.getsecvideoDev());
			}
		}
		// leaf.wait(20);
		WrapperMethods.verify_Text(VideoLeafPage.gettitle(), title,
				"After resizing the window and clicking the next video- Title name is changed");
	}

	public static void checkarticlevideogallerypage() {
		WrapperMethods.assertIsTrue(ArticlePage.articleamphtmlink().size() > 0,
				"The Page has  html tag link rel=amphtml", "The Page does not html tag link rel=amphtml");

	}

	public static void Video360Carousel() {
		int i = 0;
		for (WebElement Carouselcard : VideoLeafPage.getVideo360Carousel()) {
			i++;
			WrapperMethods.verifyElement(Carouselcard, "Carousel Card " + i + "Has the 360 icon embedded");
		}

		WrapperMethods.click(VideoLandingPage.getVideoCarouselNext(), "Carousel Next");

		WrapperMethods.click(VideoLandingPage.getVideoCarouselNext(), "Carousel Next");

		for (WebElement Carouselcard : VideoLeafPage.getVideo360Carousel()) {
			i++;
			WrapperMethods.verifyElement(Carouselcard, "Carousel Card " + i + "Has the 360 icon embedded");
		}

	}

	public static void testBranding() {
		try {
			Reporter.log("Info: Special page Branding");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBranding(),
					"the branding for the video page is displayed", "the branding for the video page is not displayed");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBrandingImg(),
					"the branding for the video is displayed", "the branding for the video is not displayed");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBrandingLogo(),
					"the branding logo for the video is displayed", "the branding logo for the video is not displayed");
			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(VideoLeafPage.getVideoBrandingLink()).getAttribute("href").isEmpty(),
					"the branding link for the video is displayed and the href is:  "
							+ WrapperMethods.getWebElement(VideoLeafPage.getVideoBrandingLink()).getAttribute("href"),
					"the branding link for the video is not displayed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the video page branding element");
			UMReporter.log(LogStatus.FAIL,E.getMessage());
		}
	}

}
