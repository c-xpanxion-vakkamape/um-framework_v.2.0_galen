package um.testng.test.pom.functions;

import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import com.relevantcodes.extentreports.LogStatus;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.sportsPage;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class sportsPageFunctions {

	public void testsportpage() {

		try {
			WrapperMethods.moveToElementClick(sportsPage.getAdelement());
			UMReporter.log(LogStatus.INFO, "Ad was present and Skip button was pressed");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Ad was not displayed");
		}

		try {
			WrapperMethods.verifyElement(sportsPage.getBleachers(), "Bleacher report");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Bleachers Report page is not loaded " + E.getMessage());
		}

	}

	public static void sportsTickerURLValidations() {
		try {
			UMReporter.log(LogStatus.INFO, "Sports Ticker Elements Validations");
			WrapperMethods.contains_Text_Attribute(sportsPage.getSportTickerSection(), "href", "?cid=sportsticker",
					"Sports Ticker section name URL should amend with ?cid=sportsticker and the URL is :  ",
					"Sports Ticker section name URL is not amended with ?cid=sportsticker");

			WrapperMethods.contains_Text_Attribute(sportsPage.getSportTickerArticle(), "href", "?cid=sportsticker",
					"Sports Ticker article name URL should amend with ?cid=sportsticker and the URL is : ",
					"Sports Ticker article name URL is not amended with ?cid=sportsticker");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Sports tweet elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void sportsTickerURL() {

		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Sports Ticker Link Open in the same Tab");
			ArrayList<String> parentAttributes_article = (ArrayList<String>) ((JavascriptExecutor) driver)
					.executeScript(
							"var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;",
							sportsPage.getSportTickerArticle());
			int c = 0;
			for (String a : parentAttributes_article) {
				if (a.startsWith("target")) {
					c++;
					UMReporter.log(LogStatus.FAIL, "The Article Page opens in different tab");
				}
			}
			if (c == 0)
				UMReporter.log(LogStatus.PASS, "The Article Page opens in same tab");
			ArrayList<String> parentAttributes_section = (ArrayList<String>) ((JavascriptExecutor) driver)
					.executeScript(
							"var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;",
							sportsPage.getSportTickerSection());
			for (String a : parentAttributes_section) {
				if (a.startsWith("target")) {
					c++;
					UMReporter.log(LogStatus.FAIL, "The Section Page opens in different tab");
				}
			}
			if (c == 0)
				UMReporter.log(LogStatus.PASS, "The Section Page opens in same tab");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Sports tweet elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}
}
