package um.testng.test.pom.functions;

import java.io.IOException;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltests.BaseTestMethods;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.pom.elements.BasePage;
import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.HomePage;
import um.testng.test.pom.elements.MoneyPage;
import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.galen.GalenTestBase;
import um.testng.test.utilities.galen.GalenTestBase.TestDevice;

public class ArticlePageFunctions extends GalleryPageFunctions {

	public void testArticlePages() {

		WrapperMethods.verifyElement(ArticlePage.getPageLoad(), "CNN logo");

		WrapperMethods.verifyElement(ArticlePage.getArticleHeaderTitle(), "Article Page Head Line");

		WrapperMethods.contains_Text(ArticlePage.getArticleHeaderTitle(), "Article Header", "", "");

		WrapperMethods.verifyElement(ArticlePage.getArticleCont(), "Article Container");

		WrapperMethods.verifyElement(ArticlePage.getNavColorStrip(), "Color Nav Strip");

		try {
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WrapperMethods.verifyElement(ArticlePage.getAuthorImage(), "author image ");
				WrapperMethods.textAttributeNotEmpty(ArticlePage.getAuthorImageSrc(), "src", "Author Image");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "The author Image is not Present - Check Manually");
		}

	}

	public void testbrokenpageurl() {

		try {

			MethodDef.explicitWaitVisibility(ArticlePage.getAllPageLoad(),
					"broken Page is loaded with logo as expected", "broken Page is not loaded with logo");

			MethodDef.explicitWaitPresenceCss(ArticlePage.brokenPageErrorheadline(), "Error Headline is present",
					"Error headline is not present");
			WrapperMethods.contains_Text(ArticlePage.brokenPageErrorheadline(), "Uh-oh!",
					"The Error Headline 1 is present as expected", "The Error Headline 2 is not present as expected");
			WrapperMethods.contains_Text(ArticlePage.brokenPageErrorheadline2(),
					"It could be you, or it could be us, but there's no page here.",
					"The Error Headline 1 is present as expected", "The Error Headline 2 is present as expected");
			UMReporter.log(LogStatus.PASS, "Broken Page loaded with correct elements");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Broken Page loaded with correct elements " + e.getMessage());
		}

	}

	public void testentpagesection(ITestContext ctx) {
		/*
		 * 
		 * try { closeTermsOfService(section.getTermsServiceConsentClose(),
		 * wf.go()); } catch (Exception E) {
		 * 
		 * }
		 * 
		 * try { WrapperMethods.moveToElementClick(sportsPage.getAdelement());
		 * UMReporter.log(LogStatus.INFO,
		 * "Ad was present and Skip button was pressed"); } catch (Exception e)
		 * { UMReporter.log(LogStatus.INFO, "Ad was not displayed"); }
		 * 
		 * if (!url.contains("money")) {
		 * wait.until(ExpectedConditions.visibilityOf(section.
		 * getEntertainmentlogo()));
		 * 
		 * SectionPageFunctions.checkSectionEntertainmentDropDown(wf.go(),
		 * error, section.getEntertainmentlogo(), "entertainment");
		 * 
		 * SectionPageFunctions.checkSectionDropDown(wf.go(), error,
		 * section.getmoreentertainmentpage(),
		 * section.entertainmentsubsection(), windowWidth);
		 * 
		 * SectionPageFunctions.checkSectionContent(wf.go(), error,
		 * section.getSectionContent(), windowWidth); } else {
		 * WrapperDef.assertIsTrue(error, section.getmoneylogo().isDisplayed(),
		 * "The Money page is loaded succesfully",
		 * "The Money page is not loaded succesfully"); }
		 * 
		 * } else {
		 * wait.until(ExpectedConditions.visibilityOf(section.getCurrentsection(
		 * )));
		 * 
		 * SectionPageFunctions.checkSectionDropDown(wf.go(), error,
		 * section.getCurrentsection(), "Entertainment");
		 * 
		 * SectionPageFunctions.checkSectionDropDown(wf.go(), error,
		 * section.getCurrentsectionDropdown(), section.getCurrentSubsection(),
		 * windowWidth);
		 * 
		 * SectionPageFunctions.checkSectionContent(wf.go(), error,
		 * section.getSectionContent(), windowWidth);
		 * 
		 * MethodDef.resultLog(error);
		 */}

	public void testarticlevidpage(String browser, String machineType, String edition) {
		BaseTestMethods.preSteps();
		WrapperMethods.videoObjectCheck();
	}

	public void testmorepage() {

		MethodDef.explicitWaitVisibility(ArticlePage.getCurrentsection(), 15, "Current Section is visible",
				"Current Section is not visible");

		WrapperMethods.containsText(ArticlePage.getCurrentsection(), "More�", "The Section drop down is present",
				"The Section drop down is not present");
		CommomPageFunctions.checkSectionDropDown(ArticlePage.getCurrentsectionDropdown(),
				ArticlePage.getCurrentSubsection());

	}

	public void testentpagesection(String url) {

		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {
		}

		if (!url.contains("money")) {
			MethodDef.explicitWaitVisibility(SectionFrontsPage.getEntertainmentlogo(), 20,
					"Entertainment logo is present", "Entertainment logo is not present");

			SectionFrontsPageFunctions.checkSectionEntertainmentDropDown(SectionFrontsPage.getEntertainmentlogo(),
					"entertainment");

			SectionFrontsPageFunctions.checkSectionDropDown(SectionFrontsPage.getmoreentertainmentpage(),
					SectionFrontsPage.entertainmentsubsection());

			SectionFrontsPageFunctions.checkSectionContent(SectionFrontsPage.getSectionContent());
		} else {
			WrapperMethods.verifyElement(MoneyPage.getmoneylogo(), "Money page");
		}

	}

	public void StaticSkinnyNav(String url) {

		WrapperMethods.verifyElement(ArticlePage.getstaticPageLoad(), "Static page logo");

	}

	public static void fbShareValidations(ITestContext ctx) {
		try {
			clickfbShare();
			fbShareValidations();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());

		}
	}

	private static void clickfbShare() throws InterruptedException {
		MethodDef.explicitWaitVisibility(ArticlePage.getFBShareDesktop(), 15, "FB Share Icon is loaded",
				"FB Share Icon is not loaded");
		WrapperMethods.click(ArticlePage.getFBShareDesktop(), "FB Share Icon");
		Thread.sleep(2000);
	}

	public static void fbShareValidations() {
		try {

			WebDriver driver = DriverFactory.getCurrentDriver();

			ArticlePage.wait(10);
			String parentHandle = driver.getWindowHandle();
			int fbCount = 0;
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
				UMReporter.log(LogStatus.INFO, "<BR>INSIDE WINDOW: " + driver.getTitle() + "<BR>");
				if (driver.getTitle().toString().contains("Facebook")) {
					UMReporter.log(LogStatus.INFO, "<br>PASSED Facebook Page is opened</br>");
					MethodDef.explicitWaitVisibility(ArticlePage.getFbEmail(), 15, "FB Email is loaded",
							"FB Email is not loaded");
					WrapperMethods.getWebElement(ArticlePage.getFbEmail()).click();
					WrapperMethods.getWebElement(ArticlePage.getFbEmail()).clear();
					WrapperMethods.getWebElement(ArticlePage.getFbEmail()).sendKeys("turnercnn1@gmail.com");
					WrapperMethods.getWebElement(ArticlePage.getFbPass()).click();
					WrapperMethods.getWebElement(ArticlePage.getFbPass()).clear();
					ArticlePage.wait(2);
					WrapperMethods.getWebElement(ArticlePage.getFbPass()).sendKeys("fbookcnn1");
					WrapperMethods.getWebElement(ArticlePage.getFbLogin()).click();
					MethodDef.explicitWaitVisibility(ArticlePage.getFbShareElem(), 20, "FB Share Element is loaded",
							"FB Share Element is loaded");
					UMReporter.log(LogStatus.PASS, "Facebook Share/Element is loading correctly");
					WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.getFbTitle())
							.getAttribute("value").endsWith("/videos"), "FB share Title Loaded",
							"FB share Title not loaded");
					fbCount++;
				}
			}
			driver.switchTo().window(parentHandle);
			if (fbCount == 0) {
				UMReporter.log(LogStatus.FAIL, "Issue in accessing FB Share window");

			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing FB share icons");

		}
	}

	public static void TwShareValidations() {
		try {
			clickTwShare();
			twShareValidations();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	private static void clickTwShare() {
		MethodDef.explicitWaitVisibility(ArticlePage.getTwShareDesktop(), 5, "Twitter Share Icon is loaded",
				"Twitter Share Icon is not loaded");
		WrapperMethods.click(ArticlePage.getTwShareDesktop(), "Twitter Share Icon");
	}

	public static void twShareValidations() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			ArticlePage.wait(10);
			String parentHandle = driver.getWindowHandle();
			int twCount = 0;
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
				UMReporter.log(LogStatus.INFO, "<BR>INSIDE WINDOW: " + driver.getTitle() + "<BR>");
				if (driver.getTitle().toString().contains("Twitter")) {
					UMReporter.log(LogStatus.INFO, "<br>PASSED Twitter Page is opened</br>");
					try {
						WrapperMethods.contains_Text(ArticlePage.getTwContent(), "cnn",
								"CNN Text is present in Twitter share", "CNN Text is not present in Twitter share");
						WrapperMethods.contains_Text(ArticlePage.getTwContent(), "http://",
								"URL is present in Twitter Share", "URL is not present in Twitter share");
						driver.close();
						// article.wait(2);
						UMReporter.log(LogStatus.PASS, "<br>PASSED: Twitter Share/Element is loading correctly</br>");
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL,
								"<br>Failed: Twitter Share/Element is not loading correctly</br>" + e.getMessage());

					}
					twCount++;
				}
			}
			driver.switchTo().window(parentHandle);
			if (twCount == 0) {
				UMReporter.log(LogStatus.FAIL, "Issue in accessing Twitter Share window");

			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing FB share icons");
			UMReporter.log(LogStatus.INFO, E.getMessage());

		}
	}

	public static void testVRheaderfont() {

		WrapperMethods.isDisplayed(ArticlePage.headline(), "The Headline is displayed",
				"The Headline is not displayed");

		WrapperMethods.verifyCssValue(ArticlePage.headline(), "font-family", "DIN Condensed", "Arabic Picker URL ");
	}

	public static void SocialShareValidation(String brow) {
		WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 15);
		ArticlePage.wait(10);
		WrapperMethods.click(ArticlePage.getGigyaBarElem2(), "Clicked Facebook icon in the gigya bar",
				"Not able to click Facebook icon in the gigya bar");
		ArticlePage.wait(10);
		int fbCount = 0;
		if (!brow.equalsIgnoreCase("SAFARI") && !ConfigProvider.getConfig("Platform").equals("PERFECTO")) {
			for (String handle : DriverFactory.getCurrentDriver().getWindowHandles()) {
				DriverFactory.getCurrentDriver().switchTo().window(handle);
				UMReporter.log(LogStatus.INFO,
						"<BR>INSIDE WINDOW: " + DriverFactory.getCurrentDriver().getTitle() + "<BR>");
				if (DriverFactory.getCurrentDriver().getTitle().toString().contains("Facebook")) {
					UMReporter.log(LogStatus.PASS, "<br>PASSED Facebook Page is opened</br>");
					ArticlePage.wait(5);
					try {
						try {
							wait.until(ExpectedConditions
									.visibilityOf(WrapperMethods.getWebElement(ArticlePage.getFbEmail())));
							WrapperMethods.click(ArticlePage.getFbEmail(), "Clicked Email textbox",
									"Not able to click Email textbox");
							WrapperMethods.clear(ArticlePage.getFbEmail());

							WrapperMethods.sendkeys(ArticlePage.getFbEmail(), "turnercnn1@gmail.com",
									"Entered the email address ", "Not able to enter the email address ");

							WrapperMethods.click(ArticlePage.getFbPass(), "Clicked Password textbox",
									"Not able to click the password textbox");
							WrapperMethods.clear(ArticlePage.getFbPass());
							ArticlePage.wait(2);

							WrapperMethods.sendkeys(ArticlePage.getFbPass(), "fbookcnn1", "Entered the password ",
									"Not able to enter the password ");
							WrapperMethods.click(ArticlePage.getFbLogin(), "Clicked the Login button",
									"Not able to click the Login button");

							WrapperMethods.isDisplayed(ArticlePage.imageChk(), "Image is displayed",
									"Image is not displayed");

							WrapperMethods.contains_Text_Attribute(ArticlePage.imageChk(), "src", "jpg",
									"JPG file is present", "JPG file is not present");
						} catch (Exception E) {

						}
						System.out.println("end");
						wait.until(ExpectedConditions
								.visibilityOf(WrapperMethods.getWebElement(ArticlePage.getFbShareElem())));
						// DriverFactory.getCurrentDriver().close();
						ArticlePage.wait(2);
						Reporter.log("<br>PASSED: Facebook Share/Element is loading correctly</br>");
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL,
								"<br>Failed: Facebook Share/Element is not loading correctly</br>" + e.getMessage());
					}
					fbCount++;
				}
			}
			if (fbCount == 0) {
				UMReporter.log(LogStatus.FAIL, "<br>Failed: Issue in accessing FB share window</br>");
			}
		}
	}

	public static void validateLivefyreHeading() {
		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreHeading(go), "Live Fyre heading is displayed",
					"Live Fyre heading is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("color")
							.equals("rgba(64, 64, 64, 1)"),
					"The color of the heading is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("color"),
					"The color of the heading is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-size")
							.equals("36px"),
					"The font size of the heading is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-size"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-weight"),
					"The font weight of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("line-height")
							.equals("40px"),
					"The font size of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("line-height"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-family"),
					"The font family of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreHeading(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments : heading ");
		}

	}

	public static void validateLivefyreParagraph() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			WrapperMethods.scrollIntoViewByElement(By.cssSelector(".s-element-content p"));
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreParagraph(go), "Live Fyre heading is displayed",
					"Live Fyre heading is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("color")
							.equals("rgba(64, 64, 64, 1)"),
					"The color of the heading is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("color"),
					"The color of the heading is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-size"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-weight"),
					"The font weight of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("line-height")
							.equals("30px"),
					"The font size of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("line-height"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-family"),
					"The font family of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreParagraph(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments : paragraph");
		}
	}

	public static void validateLivefyreItalicHeading() {
		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]//h2/i"));
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreItalicHeading(go), "Live Fyre heading is displayed",
					"Live Fyre heading is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("color")
							.equals("rgba(64, 64, 64, 1)"),
					"The color of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("color"),
					"The color of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-size")
							.equals("36px"),
					"The font size of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-size"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style")
							.equals("italic"),
					"The font style of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style"),
					"The font style of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("line-height")
							.equals("40px"),
					"The line-height of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("line-height"),
					"The line-height of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-family"),
					"The font family of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments : Italic heading ");
		}

	}

	public static void validateLivefyreItalicParagraph() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreItalicParagraph(go),
					"Live Fyre Italic Paragraph is displayed", "Live Fyre Italic Paragraph is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("color"),
					"The color of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-size"),
					"The font size of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style")
							.equals("italic"),
					"The font style of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style"),
					"The font style of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicHeading(go)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-weight"),
					"The font weight of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("line-height")
							.equals("30px"),
					"The line-height of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("line-height"),
					"The line-height of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-family"),
					"The font family of the heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreItalicParagraph(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Italic paragraph");
		}

	}

	public static void validateLiveBlogOrderedList() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live blog:");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreOrderedList(go), "The Live blog Ordered List is visible",
					"The live blog Ordered List is not visible");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the Ordered List is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("color"),
					"The color of the Ordered List  is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the Ordered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-size"),
					"The font size of the Ordered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-style")
							.equals("normal"),
					"The font style of the Ordered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-style"),
					"The font style of the Ordered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the Ordered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-weight"),
					"The font weight of the Ordered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("line-height")
							.equals("30px"),
					"The line-height of the Ordered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("line-height"),
					"The line-height of the caption is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-family")
							.contains("sans-serif"),
					"The font family of the Ordered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-family"),
					"The font family of the Ordered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreOrderedList(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Ordered List");
		}
	}

	public static void validateLiveBlogUnOrderedList() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live blog:UnOrdered List");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreUnOrderedList(go),
					"The Live blog UnOrdered List is visible", "The live blog UnOrdered List is not visible");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("color"),
					"The color of the UnOrdered List  is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-size"),
					"The font size of the UnOrdered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-style")
							.equals("normal"),
					"The font style of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-style"),
					"The font style of the UnOrdered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-weight"),
					"The font weight of the UnOrdered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("line-height")
							.equals("30px"),
					"The line-height of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("line-height"),
					"The line-height of the UnOrdered list is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-family")
							.contains("sans-serif"),
					"The font family of the UnOrdered List is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-family"),
					"The font family of the UnOrdered List is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreUnOrderedList(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :UnOrdered List");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void validateLivefyreStrikedHeading() {
		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live blog: Striked HEading");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreStrikedHeading(go),
					"Live Fyre Striked Heading is displayed", "Live Fyre Striked Heading is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("color")
							.equals("rgba(64, 64, 64, 1)"),
					"The color of the Striked Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("color"),
					"The color of the Striked Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-size")
							.equals("36px"),
					"The font size of the Striked Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-size"),
					"The font size of the Striked Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-style")
							.equals("normal"),
					"The font style of the Striked Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-style"),
					"The font style of the Striked Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-style"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("line-height")
							.equals("40px"),
					"The line-height of the Striked Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("line-height"),
					"The line-height of the Striked Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the Striked Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-family"),
					"The font family of the Striked Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedHeading(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments : Striked  heading ");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}

	}

	public static void validateLivefyreStrikedParagraph() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live blog:Striked PAragraph");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreStrikedParagraph(go),
					"Live Fyre Striked Paragraph is displayed", "Live Fyre Striked Paragraph is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("color"),
					"The color of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-size"),
					"The font size of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-style")
							.equals("normal"),
					"The font style of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-style"),
					"The font style of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-weight"),
					"The font weight of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("line-height")
							.equals("30px"),
					"The line-height of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("line-height"),
					"The line-height of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the Striked Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-family"),
					"The font family of the Striked Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreStrikedParagraph(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Striked paragraph");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void validateLivefyreBoldHeading() {
		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			WrapperMethods.scrollIntoViewByElement(By.xpath("//div[contains(@class,'s-element-content')]//h2/b"));

			Reporter.log("<br>Info: Testing with Live blog: Bold HEading");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreBoldHeading(go), "Live Fyre Bold Heading is displayed",
					"Live Fyre Bold Heading is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("color")
							.equals("rgba(64, 64, 64, 1)"),
					"The color of the Bold Heading is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("color"),
					"The color of the Bold Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-size")
							.equals("36px"),
					"The font size of the Bold Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-size"),
					"The font size of the Bold Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the Bold Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-weight"),
					"The font weight of the Bold Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("line-height")
							.equals("40px"),
					"The line-height of the Bold Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("line-height"),
					"The line-height of the Bold Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the Bold Heading is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-family"),
					"The font family of the Bold Heading is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldHeading(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments : Bold  heading");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void validateLivefyreBoldParagraph() {

		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live : Bold PAragraph");
			WrapperMethods.scrollIntoViewByElement(
					By.xpath("//div[contains(@class,'s-element-content')]//p/b[contains(text(),'Bold Text')]"));

			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreBoldParagraph(go),
					"Live Fyre Bold Paragraph is displayed", "Live Fyre Bold Paragraph is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the Bold Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("color"),
					"The color of the Bold Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-size")
							.equals("18px"),
					"The font size of the Bold Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-size"),
					"The font size of the Bold Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-weight")
							.equals("bold"),
					"The font weight of the Bold Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-weight"),
					"The font weight of the Bold Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("line-height")
							.equals("30px"),
					"The line-height of the Bold Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("line-height"),
					"The line-height of the Bold Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the Bold Paragraph is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-family"),
					"The font family of the Bold Paragraph is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreBoldParagraph(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Bold paragraph");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void validateLiveBlogQuotedText() {

		try {

			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Info: Testing with Live blog:QuotedText");
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreQuotedText(go), "The Live blog QuotedText is visible",
					"The live blog QuotedText is not visible");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("color")
							.equals("rgba(38, 38, 38, 1)"),
					"The color of the QuotedText is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("color"),
					"The color of the QuotedText  is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-size")
							.equals("20px"),
					"The font size of the QuotedText is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-size"),
					"The font size of the QuotedText is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-style")
							.equals("italic"),
					"The font style of the QuotedText is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-style"),
					"The font style of the QuotedText is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the QuotedText is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-weight"),
					"The font weight of the QuotedText is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("line-height")
							.equals("26px"),
					"The line-height of the QuotedText is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("line-height"),
					"The line-height of the QuotedText is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-family")
							.contains("sans-serif"),
					"The font family of the QuotedText is : " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-family"),
					"The font family of the QuotedText is not " + WrapperMethods
							.getWebElement(ArticlePage.getLiveFyreQuotedText(go)).getCssValue("font-family"));

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Quoted text");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void LiveFyrePlainCaption() {
		try {
			WrapperMethods.scrollIntoViewByElement(
					By.xpath("//div[@class='s-image-wrapper']//div[@class='s-upload-image-caption']/p"));

			WrapperMethods.isDisplayed(ArticlePage.liveBlogImagePlain(), "PLAIN caption Image is displayed",
					"PLAIN caption Image is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.liveBlogPlainCaption(), "PLAIN caption is displayed",
					"PLAIN caption is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("color")
							.equals("rgba(89, 89, 89, 1)"),
					"The color of the PLAIN caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("color"),
					"The color of the PLAIN caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-size")
							.equals("14px"),
					"The font size of the PLAIN caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-size"),
					"The font size of the PLAIN caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the PLAIN caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-weight"),
					"The font weight of the PLAIN caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-weight"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("line-height")
							.equals("23px"),
					"The line-height of the PLAIN caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("line-height"),
					"The line-height of the PLAIN caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("line-height"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the PLAIN text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-family"),
					"The font family of the PLAIN text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogPlainCaption()).getCssValue("font-family"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogCaption()).getCssValue("opacity").equals("1"),
					"The Opacity is 100%  ", "The Opacity is not 100% ");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live Blog Headings Validation");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void LiveFyreHeadings() {
		WebDriver go = DriverFactory.getCurrentDriver();
		WrapperMethods.scrollIntoViewByElement(By.xpath(
				"//div[contains(@class,'s-element-content')]//p/b//ancestor::div[contains(@style,'opacity')]/preceding-sibling::div[1]//b"));
		UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live Blog Image with Post Heading</b>");
		WrapperMethods.isDisplayed(ArticlePage.liveBlogPostHeader(go), "Post Header is displayed",
				"Post Header is not displayed");

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("color")
						.equals("rgba(64, 64, 64, 1)"),
				"The color of the Post Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("color"),
				"The color of the Post Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("color"));
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeaderOpacity())
				.getCssValue("opacity").equals("1"), "The Opacity is 100%  ", "The Opacity is not 100% ");
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-size")
						.equals("36px"),
				"The font size of the Post Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-size"),
				"The font size of the Post Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-size"));
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("line-height")
						.equals("40px"),
				"The line-height of the Post Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("line-height"),
				"The line-height of the Post Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("line-height"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-weight")
						.equals("300"),
				"The font weight of the Post Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-weight"),
				"The font weight of the Post Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogPostHeader(go)).getCssValue("font-weight"));
		WrapperMethods.scrollIntoViewByElement(By.xpath("//div[@class='s-element-content s-element-text']//h2//i"));
		UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live Blog Image with Italic Text Heading</b>");
		WrapperMethods.isDisplayed(ArticlePage.liveBlogItalicHeader(go), "Italic Header is displayed",
				"Italic Header is not displayed");

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("color")
						.equals("rgba(64, 64, 64, 1)"),
				"The color of the Italic Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("color"),
				"The color of the Italic Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("color"));
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeaderOpacity())
				.getCssValue("opacity").equals("1"), "The Opacity is 100%  ", "The Opacity is not 100% ");
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-size")
						.equals("36px"),
				"The font size of the Italic Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-size"),
				"The font size of the Italic Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-size"));
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("line-height")
						.equals("40px"),
				"The line- WrapperMethods.getWebElement(ArticlePage of the Italic Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("line-height"),
				"The line-height of the Italic Header is not " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("line-height"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-weight")
						.equals("300"),
				"The font weight of the Italic Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-weight"),
				"The font weight of the Italic Header is not " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogItalicHeader(go)).getCssValue("font-weight"));
		UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live Blog Image with Striked Text Heading</br>");
		WrapperMethods
				.scrollIntoViewByElement(By.xpath("//div[@class='s-element-content s-element-text']//h2//strike"));
		WrapperMethods.isDisplayed(ArticlePage.liveBlogStrikedHeader(go), "Striked Header is displayed",
				"Striked Header is not displayed");
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("color")
						.equals("rgba(64, 64, 64, 1)"),
				"The color of the Striked Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("color"),
				"The color of the Striked Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("color"));
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeaderOpacity())
				.getCssValue("opacity").equals("1"), "The Opacity is 100%  ", "The Opacity is not 100% ");
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-size")
						.equals("36px"),
				"The font size of the Striked Header is : "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-size"),
				"The font size of the Striked Header is not "
						+ WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-size"));
		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("line-height")
						.equals("40px"),
				"The line-height of the Striked Header is : " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("line-height"),
				"The line-height of the Striked Header is not " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("line-height"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-weight")
						.equals("300"),
				"The font weight of the Striked Header is : " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-weight"),
				"The font weight of the Striked Header is not " + WrapperMethods
						.getWebElement(ArticlePage.liveBlogStrikedHeader(go)).getCssValue("font-weight"));

	}

	/**
	 * Article with Embedded full video
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testArticleEmbFullVideo() {
		try {
			WrapperMethods.verifyElement(ArticlePage.getArticleEmbVideo(), "The Embedded Full width video");
			WrapperMethods.clickJavaScript(ArticlePage.getArticleEmbVideoButton());
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(5);
			try {
				WrapperMethods.assertIsTrue(ArticlePage.getVideoplaying().size() != 0, "The video is playing",
						"The video is not playing");

			} catch (Exception E) {
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Embedded Full width video element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void validateEmbedInstagram() {
		try {
			ArticlePage.wait(10);
			WrapperMethods.verifyElement(ArticlePage.getEmbedInstagram(), "Embed Instagram");
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")
					|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tab")) {
				WrapperMethods.assertIsTrue(
						ArticlePage.getEmbedInstagramAlign().getCssValue("float").equalsIgnoreCase("left"),
						"Embed Instagram is aligned " + ArticlePage.getEmbedInstagramAlign().getCssValue("float")
								+ " and not overlapping with the text..",
						"Embed Instagram is not aligned in the article page: "
								+ ArticlePage.getEmbedInstagramAlign().getCssValue("float"));
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article Page with Embed Instagram");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testHeaderColl() {
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing On Article Common Header</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getArticleHeaderTitle()).isDisplayed(),
					"Header Title is present", "Header Title is not present");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getArticleTime()).isDisplayed(),
					"Timestamp is present", "Timestamp is not present");
			try {
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getGigyaBar()).isDisplayed(),
						"Gigyabar is present", "Gigyabar is not present");
			} catch (Exception e) {
			}
			try {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(ArticlePage.getGigyaBarMessenger()).isDisplayed(),
						"Gigyabar Messenger icon is present", "Gigyabar Messenger Icon is not present");
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Gigyabar Messenger Icon is not present");
			}
			try {
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem2()).isDisplayed(),
						"Gigyabar Facebook icon is present", "Gigyabar Facebook icon is not present");
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Gigyabar FAcebook Icon is not present");

			}
			try {
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem3()).isDisplayed(),
						"Gigyabar Twitter icon is present", "Gigyabar Twitter icon is not present");
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Gigyabar Twitter Icon is not present");

			}
			try {
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getGigyaBarElem4()).isDisplayed(),
						"Gigyabar Share icon is present", "Gigyabar Share icon is not present");
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Gigyabar Share Icon is not present");

			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Header element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testArticleAuthor() {
		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Testing On Article Author</b>");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getArticleAuthorBy()).getText().startsWith("By"),
					"Author field starts with BY ", "Author field doesnot start with BY");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.getArticleAuthorBy()).getText().length() > 3,
					"Author name is displayed", "Author name is not displayed");
			try {
				if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
					WrapperMethods.assertIsTrue(
							WrapperMethods.getWebElement(ArticlePage.getAuthorImage()).isDisplayed(),
							"author image isdisplayed", "author image is not displayed");
					WrapperMethods
							.assertIsTrue(
									!WrapperMethods.getWebElement(ArticlePage.getAuthorImageSrc()).getAttribute("src")
											.isEmpty(),
									"author image src is displayed", "author image src is not displayed");
					WrapperMethods.brokenLinkValidation(
							WrapperMethods.getWebElement(ArticlePage.getAuthorImageSrc()).getAttribute("src"));
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.WARNING, "The author Image is not Present - Check Manually");
				UMReporter.log(LogStatus.INFO, e.getMessage());
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Author element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void basicArticleVideoElemValidations() {

		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Article Collections Video Elements</b>");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollCarousel(), "Video Carousel is displayed",
					"Video Carousel is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollPrevCarousel(), "Prev Owl Carousel is displayed",
					"Prev Owl Carousel is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollNextCarousel(), "Next Owl Carousel is displayed",
					"Next Owl Carousel is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollVideoElem(), "Video element is displayed: ",
					"Video element is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollVideoElemTitle(),
					"Video element title is displayed: "
							+ WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollVideoElemTitle()).getText(),
					"Video element title is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.getArticleVideoCollTotalTime(), "Total Time is displayed",
					"Total Time is not displayed");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollTotalTime())
							.getAttribute("data-post-text").isEmpty(),
					"Total Time is displayed" + WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollTotalTime())
							.getAttribute("data-post-text"),
					"Total Time is not displayed" + WrapperMethods
							.getWebElement(ArticlePage.getArticleVideoCollTotalTime()).getAttribute("data-post-text"));

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollImgSrc()).getAttribute("src")
							.isEmpty(),
					"Img Src is displayed"
							+ WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollImgSrc()).getAttribute("src"),
					"Img src is not displayed" + WrapperMethods.getWebElement(ArticlePage.getArticleVideoCollImgSrc())
							.getAttribute("src"));

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(ArticlePage.getArticleVideoHeadLines()).getText().isEmpty(),
					"Vid Headline is displayed"
							+ WrapperMethods.getWebElement(ArticlePage.getArticleVideoHeadLines()).getText(),
					"Vid Headline is not displayed"
							+ WrapperMethods.getWebElement(ArticlePage.getArticleVideoHeadLines()).getText());

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Video Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	/**
	 * Article Comments
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testArticleComments() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			try {
				WrapperMethods.moveToElement(ArticlePage.getLoginBar());
				WrapperMethods.click(ArticlePage.getLoggedOut(), "Logout Button");
				ArticlePage.wait(15);
			} catch (Exception e) {
			}
			WrapperMethods.verifyElement(ArticlePage.getCommentSignIn(), "The SignIN");
			WrapperMethods.clickJavaScript(ArticlePage.getCommentSignIn());
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-gigya-widget")));
			WrapperMethods.enter_Text(ArticlePage.getLoginEmail(), "cnnweqa@gmail.com");
			WrapperMethods.moveToElement(ArticlePage.getLoginPass());
			WrapperMethods.enter_Text(ArticlePage.getLoginPass(), "cnnweqa");
			WrapperMethods.moveToElement(ArticlePage.getLoginButton());
			WrapperMethods.click(ArticlePage.getLoginButton(), "Login Button");
			ArticlePage.wait(15);
			UMReporter.log(LogStatus.PASS, "Login Process is successful");
			ArticlePage.wait(30);
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WebElement Iframe = driver.findElement(By.cssSelector(".fyre-editor-iframe iframe"));
				driver.switchTo().frame(Iframe);
			}
			ArticlePage.wait(10);
			WrapperMethods.moveToElement(ArticlePage.getCommentField());
			WrapperMethods.enter_Text(ArticlePage.getCommentField(), ArticlePage.generateRandomString(20));
			ArticlePage.wait(15);
			WrapperMethods.contains_Text(ArticlePage.getCommentField(),
					"The comment field has value and teh comment is : ", "Comment is not passed");
			driver.switchTo().defaultContent();
			WrapperMethods.scrollDown();
			ArticlePage.wait(5);
			WrapperMethods.click(ArticlePage.getPostComment(), "Post Comment");
			ArticlePage.wait(15);
			WrapperMethods.click(ArticlePage.getCommentReply(), "Reply Comment");
			ArticlePage.wait(5);
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WebElement replyIframe = driver
						.findElement(By.cssSelector(".fyre-comment-footer div.fyre-editor iframe"));
				driver.switchTo().frame(replyIframe);
			}
			WrapperMethods.enter_Text(ArticlePage.getCommentField(), ArticlePage.generateRandomString(20));
			ArticlePage.wait(15);
			WrapperMethods.contains_Text(ArticlePage.getCommentField(),
					"The reply for the already posted comment is : ", "The reply for the Comment is not passed");
			driver.switchTo().defaultContent();
			WrapperMethods.scrollDown();
			WrapperMethods.clickJavaScript(ArticlePage.getPostReplyComment());
			// WrapperMethods.click(ArticlePage.getPostReplyComment(), "Post
			// Reply Comment");
			ArticlePage.wait(15);
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Article comments Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void postArticleComments() {

		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			/*
			 * if (System.getProperty("deviceType").equals("Desktop")) {
			 * WebElement Iframe =
			 * go.findElement(By.cssSelector(".fyre-editor-iframe iframe"));
			 * go.switchTo().frame(Iframe); }
			 */
			WrapperMethods.enter_Text(ArticlePage.getCommentField(), ArticlePage.generateRandomString(20));
			ArticlePage.wait(15);

			WrapperMethods.contains_Text(ArticlePage.getCommentField(),
					"The comment field has value and teh comment is : ", "Comment is not passed");
			driver.switchTo().defaultContent();

			WrapperMethods.page_scrollDown();

			WrapperMethods.click(ArticlePage.getPostComment(), "Post Comment");
			ArticlePage.wait(15);
			WrapperMethods.click(ArticlePage.getCommentReply(), "Reply Comment");
			ArticlePage.wait(5);
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WebElement replyIframe = driver
						.findElement(By.cssSelector(".fyre-comment-footer div.fyre-editor iframe"));
				driver.switchTo().frame(replyIframe);
			}
			WrapperMethods.enter_Text(ArticlePage.getCommentField(), ArticlePage.generateRandomString(20));
			ArticlePage.wait(15);
			WrapperMethods.contains_Text(ArticlePage.getCommentField(),
					"The reply for the already posted comment is : ", "The reply for the Comment is not passed");
			driver.switchTo().defaultContent();
			WrapperMethods.page_scrollDown();

			WrapperMethods.click(ArticlePage.getPostReplyComment(), null);

			ArticlePage.wait(15);

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Posting the Article comments Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with expandable image and vadilating elements before expand
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testImgBeforeExpand() {
		try {
			UMReporter.log(LogStatus.INFO, "Testing with Before Expand Elements");
			WrapperMethods.verifyElement(ArticlePage.getBeforeExpandImgWidth(), "Default width");
			WrapperMethods.verifyElement(ArticlePage.getImgTitle(), "The Image title");
			WrapperMethods.assertIsTrue(!ArticlePage.getBeforeExpandImg().getAttribute("src").isEmpty(),
					"The  Image source is displayed", "The  Image source is not displayed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Width before expanding");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with expandable image and vadilating elements after expand
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testImgAfterExpand() {
		try {
			UMReporter.log(LogStatus.INFO, "Testing with After Expand Elements");
			WrapperMethods.assertIsTrue(
					ArticlePage.getAfterExpandImgWidth().getAttribute("style").contains("width: 100%;"),
					"The width is 100%",
					"The width is not 100%: " + ArticlePage.getAfterExpandImgWidth().getAttribute("style"));
			WrapperMethods.verifyElement(ArticlePage.getExpandClose(), "the close button");

			WrapperMethods.verifyElement(ArticlePage.getImgTitle(), "The Image title");

			WrapperMethods.assertIsTrue(!ArticlePage.getAfterExpandImg().getAttribute("src").isEmpty(),
					"The  Image source is displayed", "The  Image source is not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Width after expanding");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with page top image
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testArticleStaticPage() {
		try {

			WrapperMethods.verifyElement(ArticlePage.getArticleStaticImage(), "The Image");
			WrapperMethods.assertIsTrue(!ArticlePage.getArticleStaticImage().getAttribute("src").isEmpty(),
					"The Image source is displayed", "The Image source is not displayed");
			MethodDef.brokenLinkValidation(ArticlePage.getArticleStaticImage().getAttribute("src"));
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the article image");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void outbrainFirstSectionAdVerify() {
		UMReporter.log(LogStatus.INFO, "Tesing First Section of the Outbrain - Right Rail");
		WrapperMethods.scrollDown();
		ArticlePage.wait(2);
		WrapperMethods.verifyElement(ArticlePage.outBrainColumnFirstZone(), "Right rail Outbrain Container");
		WrapperMethods.verifyElement(ArticlePage.articleRightRailFirstAd(), "ArticlePage Leaf First Ad");
		UMReporter.log(LogStatus.INFO, "Scroll down the Page");
		UMReporter.log(LogStatus.INFO, "Tesing First Section - AD Visibility");
		MethodDef.explicitWaitVisibility(ArticlePage.articleRightRailFirstAd(), 15,
				"Wait untill Right Rail First Ad is loaded", "Right Rail First Ad is not loaded");
		MethodDef.isVisible(ArticlePage.articleRightRailFirstAd());
		MethodDef.screenshot();
	}

	public static void outbrainSecondSectionAdVerify() {
		UMReporter.log(LogStatus.INFO, "Scrolling to the Second Zone - Section of Outbrain");
		WrapperMethods.moveToElement(ArticlePage.outBrainColumnSecondSection());
		WrapperMethods.verifyElement(ArticlePage.outBrainColumnSecondZone(), "Right rail Outbrain Container");
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			WrapperMethods.verifyElement(ArticlePage.rightRailSecondSectionAd(), "Article Leaf Second Ad");
		}
		UMReporter.log(LogStatus.INFO, "Tesing First Section - AD Visibility");
		MethodDef.isNotVisible(ArticlePage.articleRightRailFirstAd());
		WrapperMethods.scrollDown();
		if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
			UMReporter.log(LogStatus.INFO, "Tesing Second Section - AD Visibility");
			MethodDef.isVisible(ArticlePage.rightRailSecondSectionAd());
			MethodDef.screenshot();
		}
	}

	public static void outbrainThirdZoneAdVerify() {
		try {
			UMReporter.log(LogStatus.INFO, "Scrolling to the third Zone - Section of Outbrain");
			WrapperMethods.moveToElement(ArticlePage.outBrainColumnThirdSection());
			WrapperMethods.scrollDown();
			UMReporter.log(LogStatus.INFO, "Tesing First Section - AD Visibility");
			MethodDef.isNotVisible(ArticlePage.articleRightRailFirstAd());
			MethodDef.screenshot();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Leaf Long Article Outbrain elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testOutbrain() {
		try {

			WrapperMethods.verifyElement(ArticlePage.getPromotedStories(),
					"Promoted Stories Container below the Story");
			WrapperMethods.assertIsTrue(!ArticlePage.getPromotedStoriesHeadlines().isEmpty(),
					"Promoted Stories Headlines are displayed", "Promoted Stories Headlines are not displayed");
			WrapperMethods.verifyElement(ArticlePage.getMoreFromCNN(), "More from CNN Container below the Story");
			WrapperMethods.assertIsTrue(!ArticlePage.getMoreFromCNNHeadlines().isEmpty(),
					"More from CNN Headlines are displayed", "More from CNN Headlines are not displayed");

			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				WrapperMethods.verifyElement(ArticlePage.getPromotedStoriesOutbrain(),
						"Recommended by Outbrain link under the Promoted Stories");
				WrapperMethods.verifyElement(ArticlePage.getMoreFromCNNOutbrain(),
						"Recommended by Outbrain link under the More from CNN");
				WrapperMethods.verifyElement(ArticlePage.getContentByLendingTree(),
						"Content by LendingTree Container below the More from CNN");
				WrapperMethods.assertIsTrue(!ArticlePage.getContentByLendingTreeHeadlines().isEmpty(),
						"Content by LendingTree Headlines are displayed",
						"Content by LendingTree Headlines are not displayed");
			} else {
				WrapperMethods.verifyElement(ArticlePage.getMoreFromCNNOutbrain(),
						"Recommended by Outbrain link under the More from CNN");
			}

			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
				UMReporter.log(LogStatus.INFO, "<b>Click on Recommended by Outbrain link</b>");
				WrapperMethods.clickJavaScript(ArticlePage.getMoreFromCNNOutbrain());
				ArticlePage.wait(25);
				WrapperMethods.switchToFrame("ob_iframe_modal");
				WrapperMethods.verifyElement(ArticlePage.getOutbrainLogo(), "Outbrain Logo in dialogue box");
				UMReporter.log(LogStatus.INFO, "<b>Click on Close button of Outbrain dialogue box</b>");
				WrapperMethods.switchToDefaultContent();
				WrapperMethods.click(ArticlePage.getOutbrainCloseButton(), "Outbrain Close Button");
				ArticlePage.wait(5);
				WrapperMethods.assertIsTrue(ArticlePage.getMoreFromCNNOutbrain().isDisplayed(),
						"Outbrain dialogue box is closed sucessfully",
						"Outbrain dialogue box is not closed sucessfully");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Outbrain element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testOutbrainTab() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			Actions actions = new Actions(driver);
			WrapperMethods.verifyElement(ArticlePage.getMoreFromCnn(), "More from CNN Container below the Story");
			WrapperMethods.assertIsTrue(!ArticlePage.getPaidContentHeadlinestab().isEmpty(),
					"Paid Content Headlines are displayed", "Paid Content Headlines are not displayed");
			Thread.sleep(5);
			WrapperMethods.assertIsTrue(!ArticlePage.getMoreFromCnnHeadlinesTab().isEmpty(),
					"More from CNN Headlines are displayed", "More from CNN Headlines are not displayed");
			WrapperMethods.verifyElement(ArticlePage.getMoreFromCNNOutbrainTab(),
					"Recommended by Outbrain link under the More from CNN");
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				WrapperMethods.verifyElement(ArticlePage.getContentByLendingTree(),
						"Content by LendingTree Container below the More from CNN");
				WrapperMethods.assertIsTrue(!ArticlePage.getContentByLendingTreeHeadlines().isEmpty(),
						"Content by LendingTree Headlines are displayed",
						"Content by LendingTree Headlines are not displayed");
			}
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {

				WrapperMethods.verifyElement(ArticlePage.getPaidcontent(),
						"Promoted Stories Container below the Story");
				UMReporter.log(LogStatus.INFO, "<b>Click on Recommended by Outbrain link</b>");
				WrapperMethods.clickJavaScript(ArticlePage.getMoreFromCNNOutbrainTab());
				ArticlePage.wait(25);
				WrapperMethods.switchToFrame("ob_iframe_modal");
				WrapperMethods.verifyElement(ArticlePage.getOutbrainLogo(), "Outbrain logo in dialogue box");
				UMReporter.log(LogStatus.INFO, "<b>Click on Close button of Outbrain dialogue box</b>");
				WrapperMethods.switchToDefaultContent();
				if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
						|| ConfigProvider.getConfig("Browser").equalsIgnoreCase("FIREFOX")) {
					WrapperMethods.clickJavaScript(ArticlePage.getOutbrainCloseButton());
				} else {
					actions.moveToElement(ArticlePage.getOutbrainCloseButtonTab()).click().perform();
				}
				ArticlePage.wait(5);
				WrapperMethods.assertIsTrue(ArticlePage.getMoreFromCNNOutbrainTab().isDisplayed(),
						"Outbrain dialogue box is closed sucessfully",
						"Outbrain dialogue box is not closed sucessfully");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Outbrain element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void validateMultipleEmbedImages() {
		try {
			for (WebElement elem : ArticlePage.getEmbedImage()) {
				WrapperMethods.assertIsTrue(elem.isDisplayed(), "Embed image is displayed in the article page",
						"Embed image is not displayed in the article page");
				WrapperMethods.clickJavaScript(elem);
				UMReporter.log(LogStatus.PASS, "Embed image is opened");
			}
			for (WebElement elem : ArticlePage.getEmbedImageClose()) {
				WrapperMethods.clickJavaScript(elem);
				UMReporter.log(LogStatus.PASS, "Embed image which is opened is closed");
			}
			try {
				WrapperMethods.clickJavaScript(ArticlePage.getEmbedImgClose());
				MethodDef.failLog("", "All the Open Embedded Images are not closed");
			} catch (Exception e) {
				MethodDef.passLog("All the Open Embedded Images are closed");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page with Multiple Embed Images");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void validateEmbedShowElements() {
		try {
			WrapperMethods.verifyElement(ArticlePage.getEmbedShow(), "Embed show");
			WrapperMethods.verifyElement(ArticlePage.getEmbedShowImage(), "Embed show Image");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmbedShowCaption().getAttribute("alt").isEmpty(),
					"The caption or the alt of the embed show is displayed and the value is : "
							+ ArticlePage.getEmbedShowCaption().getAttribute("alt"),
					"The caption or the alt of the embed show is not displayed");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmbedShowHref().getAttribute("href").isEmpty(),
					"The embed show has URL and the value is : " + ArticlePage.getEmbedShowHref().getAttribute("href"),
					"The embed show doesnt have the  URL");
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedShowCaption().getAttribute("height").equals("169"),
					"Embed show Image height is correct and the value is : "
							+ ArticlePage.getEmbedShowCaption().getAttribute("height"),
					"Embed show Image height is not correct and the value is : "
							+ ArticlePage.getEmbedShowCaption().getAttribute("height"));
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedShowCaption().getAttribute("width").equals("300"),
					"Embed show Image width is correct and the value is : "
							+ ArticlePage.getEmbedShowCaption().getAttribute("width"),
					"Embed show Image width is not correct and the value is : "
							+ ArticlePage.getEmbedShowCaption().getAttribute("width"));
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page with Embed Show Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void validateEmbedWebTag() {
		try {
			ArticlePage.wait(5);
			WrapperMethods.verifyElement(ArticlePage.getEmbedWebTag(), "Embed webtag");
			if (ConfigProvider.getConfig("Platform").equals("Desktop")
					|| ConfigProvider.getConfig("Platform").equals("Tab")) {
				WrapperMethods.assertIsTrue(ArticlePage.getEmbedWebTag().getCssValue("float").equals("left"),
						"Embed webtag is aligned " + ArticlePage.getEmbedWebTag().getCssValue("float")
								+ " and not overlapping with the text..",
						"Embed webtag is not aligned in the article page");
			}
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedWebTag().getCssValue("margin-bottom").equals("10px"),
					"The padding bottom of the web tag is : "
							+ ArticlePage.getEmbedWebTag().getCssValue("margin-bottom"),
					"Embed webtag is not having padding ");
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedWebTag().getCssValue("margin-top").equals("10px"),
					"The padding top of the web tag is : " + ArticlePage.getEmbedWebTag().getCssValue("margin-top"),
					"Embed webtag is not having padding ");
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedWebTag().getCssValue("margin-right").equals("10px"),
					"The padding right of the web tag is : " + ArticlePage.getEmbedWebTag().getCssValue("margin-right"),
					"Embed webtag is not having padding ");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page with Embed webtag Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void validateEmbedSpecial() {
		try {
			WrapperMethods.verifyElement(ArticlePage.getEmbedSpecial(), "Embed Special");
			/*
			 * if (System.getProperty("deviceType").equals("Desktop") ||
			 * System.getProperty("deviceType").equals("Tab")) {
			 * WrapperMethods.assertIsTrue(error,
			 * ArticlePage.getEmbedSpecial().getCssValue("float").equals("left")
			 * , "Embed special is aligned " +
			 * ArticlePage.getEmbedSpecial().getCssValue("float") +
			 * " and not overlapping with the text..",
			 * "Embed special is not aligned in the article page"); }
			 */
			WrapperMethods.assertIsTrue(ArticlePage.getEmbedSpecialImage().isDisplayed(),
					"Embed Special image is displayed", "Embed Special image is not displayed");
			MethodDef.brokenLinkValidation(ArticlePage.getEmbedSpecialImage().getAttribute("src"));
			WrapperMethods.assertIsTrue(!ArticlePage.getEmbedSpecialHref().getAttribute("href").isEmpty(),
					"Embed Special has the link and the URL is : "
							+ ArticlePage.getEmbedSpecialHref().getAttribute("href"),
					"Embed Special doesn't have the link");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmbedSpecialTitle().getText().isEmpty(),
					"Embed Special has the title and the title is : " + ArticlePage.getEmbedSpecialTitle().getText(),
					"Embed Special doesn't have the title");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page with Embed special Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void contentFontstyles() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.visibilityOf(ArticlePage.getArticleContent()));
			WrapperMethods.assertIsTrue(
					(ArticlePage.getArticleContent().getCssValue("font-family"))
							.equals(ArticlePage.getArticleBulletPointsContent().getCssValue("font-family")),
					"The font family is same for both the normal content and the bullet points and the font value of content is : "
							+ ArticlePage.getArticleContent().getCssValue("font-family")
							+ " and the font family for bullet points content is : "
							+ ArticlePage.getArticleBulletPointsContent().getCssValue("font-family"),
					"The font family is different when compared to normal content and the bullet points");
			WrapperMethods.assertIsTrue(
					(ArticlePage.getArticleContent().getCssValue("font-size"))
							.equals(ArticlePage.getArticleBulletPointsContent().getCssValue("font-size")),
					"The font size is same for both the normal content and the bullet points and the font value of content is : "
							+ ArticlePage.getArticleContent().getCssValue("font-size")
							+ " and the font size for bullet points content is : "
							+ ArticlePage.getArticleBulletPointsContent().getCssValue("font-size"),
					"The font size is different when compared to normal content and the bullet points");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Page with Font content changes");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with Embedded video collections
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void basicEmVideoCollElemValidations() {

		try {
			ArticlePage.wait(5);
			WrapperMethods.verifyElement(ArticlePage.getEmVideoCollCarousel(), "Video Carousel");
			WrapperMethods.verifyElement(ArticlePage.getEmVideoCollCarouselPrev(), "Prev Owl Carousel");
			WrapperMethods.verifyElement(ArticlePage.getEmVideoCollCarouselNext(), "Next Owl Carousel");
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				WrapperMethods.verifyElement(ArticlePage.getEmVideoCollElement(), "Video element");
			} else {
				WrapperMethods.verifyElement(ArticlePage.getEmVideoCollElemMob(), "Video element");
			}
			WrapperMethods.verifyElement(ArticlePage.getEmVideoCollTotalTime(), "Total Time");
			WrapperMethods.assertIsTrue(!ArticlePage.getEmVideoCollTotalTime().getAttribute("data-post-text").isEmpty(),
					"Total Time is displayed" + ArticlePage.getEmVideoCollTotalTime().getAttribute("data-post-text"),
					"Total Time is not displayed"
							+ ArticlePage.getEmVideoCollTotalTime().getAttribute("data-post-text"));
			WrapperMethods.assertIsTrue(!ArticlePage.getEmVideoCollImgSrc().getAttribute("src").isEmpty(),
					"Img Src is displayed" + ArticlePage.getEmVideoCollImgSrc().getAttribute("src"),
					"Img src is not displayed" + ArticlePage.getEmVideoCollImgSrc().getAttribute("src"));
			MethodDef.brokenLinkValidation(ArticlePage.getEmVideoCollImgSrc().getAttribute("src"));
			WrapperMethods.assertIsTrue(!ArticlePage.getEmVideoHeadLines().getText().isEmpty(),
					"Vid Headline is displayed" + ArticlePage.getEmVideoHeadLines().getText(),
					"Vid Headline is not displayed" + ArticlePage.getEmVideoHeadLines().getText());
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Embedded Video Collection Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	/**
	 * Article with Author card & Story Highlights
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void basicAuthorCardValidations() {

		try {
			UMReporter.log(LogStatus.INFO, "Testing with Story highlight Elements");
			WrapperMethods.assertIsTrue(!ArticlePage.getStoryHighTitle().getText().isEmpty(),
					"The Story highlight is displayed : " + ArticlePage.getStoryHighTitle().getText(),
					"The Story highlight is not displayed");

			// for (WebElement elem : ArticlePage.getStoryHighContent(go)) {
			WrapperMethods.assertIsTrue(ArticlePage.getStoryHighContent().isDisplayed(),
					"Story highlight item is visible : " + ArticlePage.getStoryHighContent().getText(),
					"Story highlight item is not visible");
			ArticlePage.wait(5);

			// }
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Story highlight Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
		try {
			UMReporter.log(LogStatus.INFO, "Testing with Author card Elements");
			WrapperMethods.assertIsTrue(ArticlePage.getEditorNote().isDisplayed(),
					"The Editor Note is displayed : " + ArticlePage.getEditorNote().getText(),
					"The Editor Note is not displayed");
			WrapperMethods.assertIsTrue(ArticlePage.getEditorialSource().isDisplayed(),
					"The Editor source is displayed : " + ArticlePage.getEditorialSource().getText(),
					"The Editor source is not displayed");
			WrapperMethods.assertIsTrue(!ArticlePage.getEditorImage().getAttribute("src").isEmpty(),
					"The Image source is displayed", "The  Image source is not displayed");
			WrapperMethods.assertIsTrue(ArticlePage.getEditorName().isDisplayed(),
					"The Editor Name is displayed : " + ArticlePage.getEditorName().getText(),
					"The Editor Name is not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Author card Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with pull quote
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void basicPullQuoteValidations() {

		try {
			UMReporter.log(LogStatus.INFO, "Testing with Pull Quote Elements");
			WrapperMethods.verifyElement(ArticlePage.getPullQuoteSymbol(), "The PullQuote Symbol");
			WrapperMethods.contains_Text(ArticlePage.getPullQuote(), "The PullQuote is displayed: ",
					"The PullQuote is not displayed");
			WrapperMethods.contains_Text(ArticlePage.getPullQuoteAuthor(), "The PullQuote Author is displayed: ",
					"The PullQuote Author is not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Pull Quote Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	/**
	 * Article with page top auto play video
	 * 
	 * @param go
	 *            - webdriver instance
	 * @param error
	 *            - Soft error instance
	 * @param article
	 *            - instance of Article page
	 */
	public static void testArticleAutoPlayVideo() {
		try {
			WrapperMethods.contains_Text(ArticlePage.getVideoCaption(), "the video caption is displayed:",
					"the video caption is not displayed");
			WrapperMethods.contains_Text(ArticlePage.getVideoTime(), "the video time is displayed:",
					"the video time is not displayed");
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				try {
					if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
						WrapperMethods.waitPresenceOfEleLocated(ArticlePage.getVideoSourceName(), 20);
					}
					WrapperMethods.contains_Text(ArticlePage.getVideoSourceName(), "Source is displayed:",
							"Source is not displayed");
				} catch (Exception e) {
					UMReporter.log(LogStatus.WARNING, "Video Source is not Present - Check Manually" + e.getMessage());
				}
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the autoplay video elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testPrivacyPolicy(String Url) {

		if (Url.contains("privacy"))
			WrapperMethods.assertIsTrue(ArticlePage.paragraph().size() == 12, "The Contents are available",
					"Some Contents are missing.. Please check");
		else if (Url.contains("terms"))
			WrapperMethods.assertIsTrue(ArticlePage.paragraph().size() == 5, "The Contents are available",
					"Some Contents are missing.. Please check");
	}

	public static void testimagecutverticalgallery() throws IOException, InterruptedException {
		WrapperMethods.isDisplayed(ArticlePage.PagetopGallerydatacut(), "The Photo Gallery is displayed as expected",
				"The Photo Gallery is displayed - Unexpected");
		WrapperMethods.contains_Text_Attribute(ArticlePage.PagetopGallerydatacut(), "data-cut-format", "9:16",
				"The Page Top gallery has 9:16 data cut as expected",
				"The Page Top gallery does not have 9:16 data cut -Unexpected");
		WrapperMethods.isDisplayed(ArticlePage.previousarrowowl(), "The Previous owl button is visible as expected",
				"The Previous own putton is not expected");
		WrapperMethods.isDisplayed(ArticlePage.nextarrowowl(), "The Next owl button is visible as expected",
				"The Next own putton is not expected");
	}

	public static void commonPinnedVideoValidation() {
		try {
			ArticlePage.wait(25);
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(15);
			WrapperMethods.page_scrollDown();
			ArticlePage.wait(15);
			MethodDef.explicitWaitVisibility(ArticlePage.getVideoElementPinned(), 40,
					"Pinned Video element is visible.", "Pinned video element is not visible..");
			UMReporter.log(LogStatus.PASS, "Pinned Video is displayed correctly");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned video elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void pinnedVideoValidations() {
		if (MethodDef.getJsonVideoPinned()) {
			try {
				commonPinnedVideoValidation();
				validatePinnedNowPlaying();
				validatePinnedHeader();
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error accessing Pinned Video");
				UMReporter.log(LogStatus.INFO, E.getMessage());
			}
		} else {
			try {
				WrapperMethods.click(VideoLeafPage.getVideoElementPinned(), "Clicked Pinned Video element",
						"Not Click Pinned Video element");
				UMReporter.log(LogStatus.FAIL, "Pinned Video is displayed - But the Michonne config is turned off");
			} catch (Exception E) {
				UMReporter.log(LogStatus.PASS, "Pinned Video is not present as per configuration");
			}
		}
	}

	private static void validatePinnedNowPlaying() {
		try {
			MethodDef.explicitWaitVisibility(VideoLeafPage.getVideoPinnedNow(), 10, "Pinned Video is visible now.",
					"Pinned Video is not able to visible.");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoPinnedNow(),
					"Now Playing Element is present in Pinned Video",
					"Now Playing Element is not present in Pinned Video");

			UMReporter.log(LogStatus.INFO, "<B>Validating the CSS Property of Now Playing text</B>");

			if (WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedNow()).isDisplayed()) {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedNow()).getCssValue("color")
								.equals("rgba(227, 0, 0, 1)"),
						"Now Playing Text is Red Colour", "Now Playing Text is not Red Colour");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned Now Playing");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	private static void validatePinnedHeader() {
		try {
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoPinnedNow(),
					"Truncated Header Element is present in Pinned Video",
					"Truncated Header Element is not present in Pinned Video");
			UMReporter.log(LogStatus.INFO, "<B>Validating the CSS Property of truncated headline</B>");
			if (WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedNow()).isDisplayed()) {
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedTrunc())
						.getCssValue("max-height").equals("40px"), "Max Height is correct", "Max Height is wrong");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedTrunc())
						.getCssValue("width").equals("200px"), "width is correct", "width is wrong");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedTrunc())
						.getCssValue("float").equals("left"), "float is correct", "float is wrong");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(VideoLeafPage.getVideoPinnedTrunc())
						.getCssValue("word-wrap").equals("break-word"), "word wrap is correct", "word wrap is wrong");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned video header title");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void VRPlayervalidation() {
		WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 40);
		wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("div.vjs-big-play-button"), 0));

		WrapperMethods.moveToElementClick(ArticlePage.VRPlayButton());
		wait.until(
				ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[contains(@class,'has-started')]"), 0));
		try {
			WrapperMethods.assertIsTrue(ArticlePage.getVideoplaying().size() != 0, "The video is playing",
					"The video is not playing");
		} catch (Exception E) {
		}

		try {
			WebElement videoPlayer = MethodDef.getvideoObject();
			MethodDef.vidPlayerBasicValidations(videoPlayer);
		} catch (Exception E) {
		}

	}

	public static void testContentNavigationElements() {

		try {
			UMReporter.log(LogStatus.INFO, "Content to content navigation elements");

			// article.wait(15);
			DriverFactory.getCurrentDriver().manage().window().maximize();
			WrapperMethods.verifyElement(ArticlePage.getArrowButton(), "The arrow button ");
			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
				WrapperMethods.moveToElement(ArticlePage.getArrowButton());
				// article.wait(5);
				WrapperMethods.verifyElement(ArticlePage.getArticleImage(), "The article thumbnail image ");
				WrapperMethods.verifyElement(ArticlePage.getArticleImgSrc(), "The article thumbnail image src");
				WrapperMethods.verifyElement(ArticlePage.getArticleHeadline(), "The article headline ");
				WrapperMethods.verifyElement(ArticlePage.getArticleLink(), "The next story image and the headline ");
				JavascriptExecutor jse = (JavascriptExecutor) DriverFactory.getCurrentDriver();
				jse.executeScript("scroll(0, 3000)");

				WrapperMethods.verifyElement(ArticlePage.getArticleImage(), "The article thumbnail image ");
				WrapperMethods.verifyElement(ArticlePage.getArticleImgSrc(), "The article thumbnail image src");
				WrapperMethods.verifyElement(ArticlePage.getArticleHeadline(), "The article headline ");
				WrapperMethods.verifyElement(ArticlePage.getArticleLink(),
						"The next story image and the headline link ");

			} else {
				UMReporter.log(LogStatus.WARNING,
						"Content To content Nav testing is not supported by SAFARI Extension");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the Content to content navigation elements");

		}

	}

	public static void articleReadMore() {

		try {

			UMReporter.log(LogStatus.INFO, "Testing with Article with Expand GAllery Elements");

			WrapperMethods.verifyElement(ArticlePage.getMobileShareBar(),
					"The social icons before clicking the read more button");

			clickReadMore();

			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.readMore()).isDisplayed(),
					"The read more button should not be displayed", "The read more button is displayed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Read More element");
		}

	}

	public static void clickReadMore() {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			Boolean read = MethodDef.getJsonReadMore();
			if (read) {
				try {
					WrapperMethods.clickJavaScript(ArticlePage.readMore());
					UMReporter.log(LogStatus.INFO, "Read More is clicked");
				} catch (Exception E) {
					UMReporter.log(LogStatus.INFO, "Read More is not present");
				}
			}
		}
	}

	public static void testheader() {
		try {
			UMReporter.log(LogStatus.INFO, "Testing On Article Common Header");
			WrapperMethods.verifyElement(ArticlePage.getArticleHeaderTitle(), "Header Title");

			WrapperMethods.verifyElement(ArticlePage.getArticleTime(), "Timestamp");

			WrapperMethods.verifyElement(ArticlePage.getGigyaBar(), "Gigyabar");
			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem2(), "Gigyabar Facebook icon");

			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem3(), "Gigyabar Twitter icon");

			WrapperMethods.verifyElement(ArticlePage.getGigyaBarElem4(), "Gigyabar Share icon");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Article Header element");
		}
	}

	public static void testarticleoutbrain() throws IOException {
		try {

			WrapperMethods.verifyElement(ArticlePage.ArticlePaidContent(), "Paid Content ");

			WrapperMethods.verifyElement(ArticlePage.Newsforyou(), "News for you ");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
		}
	}

	public static void testarticlepagetopgallery() {
		try {
			// Gallery Header
			GalleryPageFunctions.testheader();
			GalleryPageFunctions.clickGalleryPrev();
			GalleryPageFunctions.clickGalleryNext();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Accessing Gallery Elements" + E.getMessage());

		}

		try {

			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				GalleryPageFunctions.galleryCaptionsOpenTest();
				GalleryPageFunctions.hideCaption();

				if (System.getProperty("edition").equalsIgnoreCase("INTL"))
					WrapperMethods.clickJavaScript(GalleryPage.getGalleryImage1());

				GalleryPageFunctions.showCaption();
			} else if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
				GalleryPageFunctions.hideCaption();
				GalleryPageFunctions.showCaption();
			} else {
				GalleryPageFunctions.showCaption();
				GalleryPageFunctions.hideCaption();
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Accessing Gallery Elements" + E.getMessage());

		}
	}

	public static void testArticleVideoPage() {
		UMReporter.log(LogStatus.INFO, "Testing Article Video Page");

		ArticlePageFunctions.testheader();
		ArticlePageFunctions.testArticleAuthor();
		ArticlePageFunctions.testArticleVideo();

	}

	public static void testArticleVideo() {

		WrapperMethods.verifyElement(ArticlePage.getArticleEmbVideoButton(), "The video button ");
		WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.getVideoCaption()).getText().isEmpty(),
				"the video caption is displayed:"
						+ WrapperMethods.getWebElement(ArticlePage.getVideoCaption()).getText(),
				"the video caption is not displayed");
		WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.getVideoTime()).getText().isEmpty(),
				"the video time is displayed:" + WrapperMethods.getWebElement(ArticlePage.getVideoTime()).getText(),
				"the video time is not displayed");
		WrapperMethods.clickJavaScript(ArticlePage.getArticleEmbVideoButton());
		WrapperMethods.waitForPageLoaded();
		WrapperMethods.assertIsTrue(ArticlePage.getVideoplaying().size() != 0, "the video is playing",
				"the video is not playing");

	}

	public static void testArticleGalleryEmbedded() {

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
			WrapperMethods.verifyElement(GalleryPage.getGalleryEmIcon(), "Embedded Gallery Icon (Camera Icon) ");

			WrapperMethods.clickJavaScript(GalleryPage.getGalleryEmIcon());
			// gallery.wait(5);
		}
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.clickJavaScript(GalleryPage.getGalleryEmIcon());

			GalleryPageFunctions.galleryCaptionsOpenTest();
		} else if (ConfigProvider.getConfig("Platform").equals("Tablet")) {
			GalleryPageFunctions.showCaption();
			GalleryPageFunctions.hideCaption();
		} else if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			GalleryPageFunctions.hideCaption();
			GalleryPageFunctions.showCaption();
		} else {
			try {
				// WrapperDef.click(gallery.readMore(wf.go()));
				WrapperMethods.clickJavaScript(GalleryPage.readMore());
			} catch (Exception E) {
				UMReporter.log(LogStatus.INFO, "READ MORE" + E.getMessage());
			}
			GalleryPageFunctions.galleryCaptionsClosedTest();
		}

		GalleryPageFunctions.clickGalleryPrev();
		GalleryPageFunctions.clickGalleryNext();

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")
				|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.verifyElement(GalleryPage.getGalleryCloseButton(), "Embedded Gallery Close (X) Button ");
			UMReporter.log(LogStatus.INFO, "Click on Close Button");
			WrapperMethods.clickJavaScript(GalleryPage.getGalleryCloseButton());
			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement((GalleryPage.getGalleryCloseButton())).isDisplayed(),
					"Embedded Expandable Gallery is Closed Sucessfully",
					"Embedded Expandable Gallery is not Closed Sucessfully");
		}
	}

	public static void testPinnedVideoNotListed() {

		try {
			// WrapperMethods.getWebElement(BasePage.getVideoElementPinned()).click();
			WrapperMethods.waitForElement(ArticlePage.getVideoplayingg(), 20, "Video is playing",
					"Video is not playing");

			WrapperMethods.scrollDown();
			WrapperMethods.scrollDown();
			WrapperMethods.scrollDown();

			UMReporter.log(LogStatus.WARNING,
					"Testing Of Pinned Video - Negative scenarios is skipped as the player started");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL,
					"Pinned Video is not displayed - when the scrolling is done before Video load" + e.getMessage());
		}
	}

	public static void validateEmbeddedbutton() {
		try {
			WrapperMethods.clickJavaScript(VideoLeafPage.getVideoEmbed());
			System.out.println("first click");
			WrapperMethods.waitForVisiblity(VideoLeafPage.embeddedtextbox(), "Embedded text Box is Visible",
					"Embedded Text Box is not visible");
		} catch (Exception e) {
			WrapperMethods.click(VideoLeafPage.getVideoEmbed(), "Video Embedded");
			System.out.println("second click");
		}
		WrapperMethods.contains_Text_Attribute(VideoLeafPage.embeddedtextbox(), "value", "iframe",
				"Iframe element is available in the text value", "Iframe tag is not available in the text value");
		WrapperMethods.contains_Text_Attribute(VideoLeafPage.embeddedtextbox(), "value", "width",
				"width attribute is available in the text value", "width attribute is not available in the text value");
		WrapperMethods.contains_Text_Attribute(VideoLeafPage.embeddedtextbox(), "value", "height",
				"height attribute is available in the text value",
				"height attribute is not available in the text value");
		WrapperMethods.contains_Text_Attribute(VideoLeafPage.embeddedtextbox(), "value", "416",
				"width attribute value is 416 in the text value",
				"width attribute value is not as expected in the text value");
		WrapperMethods.contains_Text_Attribute(VideoLeafPage.embeddedtextbox(), "value", "234",
				"height attribute value  is 234 in the text value",
				"height attribute value  is not as expected in the text value");
	}

	public static void SearchIconValidation() {

		WrapperMethods.verifyElement(ArticlePage.SearchIconEntertainment(), "Search Icon");

		WrapperMethods.click(ArticlePage.SearchIconEntertainment(), "Search icon");

		WrapperMethods.verifyElement(ArticlePage.SearchIconInputBox(), "Search Input Box");

	}

	public static void LiveblogHeaderVal() {

		try {
			WrapperMethods.verifyElement(ArticlePage.getArticleHeaderTitle(), "Header Title ");
			WrapperMethods.textNotEmpty(ArticlePage.getArticleHeaderTitle(), "Header Title");
			WrapperMethods.contains_Text_Attribute(ArticlePage.getArticleHeaderTitle(), "font-weight", "300",
					"Header Title is not contains bold text", "Header Title is contains bold text");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Live blog Header section");
		}
	}

	public static void ArticleTimestampAlignment() {

		WrapperMethods.verifyElement(ArticlePage.getArticleHeaderTitle(), "Article HeadLine");

		WrapperMethods.verifyElement(ArticlePage.getArticleTime(), "Article Time Stamp");

		class Local {
		}
		;
		String name = Local.class.getEnclosingMethod().getName();
		System.out.println("Method Name " + name);
		String platform = ConfigProvider.getConfig("Platform");
		String browser = ConfigProvider.getConfig("Browser").toLowerCase();
		String specFn = "Articlealignment.spec";
		String galenName = "galen-" + specFn + "-" + browser;
		TestDevice device = new TestDevice(platform, Arrays.asList(platform));
		try {
			GalenTestBase.checkLayout(specFn, device.getTags(), name, galenName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void RSSFeedRightRail() {

		try {
			WrapperMethods.verifyElement(ArticlePage.CoverageContainerRSS(), "RSS Container");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Live blog Header section");
		}
	}

	public static void ArticleSpeakableTimestamp() {

		WrapperMethods.assertIsTrue(ArticlePage.ArticleSpeakableParagraph().size() == 3,
				"Speakable Class is preset in the 3 paragraphs as expected",
				"Speakable Class is not preset in 3 paragraphs");
		System.out.println(ArticlePage.ArticleSpeakableParagraph().size());
		System.out.println(ArticlePage.ArtileAllParagraph().get(4).getAttribute("class"));
		WrapperMethods.assertIsTrue(
				ArticlePage.ArtileAllParagraph().get(3).getAttribute("class").equalsIgnoreCase("zn-body__paragraph"),
				"The 4th Paragraph dosen't contain the Speakable class",
				"The 4th Paragraph contains the Speakable class");
		System.out.println(WrapperMethods.getWebElement(ArticlePage.ScriptSpecificationJson()).getText());
		WrapperMethods.contains_Text(ArticlePage.ScriptSpecificationJson(), ".speakable",
				"The Page contains .speakable tag in the Script Tag",
				"The Page does not contain .speakable tag in the Script Tag");

	}

	public static void liveFyreCarousel() {

		WrapperMethods.verifyElement(ArticlePage.getCarousel(), "Live Fyre Carousel ");

		WrapperMethods.verifyElement(ArticlePage.getCarouselbackbtn(), "Live Fyre Carousel Backward button ");

		WrapperMethods.verifyElement(ArticlePage.getCarouselfrwdbtn(), "Live Fyre Carousel Forward button ");

		WrapperMethods.scrollDown();

		WrapperMethods.verifyElement(ArticlePage.getArticleNavTitle(), "After Scroll down the page, Navigation bar ");

		if (WrapperMethods.getWebElement(ArticlePage.getArticleNavTitle()).isEnabled()) {
			WrapperMethods.verifyElement(ArticlePage.getArticleNavTitle(),
					"After Scroll down the page, Navigation bar ");
		}
		// article.wait(2);
		WrapperMethods.scrollIntoViewByElement(By.xpath("//div[@class='container kc-wrap']"));
		WrapperMethods.clickJavaScript(ArticlePage.getCarouselfrwdbtn());
		// article.wait(2);
		WrapperMethods.clickJavaScript(ArticlePage.getCarouselItem());
		// article.wait(3);

		WrapperMethods.verifyElement(ArticlePage.getVideo(), "Video ");

		Actions action = new Actions(DriverFactory.getCurrentDriver());
		action.sendKeys(Keys.ESCAPE);

		// article.wait(3);
		WrapperMethods.verifyElement(ArticlePage.getCarousel(), "After pressed the ESC Live Fyre Carousel ");
	}

	public static void loginValidationGoogle() {

		try {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
			WrapperMethods.waitForPageLoaded();
			// MethodDef.navUrl(loginurl);
			WrapperMethods.waitForPageLoaded();
			UMReporter.log(LogStatus.INFO, "User is already log in and hence logging out");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "User is not logged in");
		}
		if (ConfigProvider.getConfig("Browser").equals("SAFARI")) {
			UMReporter.log(LogStatus.INFO, "Social POP UP is not supported by SAFARI");
		} else {
			try {
				int count = 0;
				String wParentHandle = DriverFactory.getCurrentDriver().getWindowHandle();
				UMReporter.log(LogStatus.INFO, "Google+ Login Testing on My CNN Page");
				// Logging into Google+
				WrapperMethods.click(HomePage.getMycnnGoogleLogin(), "My CNN Google Login");
				HomePage.wait(15);
				WrapperMethods.waitForPageLoaded();
				try {
					for (String handle : DriverFactory.getCurrentDriver().getWindowHandles()) {
						DriverFactory.getCurrentDriver().switchTo().window(handle);
						UMReporter.log(LogStatus.INFO, "Google+ Login Testing on My CNN Page");

						Reporter.log(DriverFactory.getCurrentDriver().getTitle());
						if (DriverFactory.getCurrentDriver().getTitle().toString().contains("Google")) {
							UMReporter.log(LogStatus.INFO,
									"Inside window" + DriverFactory.getCurrentDriver().getTitle());
							WrapperMethods.sendKeys(HomePage.getMycnnGoogleEmail(), "turnercnn1@gmail.com");
							WrapperMethods.click(HomePage.getMycnnGoogleNext(), "My CNN Google Next");
							WrapperMethods.sendKeys(HomePage.getMycnnGooglePass(), "cnnmail1");
							WrapperMethods.click(HomePage.getMycnnGoogleSignin(), "My CNN Google SignIn");
							DriverFactory.getCurrentDriver().switchTo().window(wParentHandle);
							Thread.sleep(20000);
						}

					}
				}

				catch (Exception e) {
					UMReporter.log(LogStatus.INFO, "User is already logged in Google");
				}

				if (!ConfigProvider.getConfig("Browser").startsWith("IE"))
					HomePageFunctions.testSocialLogin();
				UMReporter.log(LogStatus.PASS, "Google MyCNN Login worked fine");
				count++;

				if (count == 0) {
					UMReporter.log(LogStatus.ERROR, "Social Share pop up didnt open");
				}
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Google accessing Google Mycnn");
			}
		}

	}

	public static void checkwhiteBGgallerypage() {
		WrapperMethods.verifyCssValue(ArticlePage.getarticlebackground(), "background-color", "rgba(254, 254, 254, 1)",
				"The Article with White Background");

		WrapperMethods.contains_Text_Attribute(ArticlePage.PagetopGallerydatacut(), "data-cut-format", "9:16",
				"The Page Top gallery has 9:16 data cut as expected",
				"The Page Top gallery does not have 9:16 data cut -Unexpected");

		WrapperMethods.verifyElement(ArticlePage.previousarrowowl(), "The Previous owl button ");

		WrapperMethods.verifyElement(ArticlePage.nextarrowowl(), "The Next owl button ");

	}

	public static void validategalleryCaptionafterresize() {

		WrapperMethods.isDisplayed(ArticlePage.Getarticlecaptioncontent(), "The Caption is displayed before re-size",
				"The Caption is not displayed before re-size");
		WrapperMethods.hasContent(ArticlePage.Getarticlecaptioncontent(), "The Caption has content before re-size",
				"The Caption does not have content before re-size");
		MethodDef.isVisible(ArticlePage.Getarticlecaptioncontent());
		WrapperMethods.startWith(ArticlePage.Getarticlecaptioncontent(), "A Chinese J-31 stealth fighter",
				"The Caption starts with the the correct content after resize",
				"The Caption does starts with the the correct content after resize");

		WrapperMethods.containsText(ArticlePage.Getarticlecaptioncontent(), " November 9",
				"The Caption has the correct content after resize",
				"The Caption does not have the correct content after resize");

		Dimension dimension = new Dimension(360, 640);
		DriverFactory.getCurrentDriver().manage().window().setSize(dimension);
		MethodDef.explicitWaitVisibilityXpath("//div[@class='metadata ']", 20, "Metadata is visible",
				"Metadata is not visible");
		WrapperMethods.isDisplayed(ArticlePage.Getarticlecaptioncontent(),
				"The Caption is displayed after re-size to 360*640",
				"The Caption is not displayed after re-size to 360*640");
		WrapperMethods.hasContent(ArticlePage.Getarticlecaptioncontent(),
				"The Caption has content after re-size to 360*640",
				"The Caption does not have content after re-size to 360*640");
		MethodDef.isVisible(ArticlePage.Getarticlecaptioncontent());
		WrapperMethods.startWith(ArticlePage.Getarticlecaptioncontent(), "A Chinese J-31 stealth fighter",
				"The Caption starts with the the correct content after resize",
				"The Caption does starts with the the correct content after resize");

		WrapperMethods.containsText(ArticlePage.Getarticlecaptioncontent(), "November 9",
				"The Caption has the correct content after resize",
				"The Caption does not have the correct content after resize");
		Dimension seconddimension = new Dimension(800, 1024);
		DriverFactory.getCurrentDriver().manage().window().setSize(seconddimension);
		MethodDef.explicitWaitVisibilityXpath("//div[@class='metadata ']", 20, "Metadata is visible",
				"Metadata is not visible");

		WrapperMethods.isDisplayed(ArticlePage.Getarticlecaptioncontent(),
				"The Caption is displayed after re-size to 800*1024",
				"The Caption is not displayed after re-size to 800*1024");
		WrapperMethods.hasContent(ArticlePage.Getarticlecaptioncontent(),
				"The Caption has content after re-size to 800*1024",
				"The Caption does not have content after re-size to 800*1024");
		MethodDef.isVisible(ArticlePage.Getarticlecaptioncontent());
		WrapperMethods.startWith(ArticlePage.Getarticlecaptioncontent(), "A Chinese J-31 stealth fighter",
				"The Caption starts with the the correct content after resize",
				"The Caption does starts with the the correct content after resize");

		WrapperMethods.containsText(ArticlePage.Getarticlecaptioncontent(), "November 9",
				"The Caption has the correct content after resize",
				"The Caption does not have the correct content after resize");
	}

	public static void LiveFyreImageUnderlineCaption() {
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live Blog Image with Underlined Caption</b>");
			WrapperMethods.scrollIntoViewByElement(By.xpath(
					"//div[@class='s-image-wrapper']//div[@class='s-upload-image-caption']/p/u/ancestor::div[@class='s-element-content s-element-image']"));

			WrapperMethods.isDisplayed(ArticlePage.liveBlogUnderlinedImage(), "Underlined caption Image is displayed",
					"Underlined caption Image is not displayed");

			WrapperMethods.isDisplayed(ArticlePage.liveBlogUnderlinedCaption(), "Underlined caption is displayed",
					"Underlined caption is not displayed");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("color")
							.equals("rgba(89, 89, 89, 1)"),
					"The color of the Underlined caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("color"),
					"The color of the Underlined caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-size")
							.equals("14px"),
					"The font size of the Underlined caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-size"),
					"The font size of the Underlined caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the Underlined caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-weight"),
					"The font weight of the Underlined caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-weight"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("line-height")
							.equals("23px"),
					"The line-height of the Underlined caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("line-height"),
					"The line-height of the Underlined caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("line-height"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-family")
							.contains("CNN"),
					"The font family of the Underlined caption is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-family"),
					"The font family of the Underlined caption is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogUnderlinedCaption()).getCssValue("font-family"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogCaption()).getCssValue("opacity").equals("1"),
					"The Opacity is 100%  ", "The Opacity is not 100% ");

			WrapperMethods.isDisplayed(ArticlePage.liveBlogIntialRoundShape(), "Round shape with Initial is displayed",
					"Round shape with Initial is not displayed");

			WrapperMethods.scrollIntoViewByElement(By.xpath("//span[@class='storifycon-like']"));
			WrapperMethods.isDisplayed(ArticlePage.starImage(), "Star(Like) Image is displayed",
					"Star(Like) Image is not displayed");

		} catch (Exception e) {
			MethodDef.failLog(e.getMessage(), "Error Accessing Live Blog Image with Underlined Caption");
		}

	}

	public static void articleliveblog() {
		WebDriver d = DriverFactory.getCurrentDriver();
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live blog:</b>");
			WrapperMethods.isDisplayed(ArticlePage.livebloglikeicon(d), "The Live blog like icon is visible",
					"The live blog like icon is not visible");
			WrapperMethods.isDisplayed(ArticlePage.livebloglikecount(d), "The Live blog like count is visible",
					"The live blog like count is not visible");
			WrapperMethods.isDisplayed(ArticlePage.liveblogsharebutton(d), "The Live blog share button is visible",
					"The live blog share button is not visible");
			WrapperMethods.click(ArticlePage.liveblogsharebutton(d), "Clicked the Live blog share button",
					"Not able to click the Live blog share button");

			WrapperMethods.isDispWE(WrapperMethods.getfirstvisibleElement(ArticlePage.liveblogtextboxs(d)),
					"The live blog text box is displayed", "The live blog text box is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getfirstvisibleElement(ArticlePage.liveblogfacebk(d)).isDisplayed(),
					"The live blog Facebook icon is displayed", "The live blog Facebook icon is not displayed");
			WrapperMethods.assertIsTrue(
					WrapperMethods.getfirstvisibleElement(ArticlePage.liveblogtwittr(d)).isDisplayed(),
					"The live blog Twitter icon is displayed", "The live blog Twitter icon is not displayed");

		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}

	public static void liveBlogSocialShareButtonText() {
		WebDriver d = DriverFactory.getCurrentDriver();
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Live blog:</b>");
			WrapperMethods.isDisplayed(ArticlePage.liveBlogShareButtonText(d),
					"The Live blog social share button text is visible",
					"The live blog social share button text is not visible");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("color")
							.equals("rgba(166, 166, 166, 1)"),
					"The color of the Socail Share Text is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("color"),
					"The color of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("color"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-size")
							.equals("14px"),
					"The font size of the Socail Share Text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-size"),
					"The font size of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style")
							.equals("normal"),
					"The font style of the Socail Share Text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style"),
					"The font style of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight")
							.equals("300"),
					"The font weight of the Socail Share Text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight"),
					"The font weight of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height")
							.equals("normal"),
					"The line-height of the Socail Share Text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height"),
					"The line-height of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family")
							.contains("sans-serif"),
					"The font family of the Socail Share Text is : " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family"),
					"The font family of the Socail Share Text is not " + WrapperMethods
							.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family"));
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Socail Share Text");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}
	}
	public static void liveBlogAvatarLogo() {
		WebDriver d= DriverFactory.getCurrentDriver();
		try {

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("height").equalsIgnoreCase("35px"),
					"The height of the avatar icon is correct" + WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("height"),
					"The height of the avatar icon is incorrect" + WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("height"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("width").equals("35px"),
					"The height of the avatar icon is correct" + WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("width"),
					"The height of the avatar icon is incorrect" + WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("width"));
			WrapperMethods.assertIsTrue(					(WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("border-bottom-left-radius").contains("50%")
							&& (WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("border-bottom-right-radius").contains("50%"))
							&& (WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("border-top-left-radius").contains("50%"))
							&& (WrapperMethods.getWebElement(ArticlePage.AvatarLogo(d)).getCssValue("border-top-right-radius").contains("50%"))),
					"the radius of the avtar icon is as expected", "The radius of the avatar icon is not as expected");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Avatar logo");
			UMReporter.log(LogStatus.INFO,e.getMessage());
		}
	}
	
	public static void liveBlogImageCaption() {
		WebDriver d= DriverFactory.getCurrentDriver();
		try {
			Reporter.log("<br>Info: Testing with Live blog:");
			WrapperMethods.isDisplayed(ArticlePage.liveBlogImageCaption(d),
					"The Live blog Image caption is visible", "The live blog Image caption is not visible");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("color").equals("rgba(153, 153, 153, 1)"),
					"The color of the caption is : " + WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("color"),
					"The color of the caption  is not " + WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("color"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("font-size").equals("14px"),
					"The font size of the caption is : " + WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("font-size"),
					"The font size of the caption is not " + WrapperMethods.getWebElement(ArticlePage.liveBlogImageCaption(d)).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style").equals("normal"),
					"The font style of the caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style"),
					"The font style of the caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-style"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight").equals("300"),
					"The font weight of the caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight"),
					"The font weight of the caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-weight"));
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height").equals("normal"),
					"The line-height of the caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height"),
					"The line-height of the caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("line-height"));
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family").contains("sans-serif"),
					"The font family of the caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family"),
					"The font family of the caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.liveBlogShareButtonText(d)).getCssValue("font-family"));

			WrapperMethods.isDisplayed(ArticlePage.liveBlogImage(), "The Live blog Image is visible",
					"The live blog Image is not visible");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.StrikeLabel()).getCssValue("line-height").equals("23px"),
					"The Line Height of the Striked caption is : " + WrapperMethods.getWebElement(ArticlePage.StrikeLabel()).getCssValue("line-height"),
					"The Line Height of the Striked caption is not expcted it shows: "
							+ WrapperMethods.getWebElement(ArticlePage.StrikeLabel()).getCssValue("line-height"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogImageWithStrike()).getCssValue("opacity").equals("1"),
					"The Opacity is 100%  ", "The Opacity is not 100% ");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live fyre comments :Image caption");
			UMReporter.log(LogStatus.INFO,e.getMessage());
		}
	}
	public static void LiveFyreImageBoldCaption() {
		try {
			UMReporter.log(LogStatus.INFO,"<b>Info: Testing with Live Blog Image with Bold Caption</b>");
			WrapperMethods.scrollIntoViewByElement(
					By.xpath("//div[@class='s-image-wrapper']//div[@class='s-upload-image-caption']/p/b"));
			// ===
			WrapperMethods.isDisplayed(ArticlePage.liveBlogImage(), "Bolded caption Image is displayed",
					"Bolded caption Image is not displayed");
			WrapperMethods.isDisplayed(ArticlePage.liveBlogImageCaption(DriverFactory.getCurrentDriver()), "Bolded caption is displayed",
					"Bolded caption is not displayed");
			WrapperMethods.scrollIntoViewByElement( By.xpath("//div[contains(@class,'s-element-content')]//p/b"));
			WrapperMethods.isDisplayed(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver()),
					"Live Fyre Bold caption is displayed", "Live Fyre Bold caption is not displayed");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("color").equals("rgba(89, 89, 89, 1)"),
					"The color of the Bold caption is : " + WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("color"),
					"The color of the Bold caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("color"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-size").equals("14px"),
					"The font size of the Bold caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-size"),
					"The font size of the Bold caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-size"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-weight").equals("bold"),
					"The font weight of the Bold caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-weight"),
					"The font weight of the Bold caption is not "
							+WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-weight"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("line-height").equals("23px"),
					"The line-height of the Bold caption is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("line-height"),
					"The line-height of the Bold caption is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("line-height"));

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-family").contains("CNN"),
					"The font family of the Bold Paragraph is : "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-family"),
					"The font family of the Bold Paragraph is not "
							+ WrapperMethods.getWebElement(ArticlePage.getLiveFyreBoldParagraph(DriverFactory.getCurrentDriver())).getCssValue("font-family"));
							WrapperMethods.scrollIntoViewByElement(
					By.xpath("//div[@class='s-element-content s-element-image']/ancestor::div[1]"));
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(ArticlePage.liveBlogCaption()).getCssValue("opacity").equals("1"),
					"The Opacity is 100%  ", "The Opacity is not 100% ");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Live Blog Image with Bold Caption");
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}

	}
	public static void LiveblogFooterWeatherCard() {

		try {
			WrapperMethods.scrollDown();
			WrapperMethods.isDisplayed(ArticlePage.getLocation(), "Footer Weather Location  is displayed",
					"Footer Weather Location is not displayed");
			WrapperMethods.isDisplayed(ArticlePage.getWeatherIcon(), "Footer Weather Icon is displayed",
					"Footer Weather Icon is not displayed");
			WrapperMethods.isDisplayed(ArticlePage.getTemperature(), "Footer Weather Temperature is displayed",
					"Footer Weather Temperature is not displayed");
			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(ArticlePage.getTemperature()).getText().isEmpty(),
					"Footer Weather Temperature is Properly loaded",
					"Footer Weather Temperature is not Properly loaded");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing in LiveBlog Footer Weather Section");
			UMReporter.log(LogStatus.INFO, e.getMessage());

		}
	}
	public static void emailShareValidations() {
		try {
			clickEmailShare();
			emailSharValidations();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
			
		}
	}
	private static void clickEmailShare() {
		MethodDef.explicitWaitVisibility(ArticlePage.getEmailShareDesktop(),"EmailShare is visible","EmailShare is not visible");

		WrapperMethods.click(ArticlePage.getEmailShareDesktop(),"EmailShare is clicked","EmailShare is not clicked");
	}
	
	public static void emailSharValidations() {
		try {
			MethodDef.explicitWaitVisibility(BasePage.getEmailShareTo(), "EmailShareTo is visible","EmailShareTo is not visible");

			WrapperMethods.clear(BasePage.getEmailShareTo());
			WrapperMethods.sendKeys(BasePage.getEmailShareTo(), "turnercnnmail1@cnn.com");
			WrapperMethods.clear(BasePage.getEmailShareToUr());

			WrapperMethods.sendKeys(BasePage.getEmailShareToUr(), "turnercnnmail1@cnn.com");
			WrapperMethods.clear(BasePage.getEmailShareToMsg());

			WrapperMethods.sendKeys(BasePage.getEmailShareToMsg(), "Custom Message");

			MethodDef.passLog("Email Information are entered successfully");

			WrapperMethods.click(BasePage.getSendEmail(),"Clicked the Send email button","Not able to click the send mail button");
			MethodDef.explicitWaitVisibility(BasePage.getSendEmailCaptcha(), "SendEmailCaptcha is visible","SendEmailCaptcha is not visible");
			UMReporter.log(LogStatus.PASS,"Email Send Confirmation - Captcha screen is displayed correctly");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Email share icons");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
}