package um.testng.test.pom.functions;

import org.testng.ITestContext;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.MoneyPage;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class MoneyPageFunctions {

	
	public void testMoneyPage() {
		
		try {
			WrapperMethods.moveToElement(MoneyPage.Skip());
			UMReporter.log(LogStatus.INFO, "Ad was present and Skip button was pressed");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Ad was not displayed");
		}

	}
}
