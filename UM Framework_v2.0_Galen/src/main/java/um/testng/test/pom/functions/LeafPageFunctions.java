package um.testng.test.pom.functions;

import org.openqa.selenium.WebDriver;

import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.WrapperMethods;

public class LeafPageFunctions {

	public static void validateOutbrainAD(WebDriver go) {
			WrapperMethods.page_scrollDown();
			VideoLeafPage.wait(4);
			String adlen = WrapperMethods.getWebElement(VideoLeafPage.secondZoneAdContainer())
					.getCssValue("height").split("px")[0];

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(VideoLeafPage.secondZoneAdContainer()).getCssValue("width")
							.isEmpty(),
					"the width of the AD is : "
							+ WrapperMethods.getWebElement(VideoLeafPage.secondZoneAdContainer()).getCssValue(
									"width"), "");

			if (adlen.equals("600")) {

				WrapperMethods.assertIsTrue(!adlen.isEmpty(),
						"the height of the of the AD is : "
								+ WrapperMethods.getWebElement(VideoLeafPage.secondZoneAdContainer())
										.getCssValue("height"), "");

			} else {

				WrapperMethods.assertIsTrue( !adlen.isEmpty(),
						"the height of the of the AD is : "
								+ WrapperMethods.getWebElement(VideoLeafPage.secondZoneAdContainer())
										.getCssValue("height"), "");

			}

	}
	
	public static void validateZone3(WebDriver go) {

			WrapperMethods.isDisplayed(VideoLeafPage.getZone3(),
					"Zone 3 is displayed", "Zone 3 is not displayed");

			WrapperMethods.isDisplayed(VideoLeafPage.getZone3Container1(),
					"Zone 3 - Container 1 is displayed",
					"Zone 3- Container 1 is not displayed");

			WrapperMethods.isDisplayed( VideoLeafPage.getZone3Container2(),
					"Zone 3-- Container 2 is displayed",
					"Zone 3 -- Container 2 is not displayed");

			String classValue = WrapperMethods.getWebElement(VideoLeafPage.getZone3()).getAttribute("class");

			WrapperMethods.assertIsTrue( classValue.contains("t-light"),
					"The container theme is light",
					"The theme of the container is not light");

			WrapperMethods.assertIsTrue( classValue.contains("zn-balanced"),
					"The layout of the zone theme is BALANCED",
					"The layout of the zone theme is not BALANCED");

			WrapperMethods.assertIsTrue(
					classValue.contains("zn-has-two-containers"),
					"This Zone 3 contains 2 containers",
					"This Zone 3 doesn't have 2 containers");

			WrapperMethods.assertIsTrue(VideoLeafPage.getZone3Container1Card()
					.size() == 1, "the container 1 is having one card",
					"the container 1 is not having one card");

			WrapperMethods.assertIsTrue(VideoLeafPage.getZone3Container2Card()
					.size() == 4, "the container 2 is having 4 cards",
					"the container 2 is not having 4 card");

			String layout = WrapperMethods.getWebElement(VideoLeafPage.getZone3Container2Layout()).getAttribute(
					"class");

			WrapperMethods.assertIsTrue( layout.contains("cn-grid"),
					"The layout of the 2nd Container is GRID",
					"The layout of the 2nd Container is not GRID");

		}
	public static void validateZone5( WebDriver go) {

			WrapperMethods.page_scrollDown();
			WrapperMethods.page_scrollDown();
			
			WrapperMethods.isDisplayed( VideoLeafPage.getZone5(),
					"Zone 5 is displayed", "Zone 5 is not displayed");

			WrapperMethods.assertIsTrue( !WrapperMethods.getWebElement(VideoLeafPage.getZone5Label()).getText()
					.isEmpty(),
					"Zone 5- Label is displayed and the Label name is : "
							+WrapperMethods.getWebElement(VideoLeafPage.getZone5Label()).getText(),
					"Zone 5- Label is not displayed");

			WrapperMethods.isDisplayed( VideoLeafPage.getZone5Container(),
					"Zone 5-- Container  is displayed",
					"Zone 5 -- Container  is not displayed");

			String classValue = WrapperMethods.getWebElement(VideoLeafPage.getZone5()).getAttribute("class");

			 WrapperMethods.assertIsTrue(
					classValue.contains("zn-balanced"),
					"The layout of the zone theme is BALANCED",
					"The layout of the zone theme is not BALANCED");

			 WrapperMethods.assertIsTrue(
					classValue.contains("zn-has-one-container"),
					"This Zone 5 should have only 1 containers",
					"This Zone 5 doesn't have 1 containers");

			String layout = WrapperMethods.getWebElement(VideoLeafPage.getZone5ContainerLayout()).getAttribute(
					"class");

			WrapperMethods.assertIsTrue(layout.contains("cn-grid"),
					"The layout of the  Container is GRID",
					"The layout of the  Container is not GRID");

			WrapperMethods.assertIsTrue(VideoLeafPage.getZone5ContainerCard()
					.size() == 9, "the container should have 9 cards",
					"the container is not having 9 cards");
	}
	
	public static void validateZone3INTL() {

		WrapperMethods.isDisplayed( VideoLeafPage.getZone3(),
					"Zone 3 INTL is displayed", "Zone 3 is not displayed");

			String classValue = WrapperMethods.getWebElement(VideoLeafPage.getZone3()).getAttribute("class");

			WrapperMethods.assertIsTrue(classValue.contains("t-light"),
					"Zone 3 -INTL The container theme is light",
					"Zone 3-INTL The theme of the container is not light");

			WrapperMethods.assertIsTrue(
					classValue.contains("zn-has-two-containers"),
					"This Zone 3 INTL contains 2 containers",
					"This Zone 3 INTL doesn't have 2 containers");

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(VideoLeafPage.getZone3()).getAttribute("data-vr-zone")
							.equalsIgnoreCase("zone-0-2"),
					"The Zone-3 INTL is displayed correctly",
					"The Zone-3 INTL is not displayed correctly");


	}

	public static void validateINTLVideoZone5() {

		WrapperMethods.isDisplayed( VideoLeafPage.getZone5(),
					"Zone 5 is displayed in INTL",
					"Zone 5 is not displayed in INTL");

		WrapperMethods.assertIsTrue( !WrapperMethods.getWebElement(VideoLeafPage.getZone5Label()).getText()
					.isEmpty(),
					"Zone 5-INTL Label is displayed and the Label name is : "
							+ WrapperMethods.getWebElement(VideoLeafPage.getZone5Label()).getText(),
					"Zone 5-INTL Label is not displayed");

			WrapperMethods.isDisplayed( VideoLeafPage.getZone5Container(),
					"Zone 5-INTL- Container  is displayed",
					"Zone 5 -INTL- Container  is not displayed");

			String classValue = WrapperMethods.getWebElement(VideoLeafPage.getZone5()).getAttribute("class");

			WrapperMethods.assertIsTrue(
					classValue.contains("zn-has-one-container"),
					"This INTL Zone 5 has 1 container as expected",
					"This INTL Zone 5 doesn't have 1 container");

			String layout = WrapperMethods.getWebElement(VideoLeafPage.getZone5ContainerLayout()).getAttribute(
					"class");
			
			WrapperMethods.assertIsTrue( layout.contains("cn-grid"),
					"The layout of the  Container is GRID",
					"The layout of the  Container is not GRID");

	}	
}