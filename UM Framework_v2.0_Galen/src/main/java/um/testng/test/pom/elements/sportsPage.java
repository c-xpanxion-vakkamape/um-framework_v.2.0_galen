package um.testng.test.pom.elements;

import org.openqa.selenium.By;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class sportsPage extends BasePage {

	public static By getAdelement() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#skip>a");
	}

	public static By getBleachers() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-header-logo,a#br-logo");
	}

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	/**
	 * this defines the twitter widget iframe in international sports page
	 */
	public static By getTweetWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".column iframe.twitter-timeline");
	}

	/**
	 * defines the title of the twitter widget
	 */
	public static By getTweetTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".timeline-Header-title");
	}

	/**
	 * defines the twitter follow button
	 */
	public static By getTweetFollow() {
		return WrapperMethods.locatorValue(Locators.CSS, ".timeline-header a.follow-button");
	}

	public static By getSportTickerSection() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'m-sports-ticker')]//ul[contains(@class,'m-sports-ticker__list')]//li[contains(@class,'m-sports-ticker__list-item')]//a[contains(@class,'m-sports-ticker__section-link')]");
	}

	public static By getSportTickerArticle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				" //div[contains(@class,'m-sports-ticker')]//ul[contains(@class,'m-sports-ticker__list')]//li[contains(@class,'m-sports-ticker__list-item')]//a[contains(@class,'m-sports-ticker__article-link')]");
	}

	public static By getSportTicker() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'m-sports-ticker')]");
	}

}
