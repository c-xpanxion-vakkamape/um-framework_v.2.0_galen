package um.testng.test.pom.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;
import java.util.List;

public class PoliticsPage extends SectionFrontsPage {
	public PoliticsPage(WebDriver driver) {
		super();
	}

	static WebDriver driver = DriverFactory.getCurrentDriver();

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='logo-links__politics'][@data-analytics='header_logo']");
		// return
		// go.findElement(Locators.XPATH,
		// "//a[@class='navigation-cnn-politics-logo__link'][@title='CNN
		// Politics']");
	}

	public static By getCNNRedLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//nav[@class='nav js-navigation js-skinny']//a[@class='logo-links__cnn']");
	}

	public static By getCNNPoliticslogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//nav[@class='nav js-navigation js-skinny']//a[@class='logo-links__politics']");
	}

	public static By getCnnBlackLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='footer-cnn-politics-logo']");
	}

	public static By getCnnBlackLogohrefs() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='footer-cnn-politics-logo']/a[@title='CNN Home Page']");
	}

	public static By getCnnRedLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='navigation-cnn-politics-logo__link'][@title='CNN Home Page']");
	}

	public static By getPoliticsSearch() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='search-input-field']");
	}

	public static By getPoliticsLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='navigation-cnn-politics-logo__link'][@title='CNN Politics']");

	}

	public static By getCurrentSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".buckets");
	}

	public static By secondZoneAdContainer() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@class,'zn-politics-zone-2')]//div[contains(@class,'ad')]");
		// Locators.XPATH, "//div[contains(@class,'SF_6')]");
	}

	public static By secondZoneOutbrain() {
		return WrapperMethods.locatorValue(
				// Locators.XPATH,
				// "//section[contains(@class,'zn-politics-zone-2')]//div[contains(@class,'SF_6')]//div[@data-widget-id='SF_6']");
				Locators.XPATH, "//div[contains(@class,'SF_6')]");
	}

	public static By getCnnDeligateEstimate() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.section-header--centered h2");
	}

	public List<WebElement> getCandidateTickerBars() {
		return WrapperMethods.locateElements(Locators.CSS, ".delegate-tracker__row div.tick-bar");
	}

	public List<WebElement> getCandidateTotalDelegatesWon() {
		return WrapperMethods.locateElements(Locators.CSS,
				".delegate-tracker__row div.tick-bar .tick-bar__label-text--large");
	}

	public List<WebElement> getCandidateDelegatesNeededToWin() {
		return WrapperMethods.locateElements(Locators.CSS,
				".delegate-tracker__row div.tick-bar .tick-bar__votes-needed-label div");
	}

	public List<WebElement> getCandidatePlusIconOnTracker() {
		return WrapperMethods.locateElements(Locators.CSS, ".delegate-tracker__row span.table__cell--number a");
	}

	public static By getPeriodicTable() {
		return WrapperMethods.locatorValue(Locators.CSS, ".delegate-tracker__row div.row-map");
	}

	public List<WebElement> getCandidateNamesOnTicker() {
		return WrapperMethods.locateElements(Locators.CSS, ".delegate-tracker__row .tick-bar__label-text--smallcaps a");
	}

	public List<WebElement> getCandidateImages() {
		return WrapperMethods.locateElements(Locators.CSS, ".delegate-tracker__row i.candidate-headshot--circle");
	}

	public List<WebElement> getCandidateImagesHref() {
		return WrapperMethods.locateElements(Locators.CSS, ".delegate-tracker__row div.column-candidate a");
	}

	public static By getPoliticsBlackLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='footer-cnn-politics-logo']");
	}

	public static By getPoliticsBlackLogohref() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='footer-cnn-politics-logo']/a[@title='CNN Politics']");
	}

	public static By getPoliticsHam() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnn-politics-header__navigation-toggle");
	}

	public static By getElectionHeaderLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".header-navigation__logo");
	}

	public static By getElectionHeaderLogoHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".header-navigation__logo");
	}

	public static By getElectionHeaderLogoYear() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"nav.header-navigation__container > a > svg > text:nth-child(4)");
	}

	public static By getElectionHeaderNavBar() {
		return WrapperMethods.locatorValue(Locators.CSS, ".header-navigation__links");
	}

	public static By getElectionRepublicans() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'republicans')]");
	}

	public static By getElectionDemocrats() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'democrats')]");
	}

	public static By getElectionExitPoll() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'exit polls')]");
	}

	public static By getElectionResults() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'results')]");
	}

	public static By getElectionCandidates() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'candidates')]");
	}

	public static By getElectionEvents() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'events')]");
	}

	public static By getElectionPrimariesPlusCaucuses() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'header-navigation__links')]/a[contains(.,'primaries + caucuses')]");
	}

	public static By getElectionStates() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'select-box-native')]/label[contains(.,'state')]");
	}

	public static By getElectionStatesDropDown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'select-box-native')]/div[contains(@class,'select-box-native__toggle')]");
	}

	public static By getPoliticsMobileSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnn-politics-header__search-toggle");
	}

	public static By getMenuElectionResults() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'cnn-politics-header__navigation-item')]/a[contains(.,'Election Results')]");
	}

	public static By getMenu2016() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'cnn-politics-header__navigation-item')]/a[contains(.,'2016')]");
	}

	public static By getMenuNation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'cnn-politics-header__navigation-item')]/a[contains(.,'Nation')]");
	}

	public static By getMenuWorld() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'cnn-politics-header__navigation-item')]/a[contains(.,'World')]");
	}

	public static By getMenuOurTeam() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'cnn-politics-header__navigation-item')]/a[contains(.,'Our Team')]");
	}

	public static By getFooterMenuElectionResults() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'politics-footer__navigation-item')]/a[contains(.,'Election Results')]");
	}

	public static By getFooterMenu2016() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'politics-footer__navigation-item')]/a[contains(.,'2016')]");
	}

	public static By getFooterMenuNation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'politics-footer__navigation-item')]/a[contains(.,'Nation')]");
	}

	public static By getFooterMenuWorld() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'politics-footer__navigation-item')]/a[contains(.,'World')]");
	}

	public static By getFooterMenuOurTeam() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'politics-footer__navigation-item')]/a[contains(.,'Our Team')]");
	}

	public static By getFooterFollowUs() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='footer-cnn-politics__ui-group footer-cnn-politics__ui-group--links footer-cnn-politics__ui-group--social']");
	}

	public static By getPoliticsFooterFacebook() {
		// return WrapperMethods.locatorValue(Locators.XPATH,
		// "//li[@class='link-list__list-item
		// footer-cnn-politics__link-item--social']/a[text()='Facebook
		// page']");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='link-list__list-item footer-cnn-politics__link-item--social']/a[contains(@class, 'footer-cnn-politics__link--facebook')]");
	}

	public static By getPoliticsFooterTwitter() {
		// return WrapperMethods.locatorValue(Locators.XPATH,
		// "//li[@class='link-list__list-item
		// footer-cnn-politics__link-item--social']/a[text()='Twitter feed']");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='link-list__list-item footer-cnn-politics__link-item--social']/a[contains(@class, 'footer-cnn-politics__link--twitter')]");
	}

	public static By getPoliticsFooterYoutube() {
		// return WrapperMethods.locatorValue(Locators.XPATH,
		// "//li[@class='link-list__list-item
		// footer-cnn-politics__link-item--social']/a[text()='Twitter feed']");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='link-list__list-item footer-cnn-politics__link-item--social']/a[contains(@class, 'footer-cnn-politics__link--youtube')]");
	}

	public static By getPoliticsFooterVinefeed() {
		// return WrapperMethods.locatorValue(Locators.XPATH,
		// "//li[@class='link-list__list-item
		// footer-cnn-politics__link-item--social']/a[text()='Twitter feed']");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='link-list__list-item footer-cnn-politics__link-item--social']/a[contains(@class, 'footer-cnn-politics__link--vine')]");
	}

	public static By getPoliticsCopyrightYear() {
		return WrapperMethods.locatorValue(Locators.CSS, ".footer-cnn-politics__legal time");
	}

	public static By getPoliticsCopyright() {
		return WrapperMethods.locatorValue(Locators.CSS, ".footer-cnn-politics__legal");
	}

	public static By getPoliticsCopyrightTurner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='footer-cnn-politics__link'][text()='Turner Broadcasting System, Inc.']");
	}

	public static By getPoliticsCopyrightTerms() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='footer-cnn-politics__link'][text()='Terms of service']");
	}

	public static By getPoliticsCopyrightPrivacy() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='footer-cnn-politics__link'][text()='Privacy guidelines']");
	}

	public static By getheadersharecontainer() {
		return WrapperMethods.locatorValue(Locators.CSS, "#share-me");
	}

	public static By getLandingheadersharecontainer() {
		return WrapperMethods.locatorValue(Locators.CSS, ".navigation-share");
	}

	public static By getElectionLandingNightCap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".newsletter");
	}

	public static By getEventCountDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".hub-countdown-clock");
	}

	public static By getUpcomingEventSchedule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fixed ']/section[@class= 'section-wrapper pad-when-small']");
	}

	public static By getMatchmaker() {
		return WrapperMethods.locatorValue(Locators.CSS, ".promo-matchmaker");
	}

	public static By getPoliticsAppPromoAD() {
		return WrapperMethods.locatorValue(Locators.CSS, ".bleed-layout.promo-politics-app");
	}

	public static By clickshareicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='navigation-share__logo']");
	}

	public static By getheaderEmailshareicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon Email']");
	}

	public static By getheaderFBshareicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon Facebook']");
	}

	public static By getheadertwittershareicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon Twitter']");
	}

	public static By getheadermoreicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon Share']");
	}

	public static By getwinningcandidate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='candidate-name__last candidate-name--winner']");
	}

	public static By getstatesRepublicpartytoggle_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='party-toggle__button'][1]");
	}

	public static By getpartyprogressbar() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='percent-bar__progress']");
	}

	public static By getstatesDemocraticpartytoggle_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='party-toggle__button'][2]");
	}

	public static By getstateslabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[@class='page-heading select-box-native__label']");
	}

	public static By getstatesdropdownarrow() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='select-box-native select-box-native--tight select-box-native--inline']/div[@class='select-box-native__toggle']");
	}

	public List<WebElement> getstatesList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='section-header__column']//select[@class='select-box-native__select']/option");
	}

	public static By getcandidatevotecount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='table__cell race-results-table__vote-count']");
	}

	public static By getElectioncentre_Btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='button-link button-link--hub']");
	}

	public static By getelectionCentretext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='button-link button-link--hub']/span");

	}

	public static By electiontopAdvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id='google_ads_iframe_/8663477/CNN/politics/elections/landing_0__container__']");
	}

	public static By electionbottomAdvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id='google_ads_iframe_/8663477/CNN/politics/elections/landing_3__container__']");
	}

	public static By ElectionAdMobile() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id='google_ads_iframe_/8663477/CNN/politics/elections/landing/hub_0__container__']");
	}

	public static By Electiongridtitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.app-title > div > h1");
	}

	public List<WebElement> Electioncanditaesgrid() {
		return WrapperMethods.locateElements(Locators.CSS, "div.candidate-grid__item");
	}

	public static By Deligatesestimate() {
		return WrapperMethods.locatorValue(Locators.CSS, "section.section.balanced-zone.hub-delegate-tracker");
	}

	public static By repubparty_btn() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section.section.balanced-zone.hub-delegate-tracker > div:nth-child(1) > a");
	}

	public static By democraticparty_btn() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section.section.balanced-zone.hub-delegate-tracker > div:nth-child(2) > a");
	}

	public static By deligatetext() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cnn-politics-notes");
	}

	public static By learnmore_btn() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cnn-politics-notes > a");
	}

	public static By partytoggle() {
		return WrapperMethods.locatorValue(Locators.CSS, "section.section.calendar-hub div.party-toggle");
	}

	public List<WebElement> calendartogglebuttons() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.calendar-hub__party-toggle-container div.party-toggle__button");
	}

	public static By monthdatedot() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.primary-month__date");
	}

	public static By stateline() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.table__row");
	}

	public List<WebElement> calendarDate() {
		return WrapperMethods.locateElements(Locators.CSS, "div.primary-month__headline.text-align-center.text-thin");
	}

	public List<WebElement> getCalendarDateHref() {
		return WrapperMethods.locateElements(Locators.CSS, ".primary-calendar a");
	}

	public static By searchbystatedropdown() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.select-box-native.select-box-native--left > select>option:nth-child(1)");
	}

	public List<WebElement> stateslist() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.select-box-native.select-box-native--left > select>option");
	}

	public static By OutBraninwidget1() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob-widget-section ob-first']");
	}

	public static By OutBrainwidget2() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#outbrain_widget_1");
	}

	public static By Container1Outbrainicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='outbrain_widget_0']//div[@class='ob_what']/a");
	}

	public static By Container2Outbrainicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='outbrain_widget_1']//div[@class='ob_what']/a");
	}

	public static By omeliaoutbrain() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob_what ob-hover']/a");
	}

	public static By Outbrainicon() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.ob_amelia");
	}

	public static By Notesblock() {
		return WrapperMethods.locatorValue(Locators.CSS, "#editorial-notes");
	}

	public static By Notesheader() {
		return WrapperMethods.locatorValue(Locators.CSS, "#editorial-notes > h3");
	}

	public static By getPivitModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'card')]/div[contains(@class,'pivit')]");
	}

	public static By getPivitModuleHref() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'card')]/div[contains(@class,'pivit')]//div[contains(@class,'pivit-widget-gameplay-article-powered')]/a");
	}

	public static By getPivitModuleText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'card')]/div[contains(@class,'pivit')]//div[contains(@class,'pivit-widget-gameplay-article-powered')]/a/span[contains(@class,'pivit-widget-power-text-brand')]");
	}

	public static By getNightCap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".card--newsletter");
	}

	public static By getPageHeading() {
		return WrapperMethods.locatorValue(Locators.CSS, ".page-heading");
	}

	public static By getResultsByCountyHeading() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'section-header__column')]/h2[contains(.,'results by county')]");
	}

	public static By getCountyMap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".county-map div.county-map__svg");
	}

	public static By getCountyMapStateResults() {
		return WrapperMethods.locatorValue(Locators.CSS, ".county-map div.county-map__state-results");
	}

	public static By getGenderHeading() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'section-header__column')]/h2[contains(.,'gender')]");
	}

	public static By getRepublicPartyToggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'party-toggle-wrapper--inline')]/div[contains(@class,'party-toggle')]/div[contains(@data-party,'Rep')]");
	}

	public static By getDemocraticPartyToggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'party-toggle-wrapper--inline')]/div[contains(@class,'party-toggle')]/div[contains(@data-party,'Dem')]");
	}

	public static By getCandiatebioheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//header[@class='candidate-bio__header']/div[@class='select-box-native select-box-native--inline']/label");
	}

	public static By getCandiatebiodropdown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//header[@class='candidate-bio__header']/div[@class='select-box-native select-box-native--inline']/div[@class='select-box-native__toggle']");
	}

	public static By getCandiatebiobuttontext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='candidate-bio__button']/span[@class='open']");
	}

	public static By getCandiatebiobutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='candidate-bio__button']/span[@class='open']");
	}

	public static By getcandidatebioclose_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='candidate-bio__button']/span[@class='close']");
	}

	public static By getcandidatebiodescription() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='candidate-bio__writeup']/p");
	}

	public static By getcandidatedeligatepageheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='page-heading']");
	}

	public static By getcandidatedeligatebtnmore() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='button-more']");
	}

	public static By getcandidatebuttonopened() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='button-more button-more--open']");
	}

	public static By ElectionsNotesSection() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='cnn-politics-notes']");
	}

	public static By getpledgeddeligatestext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='periodic-map-heading grid-row']/h3/span[1]");
	}

	public static By getpledgeddeligatescount() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='periodic-map-heading grid-row']/h3/span[2]");
	}

	public static By getsuperdeligatestitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='periodic-map-heading grid-row']/h3[2]/span[1]");
	}

	public static By getsuperdeligatescount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='periodic-map-heading grid-row']/h3[2]/span[2]");
	}

	public static By Tickerbar() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='tick-bar__background']");
	}

	public static By Statewisebreakdown() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='map__squares']");
	}

	public List<WebElement> StatewiseWon() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='square__background'][@style='background-color: rgba(155, 155, 155, 0);']");
	}

	public List<WebElement> Statewisevotingcomplete() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='square__background'][@style='background-color: rgb(155, 155, 155);']");
	}

	public static By Woncontest() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[text()=' won contest']");
	}

	public static By Voitingcomplete() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[text()=' voting complete']");
	}

	public static By Voitingcompletecolor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[text()=' voting complete']/preceding-sibling::span");
	}

	public static By woncontestcolor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[text()=' won contest']/preceding-sibling::span");
	}

	public static By OutBrainwidgetleft() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob_dual_left']");
	}

	public static By OutBrainwidgetright() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob_dual_right']");
	}

	public static By OutBrainLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='logo']/a/img");
	}

	public static By outbrainclose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".ob_modal #ob_modal_inner img");
	}

	public static By SearchbyStateDropDownBeforeSelect() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.select-box-native--left select");
	}

	public static By SearchbyStateDropDownAfterSelect() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.select-box-native--tight select");
	}

	public static By NoExitPollDataText() {
		return WrapperMethods.locatorValue(Locators.CSS, "p.race-results__placeholder");
	}

	public static By ExitPollSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".exit-poll-select");
	}

	public static By FullExitPollButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid']/div/div/a/span");
	}

	public static By Calendarstrip() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='calendar-strip']");
	}

	public static By Calenderlabel() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='select-box-native']/label[@class='section-heading select-box-native__label']");
	}

	public static By calendartoggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='select-box-native']/div[@class='select-box-native__toggle']");
	}

	public List<WebElement> calendarmonths() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='select-box-native']/select[@class='select-box-native__select']/option");
	}

	public List<WebElement> calendardate() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='calendar-strip']/a");
	}

	public static By Candidateresult() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='table calendar-table calendar-table--candidate-results']");
	}

	public List<WebElement> StatesList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table calendar-table calendar-table--candidate-results']/div[2]/a");
	}

	public static By deligatescountbelowsocialicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='share-me_gig_containerParent']/div[2]");
	}

	public static By Candidatemessagetext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='candidate-message__text']");
	}

	public static By deligatespledgercount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tick-bar']/div//div[@class='tick-bar__label-footnote'][contains(text(),'pledged')]");
	}

	public static By deligatessupercount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tick-bar']/div//div[@class='tick-bar__label-footnote'][contains(text(),'super')]");
	}

	public static By deligatesboundcount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tick-bar']/div//div[@class='tick-bar__label-footnote'][contains(text(),'bound')]");
	}

	public static By deligatesunboundcount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tick-bar']/div//div[@class='tick-bar__label-footnote'][contains(text(),'unbound')]");
	}

	public static By deligatesperiodicmap() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='periodic-map']");
	}

	public List<WebElement> periodicmaphref() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='square__background']/a");
	}

	public List<WebElement> periodicstates() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='square__labels']/div[1]");
	}

	public List<WebElement> periodiccount() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='square__labels']/div[2]");
	}

	public static By Cnndeligateestimateheading() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='page-heading']");
	}

	public static By whoiswinning() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='section-header__column']/*[@class='page-subheading']");
	}

	public static By deligateestimate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='section-header__column']/*[@class='section-heading']");
	}

	public List<WebElement> Electionhubrepubliccandidate() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Rep']");
	}

	public List<WebElement> Electionhubdemocraticcandidate() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Dem']");
	}

	public List<WebElement> ElectionhubRepublicbound() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Rep']//div[@class='tick-bar__label']//div[@class='tick-bar__label-footnote'][1]");
	}

	public List<WebElement> ElectionubunRepublicunbound() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Rep']//div[@class='tick-bar__label']//div[@class='tick-bar__label-footnote'][2]");
	}

	public List<WebElement> ElectionhubDemocraticpledged() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Dem']//div[@class='tick-bar__label']//div[@class='tick-bar__label-footnote'][1]");
	}

	public List<WebElement> ElectionhubdemocraticSuper() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Dem']//div[@class='tick-bar__label']//div[@class='tick-bar__label-footnote'][2]");
	}

	public List<WebElement> ElectionHubRepublicCandidatepicnav() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Rep']/div[@class='table__cell column-candidate']/a");
	}

	public List<WebElement> ElectionHubDemocraticcandidatepicnav() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='delegate-tracker__row']/div[@class='table__row'][@data-party='Dem']/div[@class='table__cell column-candidate']/a");
	}

	public static By Morerepublicbutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@data-party='Rep']//a[@class='party-button']");
	}

	public static By MoreDemocraticbtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@data-party='Rep']//a[@class='party-button']");
	}

	public List<WebElement> RepublicTotalDeligates() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Rep']//div[@class='tick-bar__label']/div/div[1]");
	}

	public List<WebElement> DemocraticTotalDeligates() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Dem']//div[@class='tick-bar__label']/div/div[1]");
	}

	public List<WebElement> RepublicDeligatesneeded() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Rep']//div[@class='tick-bar__label tick-bar__votes-needed-label']/div");
	}

	public List<WebElement> Democraticdeligatesneeded() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Dem']//div[@class='tick-bar__label tick-bar__votes-needed-label']/div");
	}

	public List<WebElement> RepublicTickBar() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Rep']//div[@class='tick-bar__background']");
	}

	public List<WebElement> DemocraticTickBar() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row'][@data-party='Dem']//div[@class='tick-bar__background']");
	}

	public static By getEmailtovalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//input[@id='gig_1465907909997_showShareUI_tbFriendsEmail']");
	}

	public static By getEmailclose() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//table[contains(@id,'showShareUI_emailCanvas')]/tbody/tr//table/tbody/tr/td[2]/div/img");
	}

	public static By getEmailShareTo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[contains(@id,'showShareUI_tbFriendsEmail')]");
	}

	public static By FBLaunchvalidation() {
		return WrapperMethods.locatorValue(Locators.CSS, "#email");
	}

	public static By FBpassLaunchvalidation() {
		return WrapperMethods.locatorValue(Locators.CSS, "#pass");
	}

	public static By TwitterLAunchvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//textarea[@id='status']");
	}

	public static By twitteremailtextbox() {
		return WrapperMethods.locatorValue(Locators.CSS, "#username_or_email");
	}

	public static By twitterpassword() {
		return WrapperMethods.locatorValue(Locators.CSS, "#password");
	}

	public static By getmorevalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='gig-simpleShareUI-caption']/div[text()='Share with your friends']");
	}

	public static By toggleRepBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='party-toggle__button'][@data-party='Rep']");
	}

	public static By toggleRepBtnMob() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='party-toggle']//div[@class='party-toggle__button'][@data-party='Rep']");
	}

	public static By toggleDemBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='party-toggle__button'][@data-party='Dem']");
	}

	public static By getExitPollSummary() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='exit-poll-summary']");
	}

	public static By socialIcons() {
		return WrapperMethods.locatorValue(Locators.ID, "share-me");
	}

	public static By recentExitPollLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class='section']/div[1]/div[1]/h3[@class='page-subheading']");
	}

	public static By exitPollsLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='header-navigation__link'][@href='/election/primaries/polls']");
	}

	public static By searchByStateDropdown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//label[@class='section-heading select-box-native__label']");
	}

	public static By backToRepParty() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@href='/election/primaries/parties/republican']/span[1]");
	}

	public static By backToRepPartyLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid']//a[@class='button-link']");
	}

	public static By backToDemParty() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@href='/election/primaries/parties/democrat']/span[1]");
	}

	public static By backToDemPartyLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='button-link']/span[contains(text(),'back to democratic party')]");
	}

	public static By electionCenter() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='button-link button-link--hub']/span[@class='button-link__text']");
	}

	public static By electionCenterLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='button-link button-link--hub']");
	}

	public static By selectedStateExitPollLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[@class='page-heading select-box-native__label']");
	}

	public static By getPartyLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='party-banner']/strong");
	}

	public static By exitPolls() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='header-navigation__link'][text()='Exit Polls']");
	}

	public static By hubLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='app-title__headline text-align-center']");
	}

	public List<WebElement> exitPollstatesList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table exit-poll-table']//div[@class='table__row']/div[@class='table__cell']/a");
	}

	public List<WebElement> exitPollsDateList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table exit-poll-table']//div[@class='table__row']/div[@class='table__cell text-align-right']/a");
	}

	public static By stateDropdown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='select-box-native select-box-native--left']//select[@class='select-box-native__select']");
	}

	public static By stateExitPolls() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//label[@class='page-heading select-box-native__label'][contains(text(),'alabama exit polls')]");
	}

	public static By partyLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-column__main section-header__column']//h2[@class='page-heading page-heading--inline vertical-align-middle']");
	}

	public static By raceResultsmodule() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "race-results");
	}

	public static By raceResultModuleUnderToggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='race-results']/ancestor::div[@class='main__content main__content--constrained main__content--margin-tight rail-layout']/preceding-sibling::div[@class='main__content main__content--constrained grid-layout']");
	}

	public static By timeStamp() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='race-results']/preceding-sibling::div[@class='section-header']//div[@class='last-updated text-lowercase']");
	}

	public static By resultsByCountryTimeStamp() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='section-header__column--threefourths section-header__column']/following-sibling::div[@class='section-header__column section-header__column--right']//div[@class='last-updated text-lowercase']");
	}

	public static By genderTimeStamp() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='section-header__exit-poll-question-column section-header__column']/following-sibling::div[@class='section-header__column section-header__column--right']//div[@class='last-updated text-lowercase']");
	}

	public static By raceResultsHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='race-results__header race-results__voting-status']");
	}

	public static By delegateAllocStatus() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='voting-status text-lowercase']");
	}

	public List<WebElement> delegateCounterLabels() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='race-results__header grid-row race-results__delegate-count']/div[*]//span[@class='counter__value']");
	}

	public List<WebElement> delegateCounterValues() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='race-results__header grid-row race-results__delegate-count']/div[*]//div[@class='counter__label']");
	}

	public static By counterLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='counter__label']");
	}

	public static By statusFontColor() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "race-results__reporting-status");
	}

	public static By delegatesBG() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='race-results__header grid-row race-results__delegate-count']");
	}

	public static By delegatesColor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='counter__label']");
	}

	public static By statusFontBGC() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "race-results__reporting-status");
	}

	public static By statusAlgmt() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='voting-status']/preceding-sibling::span[@class='voting-status text-lowercase voting-status--called']");
	}

	public static By projectedWinner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='voting-status text-lowercase voting-status--called']");
	}

	public static By estStatus() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='voting-status']");
	}

	public static By verifyCandidatesUnderColm() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='table__cell race-results-table__candidate-name']/ancestor::div[@class='table__row']/preceding-sibling::div[@class='table__row']//span[@class='table__head race-results-table__candidate-name']");
	}

	public List<WebElement> candidateInfo() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]");
	}

	public List<WebElement> candidateName() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]/span[@class='table__cell race-results-table__candidate-name']");
	}

	public List<WebElement> voteCount() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]/span[@class='table__cell race-results-table__vote-count']");
	}

	public List<WebElement> votePercentage() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]//div[@class='percent-bar__percent']");
	}

	public List<WebElement> pledgedDelegate() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]/span[@class='table__cell table__cell--number race-results-table__delegate-count']");
	}

	public List<WebElement> superDelegate() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='ui-group']/div[@role='presentation']/div[*]/span[5]");
	}

	public static By starCandidate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='candidate-name__last candidate-name--winner']");
	}

	public static By redirect() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='table__cell race-results-table__candidate-name']//a");
	}

	public static By background() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='race-results__header race-results__voting-status']");
	}

	public static By getPageNavigation() {
		return WrapperMethods.locatorValue(Locators.CSS, "nav.pagination__navigation");
	}

	public static By getPageButton2() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class, 'pagination__button')][2]");
	}

	public static By ExitPollDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.select-box-native--tight");
	}

	public static By BackToResultsButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class = 'button-link']");
	}

	public static By ElectionCenterButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class = 'button-link button-link--hub']");
	}

	public static By getNoEvents() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@href='/election/primaries/calendar/january']");
	}

	public static By SearchbyMonthDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.select-box-native--tight select");
	}

	public static By SearchbyMonthDropDownSection() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.select-box-native--tight");
	}

	public static By NoEvents() {
		return WrapperMethods.locatorValue(Locators.CSS, ".calendar-date--null.text-align-center");
	}

	public static By NoData() {
		return WrapperMethods.locatorValue(Locators.CSS, ".table__row.text-align-center");
	}

	public static By DayofMonthButtonSelected() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ui-group .calendar-date[data-selected='true'] div");
	}

	public static By DelegatesAllocatedCount() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.calendar-table__delegates-cell span span");
	}

	public static By DayofMonthButtonNotSelected() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ui-group .calendar-date[data-selected='false'] div");
	}

	public List<WebElement> getCalendarStripDates() {
		return WrapperMethods.locateElements(Locators.CSS, ".calendar-strip a");
	}

	public static By getUpdatedStateList() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@href='/election/primaries/states/ks/Dem']");
	}

	public static By getStateHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='table__head']");
	}

	public static By getWinnerHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='table__head calendar-table__winner-cell']");
	}

	public static By getDelegatesAllocatedHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='table__head text-align-right calendar-table__delegates-cell']");
	}

	public List<WebElement> getStateList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='table__row']/div/a[contains(@href, '/election/primaries/states/')]");
	}

	public static By getHighestPercentage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".exit-poll__table--right div.exit-poll__ranking--first");
	}

	public static By Partypageheading() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@class='page-heading section-header__column'][text()='CNN Delegate Estimate']");
	}

	public static By PartySocialIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='share-me_gig_containerParent']");
	}

	public static By NationalMap() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='national-map']");
	}

	public List<WebElement> MapCandidateName() {
		return WrapperMethods.locateElements(Locators.XPATH, "//span[@class='national-map__candidate-name-text']");
	}

	public static By tieorother() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='national-map__candidate-color map-color--tie']");
	}

	public static By candidateheading() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='national-map__candidate-heading']");

	}

	public static By mapToopTip() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='national-map__results-tooltip']");
	}

	public static By maptooptilcatch() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='national-map__results-tooltip'][@data-open='false']");
	}

	public static By HoverElement() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.national-map__inner-svg > svg > g > path");
	}

	public static By HoverParticularelement() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.national-map__inner-svg > svg > g > path[data-name='WY']");
	}

	public static By toolTipStateName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='national-map__tooltip-content']/*[@class='national-map__state-name']");
	}

	public static By tooltipelectiondate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='national-map__tooltip-content']/*[@class='cnn-politics-metadata national-map__election-date']");
	}

	public static By tooltipreporting() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='cnn-politics-metadata national-map__pct-reporting']");
	}

	public static By tooltipcandidatetext() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='national-map__state-results']/div/div[@class='grid-column']");
	}

	public static By tooltipboundunbound() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='national-map__state-results']//div[@class='grid-column text-align-right']");
	}

	public static By tooltipwinner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-column national-map__candidate--winner']");
	}

	public static By tooltipvotescolumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-row national-map--candidate-row']/div[@class='grid-column text-align-right national-map__votes-column']");
	}

	public List<WebElement> nationalMapTerrotory() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='national-map__territory'][@data-active='true']");
	}

	public static By getElectionHeaderLogoTagline() {
		/*
		 * return WrapperMethods.locatorValue(Locators.XPATH,
		 * "//div[contains(@class,'header-navigation__logo')]/a/span[contains(@class,'header-navigation__tagline')]"
		 * );
		 */
		return WrapperMethods.locatorValue(Locators.CSS,
				"nav.header-navigation__container > a > svg > text:nth-child(3)");
	}

	public List<WebElement> ResultModule() {
		return WrapperMethods.locateElements(Locators.XPATH, "//ul[@class='result-list']/li");
	}

	public static By getHeadtoHeadImages() {
		return WrapperMethods.locatorValue(Locators.CSS, ".section-wrapper.candidates-headtohead");
	}

	public static By getCandidateName() {
		return WrapperMethods.locatorValue(Locators.CSS, ".section-wrapper.candidates-headtohead .candidate-name");
	}

	public static By getPartyColorLine() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".section-wrapper.candidates-headtohead .candidate-name__divider");
	}

	public static By getFavRatings() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'favorability ratings')]");
	}

	public static By getFavRatingsLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']");

	}

	public static By getRepFavorable() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Rep']/div/div/div/div[contains(text(),'Favorable')]");

	}

	public static By getDemFavorable() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Dem']/div/div/div/div[contains(text(),'Favorable')]");

	}

	public static By getRepUnfavorable() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Rep']/div/div/div/div[contains(text(),'Unfavorable')]");

	}

	public static By getDemUnfavorable() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Dem']/div/div/div/div[contains(text(),'Unfavorable')]");

	}

	public static By getFavRatingsText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div/div[@class= 'approval-rating__metadata']");

	}

	public List<WebElement> getRepPercentageList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Rep']/div/div/div/div[@class= 'approval-rating__value-text']/span");
	}

	public List<WebElement> getDemPercentageList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'approval-ratings ']/div[@data-party= 'Dem']/div/div/div/div[@class= 'approval-rating__value-text']/span");
	}

	public static By getCandidateWhoIsWinning() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'winning')]");
	}

	public static By getCandidateCnnOrcPoll() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout grid-layout--with-padding']/div[1]");
	}

	public static By getCandidateCnnPollOfPolls() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout grid-layout--with-padding']/div[2]");
	}

	public List<WebElement> getCandidatePageCandidateList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/div/div[1]/div/div[@class= 'stacked-poll__value']/div/div[@class= 'stacked-poll__candidate-name']");
	}

	public List<WebElement> getCandidatePercentageList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/div/div[1]/div/div[@class= 'stacked-poll__value']//div[@class= 'stacked-poll__label']/div[@class= 'stacked-poll__label-text']/span");
	}

	public static By getMoneyRaceModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'the money race')]");
	}

	public static By getMoneyRaceModuleLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']");
	}

	public static By getMoneyRaceModuleTrump() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']");
	}

	public static By getMoneyRaceModuleClinton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']");
	}

	public static By getMRaceRepTotalRaised() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@data-label='Total raised (year to date)']");
	}

	public static By getMRaceRepTotalSpent() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@data-label='Total spent']");
	}

	public static By getMRaceRepCashOnHand() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@data-label='Cash on hand']");
	}

	public static By getMRaceRepLoans() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@data-label='Loans from candidate']");
	}

	public static By getMRaceRepDebt() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@data-label='Debt']");
	}

	public static By getMRaceDemTotalRaised() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@data-label='Total raised (year to date)']");
	}

	public static By getMRaceDemTotalSpent() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@data-label='Total spent']");
	}

	public static By getMRaceDemCashOnHand() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@data-label='Cash on hand']");
	}

	public static By getMRaceDemLoans() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@data-label='Loans from candidate']");
	}

	public static By getMRaceDemDebt() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@data-label='Debt']");
	}

	public List<WebElement> getMRaceRepColorBars() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div/div[@class= 'candidate-finances__record-bar']");
	}

	public List<WebElement> getMRaceDemColorBars() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div/div[@class= 'candidate-finances__record-bar']");
	}

	public static By getMRaceRepText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Rep']/div[@class= 'candidate-finances__metadata']");
	}

	public static By getMRaceDemText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'grid-layout candidate-finances grid-layout--with-padding']/div[@data-party= 'Dem']/div[@class= 'candidate-finances__metadata']");
	}

	public static By getMeetTheVPModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'meet the vp nominees')]");
	}

	public static By getMeetTheVPModuleLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'vice-presidential-candidates']");
	}

	public static By getMVPMike() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'vice-presidential-candidates']/div[@data-party= 'Rep']");
	}

	public static By getMVPTim() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'vice-presidential-candidates']/div[@data-party= 'Dem']");
	}

	public static By getMVPRepReadMore() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'vice-presidential-candidates']/div[@data-party= 'Rep']/div/span/a");
	}

	public static By getMVPDemReadMore() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'vice-presidential-candidates']/div[@data-party= 'Dem']/div/span/a");
	}

	public static By getTPCModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'third party candidates')]");
	}

	public static By getTPCLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']");
	}

	public static By getTPCGary() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']");
	}

	public static By getTPCJill() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']");
	}

	public static By getTPCGaryImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']/div[@class= 'third-party-candidates_headshot-image']");
	}

	public static By getTPCJillImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']/div[@class= 'third-party-candidates_headshot-image']");
	}

	public static By getTPCGaryName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']/div[@class= 'third-party-candidates_headshot-image']/div[@class= 'candidate-name']");
	}

	public static By getTPCJillName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']/div[@class= 'third-party-candidates_headshot-image']/div[@class= 'candidate-name']");
	}

	public static By getTPCGaryDOB() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']/div[@class= 'third-party-candidates__stats']/div[@class= 'third-party-candidates__stats--birthdate']");
	}

	public static By getTPCJillDOB() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']/div[@class= 'third-party-candidates__stats']/div[@class= 'third-party-candidates__stats--birthdate']");
	}

	public static By getTPCGaryBio() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']/div[@class= 'third-party-candidates__stats']/div[@class= 'third-party-candidates__stats--bio']");
	}

	public static By getTPCJillBio() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']/div[@class= 'third-party-candidates__stats']/div[@class= 'third-party-candidates__stats--bio']");
	}

	public static By getTPCGaryReadMore() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Lib']/div/span/a");
	}

	public static By getTPCJillReadMore() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'third-party-candidates']/div[@data-party= 'Grn']/div/span/a");
	}

	public static By getRTPModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/h2[contains(text(),'roads to the presidency')]");
	}

	public static By getRTPLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']");
	}

	public static By getRTPRepColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--republican')]");
	}

	public static By getRTPDemColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--democrat')]");
	}

	public static By getRTPRepPartyIndicator() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--republican')]/a");
	}

	public static By getRTPDemPartyIndicator() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--democrat')]/a");
	}

	public static By getRTPRepMap() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--republican')]/a/div");
	}

	public static By getRTPDemMap() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--democrat')]/a/div");
	}

	public static By getRTPRepText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--republican')]/div");
	}

	public static By getRTPDemText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__parties']/div[contains(@class, 'roads-to-the-presidency__party--democrat')]/div");
	}

	public static By getRTPExploreTheMap() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'roads-to-the-presidency__button button button--light']");
	}

	public static By getHTGModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(text(),'how they got here')]");
	}

	public static By getHTGLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']");
	}

	public static By getHTGRepColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div[@data-party= 'Rep']");
	}

	public static By getHTGDemColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div[@data-party= 'Dem']");
	}

	public static By getHTGRepTimeline() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div[@data-party= 'Rep']/div/span/div/div");
	}

	public static By getHTGDemTimeline() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div[@data-party= 'Dem']/div/span/div/div");
	}

	public static By getHTGMoreButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div[contains(@class, 'candidate-timeline__expand-button')]");
	}

	public static By getHTGMoreButtonMobile() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'candidate-timeline-container']/div/div/div[contains(@class, 'button--expandable')]");
	}

	public static By getEditoralHeroImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".card.card--hero .card__media");
	}

	public static By getWhoIsWinning() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']");
	}

	public static By getCnnOrcPoll() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']//section[@class= 'section-wrapper pad-when-small']/div/div[1]");
	}

	public static By getCnnPollOfPolls() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']/div/div[2]");
	}

	public List<WebElement> getCandidateList() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='candidate-name__first']");
	}

	public List<WebElement> getPercentageList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper']/section[@class= 'section-wrapper pad-when-small']/div/div[1]/div/div[@class= 'stacked-poll__value']/div[@class= 'stacked-poll__value-text']/span");
	}

	public static By getFromtheTrailModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']/section[@class= 'section-wrapper pad-when-small']");
	}

	public static By getRoadto270PromoModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'bleed-layout promo-road-to-270__background']");
	}

	public static By getCampaignVideosModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/div[@class= 'bleed-layout ']");
	}

	public static By getMeetTheCandidatesModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'meet-the-candidates']");
	}

	public static By getPCPromoModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div[@class= 'grid-content grid-content--fluid pad-when-small']");
	}

	public static By getMoreCampainNewsVideoZone() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper pad-when-small']/h2[contains(.,'more campaign news')]");

	}

	/*
	 * public static By presidentsnapshot() { return Locators.XPATH,
	 * "//a[2]/div[@class='state-snapshots__item-snapshot']"); }
	 * 
	 * public static By presidentpartyrace() { return Locators.XPATH,
	 * "//a[2]//div[contains(@class,'state-snapshots__item-race')]"); }
	 * 
	 * public static By presidenticon() { return Locators.XPATH,
	 * "//a[2]//div[@class='state-snapshots__item-icon']"); }
	 * 
	 * public static By presidentcandidate() { return Locators.XPATH,
	 * "//a[2]//div[@class='state-snapshots__item-reporting']"); }
	 * 
	 * public static By sensatesnapshot() { return Locators.XPATH,
	 * "//a[3]/div[@class='state-snapshots__item-snapshot']"); }
	 * 
	 * public static By sensatepartyrace() { return Locators.XPATH,
	 * "//a[3]//div[contains(@class,'state-snapshots__item-race')]"); }
	 * 
	 * public static By sensateicon() { return Locators.XPATH,
	 * "//a[3]//div[@class='state-snapshots__item-icon']"); }
	 * 
	 * public static By sensatecandidate() { return Locators.XPATH,
	 * "//a[3]//div[@class='state-snapshots__item-reporting']"); }
	 * 
	 */

	public static By RightRailAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.rail-locking__container");
	}

	public static By RightRailStickyAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.sticky");
	}

	public static By SenateHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate h2");
	}

	public static By NoSenateRaceText() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate p");
	}

	public static By SenateResultsCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate a");
	}

	public static By SenateStateName() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class= 'group-wrapper']/div/div/span/span");
	}

	public static By SenateCRM() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate div.result-table.core-result");
	}

	public static By SenateCRMRepCand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#senate div.result-table.core-result .result-table__row.R .result-table__row-item-name");
	}

	public static By SenateCRMDemCand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#senate div.result-table.core-result .result-table__row.D .result-table__row-item-name");
	}

	public static By SenateCRMFullDetails() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#senate div.result-table.core-result .result-table__full-details.button");
	}

	public static By SenateStateMap() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate div.state-map");
	}

	public static By SenateNationalResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id= 'senate']/a");
	}

	public static By SenateExitPolls() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate .exit-poll-preview");
	}

	public static By SenateExitPollsCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate .exit-poll-preview a");
	}

	public static By SenateStateMapSizeOfTheLead() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate div.state-map .map-legend");
	}

	public static By SenateStateMapTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#senate div.state-map .state-map__header.h5");
	}

	public static By getCnnRedlogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//nav[@class='nav js-navigation js-skinny politics']//a[@class='logo-links__cnn']");
	}

	public static By getPoliticslogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//nav[@class='nav js-navigation js-skinny politics']//a[@class='logo-links__politics']");

	}

	public static By getOTIModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/h2[contains(text(),'on the issue of')]");
	}

	public static By getOTIDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".button.button--light.select.select--light");
	}

	public static By getOTIRepColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Rep']");
	}

	public static By getOTIDemColumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Dem']");
	}

	public static By getOTIRepCandName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Rep']/div[@class= 'candidate-issues__quote-candidate-name']");
	}

	public static By getOTIDemCandName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Dem']/div[@class= 'candidate-issues__quote-candidate-name']");
	}

	public static By getOTIRepCandComment() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Rep']/div[@class= 'quote-content']");
	}

	public static By getOTIDemCandComment() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@data-party= 'Dem']/div[@class= 'quote-content']");
	}

	public static By getOTICandToggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'candidate-issues__toggle-candidate-buttons']");
	}

	public static By getOTIRepCandToggleButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'candidate-issues__toggle-candidate-buttons']/div/div[contains(., 'Trump')]");
	}

	public static By getOTIDemCandToggleButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'candidate-issues__toggle-candidate-buttons']/div/div[contains(., 'Clinton')]");
	}

	public static By getOTICandComment() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div/div/div/div[@class= 'candidate-issues__quote-container']");
	}

	public static By getNomineeTweet() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/h2[contains(text(),'nominee tweets')]");
	}

	public static By SharecontainerCloseButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".navigation-share__close-icon");
	}

	public static By ExitPollsAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_bnr_atf_01");
	}

	public static By ExitPollsTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small page-header-title']/h1");
	}

	public static By ExitPollsDesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small page-header-title']/p[1]");
	}

	public static By NationalDD() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[1]");
	}

	public static By RaceTypeDD() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[2]");
	}

	public static By NationalDropDown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[1]/select");
	}

	public static By RaceTypeDropDown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[2]/select");
	}

	public static By KeywordSearch() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[3]/input");
	}

	public static By ViewAsToggleButtons() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='exit-poll-hub__controls']");
	}

	public static By GraphButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tab-control tab-control--fluid-tabs']/div[contains(., 'graph')]");
	}

	public static By TableButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tab-control tab-control--fluid-tabs']/div[contains(., 'table')]");
	}

	public static By ExitPollsTimeStamp() {
		// return WrapperMethods.locatorValue(Locators.XPATH,
		// "//section[@class='section-wrapper
		// pad-when-small section-wrapper--border']/p");
		return WrapperMethods.locatorValue(Locators.CSS, "p.exit-poll-hub__lts");
	}

	public static By PresidentCRM() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.result-table.core-result");
	}

	public static By PresidentTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.result-table__title");
	}

	public static By PresidentWinningIndicator() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.core-result__projected-winner");
	}

	public static By PresidentCRMRepCand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.R .result-table__row-item-name");
	}

	public static By PresidentCRMDemCand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.D .result-table__row-item-name");
	}

	public static By PresidentCRMRepPercent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.R .result-table__bar-value");
	}

	public static By PresidentCRMDemPercent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.D .result-table__bar-value");
	}

	public static By PresidentCRMRepRawVote() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.R .result-table__row-item-votes");
	}

	public static By PresidentCRMDemRawVote() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.D .result-table__row-item-votes");
	}

	public static By PresidentCRMRepBar() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.R .result-table__bar");
	}

	public static By PresidentCRMDemBar() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__row.D .result-table__bar");
	}

	public static By PresidentEstPercent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result div.result-table__reporting");
	}

	public static By PresidentCRMFullDetails() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result .result-table__full-details.button");
	}

	public static By PresidentTimeStamp() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#president div.result-table.core-result span.result-table__timestamp");
	}

	public static By PresidentStateMap() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.state-map");
	}

	public static By PresidentNationalResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id= 'president']/div/a");
	}

	public static By PresidentExitPolls() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president .exit-poll-preview");
	}

	public static By PresidentExitPollsCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president .exit-poll-preview a");
	}

	public static By PresidentStateMapSizeOfTheLead() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.state-map .map-legend");
	}

	public static By PresidentStateMapTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president div.state-map .state-map__header.h5");
	}

	public static By PresidentResultsTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/h2[contains(., 'presidential results')]");
	}

	public static By PresidentBM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div/div[@data-eq-state= 'balance-of-power-medium balance-of-power-large']");
	}

	public static By PresidentDemCandName() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--left span.balance-of-power__candidate-lname");
	}

	public static By PresidentRepCandName() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--right span.balance-of-power__candidate-lname");
	}

	public static By PresidentDemCandImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".balance-of-power__visual--left img");
	}

	public static By PresidentRepCandImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".balance-of-power__visual--right img");
	}

	public static By PresidentDemECVote() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--left span.balance-of-power__candidate-ec-votes");
	}

	public static By PresidentRepECVote() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--right span.balance-of-power__candidate-ec-votes");
	}

	public static By PresidentDemBarGraph() {
		return WrapperMethods.locatorValue(Locators.CSS, ".balance-of-power__visual--left i");
	}

	public static By PresidentRepBarGraph() {
		return WrapperMethods.locatorValue(Locators.CSS, ".balance-of-power__visual--right i");
	}

	public static By PresidentDemPVPercent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--left span.balance-of-power__vote-percentage");
	}

	public static By PresidentRepPVPercent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".balance-of-power__candidate--right span.balance-of-power__vote-percentage");
	}

	public static By PresidentMedLine() {
		return WrapperMethods.locatorValue(Locators.CSS, "i.balance-of-power__threshold-line");
	}

	public static By PresidentArrow() {
		return WrapperMethods.locatorValue(Locators.CSS, "i.balance-of-power__threshold-carot");
	}

	public static By PresidentTextBelowArrow() {
		return WrapperMethods.locatorValue(Locators.CSS, "#votes-needed-bop-president");
	}

	public static By AllStateCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='navigation-states__button button button--light']");
	}

	public static By AllStateCTADevice() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='navigation-states__button button button--dark']");
	}

	public static By StateDropDown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--light select select--light navigation-states__button']");
	}

	public static By StateDropDownSelect() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--light select select--light navigation-states__button']/select");
	}

	public static By StateDropDownDevice() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--dark select select--dark navigation-states__button']");
	}

	public static By StateDropDownSelectDevice() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--dark select select--dark navigation-states__button']/select");
	}

	public List<WebElement> PresidentNPMapECVoteList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//h2[contains(., 'presidential results')]/following-sibling::div[2]/div/div/div//div[@class='map__viewport']/div/div/div/div[@class='tile__background circle'][@data-is-active='true']/div/div[@class='count']");
	}

	public List<WebElement> PresidentBStateYellowCircleList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//h2[contains(., 'presidential results')]/following-sibling::div[2]/div/div/div//div[@class='map__viewport']/div/div/div[@class='circle tile__round candidate-complete tile__round--battleground']");
	}

	public static By PresidentBStateMiniCRM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div[@class='group-wrapper'][2]/div[@class='group-wrapper'][2]");
	}

	public static By PresidentExitPollModule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div[@class='group-wrapper'][2]/div[@class='group-wrapper'][3]");
	}

	public List<WebElement> PresidentExit2PollQuestions() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div[@class='group-wrapper'][2]/div[@class='group-wrapper'][3]/div/div/div");
	}

	public static By PresidentExitFooterDemCandName() {
		return WrapperMethods.locatorValue(Locators.CSS, ".exit-poll-preview__footer .key.party-d.party-indicator");
	}

	public static By PresidentExitFooterRepCandName() {
		return WrapperMethods.locatorValue(Locators.CSS, ".exit-poll-preview__footer .key.party-r.party-indicator");
	}

	public static By AllExitPollCTA() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".exit-poll-preview__link.button.button--inline.button--small.button--light a");
	}

	public static By AllPresidentResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div[@class='group-wrapper'][2]/div[@class='group-wrapper'][2]/div/span/a");
	}

	public static By SenateResultsTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/h2[contains(., 'senate results')]");
	}

	public static By SenateBM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div/div[@class= 'balance-of-power balance-of-power--senate']");
	}

	public static By SenateNPMap() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'senate results')]/following-sibling::div[2]/div/div[1]");
	}

	public List<WebElement> SenateKeyRaceMiniCRM() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//h2[contains(., 'senate results')]/following-sibling::div[2]/div/ul/li");
	}

	public static By AllSenateResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'senate results')]/following-sibling::div[3]");
	}

	public static By HouseBM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section/div/div[@class= 'balance-of-power-house__wrapper']");
	}

	public List<WebElement> HouseKeyRaceMiniCRM() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//h2[contains(., 'house results')]/following-sibling::div[2]/ul/li");
	}

	public static By AllHouseResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'house results')]/following-sibling::div[3]");
	}

	public static By GovernorResultsSection() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section[@class='section-wrapper'][4]");
	}

	public static By GovernorNPMap() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'gubernatorial results')]/following-sibling::div[1]/div/div[1]");
	}

	public List<WebElement> GovernorKeyRaceMiniCRM() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//h2[contains(., 'gubernatorial results')]/following-sibling::div[1]/div/ul/li");
	}

	public static By AllGovernorResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'gubernatorial results')]/following-sibling::div[2]");
	}

	public static By BallotMResultsSection() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section[@class='section-wrapper'][5]");
	}

	public List<WebElement> TwoKeyBMeasures() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section[@class='section-wrapper'][5]/div/div/ul/li");
	}

	public List<WebElement> TwoKeyBMeasuresHeader() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='grid-content grid-content--fluid pad-when-small top-border']/section[@class='section-wrapper'][5]/div/div/ul/li/div/header/div[@class='result-table__title']");
	}

	public static By AllBallotMResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//h2[contains(., 'ballot measures')]/following-sibling::div[2]");
	}

	public static By GovernorHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, "#governor h2");
	}

	public static By NoGovernorRaceText() {
		return WrapperMethods.locatorValue(Locators.CSS, "#governor p");
	}

	public static By GovernorResultsCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, "#governor a");
	}

	public static By GovernorStateMap() {
		return WrapperMethods.locatorValue(Locators.CSS, "#governor .state-map");
	}

	public static By GovernorCRM() {
		return WrapperMethods.locatorValue(Locators.CSS, "#governor li.result-list__item");
	}

	public static By HouseHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house h2");
	}

	public static By HouseCRM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='house']//li[@class='result-list__item']");
	}

	public static By HouseTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house li.result-list__item div.house-result__state-label");
	}

	public static By HouseCRMRepLabel() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house li.result-list__item div.house-result__party-r");
	}

	public static By HouseCRMDemLabel() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house li.result-list__item div.house-result__party-d");
	}

	public static By HouseCRMOtherLabel() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house li.result-list__item div.house-result__party-o");
	}

	public List<WebElement> HouseResultCircularBubbles() {
		return WrapperMethods.locateElements(Locators.CSS, "#house li.result-list__item div.house-result__bubbles a");
	}

	public static By HouseBGIndicator() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='key status-key-race status-icon']");
	}

	public static By HousePartyChangeIndicator() {
		return WrapperMethods.locatorValue(Locators.CSS, ".key.status-party-change.status-icon");
	}

	public static By HouseTimeStamp() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house .house-result__timestamp");
	}

	public static By HouseCounter() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house .house-result__remaining-label");
	}

	public static By NationalHouseResultsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id= 'house']/div/a");
	}

	public static By HouseStateGeoMap() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house div.state-map");
	}

	public static By HouseStateMapTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house div.state-map .state-map__header.h5");
	}

	public static By HouseStateMapSizeOfTheLead() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house div.state-map .map-legend");
	}

	public static By HouseStateMapSizeOfTheLeadTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "#house div.state-map .map-legend__title");
	}

	public static By MoreCampNews() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='OUTBRAIN'][@id='outbrain_widget_0']");
	}

	public static By PaidContent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='OUTBRAIN'][@id='outbrain_widget_1']");
	}

	public static By PresidentResultsIcons() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id= 'party-key']/div[@class ='party-key__party-section'][1]");
	}

	public static By OtherResultsIcons() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id= 'party-key']/div[@class ='party-key__party-section'][2]");
	}

	public static By IconsAndSymbols() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id= 'party-key']/div[@class ='party-key__icon-section']");
	}

	public static By HouseRaceStateName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid pad-when-small page-header-title']/h1");
	}

	public static By HouseRaceCountyCRM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']//*[@class='section-wrapper']/ul/li");
	}

	public static By HouseRaceCountySGM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid ']//*[@class='section-wrapper']/ul/li[2]");
	}

	public List<WebElement> HouseRaceCountySGMResults() {
		return WrapperMethods.locateElements(Locators.CSS, ".state-map__svg path");
	}

	public List<WebElement> HouseRaceCountyResultsModuleList() {
		return WrapperMethods.locateElements(Locators.XPATH, "//section[@class ='section-wrapper']/div/ul/li");
	}

	public static By HouseRaceCountyStatusError() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.status-error");
	}

	public static By RaceStateName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'grid-content grid-content--fluid pad-when-small page-header-title']/h1/span");
	}

	public static By ShareNavigation() {
		return WrapperMethods.locatorValue(Locators.CSS, ".navigation-share");
	}

	public static By HouseRaceFullBOP() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section/div/div[@class= 'balance-of-power-house__wrapper']");
	}

	public static By HouseRaceFullExitPollsModule() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section/div/div[@class= 'exit-poll-preview']");
	}

	public static By HouseRaceFullTabs() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper padding-top-rabbit']");
	}

	public static By KeyRacesTab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper padding-top-rabbit']/div/div[1]");
	}

	public static By LatestProjectionsTab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper padding-top-rabbit']/div/div[2]");
	}

	public static By AllRacesTab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper padding-top-rabbit']/div/div[3]");
	}

	public List<WebElement> HouseRaceFullExitPolls() {
		return WrapperMethods.locateElements(Locators.CSS, "div.exit-poll-list .exit-poll-table");
	}

	public static By HouseRaceFullExitPollsCTA() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class= 'exit-poll-preview__link button button--inline button--small button--light']/span/a");
	}

	public List<WebElement> HouseRaceDistrictNames() {
		return WrapperMethods.locateElements(Locators.CSS, "div.result-table__title");
	}

	public List<WebElement> HouseRaceRepCandNames() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row-item result-table__row-item-name party-indicator party-R']");
	}

	public List<WebElement> HouseRaceDemCandNames() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row-item result-table__row-item-name party-indicator party-D']");
	}

	public List<WebElement> HouseRaceRepVoteCount() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row R']/div[@class= 'result-table__row-item result-table__row-item-votes']");
	}

	public List<WebElement> HouseRaceDemVoteCount() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row D']/div[@class= 'result-table__row-item result-table__row-item-votes']");
	}

	public List<WebElement> HouseRaceRepVotePercent() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row R']/div/div[@class= 'result-table__bar-value']");
	}

	public List<WebElement> HouseRaceDemVotePercent() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class= 'result-table__row D']/div/div[@class= 'result-table__bar-value']");
	}

	public static By PresidentRaceNPMState() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class= 'tile__background circle'][@data-name='IL']");
	}

	public static By PresidentRaceFullDetailsCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, "#president-page-race-results span.button__contents a");
	}

	public static By StateMap() {
		return WrapperMethods.locatorValue(Locators.CSS, ".state-map");
	}

	public List<WebElement> PresidentRaceResultsModules() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper']/ul/li");
	}

	public static By PresidentRaceBackToTopCTA() {
		return WrapperMethods.locatorValue(Locators.CSS, ".back-to-top.button.button--light span");
	}

	public List<WebElement> HouseRaceWinnerResultsModules() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@class= 'section-wrapper']/div[@class= 'group-wrapper']/div/ul/li");
	}

	public List<WebElement> HouseRaceStatesModules() {
		return WrapperMethods.locateElements(Locators.CSS, "div.house-result");
	}

	public List<WebElement> HouseRaceStatesModuleTitles() {
		return WrapperMethods.locateElements(Locators.CSS, "div.house-result__state-label");
	}

	public List<WebElement> HouseRaceStatesModuleDesc() {
		return WrapperMethods.locateElements(Locators.CSS, "span.house-result__info-text");
	}

	public List<WebElement> HouseRaceRepLabel() {
		return WrapperMethods.locateElements(Locators.CSS, "div.house-result__party-r");
	}

	public List<WebElement> HouseRaceDemLabel() {
		return WrapperMethods.locateElements(Locators.CSS, "div.house-result__party-d");
	}

	public List<WebElement> HouseRaceOtherLabel() {
		return WrapperMethods.locateElements(Locators.CSS, "div.house-result__party-o");
	}

	public static By HouseBeforeRaceDataText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".result-list p");
	}

	public List<WebElement> HouseKeyRaceIndicator() {
		return WrapperMethods.locateElements(Locators.CSS, ".key.status-key-race.status-icon");
	}

	public List<WebElement> HouseRacePartyChangeIndicator() {
		return WrapperMethods.locateElements(Locators.CSS, ".key.status-party-change.status-icon");
	}

	public List<WebElement> HouseRaceCounter() {
		return WrapperMethods.locateElements(Locators.CSS, ".house-result__remaining-label");
	}

	public static By HouseRaceLoadMoreCTA() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".pagination__button.button.button--light.button--expandable span");
	}

	public static By ExitPollsSearchButtonyKeyword() {
		return WrapperMethods.locatorValue(Locators.CSS, ".autocomplete");
	}

	public static By ExitPollsSearchResults() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".section-wrapper .exit-poll-results .exit-poll-results__header");
	}

	public List<WebElement> ExitPollsGraphResults() {
		return WrapperMethods.locateElements(Locators.CSS, ".exit-poll.exit-poll--framed");
	}

	public List<WebElement> ExitPollsTableResults() {
		// return
		// go().findElements(Locators.CSS,
		// ".exit-poll-table.exit-poll-table--framed.exit-poll-table--more-than-4-candidates");
		return WrapperMethods.locateElements(Locators.CSS, ".exit-poll-table.exit-poll-table--framed");

	}

	public List<WebElement> StateMapmousehover() {
		return WrapperMethods.locateElements(Locators.CSS, "svg.state-map__svg path.state-map__division");
	}

	public static By StateMaptooltipTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__title']");
	}

	public static By StateMaptooltipRowValue() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__row-item result-table__row-item-votes']");
	}

	///////////////////

	public static By getCoreResultsModule() {
		return WrapperMethods.locatorValue(Locators.CSS, ".section-wrapper.pad-when-small");
	}

	public static By getState() {
		return WrapperMethods.locatorValue(Locators.CSS, ".section-wrapper.pad-when-small h2 a");
	}

	public static By getTypeOfContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".section-wrapper.pad-when-small h2");
	}

	public List<WebElement> getDemocraticVotePercent() {
		return WrapperMethods.locateElements(Locators.CSS, ".core-result__bar-percent.D span");
	}

	public List<WebElement> getRepublicVotePercent() {
		return WrapperMethods.locateElements(Locators.CSS, ".core-result__bar-percent.R span");
	}

	public List<WebElement> getRawVote() {
		return WrapperMethods.locateElements(Locators.CSS, ".core-result__votes.grid-column");
	}

	public List<WebElement> getDemocraticWinnerLead() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='core-result__row grid-row winner']/div[@class='core-result__bar grid-column']/div[@class='core-result__bar-percent D']");
	}

	public static By getDemocraticLoserLead() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='core-result__row grid-row']/div[@class='core-result__bar grid-column']/div[@class='core-result__bar-percent D']");
	}

	public List<WebElement> getRepublicWinnerLead() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='core-result__row grid-row winner']/div[@class='core-result__bar grid-column']/div[@class='core-result__bar-percent R']");
	}

	public static By getRepublicLoserLead() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='core-result__row grid-row']/div[@class='core-result__bar grid-column']/div[@class='core-result__bar-percent R']");
	}

	public List<WebElement> getTimeStamp() {
		return WrapperMethods.locateElements(Locators.CSS, "time.core-result__timestamp");
	}

	///

	public static By Sharecontainer() {
		return WrapperMethods.locatorValue(Locators.CSS, ".navigation-share__container");
	}

	public static By getwhatsappicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon WhatsApp']");
	}

	public static By getElectionlogotext() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"nav.header-navigation__container > a > svg > text:nth-child(3)");

	}

	public static By getheadercandidatenav() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='header-navigation__links-wrapper--inner']/a[1]");
	}

	public static By getheadereventnav() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='header-navigation__links-wrapper--inner']/a[2]");
	}

	public static By getheaderprimaryandcausesnav() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='header-navigation__links-wrapper--inner']/a[3]");
	}

	public static By getEventPageTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "h1.page-heading");
	}

	public static By getEventPagedescription() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-events-subheading");
	}

	public List<WebElement> geteventlist() {
		return WrapperMethods.locateElements(Locators.CSS, "section.election-event-list__month");
	}

	public static By geteventdate() {
		return WrapperMethods.locatorValue(Locators.CSS, ".election-event-list__month-header");
	}

	public static By getelectionevent() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event");
	}

	public static By eventdate() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__date");
	}

	public static By eventmonth() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__month");
	}

	public static By eventday() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__day");
	}

	public static By eventdetails() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__details");
	}

	public static By eventairdate() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__airdate");
	}

	public static By eventairname() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__name");
	}

	public static By eventairlocation() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__location");
	}

	public static By eventairfullcoveragebutton() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.election-event__button button button--red button--small");
	}

	public static By Eventcountdown() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='election-event__counter countdown countdown--light']");
	}

	public static By AdBanner() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.rail-locking__sticker>section.section-wrapper");
	}

	public static By FooterNation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='footer-cnn-politics__link'][text()='Nation']");

	}

	public static By FooterWorld() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='footer-cnn-politics__link'][text()='World']");

	}

	public static By FooterOurTeam() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='footer-cnn-politics__link'][text()='Our Team']");

	}

	public static By getresultssubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='header-navigation-breadcrumb__active-route']");
	}

	public static By getpresidentsubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[1]");
	}

	public static By getsensatesubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[2]");
	}

	public static By gethousesubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[3]");
	}

	public static By getgovernorsubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[4]");
	}

	public static By getexitpollssubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[5]");
	}

	public static By getballotmeasuresubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[6]");
	}

	public static By getoverviewsubnav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='header-navigation__links']//a[7]");
	}

	public static By subnavtext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "span");
	}

	public List<WebElement> getNationalmapgreyedout() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-is-active='false']");
	}

	public List<WebElement> getNationalmapstatename() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-is-active='false']/div/div[@class='text-bold']");
	}

	public static By Notes1stline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[1]");
	}

	public static By Notes2ndline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[2]");
	}

	public static By Notes3rdline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[3]");
	}

	public static By Notes4thline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[4]");
	}

	public static By Notes5thline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[5]");
	}

	public static By Notes6thline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='editorial-notes__notes']/li[6]");
	}

	public static By RaceresutlsTab() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='tab-control']/div[text()='key races']");
	}

	public static By LatestprojectionsTab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tab-control']/div[text()='latest projections']");
	}

	public static By allracesTab() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='tab-control']/div[text()='all races']");
	}

	public static By raceresults() {
		return WrapperMethods.locatorValue(Locators.CSS, "ul.result-list");
	}

	public List<WebElement> raceresultsfulldetails() {
		return WrapperMethods.locateElements(Locators.XPATH, "//*[@class='button__contents']/a");
	}

	public List<WebElement> raceresultssections() {
		return WrapperMethods.locateElements(Locators.CSS, "li.result-list__item");
	}

	public List<WebElement> raceresultsstate() {
		return WrapperMethods.locateElements(Locators.CSS, "div.result-table__title>a");
	}

	public static By toltipcandidateheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']/div[@class='result-table core-result overlay has-footer']//div[@class='result-table__row-header result-table__row-header-name']");
	}

	public static By tooltippercentageheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']/div[@class='result-table core-result overlay has-footer']//div[@class='result-table__row-header result-table__row-header-pct']");
	}

	public static By tooltipvotedheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']/div[@class='result-table core-result overlay has-footer']//div[@class='result-table__row-header result-table__row-header-votes']");
	}

	public static By tooltipTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__title']/a");
	}

	public static By tooltipTitlee() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__title']");
	}

	public static By tooltipProjectedWinner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='core-result__projected-winner']");
	}

	public static By tooltipRowWinner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__row winner']");
	}

	public static By tooltipRow() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='tooltip']//div[@class='result-table__row']");
	}

	public static By tooltipRowCandidaeName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[contains(@class,'result-table__row-item result-table__row-item-name')]");
	}

	public static By tooltipRowPercentage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__row-item result-table__row-item-pct']");
	}

	public static By zeroedtooltipRowPercentage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__row-item result-table__row-item-pct']//div[@class='result-table__bar-value']");
	}

	public static By tooltipRowValue() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__bar-value']");
	}

	public static By Zeroedoutvotescount() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__row-item result-table__row-item-votes']");
	}

	public static By tooltipFooterreporting() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__reporting']");
	}

	public static By tooltipFooterTimeStamp() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//span[@class='result-table__timestamp']");
	}

	public List<WebElement> hovertooltip() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-name='WA']");
	}

	public static By tooltipvisible() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip'][@data-open='true']//div[@class='result-table core-result overlay has-footer']");
	}

	public static By PagesenateHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='text-bold']");
	}

	public static By Senatebalancemodule() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='balance-of-power balance-of-power--senate']");
	}

	public static By Senatenationalmapmodule() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.map__tiles");
	}

	public List<WebElement> Senatestateyellowcircle() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='circle tile__round tbd tile__round--battleground']//div[@class='text-bold']");
	}

	public List<WebElement> Senateswinningstate() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='circle tile__round candidate-complete']//div[@class='text-strong']");
	}

	public List<WebElement> withoutsensateresult() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='circle tile__round tbd']/div[@class='tile__background circle'][@data-is-active='true']//div[@class='text-bold']");
	}

	public List<WebElement> statesundercounting() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='circle tile__round tbd']/div[@class='tile__background circle'][@data-is-active='false']//div[@class='text-bold']");
	}

	public static By Filtertabheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'control__tab--active')][text()='key races']");
	}

	public static By Filtertablatestheaderheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'control__tab--active')][text()='latest projections']");
	}

	public List<WebElement> StateNames() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='result-table__title']/a");
	}

	public List<WebElement> Statenames() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='state-snapshots__item']/a[1]/div");
	}

	public static By StateNamesreceresult() {
		return WrapperMethods.locatorValue(Locators.XPATH, "div/header/div[@class='result-table__title']/a");
	}

	public static By keyracelabel() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/header/div[@class='result-table__bottom']/div[@class='core-result__battleground']");
	}

	public static By candidateheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[@class='result-table__row-header result-table__row-header-name']");
	}

	public static By percentageheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[@class='result-table__row-header result-table__row-header-pct']");
	}

	public static By votedheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[@class='result-table__row-header result-table__row-header-votes']");
	}

	public static By tilecolor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "div/header");
	}

	public static By ProjectedWinner() {
		return WrapperMethods.locatorValue(Locators.XPATH, "div//div[@class='core-result__projected-winner']");
	}

	public static By RowWinner() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='result-table__row winner']");
	}

	public static By RowCandidaeName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[contains(@class,'result-table__row-header result-table__row-header-name')]");
	}

	public static By RowPercentage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[@class='result-table__row-header result-table__row-header-pct']");
	}

	public static By RowValue() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/div/div/div/div[contains(@class,'result-table__row-header result-table__row-header-votes')]");
	}

	public static By Footerreporting() {
		return WrapperMethods.locatorValue(Locators.XPATH, "div/footer/div[@class='result-table__reporting']");
	}

	public static By FooterTimeStamp() {
		return WrapperMethods.locatorValue(Locators.XPATH, "div/footer/span[@class='result-table__timestamp']");
	}

	public List<WebElement> Resultmodule() {
		return WrapperMethods.locateElements(Locators.XPATH, "//ul[@class='result-list']/li");
	}

	///////
	public static By Statessnapshothdr_president() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='state-snapshots__headings']/a[text()='president']");
	}

	public static By Statessnapshothdr_senate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='state-snapshots__headings']/a[text()='senate']");
	}

	public static By Statessnapshothdr_house() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='state-snapshots__headings']/a[text()='house']");
	}

	public static By Statessnapshothdr_governor() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='state-snapshots__headings']/a[text()='governor']");
	}

	public List<WebElement> statesnapshots() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='state-snapshots__item']");
	}

	public static By SubStatenapshot() {
		return WrapperMethods.locatorValue(Locators.XPATH, "a");
	}

	public static By reporting() {
		return WrapperMethods.locatorValue(Locators.XPATH, "/a/div/div[@class='state-snapshots__item-reporting']");
	}

	public static By StateName() {
		return WrapperMethods.locatorValue(Locators.XPATH, "a/div[@class='state-snapshots__item-label']");
	}

	public static By Statesnapshotitem() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='state-snapshots__item-snapshot']");
	}

	public static By Statesnapshotitemreporting() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='state-snapshots__item-reporting']");
	}

	/*
	 * public static By presidentsnapshot() { return Locators.XPATH,
	 * "//a[2]/div[@class='state-snapshots__item-snapshot']"); }
	 * 
	 * public static By presidentpartyrace() { return Locators.XPATH,
	 * "//a[2]//div[contains(@class,'state-snapshots__item-race')]"); }
	 * 
	 * public static By presidenticon() { return Locators.XPATH,
	 * "//a[2]//div[@class='state-snapshots__item-icon']"); }
	 * 
	 * public static By presidentcandidate() { return Locators.XPATH,
	 * "//a[2]//div[@class='state-snapshots__item-reporting']"); }
	 * 
	 * public static By sensatesnapshot() { return Locators.XPATH,
	 * "//a[3]/div[@class='state-snapshots__item-snapshot']"); }
	 * 
	 * public static By sensatepartyrace() { return Locators.XPATH,
	 * "//a[3]//div[contains(@class,'state-snapshots__item-race')]"); }
	 * 
	 * public static By sensateicon() { return Locators.XPATH,
	 * "//a[3]//div[@class='state-snapshots__item-icon']"); }
	 * 
	 * public static By sensatecandidate() { return Locators.XPATH,
	 * "//a[3]//div[@class='state-snapshots__item-reporting']"); }
	 * 
	 */

	// div[contains(@class,'state-snapshots__item-race')]
	public static By reportingcandidate() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='state-snapshots__item-reporting']");
	}

	public static By Governorheading() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='page-header']/a");
	}

	public static By Allcountrystateresult() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='section-header']");
	}

	public static By resultlist() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@class='result-list']");
	}

	public static By resultsbycountrymap() {
		return WrapperMethods.locatorValue(Locators.CSS, "svg.state-map__svg");
	}

	public static By statemapheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='state-map__header h5']");
	}

	public static By Mapmousehover() {
		return WrapperMethods.locatorValue(Locators.CSS, "svg.state-map__svg path.state-map__division");
	}

	public static By getactiveheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[contains(@class,'header-navigation__link--active')]");
	}

	public List<WebElement> getNationalraceresultstatename() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-is-active='true']/div/div[@class='text-bold']");
	}

	public List<WebElement> getNationalGubernatorialresults() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-is-active='true']");
	}

	public List<WebElement> getKeyracemodules() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='circle tile__round tbd tile__round--battleground']/div[@class='tile__background circle'][@data-is-active='true']//div[@class='text-bold']");
	}

	public List<WebElement> KeyRacecircle() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'tile__round--battleground')]/div[@class='tile__background circle']");
	}

	public List<WebElement> OutBrainContainers() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='ob_container']");
	}

	public static By Senatebalancemodulecount() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.balance-of-power__candidate.balance-of-power__candidate-info.balance-of-power__candidate--left > span.balance-of-power__candidate-ec-votes");
	}

	public static By Senatedemocratbalancemodulecount() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.balance-of-power__text > div:nth-child(2) > span.balance-of-power__candidate-ec-votes");
	}

	public static By SenateDemocratbalancemodule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='balance-of-power__candidate-lname'][text()='democrats']");
	}

	public static By SenateSeatsupforelection() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='balance-of-power__threshold-text balance-of-power__threshold-text--upper']");
	}

	public static By Senateneededforcontrol() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"//span[@class='balance-of-power__threshold-text balance-of-power__threshold-text--upper']");
	}

	public static By Senaterepublicanbalancemodule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='balance-of-power__candidate-lname'][text()='republicans']");
	}

	public static By Senateothermodule() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='balance-of-power__candidate-lname'][text()='other']");
	}

	public static By sensatearrowmedian() {
		return WrapperMethods.locatorValue(Locators.CSS, "i.balance-of-power__threshold-carot");
	}

	/////

	public List<WebElement> senatecardstatetitle() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='result-table__title']/a");
	}

	public static By Balanceofpowerwithheadshot() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='balance-of-power balance-of-power--with-headshot']");
	}

	public static By TrumpHeadshotphoto() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='candidate-headshot balance-of-power__headshot']/img[contains(@alt,'trump')]");
	}

	public static By Clintonheadshotphoto() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='candidate-headshot balance-of-power__headshot']/img[contains(@alt,'clinton')]");
	}

	public List<WebElement> percentageofvotesbelowcandidates() {
		return WrapperMethods.locateElements(Locators.CSS, "div.balance-of-power__votes-and-percent");
	}

	public static By votestowin() {
		return WrapperMethods.locatorValue(Locators.CSS, "span#votes-needed-bop-president");
	}

	public static By NationalMaptoggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tab-control__tab')][text()='national map']");
	}

	public static By Popularvotetoggle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tab-control__tab')][text()='popular vote']");
	}

	public List<WebElement> getNationalraceresultstatcount() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='tile__background circle'][@data-is-active='true']/div/div[@class='count']");
	}

	public static By toltipElectorialVote() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__subtitle']");
	}

	public static By PopularVoteHeading() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']");
	}

	public static By popularprojectedwinner() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/following-sibling::div[1]");
	}

	public static By popularElectorialvotes() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/following-sibling::div[2]//div[@class='result-table__subtitle']");
	}

	public List<WebElement> popularCandidates() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//div[contains(@class,'result-table__row ')]");
	}

	public static By popularCandiateHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//div[@class='result-table__row-header result-table__row-header-name']");
	}

	public static By popularPercentageHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//div[@class='result-table__row-header result-table__row-header-pct']");
	}

	public static By popularVotesHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//div[@class='result-table__row-header result-table__row-header-votes']");
	}

	public static By popularReportingFooter() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//div[@class='result-table__reporting']");
	}

	public static By popularTimeStampFooter() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='result-table__title'][text()='popular vote']/ancestor::div[@class='result-table core-result has-footer']//span[@class='result-table__timestamp']");
	}

	public static By exitpollheader() {
		return WrapperMethods.locatorValue(Locators.CSS, "header.exit-poll-preview__header");
	}

	public static By exitpollfooter() {
		return WrapperMethods.locatorValue(Locators.CSS, "footer.exit-poll-preview__footer");
	}

	public static By Areyouexitpollheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='exit-poll__question'][text()='Vote by Sex']");
	}

	public static By AllExitPoll_btn() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.button__contents>a");
	}

	public static By BattlegroundTab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tab-control']/div[text()='battleground states']");
	}

	public static By allStatesTab() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='tab-control']/div[text()='all states']");
	}

	public static By PageTitleBold() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='page-heading']/span");
	}

	public static By PageTitlelight() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='text-light']");
	}

	public static By SelectedState() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[1]//select");
	}

	public static By SelectedpageState() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='exit-poll-hub__filters pad-when-small']/div[2]//select");
	}

	public static By Toggletabletab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tab-control__tab')][text()='table']");
	}

	public static By Togglegraphtab() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tab-control__tab')][text()='graph']");
	}

	public static By stateresulheader() {
		return WrapperMethods.locatorValue(Locators.CSS, "h2.exit-poll-hub__heading");
	}

	public List<WebElement> PartyIndicator() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[contains(@class,'party-indicator')]");
	}

	public List<WebElement> polltable() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='exit-poll-table exit-poll-table--framed']");
	}

	public static By polltableyes() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Yes']");
	}

	public static By polltableNo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='No']");
	}

	public static By polltableClinton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Clinton']");
	}

	public static By polltableTrump() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Trump']");
	}

	public static By polltableOther() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Other/No Answer']");
	}

	public static By polltableBarksdale() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Barksdale']");
	}

	public static By polltableIsakson() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/table/thead/tr/*[@class='exit-poll__cell--header'][@data-lname='Isakson']");
	}

	public static By polltableresponds() {
		return WrapperMethods.locatorValue(Locators.XPATH, "*[@class='exit-poll-table__metadata']");
	}

	public static By polltableheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"*[@class='exit-poll__header']/*[@class='exit-poll__question']");
	}

	public List<WebElement> graphtable() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='exit-poll exit-poll--framed']");
	}

	public static By Timestamp() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='exit-poll-hub__lts']");
	}

	public List<WebElement> votingzeroedout() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='core-result-mini__candidate']//div[@class='core-result-mini__vote-percentage']");
	}

	public List<WebElement> Balootmeasurepercentage() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='result-table ballot-measure has-footer']//div[@class='result-table__bar-value']");
	}

	public List<WebElement> Balootmeasurevotes() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='result-table ballot-measure has-footer']//div[@class='result-table__row-item result-table__row-item-votes']");
	}

	public List<WebElement> Barzeroedout() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='result-table__bar-value']");
	}

	public List<WebElement> Barvotes() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='result-table__row-item result-table__row-item-votes']");
	}

	public List<WebElement> Balanceofpowerhouseallheader() {
		return WrapperMethods.locateElements(Locators.XPATH, "//*[@class='balance-of-power-house__count']");
	}

	public List<WebElement> SeatsupforElection() {
		return WrapperMethods.locateElements(Locators.CSS, "div.balance-of-power-house__seat-bubble");
	}

	public List<WebElement> allsataestabzerovalidation() {
		return WrapperMethods.locateElements(Locators.XPATH, "//*[contains(@class,'house-result__party house')]/span");
	}

	public static By Senatestatepageheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='page-heading']//span");
	}

	public static By allstatesbutton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='navigation-states__button button button--light']//span[text()='all states']");
	}

	public static By KeyButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='party-key-wrapper']//span[text()='key']");
	}

	public static By KeyButtonareaexpanded() {
		return WrapperMethods.locatorValue(Locators.XPATH, "	//div[contains(@class,'party-key__button')]");
	}

	public static By Mapstatetooltip() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='state-map__division'][@data-name='Jefferson']");
	}

	public static By esttooltip() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//div[@class='result-table__reporting']/span");
	}

	public static By timestamptooltip() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='tooltip']//*[@class='result-table__timestamp']");
	}

	public static By listallcountryheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@class='section-header pad-when-small']");
	}

	public List<WebElement> allcountrylist() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='group-wrapper']/ul/li");
	}

	public static By allcountrystateheader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div/*[@class='result-table__header']/div[@class='result-table__title']");
	}

	public static By allcountrycandidatecolumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div//div[@class='result-table__row-header result-table__row-header-name']");
	}

	public static By allcountrypercentagecolumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div//div[@class='result-table__row-header result-table__row-header-pct']");
	}

	public static By allcountryvotescolumn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"div//div[@class='result-table__row-header result-table__row-header-votes']");
	}

	public List<WebElement> whichagegroupexitpoll() {
		return WrapperMethods.locateElements(Locators.XPATH, "//*[@class='exit-poll__question'][text()='Age']");
	}

	public static By StateResultsHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='page-heading']/span");
	}

	public static By allStatesBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='navigation-states__button button button--light']");
	}

	public static By SelectStateBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--light select select--light navigation-states__button']");
	}

	public static By tinyKeyBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='party-key__button button button--light button--tiny button--inline']");
	}

	public static By allStatesDD() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='button button--light select select--light navigation-states__button']//select[@class='select__select']");
	}

	public static By presidentialLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='president']/h2");
	}

	public static By SenateLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='senate']/h2");
	}

	public static By HouseLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='house']/h2");
	}

	public static By gubernatorialLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='governor']/h2");
	}

	public static By presidentialLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='president']//a");
	}

	public static By senateLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='senate']//a");
	}

	public static By governerLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='governor']//a");
	}

	public static By houseLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='house']//a");
	}

	public static By Content() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='grid-content grid-content--fluid ']");
	}

	public static By CountryLevelMap() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='map__tiles']");
	}

	public static By PeriodicMap() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='periodic-map']");
	}

	public static By BalanceOfPower() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='balance-of-power-house']");
	}

	public static By MapTiles() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='map__tiles']");
	}

	public static By nightCapAd() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='newsletter']");
	}

	public static By footerNotes() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//footer[@id='editorial-notes']");
	}

	public static By statesLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='header-navigation__link header-navigation__link--light header-navigation__link--active']");
	}

	public static By moreCampaignNews() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='OB_HOP_41']");
	}

	public static By paidContent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob_strip_container AR_33']");
	}

	public static By stickyAdv() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='ad text-align-center ad--has-label adfuel-rendered']");
	}

	public List<WebElement> ballotScheduled() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//ul[@class='result-list']/li[*]//div[@class='result-table__reporting']/span");
	}

	public static By resultLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='header-navigation-breadcrumb__active-route']");
	}
	
	public static By PoliticsPageLinkedZone() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='zn-header']//a");
	}

	public static By PoliticsPageNONLinkedZone() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='zn-header']");
	}

	public static By PoliticscontainerLinkedHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='cn__title ']/parent::a");
	}
	
	public static By PoliticscontainerNONLinkedHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='cn__title ']/parent::ul/h2");
	}

	public static By get45() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()=45]");
	}
	public static By getCongress() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()='Congress']");
	}
	
	public static By getSecurity() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()='Security']");
	}
	public static By getTheNine() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()='The Nine']");
	}
	
	public static By getTrumpAmerica() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()='Trumpmerica']");
	}
	public static By getFooterState() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__buckets']//a[text()='State']");
	}
	public static By getFooterFacebookShareIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__content']//li[@class='social__link social__link--facebook']");
	}
	
	public static By getFooterTwitterShareIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__content']//li[@class='social__link social__link--twitter']");
	}
	public static By getFooterInstagramShareIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='l-footer__content']//li[@class='social__link social__link--instagram']");
	}
	
	
	public static By getCopyRights() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//a[@class='m-copyright__links']");
	}
	
	public static By getToS() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='m-footer__copyright']//a[text()='Terms of service']");
	}
	public static By getPrivacyGuidlines() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='m-footer__copyright']//a[text()='Privacy guidelines']");
	}
}