package um.testng.test.pom.elements;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class HomePage extends BasePage {

	public static enum weatherType {
		zip, city;
	}

	/**
	 * CNN Live Tv
	 */
	public static By liveTv() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-mobileTV");
	}

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav__logo,a.logo-cnn");
	}
	// public static By getLoginContainer() {
	// return WrapperMethods.locatorValue(Locators.ID, "js-gigya-widget");
	// }
	//
	// public static By getLoginEmail() {
	// return WrapperMethods.locatorValue(Locators.XPATH,
	// "id('gigya-login-form')//input[contains(@class,'gigya-input-text')]");
	// }
	//
	// public static By getLoginPass() {
	// return WrapperMethods.locatorValue(Locators.XPATH,
	// "id('gigya-login-form')//input[contains(@class,'gigya-input-password')]");
	// }
	//
	// public static By getErrorMsgInvalidDetails() {
	// return WrapperMethods.locatorValue(Locators.XPATH,
	// "//*[@id='gigya-login-form']//div[contains(@class,'gigya-composite-control-form-error')]/div[contains(@class,'gigya-error-msg-active')]");
	// }
	//
	// public static List<WebElement> getcookies() {
	// return
	// WrapperMethods.locateElements(Locators.XPATH,"//div/a[@href='/cookie']");
	// }
	//
	// public static List<WebElement> getFooterLinks() {
	// return WrapperMethods.locateElements(Locators.CSS,".m-legal__list__item >
	// a");
	// }
	// =======================================================================================================================================================//

	public static List<WebElement> getNavLinks() {
		return WrapperMethods.locateElements(Locators.XPATH, "id('nav')//div[contains(@class,'nav-menu-links')]//a");
	}

	public static By getHeaderWatchLive() {
		return WrapperMethods.locatorValue(Locators.CSS, ".bucket.selected div.m-pagebadge--topright a span");
	}

	public static By getHeaderWatchLivePlayer() {
		return WrapperMethods.locatorValue(Locators.ID, "cvp_1");
	}

	public static By getHomeMutedPlayer() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "cd--tool__muted-player");
	}

	public static By getHomeMutedPlayerTitle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "cn__title--muted-player");
	}

	public static By getHomeMutedPlayerTitleLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd--tool__muted-player div a");
	}

	public static By getHomeMutedPlayerContentLink() {
		return WrapperMethods.locatorValue(Locators.CSS, "#muted-player a");
	}

	public static By getHomeMutedPlayerNextShow() {
		return WrapperMethods.locatorValue(Locators.CSS, "#muted-player span");
	}

	public static By getHomeMutedPlayerNextShowHyper() {
		return WrapperMethods.locatorValue(Locators.CSS, "#muted-player h3 a");
	}

	public static By getHomeMutedPlayerShowTime() {
		return WrapperMethods.locatorValue(Locators.CSS, "#muted-player div.cd__auxiliary");
	}

	public static By getHeaderNavNews() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.news a");
	}

	public static By getHeaderNavNewsSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.news li.section");
	}

	public static By getHeaderNavNewsSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.news");
	}

	public static By getHeaderNavRgn() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.regions a");
	}

	public static By getHeaderNavRgnSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.regions li.section");
	}

	public static By getHeaderNavRgnSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.regions");
	}

	public static By getHeaderNavVid() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.video a");
	}

	public static By getHeaderNavVidSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.video li.section");
	}

	public static By getHeaderNavVidSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.video");
	}

	public static By getHeaderNavTv() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.tv a");
	}

	public static By getHeaderNavTvSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.tv  li.section");
	}

	public static By getHeaderNavTvSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.tv");
	}

	public static By getHeaderNavFeatures() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.features a");
	}

	public static By getHeaderNavFeaturesSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.features  li.section");
	}

	public static By getHeaderNavFeaturesSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.features");
	}

	public static By getHeaderNavOpinions() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.opinions a");
	}

	public static By getHeaderNavOpinionsSec() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.opinions li.section");
	}

	public static By getHeaderNavOpinionsSel() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.opinions");
	}

	public static By getHeaderNavMore() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.bucket.more");
	}

	public static List<WebElement> getHeaderNavNewsHover() {
		return WrapperMethods.locateElements(Locators.CSS, "li.bucket.news li.section");
	}

	public static List<WebElement> getHeaderNav() {
		return WrapperMethods.locateElements(Locators.CSS, "li.bucket");
	}

	public static List<WebElement> getHeaderAllNavSections() {
		// return WrapperMethods.locateElements(Locators.XPATH,"//header//a");
		return WrapperMethods.locateElements(Locators.CSS, ".nav-menu-links__link");
	}

	public static List<WebElement> getFooterAllNavSections() {
		return WrapperMethods.locateElements(Locators.XPATH, "//footer//a");
	}

	public static List<WebElement> getHeaderStories() {
		return WrapperMethods.locateElements(Locators.CSS, "div.zn-right-fluid__wrapper a span");
	}

	public static List<WebElement> getFlyoutHeadlines() {
		return WrapperMethods.locateElements(Locators.CSS, "h1.zn-right-fluid__main__headline a");
	}

	public static List<WebElement> getFlyoutSubsections() {
		return WrapperMethods.locateElements(Locators.CSS, "div.links_wrapper a");
	}

	public static List<WebElement> getFlyoutCenter() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.zn-right-fluid__mid li.container-middle h2.cd__headline");
	}

	public static List<WebElement> getFlyoutRight() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.zn-right-fluid__mid li.container-right h2.cd__headline");
	}

	public static By getTicker() {
		return WrapperMethods.locatorValue(Locators.CSS, "ul[id=js-m-ticker] li");
	}

	public static By getWeatherCard() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "js-forecast-graphic");
	}

	public static By getWeatherCardLocation() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "js-location");
	}

	public static By getWeatherCardTemperature() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-current > span:nth-child(1)");
	}

	public static By getWeatherCardHigh() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "high");
	}

	public static By getWeatherCardLow() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "low");
	}

	public static By getWeatherCardIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, "i.icon");
	}

	public static By getWeatherCardSettings() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.weather__local-query-visibility-toggle");
	}

	public static By getWeatherCardSetLocation() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "weather__local-query__search");
	}

	public static By getWeatherCardSetFar() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[data-temp=\"fahrenheit\"]");
	}

	public static By getWeatherCardSetCel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[data-temp=\"celsius\"]");
	}

	public static By getWeatherCardSet() {
		return WrapperMethods.locatorValue(Locators.CSS, "input.el-button");
	}

	public static By getWeatherCardSugg() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "tt-suggestion:nth-child(1)");
	}

	public static By skipAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#skip>a");
	}

	public static By getBreakingNews() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.breaking-news__msg");
	}

	public static By getBreakingNewsClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".breaking-news__close-btn");
	}

	public static By getCookieConsentClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-truste__consent");
	}

	public static By getTermsServiceConsentClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "user-msg--close");
	}

	public static By getSearchElement() {
		return WrapperMethods.locatorValue(Locators.CSS, "#cnnSearchSummary");
	}

	public static By getMycnnElement() {
		return WrapperMethods.locatorValue(Locators.CSS, ".msib-info-mycnn");
	}

	public static By getLoginElementLink() {
		return WrapperMethods.locatorValue(Locators.ID, "msib-login-link");
	}

	public static By getLoginElementLinkIe() {
		return WrapperMethods.locatorValue(Locators.ID, "msib-login-link");
	}

	public static By getMycnnElementLink() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "msib-link--mycnn");
	}

	public static By getMycnnElementLinkIe() {
		return WrapperMethods.locatorValue(Locators.CSS, ".msib-link--mycnn");
	}

	public static By getMycnnText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	public static By getMycnnImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".mycnn__avatar-image");
	}

	public static By getMycnnLout() {
		return WrapperMethods.locatorValue(Locators.ID, "msib-logout");
	}

	public static By getMycnnFollSec() {
		return WrapperMethods.locatorValue(Locators.CSS, ".mycnn-stream-container > h4:nth-child(1)");
	}

	public static By getMycnnFollMore() {
		return WrapperMethods.locatorValue(Locators.CSS, ".mycnn-suggested-topics > h4:nth-child(1)");
	}

	public String getMycnnUser() {
		return DriverFactory.getCurrentDriver().findElement(By.className("msib-info-username")).getText();
	}

	public static By getMycnnComment() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[id*=\"fyre-editor\"]");
	}

	public static By getMycnnPostComment() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[role=\"button\"][class*=\"fyre-post-button-new\"]");
	}

	public String getMycnnPostedComment() {
		return DriverFactory.getCurrentDriver()
				.findElement(By.cssSelector(
						"article.fyre-comment-article:nth-child(1) > div:nth-child(1) > section:nth-child(3) > div:nth-child(1) > p:nth-child(1)"))
				.getText();
	}

	public String getMycnnPostedCommentTime() {
		return DriverFactory.getCurrentDriver()
				.findElement(By.cssSelector(
						"article.fyre-comment-article:nth-child(1) > div:nth-child(1) > header:nth-child(2) > time:nth-child(5)"))
				.getText();
	}

	public List<String> getShows() {
		List<String> toreturn = new ArrayList<String>();
		toreturn.add("/programs/");
		toreturn.add("/cnn/programs/");
		toreturn.add("//ac360.blogs.cnn.com/");
		toreturn.add("//newsroom.blogs.cnn.com/");
		toreturn.add("//crossfire.blogs.cnn.com/");
		toreturn.add("//earlystart.blogs.cnn.com/");
		toreturn.add("//outfront.blogs.cnn.com/");
		toreturn.add("//thelead.blogs.cnn.com//");
		toreturn.add("//newday.blogs.cnn.com/");
		toreturn.add("//piersmorgan.blogs.cnn.com/");
		toreturn.add("//situationroom.blogs.cnn.com/");
		toreturn.add("//startingpoint.blogs.cnn.com/");
		toreturn.add("//strombo.blogs.cnn.com/");
		toreturn.add("/video/shows/anthony-bourdain-parts-unknown");
		toreturn.add("/specials/us/crimes-of-the-century/");
		toreturn.add("//globalpublicsquare.blogs.cnn.com/");
		toreturn.add("//insideman.blogs.cnn.com/");
		toreturn.add("//whatsnext.blogs.cnn.com/category/the-next-list/");
		toreturn.add("//reliablesources.blogs.cnn.com/");
		toreturn.add("//sanjayguptamd.blogs.cnn.com/");
		toreturn.add("//sotu.blogs.cnn.com/");
		toreturn.add("//yourbottomline.blogs.cnn.com/");
		toreturn.add("//yourmoney.blogs.cnn.com/");
		toreturn.add("/specials/cnn.heroes/");
		toreturn.add("/studentnews/");
		toreturn.add("//inamerica.blogs.cnn.com/");
		toreturn.add("//www.hlntv.com/shows/clark-howard/");
		toreturn.add("//www.hlntv.com/shows/dr-drew/");
		toreturn.add("//www.hlntv.com/shows/evening-express/");
		toreturn.add("//www.hlntv.com/shows/jane-velez-mitchell/");
		toreturn.add("//www.hlntv.com/shows/making-it-america/");
		toreturn.add("//www.hlntv.com/shows/morning-express-robin-meade/");
		toreturn.add("//www.hlntv.com/shows/nancy-grace/");
		toreturn.add("//www.hlntv.com/shows/raising-america/");
		toreturn.add("//www.hlntv.com/shows/showbiz-tonight/");
		toreturn.add("//www.hlntv.com/shows/weekend-express-natasha-curry/");
		toreturn.add("//www.hlntv.com/shows/what-would-you-do/");

		return toreturn;
	}

	@FindBy(css = "h1.zn-left-fluid-right-stack__main__headline a")
	public static By topStoryHeadline;

	@FindBy(css = "h1.zn-left-fluid-right-stack__main__headline")
	public static By topStoryHeadlineText;

	@FindBy(css = "a.el-button--more")
	public static By btnMore;

	@FindBy(css = "select.muted-player__switcher-select")
	public static By showsDropdown;

	public static List<WebElement> getShowsElements() {
		return WrapperMethods.locateElements(Locators.CSS, "select.muted-player__switcher-select option");
	}

	public static List<WebElement> getMobileLiveTVImgs() {
		return WrapperMethods.locateElements(Locators.CSS, "div[id=js-muted-player] a img");
	}

	public static List<WebElement> getRightHeadlines() {
		return WrapperMethods.locateElements(Locators.CSS, "div.zn-left-fluid-right-stack__sub ul.cn--idx-1 h2 a");
	};

	public static List<WebElement> getHeroHeadlines() {
		return WrapperMethods.locateElements(Locators.CSS, "div.zn-left-fluid-right-stack__main a");
	}

	public static List<WebElement> getZoneHeadlines() {
		return WrapperMethods.locateElements(Locators.CSS, "div.zn-wrapper a");
	}

	/**
	 * Gets the first instance of a static By by the given class name.
	 * 
	 * @param cssClass
	 *            The name of the class to be retrieved
	 * @return The static By of the class name provided
	 */
	public static By getModuleByClass(String cssClass) {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, cssClass);
	}

	public static By getAdRightRail1() {
		return WrapperMethods.locatorValue(Locators.CSS, "#google_ads_iframe_/8663477/CNN/homepage_2__container__");
	}

	public static By getAdRightRail2() {
		return WrapperMethods.locatorValue(Locators.CSS, "#google_ads_iframe_/8663477/CNN/homepage_1__container__");
	}

	// Home Page Search CNN Validation
	public static By getHomeHeaderSearchCNNBox() {
		return WrapperMethods.locatorValue(Locators.ID, "searchInputNav");
	}

	public static By getHomeHeaderSearchButton() {
		return WrapperMethods.locatorValue(
				// Locators.CSS,"button.search__button[type='submit']");
				Locators.CSS, ".search__submit-button");
	}

	public static By getHomeHeaderSearchButtonDev() {
		return WrapperMethods.locatorValue(Locators.CSS, "button.search__button.js-search-toggle");
	}

	public static By getHomeFooterSearchCNNBox() {
		return WrapperMethods.locatorValue(Locators.ID, "searchInputFooter");
	}

	public static By getHomeFooterSearchButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "button.search-input__button[type='submit']");
	}

	public static By getSearchNoMatch() {
		return WrapperMethods.locatorValue(Locators.ID, "cnnSearchNoMatch");
	}

	public static By getSearchEmpty() {
		return WrapperMethods.locatorValue(Locators.ID, "cnnSearchEmpty");
	}

	public static By getSearchSummary() {
		return WrapperMethods.locatorValue(Locators.CSS, "#cnnSearchSummary");
	}

	public static By getSearchInActPreviousLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pagination-arrow span.text-deactive");
	}

	public static By getSearchActNextLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pagination-arrow-right span.left");
	}

	public static By getSearchPageDigits() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pagination-digits");
	}

	public static By getSearchActPreviousLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pagination-arrow-left span.right");
	}

	public static By getSearchActPageDigit() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnnAlt");
	}

	public static By getSearchGoogleAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "#attribution a");
	}

	public static By getSearchEverything() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='Everything']");
	}

	public static By getSearchStories() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='STORIES']");
	}

	public static By getSearchVideos() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='VIDEOS']");
	}

	public static By getSearchPhotos() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='PHOTOS']");
	}

	public static By getSearchInteractives() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='INTERACTIVES']");
	}

	public static By getSearchiReport() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='IREPORT']");
	}

	public static By getSearchAnytime() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='Anytime']");
	}

	public static By getSearchPastDay() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='PASTDAY']");
	}

	public static By getSearchPastWeek() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='PASTWEEK']");
	}

	public static By getSearchPastMonth() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='PASTMONTH']");
	}

	public static By getSearchPastYear() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.facet_item label[for='PASTYEAR']");
	}

	public static By getSearchAllVideos() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnnVRimgBGSearch span.cnnVRimgLink");
	}

	public static By getSearchAllOthers() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnnVRphotoLink");
	}

	public static By getSearchSort() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sortDropMenu");
	}

	public static By getSearchSortDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".styled-select select[id ='cnn-sort2']");
	}

	public static By getSearchSortDate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//ul[contains(@class,'sortOptions')]/li[contains(@id,'date')]");
	}

	public static By getSpecialPageFB() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--facebook a");
	}

	public static By getSpecialPageTW() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--twitter a");
	}

	public static By getSpecialPageInst() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--instagram a");
	}

	// Search 404 page elements
	public static By getErrorTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error-container h1");
	}

	public static By getErrorText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error-container h2");
	}

	public static By getErrorsearch() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-container div.errorForm");
	}

	public static By getErrorsearchinput() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-container td.search-input");
	}

	public static By getErrorSearchDefaultText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-container td.search-input .search-input__text");
	}

	public static By getErrorsearchbutton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-container button");
	}

	// Market Card
	public static By getMarketCard() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__header-wrapper");
	}

	public static By getMarketCardHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__header-content");
	}

	public static By getMarketCardHeaderHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__header-content a");
	}

	public static By getMarketCardHeaderTop() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__header-top");
	}

	public static By getMarketCardHeaderBottom() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__header-bottom");
	}

	public static By getMarketCardDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__quote-content a.m-financial__quote-item--down");
	}

	public static By getMarketItemDownTitle() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-financial__quote-content a.m-financial__quote-item--down div.m-financial__quote-title");
	}

	public static By getMarketItemDownIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-financial__quote-content a.m-financial__quote-item--down div.m-financial__quote-icon");
	}

	public static By getMarketItemDownValue() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-financial__quote-content a.m-financial__quote-item--down div.m-financial__quote-value");
	}

	public static By getMarketItemDownValueChange() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-financial__quote-content a.m-financial__quote-item--down div.m-financial__quote-value-change");
	}

	public static List<WebElement> getMarketCardItem() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'m-financial__quote-content')]/a[contains(@class,'m-financial__quote-item')]");
	}

	public static List<WebElement> getMarketItemTitle() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'m-financial__quote-content')]/a[contains(@class,'m-financial__quote-item')]/div[contains(@class,'m-financial__quote-title')]");
	}

	public static List<WebElement> getMarketItemIcon() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'m-financial__quote-content')]/a[contains(@class,'m-financial__quote-item')]/div[contains(@class,'m-financial__quote-icon')]");
	}

	public static List<WebElement> getMarketItemValue() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'m-financial__quote-content')]/a[contains(@class,'m-financial__quote-item')]/div[contains(@class,'m-financial__quote-value')]");
	}

	public static List<WebElement> getMarketItemValueChange() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'m-financial__quote-content')]/a[contains(@class,'m-financial__quote-item')]/div[contains(@class,'m-financial__quote-value-change')]");
	}

	public static By getMarketQuoteBar() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-markets__quote-bar");
	}

	public static By getMarketQuoteFinancialField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-markets__quote-bar div.m-financial__field");
	}

	public static By getMarketQuoteFinancialFieldButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-markets__quote-bar div.m-financial__field-button");
	}

	public static By getMarketQuoteFinancialFieldButtonText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-financial__field-button div.js-quote-button");
	}

	public static By getMarketTimeStamp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-action-bar span.el-timestamp");
	}

	/**
	 * My CNN: Account Details Error
	 */
	public static By getMycnnAccDetailsError() {
		return WrapperMethods.locatorValue(Locators.CSS, ".mycnn__details-msg");
	}

	public static By getMycnnFbEmail() {
		// return WrapperMethods.locatorValue(Locators.ID,"email");
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@name='email']");
	}

	public static By getMycnnFbPass() {
		// return WrapperMethods.locatorValue(Locators.ID,"pass");
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@name='pass']");
	}

	public static By getMycnnFbLoginButton() {
		return WrapperMethods.locatorValue(Locators.ID, "loginbutton");
	}

	public static By getMycnnFbLoginButtonDev() {
		return WrapperMethods.locatorValue(Locators.ID, "u_0_5");
	}

	public static By getMycnnFbLoginButtonDevice() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@value='Log In']");
	}

	public static By getMycnnTwitterUser() {
		return WrapperMethods.locatorValue(Locators.CSS, "#username_or_email");
	}

	public static By getMycnnTwitterPass() {
		return WrapperMethods.locatorValue(Locators.CSS, "#password");
	}

	public static By getMycnnTwitterLogIn() {
		return WrapperMethods.locatorValue(Locators.CSS, "#allow");
	}

	public static By getMycnnGoogleEmail() {
		return WrapperMethods.locatorValue(Locators.CSS, "#Email");
	}

	public static By getMycnnGooglePass() {
		return WrapperMethods.locatorValue(Locators.CSS, "#Passwd");
	}

	public static By getMycnnGoogleNext() {
		return WrapperMethods.locatorValue(Locators.CSS, "#next");
	}

	public static By getMycnnGoogleSignin() {
		return WrapperMethods.locatorValue(Locators.CSS, "#signIn");
	}

	public static By getMycnnLinkEmail() {
		return WrapperMethods.locatorValue(Locators.CSS, "#session_key-oauthAuthorizeForm");
	}

	public static By getMycnnLinkPass() {
		return WrapperMethods.locatorValue(Locators.CSS, "#session_password-oauthAuthorizeForm");
	}

	public static By getMycnnLinkSignIn() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "allow");
	}

	public static By getMycnnAccDetails() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Account Details");
	}

	public static By getMycnnForm() {
		return WrapperMethods.locatorValue(Locators.CSS, ".form-group label");
	}

	public static By getMycnnGender() {
		return WrapperMethods.locatorValue(Locators.ID, "gender");
	}

	public static By getMycnnAlerts() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Alerts");
	}

	public static By getMycnnYourFollows() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Your Follows");
	}

	public static By getSearchMobDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnnResultsCat select[id='catDropList']");
	}

	public static By getSearchMobEverything() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'EverythingD']");
	}

	public static By getSearchMobStories() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'STORIESD']");
	}

	public static By getSearchMobVideos() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'VIDEOSD']");
	}

	public static By getSearchMobPhotos() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'PHOTOSD']");
	}

	public static By getSearchMobInteractives() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'INTERACTIVESD']");
	}

	public static By getSearchMobiReport() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='catDropList'] option[value = 'IREPORTD']");
	}

	public static By getSearchMobAnytimeDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnnResultsCat select[id='timeDropList']");
	}

	public static By getSearchMobAnytime() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='timeDropList'] option[value = 'Anytime_D']");
	}

	public static By getSearchMobPastDay() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='timeDropList'] option[value = 'PASTDAY_D']");
	}

	public static By getSearchMobPastWeek() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='timeDropList'] option[value = 'PASTWEEK_D']");
	}

	public static By getSearchMobPastMonth() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='timeDropList'] option[value = 'PASTMONTH_D']");
	}

	public static By getSearchMobPastYear() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnnResultsCat select[id='timeDropList'] option[value = 'PASTYEAR_D']");
	}

	public static List<WebElement> getHeaderMenuLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".nav-flyout .nav-flyout__submenu-item a");
	}

	public static List<WebElement> getSectionLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".nav-flyout a.nav-flyout__section-title");
	}

	public static List<WebElement> getHeaderUSMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-flyout__menu-item--us')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderWorldMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--world')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderPoliticsMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--politics')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderMoneyMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--money')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderOpinionsMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--opinions')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderHealthMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--health')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderEntertainmentMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--entertainment')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderTechMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--tech')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderStyleMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--style')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderTravelMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--travel')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLTravelMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--intl_travel')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderBleacherMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--bleacher')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderLivingMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--living')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderVideoMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--videos')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLVideoMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--intl_videos')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderMoreMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--more')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLMoreMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--intl_more')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLSportMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--sport')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLFeaturesMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--features')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static List<WebElement> getHeaderINTLRegionsMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded-menu')//div[contains(@class,'nav-flyout__menu-item--intl_regions')]//ul[contains(@class,'nav-flyout__submenu')]//a");
	}

	public static By getFlyoutFooterMenu() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-flyout-footer");
	}

	public static By getFlyoutFooterEditionMenu() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-flyout-footer div.nav-flyout-footer__edition-list");
	}

	public static By getFlyoutEditionPreference() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-flyout-footer form.edition-picker__radio-buttons");
	}

	public static By getFlyoutFooterSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-flyout-footer div.nav-flyout-footer__social");
	}

	public static By getFlyoutFooterFacebook() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".nav-flyout-footer div.nav-flyout-footer__social a.nav-flyout-footer__social-link--facebook");
	}

	public static By getFlyoutFooterTwitter() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".nav-flyout-footer div.nav-flyout-footer__social a.nav-flyout-footer__social-link--twitter");
	}

	public static By getFlyoutFooterInstagram() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".nav-flyout-footer div.nav-flyout-footer__social a.nav-flyout-footer__social-link--instagram");
	}

	public static WebElement getInstagramLogo() {
		return driver.findElement(By.cssSelector(".ehRight a.EmbedLogo"));
	}

	public static List<WebElement> getFooterEditionMenuLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'nav-flyout-footer__edition-list')]//div[contains(@class,'nav-flyout-footer__edition-list-item')]");
	}

	public static By getMenuLinks() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-menu-links");
	}

	public static List<WebElement> getVidLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//*[@class='cd__headline'][contains(@data-analytics,'video')]");
	}

	public static By gethomeVidLinks() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@class='cd__headline'][contains(@data-analytics,'video')]/a");
	}

	public static List<WebElement> getDomGalLinks() {
		return WrapperMethods.locateElements(Locators.CSS, "a[href*='gallery'] .cd__headline-icon");
	}

	public static List<WebElement> getIntlGalLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".media__icon.icon-media-gallery");
	}

	/**
	 * gets the title of the Gallery page
	 */
	public static By getGalleryHeaderTitlestat() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pg-headline");
	}

	/**
	 * gets the title of the Gallery page
	 */
	public static By getVidLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "#large-media");
	}

	public static By getBreakingNewsTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".breaking-news__title");
	}

	public static By getBreakingNewsTitleText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".breaking-news__title-text");
	}

	public static By getBreakingNewsMsg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".breaking-news__msg");
	}

	public static By getFooterDomPolitics() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='/politics'] img");
	}

	public static By getFooterDomMoney() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='//money.cnn.com'] img");
	}

	public static By getFooterDomStyle() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='/style'] img");
	}

	public static By getFooterDomBleacher() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='//bleacherreport.com'] img");
	}

	public static By getFooterIntlMoney() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"li.m-footer__title a[href='//money.cnn.com/INTERNATIONAL/'] img");
	}

	public static By getFooterIntlStyle() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='/style'] img");
	}

	public static By getFooterStaticDomPolitics() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"li.m-footer__title a[href='http://www.cnn.com/politics'] img");
	}

	public static By getFooterStaticDomMoney() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='//money.cnn.com'] img");
	}

	public static By getFooterStaticDomStyle() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='http://www.cnn.com/style'] img");
	}

	public static By getFooterStaticDomBleacher() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.m-footer__title a[href='//bleacherreport.com'] img");
	}

	public static By getFooterStaticIntlMoney() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"li.m-footer__title a[href='//money.cnn.com/INTERNATIONAL/'] img");
	}

	public static By getFooterStaticIntlStyle() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"li.m-footer__title a[href='http://edition.cnn.com/style'] img");
	}

	public static By getForgotPassword() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//a[contains(@class,'gigya-forgotPassword')]");
	}

	public static By getPassWordResetEmail() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-reset-password-form']/div[contains(@class,'gigya-layout-row')]/div[contains(@class,'gigya-composite-control-textbox')]/input");
	}

	public static By getPassWordResetSubmit() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-reset-password-form']/div[contains(@class,'gigya-layout-row')]/div[contains(@class,'gigya-composite-control-submit')]/input");
	}

	public static By getPasswordResetMessage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@id,'gigya-forgot-password-success-screen')]//label[contains(@class,'gigya-message')]");
	}

	public static By getPasswordResetInvalidEmailErrorMsg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-reset-password-form']//div[contains(@class,'gigya-composite-control-form-error')]/div[contains(@class,'gigya-error-msg-active')]");
	}

	public static By getClickHereButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-login-form']//div[contains(@class,'gigya-layout-row')]//label/a[contains(@class,gigya-register-here-link)]");
	}

	public static By getTermsOfService() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='register-site-login']/div[contains(@class,'gigya-composite-control-checkbox')]/label/span[contains(@class,'gigya-label-text')]/a[1]");
	}

	public static By getPrivacyPolicy() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='register-site-login']/div[contains(@class,'gigya-composite-control-checkbox')]/label/span[contains(@class,'gigya-label-text')]/a[2]");
	}

	public static By getRegisterDetailsSubmit() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#register-site-login > div.gigya-composite-control.gigya-composite-control-submit > input.gigya-input-submit");
	}

	public static By getErrorMsg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='register-site-login']//div[contains(@class,'gigya-layout-row')]//div[contains(@class,'gigya-composite-control-textbox')]/span[contains(@class,'gigya-error-msg-active')]");
	}

	public static By getLoginElement() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "msib-info-login");
	}

	public static By getLoginEmail() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-text')]");
	}

	public static By getLoginPass() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-password')]");
	}

	public static By getLoginButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-submit')]");
	}

	public static By getMycnnFbLogin() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@data-gigya-provider,'facebook')]");
	}

	public static By getMycnnTwitterLogin() {
		// return
		// WrapperMethods.locatorValue(Locators.CSS,".login-button
		// div[gigid='twitter']
		// div");
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@data-gigya-provider,'twitter')]");
	}

	public static By getMycnnGoogleLogin() {
		// return
		// WrapperMethods.locatorValue(Locators.CSS,".login-button
		// div[gigid='googleplus']");
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@data-gigya-provider,'googleplus')]");
	}

	public static By getMycnnLinkLogin() {
		// return
		// WrapperMethods.locatorValue(Locators.CSS,".login-button
		// div[gigid='linkedin']");
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@data-gigya-provider,'linkedin')]");
	}

	public static By getMycnnAccName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-profile-form']//div[contains(@class,'first-name-row')]/div[contains(@class,'gigya-composite-control-textbox')]//input[contains(@name,'profile.firstName')]");
	}

	public static By getMycnnAccLName() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-profile-form']//div[contains(@class,'last-name-row')]//label//span");
	}

	public static By getMycnnEmail() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-profile-form']/div[contains(@class,'gigya-layout-row')]/div[contains(@class,'gigya-composite-control-textbox')]/label/span[contains(.,'Email')]");
	}

	public static By getMycnnZip() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-profile-form']/div[contains(@class,'gigya-layout-row')]/div[contains(@class,'gigya-composite-control-textbox')]/label/span[contains(.,'Zip code:')]");
	}

	public static By getMycnnSave() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-profile-form']/div[contains(@class,'gigya-layout-row')]/div[contains(@class,'gigya-composite-control-submit')]/input");
	}

	public static By getErrorMsgInvalidDetails() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='gigya-login-form']//div[contains(@class,'gigya-composite-control-form-error')]/div[contains(@class,'gigya-error-msg-active')]");
	}

	public static By getFooterWeather() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@data-cnn-resource,'weather')]");
	}

	public static By getFooterWeatherContent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='js-weather__footer']");
	}

	public static By checkbreakingnews() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.breaking-news");
	}

	public static By checkmenuexpanded() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#nav-expanded");
	}

	public static By hamburger_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='menu']");
	}

	public static By getTimelineBlock() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-livefyre-social-live-blog");
	}

	public static By getTimelineFeed() {
		return WrapperMethods.locatorValue(Locators.CSS, ".live_feed-mini");
	}

	public static By getTimelineTitleLive() {
		return WrapperMethods.locatorValue(Locators.CSS, ".live_feed-header span.live_feed-title");
	}

	public static By getTileLinetitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".live_feed-header span.live_feed-desc");
	}

	public static By getMainTimelinetext() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.live_feed-stream>div>span");
	}

	public static By getdarktimelinebg() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.live_feed-stream");
	}

	public static By getdarkTimelinetext() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.live_feed-stream>div>span>p>a");
	}

	public static By getHeaderSearch() {
		// return WrapperMethods.locatorValue(Locators.ID,"searchInputNav");
		return WrapperMethods.locatorValue(Locators.CSS, "div#search-button");
	}

	public static String domestic_radiotext[] = { "All CNN", "U.S.", "World", "politics", "money", "Opinions", "Health",
			"Entertainment", "style", "Travel", "bleacher" };

	public static String INTL_radiotext[] = { "All CNN", "News", "money", "Entertainment", "Tech", "Sport", "Travel",
			"style" };

	public static By getSearchMobileSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sectionCombo");
	}

	public static By getSearchMobileSectionExpand() {
		return WrapperMethods.locatorValue(Locators.CSS, ".dropExpand");
	}

	public static By getsearchlaunchpage() {
		return WrapperMethods.locatorValue(Locators.CSS, "input#searchInputTop");
	}

	public static By getcleartext() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.clearText");
	}

	public static List<WebElement> getradiotext() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.zn-column.zn-column--idx-0.zn-column--col-0.search-cat-list ul.facet_list>li");
	}

	public static By getExpandButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#expandButtonContainer button#expandBtn");
	}

	public static By getClickThroughButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.clickButtonContainer button#bannerClickBtn");
	}

	public static By getNormalTopBannerAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#banner");
	}

	public static By getExpandedTopBannerAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#expansion");
	}

	public static By getCloseButton() {
		return WrapperMethods.locatorValue(Locators.ID, "closeBtn");
	}

	public static By getFrameElement() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//iframe[starts-with(@id, 'ebBannerIFrame')]");
	}

	public static By hamburgerMenu() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='nav-menu js-navigation-hamburger']");

	}

	public static By hamburgerMenuItems() {
		return WrapperMethods.locatorValue(Locators.ID, "nav-expanded");
	}

	public static By headerSearchBoxText() {
		return WrapperMethods.locatorValue(Locators.ID, "search-input-field");
	}

	public static By footerSearchBoxText() {
		return WrapperMethods.locatorValue(Locators.ID, "searchInputFooter");

	}

	public static By searchButton() {
		return WrapperMethods.locatorValue(Locators.ID, "search-button");
	}

	public static By USPloiticsSearchBoxText() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "search__input-field");
	}

	public static By moneySearchBoxText() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@id='symb']");
	}

	public static By styleSearchBoxText() {
		return WrapperMethods.locatorValue(Locators.ID, "searchInputTop");
	}

	public static By getPoliticsPageLoad() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='container']//a[@class='logo-links__politics']");
	}

	public static List<WebElement> AppiaElement() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='appia-container js-appia adfuel-rendered']");
	}

	public static List<WebElement> OutbrainOb() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@id='ob_holder']");
	}

	public static By OutBrainclass() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='ob_holder']/parent::div");
	}

	public static By outbrainimagedatacut() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-item active']//div[@class='el__resize']//img");
	}

	public static By getstylePageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-cnn");
	}

	public static By Subscribeheader() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='zn-header zn-header__text-page_header']");
	}

	public static By Sharebar() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='js-gigya-sharebar gigya-sharebar']");
	}

	public static List<WebElement> getsubscribemedia() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='media']");
	}

	public static By Subscribeheaderinfo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='headerInfo']");
	}

	public static By getnightcapimage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'nightcap')]");
	}

	public static By getnightcapsubtitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'nightcap')]/parent::div/parent::article/div[@class='subTitle']");
	}

	public static By getnightcapemaildesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'nightcap')]/parent::div/parent::article/div[@class='emaildescription']");
	}

	public static By getreliableresourceimg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'reliable')]");
	}

	public static By getreliableresourcesubtitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'reliable')]/parent::div/parent::article/div[@class='subTitle']");
	}

	public static By getreliableresourcedesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'reliable')]/parent::div/parent::article/div[@class='emaildescription']");
	}

	public static By getfivethingsimg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'fivethings')]");
	}

	public static By getfivethingssubtitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'fivethings')]/parent::div/parent::article/div[@class='subTitle']");
	}

	public static By getfivethingsdesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'fivethings')]/parent::div/parent::article/div[@class='emaildescription']");
	}

	public static By getquestmeansbusinessimg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'questmeansbusiness')]");
	}

	public static By getquestmeansbusinesssubtitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'questmeansbusiness')]/parent::div/parent::article/div[@class='subTitle']");
	}

	public static By getquestmeansbusinessdesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'questmeansbusiness')]/parent::div/parent::article/div[@class='emaildescription']");
	}

	public static By getgrreatbigstoryimg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'greatbigstory')]");
	}

	public static By getgrreatbigstorysubtitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'greatbigstory')]/parent::div/parent::article/div[@class='subTitle']");
	}

	public static By getgrreatbigstorydesc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='media']/img[contains(@src,'greatbigstory')]/parent::div/parent::article/div[@class='emaildescription']");
	}

	public static By nightcapnewsletter_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='a4165679e3']");
	}

	public static By reliablesourcenewsletter_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='e95cdc16a9']");
	}

	public static By fivethingsnewsletter_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='6da287d761']");
	}

	public static By questmeansbusinessnewsletter_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='6711ab179f']");
	}

	public static By greatbigstorynewsletter_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='87688954aa']");
	}

	public static By subscribebutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@class='selectletter']");
	}

	public static By Subscribepolicy() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='privacy']");
	}

	public static By errormessage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='email-input errorDisplay']/span");
	}

	public static By Subscribemailtextbox() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@id='subEmail']");
	}

	public static By subscribedmessage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='l-container']/div[@class='zn-header']");
	}

	public static By Emailaddresstext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='signup-text']");
	}

	public static By thanksforsigningmsg() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='zn-header__text-page_header']");
	}

	public static By SubscriptionFbicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='gigyaShareBar_0']");
	}

	public static By SubscriptionEmailicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='gigyaShareBar_0-reaction0']");
	}

	public static By SubscriptionTwittericon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='gigyaShareBar_1-reaction0']");
	}

	public static By SubscriptionMoreicon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-share");
	}

	public static By SubscriptionMessenger() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='gig-button gig-share-button gig-button-up gig-button-count-none']");
	}

	public static List<WebElement> SubscriptionMessengerr() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@data-social-media-name='messenger']");
	}

	public static By FBLaunchvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@id='email']");
	}

	public static By FBpassLaunchvalidation() {
		return WrapperMethods.locatorValue(Locators.CSS, "#pass");
	}

	public static By TwitterLAunchvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//textarea[@id='status']");
	}

	public static By twitteremailtextbox() {
		return WrapperMethods.locatorValue(Locators.CSS, "#username_or_email");
	}

	public static By twitterpassword() {
		return WrapperMethods.locatorValue(Locators.CSS, "#password");
	}

	public static By getmorevalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='gig-simpleShareUI-caption']/div[text()='Share with your friends']");
	}

	public static By getMorevalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='gig-simpleShareUI-caption']");
	}

	public static List<WebElement> getmoreiconvalidation() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='gig-simpleShareUI-content']/div");
	}

	public static By getEmailtovalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//input[@id='gig_1465907909997_showShareUI_tbFriendsEmail']");
	}

	public static By getEmailclose() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//table[contains(@id,'showShareUI_emailCanvas')]/tbody/tr//table/tbody/tr/td[2]/div/img");
	}

	public static By getEmailShareTo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[contains(@id,'showShareUI_tbFriendsEmail')]");
	}

	public static By getheadermoreicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='share-icon Share']");
	}

	public static By MessengerSharecontainer() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='fb-send-to-messenger-container']");
	}

	public static By Messengercontent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='messenger-content']/p");
	}

	public static By Messengerbutton() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='_3sxx']");
		return WrapperMethods.locatorValue(Locators.CSS, "div.fb-send-to-messenger.fb_iframe_widget span");
	}

	public static List<WebElement> searchradiobutton() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='search-facets display-facets']/ul[@class='facet_list']/li[@class='facet_item']/input[@type='radio']");
	}

	public static By Searchheadline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='cd__headline-text']");
	}

	public static By searchvalidation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='cnnSearchSummary']");
	}

	public static By getInter() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='item item--edition'][contains(text(),'International')]");
	}

	public static By getDom() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//li[@class='item item--edition'][contains(text(),'U.S.')]");
	}

	public static By feedbackGoPageLoad() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='feedback-headline pg-headline']");
	}

	public static By EmailLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'Email')]");
	}

	public static By EmailTextBox() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-go-email");
	}

	public static By PlatformLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='y-label'][contains(text(),'Platform')]");
	}

	public static By WebLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'Web')]");
	}

	public static By WebRadioBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'Web')]/input");
	}

	public static By iPadLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'iPad')]");
	}

	public static By iPadRadioBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'iPad')]/input");
	}

	public static By DefectiveLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='x-label'][contains(text(),'Defective')]");
	}

	public static By ExcellentLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='x-label'][contains(text(),'Excellent')]");
	}

	public static List<WebElement> LabelItems(String label) {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//span[@class='y-label'][contains(text(),'" + label + "')]//following-sibling::ul[1]/li[*]/label");
	}

	public static List<WebElement> LabelRadioBtns(String label) {
		return WrapperMethods.locateElements(Locators.XPATH, "//span[@class='y-label'][contains(text(),'" + label
				+ "')]//following-sibling::ul[1]/li[*]/label/input");
	}

	public static By ItemsLabel(String item) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='y-label'][contains(text(),'" + item + "')]");
	}

	public static By AdditionalCmts() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//label[contains(text(),'Additional Comments')]");
	}

	public static By AdditionalCmtsTxtBox() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-go-comments");
	}

	public static By SendFeedback() {
		return WrapperMethods.locatorValue(Locators.ID, "js-go-send");
	}

	public static List<WebElement> ErrorMsg() {
		return WrapperMethods.locateElements(Locators.XPATH, "//span[@class='feedback-form-error']");
	}

	public static By SuccMsg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='feedback-form__status']//span[@id='js-feedback-message']");
	}

	public static By StyleSeachButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "button.search__button");
	}

	public static List<WebElement> getSearchradiobuttos() {
		return WrapperMethods.locateElements(Locators.XPATH, "//input[@name='section']");
	}

	public static By getcolor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='nav__color-strip']");
	}

	public static By getHeaderTop() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='metadata-header__top']");
	}

	public static By getFB() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='gigyaShareBar_0-reaction0']");
	}

	public static By opinioncarouselstd() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@id='homepage-magellan-zone-2']//div[@class='zn__containers']//article[@data-section-name='homepage3']//img");
	}

	public static By SoundCloudPlayer() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='el-embed-soundcloud']");
	}

	public static By cookiepolicytext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='widget']//a/span[text()='Cookie policy']");
	}

	public static By cookiepolicyhref() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id='widget']//a/span[text()='Cookie policy']/ancestor::a");
	}

	public static By embeddedspundcloudmobile() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='el-embed-soundcloud']");
	}

	public static By SoundcloudLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='widget']//a[contains(@class,'logo inverse')]");
	}

	public static By SoundcloudTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='widget']//a[contains(@class,'title__h1')]");
	}

	public static By SoundcloudDescription() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='widget']//a[contains(@class,'title__h2')]");
	}

	public static By SoundCloudPlay_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@class='playButton medium']");
	}

	public static List<WebElement> VimeoPlayer() {
		return WrapperMethods.locateElements(Locators.CSS, "#player");
	}

	public static By VimeoiFrame() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//iframe[@class='el-embed-vimeo__video']");
	}

	public static List<WebElement> Vimeoimg() {
		return WrapperMethods.locateElements(Locators.CSS, "#player img");
	}

	public static List<WebElement> Vimeoheaders() {
		return WrapperMethods.locateElements(Locators.CSS, "#player .headers a");
	}

	public static List<WebElement> VimeoSubTitle() {
		return WrapperMethods.locateElements(Locators.CSS, "#player .sub-title a");
	}

	public static List<WebElement> VimeoLikeButton() {
		return WrapperMethods.locateElements(Locators.XPATH, "//button[@class='like-button rounded-box']");
	}

	public static List<WebElement> VimeoWatchlaterButton() {
		return WrapperMethods.locateElements(Locators.XPATH, "//button[@class='watch-later-button rounded-box']");
	}

	public static List<WebElement> VimeoShareButton() {
		return WrapperMethods.locateElements(Locators.XPATH, "//button[@class='share-button rounded-box']");
	}

	public static List<WebElement> VimeoLogo() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='logo']");
	}

	public static By VineiFrame() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//iframe[contains(@class,'el-embed-vine')]");
	}

	public static By VineLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='icon-v_logo logo']");
	}

	public static By VineVolumneIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'VolumeControl__icon')]");
	}

	public static By VineDescription() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='description']");

	}

	public static By VineDescriptionhref() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='description']/a");

	}

	public static By Vineloopcount() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='loop-count']");
	}

	public static By LiveFyreEmbedded() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element')]");
	}

	/*
	 * public static By SoundCloudPlayer() { return
	 * WrapperMethods.locatorValue(Locators.XPATH,
	 * "//div[@class='visualAudible']"); }
	 */
	public static By getYoutubeWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-embed-youtube iframe.el-embed-youtube__content");
	}

	public static By getVineWidget() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='el-embed-vine']");
	}

	public static By getVimeoWidget() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='el-embed-vimeo']");
	}

	public static By gettwitterWidget() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='twembed el-embed-twitter']");
	}

	public static By getFacebookWidget() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='fbelement el-embed-facebook']");
	}

	public static By getStorifyWidget() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='el__leafmedia el__leafmedia--storify']");
	}

	public static By getSendToMsgr() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='gig-button-container gig-button-container-count-none gig-button-container-messenger gig-share-button-container gig-button-container-horizontal']");
	}

	public static By getMessengerContent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='messenger-content']");
	}

	public static By getSendtoMessenger() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='fb-send-to-messenger  fb_iframe_widget']");
	}

	public static By getNotYou() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='_3sxz pluginNotYouLink']");
	}

	public static By getMeta() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//meta[@property='fb:pages']");
	}

	public static By getRightrailFBMsgr() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='pg-rail pg-rail-tall__rail']//div[@class='share-bar-messenger-container gigya-sharebar-element gig-bar-container gig-share-bar-container']");
	}

	public static By fbMsgrDialogClose() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='fb-send-to-messenger-container']//div[@class='close-button']");
	}

	public static By ErrorMsg2() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='cnnTryDifferentOptionsBlock']");
	}

	public static By sectExpd() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//li[@id='allcnn']");
	}

	public static By travelLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//li[@id='travel']");
	}

	public static By styleLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//li[@id='style']");
	}

	public static By getfooterWeather() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__footer-content']/a[@class='el-weather__footer-link']");
	}

	public static List<WebElement> headerSubsections() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='nav-menu-links']/a");
	}

	public static By editionExpand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"#nav > div.nav__container > div.js-edition-picker.edition-picker > div.edition-picker__current-edition > span");
	}

	public static By usedition() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='nav__container']//div[@class='js-nav-editions edition-picker__unabridged nav-section--expanded']//label[1]//input");
	}

	public static By intedition() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='nav__container']//div[@class='js-nav-editions edition-picker__unabridged nav-section--expanded']//label[2]//input");
	}

	public static By moneyLinkDev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='nav-flyout__menu-item nav-flyout__menu-item--money']/a");
	}

	public static By getTOS() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='user-msg headerless user-msg-flexbox']");
	}

	public static List<WebElement> getcookies() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div/a[@href='/cookie']");
	}

	public static By getPolicy() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div/a[@href='/privacy']");
	}

	public static By getTerms() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div/a[@href='/terms']");
	}

	public static By getCloseBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='user-msg--close js-user-msg--close']");
	}

	public static By getIagree() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='user-msg--agree js-user-msg--agree']");
	}

	public static List<WebElement> getAllHamburgerLinks() {
		return WrapperMethods.locateElements(Locators.CSS, "a.nav-flyout__submenu-link");
	}

	public static List<WebElement> navSectionsubmenu() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@id='nav-section-submenu']//a");
	}

	// public static List<WebElement> getAllFooterLinks() {
	// return WrapperMethods.locateElements(Locators.CSS,"ol.m-footer__bucket
	// a");
	// }

	public static By ApolloAD() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'background1')]");
	}

	public static List<WebElement> Apolloidelement() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@id='ad_bnr_atf_02']");
	}

	public static By ApolloSSID() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//html/head/script[text()]");
	}

	public static By Adthirdzone() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@class,'zn-homepage3-zone-1')]//div[@data-ad-id='ad_rect_btf_02']//iframe");
	}

	public static By BrandingBanner() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[contains(@class,'branding')]");
	}

	public static By ShowDescafterresize() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'show-desc')]");
	}

	public static By ShowDesparagraphcafterresize() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[contains(@class,'description')]");
	}

	public static By HideDesafterresize() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'hide-desc')]");
	}

	public static By emailtextbox() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@class='cmcs-input-include']");
	}

	public static By emailerrormessage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='js-subscribe-error']");
	}

	public static By emailsuccessmessage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='js-state-success']/h3");
	}

	public static By emailsuccessdesc() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='js-cnn-news-ok']");
	}

	public static By Subscribe_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@class='js-mc-submit']");
	}

	public static By RSSFeedImageElement() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='health-zone-2']//ul[contains(@class,'cn--idx-0')]//img");
	}

	public static List<WebElement> getJustfunContainer() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//ul[contains(@class,'cn cn-list-hierarchical-small-horizontal cn--idx-1 cn-coverageContainer_')]");
	}

	public static List<WebElement> JFFsections() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//ul[contains(@class,'cn cn-list-hierarchical-small-horizontal cn--idx-1 cn-coverageContainer_')]//li//span[@class='cd__headline-text']");
	}

	public static By getJustfun() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[contains(text(),'Just For Fun')]");
	}

	public static By getCopyright() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='copyright']");
	}

	public static By HeadereditionPicker() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='nav__container']//div[@class='edition-picker__current-edition']");
	}

	public static By confirmBtnHeader() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//button[@class='js-edition-confirm edition-picker__confirm-button']//ancestor::div[@class='nav__container']");
	}

	public static List<WebElement> Sections() {
		return WrapperMethods.locateElements(Locators.XPATH, "//section[@data-containers]");
	}

	public static By SubscriptionEmailContainer() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='email-container']");
	}

	public static By SubscriptionEmailForm() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='emailForm']");
	}

	public static By SubscribepolicyLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='privacy']/a");
	}

	public static By getSponsorLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav .logo");
	}

	public static By getHomeFooterSearchCNNBoxCC() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//input[@class='search-input__text'][@id='searchInputFooter']");
	}

	public static By getEntertainmentSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-toggle");
	}

	public static By getPageLoadEPU() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//body//img[@class='logo']");
	}

	public static By getbourdainImage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//body//div[@class='logos']");
	}

	public static By getWanMrBourdain() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@id='p-one']");
	}

	public static By getMailLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='mail-logo']");
	}

	public static By getmailidtextarea() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='enter']//input[@id='subEmail']");
	}

	public static By clickArrow() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='arrow']");
	}

	public static By getSubMsg() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='message submitted']");
	}

	public static By skip() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#skip>a");
	}

	public static By verifyGoogleAd() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id='google_ads_iframe_/8663477/CNN/homepage_2__container__']");
	}

	public static By verifyGoogleAdCont1() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id='google_ads_iframe_/8663477/CNN/homepage_1__container__']");
	}

	public static By verifyGoogleAdCont3Inlt() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id='google_ads_iframe_/8663477/CNNi/homepage_3__container__']");
	}

	public static By verifyGoogleAdContIntl() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id='google_ads_iframe_/8663477/CNNi/homepage_1__container__']");
	}

	public static By getFooterSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, "#searchInputFooter");
	}

	public static By getSearchField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search__input-field");
	}

	public static By getNavMenu() {
		return WrapperMethods.locatorValue(Locators.CSS, "#menu.nav-menu");
	}

	public static By getcnnSearchSummary() {
		return WrapperMethods.locatorValue(Locators.CSS, "#cnnSearchSummary");
	}

	public static By getLoginContainer() {
		return WrapperMethods.locatorValue(Locators.ID, "js-gigya-widget");
	}

	public static List<WebElement> getFooterLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".m-legal__list__item > a");
	}

	public static By getWeatherCardHM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='l-footer__tools--weather']");
	}

	public static By getWeatherCardLocationHM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='el-weather__footer-location js-el-weather__footer-location']");
	}

	public static By getWeatherCardTemperatureHM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='el-weather__footer-temperature']");
	}

	public static By getWeatherCardIconHM() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//span[@class='el-weather__footer-icon-wrapper js-el-weather__footer-icon-wrapper']");
	}
	public static By linkSectionsMoney() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[text()='Money']");

	}
	public static By emailtextboxx() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@class='cmcs-input-include']");
	}

}