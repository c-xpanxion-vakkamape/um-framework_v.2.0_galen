package um.testng.test.pom.functions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltestpack.EnvironmentHelper;
import um.testng.test.alltestpack.Prefs;
import um.testng.test.alltests.BaseTestMethods;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.BasePage;
import um.testng.test.pom.elements.HomePage;
import um.testng.test.pom.elements.HomePage.weatherType;
import um.testng.test.pom.elements.PoliticsPage;
import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.pom.elements.WeatherPage;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class HomePageFunctions {

	public void testHomePageSearch(String browser, String machineType, String edition) {
		BaseTestMethods.preSteps();

		if (machineType.equalsIgnoreCase("Desktop")) {
			WrapperMethods.verifyElement(HomePage.getHeaderSearch(), "Search Button ");
			WrapperMethods.click(HomePage.getHeaderSearch(), "Search Button is clicked",
					"Search Button is not clicked");
			WrapperMethods.sendkeysWE(HomePage.getSearchField(), Keys.BACK_SPACE,
					"Giving Backspace- we clearing contents in search box",
					"BackSpace function is not working in search box");
			WrapperMethods.sendkeys(HomePage.getSearchField(), "Shows", "Entered text in search box",
					"Text is not entering in search box");
			WrapperMethods.sendkeysWE(HomePage.getSearchField(), Keys.ENTER, "Pressed Enter through the Key ",
					"Pressed Enter in search box  is not working ");
		} else {
			WrapperMethods.click(HomePage.getNavMenu(), "Hamburger menu clicked", "Hamburger menu not clicked");
			WrapperMethods.sendkeysWE(HomePage.getSearchField(), Keys.BACK_SPACE,
					"Giving Backspace- we clearing contents in search box",
					"BackSpace function is not working in search box");
			WrapperMethods.sendkeys(HomePage.getSearchField(), "Shows", "Entered text in search box",
					"Text is not entering in search box");
			WrapperMethods.sendkeysWE(HomePage.getSearchField(), Keys.ENTER, "Pressed Enter through the Key ",
					"Pressed Enter in search box  is not working ");
		}

		WrapperMethods.waitForVisiblity(HomePage.getcnnSearchSummary(), 30, "Displaying Results Header is displayed",
				"Displaying Results Header is not displayed");

		WrapperMethods.contains_Text(HomePage.getcnnSearchSummary(), "Shows",
				"Entered text is displayed in Result Header", "Entered text is not displayed in Result Header");

	}

	public void testLoginInvalidDetails(String browser, String machineType, String edition) {
		BaseTestMethods.preSteps();
		WrapperMethods.waitForVisiblity(HomePage.getLoginContainer(), 30, "Login container is visible",
				"Login container is not visible");
		if (!browser.equalsIgnoreCase("SAFARI") && machineType.equalsIgnoreCase("Desktop")) {
			WrapperMethods.moveToElementClick(HomePage.getLoginEmail());
		} else {
			WrapperMethods.clearEleText(HomePage.getLoginEmail(), "Cleared the Email Id text box",
					"Not able to clear the email id text box");
		}

		WrapperMethods.enter_Text(HomePage.getLoginEmail(), "asdasdfasdfadsf", "Entered the invalid login id ",
				"Not able to enter the login id(invalid id) ");

		WrapperMethods.clearEleText(HomePage.getLoginPass(), "Cleared the password text field",
				"Not able Cleared the password text field");

		if (browser.startsWith("IE") || browser.equalsIgnoreCase("SAFARI") || machineType.equalsIgnoreCase("Desktop")) {
			WrapperMethods.sendkeys(HomePage.getLoginPass(), "cnnweqa", "Entered the Password ",
					"Not able to enter the password ");
			WrapperMethods.click(HomePage.getLoginButton(), "Login");
		} else {
			WrapperMethods.moveToElement(HomePage.getLoginPass());
			WrapperMethods.sendkeys(HomePage.getLoginPass(), "cnnweqa", "Entered the Password ",
					"Not able to enter the password ");
			WrapperMethods.moveToElementClick(HomePage.getLoginButton());
		}

		WrapperMethods.waitForVisiblity(HomePage.getErrorMsgInvalidDetails(), 30, "Error Message is displayed!!",
				"Error msg is not displayed!!");
		WrapperMethods.contains_Text(HomePage.getErrorMsgInvalidDetails(), "Invalid login or password",
				"Invalid login or password- Error msg ", "Invalid login or password - Error msg is not shown");
	}

	public void testLoginSignIn(String browser, String machineType, String edition) {
		BaseTestMethods.preSteps();

		if (WrapperMethods.isDisplayed(HomePage.getMycnnLout())) {
			WrapperMethods.clickJavaScript(HomePage.getMycnnLout(), "Log out button is clicked",
					"Log out button not able to clicked");
			UMReporter.log(LogStatus.PASS, "User is already log in and hence logging out");
		}

		WrapperMethods.waitPresenceOfEleLocated(HomePage.getLoginContainer(), 30);
		// if (!browser.equalsIgnoreCase("SAFARI") &&
		// machineType.equalsIgnoreCase("Desktop")) {
		WrapperMethods.moveToElementClick(HomePage.getLoginEmail());
		// } else {
		WrapperMethods.clearEleText(HomePage.getLoginEmail(), "Cleared the Email Id text box",
				"Not able to clear the email id text box");
		// }

		WrapperMethods.enter_Text(HomePage.getLoginEmail(), "cnnweqa@gmail.com", "Entered the Valid login id ",
				"Not able to enter the login id(Valid id) ");

		WrapperMethods.clearEleText(HomePage.getLoginPass(), "Cleared the password text field",
				"Not able Cleared the password text field");

		// if (browser.startsWith("IE") || browser.equalsIgnoreCase("SAFARI") ||
		// machineType.equalsIgnoreCase("Desktop")) {
		// WrapperMethods.sendkeys(HomePage.getLoginPass(), "cnnweqa", "Entered
		// the Password ",
		// "Not able to enter the password ");
		// WrapperMethods.click(HomePage.getLoginButton(), "Login");
		// } else {
		WrapperMethods.moveToElement(HomePage.getLoginPass());
		WrapperMethods.sendkeys(HomePage.getLoginPass(), "cnnweqa", "Entered the Password ",
				"Not able to enter the password ");
		WrapperMethods.moveToElementClick(HomePage.getLoginButton());
		// }

		WrapperMethods.waitForVisiblity(HomePage.getMycnnLout(), 30, "Logged in successfully",
				"Login failed please check manually (Password might be changed..)");
	}

	public void TOSBorder(String browser, String machineType, String edition) {
		if (browser.contains("PERFECTO_A")) {
			DriverFactory.getCurrentDriver().manage().deleteAllCookies();
		}
		WrapperMethods.isDisplayed(HomePage.getTOS(), "Terms of services popup is displayed",
				"Terms of services popup is not displayed");

		WrapperMethods.assertIsTrue(
				WrapperMethods.getCssValue(HomePage.getTOS(), "border-bottom-width").equals("1px")
						&& WrapperMethods.getCssValue(HomePage.getTOS(), "border-bottom-style").equals("solid"),
				"Bottom Border of Terms of Services(TOS) are bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-bottom-width"),
				"Border of Terms of Services(TOS) are not bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-bottom-width"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getCssValue(HomePage.getTOS(), "border-left-style").equals("solid")
						&& WrapperMethods.getCssValue(HomePage.getTOS(), "border-left-width").equals("1px"),
				"Left side Border of Terms of Services are bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-left-width"),
				"Left side Border of Terms of Services are not bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-left-width"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getCssValue(HomePage.getTOS(), "border-right-width").equals("1px")
						&& WrapperMethods.getCssValue(HomePage.getTOS(), "border-right-style").equals("solid"),
				"Right side Border of Terms of Services are bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-right-width"),
				"Right side Border of Terms of Services are not bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-right-width"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getCssValue(HomePage.getTOS(), "border-top-style").equals("solid")
						&& WrapperMethods.getCssValue(HomePage.getTOS(), "border-top-width").equals("1px"),
				"Top Border of Terms of Services are bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-top-width"),
				"Top Border of Terms of Services are not bold "
						+ WrapperMethods.getCssValue(HomePage.getTOS(), "border-top-width"));

		WrapperMethods.contains_Text(HomePage.getPolicy(), "Privacy Policy");
		WrapperMethods.contains_Text(HomePage.getTerms(), "Terms of Service");

		try {
			WrapperMethods.brokenLinkValidation(HomePage.getPolicy());
			WrapperMethods.brokenLinkValidation(HomePage.getTerms());
		} catch (IOException e) {
			UMReporter.log(LogStatus.FAIL, "Error in URL");
			UMReporter.log(LogStatus.INFO, e.getMessage());

		}

		// WrapperMethods.isDisplayed(HomePage.getCloseBtn(), "Close button is
		// displayed", "Close button is not displayed");

		WrapperMethods.isElementPresentOrNot(HomePage.getCloseBtn());
		if (edition.equalsIgnoreCase("INTL")) {

			WrapperMethods.contains_Text(HomePage.getcookies().get(1), "cookies");
			try {
				WrapperMethods.brokenLinkValidation(HomePage.getcookies().get(1));
				WrapperMethods.brokenLinkValidation(HomePage.getcookies().get(0));
			} catch (IOException e) {
				UMReporter.log(LogStatus.FAIL, "Error in URL");
				UMReporter.log(LogStatus.INFO, e.getMessage());
			}

			WrapperMethods.isDisplayed(HomePage.getIagree(), "I Agree Symbol is displayed",
					"I Agree Symbol is not displayed");
			WrapperMethods.contains_Text(HomePage.getIagree(), "I agree");
		}

	}

	public void editionPicker(String browser, String machineType, String edition) {

		WrapperMethods.click(HomePage.editionExpand(), "Edition Button is clicked", "Edition Button is not clicked");

		WrapperMethods.assertIsTrue(WrapperMethods.getAttribute(HomePage.usedition(), "id").isEmpty(),
				"Id attribute/Value is not present for US radio button in Header Edition picker",
				"Id attribute/Value is present for US radio button in Header Edition picker");
		WrapperMethods.assertIsTrue(WrapperMethods.getAttribute(HomePage.intedition(), "id").isEmpty(),
				"Id attribute/Value is not present for International radio button in Header Edition picker",
				"Id attribute/Value is present for International radio button in Header Edition picker");

	}

	public void splpagesection(String browser, String machineType, String edition) {
		BaseTestMethods.preSteps();
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentsection(), 30, "Section name is visible",
				"Section name is not visible");
		WrapperMethods.contains_Text(SectionFrontsPage.getCurrentsection(), "Features");
		if (!machineType.equalsIgnoreCase("Desktop")) {

			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
			WrapperMethods.clickJavaScript(SectionFrontsPage.getCurrentsectionDropdown(),
					"sub section is dropdown is clicked", "sub section is dropdown is not clicked");
			WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentSubsection(), 20, "Sub Section is visible",
					"Sub Section is not visible");
			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
		}

		// WrapperMethods.isDisplayed(SectionFrontsPage.getSectionContent(),
		// "Page Content is present", "Page Content is not present");
		WrapperMethods.isElementPresentOrNot(SectionFrontsPage.getSectionContent());
	}

	public void testFooters(String browser, String machineType, String edition) {

		if (edition.equalsIgnoreCase("DOM")) {
			if (SectionFrontsPage.getAllFooterLinks().size() != 80) {
				UMReporter.log(LogStatus.FAIL, "Correct number of footer links not on page!! (Expected 80 links, got "
						+ SectionFrontsPage.getAllFooterLinks().size());
			} else {
				UMReporter.log(LogStatus.PASS, "Verified the Footer links are displayed");
			}
		} else if (edition.equalsIgnoreCase("INTL")) {
			if (SectionFrontsPage.getAllFooterLinks().size() != 83) {
				UMReporter.log(LogStatus.FAIL, "Correct number of footer links not on page!! (Expected 83 links, got "
						+ SectionFrontsPage.getAllFooterLinks().size());
			} else {
				UMReporter.log(LogStatus.PASS, "Verified the Footer links are displayed");
			}
		}

		if (!browser.equalsIgnoreCase("SAFARI") && machineType.equalsIgnoreCase("Desktop")) {
			WrapperMethods.scrollIntoViewByElement(HomePage.getFooterSearch());
			WrapperMethods.verifyElement(HomePage.getFooterSearch(), "Search Button in Footer ");
			WrapperMethods.moveToElementClick(HomePage.getFooterSearch());
			WrapperMethods.sendkeysWE(HomePage.getFooterSearch(), Keys.BACK_SPACE,
					"Giving Backspace- we clearing contents in search box",
					"BackSpace function is not working in search box");
			WrapperMethods.sendkeys(HomePage.getFooterSearch(), "Shows", "Entered text in search box",
					"Text is not entering in search box");
			WrapperMethods.sendkeysWE(HomePage.getFooterSearch(), Keys.ENTER, "Pressed Enter through the Key ",
					"Pressed Enter in search box  is not working ");
		}

		WrapperMethods.waitForVisiblity(HomePage.getcnnSearchSummary(), "Displaying Results Header is displayed",
				"Displaying Results Header is not displayed");

		WrapperMethods.contains_Text(HomePage.getcnnSearchSummary(), "Shows",
				"Entered text is displayed in Result Header", "Entered text is not displayed in Result Header");
	}

	public void testFootersOther(String browser, String machineType, String edition, boolean weather) {

		if (weather == true) {
			WrapperMethods.isDisplayed(SectionFrontsPage.getFooterWeatherValue(), "Footer Weather value is present",
					"Footer Weather value is not present");
			WrapperMethods.isDisplayed(SectionFrontsPage.getFooterWeathericon(), "Footer Weather Icon is present",
					"Footer Weather Icon is not present");
			WrapperMethods.isDisplayed(SectionFrontsPage.getFooterWeatherLoc(), "Footer Weather Loc is present",
					"Footer Weather Loc is not present");
			WrapperMethods.waitElemToBeClickable(SectionFrontsPage.getFooterWeather(), 20,
					"Footer Weather Elements is Clickable", "Footer Weather Elements is not Clickable");
		} else {
			UMReporter.log(LogStatus.FAIL, "The Weather card is disabled in the JSON");
		}

		// WrapperMethods.isDisplayed(SectionFrontsPage.getFooterCopyright(),"Copyright
		// info is present","Copyright info is not present");
		// WrapperMethods.isDisplayed(SectionFrontsPage.getFooterCopyrightLink());

		WrapperMethods.isElementPresentOrNot(SectionFrontsPage.getSectionContent(), "Copyright info is present",
				"Copyright info is not present");

		WrapperMethods.textNotEmpty(SectionFrontsPage.getFooterCopyright(), "Copyright Text info is present",
				"Copyright Text info is not present");

		if (machineType.equalsIgnoreCase("desktop")) {
			WrapperMethods.assertHasAttribute(WrapperMethods.getWebElement(SectionFrontsPage.getFooterCopyrightLink()),
					"href", "http://www.turner.com/", "Copyright link is correct", "Copyright link is not correct");
			WrapperMethods.contains_Text(SectionFrontsPage.getFooterCopyright(), "CNN Sans � & ");
		}

		String[] domData = null;

		if (edition.equalsIgnoreCase("DOM"))
			domData = ConfigProp.getPropertyValue("global_footer_us").split(",");
		else if (edition.equalsIgnoreCase("INTL"))
			domData = ConfigProp.getPropertyValue("global_footer_intl").split(",");

		int i = 0;

		for (String temp : domData) {

			String[] footItems = temp.split("~");

			Reporter.log("Element under test: " + footItems[0]);

			WrapperMethods.contains_Text(HomePage.getFooterLinks().get(i), footItems[0], "Link has the correct text",
					"Link has wrong text");

			WrapperMethods.assertHasAttribute(HomePage.getFooterLinks().get(i), "href", footItems[1],
					"Link href is correct", "Link href is wrong");

			i++;
		}
	}

	public void testintsecfrontpage(String browser, String machineType, String edition, String currentURL) {
		System.out.println("1");
		BaseTestMethods.preSteps();
		System.out.println("2");
		WrapperMethods.validateJSErrors();
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentsection(), 20, "Current Section is visible",
				"Current Section is not visible");
		currentURL = WrapperMethods.correctUrl(currentURL);

		if (currentURL.equals("/Middle-East")) {
			currentURL = "Middle East";
		}
		if (currentURL.equals("/US")) {
			currentURL = "U.S.";
		}
		int width = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
		System.out.println("one " + ConfigProvider.getConfig("Res_Width"));
		System.out.println("two " + width);
		if (width < 1120) {
			WrapperMethods.click(SectionFrontsPage.getCurrentsection(), "Current Section Element");
		}
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentsectionSelect(), 20,
				"Current Selected Section is visible", "Current Selected Section is not visible");
		WrapperMethods.contains_Text(SectionFrontsPage.getCurrentsectionSelect(), currentURL.replace("/", ""));
		WrapperMethods.contains_Text(SectionFrontsPage.getCurrentsectionSelect(), currentURL.replace("/", ""));
		System.out.println("DONEEEEEEEEEEEEEEEEEEEEEEE");
	}

	public void sportpagesection(String config, String name, String config2) {

		BaseTestMethods.preSteps();
		WrapperMethods.validateJSErrors();
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentsection(), 20, "Current Section is visible",
				"Current Section is not visible");
		WrapperMethods.contains_Text(SectionFrontsPage.getCurrentsection(), "Sport");

		int width = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
		if (width < 1120) {

			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
			WrapperMethods.clickJavaScript(SectionFrontsPage.getCurrentsectionDropdown(), "Clicked drop down ",
					"Not able to click drop down");
			WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentSubsection(), 20, "Current sub Section is visible",
					"Current sub Section is not visible");
			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
		}

		WrapperMethods.isElementPresentOrNot(SectionFrontsPage.getSectionContent());// ,
																					// "Page
																					// Content
																					// is
																					// present",
																					// "Page
																					// Content
																					// is
																					// not
																					// present");
	}

	public void travelpagesection(String config, String name, String config2) {

		BaseTestMethods.preSteps();
		WrapperMethods.validateJSErrors();
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentsection(), 20, "Current Section is visible",
				"Current Section is not visible");

		WrapperMethods.contains_Text(SectionFrontsPage.getCurrentsection(), "Travel");
		int width = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
		if (width < 1120) {
			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
			WrapperMethods.clickJavaScript(SectionFrontsPage.getCurrentsectionDropdown());
			WrapperMethods.Explicit_Wait(SectionFrontsPage.getCurrentSubsection(), 20, "Current Sub Section is visible",
					"Current Sub Section is not visible");
			WrapperMethods.isDisplayed(SectionFrontsPage.getCurrentsectionDropdown(), "DropDown Icon is present",
					"DropDown Icon is notpresent");
		}
		WrapperMethods.isElementPresentOrNot(SectionFrontsPage.getSectionContent());
		// WrapperMethods.isDisplayed(SectionFrontsPage.getSectionContent(),"Page
		// Content is present", "Page Content is not present");
	}

	public void techpagesection(String config, String name, String config2) {

		WrapperMethods.Explicit_Wait(SectionFrontsPage.getTechCurrentsection(), 30, "Subsection of Tech is displayed.",
				"Subsection of Tech is not displayed.");
		WrapperMethods.Explicit_Wait(SectionFrontsPage.getTechCurrentsection(), 30,
				WrapperMethods.getWebElement(SectionFrontsPage.getTechCurrentsection()).getText()
						+ " - Subsection Text of Tech is displayed",
				WrapperMethods.getWebElement(SectionFrontsPage.getTechCurrentsection()).getText()
						+ " - Subsection Text of Tech is not displayed");

	}

	public static void headerLiveTv() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		driver.navigate().refresh();
		if (System.getProperty("edition").equals("DOM")) {

			try {
				WrapperMethods.waitForVisiblity(HomePage.liveTv(), 25, "Live TV is loaded",
						"Live TV is not loaded in the specified time");

			} catch (Exception e) {
				UMReporter.log(LogStatus.INFO,
						"INFO : Element is not visible even after waiting for 25 seconds--Please check");

			}
			WrapperMethods.verifyElement(HomePage.liveTv(), "Live Tv");
			WrapperMethods.contains_Text_Attribute(HomePage.liveTv(), "href", "http://cnn.it/go2",
					"Live TV has correct URL", "Live TV has incorrect URL");

		} else {

			try {
				WrapperMethods.waitForVisiblity(HomePage.liveTv(), 25, "Live TV is loaded",
						"Live TV is not loaded in the specified time");
			} catch (Exception e) {
				UMReporter.log(LogStatus.INFO,
						"INFO : Element is not visible even after waiting for 25 seconds--Please check");
			}

			WrapperMethods.verifyElement(HomePage.liveTv(), "Live Tv");

			WrapperMethods.contains_Text_Attribute(HomePage.liveTv(), "href", "http://cnn.it/go2",
					"Live TV has correct URL", "Live TV has incorrect URL");
		}
	}

	public static void headersearchElem() {

		WrapperMethods.verifyElement(HomePage.getHeaderSearch(), "Search Button");

		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
			WrapperMethods.click(HomePage.getHeaderSearch(), "Search Button");
			HomePage.wait(2);
		}

		WrapperMethods.verifyElement(HomePage.getSearchField(), "Search Field");
		WrapperMethods.contains_Text_Attribute(HomePage.getSearchField(), "placeholder", "Search CNN...",
				"Search box has correct text", "Search box has incorrect text");

		if (ConfigProvider.getConfig("Platform").equals("Desktop")) {

			WrapperMethods.contains_Text(HomePage.getSearchButton(), "Search �", "Button text is correct",
					"Button text is wrong");

			WrapperMethods.verifyElement(HomePage.getSearchButton(), "Search Button");

		} else
			WrapperMethods.verifyElement(HomePage.getHeaderSearch(), "Search Button");

	}

	public static void navMenuClick() {
		HomePage.wait(10);
		//MethodDef.cnnConsent(BasePage.getTermsConsentClose());
		MethodDef.explicitWaitClickable(HomePage.getNavMenu(), "Nav Menu is Clickable", "Nav Menu is not Clickable");
		WrapperMethods.clickJavaScript(HomePage.getNavMenu());
		HomePage.wait(2);
	}

	public static Boolean chkDeviceHdr() {
		// Boolean isDev = MethodDef.chkDeviceHdr(windowWidth);

		if (!ConfigProvider.getConfig("Platform").equals("Desktop")) {
			navMenuClick();
			return true;
		} else
			return false;
	}

	public static void headerFlyoutfooterMenu() {

		UMReporter.log(LogStatus.INFO, "<b><i><em>HOME PAGE Header Flyout Footer Menu Validations:</b></i></em>");
		Boolean dev = chkDeviceHdr();
		HomePage.wait(2);

		if (!dev) {
			navMenuClick();
		}

		try {
			WrapperMethods.verifyElement(HomePage.getFlyoutFooterMenu(), "Header Flyout Footer Menu");

			if (!dev) {
				WrapperMethods.verifyElement(HomePage.getFlyoutFooterEditionMenu(),
						"Header Flyout Footer Edition Menu");

				WrapperMethods.verifyElement(HomePage.getFlyoutEditionPreference(), "Footer Flyout Edition Preference");

			}
			WrapperMethods.verifyElement(HomePage.getFlyoutFooterSocialIcons(), "Footer Social Icons");
			WrapperMethods.contains_Text_Attribute(HomePage.getFlyoutFooterFacebook(), "href", "facebook",
					"Facebook link is correct and the link: "
							+ WrapperMethods.getWebElement(HomePage.getFlyoutFooterFacebook()).getAttribute("href"),
					"Wrong href for facebook");

			WrapperMethods.contains_Text_Attribute(HomePage.getFlyoutFooterTwitter(), "href", "twitter",
					"Twitter link is correct and the link: "
							+ WrapperMethods.getWebElement(HomePage.getFlyoutFooterTwitter()).getAttribute("href"),
					"Wrong href for Twitter");

			WrapperMethods.contains_Text_Attribute(HomePage.getFlyoutFooterInstagram(), "href", "instagram",
					"Instagram link is correct and the link: "
							+ WrapperMethods.getWebElement(HomePage.getFlyoutFooterInstagram()).getAttribute("href"),
					"Wrong href for Instagram");

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR,
					"<BR> Error in accessing Header Flyout Footer Menu elements:  " + E.getMessage());

		}
		try {
			if (!dev)
				WrapperMethods.verifyElement(HomePage.getFlyoutFooterEditionMenu(),
						"In Header Flyout Footer Edition Menu");

			for (WebElement elem : HomePage.getFooterEditionMenuLinks()) {

				WrapperMethods.assertIsTrue(!elem.getAttribute("data-value").isEmpty(),
						"the value is displayed and the value is: " + elem.getAttribute("data-value"),
						"the value is not displayed");

			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.ERROR,
					"<BR> Error in accessing Header Flyout Footer Edition Menu elements:  " + E.getMessage());

		}
	}

	public static void footerEditionPicker() {

		By editionPick, setEditionHdr, setEditionHdrButton;
		List<WebElement> editionPickOther, setEditionHdrLabels, setEditionHdrRadioButton;

		editionPick = HomePage.getFooterCurrentEdition();

		WrapperMethods.verifyElement(editionPick, "Edition Picker");

		WrapperMethods.clickJavaScript(editionPick);

		HomePage.wait(2);

		editionPick = HomePage.getFooterCurrentEdition();
		editionPickOther = HomePage.getFooterOtherEditionHeaderDev();
		setEditionHdr = HomePage.getFooterSetEdition();
		setEditionHdrLabels = HomePage.getFooterSetEditionHdrLabels();
		setEditionHdrRadioButton = HomePage.getFooterSetEditionHdrRadio();
		setEditionHdrButton = HomePage.getFooterSetEditionHdrButton();

		int i = 0;
		List<WebElement> pick = editionPickOther;
		List<WebElement> usintl = HomePage.getFooterEditionUsIntl();
		for (WebElement pickItem : pick) {
			switch (i) {
			case 0:
				if (System.getProperty("edition").equals("INTL")) {

					WrapperMethods.contains_Text(usintl.get(i), "U.S.", "Picker is correct", "Picker is not correct");

					WrapperMethods.assertIsTrue(
							pickItem.getAttribute("data-value").endsWith("cnn.com")
									&& !pickItem.getAttribute("data-value").contains("edition"),
							"Attribute HREF is correct", "Attribute HREF is not correct");

				}
				i++;
				break;
			case 1:
				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.contains_Text(usintl.get(i), "International", "International Picker is correct",
							"International Picker is not correct");

					/*
					 * error.assertTrue(pickItem.getAttribute("data-value").
					 * endsWith("cnn.com") &&
					 * pickItem.getAttribute("data-value").contains("edition"));
					 */
				}
				i++;
				break;
			case 2:
				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.verifyElement(HomePage.getInter(), "International");

					if (System.getProperty("environment").equals("PROD")) {
						WrapperMethods.contains_Text_Attribute(HomePage.getInter(), "data-value",
								"http://edition.cnn.com", "INTL Picker URL is correct", "INTL Picker URL is incorrect");

					} else {
						String envr = System.getProperty("environment").toLowerCase();
						WrapperMethods.contains_Text_Attribute(HomePage.getInter(), "data-value",
								"http://edition." + envr + ".next.cnn.com", "INTL Picker URL is correct",
								"INTL Picker URL is incorrect");

					}
				} else {
					WrapperMethods.verifyElement(HomePage.getDom(), "US");

					if (System.getProperty("environment").equals("PROD")) {
						WrapperMethods.contains_Text_Attribute(HomePage.getDom(), "data-value", "http://us.cnn.com",
								"US Picker URL is correct", "US Picker URL is incorrect");

					} else {
						String envr = System.getProperty("environment").toLowerCase();
						WrapperMethods.contains_Text_Attribute(HomePage.getDom(), "data-value",
								"http://" + envr + ".next.cnn.com", "US Picker URL is correct",
								"US Picker URL is incorrect");

					}
				}
				i++;
				break;

			/**
			 * Since the 'International' is not showing in the webpage but 'li'
			 * tag is visible in DOM, we using below Case 3: for move to the
			 * next LI tag through i++
			 */
			case 3:
				i++;
				break;
			case 4:
				WrapperMethods.contains_Text(pickItem, "Arabic", "Arabic Picker is correct",
						"Arabic Picker is not correct");
				WrapperMethods.contains_Text_Attribute(pickItem, "data-value", "http://arabic.cnn.com",
						"Arabic Picker URL is correct", "Arabic Picker URL is incorrect");

				i++;
				break;

			case 5:
				WrapperMethods.contains_Text(pickItem, "Espa�ol", "Espa�ol Picker is correct",
						"Espa�ol Picker is not correct");

				WrapperMethods.contains_Text_Attribute(pickItem, "data-value", "http://cnnespanol.cnn.com/",
						"Espa�ol Picker URL is correct", "Espa�ol Picker URL is incorrect");

				i++;
				break;
			}
		}
		UMReporter.log(LogStatus.INFO, "<BR>INFO: TESTING OF SET EDITION PREFERENCES");
		WrapperMethods.verifyElement(setEditionHdr, "Set Default Edition pref option");

		// WrapperDef.click(setEditionHdr);
		WrapperMethods.clickJavaScript(setEditionHdr);
		UMReporter.log(LogStatus.INFO, "<BR>INFO: Clicked SET EDITION ");

		i = 0;
		for (WebElement button : setEditionHdrRadioButton) {
			/*
			 * WrapperDef.assertHasAttribute(error, button, "type", "radio",
			 * "Radio Button is present", "Radio Button is not present");
			 */
			switch (i) {
			case 0:

				WrapperMethods.assertIsTrue(
						button.getAttribute("data-value").endsWith("cnn.com")
								&& !button.getAttribute("data-value").contains("edition"),
						"URL is correct in Edition preference button",
						"URL is not correct in Edition preference button");

				i++;
				break;
			case 1:
				WrapperMethods.assertIsTrue(
						button.getAttribute("data-value").endsWith("cnn.com")
								&& button.getAttribute("data-value").contains("edition"),
						"URL is correct in Edition preference button",
						"URL is not correct in Edition preference button");

				i++;
				break;
			}
		}
		HomePage.wait(5);
		WrapperMethods.verifyElement(setEditionHdrButton, "Set Button");

	}

	public static void headerEditionPicker() {

		Boolean chkdev = chkDeviceHdr();
		By editionPick, setEditionHdr, setEditionHdrTxtDev, setEditionHdrButton;
		List<WebElement> editionPickOther, setEditionHdrLabels, setEditionHdrRadioButton;

		if (chkdev) {
			editionPick = HomePage.getCurrentEditionDev();
		} else
			editionPick = HomePage.getCurrentEdition();

		MethodDef.explicitWaitVisibility(editionPick, 20, "Edition Picker is loaded", "Edition Picker is not loaded");
		WrapperMethods.verifyElement(editionPick, "Edition Picker");
		WrapperMethods.clickJavaScript(editionPick);
		HomePage.wait(5);

		if (chkdev) {
			// editionPick = home.getCurrentEditionDev();
			editionPickOther = HomePage.getOtherEditionHeaderDev();
			setEditionHdr = HomePage.getSetEditionHdrDev();
			setEditionHdrTxtDev = HomePage.getSetEditionHdrTxtDev();
			setEditionHdrLabels = HomePage.getSetEditionHdrLabelsDev();
			setEditionHdrRadioButton = HomePage.getSetEditionHdrRadioButtonDev();
			setEditionHdrButton = HomePage.getSetEditionHdrButtonDev();
		} else {
			editionPick = HomePage.getCurrentEdition();
			editionPickOther = HomePage.getOtherEditionHeader();
			setEditionHdr = HomePage.getSetEditionHdr();
			setEditionHdrTxtDev = HomePage.getSetEditionHdrTxt();
			setEditionHdrLabels = HomePage.getSetEditionHdrLabels();
			setEditionHdrRadioButton = HomePage.getSetEditionHdrRadioButton();
			setEditionHdrButton = HomePage.getSetEditionHdrButton();
		}

		int i = 0;
		List<WebElement> pick = editionPickOther;

		for (WebElement pickItem : pick) {
			switch (i) {
			case 0:
				if (System.getProperty("edition").equals("INTL")) {
					WrapperMethods.contains_Text(pickItem, "U.S.", "Picker is correct", "Picker is not correct");
					WrapperMethods.assertIsTrue(
							pickItem.getAttribute("data-value").endsWith("cnn.com")
									&& !pickItem.getAttribute("data-value").contains("edition"),
							"Attribute HREF is correct", "Attribute HREF is not correct");
				}
				i++;
				break;
			case 1:
				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.contains_Text(pickItem, "International", "International Picker is correct",
							"International Picker is not correct");
					/*
					 * error.assertTrue(pickItem.getAttribute("data-value").
					 * endsWith("cnn.com") &&
					 * pickItem.getAttribute("data-value").contains("edition"));
					 */
				}
				i++;
				break;
			case 2:
				WrapperMethods.contains_Text(pickItem, "Arabic", "Arabic Picker is correct",
						"Arabic Picker is not correct");
				WrapperMethods.contains_Text_Attribute(pickItem, "data-value", "http://arabic.cnn.com",
						"Arabic Picker URL is correct", "Arabic Picker URL is incorrect");
				i++;
				break;
			case 3:
				WrapperMethods.contains_Text(pickItem, "Espa�ol", "Espa�ol Picker is correct",
						"Espa�ol Picker is not correct");
				WrapperMethods.contains_Text_Attribute(pickItem, "data-value", "http://cnnespanol.cnn.com/",
						"Espa�ol Picker URL is correct", "Espa�ol Picker URL is incorrect");
				i++;
				break;
			}
		}

		UMReporter.log(LogStatus.INFO, "<BR>INFO: TESTING OF SET EDITION PREFERENCES");
		WrapperMethods.verifyElement(setEditionHdr, "Set Default Edition pref option");
		WrapperMethods.contains_Text(setEditionHdrTxtDev, "Set edition preference:", "Text is correct",
				"Text is not Correct");

		/*
		 * i = 0; for (WebElement label : setEditionHdrLabels) { switch (i) {
		 * case 0: WrapperDef.assertHasContent(error, label, "U.S.:",
		 * "Picker 1 Text is correct", "Picker 1 Text is not Correct"); i++;
		 * break; case 1: WrapperDef.assertHasContent(error, label,
		 * "International:", "Picker 2 Text is correct",
		 * "Picker 2 Text is not Correct"); i++; break; } }
		 */

		i = 0;
		for (WebElement button : setEditionHdrRadioButton) {
			/*
			 * WrapperDef.assertHasAttribute(error, button, "type", "radio",
			 * "Radio Button is present", "Radio Button is not present");
			 */
			switch (i) {
			case 0:
				WrapperMethods.assertIsTrue(
						button.getAttribute("data-value").endsWith("cnn.com")
								&& !button.getAttribute("data-value").contains("edition"),
						"URL is correct in Edition preference button",
						"URL is not correct in Edition preference button");
				i++;
				break;
			case 1:
				WrapperMethods.assertIsTrue(
						button.getAttribute("data-value").endsWith("cnn.com")
								&& button.getAttribute("data-value").contains("edition"),
						"URL is correct in Edition preference button",
						"URL is not correct in Edition preference button");
				i++;
				break;
			}
		}
		WrapperMethods.verifyElement(setEditionHdrButton, "Set Button");

	}

	public static void validateWeatherCard() {
		MethodDef.explicitWaitVisibility(HomePage.getWeatherCardHM(), "Weather card element is present",
				"Weather card element is not present");

		WrapperMethods.isDisplayed(HomePage.getWeatherCardLocationHM(), "Location Info is present",
				"Location Info is not present");
		WrapperMethods.isDisplayed(HomePage.getWeatherCardTemperatureHM(), "Temperature is present",
				"Temperature is not present");
		WrapperMethods.isDisplayed(HomePage.getWeatherCardIconHM(), "Temperature icon is present",
				"Temperature icon is not present");

	}

	public static void testSearchSummaryDesktop() {
		WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 60);
		wait.until(ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.getSearchSummary())));
		WrapperMethods.isDisplayed(HomePage.getSearchSummary(), "Search results summary displayed successfully",
				"Search results summary not displayed successfully");

		WrapperMethods.isDisplayed(HomePage.getSearchInActPreviousLink(), "Inactive<b> Previous </b>link is displayed",
				"Inactive<b> Previous </b>link is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchPageDigits(), "Page Digits (1 2 3 4 5) displayed successfully",
				"Page Digits (1 2 3 4 5) not displayed successfully");
		WrapperMethods.isDisplayed(HomePage.getSearchActPageDigit(),
				"Page Digit " + WrapperMethods.getWebElement(HomePage.getSearchActPageDigit()).getText() + " is Active",
				"Page Digit " + WrapperMethods.getWebElement(HomePage.getSearchActPageDigit()).getText()
						+ " is not Active");
		wait.until(ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.getSearchActNextLink())));
		WrapperMethods.isDisplayed(HomePage.getSearchActNextLink(), "Active <b>Next></b> link is displayed",
				"Active <b>Next></b> link is not displayed");
		HomePage.wait(5);
		WrapperMethods.clickJavaScript(HomePage.getSearchActNextLink());
		WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
		HomePage.wait(5);
		wait.until(ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.getSearchActPageDigit())));
		WrapperMethods.isDisplayed(HomePage.getSearchActPageDigit(),
				"Page Digit " + WrapperMethods.getWebElement(HomePage.getSearchActPageDigit()).getText() + " is Active",
				"Page Digit " + WrapperMethods.getWebElement(HomePage.getSearchActPageDigit()).getText()
						+ " is not Active");
		WrapperMethods.isDisplayed(HomePage.getSearchActPreviousLink(), "<b>Previous</b> link become Active",
				"<b>Previous</b> link is not become Active");
		WrapperMethods.isDisplayed(HomePage.getSearchActNextLink(), "Active <b>Next></b> link is displayed",
				"Active <b>Next></b> link is not displayed");
	}

	public static void testSearchResults() {
		WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 65);
		WrapperMethods.isDisplayed(HomePage.getSearchEverything(), "<b>Everything</b> Checkbox is displayed",
				"<b>Everything</b> Checkbox is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchStories(), "<b>Stories</b> Checkbox is displayed",
				"<b>Stories</b> Checkbox is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchVideos(), "<b>Videos</b> Checkbox is displayed",
				"<b>Videos</b> Checkbox is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchPhotos(), "<b>Photos</b> Checkbox is displayed",
				"<b>Photos</b> Checkbox is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchInteractives(), "<b>Interactives</b> Checkbox is displayed",
				"<b>Interactives</b> Checkbox is not displayed");
		WrapperMethods.isDisplayed(HomePage.getSearchiReport(), "<b>iRepor</b> Checkbox is displayed",
				"<b>iRepor</b> Checkbox is not displayed");

		UMReporter.log(LogStatus.INFO, "Search CNN: Click on Video's Check box");
		WrapperMethods.scrollUp();
		WrapperMethods.click(HomePage.getSearchVideos(), "SearchVideos");
		WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
		HomePage.wait(3);

		WrapperMethods.isDisplayed(HomePage.getSearchAllVideos(), "Only <b>Video</b> results are displayed",
				"Along with <b>Video</b> other results are also displayed");
		WrapperMethods.scrollUp();
		HomePage.wait(3);
		WrapperMethods.click(HomePage.getSearchEverything(), "SearchEverything");
		WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
		HomePage.wait(5);
		UMReporter.log(LogStatus.INFO, "Search CNN: Click on Photos Check box");
		wait.until(ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.getSearchPhotos())));
		try {
			WrapperMethods.click(HomePage.getSearchPhotos(), "SearchPhotos");
		} catch (Exception e) {
			WrapperMethods.clickJavaScript(HomePage.getSearchPhotos(), "SearchPhotos clicked",
					"SearchPhotos not clicked");
		}
		WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());

		WrapperMethods.isDisplayed(HomePage.getSearchSort(), "<b>Sort by</b> section is displayed",
				"<b>Sort by</b> section is not displayed");

		Reporter.log("<br><b>Search CNN: Click on Sort by Date</b>");

		WrapperMethods.click(HomePage.getSearchSort(), "Search Sort");
		WrapperMethods.click(HomePage.getSearchSortDate(), "Search Sort date");
		HomePage.wait(5);
		wait.until(ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.getSearchAllOthers())));
		WrapperMethods.isDisplayed(HomePage.getSearchAllOthers(), "<b>Search</b> results are sorted by date",
				"<b>Search</b> results are not sorted by date");
	}

	public static void testEmVinePlayer() throws IOException {
		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mob")) {

			WrapperMethods.switchToFrame(HomePage.VineiFrame());

			WrapperMethods.isDisplayed(HomePage.VineLogo(), "The Vine Logo is present as expected",
					"The Vine Logo is not present - UnExpected");

			WrapperMethods.isDisplayed(HomePage.VineVolumneIcon(), "The Vine Volume Icon is present as expected",
					"The Vine Volume Icon is not present - UnExpected");

			MethodDef.brokenLinkValidation(
					WrapperMethods.getWebElement(HomePage.VineDescriptionhref()).getAttribute("href"));

			WrapperMethods.moveToElement(HomePage.VineVolumneIcon());

			WrapperMethods.isDisplayed(HomePage.VineDescription(), "The Vine Description is present as expected",
					"The Vine Description is not present - UnExpected");

			WrapperMethods.isDisplayed(HomePage.Vineloopcount(), "The Vine Loop count is present as expected",
					"The Vine Loop Count is not present - UnExpected");
		}
	}

	public static void testEmVimeoPlayer() throws IOException {
		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mob")) {

			WrapperMethods.switchToFrame(HomePage.VimeoiFrame());

			WrapperMethods.assertIsTrue(HomePage.VimeoPlayer().size() != 0,
					"The Vimeo Play is present in the Page as expected",
					"The Vimeo Play is is not present in the Page - Unexpected");

			WrapperMethods.assertIsTrue(HomePage.Vimeoimg().size() != 0, "The Vimeo Play image is present as expected ",
					"The Vimeo Play image is not present - Unexpected");

			WrapperMethods.assertIsTrue(!HomePage.Vimeoimg().get(0).getAttribute("src").isEmpty(),
					"The Vimeo Play image src is present as expected ",
					"The Vimeo Play image src is not present - Unexpected");

			WrapperMethods.assertIsTrue(HomePage.Vimeoheaders().size() != 0,
					"The Vimeo Play header is present as expected",
					"The Vimeo Play header is not present - Unexpected");

			Reporter.log("<br>Vimeo Header href validation");

			MethodDef.brokenLinkValidation(HomePage.Vimeoheaders().get(0).getAttribute("href"));

			WrapperMethods.assertIsTrue(HomePage.VimeoSubTitle().size() != 0,
					"The Vimeo sub-title is present in the Page as expected",
					"The Vimeo sub-title is not present in the Page - Unexpected");

			Reporter.log("<br>Vimeo subtitle href Validation");

			MethodDef.brokenLinkValidation(HomePage.VimeoSubTitle().get(0).getAttribute("href"));

			WrapperMethods.assertIsTrue(HomePage.VimeoLikeButton().size() != 0,
					"The vimeo like button is present in the Page as expected",
					"The vimeo like button is not present in the Page - Unexpected");

			WrapperMethods.assertIsTrue(HomePage.VimeoWatchlaterButton().size() != 0,
					"The vimeo watch later button is present in the Page as expected",
					"The vimeo watch later button is not present in the Page - Unexpected");

			WrapperMethods.assertIsTrue(HomePage.VimeoShareButton().size() != 0,
					"The vimeo share button is present in the Page as expected",
					"The vimeo share button is not present in the Page - Unexpected");

			WrapperMethods.assertIsTrue(HomePage.VimeoLogo().size() != 0,
					"The vimeo Logo is present in the Page as expected",
					"The vimeo Logo is not present in the Page - Unexpected");
		}
	}

	public static void CNNFeedbackGovalidation() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WrapperMethods.verifyElement(HomePage.EmailLabel(), "Email Label");
		WrapperMethods.verifyElement(HomePage.EmailTextBox(), "Email TextBox");
		WrapperMethods.verifyElement(HomePage.PlatformLabel(), "Platform Label");
		WrapperMethods.verifyElement(HomePage.WebLabel(), "Web Label");
		WrapperMethods.verifyElement(HomePage.WebRadioBtn(), "Web Radio button");
		WrapperMethods.verifyElement(HomePage.iPadLabel(), "iPad Label");
		WrapperMethods.verifyElement(HomePage.iPadRadioBtn(), "iPad Radio button");
		WrapperMethods.verifyElement(HomePage.DefectiveLabel(), "Defective Label");
		WrapperMethods.verifyElement(HomePage.ExcellentLabel(), "Excellent Label");

		List<String> items = new ArrayList<>();
		items.add("Content");
		items.add("Design");
		items.add("Ease of Use");
		items.add("Overall");

		for (String item : items) {
			WrapperMethods.verifyElement(HomePage.ItemsLabel(item), item);
			for (int i = 0; i < HomePage.LabelItems(item).size(); i++) {
				WrapperMethods.verifyElement(HomePage.LabelRadioBtns(item).get(i), item + "Radio button");
				WrapperMethods.verifyElement(HomePage.LabelItems(item).get(i),
						item + " Label: " + HomePage.LabelItems(item).get(i).getText());
			}
		}

		WrapperMethods.verifyElement(HomePage.AdditionalCmts(), "Additional Comments Label");
		WrapperMethods.verifyElement(HomePage.AdditionalCmtsTxtBox(), "Additional Comments TextBox");
		WrapperMethods.verifyElement(HomePage.SendFeedback(), "'Send Feedback' buttton");
		WrapperMethods.click(HomePage.SendFeedback(), "'Send Feedback' buttton");
		ErrorMsgValidate();
		WrapperMethods.enter_Text(HomePage.EmailTextBox(), "@gmail.com");
		WrapperMethods.click(HomePage.SendFeedback(), "'Send Feedback' buttton");
		ErrorMsgValidate();
		WrapperMethods.enter_Text(HomePage.EmailTextBox(), "testing@gmail.com");
		WrapperMethods.clickJavaScript(HomePage.WebRadioBtn());
		UMReporter.log(LogStatus.INFO, "Web Radio button is clicked");
		for (String item : items) {
			WrapperMethods.clickJavaScript(HomePage.LabelRadioBtns(item).get(1));
			UMReporter.log(LogStatus.INFO, item + " Radio button is clicked");
		}
		WrapperMethods.enter_Text(HomePage.AdditionalCmtsTxtBox(), "Testing");
		UMReporter.log(LogStatus.INFO, "Text entered in Addtional Comments Textbox");
		try {
			Actions actions = new Actions(driver);
			WebElement SendFeedback = driver.findElement(HomePage.SendFeedback());
			actions.doubleClick(SendFeedback);
			MethodDef.moveAndClick(ConfigProvider.getConfig("Browser"), HomePage.SendFeedback(),
					"Send Feedback button");
			// WrapperDef.clickJavaScript(home.SendFeedback(), go);
		} catch (Exception e) {
			WrapperMethods.clickJavaScript(HomePage.SendFeedback());
		}
		UMReporter.log(LogStatus.INFO, "Send Feedback button is clicked");
		WrapperMethods.scrollUp();
		try {
			WrapperMethods.waitForVisiblity(HomePage.SuccMsg(), "Waiting untill Success message appears",
					"Success message not found");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "The Success message is not displayed unexpected - please check");

		}
		WrapperMethods.verifyElement(HomePage.SuccMsg(), "Successful message");
	}

	public static void ErrorMsgValidate() {
		for (int i = 0; i < HomePage.ErrorMsg().size(); i++) {
			if (i == (HomePage.ErrorMsg().size() - 1)) {
				break;
			} else {
				WrapperMethods.assertIsTrue(HomePage.ErrorMsg().get(i).getText().contains("is required"),
						"Error message is displayed " + HomePage.ErrorMsg().get(i).getText(),
						"Error message is not displayed");
			}
		}
	}

	/////////

	public static void validateWeatherCardFn(String weatherinput, weatherType Type, String valstring, Boolean toggle) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		MethodDef.explicitWaitVisibility(HomePage.getWeatherCard(), "Weather card is visible in the page",
				"Weather card is not visible in the page");
		WrapperMethods.verifyElement(HomePage.getWeatherCardSettings(), "Temperature settings");
		// WrapperDef.moveToElement(home.getWeatherCard(), go);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-100)", "");
		WrapperMethods.clickJavaScript(HomePage.getWeatherCardSettings());
		WrapperMethods.verifyElement(HomePage.getWeatherCardSetLocation(), "Location input");
		WrapperMethods.contains_Text(HomePage.getWeatherCardSetFar(), "�F", "Farenheit Element is present",
				"Farenheit Element is not present");
		WrapperMethods.contains_Text(HomePage.getWeatherCardSetCel(), "�C", "Celcius Element is present",
				"Celcius Element is not present");
		/*
		 * WrapperMethods.contains_Text(HomePage.getWeatherCardSet(),
		 * "Weather Set button is present",
		 * "Weather Set button is not present");
		 */ WrapperMethods.sendKeys(HomePage.getWeatherCardSetLocation(), weatherinput);
		/*
		 * if(Type.equals(weatherType.zip)){
		 * WrapperDef.sendKeys(home.getWeatherCardSetLocation(), Keys.DOWN);
		 * WrapperDef.sendKeys(home.getWeatherCardSetLocation(), Keys.ENTER); }
		 */
		WrapperMethods.verifyElement(HomePage.getWeatherCardSugg(), "Drop down");

		if (toggle)
			WrapperMethods.clickJavaScript(HomePage.getWeatherCardSetCel());
		WrapperMethods.clickJavaScript(HomePage.getWeatherCardSet());
		UMReporter.log(LogStatus.INFO, "After set Weather validations");
		WrapperMethods.contains_Text(HomePage.getWeatherCardLocation(), valstring, "Set Location worked",
				"Set Location is not working");
		validateWeatherCard();
	}

	public static void validateWeatherCardRedirect() {
		WrapperMethods.clickJavaScript(HomePage.getWeatherCardLocation());
		MethodDef.explicitWaitVisibility(WeatherPage.getCurrentLocStat(), "Current City is visible in weathers page",
				"Current City is not visible in weathers page");
		WrapperMethods.assertIsTrue(MethodDef.getURL().equals(EnvironmentHelper.getURL() + "/weather"),
				"Weather Card redirects correcly -" + MethodDef.getURL(),
				"Weather Card didn't redirects correcly - " + MethodDef.getURL());
	}

	public static void testHomePageHeaderSearchInvalid() {

		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
				UMReporter.log(LogStatus.INFO, "Info: Testing Home Page Header Search CNN: Entering Invalid Text");
				String invalidText = "gdfgfe";
				WrapperMethods.getWebElement(HomePage.getHeaderSearch()).click();
				WrapperMethods.sendKeys(HomePage.getSearchField(), invalidText);
				WrapperMethods.clickJavaScript(HomePage.getHomeHeaderSearchButton());
				WrapperMethods.waitForPageLoaded();
				Thread.sleep(3000);
				String content1 = "Your search for " + invalidText + " did not match any documents.";
				WrapperMethods.contains_Text(HomePage.getSearchNoMatch(), content1,
						"Your search did not match message posted successfully",
						"Your search did not match message not posted successfully");
			} else {
				UMReporter.log(LogStatus.INFO, "Info: Testing Home Page Header Search CNN: Entering Invalid Text");

				WrapperMethods.clickJavaScript(HomePage.getNavMenu());
				String invalidText = "gdfgfe";
				// home.getHeaderSearch().click();
				WrapperMethods.sendKeys(HomePage.getSearchField(), invalidText + Keys.ENTER);
				WrapperMethods.waitForPageLoaded();
				Thread.sleep(3000);
				String content1 = "Your search for " + invalidText + " did not match any documents.";
				WrapperMethods.contains_Text(HomePage.getSearchNoMatch(), content1,
						"Your search did not match message posted successfully",
						"Your search did not match message not posted successfully");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Home Page Search CNN Section");
		}

	}

	public static void testHomePageHeaderSearchBlank() {
		// Creating custom Page instances

		try {
			if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
				UMReporter.log(LogStatus.INFO, "Info: Testing Home Page Header Search CNN: Entering Blank Text");
				WrapperMethods.clickJavaScript(HomePage.getHomeHeaderSearchButton());
				WrapperMethods.clickJavaScript(HomePage.getSearchField());
				WrapperMethods.getWebElement(HomePage.getSearchField()).sendKeys(Keys.ENTER);
				MethodDef.explicitWaitVisibility(HomePage.getSearchEmpty(), 20, "Empty Search is visible",
						"Empty Search is not visible");
				String content2 = "Your search did not match any documents.";
				WrapperMethods.contains_Text(HomePage.getSearchEmpty(), content2,
						"Your search did not match message posted successfully",
						"Your search did not match message not posted successfully");
			} else {
				UMReporter.log(LogStatus.INFO, "Info: Testing Home Page Header Search CNN: Entering Blank Text");
				try {
					WrapperMethods.clickJavaScript(HomePage.getNavMenu());
				} catch (Exception e) {
					WrapperMethods.click(HomePage.getNavMenu(), "Nav Menu");
				}
				try {
					WrapperMethods.clickJavaScript(HomePage.getSearchField());
				} catch (Exception e) {
					WrapperMethods.click(HomePage.getSearchField(), "Search field");
				}
				WrapperMethods.sendKeys(HomePage.getSearchField(), Keys.ENTER);

				WrapperMethods.waitPresenceOfEleLocated(By.xpath("//p[@id='cnnSearchEmpty']"), 40);
				String content2 = "Your search did not match any documents.";
				WrapperMethods.contains_Text(HomePage.getSearchEmpty(), content2,
						"Your search did not match message posted successfully",
						"Your search did not match message not posted successfully");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.INFO, "Error in accessing the Home Page Search CNN Section");
		}
	}

	public static void weatherpageRecentLocationValidation() {
		try {
			Reporter.log("<br>Launched the Weather page</br>");
			List<String> country = new ArrayList<String>();
			country.add("Aabenraa, Denmark");
			country.add("B0 Los Sitios, PR");
			country.add("C Gables, FL");
			country.add("D Hanis, TX");
			country.add("EJido Hermosillo, Mexico");
			country.add("F E Warren AFB, WY");
			country.add("Aaron, GA");
			country.add("Babaevo, Russia");
			country.add("Caapiranga, Brazil");
			country.add("Da Costa, TX");
			country.add("Eagan, TN");
			country.add("Fabius, AL");

			for (int i = 0; i < 12; i++) {
				WrapperMethods.sendKeys(WeatherPage.getForecastsearch(), country.get(i));
				MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "Forecase drop down is visible",
						"Forecase drop down is not visible");
				WrapperMethods.clickJavaScript(WeatherPage.getForecastdrop());
				WrapperMethods.getWebElement(WeatherPage.getForecast()).click();
				UMReporter.log(LogStatus.INFO, "Entered Location " + country.get(i).toString() + "");
			}

			country.add("G Wood, MS");
			boolean flag = false;
			int itemCount = 0;
			for (int i = 0; i < WeatherPage.getRecentLocationItems().size(); i++) {
				if (WeatherPage.getRecentLocationItems().get(i).getText() == country.get(1)) {
					UMReporter.log(LogStatus.WARNING,
							"Un-expectedly, After added 7th country, First added country is displayed  ");
					flag = true;
				}
				itemCount++;
			}
			if (itemCount > 6) {
				UMReporter.log(LogStatus.WARNING, "Recent Location displaying more than 6 items.");
			}
			if (flag == false) {
				UMReporter.log(LogStatus.INFO, "As expected First location has changed after entering 7th country");
			}

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, " Error in RecentLocation Validation");
		}
	}

	public static void testHomePageFooterSearchInvalid() {
		// Creating custom Page instances

		try {
			UMReporter.log(LogStatus.INFO, "Info: Testing Home Page Footer Search CNN: Entering Invalid Text");
			String invalidText = "gdfgfe";
			Thread.sleep(3000);
			try {
				WrapperMethods.sendKeys(HomePage.getHomeFooterSearchCNNBox(), invalidText);
			} catch (Exception ee) {
				WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchCNNBoxCC());
				WrapperMethods.sendKeys(HomePage.getHomeFooterSearchCNNBoxCC(), invalidText);
			}
			WrapperMethods.clickJavaScript(HomePage.getHomeFooterSearchButton());
			// WrapperDef.scrollDown(wf.go());
			WrapperMethods.waitForPageLoaded();
			Thread.sleep(3000);
			String content1 = "Your search for " + invalidText + " did not match any documents.";
			// System.out.println(content1);
			// Reporter.log("content1");
			WrapperMethods.contains_Text(HomePage.getSearchNoMatch(), content1,
					"Your search did not match message posted successfully",
					"Your search did not match message not posted successfully");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Home Page Search CNN Section");
		}
	}

	public static void testHomeVidLinks() {
		WebElement vid = null;

		try {

			for (WebElement test : HomePage.getVidLinks()) {
				if (test.isDisplayed()) {
					vid = test;
					break;
				}
			}

			try {
				WrapperMethods.verifyElement(vid, "Video links ");
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Video Element is not present in the page");
			}

			WrapperMethods.clickJavaScript(HomePage.gethomeVidLinks());

			WrapperMethods.waitForPageLoaded();

			MethodDef.explicitWaitVisibility(HomePage.getVidLoad(), 20, "Video Load is visible",
					"Video Load is not visible");

			WrapperMethods.verifyElement(HomePage.getVidLoad(), "Video page ");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Video element");
		}
	}

	public static void WeatherPageOtherValidations() {

		try {
			UMReporter.log(LogStatus.INFO, "Weather Page Ads Validation:");
			try {
				if (WrapperMethods.getWebElement(WeatherPage.getTopAd()).isDisplayed()) {
					UMReporter.log(LogStatus.PASS, "Ad is displayed at top of the Page (Just below the Header Nav)");
				} else {
					throw new Exception();
				}
			} catch (Exception e1) {
				try {
					WrapperMethods.assertIsTrue(WeatherPage.getTopAdv().size() != 0,
							"Ad is not present but Ad element is available in the page",
							"Ad is not present and Ad element is also not available in the page");
				} catch (Exception e2) {
					UMReporter.log(LogStatus.WARNING, "Ad is displayed at top of the Page. Please check Manually");
				}
			}

			try {
				WrapperMethods.verifyElement(WeatherPage.getRightRailAd1(), "Ad in right rail");
			} catch (NoSuchElementException e2) {

				try {
					WrapperMethods.verifyElement(WeatherPage.getRightRailAd2(), "Ad in Right Rail");
				} catch (Exception e3) {
					UMReporter.log(LogStatus.WARNING,
							"Ad is displayed in Right Rail of the Page. Please check Manually");
				}
			}

			try {
				if (WrapperMethods.getWebElement(WeatherPage.getBottomAd1()).isDisplayed()) {

					WrapperMethods.verifyElement(WeatherPage.getBottomAd1(), "Ad in Bottom of the Page");
				} else if (WrapperMethods.getWebElement(WeatherPage.getBottomAd2()).isDisplayed()) {
					WrapperMethods.verifyElement(WeatherPage.getBottomAd2(), "Ad in Bottom of the Page");
				} else {

					WrapperMethods.verifyElement(WeatherPage.getBottomAd3(), "Ad in Bottom of the Page");
				}

			} catch (NoSuchElementException e4) {
				try {

					if (WrapperMethods.getWebElement(WeatherPage.getBottomAd2()).isDisplayed()) {

						WrapperMethods.verifyElement(WeatherPage.getBottomAd2(), "Ad in Bottom of the Page");
					} else if (WrapperMethods.getWebElement(WeatherPage.getBottomAd3()).isDisplayed()) {
						WrapperMethods.verifyElement(WeatherPage.getBottomAd3(), "Ad in Bottom of the Page");
					} else {

						WrapperMethods.verifyElement(WeatherPage.getBottomAd1(), "Ad in Bottom of the Page");
					}
				} catch (NoSuchElementException e5) {
					try {
						if (WrapperMethods.getWebElement(WeatherPage.getBottomAd3()).isDisplayed()) {

							WrapperMethods.verifyElement(WeatherPage.getBottomAd3(), "Ad in Bottom of the Page");
						} else if (WrapperMethods.getWebElement(WeatherPage.getBottomAd1()).isDisplayed()) {
							WrapperMethods.verifyElement(WeatherPage.getBottomAd1(), "Ad in Bottom of the Page");
						} else {

							WrapperMethods.verifyElement(WeatherPage.getBottomAd2(), "Ad in Bottom of the Page");
						}
					} catch (Exception e6) {
						UMReporter.log(LogStatus.WARNING,
								"Ad is displayed in Bottom of the Page. Please check Manually");
					}
				}
			}
			UMReporter.log(LogStatus.INFO, "Ad is displayed in Bottom of the Page. Please check Manually");
			int i = 0;

			for (WebElement WeatherParameter : WeatherPage.getWeatherParameters()) {
				i++;
				WrapperMethods.verifyElement(WeatherParameter, "Weather Parameter");

			}

			WrapperMethods.contains_Text_Attribute(WeatherPage.getLocationTextBox(), "placeholder", "Enter Location",
					"Default text in text box is displayed Correctly: ", "Default text in text box is not displayed:");

			WrapperMethods.verify_Text(WeatherPage.getWeatherPageCurrentTemp(),
					WrapperMethods.getWebElement(WeatherPage.getWeatherPageFooterTemp()).getText(),
					"Footer weather temparature and Current Conditions temparature in Weather Page ");

			WrapperMethods.verifyElement(WeatherPage.getWeatherMapsSection(), "Weather Maps Section ");
			UMReporter.log(LogStatus.INFO, "Changing the location to California");
			WrapperMethods.sendKeys(WeatherPage.getForecastsearch(), "California,KY");
			MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "Forecast drop down is visible",
					"Forecast drop down is not visible");
			WrapperMethods.click(WeatherPage.getForecastdrop(), "Forecast dropdown");
			WrapperMethods.click(WeatherPage.getForecast(), "Forecast");

			WrapperMethods.startwith_Text(WeatherPage.getCurrentLoc(), "California");

			WrapperMethods.verifyElement(WeatherPage.getWeatherMapsSection(),
					"Weather Maps Section after location Change");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the Weather Page elements");
		}
	}

	public static void testWeatherPageRecentLocation() {

		try {

			WrapperMethods.verifyElement(WeatherPage.getWeatherRecentLocationBlock(), "The Recent Location Block ");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Accessing Weather Recent Location Block" + E.getMessage());
		}

	}

	public static void explorePartsValidations() {
		WrapperMethods.verifyElement(HomePage.getbourdainImage(), "Bourdain Image ");

		WrapperMethods.verifyElement(HomePage.getWanMrBourdain(), "'Want more Bourdain? Sign up here.' Heading ");

		WrapperMethods.contains_Text(HomePage.getWanMrBourdain(), "Want more Bourdain? Sign up here.",
				"'Want more Bourdain? Sign up here.' Heading text is correct",
				"'Want more Bourdain? Sign up here.' Heading text is not correct -"
						+ WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getText());

		WrapperMethods.verifyCssValue(HomePage.getWanMrBourdain(), "font-family", "Montserrat",
				"Want more Bourdain? Sign up here. heading is Montserrat"
						+ WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-family"));

		WrapperMethods.assertIsTrue(
				WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-weight").contains("bold")
						|| WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-weight")
								.contains("700"),
				"Want more Bourdain? Sign up here. heading is BOLD",
				"Want more Bourdain? Sign up here. heading is not BOLD -"
						+ WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-weight"));

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-size").contains("22px")
							|| WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-size")
									.contains("36px"),
					"Want more Bourdain? Sign up here. heading is displayed in correct pixel size",
					"Want more Bourdain? Sign up here. heading is not displayed in correct pixel size");
		} else {
			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-size").contains("18px")
							|| WrapperMethods.getWebElement(HomePage.getWanMrBourdain()).getCssValue("font-size")
									.contains("28px"),
					"Want more Bourdain? Sign up here. heading is displayed in correct pixel size",
					"Want more Bourdain? Sign up here. heading is not displayed in correct pixel size");
		}
		WrapperMethods.verifyCssValue(HomePage.getWanMrBourdain(), "color", "rgba(0, 0, 0, 1)",
				"Want more Bourdain? Sign up here. heading color ");

		WrapperMethods.verifyElement(HomePage.getMailLogo(), "Mail Logo");

		WrapperMethods.verifyElement(HomePage.getmailidtextarea(), "Mail Id Text box ");

		String random = RandomStringUtils.random(5, "ABFERPLD");
		try {
			WrapperMethods.sendKeys(HomePage.getmailidtextarea(), random + "@gmail.com");
			UMReporter.log(LogStatus.INFO, "Entered the Email id..");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Email id is not entered.." + random + "@gmail.com");
		}

		WrapperMethods.verifyElement(HomePage.clickArrow(), "Arrow icon ");

		try {
			WrapperMethods.click(HomePage.clickArrow(), "Click Arrow");
			UMReporter.log(LogStatus.PASS, "Clicked Arrow ..");
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Not able to click the Arrow icon");
		}

		MethodDef.explicitWaitVisibilityXpath("//div[@class='message submitted']", 30, "Submitted message is present",
				"Submitted message is noy present");

		WrapperMethods.verify_Text(HomePage.getSubMsg(), "Thanks for signing up! Check your inbox to confirm.",
				"Thanks for signing up! Check your inbox to confirm. ");

	}

	public static void basicMarketCardValidations() {
		try {
			UMReporter.log(LogStatus.INFO, "Info: Basic Market Card Validations");

			WrapperMethods.verifyElement(HomePage.getMarketCard(), "the Market card widget ");
			WrapperMethods.textNotEmpty(HomePage.getMarketCardHeader(),
					"the Market card header text is displayed: "
							+ WrapperMethods.getWebElement(HomePage.getMarketCardHeader()).getText(),
					"the Market card header text is not displayed");

			WrapperMethods.contains_Text_Attribute(HomePage.getMarketCardHeaderHref(), "href", "",
					"the Market card header href is displayed: "
							+ WrapperMethods.getWebElement(HomePage.getMarketCardHeaderHref()).getAttribute("href"),
					"the Market card header href is not displayed");
			WrapperMethods.verifyElement(HomePage.getMarketCardHeaderTop(), "the Market card widget header top ");
			WrapperMethods.verifyElement(HomePage.getMarketCardHeaderBottom(), "the Market card widget header bottom ");
			UMReporter.log(LogStatus.INFO, "Info: Basic Market Card  Validations");

			for (WebElement elem : HomePage.getMarketCardItem()) {
				WrapperMethods.verifyElement(elem, "the Market card Item ");
			}
			for (WebElement elem1 : HomePage.getMarketCardItem()) {
				WrapperMethods.textNotEmpty(elem1, "the Market card title is displayed:  " + elem1.getText(),
						"the Market card title is not displayed");
			}
			for (WebElement elem2 : HomePage.getMarketItemIcon()) {
				WrapperMethods.verifyElement(elem2, "the Market card icon ");
			}
			for (WebElement elem3 : HomePage.getMarketItemValue()) {
				WrapperMethods.textNotEmpty(elem3, "the Market card value is displayed:  " + elem3.getText(),
						"the Market card value is not displayed");
			}
			for (WebElement elem4 : HomePage.getMarketItemValueChange()) {
				WrapperMethods.textNotEmpty(elem4, "the Market card value change is displayed:  " + elem4.getText(),
						"the Market card value change is not displayed");
			}
			UMReporter.log(LogStatus.INFO, "Info: Basic Market Card Quote bar Validations");

			WrapperMethods.verifyElement(HomePage.getMarketQuoteBar(), "the Market card Quote bar ");
			WrapperMethods.verifyElement(HomePage.getMarketQuoteFinancialField(), "the Market card FinancialField ");
			WrapperMethods.verifyElement(HomePage.getMarketQuoteFinancialFieldButton(),
					"the Market card Financial Field Button ");
			WrapperMethods.textNotEmpty(HomePage.getMarketQuoteFinancialFieldButtonText(),
					"the Market card Financial Field Button text is displayed:  "
							+ WrapperMethods.getWebElement(HomePage.getMarketQuoteFinancialFieldButtonText()).getText(),
					"the Market card Financial Field Button text is not displayed");
			WrapperMethods.textNotEmpty(HomePage.getMarketTimeStamp(),
					"the Market card Timestamp is displayed:  "
							+ WrapperMethods.getWebElement(HomePage.getMarketTimeStamp()).getText(),
					"the Market card Timestamp is not displayed");

		}

		catch (Exception E) {
			UMReporter.log(LogStatus.WARNING,
					"Error in accessing the Market Card elements - May be the scripts are ran in non Market timing");
			// error.addError("Error in accessing the Market Card elements");
		}
	}

	public static void testAdpresentthirdzone() throws IOException, InterruptedException {

		WrapperMethods.verifyElement(HomePage.Adthirdzone(), "Ad in the Third Zone ");
	}

	public static void metaContentValidation() {

		WrapperMethods.contains_Text_Attribute(HomePage.getMeta(), "content", "5550296508,18793419640",
				"Meta data contains the content as expected", "Meta data does not contains the content as expected");
	}

	public static void testSoundCloudPlayer() {

		if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {

			WrapperMethods.switchToFrame(By.xpath("//iframe[contains(@src,'sound')]"));
			WrapperMethods.verifyElement(HomePage.SoundCloudPlay_btn(), "The Sound cloud Player Play Button ");

			WrapperMethods.contains_Text(HomePage.cookiepolicytext(), "Cookie policy",
					"The Sound cloud Player Cookie Policy text is present - Unexpected",
					"The Sound cloud Player Cookie Policy text is not present - Unexpected");

			UMReporter.log(LogStatus.INFO, "Cookie Policy Href Validation");

			MethodDef.brokenLinkValidation(
					WrapperMethods.getWebElement(HomePage.cookiepolicyhref()).getAttribute("href"));

			WrapperMethods.verifyElement(HomePage.SoundcloudLogo(), "The Sound cloud Player Logo ");

			Reporter.log("<br>Sound cloud Player Logo href Validation");

			MethodDef
					.brokenLinkValidation(WrapperMethods.getWebElement(HomePage.SoundcloudLogo()).getAttribute("href"));

			WrapperMethods.verifyElement(HomePage.SoundcloudTitle(), "The Sound cloud Player Title ");

			UMReporter.log(LogStatus.INFO, "Sound cloud Player Title href Validation");

			MethodDef.brokenLinkValidation(
					WrapperMethods.getWebElement(HomePage.SoundcloudTitle()).getAttribute("href"));

			WrapperMethods.verifyElement(HomePage.SoundcloudDescription(), "The Sound cloud Player Description");

			UMReporter.log(LogStatus.INFO, "Sound cloud Player Description href Validation");

			MethodDef.brokenLinkValidation(
					WrapperMethods.getWebElement(HomePage.SoundcloudDescription()).getAttribute("href"));
		} else {
			if (!System.getProperty("environment").equals("PROD")) {
				WrapperMethods.verifyElement(HomePage.embeddedspundcloudmobile(), "The Embedded Sound cloud Player");
			}
		}
	}

	public static void Validatesubscriptionmodule() {

		WrapperMethods.contains_Text(HomePage.Subscribeheader(), "CNN Newsletters",
				"The CNN Subscribe Header is displayed in the page as expeted",
				"The CNN Subscribe Header is not displayed as expected ");

		WrapperMethods.verifyElement(HomePage.Sharebar(), "Social icons are visible");

		WrapperMethods.contains_Text(HomePage.Subscribeheaderinfo(),
				"CNN News, delivered. Select from our newsletters below and enter your email to subscribe.",
				"The CNN Subscribe Header info is displayed in the page as expeted",
				"The CNN Subscribe Header info is not displayed as expected ");

		WrapperMethods.checkIfPresent(HomePage.getsubscribemedia(), 15, "Subscripbe Media");

		WrapperMethods.verifyElement(HomePage.getnightcapimage(), "The Night Cap image");

		WrapperMethods.verifyElement(HomePage.getreliableresourceimg(), "The Reliable resource image ");

		WrapperMethods.verifyElement(HomePage.getfivethingsimg(), "The Five things image ");

		WrapperMethods.verifyElement(HomePage.getquestmeansbusinessimg(), "The Quest means business image ");

		WrapperMethods.verifyElement(HomePage.getquestmeansbusinessimg(), "The Great Big Story image ");

		WrapperMethods.verify_Text(HomePage.getnightcapsubtitle(), "You�ll Sleep Better Knowing.",
				"The Night Cap image");

		WrapperMethods.verify_Text(HomePage.getreliableresourcesubtitle(), "Essential Media News",
				"The Reliable resource subtitle");

		WrapperMethods.verify_Text(HomePage.getfivethingssubtitle(), "Up to Speed. Out The Door.",
				"The Five things subtitle");

		WrapperMethods.verify_Text(HomePage.getquestmeansbusinesssubtitle(), "Quest Means Business",
				"The Quest means business subtitle");

		WrapperMethods.verify_Text(HomePage.getgrreatbigstorysubtitle(), "Feed Your Feed.",
				"The Great Big Story subtitle");

		WrapperMethods.verifyElement(HomePage.getnightcapemaildesc(), "The Night Cap Email description");

		WrapperMethods.verifyElement(HomePage.getreliableresourcedesc(), "The Reliable resource Email description");
		WrapperMethods.verifyElement(HomePage.getfivethingsdesc(), "The Five things Email description");

		WrapperMethods.verifyElement(HomePage.getquestmeansbusinessdesc(),
				"The Quest means business Email description");

		WrapperMethods.verifyElement(HomePage.getgrreatbigstorydesc(), "The Great Big Story Email description");

		WrapperMethods.verify_Text(HomePage.nightcapnewsletter_btn(), "Politics Straight Up. Add Now.",
				"The Night Cap Newsletter button");

		WrapperMethods.verify_Text(HomePage.reliablesourcenewsletter_btn(), "Take on Media and Add Now.",
				"The Reliable resource Newsletter button");

		WrapperMethods.verify_Text(HomePage.fivethingsnewsletter_btn(), "Know in 5 and Add Now.",
				"The Five things Newsletter button");

		WrapperMethods.verify_Text(HomePage.questmeansbusinessnewsletter_btn(), "Fresh Biz Perspective. Add Now.",
				"The Quest means business Newsletter button");

		WrapperMethods.verifyElement(HomePage.greatbigstorynewsletter_btn(), "The Great Big Story Newsletter button");

	}

	public static void Validatesubscribebutton() {

		WrapperMethods.verifyElement(HomePage.Emailaddresstext(),
				"Email Address text in the footer subscribe container");

		WrapperMethods.assertHasAttribute(WrapperMethods.getWebElement(HomePage.Subscribemailtextbox()), "placeholder",
				"Enter email here", "The Default text in the subscribe text box is correct !!!",
				"The default text is invisible/Incorrect in the subscribe container text box");

		WrapperMethods.verifyElement(HomePage.subscribebutton(), "The Subscription button");

		WrapperMethods.verifyElement(HomePage.Subscribepolicy(), "The Subscribe policy text");

		WrapperMethods.click(HomePage.subscribebutton(), "Subscribe button");

		WrapperMethods.verify_Text(HomePage.errormessage(), "*Please select at least one newsletter.",
				"The Subscription button error message");

		WrapperMethods.clickJavaScript(HomePage.nightcapnewsletter_btn());

		WrapperMethods.click(HomePage.subscribebutton(), "Subscribe button");

		WrapperMethods.verify_Text(HomePage.errormessage(), "*Please enter valid email",
				"The Subscription button error message");

		WrapperMethods.sendKeys(HomePage.Subscribemailtextbox(), "cnnweqa@gmail.com");

		UMReporter.log(LogStatus.INFO, "The Email address is entered in the Subscribtion text box");

		WrapperMethods.clickJavaScript(HomePage.subscribebutton(), "Subscribe button is clicked",
				"Subscribe button is not clicked");

		MethodDef.explicitWaitVisibility(HomePage.subscribedmessage(), 20, "Subscribe message is visible",
				"Subscribe message is not visible");

		WrapperMethods.verifyElement(HomePage.thanksforsigningmsg(), "The subscription for the selected newsletter");

	}

	public static void CFacebooksharevalidation() throws InterruptedException {
		WebDriver driver = DriverFactory.getCurrentDriver();

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {

			try {
				WrapperMethods.clickJavaScript(HomePage.SubscriptionFbicon(), "Facebook icon is clicked",
						"Facebook icon is not clicked");
				Reporter.log("<BR> The Facebook icon is clicke- first attempt<BR>");
				Thread.sleep(15000);
				WrapperMethods.switchToLastWindow();
				WebDriverWait wait = new WebDriverWait(driver, 15);

				wait.until(
						ExpectedConditions.visibilityOf(WrapperMethods.getWebElement(HomePage.FBLaunchvalidation())));
				UMReporter.log(LogStatus.INFO, "The Facebook window is opened after click");

			} catch (Exception e) {
				try {
					WrapperMethods.click(HomePage.SubscriptionFbicon(), "Subscription FB icon");
					UMReporter.log(LogStatus.INFO, "The Facebook icon is clicked -second attempt");
					Thread.sleep(15000);
					WrapperMethods.switchToLastWindow();
					WebDriverWait wait = new WebDriverWait(driver, 35);
					wait.until(ExpectedConditions
							.visibilityOf(WrapperMethods.getWebElement(HomePage.FBLaunchvalidation())));
					UMReporter.log(LogStatus.INFO, "The Facebook window is opened after click");

				} catch (Exception E) {
					UMReporter.log(LogStatus.INFO, "The share icon is Not clicked");

				}
			}

			WrapperMethods.verifyElement(HomePage.FBLaunchvalidation(), "The Facebook page is opened after click");

			WrapperMethods.verifyElement(HomePage.FBLaunchvalidation(), "The Enter Email Text box");

			WrapperMethods.verifyElement(HomePage.FBpassLaunchvalidation(), "The Password Text box");
		}
	}

	public static void CTwittersharevalidation() throws InterruptedException {
		try {
			WrapperMethods.clickJavaScript(HomePage.SubscriptionTwittericon(), "Subscription Twitter Icon is clicked",
					"Subscription Twitter Icon is not clicked");
			UMReporter.log(LogStatus.INFO, "The Twitter icon is clicked - first attempt");

			Thread.sleep(5000);
			WrapperMethods.switchToLastWindow();
			MethodDef.explicitWaitVisibility(HomePage.TwitterLAunchvalidation(), 15, "Twitter Application is visible",
					"Twitter Application is not visible");
		} catch (Exception e) {
			try {
				WrapperMethods.click(HomePage.SubscriptionTwittericon(), "Twitter icon");
				UMReporter.log(LogStatus.INFO, "The Twitter icon is clicked - second attempt");

				Thread.sleep(5000);
				WrapperMethods.switchToLastWindow();
				MethodDef.explicitWaitVisibility(HomePage.TwitterLAunchvalidation(), 15,
						"Twitter Application is visible", "Twitter Application is not visible");
			} catch (Exception E) {

			}

			UMReporter.log(LogStatus.INFO, "The share icon is already clicked");

		}
		// WrapperDef.click(politics.getheadertwittershareicon());

		WrapperMethods.verifyElement(HomePage.TwitterLAunchvalidation(), "The Twitter page is opened after click");
		WrapperMethods.verifyElement(HomePage.twitteremailtextbox(), "The Enter Email Text box ");
		WrapperMethods.verifyElement(HomePage.twitterpassword(), "The Password Text box ");
	}

	public static void Cmorevalidation() throws InterruptedException {

		WrapperMethods.verifyElement(HomePage.SubscriptionMoreicon(), "The more share icon in the share container");

		MethodDef.explicitWaitVisibility(HomePage.SubscriptionMoreicon(), "Subscription More Icon is visible",
				"Subscription More Icon is not visible");
		try {
			WrapperMethods.click(HomePage.SubscriptionMoreicon(), "Subscription more icon");
			MethodDef.explicitWaitVisibility(HomePage.getMorevalidation(), "More validaiton is visible",
					"More validation is not present");

		} catch (Exception e) {
			WrapperMethods.clickJavaScript(HomePage.SubscriptionMoreicon());

			MethodDef.explicitWaitVisibility(HomePage.getMorevalidation(), "More validation is visible",
					"More validation is not visible");

		}

		if (HomePage.getmoreiconvalidation().size() == 5) {
			for (WebElement moreicon : HomePage.getmoreiconvalidation()) {
				WrapperMethods.verifyElement(moreicon, "The more element pop-up Icon");
			}
		}

	}

	public static void SubscriptonNav() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		try {
			if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
				for (int subsecSz = 0; subsecSz < HomePage.headerSubsections().size(); subsecSz++) {
					String header = HomePage.headerSubsections().get(subsecSz).getText();
					HomePage.headerSubsections().get(subsecSz).click();
					WrapperMethods.waitForPageLoaded();
					String urlContent = driver.getCurrentUrl();
					WrapperMethods.assertIsTrue(!urlContent.contains("edition"),
							"After Clicking the " + header + " Navigated to " + driver.getCurrentUrl()
									+ " and it is navigated correct edition",
							"After Clicking the " + header + " Navigated to " + driver.getCurrentUrl()
									+ " and it is not navigated correct edition");
					driver.navigate().back();
					WrapperMethods.waitForPageLoaded();
				}
			} else if (System.getProperty("edition").equalsIgnoreCase("INTL")) {
				for (int subsecSz = 0; subsecSz < HomePage.headerSubsections().size(); subsecSz++) {

					if (!HomePage.headerSubsections().get(subsecSz).getText().equalsIgnoreCase("Tech")) {
						String header = HomePage.headerSubsections().get(subsecSz).getText();
						HomePage.headerSubsections().get(subsecSz).click();
						WrapperMethods.waitForPageLoaded();
						String urlContent = driver.getCurrentUrl();
						WrapperMethods.assertIsTrue(
								urlContent.contains("edition") || urlContent.contains("INTERNATIONAL/"),
								"After Clicking the " + header + " Navigated to " + driver.getCurrentUrl()
										+ " and it is navigated correct edition",
								"After Clicking the " + header + " Navigated to " + driver.getCurrentUrl()
										+ " and it is not navigated correct edition");
						driver.navigate().back();
						WrapperMethods.waitForPageLoaded();
					}
				}
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL,
					"Error On navigating section/subsection from subscription page- Edition specific navigation");
		}
	}

	/**
	 * Validating the Search 404 page elements
	 * 
	 * @param go
	 *            - wedbriver instance
	 * @param error
	 *            - Soft error instance
	 * @param home
	 *            - homepage instance
	 */
	public static void validateSearch404elements() {
		try {
			WrapperMethods.contains_Text(HomePage.getErrorTitle(), "the error title is displayed:",
					"the error title is not displayed");
			WrapperMethods.contains_Text(HomePage.getErrorText(), "the error text is displayed: ",
					"the error text is not displayed");
			WrapperMethods.isDisplayed(HomePage.getErrorsearch(), "the search form is displayed",
					"the search form is not displayed");
			WrapperMethods.isDisplayed(HomePage.getErrorsearchinput(), "the search input field is displayed",
					"the search input field is not displayed");
			WrapperMethods.contains_Text_Attribute(HomePage.getErrorSearchDefaultText(), "placeholder",
					"the error text is displayed: ", "the error text is not displayed");
			WrapperMethods.isDisplayed(HomePage.getErrorsearchbutton(), "the search button is displayed",
					"the search button is not displayed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Search 404 page Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void testshowdescafterresize() throws IOException, InterruptedException {
		WebDriver go = DriverFactory.getCurrentDriver();
		go.manage().window().setSize(new Dimension(400, 250));
		WrapperMethods.isDisplayed(HomePage.ShowDescafterresize(), "Show Description is present as expected",
				"Show Description is not present  - UnExpected");
		WrapperMethods.click(HomePage.ShowDescafterresize(), "Show Description");
		WrapperMethods.isDisplayed(HomePage.ShowDesparagraphcafterresize(),
				"Description Paragraph is present as expected", "Description Paragraph is not present - UnExpected");
		WrapperMethods.isDisplayed(HomePage.HideDesafterresize(), "Hide Description is present as expected",
				"Hide Description is not present - UnExpected");
	}

	public static void testBrandingBanner() throws IOException, InterruptedException {
		WrapperMethods.isDisplayed(HomePage.BrandingBanner(), "Branding Banner is present at the top as expected",
				"Branding Banner is not present at the top - UnExpected");
	}



	public static void headerHeaderNavEntertainMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderEntertainmentMenuLinks();
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_entertainment").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_entertainment").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavHealthMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderHealthMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_health").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavBleacherMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderBleacherMenuLinks();
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_bleacher").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_style").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavLivingMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderLivingMenuLinks();
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_living").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_style").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavMoneyMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderMoneyMenuLinks();
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_money").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_money").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavMoreMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderMoreMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_more").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNav() {
		WebDriver go = DriverFactory.getCurrentDriver();
		chkDeviceHdr();
		String[] domData;
		if (System.getProperty("edition").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header").split(",");
		int i = 0;
		for (String temp : domData) {
			List<WebElement> Nav = HomePage.getNavLinks();
			String[] headItems = temp.split("~");
			if (!headItems[0].equals("Politics") && !headItems[0].equals("Money") && !headItems[0].equals("Video")
					&& !headItems[0].equals("Style")) {
				UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
				WrapperMethods.contains_Text(Nav.get(i), headItems[0], "Section Text is correct",
						"Section Text is not correct");
				WrapperMethods.assertHasAttribute(Nav.get(i), "href", headItems[1], "Section href is correct",
						"Section href is not correct");
				WrapperMethods.clickJavaScript(Nav.get(i));
				// Nav.get(i).click();
				WrapperMethods.waitForPageLoaded();
				HomePage.wait(15);
				if (headItems[2].equals("image")) {
					Reporter.log("<BR>This Section Title contains Logo but not the Text</BR>");
				} else {
					try {
						WrapperMethods.contains_Text(HomePage.getSectionTitle(), headItems[2],
								"Section Title is correct", "Section Title is not correct");
					} catch (Exception e) {
						if (headItems[0].equals("Features"))
							WrapperMethods.contains_Text(HomePage.getSectionTitles(), headItems[2],
									"Section Title is correct", "Section Title is not correct");
					}
				}
				if (System.getProperty("edition").equals("DOM") && !System.getProperty("environment").equals("PROD"))
					go.navigate().to("http://" + System.getProperty("environment").toLowerCase() + ".next.cnn.com/");
				if (System.getProperty("edition").equals("INTL") && !System.getProperty("environment").equals("PROD"))
					go.navigate()
							.to("http://edition." + System.getProperty("environment").toLowerCase() + ".next.cnn.com/");
				System.out.println("-------" + "http://edition." + System.getProperty("environment").toLowerCase()
						+ ".next.cnn.com/");
				if (System.getProperty("edition").equals("DOM") && System.getProperty("environment").equals("PROD"))
					go.navigate().to("http://www.cnn.com/");
				if (System.getProperty("edition").equals("INTL") && System.getProperty("environment").equals("PROD"))
					go.navigate().to("http://edition.cnn.com/");
				HomePage.wait(15);
			}
			i++;
		}
	}

	public static void headerNavFlyOut() {
		WrapperMethods.isDisplayed(HomePage.getNavMenu(), "Nav Flyout Button is present",
				"Nav Flyout Button is not present");
		// WrapperMethods.click(HomePage.getNavMenu());
		WrapperMethods.clickJavaScript(HomePage.getNavMenu());
		HomePage.wait(5);
		WrapperMethods.isDisplayed(HomePage.getNavFlyOut(), "Nav Flyout is present", "Nav Flyout is not present");
		/*
		 * try { home.getCurrentEdition().click(); home.wait(5); error.addError(
		 * "Edition is not disabled"); } catch (Exception E) {
		 * MethodDef.passLog("Edition is disabled"); }
		 */
	}

	public static void headerHeaderNavOpinionsMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderOpinionsMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_opinions").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavPoliticsMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderPoliticsMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_politics_menu").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavTechMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderTechMenuLinks();
		String[] domData;
		if (System.getProperty("site").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_tech").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_tech").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavStyleMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderStyleMenuLinks();
		String[] domData;
		if (System.getProperty("site").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_style").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_style").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavTravelMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderTravelMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_travel").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavUSMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderUSMenuLinks();
		String[] domData;
		if (System.getProperty("site").equals("DOM"))
			domData = ConfigProp.getPropertyValue("dom_header_us").split(",");
		else
			domData = ConfigProp.getPropertyValue("ed_header_us").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavVideosMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderVideoMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_videos").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderNavWorldMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderWorldMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("dom_header_world").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderINTLFeaturesNavMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderINTLFeaturesMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("ed_header_features").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderINTLRegionsNavMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderINTLRegionsMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("ed_header_regions").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void headerHeaderINTLNavMoreMenu() {
		navMenuClick();
		List<WebElement> Nav = HomePage.getHeaderINTLMoreMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("ed_header_more").split(",");
		int i = 0;
		for (String temp : domData) {
			String[] headItems = temp.split("~");
			UMReporter.log(LogStatus.INFO, "Element under test " + Nav.get(i).getText());
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}
	}

	public static void ValidatesubscribtionCSSproperties() {

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "font-size", "18",
				"The Font Size of the  subscribe button is 18px");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-left-width", "1px",
				"The border-left-width  subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-right-width", "1px",
				"The border-right-width of the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-top-width", "1px",
				"The border-top-width of the  subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-bottom-width", "1px",
				"The border-bottom-width of the  subscribe button");

		WrapperMethods.clickJavaScript(HomePage.greatbigstorynewsletter_btn());

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "font-size", "18",
				"The Font Size of the Added Text after cliking the subscribe button is 18px");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "color", "rgba(35, 112, 253, 1)",
				"The Color of the Added Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "background-color",
				"rgba(254, 254, 254, 1)", "The background-color of the Added Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-left-width", "1px",
				"The border-left-width of the 'Added' Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-right-width", "1px",
				"The border-right-width of the 'Added' Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-top-width", "1px",
				"The border-top-width of the 'Added' Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.greatbigstorynewsletter_btn(), "border-bottom-width", "1px",
				"The border-bottom-width of the 'Added' Text after cliking the subscribe button");

		WrapperMethods.verifyCssValue(HomePage.getnightcapemaildesc(), "font-size", "16",
				"The Font Size of the description is 16px");

		WrapperMethods.verifyCssValue(HomePage.getnightcapemaildesc(), "color", "rgba(89, 89, 89, 1)",
				"The Font Color of the description");

		WrapperMethods.verifyCssValue(HomePage.getquestmeansbusinesssubtitle(), "font-size", "20",
				"The Font Size of the description is 20px");

		WrapperMethods.verifyCssValue(HomePage.getquestmeansbusinesssubtitle(), "color", "rgba(38, 38, 38, 1)",
				"The Font Color of the Title");

		WrapperMethods.verifyCssValue(HomePage.SubscriptionEmailContainer(), "background-color",
				"rgba(241, 241, 241, 1)", "The Subscription container Fill in color");

		WrapperMethods.verifyCssValue(HomePage.Emailaddresstext(), "font-size", "18px",
				"The Email Address Label size is 18px");

		WrapperMethods.verifyCssValue(HomePage.Emailaddresstext(), "color", "rgba(38, 38, 38, 1)",
				"The Email Address Font Color");

		WrapperMethods.verifyCssValue(HomePage.SubscriptionEmailForm(), "line-height", "60px",
				"The Email Baner Line height is 60px");

		WrapperMethods.verifyCssValue(HomePage.Subscribemailtextbox(), "font-size", "16px",
				"The Subscribe Email text size is 16px");

		WrapperMethods.verifyCssValue(HomePage.Subscribemailtextbox(), "color", "rgba(89, 89, 89, 1)",
				"The Subscribe Email text Color");

		WrapperMethods.verifyCssValue(HomePage.Subscribepolicy(), "font-size", "16px",
				"The Subscribe Policy text size is 16px");

		WrapperMethods.verifyCssValue(HomePage.Subscribepolicy(), "color", "rgba(89, 89, 89, 1)",
				"The Subscribe Policy text Color");

		WrapperMethods.verifyCssValue(HomePage.SubscribepolicyLink(), "font-size", "16px",
				"The Subscribe Policy Link text size is 16px");

		WrapperMethods.verifyCssValue(HomePage.SubscribepolicyLink(), "color", "rgba(35, 112, 253, 1)",
				"The Subscribe Policy Link text Color");

		WrapperMethods.verifyCssValue(HomePage.subscribebutton(), "font-size", "18px",
				"The Subscribe Policy Link text size is 18px");

		WrapperMethods.verifyCssValue(HomePage.subscribebutton(), "color", "rgba(254, 254, 254, 1)",
				"The Subscribe Policy Link text Color");

		WrapperMethods.verifyCssValue(HomePage.subscribebutton(), "width", "308px",
				"The Subscribe button text size is 308px");

		WrapperMethods.verifyCssValue(HomePage.subscribebutton(), "height", "40px",
				"The Subscribe Policy Link text size is 40px");

	}

	public static void outbrainValidate() {
		try {

			int adlen = Integer.parseInt(WrapperMethods.getWebElement(PoliticsPage.secondZoneAdContainer())
					.getCssValue("height").split("px")[0]);

			if (adlen >= 600) {
				UMReporter.log(LogStatus.INFO,
						"Ad length is greater than or equal to 600 px. Verifying source code for SF_6");
				JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getCurrentDriver();
				String Text = (String) executor
						.executeScript("return document.getElementsByTagName('html')[0].innerHTML");
				WrapperMethods.assertIsTrue(Text.contains("SF_6"), "the SF_6 outbrain Zone is Present in page source",
						"the SF_6 outbrain Zone is not Present in page source");
			} else {
				WrapperMethods.verifyElement(PoliticsPage.secondZoneOutbrain(), "the SF_6 outbrain Zone");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error Accessing Politics outbrain content");
		}
	}

	public void testContentElements() {
		// Creating custom Page instances
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				UMReporter.log(LogStatus.INFO, "Content to content navigation elements");
				ArticlePageFunctions.testContentNavigationElements();
			} else {
				Reporter.log("INFO: Content to content navigation is not applicable for devices");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, " error in accessing the Content to content navigation elements");
		}
	}

	public static void headerEditionPickerValidations() {

		try {

			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")
					|| ConfigProvider.getConfig("Platform").equalsIgnoreCase("Tablet")) {
				navMenuClick();
				WrapperMethods.click(HomePage.getMobileCurrentEdition(), "Mobile current edition");
				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.click(HomePage.getMobileIntlRadiobutton(), "Mobile International Radio button");
					WrapperMethods.click(HomePage.getSetEditionHdrButtonDev(), "International Header Button Dev");
					WrapperMethods.waitForPageLoaded();
					String currentURL = DriverFactory.getCurrentDriver().getCurrentUrl();

					WrapperMethods.assertIsTrue(currentURL.contains("edition"), "Both URLs are different",
							"Both URLs are same");

				} else {
					UMReporter.log(LogStatus.INFO, "Validating the Edition Picker for International Site");
					WrapperMethods.clickJavaScript(HomePage.getMobileUSRadiobutton());
					WrapperMethods.clickJavaScript(HomePage.getSetEditionHdrButtonDev());
					WrapperMethods.waitForPageLoaded();
					String currentURL = DriverFactory.getCurrentDriver().getCurrentUrl();

					WrapperMethods.assertIsTrue(currentURL.contains("us"), "Both URLs are different",
							"Both URLs are same");
				}
			} else {
				UMReporter.log(LogStatus.INFO, "Validating the Edition Picker for Domestic Site");

				if (System.getProperty("edition").equals("DOM")) {
					WrapperMethods.clickJavaScript(HomePage.getIntlRadiobutton());
					WrapperMethods.clickJavaScript(HomePage.getSetEditionHdrButton());
					WrapperMethods.waitForPageLoaded();
					String currentURL = DriverFactory.getCurrentDriver().getCurrentUrl();

					WrapperMethods.assertIsTrue(currentURL.contains("edition"), "Both URLs are different",
							"Both URLs are same");
				} else {
					UMReporter.log(LogStatus.INFO, "Validating the Edition Picker for International Site");
					WrapperMethods.clickJavaScript(HomePage.getUSRadiobutton());
					WrapperMethods.clickJavaScript(HomePage.getSetEditionHdrButton());
					WrapperMethods.waitForPageLoaded();
					String currentURL = DriverFactory.getCurrentDriver().getCurrentUrl();

					WrapperMethods.assertIsTrue(currentURL.contains("us"), "Both URLs are different",
							"Both URLs are same");
				}
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in validating edition picker validations" + E.getMessage());
		}
	}

	public static void testHomePageWatchTv() {
		// Creating custom Page instances
		if (System.getProperty("edition").equals("DOM")) {
			int counter = 0;
			try {
				MethodDef.explicitWaitVisibility(HomePage.getHomeMutedPlayer(), "Mute Player is present",
						"Mute Playeris not present");
				try {
					WrapperMethods.verifyElement(HomePage.getHomeMutedPlayerTitle(), "Mute Player Title");

					WrapperMethods.contains_Text_Attribute(HomePage.getHomeMutedPlayerTitleLink(), "href",
							"http://cnn.it/go", "Title has correct Hyperlink", "Title has incorrect Hyperlink");
					MethodDef.brokenLinkValidation(
							WrapperMethods.getWebElement(HomePage.getHomeMutedPlayerTitleLink()).getAttribute("href"));

					try {
						if (WrapperMethods.getWebElement(HomePage.getHomeMutedPlayerContentLink()).isDisplayed()) {
							MethodDef.explicitWaitClickable(HomePage.getHomeMutedPlayerContentLink(),
									"Mute Player Content Link is clickable",
									"Mute Player Content link isnot clickable");
							WrapperMethods.contains_Text_Attribute(HomePage.getHomeMutedPlayerContentLink(), "href",
									Prefs.AUT_URL + "go/?stream=CNN", "Player has correct Hyperlink",
									"Player has wrong Hyperlink");
							MethodDef.brokenLinkValidation(WrapperMethods
									.getWebElement(HomePage.getHomeMutedPlayerContentLink()).getAttribute("href"));
							counter++;
							WrapperMethods.textNotEmpty(HomePage.getHomeMutedPlayerNextShow(), "Next show");

							WrapperMethods.assertIsTrue(
									!WrapperMethods.getWebElement(HomePage.getHomeMutedPlayerNextShowHyper())
											.getAttribute("href").isEmpty(),
									"Next show is hyperlinked", "Next show is not hyperlinked");
							WrapperMethods
									.assertIsTrue(
											!WrapperMethods.getWebElement(HomePage.getHomeMutedPlayerShowTime())
													.getText().isEmpty(),
											"Next show time is present", "Next show time is not present");
						}
					} catch (Exception e) {
					}
					try {
						if (WrapperMethods.getWebElement(HomePage.getHeaderWatchLivePlayer()).isDisplayed()) {
							MethodDef.explicitWaitClickable(HomePage.getHeaderWatchLivePlayer(),
									"WAtch Live Player is clickable", "Watch Live Player is not clickable");
							WrapperMethods.verifyElement(HomePage.getHeaderWatchLivePlayer(), "Muted player ");
							counter++;
						}
					} catch (Exception e) {

					}

					if (counter == 0) {
						UMReporter.log(LogStatus.ERROR, "No Muted Player is present");
					}

				} catch (Exception E) {
					UMReporter.log(LogStatus.FAIL, "Error in Muted Player Element/s");
				}
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Muted Player is not present");
			}
		}
	}

	public static void testFooterSectionValidation() {
		// Creating custom Page instances
		try {
			if (System.getProperty("edition").equals("INTL")) {
				// Home Page
				UMReporter.log(LogStatus.INFO, "HOME PAGE Footer Links Testing");
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlSectionLinks());
						HomePageFunctions.footerIntlTestingImages();
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlSectionLinks");
					}
				} else {
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlSectionLinks());
						HomePageFunctions.footerIntlTestingImages();
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlSectionLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlRegionsLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlRegionsLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlUSLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlUSLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlMoneyLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlMoneyLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlEntertainmentLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlEntertainmentLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlTechLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlTechLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlSportLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlSportLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlTravelLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlTravelLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlStyleLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlStyleLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlHealthLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlHealthLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlFeaturesLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlFeaturesLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlVideosLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlVideosLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlVrLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footer International VR Links");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerIntlMoreLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerIntlMoreLinks");
					}

				}
			} else if (System.getProperty("edition").equals("DOM")) {
				// Home Page
				//Reporter.log("<b><i><em>HOME PAGE Footer Links Testing</b></i></em>");
				if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomSectionLinks());
						HomePageFunctions.footerDomTestingImages();
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomSectionLinks");
					}
				} else {
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomSectionLinks());
						HomePageFunctions.footerDomTestingImages();
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomSectionLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomUSLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomUSLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomWorldLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomWorldLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomPoliticsLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomPoliticsLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomMoneyLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomMoneyLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomOpinionLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomOpinionLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomHealthLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomHealthLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomEntertainmentLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomEntertainmentLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomTechLinks());
					} catch (Exception e) {

						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomTechLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomStyleLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomStyleLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomTravelLinks());
					} catch (Exception e) {

						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomTravelLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomSportsLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomSportsLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomLivingLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomLivingLinks");

					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomVideoLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomVideoLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomVrLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomVrLinks");
					}
					try {
						HomePageFunctions.footerTesting(HomePageFunctions.footerDomMoreLinks());
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Error accessing the footerDomMoreLinks");
					}
				}
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error accessing the footer item");
		}
	}

	public static void footerTesting(Map<String, List<String>> footerLinks) {
		Map<String, List<String>> map;
		map = footerLinks;
		splitHrefValidation(map.entrySet());
	}

	public static void splitHrefValidation(Set<Entry<String, List<String>>> entrySet) {
		for (Entry<String, List<String>> entry : entrySet) {
			String path = entry.getKey();
			List<String> value = entry.getValue();
			String linktext = value.get(0);
			String href = value.get(1);
			MethodDef.validateLinkTextHref(path, linktext, href);
		}
	}

	public static Map<String, List<String>> footerDomSectionLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "li.m-footer__title";

		map.put(path + " a[href='/us']", addValuesMap(map, "U.S.", "/us"));
		map.put(path + " a[href='/world']", addValuesMap(map, "WORLD", "/world"));
		map.put(path + " a[href='/politics']", addValuesMap(map, "POLITICS", "/politics"));
		map.put(path + " a[href='//money.cnn.com']", addValuesMap(map, "MONEY", "//money.cnn.com"));
		map.put(path + " a[href='/opinions']", addValuesMap(map, "OPINION", "/opinions"));
		map.put(path + " a[href='/health']", addValuesMap(map, "HEALTH", "/health"));
		map.put(path + " a[href='/entertainment']", addValuesMap(map, "ENTERTAINMENT", "/entertainment"));
		map.put(path + " a[href='//money.cnn.com/technology']",
				addValuesMap(map, "TECH", "//money.cnn.com/technology"));
		map.put(path + " a[href='/style']", addValuesMap(map, "STYLE", "/style"));
		map.put(path + " a[href='/travel']", addValuesMap(map, "TRAVEL", "/travel"));
		map.put(path + " a[href='//bleacherreport.com']", addValuesMap(map, "BLEACHER", "//bleacherreport.com"));
		map.put(path + " a[href='/living']", addValuesMap(map, "LIVING", "/living"));
		map.put(path + " a[href='/videos']", addValuesMap(map, "VIDEO", "/videos"));
		map.put(path + " a[href='/vr']", addValuesMap(map, "CNNVR", "/vr"));
		map.put(path + " a[href='/more']", addValuesMap(map, "MORE�", "/more"));

		return map;
	}

	public static List<String> addValuesMap(Map<String, List<String>> map, String val1, String val2) {
		List<String> v = new ArrayList<String>();
		v.add(val1);
		v.add(val2);
		return (v);
	}

	public static void footerIntlTestingImages() {

		UMReporter.log(LogStatus.INFO, "Object under Test: MONEY");
		Reporter.log("<br>");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterIntlMoney(), "src", "img",
				"Link has the correct Image", "Link is not having the Image");
		UMReporter.log(LogStatus.INFO, "Object under Test: STYLE");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterIntlStyle(), "src", "img",
				"Link has the correct Image", "Link is not having the Image");

	}

	public static Map<String, List<String>> footerIntlSectionLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "li.m-footer__title";

		map.put(path + " a[href='/regions']", addValuesMap(map, "REGIONS", "/regions"));
		map.put(path + " a[href='/politics']", addValuesMap(map, "U.S. Politics", "/politics"));
		map.put(path + " a[href='//money.cnn.com/INTERNATIONAL/']",
				addValuesMap(map, "MONEY", "//money.cnn.com/INTERNATIONAL/"));
		map.put(path + " a[href='/entertainment']", addValuesMap(map, "ENTERTAINMENT", "/entertainment"));
		map.put(path + " a[href='//money.cnn.com/technology/']",
				addValuesMap(map, "TECH", "//money.cnn.com/technology/"));
		map.put(path + " a[href='/sport']", addValuesMap(map, "SPORT", "/sport"));
		map.put(path + " a[href='/travel']", addValuesMap(map, "TRAVEL", "/travel"));
		map.put(path + " a[href='/style']", addValuesMap(map, "STYLE", "/style"));
		map.put(path + " a[href='/specials']", addValuesMap(map, "FEATURES", "/specials"));
		map.put(path + " a[href='/videos']", addValuesMap(map, "VIDEO", "/videos"));
		map.put(path + " a[href='/vr']", addValuesMap(map, "CNNVR", "/vr"));
		map.put(path + " a[href='/more']", addValuesMap(map, "MORE�", "/more"));
		return map;
	}

	public static Map<String, List<String>> footerIntlRegionsLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--intl_regions li.m-footer__list-item";
		map.put(path + " a[href='/us']", addValuesMap(map, "U.S.", "/us"));
		map.put(path + " a[href='/africa']", addValuesMap(map, "AFRICA", "/africa"));
		map.put(path + " a[href='/americas']", addValuesMap(map, "AMERICAS", "/americas"));
		map.put(path + " a[href='/asia']", addValuesMap(map, "ASIA", "/asia"));
		map.put(path + " a[href='/china']", addValuesMap(map, "CHINA", "/china"));
		map.put(path + " a[href='/europe']", addValuesMap(map, "EUROPE", "/europe"));
		map.put(path + " a[href='/middle-east']", addValuesMap(map, "MIDDLE EAST", "/middle-east"));
		map.put(path + " a[href='/opinions']", addValuesMap(map, "Opinion", "/opinions"));

		return map;
	}

	public static Map<String, List<String>> footerIntlUSLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--politics li.m-footer__list-item";

		map.put(path + " a[href='/specials/politics/president-donald-trump-45']",
				addValuesMap(map, "45", "/specials/politics/president-donald-trump-45"));
		map.put(path + " a[href='/specials/politics/congress-capitol-hill']",
				addValuesMap(map, "CONGRESS", "/specials/politics/congress-capitol-hill"));
		map.put(path + " a[href='/specials/politics/us-security']",
				addValuesMap(map, "SECURITY", "/specials/politics/us-security"));
		map.put(path + " a[href='/specials/politics/supreme-court-nine']",
				addValuesMap(map, "THE NINE", "/specials/politics/supreme-court-nine"));
		map.put(path + " a[href='/specials/politics/trumpmerica']",
				addValuesMap(map, "TRUMPMERICA", "/specials/politics/trumpmerica"));

		return map;
	}

	public static Map<String, List<String>> footerIntlTravelLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--intl_travel li.m-footer__list-item";

		map.put(path + " a[href='/travel/aviation']", addValuesMap(map, "AVIATION", "/travel/aviation"));
		map.put(path + " a[href='/specials/travel/business-traveller']",
				addValuesMap(map, "BUSINESS TRAVELLER", "/specials/travel/business-traveller"));
		map.put(path + " a[href='/travel/destinations']", addValuesMap(map, "DESTINATIONS", "/travel/destinations"));
		map.put(path + " a[href='/specials/travel']", addValuesMap(map, "FEATURES", "/specials/travel"));

		map.put(path + " a[href='/travel/food-and-drink']", addValuesMap(map, "Food/Drink", "/travel/food-and-drink"));

		map.put(path + " a[href='/travel/hotels']", addValuesMap(map, "HOTELS", "/travel/hotels"));

		map.put(path + " a[href='http://www.cnnpartners.com/']",
				addValuesMap(map, "Partner Hotels", "http://www.cnnpartners.com/"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : Style
	 */
	public static Map<String, List<String>> footerIntlStyleLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--style li.m-footer__list-item";

		map.put(path + " a[href='/style/fashion']", addValuesMap(map, "FASHION", "/style/fashion"));
		map.put(path + " a[href='/style/design']", addValuesMap(map, "DESIGN", "/style/design"));
		map.put(path + " a[href='/style/architecture']", addValuesMap(map, "ARCHITECTURE", "/style/architecture"));
		map.put(path + " a[href='/style/arts']", addValuesMap(map, "ARTS", "/style/arts"));
		map.put(path + " a[href='/style/autos']", addValuesMap(map, "AUTOS", "/style/autos"));
		map.put(path + " a[href='/style/luxury']", addValuesMap(map, "LUXURY", "/style/luxury"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : Money
	 */
	public static Map<String, List<String>> footerIntlMoneyLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--money li.m-footer__list-item";

		map.put(path + " a[href='//money.cnn.com/news/']", addValuesMap(map, "BUSINESS", "//money.cnn.com/news/"));
		map.put(path + " a[href='//money.cnn.com/data/markets/']",
				addValuesMap(map, "MARKETS", "//money.cnn.com/data/markets/"));
		map.put(path + " a[href='//money.cnn.com/technology/']",
				addValuesMap(map, "TECH", "//money.cnn.com/technology/"));
		map.put(path + " a[href='//money.cnn.com/luxury/']", addValuesMap(map, "LUXURY", "//money.cnn.com/luxury/"));

		return map;
	}

	/**
	 * Footer Internaional Menu item : Entertainment
	 */
	public static Map<String, List<String>> footerIntlEntertainmentLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__bucket.m-footer__bucket__entertainment li";

		map.put(path + " a[href='/entertainment/celebrities']",
				addValuesMap(map, "Stars", "/entertainment/celebrities"));
		map.put(path + " a[href='/entertainment/movies']", addValuesMap(map, "Screen", "/entertainment/movies"));
		map.put(path + " a[href='/entertainment/tv-shows']", addValuesMap(map, "Binge", "/entertainment/tv-shows"));
		map.put(path + " a[href='/entertainment/culture']", addValuesMap(map, "Culture", "/entertainment/culture"));
		map.put(path + " a[href='//money.cnn.com/media']", addValuesMap(map, "Media", "//money.cnn.com/media"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : Tech
	 */
	public static Map<String, List<String>> footerIntlTechLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--tech li.m-footer__list-item";

		map.put(path + " a[href='//money.cnn.com/technology/business/']",
				addValuesMap(map, "BUSINESS", "//money.cnn.com/technology/business/"));
		map.put(path + " a[href='//money.cnn.com/technology/culture/']",
				addValuesMap(map, "CULTURE", "//money.cnn.com/technology/culture/"));
		map.put(path + " a[href='//money.cnn.com/technology/gadgets/']",
				addValuesMap(map, "GADGETS", "//money.cnn.com/technology/gadgets/"));
		map.put(path + " a[href='//money.cnn.com/technology/future/']",
				addValuesMap(map, "FUTURE", "//money.cnn.com/technology/future/"));
		map.put(path + " a[href='//money.cnn.com/technology/startups/']",
				addValuesMap(map, "STARTUPS", "//money.cnn.com/technology/startups/"));

		return map;
	}

	/**
	 * Footer Internaional Menu item : Health
	 */
	public static Map<String, List<String>> footerIntlHealthLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--health li.m-footer__list-item";

		map.put(path + " a[href='/specials/health/diet-fitness']",
				addValuesMap(map, "DIET + FITNESS", "/specials/health/diet-fitness"));
		map.put(path + " a[href='/specials/health/living-well']",
				addValuesMap(map, "LIVING WELL", "/specials/health/living-well"));
		map.put(path + " a[href='/specials/living/cnn-parents']",
				addValuesMap(map, "PARENTING + FAMILY", "/specials/living/cnn-parents"));
		map.put(path + " a[href='/specials/health/vital-signs']",
				addValuesMap(map, "VITAL SIGNS", "/specials/health/vital-signs"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : Sport
	 */
	public static Map<String, List<String>> footerIntlSportLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--sport li.m-footer__list-item";

		map.put(path + " a[href='/sport/football']", addValuesMap(map, "FOOTBALL", "/sport/football"));
		map.put(path + " a[href='/sport/golf']", addValuesMap(map, "GOLF", "/sport/golf"));
		map.put(path + " a[href='/sport/tennis']", addValuesMap(map, "TENNIS", "/sport/tennis"));
		map.put(path + " a[href='/sport/motorsport']", addValuesMap(map, "MOTORSPORT", "/sport/motorsport"));
		map.put(path + " a[href='/sport/horse-racing']", addValuesMap(map, "HORSERACING", "/sport/horse-racing"));
		map.put(path + " a[href='/sport/equestrian']", addValuesMap(map, "EQUESTRIAN", "/sport/equestrian"));
		map.put(path + " a[href='/sport/sailing']", addValuesMap(map, "SAILING", "/sport/sailing"));
		map.put(path + " a[href='/sport/skiing']", addValuesMap(map, "SKIING", "/sport/skiing"));
		return map;
	}

	/**
	 * Footer Internaional Menu Item : Features
	 */
	public static Map<String, List<String>> footerIntlFeaturesLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--features li.m-footer__list-item";

		// map.put(path + " a[href='/specials/travel/guides']",
		// addValuesMap(map, "INSIDER GUIDES", "/specials/travel/guides"));
		map.put(path + " a[href='/specials/world/freedom-project']",
				addValuesMap(map, "FREEDOM PROJECT", "/specials/world/freedom-project"));
		map.put(path + " a[href='/specials/impact-your-world']",
				addValuesMap(map, "IMPACT YOUR WORLD", "/specials/impact-your-world"));
		map.put(path + " a[href='/specials/africa/inside-africa']",
				addValuesMap(map, "INSIDE AFRICA", "/specials/africa/inside-africa"));
		map.put(path + " a[href='/specials/opinions/two-degrees']",
				addValuesMap(map, "2 DEGREES", "/specials/opinions/two-degrees"));
		map.put(path + " a[href='/specials']", addValuesMap(map, "All Features", "/specials"));
		return map;
	}

	/**
	 * Footer Internaional Menu Item : Videos
	 */
	public static Map<String, List<String>> footerIntlVideosLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--intl_videos li.m-footer__list-item";

		map.put(path + " a[href='/specials/latest-news-videos']",
				addValuesMap(map, "VIDEO NEWS", "/specials/latest-news-videos"));
		map.put(path + " a[href='/specials/international-video-landing/feature-show-videos']",
				addValuesMap(map, "FEATURE SHOWS", "/specials/international-video-landing/feature-show-videos"));
		map.put(path + " a[href='/specials/videos/hln']", addValuesMap(map, "HLN", "/specials/videos/hln"));
		map.put(path + " a[href='/tv/shows']", addValuesMap(map, "TV SHOWS", "/tv/shows"));
		map.put(path + " a[href='/tv/schedule/europe']", addValuesMap(map, "TV SCHEDULE", "/tv/schedule/europe"));
		map.put(path + " a[href='/specials/tv/anchors-and-reporters']",
				addValuesMap(map, "FACES OF CNN WORLDWIDE", "/specials/tv/anchors-and-reporters"));
		map.put(path + " a[href='/vr']", addValuesMap(map, "CNNVR", "/vr"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : VR
	 */
	public static Map<String, List<String>> footerIntlVrLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--vr li.m-footer__list-item";

		map.put(path + " a[href='/2017/03/04/vr/how-to-watch-vr']",
				addValuesMap(map, "HOW TO WATCH VR", "/2017/03/04/vr/how-to-watch-vr"));
		map.put(path + " a[href='/specials/vr/vr-archives']",
				addValuesMap(map, "ARCHIVES", "/specials/vr/vr-archives"));

		return map;
	}

	/**
	 * Footer Internaional Menu Item : More
	 */
	public static Map<String, List<String>> footerIntlMoreLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--intl_more li.m-footer__list-item";

		map.put(path + " a[href='/weather']", addValuesMap(map, "WEATHER", "/weather"));
		map.put(path + " a[href='/specials/cnn-heroes']", addValuesMap(map, "CNN HEROES", "/specials/cnn-heroes"));
		map.put(path + " a[href='/specials/profiles']", addValuesMap(map, "CNN PROFILES", "/specials/profiles"));
		map.put(path + " a[href='/specials/more/cnn-leadership']",
				addValuesMap(map, "LEADERSHIP", "/specials/more/cnn-leadership"));
		map.put(path + " a[href='//cnnpressroom.blogs.cnn.com/']",
				addValuesMap(map, "PRESSROOM", "//cnnpressroom.blogs.cnn.com/"));
		map.put(path + " a[href='/specials/world/specials-page-cnn-partners']",
				addValuesMap(map, "Partner sites", "/specials/world/specials-page-cnn-partners"));
		map.put(path + " a[href='/specials/opinions/cnnireport']",
				addValuesMap(map, "IREPORT", "/specials/opinions/cnnireport"));

		return map;
	}

	public static void footerDomTestingImages() {

		UMReporter.log(LogStatus.INFO, "Object under Test: POLITICS");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterDomPolitics(), "src", "img",
				"Link has the correct Image", "Link has incorrect Image");
		UMReporter.log(LogStatus.INFO, "Object under Test: MONEY");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterDomMoney(), "src", "img", "Link has the correct Image",
				"Link has incorrect Image");
		UMReporter.log(LogStatus.INFO, "Object under Test: STYLE");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterDomStyle(), "src", "img", "Link has the correct Image",
				"Link has incorrect Image");
		UMReporter.log(LogStatus.INFO, "Object under Test: BLEACHER");
		WrapperMethods.contains_Text_Attribute(HomePage.getFooterDomBleacher(), "src", "img",
				"Link has the correct Image", "Link has incorrect Image");

	}

	public static Map<String, List<String>> footerDomUSLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--us li.m-footer__list-item";

		map.put(path + " a[href='/specials/us/crime-and-justice']",
				addValuesMap(map, "CRIME + JUSTICE", "/specials/us/crime-and-justice"));
		map.put(path + " a[href='/specials/us/energy-and-environment']",
				addValuesMap(map, "ENERGY + ENVIRONMENT", "/specials/us/energy-and-environment"));
		map.put(path + " a[href='/specials/us/extreme-weather']",
				addValuesMap(map, "EXTREME WEATHER", "/specials/us/extreme-weather"));
		map.put(path + " a[href='/specials/space-science']",
				addValuesMap(map, "SPACE + SCIENCE", "/specials/space-science"));
		return map;
	}

	/**
	 * Footer Domestic Menu Item : World
	 */
	public static Map<String, List<String>> footerDomWorldLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--world li.m-footer__list-item";

		map.put(path + " a[href='/africa']", addValuesMap(map, "AFRICA", "/africa"));
		map.put(path + " a[href='/americas']", addValuesMap(map, "AMERICAS", "/americas"));
		map.put(path + " a[href='/asia']", addValuesMap(map, "ASIA", "/asia"));
		map.put(path + " a[href='/europe']", addValuesMap(map, "EUROPE", "/europe"));
		map.put(path + " a[href='/middle-east']", addValuesMap(map, "MIDDLE EAST", "/middle-east"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Politics
	 */
	public static Map<String, List<String>> footerDomPoliticsLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--politics li.m-footer__list-item";

		map.put(path + " a[href='/specials/politics/president-donald-trump-45']",
				addValuesMap(map, "45", "/specials/politics/president-donald-trump-45"));
		map.put(path + " a[href='/specials/politics/congress-capitol-hill']",
				addValuesMap(map, "CONGRESS", "/specials/politics/congress-capitol-hill"));
		map.put(path + " a[href='/specials/politics/us-security']",
				addValuesMap(map, "SECURITY", "/specials/politics/us-security"));
		map.put(path + " a[href='/specials/politics/supreme-court-nine']",
				addValuesMap(map, "THE NINE", "/specials/politics/supreme-court-nine"));
		map.put(path + " a[href='/specials/politics/trumpmerica']",
				addValuesMap(map, "TRUMPMERICA", "/specials/politics/trumpmerica"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Money
	 */
	public static Map<String, List<String>> footerDomMoneyLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--money li.m-footer__list-item";

		map.put(path + " a[href='//money.cnn.com/data/markets/']",
				addValuesMap(map, "MARKETS", "//money.cnn.com/data/markets/"));
		map.put(path + " a[href='//money.cnn.com/technology/']",
				addValuesMap(map, "TECH", "//money.cnn.com/technology/"));
		map.put(path + " a[href='//money.cnn.com/media/']", addValuesMap(map, "MEDIA", "//money.cnn.com/media/"));
		map.put(path + " a[href='//money.cnn.com/pf/']", addValuesMap(map, "PERSONAL FINANCE", "//money.cnn.com/pf/"));
		map.put(path + " a[href='//money.cnn.com/luxury/']", addValuesMap(map, "LUXURY", "//money.cnn.com/luxury/"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item: Opinion
	 */
	public static Map<String, List<String>> footerDomOpinionLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--opinions li.m-footer__list-item";

		map.put(path + " a[href='/specials/opinion/opinion-politics']",
				addValuesMap(map, "POLITICAL OP-EDS", "/specials/opinion/opinion-politics"));
		map.put(path + " a[href='/specials/opinion/opinion-social-issues']",
				addValuesMap(map, "SOCIAL COMMENTARY", "/specials/opinion/opinion-social-issues"));
		return map;
	}

	/**
	 * Footer Domestic Menu item : Health
	 */
	public static Map<String, List<String>> footerDomHealthLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--health li.m-footer__list-item";

		map.put(path + " a[href='/specials/health/diet-fitness']",
				addValuesMap(map, "DIET + FITNESS", "/specials/health/diet-fitness"));
		map.put(path + " a[href='/specials/health/living-well']",
				addValuesMap(map, "LIVING WELL", "/specials/health/living-well"));
		map.put(path + " a[href='/specials/living/cnn-parents']",
				addValuesMap(map, "PARENTING + FAMILY", "/specials/living/cnn-parents"));

		return map;
	}

	/**
	 * Footer Domestic Menu item : Entertainment
	 */
	public static Map<String, List<String>> footerDomEntertainmentLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__bucket.m-footer__bucket__entertainment li";

		map.put(path + " a[href='/entertainment/celebrities']",
				addValuesMap(map, "Stars", "/entertainment/celebrities"));
		map.put(path + " a[href='/entertainment/movies']", addValuesMap(map, "Screen", "/entertainment/movies"));
		map.put(path + " a[href='/entertainment/tv-shows']", addValuesMap(map, "Binge", "/entertainment/tv-shows"));
		map.put(path + " a[href='/entertainment/culture']", addValuesMap(map, "Culture", "/entertainment/culture"));
		map.put(path + " a[href='//money.cnn.com/media']", addValuesMap(map, "Media", "//money.cnn.com/media"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Tech
	 */
	public static Map<String, List<String>> footerDomTechLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--tech li.m-footer__list-item";

		map.put(path + " a[href='//money.cnn.com/technology/business']",
				addValuesMap(map, "BUSINESS", "//money.cnn.com/technology/business"));
		map.put(path + " a[href='//money.cnn.com/technology/culture']",
				addValuesMap(map, "CULTURE", "//money.cnn.com/technology/culture"));
		map.put(path + " a[href='//money.cnn.com/technology/gadgets']",
				addValuesMap(map, "GADGETS", "//money.cnn.com/technology/gadgets"));
		map.put(path + " a[href='//money.cnn.com/technology/future']",
				addValuesMap(map, "FUTURE", "//money.cnn.com/technology/future"));
		map.put(path + " a[href='//money.cnn.com/technology/startups']",
				addValuesMap(map, "STARTUPS", "//money.cnn.com/technology/startups"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Style
	 */
	public static Map<String, List<String>> footerDomStyleLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--style li.m-footer__list-item";

		map.put(path + " a[href='/style/fashion']", addValuesMap(map, "FASHION", "/style/fashion"));
		map.put(path + " a[href='/style/design']", addValuesMap(map, "DESIGN", "/style/design"));
		map.put(path + " a[href='/style/architecture']", addValuesMap(map, "ARCHITECTURE", "/style/architecture"));
		map.put(path + " a[href='/style/autos']", addValuesMap(map, "AUTOS", "/style/autos"));
		map.put(path + " a[href='/style/luxury']", addValuesMap(map, "LUXURY", "/style/luxury"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Travel
	 */
	public static Map<String, List<String>> footerDomTravelLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--travel li.m-footer__list-item";

		map.put(path + " a[href='/specials/travel/best-of-travel']",
				addValuesMap(map, "BEST OF TRAVEL", "/specials/travel/best-of-travel"));
		map.put(path + " a[href='/specials/travel/sleep-eats']",
				addValuesMap(map, "SLEEPS + EATS", "/specials/travel/sleep-eats"));
		map.put(path + " a[href='/specials/travel/business-traveller']",
				addValuesMap(map, "BUSINESS TRAVEL", "/specials/travel/business-traveller"));
		map.put(path + " a[href='/specials/travel/aviation-more']",
				addValuesMap(map, "AVIATION + BEYOND", "/specials/travel/aviation-more"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : Sports
	 */
	public static Map<String, List<String>> footerDomSportsLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--bleacher li.m-footer__list-item";

		map.put(path + " a[href='//bleacherreport.com/nfl']",
				addValuesMap(map, "PRO FOOTBALL", "//bleacherreport.com/nfl"));
		map.put(path + " a[href='//bleacherreport.com/college-football']",
				addValuesMap(map, "COLLEGE FOOTBALL", "//bleacherreport.com/college-football"));
		map.put(path + " a[href='//bleacherreport.com/nba']",
				addValuesMap(map, "BASKETBALL", "//bleacherreport.com/nba"));
		map.put(path + " a[href='//bleacherreport.com/mlb']",
				addValuesMap(map, "BASEBALL", "//bleacherreport.com/mlb"));
		map.put(path + " a[href='//bleacherreport.com/world-football']",
				addValuesMap(map, "SOCCER", "//bleacherreport.com/world-football"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item: Living
	 */
	public static Map<String, List<String>> footerDomLivingLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--living li.m-footer__list-item";

		map.put(path + " a[href='/specials/living/eatocracy']",
				addValuesMap(map, "FOOD", "/specials/living/eatocracy"));
		map.put(path + " a[href='/specials/living/relationships']",
				addValuesMap(map, "RELATIONSHIPS", "/specials/living/relationships"));
		map.put(path + " a[href='/specials/belief']", addValuesMap(map, "RELIGION", "/specials/belief"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item: Videos
	 */
	public static Map<String, List<String>> footerDomVideoLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--videos li.m-footer__list-item";

		map.put(path + " a[href='//cnn.it/go2']", addValuesMap(map, "LIVE TV", "//cnn.it/go2"));
		map.put(path + " a[href='/specials/digital-studios']",
				addValuesMap(map, "DIGITAL STUDIOS", "/specials/digital-studios"));
		map.put(path + " a[href='/specials/videos/digital-shorts']",
				addValuesMap(map, "CNN FILMS", "/specials/videos/digital-shorts"));
		map.put(path + " a[href='/specials/videos/hln']", addValuesMap(map, "HLN", "/specials/videos/hln"));
		map.put(path + " a[href='/tv/schedule/cnn']", addValuesMap(map, "TV SCHEDULE", "/tv/schedule/cnn"));
		map.put(path + " a[href='/specials/tv/all-shows']",
				addValuesMap(map, "TV SHOWS A-Z", "/specials/tv/all-shows"));
		map.put(path + " a[href='/vr']", addValuesMap(map, "CNNVR", "/vr"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item: VR
	 */
	public static Map<String, List<String>> footerDomVrLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--vr li.m-footer__list-item";

		map.put(path + " a[href='/2017/03/04/vr/how-to-watch-vr']",
				addValuesMap(map, "HOW TO WATCH VR", "/2017/03/04/vr/how-to-watch-vr"));
		map.put(path + " a[href='/specials/vr/vr-archives']",
				addValuesMap(map, "ARCHIVES", "/specials/vr/vr-archives"));

		return map;
	}

	/**
	 * Footer Domestic Menu Item : More
	 */
	public static Map<String, List<String>> footerDomMoreLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "ol.m-footer__subtitles--more li.m-footer__list-item";

		map.put(path + " a[href='/specials/photos']", addValuesMap(map, "PHOTOS", "/specials/photos"));
		map.put(path + " a[href='/specials/cnn-longform']", addValuesMap(map, "LONGFORM", "/specials/cnn-longform"));
		map.put(path + " a[href='/specials/cnn-investigations']",
				addValuesMap(map, "INVESTIGATIONS", "/specials/cnn-investigations"));
		map.put(path + " a[href='/specials/opinions/cnnireport']",
				addValuesMap(map, "IREPORT", "/specials/opinions/cnnireport"));
		map.put(path + " a[href='/specials/profiles']", addValuesMap(map, "CNN PROFILES", "/specials/profiles"));
		map.put(path + " a[href='/specials/more/cnn-leadership']",
				addValuesMap(map, "CNN LEADERSHIP", "/specials/more/cnn-leadership"));
		map.put(path + " a[href='/email/subscription']", addValuesMap(map, "CNN NEWSLETTERS", "/email/subscription"));

		return map;
	}

	public static void breakingnewsvalidation() {
		try {
			WrapperMethods.verifyElement(HomePage.checkbreakingnews(), "The breaking news ");
		} catch (Exception e) {
			Reporter.log("The breaking news validation has the following error:" + e.getMessage());
		}
		WrapperMethods.clickJavaScript(HomePage.hamburger_btn());

		WrapperMethods.scrollDown();
		WrapperMethods.verifyElement(HomePage.getFlyoutFooterSocialIcons(), "The footer social icons ");
		WrapperMethods.verifyElement(HomePage.getFlyoutFooterFacebook(), "The footer Facebook icons ");
		WrapperMethods.verifyElement(HomePage.getFlyoutFooterTwitter(), "The footer Twitter icons ");
		WrapperMethods.verifyElement(HomePage.getFlyoutFooterInstagram(), "The footer Instagram icons ");
	}

	public static void testTimelineDarkTheme() {

		try {

			HomePageFunctions.timelineValidations();

			HomePageFunctions.timelinetitleValidationsDarkTheme();

			if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("CHROME")
					|| ConfigProvider.getConfig("Browser").equalsIgnoreCase("Safari")) {
				WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "font-weight", "normal",
						"The font weight of the title ");

			} else {
				WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "font-weight", "400",
						"The font weight of the title ");
			}

			 String color =	WrapperMethods.getCssValue(HomePage.getdarktimelinebg(),"background-color");
			 System.out.println("=------------------------------------"+color);
			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("Safari")) {
				WrapperMethods.moveToElement(HomePage.getdarkTimelinetext());

				WrapperMethods.verifyNotCssValue(HomePage.getdarkTimelinetext(), "color", color,
						"The color of the Text is correct and it is not equal to the color before hover");


				WrapperMethods.verifyCssValue(HomePage.getdarkTimelinetext(), "color", "rgba(242, 242, 242, 1)",
						"The color of the Text is correct and ");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Timeline elements");
		}

	}

	public static void timelineValidations() {

		WrapperMethods.verifyElement(HomePage.getTimelineBlock(), "The Timeline Block ");

		WrapperMethods.verifyCssValue(HomePage.getTimelineFeed(), "height", "300px",
				"The height of the timeline block is: "
						+ WrapperMethods.getWebElement(HomePage.getTimelineFeed()).getCssValue("height"));

		WrapperMethods.verifyCssValue(HomePage.getTimelineTitleLive(), "min-width", "60px",
				"The min width of the Live block is: "
						+ WrapperMethods.getWebElement(HomePage.getTimelineTitleLive()).getCssValue("min-width"));

	}

	public static void timelinetitleValidationsDarkTheme() {

		UMReporter.log(LogStatus.INFO, "Dark theme : TimeLine validation Starts........");

		WrapperMethods.contains_Text(HomePage.getTimelineTitleLive(), "Live",
				"The TimeLine contains the Live Text in the title",
				"The TimeLine does not contains the Live Text in the title");

		WrapperMethods.verifyElement(HomePage.getTileLinetitle(), "The Title of the timeline ");

		WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "line-height", "20",
				"The line height weight of the title");

		WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "color", "rgba(242, 242, 242, 1)",
				"The font color of the title ");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "font-size", "14px",
				"The font size of the SubTimeline is");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "color", "rgba(242, 242, 242, 1)",
				"The font color of the SubTimeline");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "font-weight", "300",
				"The font weight of the SubTimeline ");
	}

	public static void testTimelineLightTheme() {

		try {

			HomePageFunctions.timelineValidations();

			HomePageFunctions.timelinetitleValidationsLightTheme();

			if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("CHROME")
					|| ConfigProvider.getConfig("Browser").equalsIgnoreCase("Safari")) {
				WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "font-weight", "normal",
						"The font weight of the title ");

			} else {
				WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "font-weight", "400",
						"The font weight of the title ");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Timeline elements");
		}

	}

	public static void timelinetitleValidationsLightTheme() {

//		Reporter.log("<BR>Light theme : TimeLine validation Starts........</BR>");

		WrapperMethods.contains_Text(HomePage.getTimelineTitleLive(), "Live",
				"The TimeLine contains the Live Text in the title",
				"The TimeLine does not contains the Live Text in the title");

		WrapperMethods.verifyElement(HomePage.getTileLinetitle(), "The Title of the timeline ");

		WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "line-height", "20",
				"The line height weight of the title ");

		WrapperMethods.verifyCssValue(HomePage.getTileLinetitle(), "color", "rgba(26, 26, 26, 1)",
				"The font color of the title ");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "font-size", "14px",
				"The font size of the SubTimeline ");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "color", "rgba(26, 26, 26, 1)",
				"The font color of the SubTimeline ");

		WrapperMethods.verifyCssValue(HomePage.getMainTimelinetext(), "font-weight", "300",
				"The font weight of the SubTimeline ");
	}

	public static void SearchEntertainmentRadiobuttonvalidation(String Key) throws InterruptedException {

		WrapperMethods.click(HomePage.getEntertainmentSearch(), "Entertainment Search");
		MethodDef.explicitWaitVisibility(HomePage.headerSearchBoxText(), 10, "Header Seatch Box is visible",
				"Header Seatch Box is not visible");
		WrapperMethods.sendKeys(HomePage.headerSearchBoxText(), "Donald trump");

		WrapperMethods.sendKeys(HomePage.headerSearchBoxText(), Keys.ENTER);
		MethodDef.explicitWaitVisibility(HomePage.styleSearchBoxText(), 15, "Style Search Box is visible",
				"Style Search Box is not visible");

		List<WebElement> radio = HomePage.getSearchradiobuttos();
		UMReporter.log(LogStatus.INFO, "Total Number of raido buttons are " + radio.size());
		for (WebElement radioelement : radio) {
			if (radioelement.isSelected()) {
				WrapperMethods.assertIsTrue(radioelement.getAttribute("value").equalsIgnoreCase(Key),
						"The Radio button is selected as expected " + radioelement.getAttribute("value").toUpperCase(),
						"Incorrect radio button is selected " + radioelement.getAttribute("value").toUpperCase());
			} else {
				continue;
			}
		}

	}

	public static void SearchRadiobuttonvalidation(String Key) throws InterruptedException {

		WrapperMethods.click(HomePage.getHeaderSearch(), "Header Search");
		MethodDef.explicitWaitVisibility(HomePage.headerSearchBoxText(), 10, "Search Box is visible",
				"Search Box is not visible");
		WrapperMethods.sendKeys(HomePage.headerSearchBoxText(), "Donald trump");

		WrapperMethods.sendKeys(HomePage.headerSearchBoxText(), Keys.ENTER);
		MethodDef.explicitWaitVisibility(HomePage.styleSearchBoxText(), 15, "Style Search bix is visible",
				"Style Search box is not visible");
		Reporter.log("<BR>The Page is loaded as expected after search<BR>");

		List<WebElement> radio = HomePage.getSearchradiobuttos();
		Reporter.log("<BR>Total Number of raido buttons are " + radio.size());
		for (WebElement radioelement : radio) {
			if (radioelement.isSelected()) {
				WrapperMethods.assertIsTrue(radioelement.getAttribute("value").equalsIgnoreCase(Key),
						"The Radio button is selected as expected " + radioelement.getAttribute("value").toUpperCase(),
						"Incorrect radio button is selected " + radioelement.getAttribute("value").toUpperCase());
			} else {
				continue;
			}
		}

	}

	public static void SearchStyleRadiobuttonvalidation(String Key) throws InterruptedException {

		MethodDef.explicitWaitVisibility(HomePage.StyleSeachButton(), 10, "Search Button is visible",
				"Search Button is not visible");
		WrapperMethods.click(HomePage.StyleSeachButton(), "Style Search button");

		MethodDef.explicitWaitVisibility(HomePage.styleSearchBoxText(), 10, "Style Search Box is visible",
				"Style Search Box is not visible");

		WrapperMethods.sendKeys(HomePage.styleSearchBoxText(), "Donald trump");

		WrapperMethods.sendKeys(HomePage.styleSearchBoxText(), Keys.ENTER);
		MethodDef.explicitWaitVisibility(HomePage.styleSearchBoxText(), 15, "Style Search box is visible",
				"Style Search box is not visible");
		UMReporter.log(LogStatus.INFO, "The Page is loaded as expected after search");

		List<WebElement> radio = HomePage.getSearchradiobuttos();
		UMReporter.log(LogStatus.INFO, "Total Number of raido buttons are " + radio.size());

		for (WebElement radioelement : radio) {
			if (radioelement.isSelected()) {
				WrapperMethods.assertIsTrue(radioelement.getAttribute("value").equalsIgnoreCase(Key),
						"The Radio button is selected as expected " + radioelement.getAttribute("value").toUpperCase(),
						"Incorrect radio button is selected " + radioelement.getAttribute("value").toUpperCase());
			} else {
				continue;
			}
		}

	}

	public static void testSocialLogin() throws InterruptedException {

		HomePage.wait(20);

		try {
			MethodDef.explicitWaitVisibility(HomePage.getMycnnImg(), "My CNN Image is visible",
					"My CNN Image is not visible");

			/*
			 * error.assertTrue(home.getMycnnText().getText().equals("MyCNN"),
			 * "My CNN Text is wrong", "MyCNN Text is correct");
			 */
			WrapperMethods.verifyElement(HomePage.getMycnnImg(), "My CNN Avatar ");
			WrapperMethods.verifyElement(HomePage.getMycnnLout(), "My CNN Logout element ");
			WrapperMethods.contains_Text(HomePage.getMycnnLout(), "Log out", "My CNN Logout text is wrong",
					"My CNN Logout text is correct");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "FAILED: Error in Validating the My CNN Page");
		}
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WrapperMethods.verifyElement(HomePage.getMycnnAccDetails(), "<b>Account Details</b> link ");
			WrapperMethods.verifyElement(HomePage.getMycnnAlerts(), "Alerts link ");

		}
		// Verifying the Account details elements
		WrapperMethods.clickJavaScript(HomePage.getMycnnAccDetails());
		UMReporter.log(LogStatus.INFO, "Click on Account Details link");
		MethodDef.explicitWaitVisibility(HomePage.getMycnnAccName(), "My CNN Account name is visible",
				"My CNN Name is not visible");
		WrapperMethods.verifyElement(HomePage.getMycnnAccName(), "<b>First Name</b> field ");
		WrapperMethods.verifyElement(HomePage.getMycnnAccLName(), "Last Name field ");
		WrapperMethods.verifyElement(HomePage.getMycnnEmail(), "Email field ");

		WrapperMethods.verifyElement(HomePage.getMycnnZip(), "Postal Code field ");
		WrapperMethods.verifyElement(HomePage.getMycnnSave(), "Save button ");

		// Logout
		WrapperMethods.clickJavaScript(HomePage.getMycnnLout());
		WrapperMethods.waitForPageLoaded();
	}

	public static void headerRedLogo() {
		WrapperMethods.verifyElement(HomePage.cnnRedLogo, "RED Logo ");
	}

	public static void headerHeaderINTLNavVideosMenu() {

		navMenuClick();

		List<WebElement> Nav = HomePage.getHeaderINTLVideoMenuLinks();
		String[] domData;
		domData = ConfigProp.getPropertyValue("ed_header_videos").split(",");
		int i = 0;

		for (String temp : domData) {

			String[] headItems = temp.split("~");
			Reporter.log("<br>Element under test " + Nav.get(i).getText() + "<br>");
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}

	}

	private static void testHeaderNavCont(WebElement elem, String txt, String href) {
		if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI"))
			WrapperMethods.contains_Text(elem, txt, "Menu Text is correct: ",
					"Menu Text is not correct: " + elem.getText());
		WrapperMethods.contains_Text_Attribute(elem, "href", href, "Menu href is correct", "Menu href is not correct");

	}

	public static void headerHeaderINTLNavSportMenu() {
		navMenuClick();

		List<WebElement> Nav = HomePage.getHeaderINTLSportMenuLinks();
		String[] domData;

		domData = ConfigProp.getPropertyValue("ed_header_sport").split(",");

		int i = 0;

		for (String temp : domData) {

			String[] headItems = temp.split("~");
			Reporter.log("<br>Element under test " + Nav.get(i).getText() + "<br>");
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}

	}

	public static void headerHeaderNavINTLTravelMenu() {
		navMenuClick();

		List<WebElement> Nav = HomePage.getHeaderINTLTravelMenuLinks();
		String[] domData;

		domData = ConfigProp.getPropertyValue("ed_header_travel").split(",");

		int i = 0;

		for (String temp : domData) {

			String[] headItems = temp.split("~");
			Reporter.log("<br>Element under test " + Nav.get(i).getText() + "<br>");
			testHeaderNavCont(Nav.get(i), headItems[0], headItems[1]);
			i++;
		}

	}
	public static void moneyNav() {

		WebDriver driver = DriverFactory.getCurrentDriver();

		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				System.out.println("----------------");
				WrapperMethods.click(HomePage.linkSectionsMoney(), "Money link");
				UMReporter.log(LogStatus.INFO, "Money Link clicked");
			} else {
				WrapperMethods.getWebElement(HomePage.hamburgerMenu()).click();
				UMReporter.log(LogStatus.INFO, "Hamburger menu item link clicked");

				try {
					WrapperMethods.getWebElement(HomePage.moneyLinkDev()).click();
					UMReporter.log(LogStatus.INFO, "Money Link clicked");
				} catch (Exception e) {
					WrapperMethods.clickJavaScript(HomePage.moneyLinkDev());
					UMReporter.log(LogStatus.INFO, "Money Link clicked");
				}
			}
			WrapperMethods.waitForPageLoaded();
			WrapperMethods.assertIsTrue(
					(driver.getCurrentUrl().toString().contains("money.cnn.com/"))
							|| (driver.getCurrentUrl().toString().contains("money.cnn.com/INTERNATIONAL/")),
					"Navigation from email subscription page is correct and the url is " + driver.getTitle().toString(),
					"Navigation from email subscription page is not correct and the url is "
							+ driver.getTitle().toString());
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in navigating Money page from the Email Subscription page");
		}
	}
	public static void CMessengervalidation( WebElement element)
			throws InterruptedException {

		MethodDef.explicitWaitVisibility( element,"The more share icon is displayed in the share container",
				"The more share icon is not displayed in the share container");
		WrapperMethods.isDispWE(element, "The more share icon is displayed in the share container",
				"The more share icon is not displayed in the share container");

		try {
			WrapperMethods.clickJavaScript(element);
			UMReporter.log(LogStatus.INFO, "<B> The MEssenger icon is clicked</B>");
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "<B>The MEssenger icon is already clicked</B>");
		}

		Thread.sleep(5000);
		MethodDef.explicitWaitVisibility(HomePage.MessengerSharecontainer(), "MessengerSharecontainer is visible","MessengerSharecontainer is not visible");

		WrapperMethods.isDisplayed(HomePage.Messengercontent(), "The Messenger Content is displayed and visible",
				"The Messenger Content is  not displayed and  not visible");

		WrapperMethods.isDisplayed(HomePage.Messengerbutton(),
				"The Send to Messenger button is displayed and visible",
				"The Send to Messenger button is  not displayed and  not visible");
	}

	public static void searchValidation(String url) {
		try {
			if (url.contains("politics")) {
				WrapperMethods.waitForPageLoaded();
				HomePage.wait(10);
				UMReporter.log(LogStatus.INFO,"<B>Launched Politics Page</B>");
				WrapperMethods.isDisplayed(HomePage.USPloiticsSearchBoxText(), "Search Text Box is displayed",
						"Search Text Box is not displayed");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.USPloiticsSearchBoxText()).getAttribute("placeholder").contains("Search CNN politics"),
						"Politics- Header Search text displayed"
								+ WrapperMethods.getWebElement(HomePage.USPloiticsSearchBoxText()).getAttribute("placeholder"),
						"Politics- Header Search text is displayed");

			} else if (url.contains("money")) {
				WrapperMethods.waitForPageLoaded();
				UMReporter.log(LogStatus.INFO,"<B>Launched Money Page</B>");

				WrapperMethods.isDisplayed(HomePage.moneySearchBoxText(), "Search Text Box is displayed",
						"Search Text Box is not displayed");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.moneySearchBoxText()).getAttribute("placeholder").contains("stock tickers"),
						"Money- Header Search text displayed" + WrapperMethods.getWebElement(HomePage.moneySearchBoxText()).getAttribute("placeholder"),
						"Money- Header Search text is displayed");

			} else if (url.contains("style")) {
				WrapperMethods.waitForPageLoaded();
				UMReporter.log(LogStatus.INFO,"<B>Launched Style Page</B>");
				WrapperMethods.isDisplayed(HomePage.styleSearchBoxText(), "Search Text Box is displayed",
						"Search Text Box is not displayed");
				WrapperMethods.click(HomePage.styleSearchBoxText(), "Style Search Box Text is clicked", "Style Search Box Text is not clicked");

			} else if (url.contains("travel")) {
				WrapperMethods.waitForPageLoaded();
				UMReporter.log(LogStatus.INFO,"<B>Launched Travel Page</B>");
				searchHamburger(url);

				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder").contains("Search CNN travel"),
						"Header Default Search text displayed" + WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder"),
						"Header Default Search text is not displayed");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder").contains("Search CNN travel"),
						"Footer Default Search text displayed" + WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder"),
						"Footer Default Search text is not displayed");

			} else if (url.contains("sport")) {
				WrapperMethods.waitForPageLoaded();
				HomePage.wait(10);
				UMReporter.log(LogStatus.INFO,"<B>Launched Sport Page</B>");
				searchHamburger(url);

				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder").contains("Search CNN sport"),
						"Header Default Search text displayed" + WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder"),
						"Header Default Search text is not displayed");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder").contains("Search CNN sport"),
						"Footer Default Search text displayed" + WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder"),
						"Footer Default Search text is not displayed");

			} else if (url.contains("entertainment")) {
				WrapperMethods.waitForPageLoaded();
				HomePage.wait(10);
				UMReporter.log(LogStatus.INFO,"<B>Launched entertainment Page</B>");

				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder").contains("Search CNN entertainment..."),
						"Header Default Search text displayed" + WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder"),
						"Header Default Search text is not displayed");

			} else {
				WrapperMethods.waitForPageLoaded();
				HomePage.wait(10);
				searchHamburger(url);

				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder").contains("Search CNN"),
						"Header Default Search text displayed" + WrapperMethods.getWebElement(HomePage.headerSearchBoxText()).getAttribute("placeholder"),
						"Header Default Search text is not displayed");
				WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder").contains("Search CNN"),
						"Footer Default Search text displayed" + WrapperMethods.getWebElement(HomePage.footerSearchBoxText()).getAttribute("placeholder"),
						"Footer Default Search text is not displayed");

			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the search page elements");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}

	}
	public static void searchHamburger(String url) {
		WrapperMethods.isDisplayed(HomePage.hamburgerMenu(), "Hamburger Menu is displayed",
				"Hamburger Menu is not displayed");

		WrapperMethods.clickJavaScript(HomePage.hamburgerMenu(),"Clicked HamburgerMenu","Not able to click the HamburgerMenu");
		MethodDef.explicitWaitVisibilityNoError(HomePage.hamburgerMenuItems(), 10, "After clicked the HamburgerMenu Items are displayed","After clicked the HamburgerMenu Items are not displayed");

		WrapperMethods.isDisplayed(HomePage.hamburgerMenuItems(), "Hamburger Menu Items are displayed",
				"Hamburger Menu Items are not displayed");

		WrapperMethods.isDisplayed(HomePage.headerSearchBoxText(), "Header Search Text Box is displayed",
				"Header Search Text Box is not displayed");

		WrapperMethods.isDisplayed(HomePage.footerSearchBoxText(), "Footer Search Text Box is displayed",
				"Footer Search Text Box is not displayed");

		WrapperMethods.click(HomePage.searchButton(),"Search Button is clicked","Search Button is not able to click");
	}

	public static void validateOutbrainOb(String value, String datacut) {
		try {
			WrapperMethods.assertIsTrue(HomePage.OutbrainOb().size() != 0, "The OutBrain Holder is available",
					"The Outbrain Holder is not present " + HomePage.OutbrainOb().size());
			UMReporter.log(LogStatus.INFO,value);
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.OutBrainclass()).getAttribute("class").trim().equalsIgnoreCase(value.trim()),
					"The Outbrain class is as expected",
					"The Outbrain class is not as expected " + WrapperMethods.getWebElement(HomePage.OutBrainclass()).getAttribute("class"));
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(HomePage.outbrainimagedatacut()).getAttribute("data-cut-format").equalsIgnoreCase(datacut),
					"The data cut format is as expected", "The data cut format is not as expected "
							+ WrapperMethods.getWebElement(HomePage.outbrainimagedatacut()).getAttribute("data-cut-format"));
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the OutbrainOb elements");
			UMReporter.log(LogStatus.INFO,e.getMessage());
		}
	}
}