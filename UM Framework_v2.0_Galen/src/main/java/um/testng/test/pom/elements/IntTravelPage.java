package um.testng.test.pom.elements;

import org.openqa.selenium.By;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class IntTravelPage extends BasePage {
	
	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}
	public static By tripadvsr() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-partner-hotels__wrapper");
	}

	public static By tripadvsrImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-partner-hotels__wrapper img");
	}
	
	public static By gettitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='container']//a[@title='CNN - Partner Hotels']");
	}
	
	public static By getHotelCol() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='four columns header-find']");
	}
	
	public static By getTravelArticle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='eight columns latest-news-box']");
	}

}
