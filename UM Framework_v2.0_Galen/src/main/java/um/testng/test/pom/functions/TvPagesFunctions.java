package um.testng.test.pom.functions;

import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.TvPages;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class TvPagesFunctions {

	
	public static void showsValidations() {
		try {
			UMReporter.log(LogStatus.INFO,"<b>INFO: Show Page Elements</b>");

			for (WebElement elem : TvPages.getShowImages()) {
				WrapperMethods.assertIsTrue(elem.isDisplayed(), "the show has the image",
						"the show doesnt have the image");
			}
			for (WebElement elem : TvPages.getShowImagesHref()) {
				WrapperMethods.assertIsTrue( elem.isDisplayed(),
						"the show  image has the href and the image href is: " + elem.getAttribute("href"),
						"the show image doesnt have the href");
			}
			for (WebElement elem : TvPages.getShowHeadline()) {
				WrapperMethods.assertIsTrue(elem.isDisplayed(),
						"the show has the headline and the headline is :   " + elem.getText(),
						"the show doesnt have the headline");
			}
			for (WebElement elem : TvPages.getShowHeadlineHref()) {
				WrapperMethods.assertIsTrue(elem.isDisplayed(),
						"the show headline has the href and the href is :   " + elem.getAttribute("href"),
						"the show headline doesnt have the href");
			}
			int showcount = TvPages.getShowImages().size();
			int showheadlinecount = TvPages.getShowHeadline().size();

			WrapperMethods.assertIsTrue(showcount == showheadlinecount,
					"the no.of shows and the headlines are equal" + "to no.of overlays",
					"there is a mismatch in the shows and the headlines");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the show elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}
	
	public static void scheduleDDValidations() {
		UMReporter.log(LogStatus.INFO,"<b>Info: TV Schedule Page Elements</b>");
		WrapperMethods.isDisplayed(TvPages.getTVScheduleTitle(),
				"The title is displayed:  " + WrapperMethods.getWebElement(TvPages.getTVScheduleTitle()).getText(), "The title is not displayed");
		WrapperMethods.isDisplayed(TvPages.getTVScheduleMorningLink(), "Morning link is displayed",
				"Morning link is not displayed");
		WrapperMethods.isDisplayed( TvPages.getTVScheduleAfternoonLink(), "Afternoon link is displayed", "Afternoon link is not displayed");
		WrapperMethods.isDisplayed( TvPages.getTVScheduleEveningLink(), "Evening link is displayed",
				"Evening link is not displayed");
		WrapperMethods.isDisplayed( TvPages.getTVScheduleOvernightLink(), 
				"Overnight link is displayed", "Overnight link is not displayed");
		WrapperMethods.isDisplayed( TvPages.getTVScheduleDayDropDown(),
				"TV Schedule Day Dropdown is displayed", "TV Schedule Day Dropdown is not displayed");

		WrapperMethods.click(TvPages.getTVScheduleDayDropDown(),"Clicked the TV schedule Day drop down","Not able to click the TV schedule Day drop down");
		UMReporter.log(LogStatus.INFO,"<b>Drop down clicked...</b>");
		for (int i = 0; i <TvPages.weekdays().size(); i++) {
			String dayname = TvPages.weekdays().get(i).getText();
			int count = 0;
			for (int j = 0; j < TvPages.weekdays().size(); j++) {

				if (dayname.equals(TvPages.weekdays().get(j).getText())) {
					count++;
				}
			}
			if (count > 1) {
				UMReporter.log(LogStatus.INFO,"<b>Day is displayed twice</b>");
				WrapperMethods.isDispWE( TvPages.weekdays().get(i), "Day is displayed once",
						"Day is displayed more than once" + TvPages.weekdays().get(i).getText());
			} else {
				WrapperMethods.isDispWE(TvPages.weekdays().get(i),
						"Day list item is displayed once." + TvPages.weekdays().get(i).getText(),
						"Day is displayed more than once" + TvPages.weekdays().get(i).getText());
			}
		}
	}
	public static void RedirectOldBourdainPage(String Redirectpage) {

		/*
		 * WrapperDef.assertIsTrue(error,
		 * go.getCurrentUrl().equalsIgnoreCase(Redirectpage),
		 * "The Old Bourdain page is redirected to the correct page as expected"
		 * ,
		 * "The old bourdain page is not redirect tothe correct page - Unexpected "
		 * + go.getCurrentUrl());
		 */

		WrapperMethods.assertIsTrue(DriverFactory.getCurrentDriver().getCurrentUrl().contains(Redirectpage),
				"The Old Bourdain page is redirected to the correct page as expected",
				"The old bourdain page is not redirect tothe correct page - Unexpected " + DriverFactory.getCurrentDriver().getCurrentUrl());

	}
}
