package um.testng.test.pom.functions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.pom.elements.BasePage;
import um.testng.test.pom.elements.IntTravelPage;
import um.testng.test.pom.elements.VideoLandingPage;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class CommomPageFunctions {

	public static void testVideosDesktop() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 15);

		if (System.getProperty("edition").equalsIgnoreCase("DOM")) {
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.cssSelector("div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-next']")));
				WebElement lnknext = VideoLandingPage.getVideoCarouselNext();
				WebElement lnkprev = VideoLandingPage.getVideoCarouselPrev();
				if (lnknext.isDisplayed() == true && lnkprev.isDisplayed() == true) {
					try {
						if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
								&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
							actions.moveToElement(lnknext).click().perform();
						else
							lnknext.click();
						UMReporter.log(LogStatus.PASS, "Carousel links in Video Player is accessible");
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Carousel links in Video Player is not accessible");
					}
				} else {
					UMReporter.log(LogStatus.FAIL, "Carousel is not starting with first video in landing page");
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Error in the Carousel elements in video landing page elements");
			}
		}
	}

	public static void testVideosSocialIcons() {

		try {
			UMReporter.log(LogStatus.INFO, "Testing social icons");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIcons(), "gigya social bar");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem1(), "gigya social element 1");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem2(), "gigya social element 2");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem3(), "gigya social element 3");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem4(), "gigya social element 4");

			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
				WrapperMethods.click(VideoLandingPage.getVideoSocialIconsElem4(), "gigya share icon");
				MethodDef.explicitWaitVisibility(VideoLandingPage.getVideoShare(), "share block is visible after click",
						"share block is not visible after click");
				WrapperMethods.verifyElement(VideoLandingPage.getVideoShare(), "share block");

				WrapperMethods.click(VideoLandingPage.getVideoShareClose(), "share block close");
				UMReporter.log(LogStatus.PASS, "Video Share elements passed");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Video Share elements");
		}
	}

	public static void basicLandingPageElements() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		Boolean vidPlayer = false;
		WebDriverWait wait = new WebDriverWait(driver, 15);

		wait.until(ExpectedConditions.visibilityOf(VideoLeafPage.getVideoPlayElement()));
		vidPlayer = true;

		if (vidPlayer)
			UMReporter.log(LogStatus.PASS, "Video Player is present in the page");
		else {
			UMReporter.log(LogStatus.FAIL, "Video Player is not present in the page");
		}

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {

			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHead(), "Video Title");

			WrapperMethods.assertIsTrue(
					VideoLeafPage.getVideoSource().getText().contains("Source: ")
							&& VideoLeafPage.getVideoSource().getText().length() > 8,
					"Source Element is present and contains text",
					"Source Element is not present / doesn't contains text");
		} else {
			MethodDef.explicitWaitVisibility(VideoLeafPage.getVideoHeadDvc(), "Video header DVC is present",
					"Video header DVC is not present");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHeadDvc(), "Video Title");

			WrapperMethods.assertIsTrue(
					VideoLeafPage.getVideoSrcdvc().getText().contains("Source: ")
							&& VideoLeafPage.getVideoSrcdvc().getText().length() > 8,
					"Source Element is present and contains text",
					"Source Element is not present / doesn't contains text");

		}
		/*
		 * error.assertTrue(leaf.getVideoDate(go).getText()
		 * .startsWith("Added on ") && leaf.getVideoDate(go).getText().length()
		 * > 35, "Date added Element is not present / doesn't contains text",
		 * "Date added Element is present and contains text");
		 */
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				MethodDef.ElementisClickable(VideoLeafPage.getVideoEmbed());
				UMReporter.log(LogStatus.PASS, "Embed Video is clickable");
			} catch (Exception E) {
				UMReporter.log(LogStatus.WARNING, "Error in accessing Embed Element - Check manually");
			}
		}

	}

	public static void clickReadMore() {
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			Boolean read = MethodDef.getJsonReadMore();
			if (read) {
				try {
					WrapperMethods.clickJavaScript(ArticlePage.readMore());
					ArticlePage.wait(5);
					UMReporter.log(LogStatus.INFO, "Read More is clicked");
				} catch (Exception E) {
					UMReporter.log(LogStatus.INFO, "Read More is not clicked");
				}
			}
		}
	}

	public static void checkSectionDropDown(By currentsectionDropdown, By currentSubsection) {

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Mobile")) {
			try {
				WrapperMethods.verifyElement(currentsectionDropdown, "DropDown Icon");

				WrapperMethods.verifyElement(currentsectionDropdown, "DropDown Icon");
				MethodDef.explicitWaitVisibility(currentSubsection, "SubSection is present",
						"SubSection is not present");
				WrapperMethods.verifyElement(currentsectionDropdown, "DropDown Icon");
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error accessing the Drop Down Section");
			}
		}
	}

	public static void validateFeedbackForm() {
		try {
			WrapperMethods.containsText(BasePage.feedbackName(), "Your Name (required)",
					"Label: Feedback Name is present", "Label: Feedback Name is not present");

			WrapperMethods.containsText(BasePage.feedbackEmail(), "Your Email (required)",
					"Label: Feedback Email is present", "Label: Feedback Email is not present");
			WrapperMethods.containsText(BasePage.feedbackThoughts(), "Initial Thoughts (required)",
					"Label: Feedback Thoughts is present", "Label: Feedback Thoughts is not present");
			WrapperMethods.containsText(BasePage.feedbackAddComments(), "Additional Comments",
					"Label: Feedback Additional comments is present", "Label: Additional comments is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Labels in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

		try {
			WrapperMethods.verifyElement(BasePage.feedbackNameInput(), "Form Input: Feedback Name");
			WrapperMethods.verifyElement(BasePage.feedbackEmailInput(), "Form Input: Feedback Email");
			WrapperMethods.verifyElement(BasePage.feedbackThoughtsInput(), "Form Input: Feedback Thoughts");
			WrapperMethods.verifyElement(BasePage.feedbackCommentsInput(), "Form Input: Feedback Comments");
			WrapperMethods.verifyElement(BasePage.feedbackSubmit(), "Form Input: Feedback Submit form");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void submitFeedbackFormMand() {
		try {
			WrapperMethods.clickJavaScript(BasePage.feedbackSubmit());
			if (!ConfigProvider.getConfig("Browser").startsWith("IE")) {
				WrapperMethods.contains_Text(BasePage.feedbackNameWarn().get(0), "Your name is required",
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.contains_Text(BasePage.feedbackEmailWarn().get(0), "A valid email address is required",
						"Label: Feedback Email Warning (mandatory validation) is present",
						"Label: Feedback Email Warning (mandatory validation) is not present");
				WrapperMethods.contains_Text(BasePage.feedbackThoughtsWarn().get(0), "Your thoughts are required",
						"Label: Feedback Thoughts Warning (mandatory validation) is present",
						"Label: Feedback Thoughts Warning (mandatory validation) is not present");
			} else {
				WrapperMethods.assertIsTrue(BasePage.feedbackNameWarn().size() != 0,
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.assertIsTrue(BasePage.feedbackEmailWarn().size() != 0,
						"Label: Feedback Email Warning (mandatory validation) is present",
						"Label: Feedback Email Warning (mandatory validation) is not present");
				WrapperMethods.assertIsTrue(BasePage.feedbackThoughtsWarn().size() != 0,
						"Label: Feedback Thoughts Warning (mandatory validation) is present",
						"Label: Feedback Thoughts Warning (mandatory validation) is not present");

			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	private static void feedbackForm(String email) {

		WrapperMethods.enter_Text(BasePage.feedbackNameInput(), "TEST NAME");
		WrapperMethods.enter_Text(BasePage.feedbackEmailInput(), email);
		WrapperMethods.enter_Text(BasePage.feedbackThoughtsInput(), "Sample Thoughts");
		WrapperMethods.clickJavaScript(BasePage.feedbackSubmit());

	}

	private static void feedbackFormsubmtVal() {
		MethodDef.explicitWaitVisibility(BasePage.feedbackConfirm(),
				"Feedback Submit Confirmation Message is displayed",
				"Feedback Submit Confirmation Message is not displayed");
		/*
		 * ArrayList<?> parentAttributes = (ArrayList<?>) ((JavascriptExecutor)
		 * go) .executeScript(
		 * "var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;"
		 * , page.feedbackSubmit()); System.out.println(parentAttributes);
		 */

		WrapperMethods.contains_Text(BasePage.feedbackConfirm(),
				"Thank you for your feedback. It has been submitted successfully.", "Submit is successful",
				"Submit is not successful");
		/*
		 * parentAttributes.clear(); parentAttributes = (ArrayList<?>)
		 * ((JavascriptExecutor) go) .executeScript(
		 * "var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;"
		 * , page.feedbackSubmit()); System.out.println(parentAttributes);
		 */
	}

	public static void submitFeedbackForm() {
		try {
			feedbackForm("test@test.com");
			feedbackFormsubmtVal();

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void submitFeedbackEmail() {
		try {
			feedbackForm("test");
			WrapperMethods.contains_Text(BasePage.feedbackEmailWarn().get(0), "A valid email address is required",
					"Feedback: Email Warning (valid email validation) is present",
					"Feedback: Email Warning (valid email validation) is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void feedbackForm360Details() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String document = (String) executor
					.executeScript("return document.getElementsByTagName('html')[0].innerHTML");

			if (document.contains("<meta content=\"AC360 Feedback\""))
				UMReporter.log(LogStatus.PASS, "MetaContent is correct");

			else
				UMReporter.log(LogStatus.FAIL, "MetaContent is wrong");

			WrapperMethods.assertIsTrue(
					driver.getTitle().equals("AC360 Feedback CNN - Videos, Pictures, and News - CNN.com"),
					"Page title is correct", "Page title is wrong");

			WrapperMethods.assertIsTrue(BasePage.feedbackTitle().getText().equals("Anderson Cooper 360"),
					"Feedback form Title is correct", "Feedback form title is wrong");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void submitFeedbackForm360() {
		try {
			feedbackForm360("test@test.com");
			feedbackForm360submtVal();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	private static void feedbackForm360(String email) {
		WrapperMethods.enter_Text(BasePage.feedbackNameInputac360(), "TEST NAME");
		WrapperMethods.enter_Text(BasePage.feedbackEmailInputac360(), email);
		WrapperMethods.enter_Text(BasePage.feedbackCommentsInputac360(), "Sample Comments");
		WrapperMethods.clickJavaScript(BasePage.feedbackSendInputac360());
	}

	private static void feedbackForm360submtVal() {
		MethodDef.explicitWaitVisibility(BasePage.feedbackConfirm(),
				"Feedback Submit Confirmation Message is displayed",
				"Feedback Submit Confirmation Message is not displayed");
		WrapperMethods.contains_Text(BasePage.feedbackConfirm(),
				"Thank you for your feedback. It has been submitted successfully.", "Submit is successful",
				"Submit is not successful");
	}

	public static void submitFeedbackFormMand360() {
		try {
			WrapperMethods.clickJavaScript(BasePage.feedbackSendInputac360());
			WrapperMethods.contains_Text(BasePage.feedbackNameWarn360(), "Your name is required",
					"Label: Feedback Name Warning (mandatory validation) is present",
					"Label: Feedback Name Warning (mandatory validation) is not present");
			WrapperMethods.contains_Text(BasePage.feedbackEmailWarn360(), "A valid email address is required",
					"Label: Feedback Email Warning (mandatory validation) is present",
					"Label: Feedback Email Warning (mandatory validation) is not present");
			WrapperMethods.contains_Text(BasePage.feedbackCommentWarn360(), "Your comment is required",
					"Label: Feedback comment Warning (mandatory validation) is present",
					"Label: Feedback comment Warning (mandatory validation) is not present");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void submitFeedbackEmail360() {
		try {
			feedbackForm360("test");
			WrapperMethods.contains_Text(BasePage.feedbackEmailWarn360(), "A valid email address is required",
					"Feedback:  Email Warning (valid email validation) is present",
					"Feedback: Email Warning (valid email validation) is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void submitFeedbackFormMandStud() {
		try {
			WrapperMethods.clickJavaScript(BasePage.feedbackSendInputStud());
			if (!ConfigProvider.getConfig("Browser").startsWith("IE")) {
				WrapperMethods.contains_Text(BasePage.feedbackNameWarnStud().get(0), "Your name is required",
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.contains_Text(BasePage.feedbackSchoolWarnStud().get(0), "Your school name is required",
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.contains_Text(BasePage.feedbackEmailWarnStud().get(0),
						"A valid email address is required",
						"Label: Feedback Email Warning (mandatory validation) is present",
						"Label: Feedback Email Warning (mandatory validation) is not present");
				WrapperMethods.contains_Text(BasePage.feedbackCommentWarnStud().get(0), "Your comment is required",
						"Label: Feedback comment Warning (mandatory validation) is present",
						"Label: Feedback comment Warning (mandatory validation) is not present");
			} else {
				WrapperMethods.assertIsTrue(BasePage.feedbackNameWarnStud().size() != 0,
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.assertIsTrue(BasePage.feedbackSchoolWarnStud().size() != 0,
						"Label: Feedback Name Warning (mandatory validation) is present",
						"Label: Feedback Name Warning (mandatory validation) is not present");
				WrapperMethods.assertIsTrue(BasePage.feedbackEmailWarnStud().size() != 0,
						"Label: Feedback Email Warning (mandatory validation) is present",
						"Label: Feedback Email Warning (mandatory validation) is not present");
				WrapperMethods.assertIsTrue(BasePage.feedbackCommentWarnStud().size() != 0,
						"Label: Feedback comment Warning (mandatory validation) is present",
						"Label: Feedback comment Warning (mandatory validation) is not present");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void feedbackFormStudDetails() {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String document = (String) executor
					.executeScript("return document.getElementsByTagName('html')[0].innerHTML");

			if (document.contains("<meta content=\"CNN10 Feedback\""))
				UMReporter.log(LogStatus.PASS, "MetaContent is correct");
			else
				UMReporter.log(LogStatus.FAIL, "MetaContent is wrong");

			WrapperMethods.assertIsTrue(
					driver.getTitle().equals("CNN10 Feedback CNN - Videos, Pictures, and News - CNN.com"),
					"Page title is correct", "Page title is wrong");

			WrapperMethods.assertIsTrue(BasePage.feedbackTitle().getText().equals("CNN10"),
					"Feedback form Title is correct", "Feedback form title is wrong");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	private static void feedbackFormStud(String email) {
		WrapperMethods.enter_Text(BasePage.feedbackNameInputStud(), "TEST NAME");
		// Reporter.log("Name is entered: " +
		// BasePage.feedbackNameInputStud().getText());
		WrapperMethods.enter_Text(BasePage.feedbackSchoolInputStud(), "School NAME");
		WrapperMethods.enter_Text(BasePage.feedbackEmailInputStud(), email);
		WrapperMethods.enter_Text(BasePage.feedbackCommentsInputStud(), "Sample Comments");
		WrapperMethods.clickJavaScript(BasePage.feedbackRaido_btn());
		WrapperMethods.clickJavaScript(BasePage.feedbackSendInputStud());
	}

	private static void feedbackFormStudsubmtVal() {
		MethodDef.explicitWaitVisibility(BasePage.feedbackConfirm(),
				"Feedback Submit Confirmation Message is displayed",
				"Feedback Submit Confirmation Message is not displayed");
		WrapperMethods.contains_Text(BasePage.feedbackConfirm(),
				"Thank you for your feedback. It has been submitted successfully.", "Submit is successful",
				"Submit is not successful");
	}

	public static void submitFeedbackFormStud() {
		try {
			feedbackFormStud("test@test.com");
			feedbackFormStudsubmtVal();
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void submitFeedbackEmailStud() {
		try {
			feedbackFormStud("test");
			WrapperMethods.contains_Text(BasePage.feedbackEmailWarnStud().get(0), "A valid email address is required",
					"Feedback: Email Warning (valid email validation) is present",
					"Feedback: Email Warning (valid email validation) is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the feedback Inputs in form information");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void stormTrackerValidations() {
		try {
			WrapperMethods.verifyElement(BasePage.getRadarWarnings(), "the Radar warnings button");
			try {
				WrapperMethods.clickJavaScript(BasePage.getRadarWarnings());
			} catch (Exception e) {
			}
			WrapperMethods.verifyElement(BasePage.getRadarWatches(), "the Radar watches button");
			WrapperMethods.verifyElement(BasePage.getRadarForecast(), "the Radar Forecast button");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Storm tracker validations");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void tripAdvisorVal() {
		try {
			WebDriver go = DriverFactory.getCurrentDriver();
			UMReporter.log(LogStatus.INFO, "Travel page launched");
			WrapperMethods.isDisplayed(IntTravelPage.tripadvsr(), "Trip Advisor image is displayed",
					"Trip Advisor image is not displayed");
			WrapperMethods.assertIsTrue(go.getCurrentUrl().contains("edition"),
					"Trip Advisor image is displayed in International edition",
					"Trip Advisor image is displayed in International edition");
			WrapperMethods.click(IntTravelPage.tripadvsrImg(), "Trip Advisor Placeholder_Cnnpartners Image");
			IntTravelPage.wait(5);
			MethodDef.explicitWaitVisibility(IntTravelPage.gettitle(), 20, "wait untill the page loads",
					"page is not loaded");
			WrapperMethods.assertIsTrue(go.getCurrentUrl().contains("http://www.cnnpartners.com/"),
					"After clicked the Cnn partner image in travel page, Correct url opened - " + go.getCurrentUrl(),
					"After clicked the Cnn partner image in travel page, Correct url is not opened - "
							+ go.getCurrentUrl());
			WrapperMethods.isDisplayed(IntTravelPage.getHotelCol(), "Find a hotel column is displayed",
					"Find a hotel column is not displayed");
			WrapperMethods.isDisplayed(IntTravelPage.getTravelArticle(), "Latest CNN.com Travel Articles are displayed",
					"Latest CNN.com Travel Articles are not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing cnn partner page element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}
	public static void testHp10Element() {
		try {
			UMReporter.log(LogStatus.INFO,"TEST");
			WrapperMethods.isDisplayed(BasePage.hp10Header(), "the branding for the Header is displayed",
					"the branding for the Header is not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreview(), "the branding for the Preview is displayed",
					"the branding for the Preview is not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewPlayButton(),
					"the branding for the Preview Play is displayed",
					"the branding for the Preview Play is not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewHeadline(),
					"the branding for the Headline is displayed", "the branding for the Headline is not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the HP10 element");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}

	public static void testHp10ElementPlay() {
		try {
			UMReporter.log(LogStatus.INFO,"TEST");
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button is clicked","Not able to click the HP10 FreePreview Play Button");

			MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), 30,"HP10 FreePreview Spinner is visible","HP10 FreePreview Spinner is not visible");

			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewSpinner(), "the HP 10 Spinner is displayed",
					"the HP 10 Spinner is not displayed");
			MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewLinkImage(), 60,"HP10 FreePreview Link Image is visible","HP10 FreePreview Link Image is not visible");

			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkImage(), "the Experience Image is displayed",
					"the Experience Image is not displayed");

			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkText(), "the Experience text is displayed",
					"the Experience text is not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkArrow(), "the Experience Arrow is displayed",
					"the Experience Arrow is not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCountDownSpan(),
					"the  Count Down text is displayed", "the  Count Down text not displayed");
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCountDown(), "the  Count Down time is displayed",
					"the  Count Down time is not displayed");

			WrapperMethods.moveToElement(BasePage.hp10FreePreviewLinkImage());
			WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCloseButton(),
					"Close Button is displayed on hover", "Close button is not displayed on hover");
		} catch (Exception E) {
			
			UMReporter.log(LogStatus.FAIL, "error in accessing the HP10 element");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}

	public static void testHp10ElementPlayLogin() {
		try {
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button");
			WrapperMethods.clickJavaScript(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button is clicked","HP10 FreePreview Play Button is not clicked");
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button");
			WrapperMethods.clickJavaScript(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button is clicked","HP10 FreePreview Play Button is not clicked");
			BasePage.wait(5);
			if (BasePage.getLoginTxt1().size() >= 1) {
				WrapperMethods.isDisplayed(BasePage.getOptimum(), "Optimum is displayed",
						"Optimum is not displayed");
				WrapperMethods.clickJavaScript(BasePage.getOptimum(),"GetOptimum is clicked","GetOptimum is not clicked");
				WrapperMethods.click(BasePage.getOptimum(),"GetOptimum is clicked","GetOptimum is not clicked");
				BasePage.wait(30);
				WrapperMethods.sendkeys(BasePage.idtxt(),"research1001","Entered the ID - ","Not able to enter the ID - ");
				BasePage.wait(2);
				WrapperMethods.sendkeys(BasePage.idtPass(),"support1001","Entered the Password - ","Not able to enter the Password - ");
				BasePage.wait(2);
				WrapperMethods.click(BasePage.signin(),"Sign in is clicked","Sign in is not clicked");
				BasePage.wait(2);
				DriverFactory.getCurrentDriver().navigate().to("http://www.cnn.com/specials/hp10");
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewLinkImage(),"HP10 FreePreview Link Image is visible","HP10 FreePreview Link Image is not visible");
				UMReporter.log(LogStatus.INFO,"TESING Play");
				WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10  FreePreview Play Button is clicked","HP10  FreePreview Play Button is not clicked");
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), "HP10 FreePreview Spinner is visible","HP10 FreePreview Spinner is not visible");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewSpinner(), "the HP 10 Spinner is displayed",
						"the HP 10 Spinner is not displayed");
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewLinkImage(), "HP10Free Preview Link Image is visible","HP10 FreePreview Link Image is not visible");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkImage(),
						"the Experience Image is displayed", "the Experience Image is not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkText(), "the Experience text is displayed",
						"the Experience text is not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkArrow(),
						"the Experience Arrow is displayed", "the Experience Arrow is not displayed");
				WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10 Free Preview Play Button is visible","HP10 Free Preview Play Button is not visible");

			} else {
				UMReporter.log(LogStatus.INFO,"Without login any provider can able to watch 10 minutes of video.");
				UMReporter.log(LogStatus.INFO,"TESING Play");
				WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"HP10 FreePreview Play Button is visible","HP10 FreePreview Play Button is not visible");
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), "HP10 FreePreview Spinner is visible","HP10 FreePreview Spinner is not visible");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewSpinner(), "the HP 10 Spinner is displayed",
						"the HP 10 Spinner is not displayed");
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewLinkImage(),"HP10 FreePreview Link Image is visible","HP10 FreePreview Link Image is not visible");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkImage(),
						"the Experience Image is displayed", "the Experience Image is not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkText(), "the Experience text is displayed",
						"the Experience text is not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewLinkArrow(),
						"the Experience Arrow is displayed", "the Experience Arrow is not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCountDownSpan(),
						"the  Count Down text is displayed", "the  Count Down text not displayed");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCountDown(),
						"the  Count Down time is displayed", "the  Count Down time is not displayed");
				WrapperMethods.moveToElement(BasePage.hp10FreePreviewLinkImage());
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewCloseButton(),
						"Close Button is displayed on hover", "Close button is not displayed on hover");
			}

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL,"error in accessing the HP10 element");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	public static void testHp10VideoOpensInSameBrowser() {
		try {
			UMReporter.log(LogStatus.INFO,"TESING Play");
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(), "HP10 Free Preview Play Button is clicked", "HP10 Free Preview Play Button is not clicked");
			if (ConfigProvider.getConfig("Platform").equals("Desktop")) {
				MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), "HP10 Free Preview Spinner is visible","HP10 Free Preview Spinner is not visible");
				WrapperMethods.isDisplayed(BasePage.hp10FreePreviewSpinner(), "the HP 10 Spinner is displayed",
						"the HP 10 Spinner is not displayed");
			} else {

				BasePage.wait(10);

				String url = DriverFactory.getCurrentDriver().getCurrentUrl();

				WrapperMethods.assertIsTrue( url.equals("http://smartlink.cnn.com/go"),
						"By clicking watch live tv,the video opens in a new site and the new site url is :" + url,
						"By clicking watch live tv,the video didnt open in the new tab");

				WrapperMethods.isDisplayed(BasePage.hp10VideoInMobile(), "the HP 10 Spinner is displayed",
						"the HP 10 Spinner is not displayed");

				WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(BasePage.hp10VideoInMobile()).getAttribute("href").isEmpty(),
						"the HP 10 video in mobile links to APP url and the app url is : "
								+ WrapperMethods.getWebElement(BasePage.hp10VideoInMobile()).getAttribute("href"),
						"the HP 10 video in mobile doesn't link to APP url");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the HP10 element" );
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
	public static void pinnedVidLocation() {

		WebElement pinned = WrapperMethods.getWebElement(BasePage.getVideoElementPinned());

		MethodDef.explicitWaitVisibility(BasePage.getVideoElementPinned(), 40, "VideoElementPinned is visible","VideoElementPinned is not visible");
		UMReporter.log(LogStatus.INFO,"<B>Validating the CSS Property controlling location of the pinned video</B>");
		if (pinned.isDisplayed()) {
			WrapperMethods.assertIsTrue(pinned.getCssValue("width").equals("300px"), "width is correct",
					"width is wrong");
			WrapperMethods.assertIsTrue(pinned.getCssValue("height").equals("168.75px"), "height is correct",
					"height is wrong");
			WrapperMethods.assertIsTrue( pinned.getCssValue("left").contains("px"), "left contains data",
					"left contains data");
			WrapperMethods.assertIsTrue(pinned.getCssValue("top").equals("60px"), "top is correct", "top is wrong");
			WrapperMethods.assertIsTrue( pinned.getCssValue("position").equals("fixed"), "position is correct",
					"position is wrong");
		}
	}

	public static void testHP10MinRefresh() {

		try {
			UMReporter.log(LogStatus.INFO,"TESING: Refresh of the page while the video is playing");
			WrapperMethods.click(BasePage.hp10FreePreviewPlayButton(),"hp10FreePreviewPlayButton is clicked","hp10FreePreviewPlayButton is not clicked");

			MethodDef.explicitWaitVisibility(BasePage.hp10FreePreviewSpinner(), 30,"hp10FreePreviewSpinner is visible","hp10FreePreviewSpinner is not visible");

			BasePage.wait(180);

			String url = DriverFactory.getCurrentDriver().getCurrentUrl();

			WrapperMethods.assertIsTrue(!url.contains("refresh"), "the Page  is not refreshed",
					"the pafe is refreshed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL,"error in accessing the HP10 element");
			UMReporter.log(LogStatus.INFO,E.getMessage());
		}
	}
}