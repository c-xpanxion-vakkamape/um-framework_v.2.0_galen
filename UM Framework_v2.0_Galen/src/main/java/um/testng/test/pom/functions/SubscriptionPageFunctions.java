package um.testng.test.pom.functions;

import org.apache.commons.lang3.RandomStringUtils;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class SubscriptionPageFunctions {

	public static void invalidEmailSubscription() {

		UMReporter.log(LogStatus.INFO, "Info: Testing MailSubscripton in the story");

		WrapperMethods.verifyElement(ArticlePage.getMailSubscripton(), " MailSubscripton ");
		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmail()).sendKeys("ahfshdfhsdfh");

		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmailSubmit()).click();
		WrapperMethods.verifyElement(ArticlePage.getMailSubscriptonError(), " MailSubscripton error message ");

	}

	public static void validEmailSubscription() {

		UMReporter.log(LogStatus.INFO, "Testing MailSubscripton in the story");

		WrapperMethods.verifyElement(ArticlePage.getMailSubscripton(), " MailSubscripton ");
		String random = RandomStringUtils.random(5, "ABFERPLD");

		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmail()).sendKeys(random + "@gmail.com");

		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmailSubmit()).click();

		if (WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonSuccess()).isDisplayed())
			WrapperMethods.verifyElement(ArticlePage.getMailSubscriptonSuccess(), " MailSubscripton success message ");

	}

	public static void existingEmailSubscription() {

		UMReporter.log(LogStatus.INFO, "Info: Testing MailSubscripton in the story");

		WrapperMethods.verifyElement(ArticlePage.getMailSubscripton(), " MailSubscripton ");
		// String random = RandomStringUtils.random(5, "ABFERPLD");

		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmail()).sendKeys("turnercnn1@gmail.com");

		WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonEmailSubmit()).click();

		if (WrapperMethods.getWebElement(ArticlePage.getMailSubscriptonSuccess()).isDisplayed())
			WrapperMethods.verifyElement(ArticlePage.getMailSubscriptonError(), " MailSubscripton error message ");

	}


}
