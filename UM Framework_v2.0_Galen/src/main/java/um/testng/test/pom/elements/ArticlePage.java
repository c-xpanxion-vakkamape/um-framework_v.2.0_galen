package um.testng.test.pom.elements;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class ArticlePage extends HomePage {

	public static By getTermsServiceConsentClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "user-msg--close");
	}
	
	public static By getVideoplayingg() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'has-started')]");
	}


	public static By getArticleAuthorBy() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "metadata__byline__author");
	}

	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "article[class^='pg-rail']");
	}

	public static By getArticleHeaderTitle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pg-headline");
	}

	public static By getArticleCont() {
		return WrapperMethods.locatorValue(Locators.CSS, "section#body-text");
	}

	public static By getNavColorStrip() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav__color-strip");
	}

	public static By getAuthorImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".static Byline-images > img");
	}

	public static By getAuthorImageSrc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata div.static Byline-images img");
	}

	public static By readMore() {
		return WrapperMethods.locatorValue(Locators.CSS, ".read-more-link");
	}

	public static By brokenPageErrorheadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error_headline");
	}

	public static By brokenPageErrorheadline2() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error_headline2");
	}

	public static By getAllPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__logo,.zn-header__text-page_header,.logo-style,div.media__video,.zn-header__text-page_header,.nav__logo,a#logo,.money-logo");
	}

	public static By getCurrentsection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__name > a");
	}

	public static By getCurrentsectionDropdown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__name span.nav-section__expand-icon");
	}

	public static By getCurrentSubsection() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-section-submenu");
	}

	public static By getstaticPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.nav__container #logo");
	}

	public static By getFBShareDesktop() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata .gigya-sharebar .gig-button-container-facebook");

	}

	public static By getTwShareDesktop() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata .gigya-sharebar .gig-button-container-twitter");

	}

	/**
	 * returns Facebook email text box in the Article Page
	 */
	public static By getFbEmail() {
		return WrapperMethods.locatorValue(Locators.CSS, "#email");

	}

	/**
	 * returns Facebook password text box in the Article Page
	 */
	public static By getFbPass() {
		return WrapperMethods.locatorValue(Locators.CSS, "#pass");

	}

	/**
	 * returns Facebook login button in the Article Page
	 */
	public static By getFbLogin() {
		return WrapperMethods.locatorValue(Locators.CSS, "#loginbutton");

	}

	/**
	 * returns Facebook Share Element in the Article Page
	 */
	public static By getFbShareElem() {
		return WrapperMethods.locatorValue(Locators.CSS, ".unclickableMask");

	}

	public static By getFbTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@name='share_action_properties']");

	}

	/**
	 * returns Twitter Content in the Article Page
	 */
	public static By getTwContent() {

		return WrapperMethods.locatorValue(Locators.CSS, "#status");
	}

	/**
	 * returns gigya Share Element4 in the Article Page
	 */
	public static By getGigyaBarElem4() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-share");

	}

	/**
	 * returns gigya Share Email Other Section in the Article Page
	 */
	public static By getGigyaBarOthers() {
		return WrapperMethods.locatorValue(Locators.CSS, ".gig-simpleShareUI-inner");

	}

	/**
	 * returns gigya Share Email Other links in the Article Page
	 */
	public static List<WebElement> getGigyaBarOtherLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".gig-simpleShareUI-button");
	}

	public static By getPageLoad(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "article[class^='pg-rail']");
	}

	public static By pageloadlogo(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "a#logo");
	}

	/**
	 * Page Load Condition
	 */
	public static By getPageLoadNew(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "article[class^='pg-special-article ']");
	}

	/**
	 * returns list of Article Links in the Article Page
	 */
	public List<WebElement> getArticleLinks() {

		return WrapperMethods.locateElements(Locators.CSS, "article[class^='cd cd--card cd--article cd--idx-']");

	}

	/**
	 * returns VideoElement in the Article Page
	 */
	public static By getVideoElement() {
		return WrapperMethods.locatorValue(Locators.ID, "large-media_0");
	}

	/**
	 * returns Video Play Button in the Article Page
	 */
	public static By getVideoPlayButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "#player-large-media_0 div.vjs-big-play-button");
	}

	/**
	 * returns Video Element Play button in the Article Page
	 */

	public static By getVideoElementPlay() {
		return WrapperMethods.locatorValue(Locators.CSS, "object[id^=cvp_cnnvideo_]");
	}

	/**
	 * returns Embedded Video Element in the Article Page
	 */

	public static By getEmVideoElement() {
		// return
		// WrapperMethods.locatorValue(Locators.CSS,".media__video--responsive");
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video--thumbnail");
	}

	/**
	 * returns Embedded Video Element Play button in the Article Page
	 */

	public static By getEmVideoElementPlayElem() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video--standard");
	}

	/**
	 * returns Embedded Video Element Play button in the Article Page
	 */

	public static By getEmVideoElementPlayElemOB() {
		// return
		// WrapperMethods.locatorValue(Locators.CSS,".media__video--responsive");
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video--thumbnail");
	}

	/**
	 * returns Embedded Video Element Close Link in the Article Page
	 */
	public static By getEmVideoElementPlayClose() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[class^='el__video__close']");
	}

	/**
	 * returns gigya Share Bar in the Article Page
	 */
	public static By getGigyaBar() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-share__rail-top>div[class='js-gigya-sharebar gigya-sharebar']");
	}

	/**
	 * returns gigya Share Element1 in the Article Page
	 */
	public static By getGigyaBarElem1() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-email");
	}

	/**
	 * returns gigya Share Element2 in the Article Page
	 */
	public static By getGigyaBarElem2() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-facebook");
	}

	/**
	 * returns gigya Share Element3 in the Article Page
	 */
	public static By getGigyaBarElem3() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-twitter");
	}

	/**
	 * returns gigya Share Element4 in the Article Page
	 */

	/**
	 * returns gigya Share Element5 in the Article Page
	 */
	public static By getGigyaBarElem5() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-messenger");
	}

	// These elements are located in left side of the page. Where as the above
	// elements are located at top of the page

	/**
	 * returns gigya Share Element1 in the Article Page - On scroll/page down
	 */
	public static By getGigyaBarElemski1() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_0-reaction0");
	}

	/**
	 * returns gigya Share Element2 in the Article Page - On scroll/page down
	 */
	public static By getGigyaBarElemski2() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_1-reaction0");
	}

	/**
	 * returns gigya Share Element3 in the Article Page - On scroll/page down
	 */
	public static By getGigyaBarElemski3() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_2-reaction0");
	}

	/**
	 * returns gigya Share Element4 in the Article Page - On scroll/page down
	 */
	public static By getGigyaBarElemski4() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_3-reaction0");
	}

	/**
	 * returns gigya Share Friends Email in the Article Page
	 */
	public static By getGigyaBarEmailFrd() {
		return WrapperMethods.locatorValue(Locators.CSS, "input[id*='_showShareUI_tbFriendsEmail']");
	}

	/**
	 * returns gigya Share Friends Email in the Article Page
	 */
	public static By getGigyaBarEmailMy() {
		return WrapperMethods.locatorValue(Locators.CSS, "input[id*='_showShareUI_tstatic ByourEmail']");
	}

	/**
	 * returns gigya Share Email Message in the Article Page
	 */
	public static By getGigyaBarEmailMsg() {
		return WrapperMethods.locatorValue(Locators.CSS, "textarea[id*='_showShareUI_tbMessage']");
	}

	/**
	 * returns gigya Share Email Text Header in the Article Page
	 */
	public static By getGigyaBarEmailTxtHeader() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"table[id*='_showShareUI_emailCanvas'] > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)");
	}

	/**
	 * returns gigya Share Email Text in the Article Page
	 */
	public static By getGigyaBarEmailText() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"table[id*='_showShareUI_emailCanvas'] > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(2)");
	}

	/**
	 * returns gigya Share Email Send button in the Article Page
	 */
	public static By getGigyaBarEmailSend() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"table[id*='_showShareUI_emailCanvas'] > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1)");
	}

	/**
	 * returns gigya Share Email Other Section in the Article Page
	 */

	/**
	 * returns Article Author in the Article Page
	 */
	public static By By() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "metadata__static Byline__author");
	}

	/**
	 * returns Article Time in the Article Page
	 */
	public static By getArticleTime() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container p.update-time");
	}

	// ARTICLE VIDEO PAGE ELEMENTS

	/**
	 * returns Article Video Button in the Article Page
	 */
	public static By getArticleVideoButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video__play-button");
	}

	/**
	 * returns Article Video Caption in the Article Page
	 */
	public static By getVideoCaption() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el__storyelement__header");
	}

	/**
	 * returns Article Video Time in the Article Page
	 */
	public static By getVideoTime() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el__storyelement__gray");
	}

	/**
	 * returns Article Video Source name in the Article Page
	 */
	public static By getVideoSourceName() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "metadata__source-name");
	}

	/**
	 * returns Article Video Auto Play Icon in the Article Page
	 */
	public static By getVideoPlayedAuto() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='large-media_0']");
	}

	/**
	 * returns Article Video Auto Play Icon in the Article Page
	 */
	public static By getVideoPlayed() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id=\"cvp_1\"]");
	}

	public static By getVideoPlayedPlay() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id=\"large-media_0\"]/div/a/div");
	}

	public static By getVideoPlayedEm() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[class='media__video--responsive'] object");
	}

	public static By getVideoPlayedEmVid() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[class='media__video--responsive'] video");
	}

	// ARTICLE STATIC PAGE ELEMENTS

	/**
	 * returns Article Static Image in the Article Page
	 */
	public static WebElement getArticleStaticImage() {
		return driver.findElement(By.xpath("//*[@id='large-media']/div/img"));
	}

	/**
	 * returns Article Video Collection Carousel Section in the Article Page
	 */
	public static By getArticleVideoCollCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[id*='cn-featured'] div.owl-stage");
	}

	// Prev Owl

	/**
	 * returns Article Video Collection Carousel Prev button in the Article Page
	 */
	public static By getArticleVideoCollPrevCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[id*='cn-featured'] div.owl-nav div.owl-prev");
	}

	// Next Owl

	/**
	 * returns Article Video Collection Carousel Next button in the Article Page
	 */
	public static By getArticleVideoCollNextCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[id*='cn-featured'] div.owl-nav div.owl-next");
	}

	/**
	 * returns Article Video Collection video Element in the Article Page
	 */
	public static By getArticleVideoCollVideoElem() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.carousel__content__item div.media");
	}

	/**
	 * 
	 * returns Article Video Collection title of the video element in the
	 * Article Page
	 */
	public static By getArticleVideoCollVideoElemTitle() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.carousel__content__item  div.cd__content span.cd__headline-text");
	}

	/**
	 * returns list of Article Video Collection video Elements in the Article
	 * Page
	 */

	public static By getArticleVideoCollVideoElements() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[id*='cn-featured'] div.owl-stage div.owl-item.active");
	}

	// Now Playing

	/**
	 * returns Article Video Collection Now Play Button in the Article Page
	 */
	public static By getArticleVideoCollNowPlay() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.media a div.media__over-text");
	}

	// Total Time item

	/**
	 * returns Article Video Collection Total Time in the Article Page
	 */
	public static By getArticleVideoCollTotalTime() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.media a i.media__icon[aria-hidden='true']:not([style])");
	}

	// Img Src

	/**
	 * returns Article Video Collection Image Source in the Article Page
	 */
	public static By getArticleVideoCollImgSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.media a img");
	}

	// Headlines

	/**
	 * returns Article Video Headline in the Article Page
	 */
	public static By getArticleVideoHeadLines() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='cn-featured'] div.owl-stage div.owl-item.active div.cd__content h3 a span.cd__headline-text");
	}

	/**
	 * returns Pull Quote Symbol in the Article Page
	 */
	public static By getPullQuoteSymbol() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pullquote > blockquote:nth-child(1)");
	}

	/**
	 * returns Pull Quote in the Article Page
	 */
	public static By getPullQuote() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pullquote__quote");
	}

	/**
	 * returns Pull Quote Author in the Article Page
	 */
	public static By getPullQuoteAuthor() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pullquote__author");
	}

	// Article Expandabale Image Elements

	/**
	 * returns Expandable Image in the Article Page
	 */
	public static By getExpandImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__image--expandable");
	}

	// Article Standard Image
	/**
	 * returns Standard Image in the Article Page
	 */
	public static By getStandardImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__image--standard");
	}

	// Arun: Article Instagram Image
	/**
	 * returns Instagram Image in the Article Page
	 */
	public static WebElement getInstagramImg() {
		return driver.findElement(By.id("instagram-embed-0"));

	}

	// Width Before expanding
	/**
	 * returns Width of Image Before expanding in the Article Page
	 */
	public static By getBeforeExpandImgWidth() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__image--expandable");
	}

	// Article image full width
	/**
	 * returns Full width Image in the Article Page
	 */
	public static By getImgFullWidth() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__image--fullwidth");
	}

	/**
	 * returns Full width Image Ref in the Article Page
	 */
	public static WebElement getImgFullWidthImg() {
		return driver.findElement(By.cssSelector(".el__image--fullwidth img"));
	}

	/**
	 * returns Expandable Image Before expanding in the Article Page
	 */
	public static WebElement getBeforeExpandImg() {
		return driver.findElement(By.cssSelector(".el__image--expandable img"));
	}

	// Width After expanding
	/**
	 * returns Width of Image After expanding in the Article Page
	 */
	public static WebElement getAfterExpandImgWidth() {
		return driver.findElement(By.cssSelector("div[style*='width: 100%'][class*='el__image--expandfull']"));
	}

	/**
	 * returns Expand Close button in the Article Page
	 */
	public static By getExpandClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el__image__close--expandable");
	}

	/**
	 * returns Image ref After expanding in the Article Page
	 */
	public static WebElement getAfterExpandImg() {
		return driver.findElement(By.cssSelector(".el__image--expandfull img"));
	}

	/**
	 * returns Image Title in the Article Page
	 */
	public static By getImgTitle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el__gallery_image-title");
	}

	/**
	 * returns Image width after close in the Article Page
	 */
	public static WebElement getImgWidthAfterClose() {
		return driver.findElement(By.cssSelector("div[style*='width: 300px'][class*='el__image--expandable']"));
	}

	// Article 3rd party elements
	/**
	 * returns Facebook Widget in the Article Page
	 */
	public static WebElement getFbWidget() {
		return driver.findElement(By.cssSelector("iframe[title='fb:post Facebook Social Plugin']"));

	}

	/**
	 * returns Twitter Widget Frame in the Article Page
	 */

	public static By getTwWidgetFrame() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.twembed.el-embed-twitter");
	}

	/*
	 * public static By getTwWidget() { return go().findElement( static
	 * By.cssSelector(".el-embed-twitter iframe.twitter-tweet"); }
	 */

	/**
	 * returns Twitter Widget in the Article Page
	 */
	public static By getTwWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".MediaCard-borderOverlay");
	}

	/**
	 * returns YouTube Widget in the Article Page
	 */
	public static By getYoutubeWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-embed-youtube iframe.el-embed-youtube__content");
	}

	/**
	 * returns Twitter Follow Widget in the Article Page
	 */
	public static By getTwFollowWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, "iframe.twitter-follow-button");
	}

	/**
	 * returns Embedded Video in the Article Page
	 */
	public static By getArticleEmbVideo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video--fullwidth");
	}

	/**
	 * returns Embedded Video Play button in the Article Page
	 */
	public static By getArticleEmbVideoButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.vjs-big-play-button");
	}

	// Fact Box elements
	/**
	 * returns Fact box title in the Article Page
	 */
	public static By getFactBoxTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__leafmedia--standard div.el__factbox--title");
	}

	/**
	 * returns Fact box content in the Article Page
	 */
	public static By getFactBoxContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__leafmedia--standard div.el__storycontent--standard");
	}

	// Author card & Story Highlights
	public static WebElement getStoryHighTitle() {
		return driver.findElement(By.className("el__headline"));
	}

	/*
	 * public static By getStoryHighContent() { return d.findElements(static
	 * By.cssSelector(".el__storyhighlights ul li"); }
	 */

	public static WebElement getEditorNote() {
		return driver.findElement(By.className("el-editorial-note"));
	}

	public static WebElement getEditorialSource() {
		return driver.findElement(By.className("el-editorial-source"));
	}

	public static WebElement getEditorImage() {
		return driver.findElement(By.cssSelector(".js__image--standard img"));
	}

	public static WebElement getEditorName() {
		return driver.findElement(By.cssSelector(".js__image--standard div.el__gallery_image-title"));
	}

	// Branding : Article Leaf Page
	public static By getArticleBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark");
	}

	public static By getArticleBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark img.pg__branding-logo");
	}

	public static By getArticleBrandingLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark a");
	}

	// Content to Content Navigation
	public static By getArrowButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//nav[@class='sibling sibling--next']");
	}

	public static By getArticleImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sibling--next a img.media__image");
	}

	public static By getArticleImgSrc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sibling--next div.thumbnail img");
	}

	public static By getArticleHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sibling--next div.headline");
	}

	public static By getArticleLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".sibling--next a.link");
	}

	public static By getCommentField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".editable");
	}

	public static By getCommentiframe() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-editor-iframe iframe");
	}

	public static By getPostComment() {
		return WrapperMethods.locatorValue(Locators.CSS, ".goog-toolbar div.fyre-post-button");
	}

	public static By getCommentReply() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-stream-content a.fyre-comment-reply");
	}

	public static By getPostReplyComment() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-comment-footer div.goog-toolbar div.fyre-post-button");
	}

	public static String generateRandomString(int length) {
		return RandomStringUtils.randomAlphabetic(length);
	}

	// chrome elements
	public static By getCommentTextField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-widget  div.fyre-editor div.editable");
	}

	public static By getReplyCommentTextField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-comment-footer div.fyre-editor-editable");
	}

	// Embedded video Collections

	// Video Collection Carousel
	public static By getEmVideoCollCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-carousel div.owl-stage-outer div.owl-stage");
	}

	// Prev and Next
	public static By getEmVideoCollCarouselPrev() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__leafmedia--featured-video-collection div.owl-nav div.owl-prev");
	}

	public static By getEmVideoCollCarouselNext() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__leafmedia--featured-video-collection div.owl-nav div.owl-next");
	}

	// Video Element

	public static By getEmVideoCollElement() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active span.cd__headline-text");
	}

	public static By getEmVideoCollVideoElements() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active");
	}

	// Now Playing
	public static WebElement getEmVideoCollNowPlay() {
		return driver.findElement(By.cssSelector(
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active div.media a div.media__over-text"));
	}

	// Total Time item
	public static WebElement getEmVideoCollTotalTime() {
		return driver.findElement(By.cssSelector(
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active div.media a i.media__icon[aria-hidden='true']:not([style])"));
	}

	// Img Src
	public static WebElement getEmVideoCollImgSrc() {
		return driver.findElement(
				By.cssSelector(".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active div.media a img"));
	}

	// Headlines
	public static WebElement getEmVideoHeadLines() {
		return driver.findElement(By.cssSelector(
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active div.cd__content h3 a span.cd__headline-text"));
	}

	// Article Leaf Page Outbrain
	public static By getPromotedStoriesOutbrain() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-0 .ob-strip-layout.AR_36 div.ob_what a");
	}

	public static WebElement getMoreFromCNNOutbrain() {
		return driver.findElement(By.cssSelector("div.column.zn__column--idx-1 div.ob_what a"));

	}

	public static By getPromotedStories() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column.zn__column--idx-0 div.ob-widget-header");
	}

	public static By getMoreFromCNN() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column.zn__column--idx-1 div.ob-widget-section.ob-last");
	}

	public static By getMoreFromCNNIntl() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-1 div.ob_dual_right span.ob_org_header");
	}

	public static By getOutbrainContent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".ob_modal p[style='z-index: auto; display: block; margin-bottom: 20px; padding-bottom: 10px;']");
	}

	public static By getOutbrainLearnMoreButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".ob_modal a.standard-button");
	}

	public static By getOutbrainLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo a img");
	}

	public static By getOutbrainCloseButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".ob_modal #ob_modal_inner img");
	}

	public static By getPromotedStoriesTab() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container .ob_dual_left span.obpd_header");
	}

	public static By getMoreFromCNNTab() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container .ob_dual_right span.ob_org_header");
	}

	public static By getPromotedStoriesHeadlinesTab() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container .ob_dual_left li");
	}

	public static By getMoreFromCNNHeadlinesTab() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container .ob_dual_right li");
	}

	public static WebElement getMoreFromCNNOutbrainTab() {
		return driver.findElement(By.cssSelector(".OB_AR_13 div.ob_what a"));

	}

	public static By getMoreFromCnn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='OUTBRAIN js-m-outbrain m-outbrain']//div[@class='ob-widget-section ob-last']/div");
	}

	public static List<WebElement> getPaidContentHeadlinestab() {
		return driver.findElements(By.xpath("//span[@class='ob-unit ob-rec-text']"));
	}

	public static List<WebElement> getMoreFromCnnHeadlinesTab() {
		return driver.findElements(By.cssSelector("#outbrain_widget_2 > div > div.ob-widget-section.ob-last > ul >li"));

	}

	public static WebElement getOutbrainCloseButtonTab() {
		return driver.findElement(By.cssSelector("#ob_modal_inner img[onclick='OBR.modal.closeModal()']"));
	}

	public static By getPaidcontent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"//div[@class='ob-widget ob-two-rows-strip-layout CRMB_1']/div[1]/div[@class='ob-widget-header']");
	}

	public static By getStoryHighCont() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__storyhighlights ul li");
	}

	public static WebElement getStoryHighContent() {
		return driver.findElement(By.cssSelector(".el__storyhighlights ul.el__storyhighlights__list"));
	}

	public static By getEmVideoCollElemMob() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-carousel div.owl-stage-outer div.owl-stage div.owl-item.active div.carousel__content__item div.media");
	}

	/*
	 * public static By readMore() { return d.findElement(static By
	 * .cssSelector(".read-more-link"); }
	 */

	public static By getVideoExpand() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video__player-wrapper");
	}

	/**
	 * Video Play Button in the Video Leaf page
	 */
	public static By getVideoPlayElementDyn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//video[contains(@id,'cvp')]");
	}

	public static By tempelements() {
		return WrapperMethods.locatorValue(Locators.CSS, "#cvp_4");
	}

	/**
	 * Video Play Button in the Video Leaf page
	 */
	public static By getVideoPlayElementObjDyn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//object[contains(@id,'cvp')]");
	}

	public static By getVideoPlayElementObjEm() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'media__video--responsive')]/object");
	}

	public static By getPromotedStoriesLeaf() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container div.ob_dual_left");
	}

	public static By getPaidContent() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column.zn__column--idx-1 .ob-widget-header");
	}

	public static By getFromAroundWebLeaf() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container div.ob_dual_left");
	}

	public static By getMoreFromCNNLeaf() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container div.ob_dual_right");
	}

	public static By getRecommendedForYou() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column.zn__column--idx-0 .ob-widget-header");
	}

	public static List<WebElement> getPromotedStoriesHeadlines() {
		return driver.findElements(By.cssSelector("div.column.zn__column--idx-0 ul.ob-widget-items-container li.ob-p"));
	}

	public static By getPaidContentHeadlines() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-1 ul.ob-widget-items-container li.ob-p");
	}

	public static List<WebElement> getMoreFromCNNHeadlines() {
		return driver.findElements(By.cssSelector("div.column.zn__column--idx-1 ul.ob-widget-items-container li.ob-o"));
	}

	public static By getMoreFromCNNHeadlinesIntl() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column.zn__column--idx-1 div.ob_dual_right li");
	}

	public static By getRecommendedForYouHeadlines() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-0 ul.ob-widget-items-container li.ob-o");
	}

	public static By getFromAroundWebHeadlines() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ob_dual_left div.odb_div");
	}

	public static By getMoreFromCNNHeadlinesGallery() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ob_dual_right div.odb_div");
	}

	public static By getMoreFromCNNOutbrainGallery() {
		return WrapperMethods.locatorValue(Locators.CSS, ".ob_stripDual_container div.ob_what a");
	}

	public static By getOutbrainLogoLeaf() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo a img");
	}

	public static By getArticleNavTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-nav-section-article-title");
	}

	public static By getArticleTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='pg-headline']");
	}

	public static By getArticletitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[contains(@id,'js-mobile-video-headline-')]");
	}

	public static By getarticletitle() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[contains(@id,'js-leaf-video_headline-')]");
	}

	public static By getRightRailPinVideoArtCol() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-pinned-video-collection-title span");
	}

	public static By getRightRailPinVideoArtVid() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fade-in span.el__storyelement__header");
	}

	public static By getStorySlug() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:url']");
	}

	public static By getStoryHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-headline");
	}

	public static By Byline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata__static Byline");
	}

	public static By getStorySection() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='section']");
	}

	public static By getStoryBodyCopy() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-body-text");
	}

	public static By getStoryFactBoxTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__factbox--title");
	}

	public static By getStoryGalleryCaption() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__gallery_image-title");
	}

	public static By getStoryVideoHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__storyelement__header");
	}

	public static By getStoryVideoDuration() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__storyelement__gray");
	}

	public static By getStoryFactBoxContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__storycontent");
	}

	public static By getStoryBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_mod_011ba0778");
	}

	public static By getStoryPartner() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "ARSTECHNICA.COM");
	}

	public static By getStoryLocationSource() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-editorial-source");
	}

	public static By getStoryBackgroundImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	public static By getStoryUS() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".pg-rail-tall__body .zn--idx-0 .l-container div.zn-body__paragraph");
	}

	public static By getStoryFooter() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-body__footer");
	}

	public static By getStoryHighlights() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__storyhighlights__list");
	}

	public static By getStorySeoDesc() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='description']");
	}

	public static By getStorySeoTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='keywords']");
	}

	public static By getStoryVcard() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata__static Byline__author");
	}

	public static By getStoryVcardFB() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='article:author']");
	}

	public static By outBrainColumnFirstZone() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-1 .column.zn__column--idx-0");
	}

	public static By outBrainColumnSecondZone() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-2 .column.zn__column--idx-0");
	}

	public static By articleRightRailFirstAd() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-1 .column.zn__column--idx-1");
	}

	public static By outBrainColumnSecondSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-2");
	}

	public static By outBrainColumnSecondSectionSpace() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-2 .column.zn__column--idx-0");
	}

	public static By rightRailSecondSectionAd() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-2 .column .ad");
	}

	public static By outBrainColumnThirdSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-sponsored-outbrain-3");
	}

	public static By getArticleSecondAd() {
		return WrapperMethods.locatorValue(Locators.ID,
				"google_ads_iframe_/8663477/CNN/entertainment/leaf/small-gls_4__container__");
	}

	public static By getMailSubscripton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cmcs-subscription-wrapper");
	}

	public static By getMailSubscriptonEmail() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cmcs-input-email input");
	}

	public static By getMailSubscriptonEmailSubmit() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-mc-submit");
	}

	public static By getMailSubscriptonError() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-subscribe-error");
	}

	public static By getMailSubscriptonSuccess() {
		return WrapperMethods.locatorValue(Locators.CSS, ".state-success h3");
	}

	public static By getTripAdvisorServiceInfo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-embed-tripadvisor");
	}

	public static By getStandardWidth() {
		return WrapperMethods.locatorValue(Locators.CSS, ".appearance-standard div.cnn-erm");
	}

	public static By getFullWidth() {
		return WrapperMethods.locatorValue(Locators.CSS, ".appearance-fullwidth div.cnn-erm");
	}

	public static WebElement getArticleContent() {
		return driver.findElement(By.xpath(
				"//section[contains(@class,'zn-has-multiple-containers')]/div[@class='l-container']/div[@class='zn-body__paragraph'][4]"));
	}

	public static WebElement getArticleBulletPointsContent() {
		return driver.findElement(By.cssSelector(".zn-body__read-all div.zn-body__paragraph ul.cnn_rich_text"));
	}

	public static By getMobileShareBar() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top");
	}

	public static By getEmbedShow() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__show--embed");
	}

	public static By getEmbedShowImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__show--teaseimage");
	}

	public static WebElement getEmbedShowCaption() {
		return driver.findElement(By.cssSelector(".el__show--teaseimage a img"));
	}

	public static WebElement getEmbedShowHref() {
		return driver.findElement(By.cssSelector(".el__show--teaseimage a"));
	}

	public static WebElement getEmbedWebTag() {
		return driver.findElement(By.cssSelector(".el__embedded--standard div.cnn-erm"));
	}

	public static List<WebElement> getEmbedImage() {
		return driver.findElements(By.xpath("//div[contains(@class,'el__image--expandable')]/div/img"));
	}

	public static List<WebElement> getEmbedImageClose() {
		return driver.findElements(By.cssSelector(".el__image__close--expandable"));
	}

	public static By getEmbedImgClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__image__close--expandable");
	}

	public static By getEmbedSpecial() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__embedded--standard div.el__special--embed");
	}

	public static WebElement getEmbedSpecialImage() {
		return driver.findElement(By.cssSelector(".el__special--teaseimage a img"));
	}

	public static WebElement getEmbedSpecialHref() {
		return driver.findElement(By.cssSelector(".el__special--teaseimage a"));
	}

	public static WebElement getEmbedSpecialTitle() {
		return driver.findElement(By.cssSelector(".el__storyelement__header"));
	}

	public static By getEmbedInstagram() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='instagram-embed-0']");
	}

	public static By getEmailShareDesktop() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata .gigya-sharebar .gig-button-container-email");
	}

	public static By getEmVideoElementPlayerElem() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video--expandfull .media__video--responsive");
	}

	public static By getEmVideoElementPlayerVid() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__video--expandfull video[preload='none']");
	}

	// Comments
	public static By getCommentSignIn() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-user-loggedout");
	}

	public static By getLoginEmail() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-text')]");
	}

	public static By getLoginPass() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-password')]");
	}

	public static By getLoginButton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('gigya-login-form')//input[contains(@class,'gigya-input-submit')]");
	}

	public static By getLoggedIn() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-user-loggedin");
	}

	public static By getLoginBar() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-user-drop");
	}

	public static By getLoggedOut() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fyre-logout-link");
	}

	public static WebElement getEmbedInstagramAlign() {
		return driver.findElement(
				By.cssSelector("#body-text > div.l-container > div.el__embedded.el__embedded--standard > div > div"));
	}

	public static By getEntitlement() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-entitlement");
	}

	public static By liveblogsharebutton(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-post-footer>div:nth-child(2)>div>button");
	}

	public static By livebloglikeicon(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-post-footer>div:nth-child(1)>div>span:nth-child(1)");
	}

	public static By livebloglikecount(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-post-footer>div:nth-child(1)>div>span:nth-child(2)");
	}

	public static By liveblogtextbox(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-share-menu div.s-raw-share.share input");
	}

	public static By liveblogtwitter(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-share-menu div.s-twitter-share.share");
	}

	public static By liveblogfacebook(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "div.s-share-menu div.s-facebook-share.share");
	}

	public static By getLiveFyreHeading(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-element-content h2");
	}

	public static By getLiveFyreParagraph(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-element-content p");
	}

	public static By AvatarLogo(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-post-header span.s-author-avatar span.s-author-initials");
	}

	public static By getLiveFyreItalicHeading(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//h2/i");
	}

	public static By getLiveFyreItalicParagraph(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//p/i");
	}

	public static By getLiveFyreOrderedList(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]/ol/li");
	}

	public static By getLiveFyreQuotedText(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]/blockquote");
	}

	public static By getLiveFyreUnOrderedList(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]/ul/li");
	}

	public static By getLiveFyreStrikedHeading(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//h2/strike");
	}

	public static By getLiveFyreStrikedParagraph(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//p/strike");
	}

	public static By getLiveFyreBoldHeading(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//h2/b");
	}

	public static By getLiveFyreLiked(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-footer-left div.liked");
	}

	public static By liveBlogShareButtonText(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-share-button span.button-text");
	}

	public static By liveblogShareIcon(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-share-button span.storifycon-share");
	}

	public static By getLocation() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-location.js-el-weather__footer-location");
	}

	public static By getWeatherIcon() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el-weather__header-icon");
	}

	public static By getTemperature() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "js-temp");
	}

	public static By Getarticlecaptioncontent() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-item active']//div[@class='media__caption el__gallery_image-title']/span[@class='el__storyelement__gray']");
	}

	public static By Waitarticlevisiblity() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='metadata ']");
	}

	public static By showcaption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-item active']//div[@class='js-el__gallery-caption el__gallery-caption el__gallery-caption--closed']");
	}

	public static By hidecaption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-item active']//div[@class='js-el__gallery-caption el__gallery-caption']");
	}

	public static By liveBlogImageCaption(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS,
				".s-element-content div.s-image-wrapper div.s-upload-image-caption");
	}

	public static By getLiveFyreBoldParagraph(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'s-element-content')]//p/b");
	}

	public static By liveBlogImageWithStrike() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-element-content s-element-image']/ancestor::div[1]");
	}

	public static By StrikeLabel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='s-upload-image-caption']/p/strike");
	}

	public static By liveBlogImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".s-element-content div.s-image-wrapper div.s-image-content");
	}

	public static By liveBlogUnderlinedImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-upload-image-caption']/p/u/ancestor::div[@class='s-upload-image-caption']//preceding-sibling::div[@class='s-image-content']");
	}

	public static By liveBlogUnderlinedCaption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-image-wrapper']//div[@class='s-upload-image-caption']/p/u");
	}

	public static By liveBlogCaption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-element-content s-element-image']/ancestor::div[1]");
	}

	public static By starImage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='storifycon-like']");
	}

	public static By liveBlogIntialRoundShape() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-upload-image-caption']/p/u/ancestor::div[contains(@style,'opacity')]//preceding-sibling::div[1]//span[@class='s-author-initials']");
	}

	public static By liveBlogPlainCaption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-image-wrapper']//div[@class='s-upload-image-caption']/p");
	}

	public static By liveBlogImagePlain() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-upload-image-caption']/p/ancestor::div[@class='s-upload-image-caption']//preceding-sibling::div[@class='s-image-content']");
	}

	public static By liveBlogPostHeader(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'s-element-content')]//p/b//ancestor::div[contains(@style,'opacity')]/preceding-sibling::div[1]//b");
	}

	public static By liveBlogPostHeaderOpacity() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'s-element-content')]//p/b//ancestor::div[contains(@style,'opacity')]/preceding-sibling::div[1]");
	}

	public static By liveBlogItalicHeader(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='s-element-content s-element-text']//h2//i");
	}

	public static By liveBlogItalicHeaderOpacity() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-element-content s-element-text']//h2//i//ancestor::div[contains(@style,'opacity')]");
	}

	public static By liveBlogStrikedHeader(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-element-content s-element-text']//h2//strike");
	}

	public static By liveBlogStrikedHeaderOpacity() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='s-element-content s-element-text']//h2//strike//ancestor::div[contains(@style,'opacity')]");
	}

	public static By imageChk() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='_6l- __c_']//img");
	}

	public static List<WebElement> articleamphtmlink() {
		return WrapperMethods.locateElements(Locators.XPATH, "//head//link[@rel='amphtml']");
	}

	public static By getarticlebackground() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//article[@class='pg-rail-tall pg-rail--align-right ']");
	}

	public static By PagetopGallerydatacut() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-item active']//div[@class='el__resize']//img");
	}

	public static By previousarrowowl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-prev");
	}

	public static By nextarrowowl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-next");
	}

	public static By OB_Container1() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-next");
	}

	public static By getGalleryShowCaption() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery-showhide  div.el__gallery-caption--closed");
	}

	public static By getGigyaBarMessenger() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-messenger");
	}

	public static WebElement getStandardImgSize() {
		return driver.findElement(By.cssSelector(".el__image--standard div img"));
	}

	public static By getEmVideoExpanded() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='cvp_1']");
	}

	public static By getInstagramFollow_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[text()='Follow']");
	}

	public static By getInstagram_Likes() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[contains(@class,'espLikes')]");
	}

	public static By getInstagram_Comments() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[contains(@class,'espComments')]");
	}

	public static By trvGalWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@data-cut-format='16:9']");
	}

	public static By fasGalWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@data-cut-format='4:3']");
	}

	public static By lux1GalWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@data-cut-format='16:9']");
	}

	public static By lux2GalWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@data-cut-format='4:3']");
	}

	public static By getPaidContentSection() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ob-widget-section.ob-first h2.cn__title");
	}

	public static By getPaidContentSectionHeadlines() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='ob-widget-section ob-first']//h2[@class='cn__title']/ancestor::div[@class='ob-widget-section ob-first']//li");
	}

	public static By getGLMoreFromCNN() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ob-widget-section.ob-last");
	}

	public static By getGLMoreFromCNNHeadlines() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.ob-widget-section.ob-last");
	}

	public static By getGLMoreFromCNNOutbrain() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='ob-widget-section ob-last']/following-sibling::div/a");
	}

	public static By CVPAutoplaycheck() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='js-el__video__play-button el__video__play-button']");
	}

	public static By getTheoplayerPlay() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='js-media__video media__video']");
	}

	public static By Nowplayingtheoplayer() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='media__over-text']");
	}

	public static By getLightBackgroundTheme() {
		return WrapperMethods.locatorValue(Locators.CSS, "body.t-light");
	}

	public static By getDarkBackgroundTheme() {
		return WrapperMethods.locatorValue(Locators.CSS, "body.t-dark");
	}

	public static By getFBWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__leafmedia.el__leafmedia--facebook");
	}

	public static By EmbeddedTagTripAdvisor() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//em/parent::a");
	}

	public static By ByLendingTree() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-0 .cn-coverageContainer_40CA20EF-1659-68BF-A23E-3E4BD5F895CC a h2");
	}

	public static By yLendingTreeHeadlines() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-0 .cn-coverageContainer_40CA20EF-1659-68BF-A23E-3E4BD5F895CC li");
	}

	public static By getVideoinactive() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"//div[contains(@class,'theoplayer-dvr-now vjs-user-inactive')]");
	}

	public static List<WebElement> getVideoplaying() {
		return driver.findElements(By.xpath("//div[contains(@class,'has-started')]"));
	}

	public static WebElement getcolectionVideoplayer() {
		return driver.findElement(By.xpath(
				"//div[contains(@class,'el__leafmedia el__leafmedia--featured-video-collection')]//div[contains(@id,'player-body')]"));
	}

	public static By getfirstEmvideo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'embedded')]//div[@class='js-media__video media__video']");
	}

	public static By gettheoVideoPlayButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "#player-large-media_0 div.vjs-big-play-button");
	}

	public static By getCarouselbackbtn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='container kc-wrap']//div[@class='back-nav-button']");
	}

	public static By getCarouselfrwdbtn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='container kc-wrap']//div[@class='forward-nav-button']");
	}

	public static By getCarousel() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='container kc-wrap']");
	}

	public static By getCarouselItem() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='kc-item kc-front-item']//div[@class='content-attachment-controls content-attachment-controls-play']");
	}

	public static By getCarouselItems() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='container kc-wrap']//div[starts-with(@class,'kc-item')]");
	}

	public static By getVideo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='content-attachment-video content-attachments-focused']//video");
	}

	public static By ArticlePaidContent() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='ob-widget-header'][text()='Paid Content']");
	}

	public static By Newsforyou() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='outbrain_widget_3']/div/span[2]");
	}

	public static List<WebElement> paragraph() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='paragraph']");
	}

	public static By VRCNNLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-cnn");
	}

	public static By VRLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-vr");
	}

	public static By headerNav() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul[@role='navigation']/li/a");
	}

	public static By disabledHeaderNav() {
		return WrapperMethods.locatorValue(Locators.CSS, "li.selected a");
	}

	public static By HowtowatchVR() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@title='How to watch VR']");
	}

	public static By copyrightVR() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='legal__copyright']");
	}

	public static By copyrightVR1() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='legal__font-copyright']");
	}

	public static By Termsofservicefooter() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='legal__link'][text()='Terms of service']");
	}

	public static By PrivacyGuidelinefooter() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@class='legal__link'][text()='Privacy guidelines']");
	}

	public static By VRPlayButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='vjs-big-play-button']");
	}

	public static By headline() {
		return WrapperMethods.locatorValue(Locators.CSS, "h1.pg-headline,span.cd__headline-text");
	}

	public static By VRgetPageLoad(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-cnn");
	}

	public static By getContentByLendingTree() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.column.zn__column--idx-0 .cn-coverageContainer_40CA20EF-1659-68BF-A23E-3E4BD5F895CC a h2");

	}

	public static List<WebElement> getContentByLendingTreeHeadlines() {
		return driver.findElements(By.cssSelector(
				"div.column.zn__column--idx-0 .cn-coverageContainer_40CA20EF-1659-68BF-A23E-3E4BD5F895CC li"));
	}

	public static List<WebElement> EmbeddedTagTripAdvisors() {
		return WrapperMethods.locateElements(Locators.XPATH,"//em/parent::a");
	}
	public static By SearchIconEntertainment() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'search-toggle')]");
	}

	public static By SearchIconInputBox() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@class='search__input-field']");
	}

	public static By CoverageContainerRSS() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[contains(@class,'coverageContainer')]");
	}

	public static List<WebElement> ArticleSpeakableParagraph() {
		return WrapperMethods.locateElements(Locators.XPATH, "//*[@class='zn-body__paragraph speakable']");
	}

	public static List<WebElement> ArtileAllParagraph() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//*[@class='el__leafmedia el__leafmedia--speakable-paragraph' or @class='el__leafmedia el__leafmedia--sourced-paragraph']/*");
	}

	public static By ScriptSpecificationJson() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//body/script[@type='application/ld+json']");

	}

	public static List<WebElement> liveblogtextboxs(WebDriver go) {
		return go.findElements(By
				.cssSelector("div.s-share-menu div.s-raw-share.share input"));
	}
	public static List<WebElement> liveblogfacebk(WebDriver go) {
		return WrapperMethods.locateElements(Locators.CSS,"div.s-share-menu div.s-facebook-share.share");
	}
	public static List<WebElement> liveblogtwittr(WebDriver go) {
		return WrapperMethods.locateElements(Locators.CSS,"div.s-share-menu div.s-twitter-share.share");
	}
	public static By VRZone1() {
		return WrapperMethods.locatorValue(Locators.XPATH,"//div[@class='vr_overlay']");
	}
}