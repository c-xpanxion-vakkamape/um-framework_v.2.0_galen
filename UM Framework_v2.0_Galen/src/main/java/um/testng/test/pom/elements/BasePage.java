package um.testng.test.pom.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class BasePage {

	static WebDriver driver = DriverFactory.getCurrentDriver();

	/**
	 * Explicitly waits the given amount of seconds before proceeding.
	 * 
	 * @param seconds
	 *            The amount of time, in seconds, to wait
	 */
	public static void wait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static By getTermsServiceConsentClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "user-msg--close");
	}

	/**
	 * returns Terms of Service Consent Close button
	 */
	public static By getTermsConsentClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "user-msg--close");
	}

	/**
	 * returns Ad Choice Element
	 */
	public static By getAdChoice() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "AdChoices");
	}

	/**
	 * returns Ad Choice Frame Element
	 */
	public static By getAdChoiceFrame() {

		return WrapperMethods.locatorValue(Locators.CSS,
				"div[id*='pop-div'] > div > iframe[src*='http://preferences-mgr.truste.com'][id*='popFrame']");

	}

	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "article[class^='pg-rail']");
	}

	public static By getArticleHeaderTitle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pg-headline");
	}

	public static By getArticleCont() {
		return WrapperMethods.locatorValue(Locators.CSS, "section#body-text");
	}

	public static By getNavColorStrip() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav__color-strip");
	}

	public static By getAuthorImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".byline-images > img");
	}

	public static By getAuthorImageSrc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata div.byline-images img");
	}

	public static By getFooterCurrentEdition() {

		return WrapperMethods.locatorValue(Locators.CSS, ".l-footer .current");
	}

	public static By getFooterSetEdition() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'drop-down')]//ul//li[contains(@class,'item')]");

	}

	public static List<WebElement> getFooterSetEditionHdrRadio() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'drop-down')]//ul[contains(@class,'list--set')]/li[@data-value]");
	}

	public static List<WebElement> getFooterSetEditionHdrLabels() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'drop-down')]//ul[contains(@class,'list--set')]/li/input");
	}

	public static By getFooterSetEditionHdrButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//button[@class='edition-picker__confirm-button']");

	}

	public static List<WebElement> getFooterEditionUsIntl() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//ul[contains(@class,'js-edition-picker__list')]/li/label");
	}

	/**
	 * returns Current Edition Element
	 */
	public static By getCurrentEditionDev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@id='nav-expanded']//div[@class='edition-picker__current-edition']");

	}

	public static By getCurrentEdition() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".nav__container div.edition-picker > .edition-picker__current-edition");

	}

	public static By getSearchField() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search__input-field");

	}

	public static By getSearchButton() {
		return WrapperMethods.locatorValue(Locators.ID, "submit-button");

	}

	/**
	 * CNN Live Tv
	 */
	public static By liveTv() {
		return WrapperMethods.locatorValue(Locators.ID, "nav-mobileTV");

	}

	public static By getNavMenu() {
		return WrapperMethods.locatorValue(Locators.CSS, "#menu.nav-menu");

	}

	public static List<WebElement> getOtherEditionHeaderDev() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@id='nav-expanded']//div[@class='edition-picker__list']/div");
	}

	public static By getSetEditionHdrDev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-section--expanded')]/form");

	}

	public static By getSetEditionHdrTxtDev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-section--expanded')]/form/p");

	}

	public static List<WebElement> getSetEditionHdrLabelsDev() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-section--expanded')]/form/label");
	}

	public static List<WebElement> getSetEditionHdrRadioButtonDev() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-section--expanded')]/form/label/input");
	}

	public static By getSetEditionHdrButtonDev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'nav-section--expanded')]/form/button");

	}

	public static List<WebElement> getOtherEditionHeader() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@id='nav']//div[@class='edition-picker__list']/div");
	}

	public static By getSetEditionHdr() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav div.edition-picker form");

	}

	public static By getSetEditionHdrTxt() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav div.edition-picker form > p");

	}

	public static List<WebElement> getSetEditionHdrLabels() {

		return WrapperMethods.locateElements(Locators.CSS, "#nav div.edition-picker form > label");
	}

	public static List<WebElement> getSetEditionHdrRadioButton() {

		return WrapperMethods.locateElements(Locators.CSS, "#nav div.edition-picker form > label > input");
	}

	public static By getSetEditionHdrButton() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav div.edition-picker form > button");

	}

	/**
	 * gets the header links : Mobile
	 */
	@FindBy(css = "div.m-navigation__buckets-container--mobile a[href=\"#\"]")
	public static By mobileListToggle;

	/**
	 * CNN logo : Mobile
	 */
	@FindBy(css = "div[class=m-navigation__logo--toggle]")
	public static By linkLogoMobile;

	/**
	 * CNN logo : Desktop
	 */
	@FindBy(css = ".nav__logo")
	public static By cnnRedLogo;

	/**
	 * Navigation menu item : News
	 */
	@FindBy(css = "li.m-navigation__bucket-item a[href^=\"/news\"]")
	public static By linkNews;

	/**
	 * Navigation menu item : News :Mobile
	 */
	@FindBy(linkText = "NEWS")
	public static By linkNewsMobile;

	/**
	 * Navigation menu item : TV
	 */
	@FindBy(linkText = "TV")
	public static By linkTV;

	/**
	 * Navigation menu item : World
	 */
	@FindBy(css = "li.m-navigation__section-item--world a span")
	public static By linkWorld;

	/**
	 * returns Muted Video Player
	 */
	public static By getMutedVideo() {
		return WrapperMethods.locatorValue(Locators.CSS, "#muted-player");
	}

	/**
	 * returns Footer Weather Value
	 */
	public static By getFooterWeatherValue() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el-weather__footer-temperature");
	}

	/**
	 * returns Footer Weather Icon
	 */
	public static By getFooterWeathericon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-icon-wrapper");
	}

	/**
	 * returns Footer Weather Location
	 */
	public static By getFooterWeatherLoc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-location");
	}

	/**
	 * returns Footer Weather
	 */
	public static By getFooterWeather() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__footer-content");
	}

	public static By getFooter() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-footer");
	}

	public static List<WebElement> getFooterOtherEditionHeaderDev() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//ul[@class='list js-edition-picker__list list--set']//li[contains(@class,'item')][@data-value]");
	}

	public static By getNavFlyOut() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-expanded.nav-flyout");
	}

	/**
	 * returns Current Edition Skin Element
	 */
	public static By getCurrentEditionSkin() {
		return WrapperMethods.locatorValue(Locators.CSS, ".edition-picker >div > div[class='current']");
	}

	/**
	 * returns Current Edition Other Element
	 */
	public static By getCurrentEditionOther() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.container>div.edition-picker>div>ul>li:not([class*='selected']");
	}

	/**
	 * returns Ad Choice Element
	 */

	/**
	 * returns list of Ad Frame Elements
	 */
	public By getAdFrames() {
		return WrapperMethods.locatorValue(Locators.CSS, "iframe");
	}

	/**
	 * returns Ad Choice Frame Element
	 */

	public static By getAdChoiceFrame1() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//iframe[contains(@id,'popFrame')]");
	}

	/**
	 * returns Ad Choice Content Element
	 */
	public static By getAdChoiceContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".headTitle");
	}

	/**
	 * returns Header Search Element
	 */
	public static By getHeaderSearch() {
		// return WrapperMethods.locatorValue(Locators.ID,"searchInputNav");
		return WrapperMethods.locatorValue(Locators.CSS, ".search__button");
	}

	/**
	 * returns Header Mouse Hover Element
	 */
	public static By getHeaderMouseHover() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.search");
	}

	/**
	 * returns Search Element
	 */
	public static By getSearchElement() {
		return WrapperMethods.locatorValue(Locators.CSS, "#cnnSearchSummary");
	}

	/**
	 * returns Footer Search Element
	 */
	public static By getFooterSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, "#searchInputFooter");
	}

	/**
	 * returns list of Editions
	 */
	public By getEditionList() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"id('nav-header')/nav/div/div[1]/div/ul/li[not(contains(@class,\"selected\")]");
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.container>div.edition-picker>div>ul>li:not([class*='selected']");
	}

	/**
	 * returns list of Editions in Safari Browser
	 */
	public By getEditionListSaf() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-header')/nav/div/div[1]/div/ul/li[not(contains(@class,\"selected\")]");
	}

	/**
	 * returns Current Edition footer Element
	 */
	public static By getCurrentEditionfooter() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-copyright-outercontainer > div[class='edition-picker'] > div > div[class='current']");
	}

	/**
	 * returns list of Editions in footer
	 */
	public List<WebElement> getEditionListfooter() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.drop-down:nth-child(2) > ul:nth-child(2) > li:not([class*=\"selected\"])");
	}

	/**
	 * returns list of Editions in footer - Safari Browser
	 */
	public By getEditionListfooterSaf() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"/html/body/footer/div[2]/div/div[2]/div[1]/div/ul/li[not(contains(@class,\"selected\")]");
	}

	/**
	 * returns CNN Red Logo Element
	 */
	public static By getCnnRedLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav__logo");
	}

	public static By getCNNLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo .logo-links a.logo-links__cnn");
	}

	/**
	 * returns CNN Black Logo Element
	 */
	public static By getCnnBlackLogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"/html/body/footer/div[2]/div/div[2]/div[1]/a[@class=\"logo\"]");
	}

	/**
	 * returns Error Line1
	 */
	public static By getErrorLine1() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error_headline");
	}

	/**
	 * returns Error Line2
	 */
	public static By getErrorLine2() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error_headline2");
	}

	/**
	 * returns Terms of Service Consent Close button
	 */

	/**
	 * Navigation menu item : trends
	 */
	@FindBy(linkText = "TRENDS")
	public static By linkTrends;

	/**
	 * Navigation menu item : trends
	 */
	@FindBy(linkText = "OPINIONS")
	public static By linkOpinions;

	/**
	 * Navigation menu item : This is CNN
	 */
	@FindBy(linkText = "THIS IS CNN")
	public static By linkThisIsCNN;

	/**
	 * Navigation menu item : ENTERTAINMENT
	 */
	@FindBy(linkText = "ENTERTAINMENT")
	public static By linkEntertainment;

	/**
	 * edition picker : which is used to change the edition to US or
	 * international
	 */
	@FindBy(css = "div.edition-picker")
	public static By linkEditionPick;

	/**
	 * get the pop-up breaking news
	 */
	@FindBy(css = "div.breaking-news")
	public static By linkBreakNews;

	/**
	 * CNN logo in the header
	 */
	@FindBy(css = "div.nav__container #logo")
	public static By linkCNNLogo;

	@FindBy(css = "div.utilities")
	public static By linkUtilities;

	/**
	 * navigation inside the News menu
	 */
	@FindBy(css = "li.bucket.news")
	public static By linkBucketNews;

	/**
	 * navigation inside the Tv menu
	 */
	@FindBy(css = "li.bucket.tv")
	public static By linkBuckTV;

	/**
	 * navigation inside the Video menu
	 */
	@FindBy(css = "li.bucket.video")
	public static By linkBuckVideo;

	/**
	 * navigation inside the Voices menu
	 */
	@FindBy(css = "li.bucket.voices")
	public static By linkBuckVoices;

	/**
	 * mega navigation
	 */
	@FindBy(css = "nav.nav")
	public static By linkHeaderSec;

	@FindBy(css = "span.location")
	public static By linkLocation;

	/**
	 * Mega Nav : menu item : Money
	 */
	@FindBy(css = "li.bucket.money")
	public static By linkBuckMoney;

	/**
	 * Mega Nav : menu item : More
	 */
	@FindBy(css = "li.bucket.more")
	public static By linkBuckMore;

	/**
	 * Mega Nav : menu item : China
	 */
	@FindBy(css = "li.section.china")
	public static By linkSecChina;

	/**
	 * Mega Nav : menu item : Asia
	 */
	@FindBy(css = "li.section.asia")
	public static By linkSecAsia;

	/**
	 * Mega Nav : menu item : Middle-east
	 */
	@FindBy(css = "li.section.middle-east")
	public static By linkSecMiddleEast;

	/**
	 * Mega Nav : menu item : Africa
	 */
	@FindBy(css = "li.section.africa")
	public static By linkSecAfrica;

	/**
	 * Mega Nav : menu item : Europe
	 */
	@FindBy(css = "li.section.europe")
	public static By linkSecEurope;

	/**
	 * Mega Nav : menu item : Americas
	 */
	@FindBy(css = "li.section.americas")
	public static By linkSecAmericas;

	/**
	 * Section Pages : US : International
	 */
	@FindBy(css = "li.section.u-s")
	public static By linkSecUSINTL;

	/**
	 * Section Pages : US : Domestic
	 */
	@FindBy(css = "li.section.us")
	public static By linkSecUS;

	/**
	 * Section Pages : World : Domestic
	 */
	@FindBy(css = "li.section.world")
	public static By linkSecWorld;

	/**
	 * Section Pages : Politics : Domestic
	 */
	@FindBy(css = "li.section.politics")
	public static By linkSecPolitics;

	/**
	 * Section Pages : Tech : Domestic
	 */
	@FindBy(css = "li.section.tech")
	public static By linkSecTech;

	/**
	 * Section Pages : Health : Domestic
	 */
	@FindBy(css = "li.section.health")
	public static By linkSecHealth;

	/**
	 * Section Pages : Entertainment : Domestic
	 */
	@FindBy(css = "li.section.entertainment ")
	public static By linkSecEntertainment;

	/**
	 * Section Pages : Money : Domestic
	 */
	@FindBy(css = "li.section.money")
	public static By linkSecMoney;

	/**
	 * Section Pages : Living : Domestic
	 */
	@FindBy(css = "li.section.living")
	public static By linkSecLiving;

	/**
	 * Section Pages : Travel : Domestic
	 */
	@FindBy(css = "li.section.travel")
	public static By linkSecTravel;

	/**
	 * Section Pages : Sports : Domestic
	 */
	@FindBy(css = "li.section.sports")
	public static By linkSecSports;

	/**
	 * Search box : input field
	 */
	@FindBy(css = "input#searchInputNav")
	public static By linkSearchBox;

	@FindBy(css = "div.owl-next")
	public static By linkCaroNext;

	@FindBy(css = "div.owl-prev")
	public static By linkCaroPrev;

	@FindBy(css = "div.js-owl-carousel")
	public static By linkimageDisp;

	@FindBy(css = "div[class=m-navigation__widgets] li:nth-of-type(2) a")
	public static By linkGallery;

	@FindBy(css = "div.el__media__video div[data-video-id]")
	public By getIframeDivs;

	@FindBy(css = "li.m-navigation__section-item--world a span")
	public static By linkSectionsWorld;

	@FindBy(css = "li.m-navigation__section-item--politics a span")
	public static By linkSectionsPolitics;

	@FindBy(css = "li.m-navigation__section-item--us a span")
	public static By linkSectionsUS;

	@FindBy(css = "li.m-navigation__section-item--tech a span")
	public static By linkSectionsTech;

	@FindBy(css = "li.m-navigation__section-item--health a span")
	public static By linkSectionsHealth;

	@FindBy(css = "li.m-navigation__section-item--living a span")
	public static By linkSectionsLiving;

	@FindBy(css = "li.m-navigation__section-item--travel a span")
	public static By linkSectionsTravel;

	@FindBy(css = "li.m-navigation__section-item--showbiz a span")
	public static By linkSectionsEntertainment;

	/**
	 * Section Pages : US : Domestic: mobile
	 */
	@FindBy(linkText = "U.S.")
	public static By linkMobileUS;

	/**
	 * Section Pages : World : Domestic: mobile
	 */
	@FindBy(linkText = "WORLD")
	public static By linkMobileWorld;

	/**
	 * Section Pages : POLITICS : Domestic: mobile
	 */
	@FindBy(linkText = "POLITICS")
	public static By linkMobilePolitics;

	/**
	 * Section Pages : TECH : Domestic: mobile
	 */
	@FindBy(linkText = "TECH")
	public static By linkMobileTech;

	/**
	 * Section Pages : HEALTH : Domestic: mobile
	 */
	@FindBy(linkText = "HEALTH")
	public static By linkMobileHealth;

	/**
	 * Section Pages : LIVING : Domestic: mobile
	 */
	@FindBy(linkText = "LIVING")
	public static By linkMobileLiving;

	/**
	 * Section Pages : TRAVEL : Domestic: mobile
	 */
	@FindBy(linkText = "TRAVEL")
	public static By linkMobileTravel;

	/**
	 * Section Pages : ENTERTAINMENT : Domestic: mobile
	 */
	@FindBy(linkText = "ENTERTAINMENT")
	public static By linkMobileEntertainment;

	/**
	 * Section Pages : SPORTS : Domestic: mobile
	 */
	@FindBy(linkText = "SPORTS")
	// not implemented in the mobile site
	public static By linkMobileSports;

	@FindBy(linkText = "Sports")
	public static By linkSectionsSports;

	@FindBy(linkText = "Justice")
	public static By linkSectionsJustice;

	@FindBy(linkText = "Money")
	public static By linkSectionsMoney;

	@FindBy(css = "iframe")
	public By getIframes;

	private List<By> headLinks;

	private List<By> mainFooterLinks;

	private List<By> desktopFooterLinks;

	private List<By> sectionLinks;

	// Footers below
	@FindBy(css = "a.m-footer__title__link[href=\"/news?hpt=footer\"]")
	public static By linkFooterNews;

	@FindBy(css = "a.m-footer__title__link[href=\"http://cnnpressroom.blogs.cnn.com?hpt=footer\"]")
	public static By linkFooterThisIsCNN;

	@FindBy(css = "a.m-footer__title__link[href=\"http://www.cnn.com/cnn/programs?hpt=footer\"]")
	public static By linkFooterTV;

	@FindBy(css = "a.m-footer__title__link[href=\"http://www.cnn.com/trends?hpt=footer\"]")
	public static By linkFooterTrends;

	@FindBy(css = "a.m-footer__title__link[href=\"http://www.cnn.com/opinion?hpt=footer\"]")
	public static By linkFooterOpinions;

	@FindBy(css = "a.m-footer__title__link[href=\"/multimedia/videos?hpt=footer\"]")
	public static By linkFooterVideos;

	@FindBy(css = "a.m-footer__title__link[href=\"/multimedia/photos?hpt=footer\"]")
	public static By linkFooterPhotos;

	@FindBy(css = "a.m-footer__title__link[href=\"/multimedia/charts-and-maps?hpt=footer\"]")
	public static By linkFooterCharts;

	@FindBy(css = "a[href=\"/us?hpt=footer\"]")
	public static By linkFooterUS;

	@FindBy(css = "a[href=\"/world?hpt=footer\"]")
	public static By linkFooterWorld;

	@FindBy(css = "a[href=\"/politics?hpt=footer\"]")
	public static By linkFooterPolitics;

	@FindBy(css = "a[href=\"/justice?hpt=footer\"]")
	public static By linkFooterJustice;

	@FindBy(css = "a[href=\"/showbiz?hpt=footer\"]")
	public static By linkFooterEntertainment;

	@FindBy(css = "a[href=\"/tech?hpt=footer\"]")
	public static By linkFooterTech;

	@FindBy(css = "a[href=\"/health?hpt=footer\"]")
	public static By linkFooterHealth;

	@FindBy(css = "a[href=\"/living?hpt=footer\"]")
	public static By linkFooterLiving;

	@FindBy(css = "a[href=\"/travel?hpt=footer\"]")
	public static By linkFooterTravel;

	@FindBy(css = "a[href=\"http://money.cnn.com?hpt=footer\"]")
	public static By linkFooterMoney;

	@FindBy(css = "a[href=\"http://www.bleacherreport.com?hpt=footer\"]")
	public static By linkFooterSports;

	@FindBy(css = "a[href=\"http://www.cnn.com/about?hpt=footer\"]")
	public static By linkFooterMission;

	@FindBy(css = "a[href=\"http://www.cnn.com/CNN/anchors_reporters?hpt=footer\"]")
	public static By linkFooterPersonalities;

	@FindBy(css = "a[href=\"http://ireport.cnn.com?hpt=footer\"]")
	public static By linkFooterIReport;

	@FindBy(css = "a[href=\"http://cnnpressroom.blogs.cnn.com?hpt=footer\"]")
	public static By linkFooterPressroom;

	@FindBy(css = "a[href=\"http://www.cnn.com/specials/impact.your.world?hpt=footer\"]")
	public static By linkFooterImpact;

	@FindBy(css = "a[href=\"http://thecnnfreedomproject.blogs.cnn.com?hpt=footer\"]")
	public static By linkFooterFreedomProject;

	@FindBy(css = "a[href=\"/video/data/2.0/video/cvptve/cvpstream1.html?hpt=footer\"]")
	public static By linkFooterWatchCNN;

	@FindBy(css = "a[href=\"http://www.cnn.com/cnn/programs?hpt=footer\"]")
	public static By linkFooterSchedule;

	@FindBy(css = "a[href=\"http://cnnpressroom.blogs.cnn.com/category/cnn/cnn-special-programming-documentaries/cnn-films?hpt=footer\"]")
	public static By linkFooterCNNFilms;

	@FindBy(css = "a[href=\"http://www.cnn.com/cnn/programs?hpt=footer\"]")
	public static By linkFooterAllShows;

	@FindBy(css = "#nav-header > nav > div > div.utilities.drawer > div.weather > a > span.temperature > span")
	public static By weatherLink;

	/**
	 * Gives the title of the page.
	 * 
	 * @return The current page title
	 */

	/**
	 * Gets all the elements of a current class on the page.
	 * 
	 * @param cssClass
	 *            The class of elements to be returned
	 * @return A List containing all elements of the desired class
	 */
	public List<WebElement> getModulesByClass(String cssClass) {
		return WrapperMethods.locateElements(Locators.CSS, cssClass);
	}

	/**
	 * Gets a element based on the text of the link.
	 * 
	 * @param linkText
	 *            The link text to search for
	 * @return The element containing the searched text
	 */
	public static By getLinkByText(String linkText) {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, linkText);
	}

	/**
	 * Gets the header links on the current page.
	 * 
	 * @return A List of current header elements
	 */
	public List<By> getHeaderLinks() {
		if (headLinks == null) {
			headLinks = new ArrayList<By>();

			headLinks.add(linkNews);
			headLinks.add(linkTV);
			headLinks.add(linkTrends);
			headLinks.add(linkOpinions);
			headLinks.add(linkThisIsCNN);

		}
		return headLinks;
	}

	/**
	 * Gets the Footer Main links on the current page.
	 * 
	 * @return A List of current Footer elements
	 */

	public List<By> getMainFooterLinks() {
		if (mainFooterLinks == null) {
			mainFooterLinks = new ArrayList<By>();

			mainFooterLinks.add(linkFooterNews);
			mainFooterLinks.add(linkFooterThisIsCNN);
			mainFooterLinks.add(linkFooterTV);
			mainFooterLinks.add(linkFooterTrends);
			mainFooterLinks.add(linkFooterOpinions);
			mainFooterLinks.add(linkFooterVideos);
			mainFooterLinks.add(linkFooterPhotos);
			mainFooterLinks.add(linkFooterCharts);
		}
		return mainFooterLinks;
	}

	/**
	 * Gets the desktop Footer links on the current page.
	 * 
	 * @return A List of current Footer elements
	 */
	public List<By> getDesktopFooterLinks() {
		if (desktopFooterLinks == null) {

			desktopFooterLinks.add(linkFooterUS);
			desktopFooterLinks.add(linkFooterWorld);
			desktopFooterLinks.add(linkFooterPolitics);
			desktopFooterLinks.add(linkFooterJustice);
			desktopFooterLinks.add(linkFooterTech);
			desktopFooterLinks.add(linkFooterHealth);
			desktopFooterLinks.add(linkFooterEntertainment);
			desktopFooterLinks.add(linkFooterLiving);
			desktopFooterLinks.add(linkFooterTravel);
			desktopFooterLinks.add(linkFooterMoney);
			desktopFooterLinks.add(linkFooterSports);
			desktopFooterLinks.add(linkFooterMission);
			desktopFooterLinks.add(linkFooterPersonalities);
			desktopFooterLinks.add(linkFooterIReport);
			desktopFooterLinks.add(linkFooterPressroom);
			desktopFooterLinks.add(linkFooterImpact);
			desktopFooterLinks.add(linkFooterFreedomProject);
			desktopFooterLinks.add(linkFooterWatchCNN);
			desktopFooterLinks.add(linkFooterSchedule);
			desktopFooterLinks.add(linkFooterCNNFilms);
			desktopFooterLinks.add(linkFooterAllShows);
		}
		return desktopFooterLinks;
	}

	/**
	 * returns Breaking News Section
	 */
	public static By getBreakingNews() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.breaking-news__msg");
	}

	/**
	 * returns Breaking News Close Button
	 */
	public static By getBreakingNewsClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".breaking-news__close-btn");
	}

	/**
	 * returns Breaking News Close Button
	 */
	public static By BreakingNewsClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "breaking-news__close-btn");
	}

	/**
	 * Gets a List of all the section links on the page.
	 * 
	 * @return A List of the page's section links
	 */
	public List<By> getSectionLinks(boolean mobile) {
		if (mobile) {
			if (sectionLinks == null) {
				sectionLinks = new ArrayList<By>();

				sectionLinks.add(linkMobileWorld);
				sectionLinks.add(linkMobilePolitics);
				sectionLinks.add(linkMobileUS);
				sectionLinks.add(linkMobileTech);
				sectionLinks.add(linkMobileHealth);
				sectionLinks.add(linkMobileLiving);
				sectionLinks.add(linkMobileTravel);
				sectionLinks.add(linkMobileEntertainment);
			}
		} else {
			if (sectionLinks == null) {
				sectionLinks = new ArrayList<By>();

				sectionLinks.add(linkSectionsWorld);
				sectionLinks.add(linkSectionsPolitics);
				sectionLinks.add(linkSectionsUS);
				sectionLinks.add(linkSectionsTech);
				sectionLinks.add(linkSectionsHealth);
				sectionLinks.add(linkSectionsLiving);
				sectionLinks.add(linkSectionsTravel);
				sectionLinks.add(linkSectionsEntertainment);
			}
		}

		return sectionLinks;
	}

	/**
	 * Gets a List of all the video links on the page.
	 * 
	 * @return A List of the page's video links
	 */
	/**
	 * Gets a List of all the video links on the page.
	 * 
	 * @return A List of the page's video links
	 */
	public List<WebElement> getVideopageLinks() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		List<WebElement> pagelinks = new ArrayList<WebElement>();
		pagelinks = driver.findElements(By.tagName("a"));
		return pagelinks;
	}

	/**
	 * Gets a List of all the video image links on the page.
	 * 
	 * @return A List of the page's video image links
	 */
	public By getvdoImageLinks() {
		return WrapperMethods.locatorValue(Locators.CSS, "a>img");

	}

	/**
	 * Waits for the current page to load.
	 */
	/*
	 * @Override protected void waitForLoad() { wait.until(new
	 * ExpectedCondition<WebElement>() {
	 * 
	 * @Override public static By apply() { return
	 * WrapperMethods.locatorValue(Locators.CSS,".logo"); // trying to //
	 * guarantee IE8 // will detect // page loads } }); }
	 */

	/**
	 * Checks the header links to ensure that they are visible on the current
	 * page.
	 */

	// Easiest to just get all the items and update if the footer changes -
	// update would need to happen anyways to maintain tests
	// If tests need to be more granular individual buckets could be grabbed
	// (class m-footer__bucket__news for example)
	public static List<WebElement> getAllFooterLinks() {
		return WrapperMethods.locateElements(Locators.CSS, "ol.m-footer__bucket a");
	}

	// same as footers - grabs all elements that should be appearing and checks
	// number - can be more granular if needed, but most of the
	// time if there is an issue none of the links will be showing up...
	public By getAllHeaderLinks() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.m-navigation__section-link");
	}

	/**
	 * Explicitly waits the given amount of seconds before proceeding.
	 * 
	 * @param seconds
	 *            The amount of time, in seconds, to wait
	 */

	/**
	 * Explicitly waits the given amount of milliseconds before proceeding.
	 * 
	 * @param milliseconds
	 *            The amount of time, in milliseconds, to wait
	 */
	public void preciseWait(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Grabs the meta tags from the raw source, so they can be tested further.
	 * 
	 * @return The labels and values of any meta tags that appear in the page
	 *         source
	 */
	public List<Pair> getMetaTags() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		String rawSource = driver.getPageSource();
		List<Pair> metaPairs = new ArrayList<Pair>();
		int startIndex = 0, endIndex = 5;

		while (endIndex < rawSource.length()) {
			if (rawSource.substring(startIndex, endIndex).equals("<meta")) {
				while (rawSource.charAt(endIndex) != '>') {
					endIndex++;
				} // Grab the entire meta tag...
				metaPairs.add(processMeta(rawSource.substring(startIndex, endIndex - 1))); // Send
																							// it
																							// to
																							// be
																							// processed
				startIndex = endIndex; // Keep traversing at the end of the meta
										// tag
				if (startIndex + 5 < rawSource.length()) {
					endIndex = startIndex + 5;
				} else {
					return metaPairs; // Go back if we're at the EOF
				}
			}
			startIndex++;
			endIndex++; // Continue traversing until the next meta tag
		}
		return metaPairs;
	}

	/**
	 * Processes the raw meta tag data from the source.
	 * 
	 * @param s
	 *            The pre-processed meta tag
	 * @return A pair containing the label of the meta tag, and the value (if
	 *         applicable)
	 */
	private Pair processMeta(String s) {
		String labelTag = null, valueTag = null;
		// remove the meta tag
		s = s.substring(5, s.length());
		String[] toreturn = s.split("\""); // Split the meta tags (relies on
											// double quotes being used)
		String[] labelParse = toreturn[1].split(" ");
		labelTag = labelParse[0].trim();

		if (toreturn.length > 3) {
			valueTag = toreturn[3];
		}

		Pair toReturn = new Pair(labelTag, valueTag);

		return toReturn;
	}

	/**
	 * Attempts to retrieve a visible element from a List of static Bys, or null
	 * if none are visible.
	 * 
	 * @param list
	 *            A List of static Bys to search through
	 * @return A visible element from the list parameter
	 */
	public WebElement getVisible(List<WebElement> list) {
		Iterator<WebElement> iter = list.iterator();
		while (iter.hasNext()) {
			WebElement elem = iter.next();
			if (elem.isDisplayed())
				return elem;
		}
		return null;
	}

	/**
	 * A class to hold the label/value combinations of meta tags. Public, so it
	 * can be used by other classes which deal with meta tags.
	 * 
	 */
	public class Pair {
		private String f;
		private String l;

		public Pair(String f, String l) {
			this.f = f;
			this.l = l;
		}

		public String getFirst() {
			return f;
		}

		public String getLast() {
			return l;
		}
	}

	public WebElement getParent(WebElement elem) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebElement toreturn;
		try {
			toreturn = elem.findElement(By.xpath(".."));
		} catch (Exception e) {
			toreturn = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;",
					elem);
		}
		return toreturn;
	}

	public WebElement getSpan(WebElement elem) {
		return elem.findElement(By.cssSelector("span"));
	}

	/**
	 * 
	 * @param elem
	 * @returns Hidden Text
	 */
	public String getHiddenText(WebElement elem) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		if (((RemoteWebDriver) driver).getCapabilities().getBrowserName().equals("internet explorer")) {
			return elem.getAttribute("innerText").trim();
		} else {
			Object name = ((JavascriptExecutor) driver).executeScript("return arguments[0].textContent;", elem);
			// This is a workaround for not being able to see hidden text in
			// Selenium
			return ((String) name).trim();
		}
	}

	/**
	 * @returns list of Hoverables in the page
	 */
	public By getHoverables() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.m-navigation__section-item-content");
	}

	/**
	 * checks whether particular element exists or not
	 */
	public boolean isElementExists(By by) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		boolean isExists = true;

		try {

			driver.findElement(by);

		} catch (NoSuchElementException e) {

			isExists = false;

		}

		return isExists;

	}

	public static By readMore() {
		return WrapperMethods.locatorValue(Locators.CSS, ".read-more-link");
	}

	/**
	 * returns Current section of the page
	 */
	/*
	 * public static By getCurrentsection() { return
	 * WrapperMethods.locatorValue(Locators.CSS,".nav-section__name > a"); }
	 *//**
		 * returns Current selected section of the page
		 */
	/*
	 * public static By getCurrentsectionSelect() { return
	 * WrapperMethods.locatorValue(Locators.CSS,".nav-section__submenu-active");
	 * }
	 * 
	 * public static By getCurrentsectionDropdown() { return
	 * WrapperMethods.locatorValue
	 * (Locators.CSS,".nav-section__name span.nav-section__expand-icon"); }
	 * 
	 * public static By getCurrentSubsection() { return
	 * WrapperMethods.locatorValue(Locators.CSS,"#nav-section-submenu"); }
	 */
	public static By getCurrentSubsectionElem() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-section-submenu a");
	}

	public static By getCurrentPageHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	public static By getSectionContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-no-rail");
	}

	public static By getErrorHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".error_headline");
	}

	public static By getSectionName() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-nav-section-name");
	}

	public static By getSubSectionImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	public static By getSubSectionTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__title");
	}

	public static By getSubSectionAltTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	public static By getSubSecPlusIcon() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='nav-section__name js-nav-section-name']/span[@class='nav-section__expand-icon']");
	}

	public static By getHamFlyout() {
		return WrapperMethods.locatorValue(Locators.CSS, ".menu-collapse");
	}

	public static By getIntlRadiobutton() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav div.edition-picker form > label [data-type='edition']");
	}

	public static By getUSRadiobutton() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav div.edition-picker form > label [data-type='www']");
	}

	public static By getMobileCurrentEdition() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'edition-picker__current-edition')]");
	}

	public static By getMobileIntlRadiobutton() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('nav-expanded')//div[contains(@class,'edition-picker')]//label/input[contains(@data-type,'edition')]");
	}

	public static By getMobileUSRadiobutton() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"id('nav-expanded')//div[contains(@class,'edition-picker')]//label/input[contains(@data-type,'www')]");
	}

	public static By getFooterCopyright() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.copyright");
	}

	public static By getFooterCopyrightLink() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.copyright a");
	}

	public static List<WebElement> getFooterLinks() {
		return WrapperMethods.locateElements(Locators.CSS, ".m-legal__list__item > a");
	}

	public static By getVideoCarouselNext() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-next']");
	}

	public static By getVideoCarouselPrev() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-prev']");
	}

	public static By getVideoSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.pg-body__social");
	}

	public static By getVideoSocialIconsElem1() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-email");
	}

	public static By getVideoSocialIconsElem2() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-facebook");
	}

	public static By getVideoSocialIconsElem3() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-twitter");
	}

	public static By getVideoSocialIconsElem4() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-share");
	}

	public static By getVideoSocialIconsElem5() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-messenger");
	}

	public static By getVideoShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".gig-simpleShareUI");
	}

	public static By getVideoShareClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".gig-simpleShareUI-closeButton");
	}

	/**
	 * returns Travel Section Page Title
	 */
	public static By getSectionTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header h2.zn-header__text-page_header");
	}

	public static By getSectionTitles() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__top h1");
	}

	public static By feedbackName() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/label[1]");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/div[1]/label");
	}

	public static By feedbackEmail() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/label[2]");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/div[2]/label");
	}

	public static By feedbackThoughts() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/label[3]");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/div[3]/label");
	}

	public static By feedbackAddComments() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/label[4]");
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='js-feedback-form']/div[@class='feedback-form__fields']/div[4]/label");
	}

	public static By feedbackNameInput() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-form-name");
	}

	public static By feedbackEmailInput() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-form-email");
	}

	public static By feedbackThoughtsInput() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-form-thoughts");
	}

	public static By feedbackCommentsInput() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-form-comments");
	}

	public static By feedbackSubmit() {
		return WrapperMethods.locatorValue(Locators.ID, "js-feedback-send");
	}

	public static By feedbackConfirm() {
		return WrapperMethods.locatorValue(Locators.ID, "js-feedback-message");
	}

	public static List<WebElement> feedbackNameWarn() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/div[1]/span"));
	}

	public static List<WebElement> feedbackEmailWarn() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/div[2]/span"));
	}

	public static List<WebElement> feedbackThoughtsWarn() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/div[3]/span"));
	}

	// Stud
	public static By feedbackNameInputStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"feedback-studentnews-name");
		return WrapperMethods.locatorValue(Locators.ID, "feedback-cnn10-name");
	}

	public static By feedbackEmailInputStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"feedback-studentnews-email");
		return WrapperMethods.locatorValue(Locators.ID, "feedback-cnn10-email");
	}

	public static By feedbackPhoneInputacStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"feedback-studentnews-phone");
		return WrapperMethods.locatorValue(Locators.ID, "feedback-cnn10-phone");
	}

	public static By feedbackCommentsInputStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"feedback-studentnews-comments");
		return WrapperMethods.locatorValue(Locators.ID, "feedback-cnn10-comments");
	}

	public static By feedbackSchoolInputStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"feedback-studentnews-school");
		return WrapperMethods.locatorValue(Locators.ID, "feedback-cnn10-school");
	}

	public static By feedbackSendInputStud() {
		// return
		// WrapperMethods.locatorValue(Locators.ID,"js-studentnews-send");
		return WrapperMethods.locatorValue(Locators.CSS, ".feedback-form__button");
	}

	// 360
	public static By feedbackNameInputac360() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-ac360-name");
	}

	public static By feedbackEmailInputac360() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-ac360-email");
	}

	public static By feedbackPhoneInputac360() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-ac360-phone");
	}

	public static By feedbackCommentsInputac360() {
		return WrapperMethods.locatorValue(Locators.ID, "feedback-ac360-comments");
	}

	public static By feedbackSendInputac360() {
		return WrapperMethods.locatorValue(Locators.ID, "js-ac360-send");
	}

	public static WebElement feedbackTitle() {
		return driver.findElement(By.cssSelector(".feedback-headline"));
	}

	public static By feedbackNameWarn360() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='js-ac360-form']/div[2]/span[1]");
	}

	public static By feedbackEmailWarn360() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='js-ac360-form']/div[2]/span[2]");
	}

	public static By feedbackCommentWarn360() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='js-ac360-form']/div[2]/span[3]");
	}

	public static List<WebElement> feedbackNameWarnStud() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/span[1]"));
	}

	public static List<WebElement> feedbackSchoolWarnStud() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/span[2]"));
	}

	public static List<WebElement> feedbackEmailWarnStud() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/span[3]"));
	}

	public static List<WebElement> feedbackCommentWarnStud() {
		return driver.findElements(By.xpath("//div[@class='feedback-form__fields']/span[4]"));
	}

	public static By getEmailShareTo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[contains(@id,'showShareUI_tbFriendsEmail')]");
	}

	public static By getEmailShareToUr() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[contains(@id,'showShareUI_tbYourEmail')]");
	}

	public static By getEmailShareToMsg() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//textarea[contains(@id,'showShareUI_tbMessage')]");
	}

	public static By getSendEmail() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@onclick,'sendEmail')]");
	}

	public static By getSendEmailCaptcha() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//iframe[contains(@id,'showShareUI_captcha')]");
	}

	public static By getVideoPinnedNow() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.media__caption.el__storyelement__title.fade-in > span.pinCurrentPlay");
	}

	public static By getVideoPinnedTrunc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.media__caption.el__storyelement__title.fade-in > span.el__storyelement__header.is-truncated");
	}

	public static By hp10Header() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__title");
	}

	public static By hp10FreePreview() {
		return WrapperMethods.locatorValue(Locators.CSS, ".media .cnngo-free-preview");
	}

	public static By hp10FreePreviewPlayButton() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".media .cnngo-free-preview .el__video__play-button.js-el__video__play-button");
	}

	public static By hp10FreePreviewHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.cd__headline-text");
	}

	public static By hp10FreePreviewSpinner() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnngo-loading-slate div.spinner");
	}

	public static By hp10FreePreviewContainer() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta .cobranding-container");
	}

	public static By hp10FreePreviewCoBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta .cobranding");
	}

	public static By hp10FreePreviewCoBrandingImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta .cobranding img");
	}

	public static By hp10FreePreviewLinkImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta > a .image.vertical-center");
	}

	public static By hp10FreePreviewLinkText() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta > a .text.vertical-center");
	}

	public static By hp10FreePreviewLinkArrow() {
		return WrapperMethods.locatorValue(Locators.CSS, ".video-slate > .persistent-cta > a .arrow.vertical-center");
	}

	public static By hp10FreePreviewCountDownSpan() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnngo-free-preview .video-slate .countdown-container span");
	}

	public static By hp10FreePreviewCountDown() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".cnngo-free-preview .video-slate .countdown-container span.countdown");
	}

	public static By hp10FreePreviewCloseButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".slates-wrapper.animate-show .video-slate .close-button");
	}

	public static By hp10VideoInMobile() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='deeplink']");
	}

	public static By afterExpire() {
		return WrapperMethods.locatorValue(Locators.ID, "mvpdpicker");
	}

	public static By loginProvider() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//ul[contains(@class,'mvpdsbylogo')]/li[contains(@data-mvpdid,'Cablevision')]");
	}

	public static By getUsername() {
		return WrapperMethods.locatorValue(Locators.CSS, "#IDToken1");
	}

	public static By getPassword() {
		return WrapperMethods.locatorValue(Locators.CSS, "#IDToken2");
	}

	public static By getLogin() {
		return WrapperMethods.locatorValue(Locators.CSS, "#signin_button");
	}

	public static By getVideoElementPinned() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-inbetweener-pinner--type1");
	}

	public static By getRadarWarnings() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='storm-nav-0']");
	}

	public static By getRadarWatches() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='storm-nav-1']");
	}

	public static By getRadarForecast() {
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//div[@id='trackervid-control']");
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='storm-nav-2']");
	}

	public static By feedbackRaido_btn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//ul/li/label/input[@value='other']");
	}

	public static By getPoliticsStyleFooterCopyright() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.m-footer__copyright");
	}

	/**
	 * returns Terms of Service Consent Close button
	 */
	public static By getPoliticsTermsConsentClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cnn-politics-dialog__close.circle");
	}

	public static By getEnterlocationtextbox() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@name='weather-local-search']");
	}

	public static By getForecastbutton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@class='weather__local-query__search-button']");
	}

	public static By GetLocationDropdown() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='tt-dropdown-menu']/div");
	}

	public static By getdefaultlocation() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='zn-top ']//h2[@class='zn-header__text']");
	}

	public By MakeDefault_Btn() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__stored-locations']/div[@class='weather__conditions weather__conditions--location js-weather__conditions--recent--locations ']/div[@class='weather__stored-default']//span[text()='Make Default']");
	}

	public By MakeDefault_State() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='weather__stored-locations']/div[@class='weather__conditions weather__conditions--location js-weather__conditions--recent--locations ']/h4[@class='weather__conditions-title']");
	}

	public static By idtxt() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@id='IDToken1']");
	}

	public static By idtPass() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//input[@id='IDToken2']");
	}

	public static By signin() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@id='signin_button']");
	}

	public static By getOptimum() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//li[@title='Optimum']");
	}

	public static By getLoginTxt() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(text(),'Login')]");
	}

	public static List<WebElement> getLoginTxt1() {
		return  WrapperMethods.locateElements(Locators.XPATH,"//span[contains(text(),'Login')]");
	}
	public static By getEntertainmentlogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='logo-entertainment logo-entertainment--header']");
	}

	public static By getmoreentertainmentpage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'menu-toggle')]");
	}

	public static By entertainmentsubsection() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='entertainment-drawer']");
	}

	public static By getAnimate() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='el__leafmedia el__leafmedia--animation']/video");
	}

	public static By getAnimateFullWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='el__image--fullwidth js__image--fullwidth']//video");
	}

	public static By getmoneylogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@alt='CNN Media logo']");
	}

	public static By getAnimateStandWidth() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='el__image--standard js__image--standard']//video");
	}

}