package um.testng.test.pom.functions;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.SpecialPage;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class SpecialPageFunctions {
	
	public void testshowpage() {
		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}

		try {

			WrapperMethods.verifyElement(SpecialPage.getSpecialPageLogo(), "Shows page logo");

			MethodDef.brokenLinkValidation(
					WrapperMethods.getWebElement(SpecialPage.getSpecialPageLogo()).getAttribute("src"));

			WrapperMethods.contains_Text_Attribute(SpecialPage.getSpecialPageLogo(), "src", "large",
					"The page logo of special page has large cut",
					"The page logo of special page is not pulling large cut");
			WrapperMethods.contains_Text_Attribute(SpecialPage.getSpecialPageImage(), "data-src-xsmall", "medium",
					"The image in the  special page has medium tease and the value is : ",
					"The image in the special page is not pulling medium tease");

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in testing the special Page elements");
		}

	}

	public static void testBranding() {
		try {
			UMReporter.log(LogStatus.INFO,"Info: Special page Branding");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBranding(),
					"the branding for the video page is displayed", "the branding for the video page is not displayed");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBrandingImg(),
					"the branding for the video is displayed", "the branding for the video is not displayed");
			WrapperMethods.isDisplayed(VideoLeafPage.getVideoBrandingLogo(),
					"the branding logo for the video is displayed", "the branding logo for the video is not displayed");
			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(VideoLeafPage.getVideoBrandingLink()).getAttribute("href").isEmpty(),
					"the branding link for the video is displayed and the href is:  "
							+ WrapperMethods.getWebElement(VideoLeafPage.getVideoBrandingLink()).getAttribute("href"),
					"the branding link for the video is not displayed");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "error in accessing the video page branding element");
			UMReporter.log(LogStatus.FAIL,E.getMessage());
		}
	}
}
