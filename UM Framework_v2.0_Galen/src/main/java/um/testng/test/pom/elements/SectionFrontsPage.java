package um.testng.test.pom.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class SectionFrontsPage extends GalleryPage {

	public static By getFooterWeather() {
		return WrapperMethods.locatorValue(Locators.CSS, ".weather__footer-content");
	}

	public static By getFooterWeatherValue() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el-weather__footer-temperature");
	}

	public static By getFooterWeathericon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-icon-wrapper");
	}

	public static By getFooterWeatherLoc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-weather__footer-location");
	}

	public static By getFooterCopyright() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.copyright");
	}

	public static By getFooterCopyrightLink() {
		return WrapperMethods.locatorValue(Locators.CSS, "span.copyright a");
	}

	public static By getCurrentsection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__name > a");
	}

	public static By getCurrentsectionDropdown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__name span.nav-section__expand-icon");
	}

	public static By getCurrentsectionSelect() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__submenu-active");
	}

	public static By getCurrentPageHeader() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	public static By getTechCurrentsection() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='metadata-header__title']");
	}

	public static By getCurrentSubsection() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-section-submenu");
	}

	public static By getEntertainmentlogo() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a[@class='logo-entertainment logo-entertainment--header']");
	}

	public static By getmoreentertainmentpage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'menu-toggle')]");
	}

	public static By entertainmentsubsection() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='entertainment-drawer']");
	}

	public static By getSectionContent() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-no-rail");
	}

	// //

	public ArrayList<String> getArticlelinks() {
		ArrayList<String> articles = new ArrayList<String>();
		// List<WebElement> elements =
		// go().findElements(By.xpath("//article[starts-with(@class, 'cd
		// cd--card cd--article cd--idx-') and boolean(@data-vr-contentbox)]");
		List<WebElement> elements = WrapperMethods.locateElements(Locators.CSS,
				"article[class^='cd cd--card cd--article cd--idx-']");
		// articles.add("/");
		for (WebElement element : elements) {
			if ((!articles.contains((element.getAttribute("data-vr-contentbox"))))
					&& (!(element.getAttribute("data-vr-contentbox").isEmpty()))) {
				articles.add(element.getAttribute("data-vr-contentbox"));
			}
		}
		return articles;
	}

	/**
	 * gets all the galleries present in the specified section page
	 */
	public ArrayList<String> getGalleriesLinks() {
		ArrayList<String> galleries = new ArrayList<String>();
		List<WebElement> elements = WrapperMethods.locateElements(Locators.XPATH, "//a[contains(@href,'/gallery/')]");
		for (WebElement element : elements) {
			galleries.add(element.getAttribute("href"));
		}
		return galleries;
	}

	/**
	 * gets all the elements related to the specified section
	 */
	public List<WebElement> getAllSectionLinks() {
		List<WebElement> elements = WrapperMethods.locateElements(Locators.XPATH,
				"//section//a[boolean(@href) and starts-with(@href,'/') and contains(@href,'.htm')]");
		return elements;
	}

	/**
	 * gets the section link of the particular section which starts with href
	 */
	public static WebElement getSectionLink() {
		List<WebElement> elements = WrapperMethods.locateElements(Locators.XPATH,
				"//section//a[boolean(@href) and starts-with(@href,'/') and contains(@href,'.htm')]");
		return elements.get(0);
	}

	/**
	 * gets the sponsor Nativ logo on the section page
	 */
	public static By getSponsorNativoLogo() {
		By element = WrapperMethods.locatorValue(Locators.CSS, "#ntv-logo");
		return element;
	}

	/**
	 * gets the sponsor content logo on the section page
	 */
	public static By getSponsorContentLogo() {
		By element = WrapperMethods.locatorValue(Locators.CSS, ".sponsor-article-author-logo");
		return element;
	}

	/**
	 * gets the social share icons - gigya bar
	 * 
	 * @param d
	 *            - instance of webdriver
	 */
	public static By getGigyaBar() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_1_gig_containerParent");
	}

	/**
	 * gets the first element from the gigya bar which may be facebook or
	 * twitter
	 * 
	 * @param d
	 *            - instance of webdriver
	 */
	public static By getGigyaBarElem1() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_0-reaction0");
	}

	/**
	 * gets the second element from the gigya bar which may be facebook or
	 * twitter
	 * 
	 * @param d
	 *            - instance of webdriver
	 */
	public static By getGigyaBarElem2() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_1-reaction0");
	}

	/**
	 * gets the third element from the gigya bar which may be facebook or
	 * twitter
	 * 
	 * @param d
	 *            - instance of webdriver
	 */
	public static By getGigyaBarElem3() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_2-reaction0");
	}

	/**
	 * gets the fourth element from the gigya bar which may be facebook or
	 * twitter
	 * 
	 * @param d
	 *            - instance of webdriver
	 */
	public static By getGigyaBarElem4() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_3-reaction0");
	}

	/**
	 * gets all the zone titles in the section page
	 */
	@FindBy(css = "h2.zn-header__text")
	public List<WebElement> zoneHeaders;

	/**
	 * Page Top Banner ad
	 */
	public static List<WebElement> topBannerAd() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad.ad--epic div[class*='ad-ad_bnr_atf_'] div iframe");
	}

	/**
	 * Zone ads Desktop
	 */
	public static List<WebElement> zoneAdDesktop() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad.ad--epic div[data-ad-position='desktop'] iframe");
	}

	/**
	 * Zone ads Mobile
	 */
	public static List<WebElement> zoneAdMob() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad.ad--epic div[data-ad-position='mobile'] iframe");
	}

	/**
	 * Zone ads Tablet
	 */
	public static List<WebElement> zoneAdTab() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad.ad--epic div[data-ad-position='tablet'] iframe");
	}

	/**
	 * Page Footer Banner ad
	 */
	public static List<WebElement> footerBannerAd() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad.ad--epic div[class*='ad-ad_bnr_btf_'] div iframe");
	}

	/**
	 * Inner Zone - ads
	 */
	public List<WebElement> zoneAds() {
		return WrapperMethods.locateElements(Locators.CSS, ".ad--desktop iframe");
	}

	/**
	 * title of the section page
	 */
	public static By getHeaderTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	/**
	 * gets the dropdown picker for the section
	 */
	public static By getHeaderPicker() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-pg-header__subsections--select");
	}

	/**
	 * once the picker is clicked, the arrow shape changes . it gets the whether
	 * the picker is clicked
	 */
	public static By getHeaderPickerArrowUp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-pg-header__subsections--select.el-drop-down__arrow-up");
	}

	/**
	 * list of items present in the dropdown
	 */
	public List<WebElement> getDropDown() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class,'el-drop-down__list__container')]/ul[contains(@class,'js-el-drop-down__list')]/li[contains(@class,'js-el-drop-down__list-item')]/a");
	}

	/**
	 * gets the feedcard present in the section page
	 */
	public static By getFeedCard() {
		return WrapperMethods.locatorValue(Locators.CSS, ".t-green");
	}

	/**
	 * gets the title of the feed card
	 */
	public static By getFeedCardTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".t-green h2");
	}

	/**
	 * @param d
	 *            - instance of the webdriver lists the articles present in the
	 *            feedcard
	 */
	public static List<WebElement> getFeedCardList() {
		return WrapperMethods.locateElements(Locators.CSS, ".t-green li");
	}

	/**
	 * @param d
	 *            - instance of the webdriver lists the overlay number on the
	 *            images
	 */
	public static List<WebElement> getFeedCardOverlay() {
		return WrapperMethods.locateElements(Locators.CSS, ".t-green li div.cd__overlay");
	}

	/**
	 * @param d
	 *            - instance of the webdriver lists the image of the artilce
	 *            displayed in the feedcard
	 */
	public static List<WebElement> getFeedCardImg() {
		return WrapperMethods.locateElements(Locators.CSS, ".t-green li img");
	}

	/**
	 * gets the container which is only hyperlinked in the section page
	 */
	public static By getHyperlinkedContainer() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn__column--idx-0 a.cn__title--link");
	}

	/**
	 * gets the zone which is only hyperlinked in the section page
	 */
	public static By getHyperlinkedZone() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn__background--content-relative");
	}

	/**
	 * gets the href of the zone which is only hyperlinked in the section page
	 */
	public static By getHyperlinkedZoneHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn__background--content-relative a");
	}

	/**
	 * gets the Politics logo from the header of the page
	 */
	public static By getPoliticsLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-header div.logo-links a.logo-links__politics");
	}

	/**
	 * gets the serach field from the header of the page
	 */
	public static By getSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, ".utilities__search div.search");
	}

	/**
	 * gets the serach button from the header of the page
	 */
	public static By getSearchButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search__field button ");
	}

	public static By getPoliticsContainerTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.column h2.cn__title");
	}

	public static By getPoliticsContainerArrowTitle() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='CNN Video'] h2[data-analytics='Being%20Moody_list-hierarchical-xs_']");
	}

	/**
	 * gets the CNN red logo from the header of the style page
	 */
	public static By getStyleCnnRedLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo a.logo-cnn");
	}

	/**
	 * gets the Style logo from the header of the style page
	 */
	public static By getStyleLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo a.logo-style");
	}

	/**
	 * gets the Back to CNN logo from the header of the style page
	 */
	public static By getBacktoCNNLogo() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, ".cnn-header-link");
	}

	/**
	 * gets the social icons from the header of the style page
	 */
	public static By getSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-links--desktop");
	}

	/**
	 * gets the fb icons from the header of the style page
	 */
	public static By getSocialFbIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon");
	}

	/**
	 * gets the fb icon herf from the header of the style page
	 */
	public static By getSocialFbIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon a");
	}

	/**
	 * gets the twitter icons from the header of the style page
	 */
	public static By getSocialTwIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon--twitter");
	}

	/**
	 * gets the twitter herf from the header of the style page
	 */
	public static By getSocialTwIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon--twitter a");
	}

	/**
	 * gets the instagram icon from the header of the style page
	 */
	public static By getSocialInstIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon--instagram");
	}

	/**
	 * gets the instagram herf from the header of the style page
	 */
	public static By getSocialInstIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--desktop div.metadata-header__follow-icon--instagram a");
	}

	/**
	 * gets the footer ad of the style page
	 */
	public static By getFooterAd() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-no-rail >div.ad > div.ad-refresh-adbody > div  iframe");
	}

	// Mobile
	/**
	 * gets the social icons from the header of the style page :mobile
	 */
	public static By getMobileSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-links--mobile");
	}

	/**
	 * gets the facebook icon from the header of the style page:mobile
	 */
	public static By getMobileSocialFbIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon");
	}

	/**
	 * gets the facebook href from the header of the style page:mobile
	 */
	public static By getMobileSocialFbIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon a");
	}

	/**
	 * gets the twitter icon from the header of the style page:mobile
	 */
	public static By getMobileSocialTwIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon--twitter");
	}

	/**
	 * gets the twitter href from the header of the style page:mobile
	 */
	public static By getMobileSocialTwIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon--twitter a");
	}

	/**
	 * gets the instagram icon from the header of the style page:mobile
	 */
	public static By getMobileSocialInstIcon() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon--instagram");
	}

	/**
	 * gets the instagram href from the header of the style page:mobile
	 */
	public static By getMobileSocialInstIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".metadata-header__follow-links--mobile div.metadata-header__follow-icon--instagram a");
	}

	/**
	 * gets the search toggle button , on clicking search field displays
	 */
	public static By getsearchtoggle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "js-search-toggle");
	}

	/**
	 * gets the hamburger icon , on clicking opens the dropdown
	 */
	public static By getHam() {
		return WrapperMethods.locatorValue(Locators.CSS, ".menu-collapse");
	}

	/**
	 * gets the gallery element
	 */
	public static By getGallery() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage");
	}

	/**
	 * gets the gallery content of each image
	 */
	public static By getGalleryItemContent() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// div.cd__content");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active div.cd__content");
	}

	/**
	 * gets the gallery content title of image
	 */
	public static By getGalleryContentTitle() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// div.cd__content");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active div.cd__content");
	}

	/**
	 * gets the gallery content title href of each image
	 */
	public static By getGalleryContentTitleHref() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// div.cd__content a");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active div.cd__content a");
	}

	/**
	 * gets the gallery content Headline of each image
	 */
	public static By getGalleryItemContentHeadline() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// div.cd__content h3");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active div.cd__content h3");
	}

	/**
	 * gets the gallery content Headline href of each image
	 */
	public static By getGalleryItemContentHeadlineHref() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// div.cd__content h3 a");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active div.cd__content h3 a");
	}

	public static By getrofileandleadershiptitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "h2.zn-header__text");
	}

	/**
	 * gets the gallery active image
	 */
	public static By getGalleryItemImg() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// img.owl-lazy");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active img.owl-lazy");
	}

	/**
	 * gets the gallery active image href
	 */
	public static By getGalleryItemImgHref() {
		// return WrapperMethods.locatorValue(Locators.CSS,".owl-height
		// .owl-item.active
		// a");
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage .owl-item.active a");
	}

	/**
	 * gets the gallery scroll dot which is present below the gallery
	 */
	public static By getGalleryDot() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-dots div.active");
	}

	/**
	 * gets the gallery Prev toggle
	 */
	public static By getStylePageGalleryPrev() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-nav div.owl-prev");
	}

	/**
	 * gets the gallery next toggle
	 */
	public static By getStylePageGalleryNext() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-nav > div.owl-next");
	}

	// Style Section: Outer Gallery

	public static By getStyleOuterGallery() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage-outer");
	}

	public static By getOuterGalleryItemContentHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage-outer .owl-item.active div.cd__content h3");
	}

	public static By getOuterGalleryItemContentHeadlineHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage-outer .owl-item.active div.cd__content h3 a");
	}

	public static By getOuterGalleryItemImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage-outer .owl-item.active img.owl-lazy");
	}

	public static By getOuterGalleryItemImgHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-stage-outer .owl-item.active a");
	}

	// Branding elements

	/**
	 * checks for the hero3 zone present in the section page
	 */
	public static By getHero3() {
		return WrapperMethods.locatorValue(Locators.CSS, "section[data-zone-label='Hero 3']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getHero3BrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Hero 3'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getZone1TargetURL() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div/div/h2/a");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getHero3ZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[contains(@id, '-zone-1')]/div/div/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPriority2ZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/a/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getFullBleedZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/a/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPriority1ZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getBalancedZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getSuperHeroZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@class, 'zn--idx-0')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/a/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getRightPriority1ZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getSingleColumnZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getHero3ReverseZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPriorityCenterZoneBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div[@class= 'zn-top__background']/div/img");
	}

	/**
	 * checks for the Priority + 1 zone present in the section page
	 */
	public static By getPriority1() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority + 1']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPriority1BrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority + 1'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Priority + 2 zone present in the section page
	 */
	public static By getPriority2() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority + 2']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPriority2BrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority + 2'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Priority Center zone present in the section page
	 */
	public static By getPrioritycenter() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority Center']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getPrioritycenterBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Priority Center'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Hero 3 Reverse zone present in the section page
	 */
	public static By getHero3Reverse() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Hero 3 Reverse']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getHero3ReverseBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Hero 3 Reverse'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Super hero zone present in the section page
	 */
	public static By getSuperHero() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Superhero']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getSuperHeroBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Superhero'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Single column zone present in the section page
	 */
	public static By getSingleColoum() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Single column']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getSingleColoumBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Single column'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the balanced zone present in the section page
	 */
	public static By getBalanced() {
		return WrapperMethods.locatorValue(Locators.CSS, "section[data-zone-label='Balanced']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getBalancedBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Balanced'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Right Priority + 1 zone present in the section page
	 */
	public static By getRightPriority1() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Right Priority+1 ']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getRightPriority1BrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Right Priority+1 '] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the Full Bleed zone present in the section page
	 */
	public static By getFullBleed() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Full Bleed']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getFullBleedBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"section[data-zone-label='Full Bleed'] div.zn-top__background div.zn__background--img-tag img");
	}

	/**
	 * checks for the List full card present in the section page
	 */
	public static By getListFullcardbranding() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='List%20Full%20Cards_list-large-horizontal_article_']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getListFullcardbrandingimg() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='List%20Full%20Cards_list-large-horizontal_article_'] div.cd__branding img");
	}

	/**
	 * checks for the Lead Headlines card present in the section page
	 */
	public static By getLeadHeadlinesbranding() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='Lead%20+%20Headlines_list-hierarchical-xs_article_']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getLeadHeadlinesbrandingimg() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='Lead%20+%20Headlines_list-hierarchical-xs_article_'] div.cd__branding img");
	}

	/**
	 * checks for the List full card present in the section page
	 */
	public static By getListFullcardSpacedbranding() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='List%20Full%20Cards%20(spaced)_stack-large-horizontal_article_']:not([class*='t-hidden']");
	}

	/**
	 * gets the branding of the specified zone if exists in the section page
	 */
	public static By getListFullcardSpacedbrandingimg() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div[data-analytics='List%20Full%20Cards%20(spaced)_stack-large-horizontal_article_'] div.cd__branding img");
	}

	// Footer Politics
	public static By getPoliticsFooterLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-links__cnn[data-analytics='footer_logo']");
	}

	public static By getPoliticsFooterPoliticsLink() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo-links__politics[data-analytics='footer_logo']");
	}

	public static By getStyleFooterLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, "a.logo");
	}

	public static By getStyleFooterEditionPickerLink() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.current");
	}

	public static By getStyleFooterWeatherCity() {
		return WrapperMethods.locatorValue(Locators.CSS, "#js-weather__footer span.el-weather__footer-location");
	}

	public static By getStyleFooterWeatherImage() {
		return WrapperMethods.locatorValue(Locators.CSS, "img.el-weather__header-icon");
	}

	public static By getStyleFooterWeatherTemp() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-temp");
	}

	public static By getStyleFooterSearchCNNBox() {
		return WrapperMethods.locatorValue(Locators.ID, "searchInputFooter");
	}

	public static By getStyleFooterSearchButton() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search-input__button");
	}

	// Style Page Footer
	public static By getStyleFooterSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__follow__link");
	}

	public static By getStyleFooterFbIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__follow__link--facebook a");
	}

	public static By getStyleFooterTwIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__follow__link--twitter a");
	}

	public static By getStyleFooterInstIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__follow__link--instagram a");
	}

	public static By skipAd() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#skip>a");
	}

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "a#logo,a.logo-cnn,a._2MUJ4");
	}

	/**
	 * Page Load Condition
	 */
	public static By getstaticPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.nav__container #logo");
	}

	/**
	 * returns Trip Advisor logo
	 */
	public static By getTripAdvisorBlock() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='js-m-partner-hotels']");
	}

	/**
	 * returns Trip Advisor Href
	 */
	public static By getTripAdvisorHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__affiliates a.m-footer__affiliate--tripadvisor");
	}

	/**
	 * returns Trip Advisor Href
	 */
	public static By getPartnerHotelsWidget() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-partner-hotels__wrapper");
	}

	public static By getPartnerHotelImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-partner-hotels__hotel-image");
	}

	public static By getPartnerHotelPlaceholderImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-partner-hotels__wrapper img");
	}

	public static By getPartnerHotelTripAdvisor() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".m-partner-hotels__container div.m-partner-hotels__tripadvisor");
	}

	public static By getHero3ZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div/div/h2/a");
	}

	public static By getPriority2ZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div/div/div/h2/a");
	}

	public static By getFullBleedZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div/div/div/h2/a");
	}

	public static By getPriority1ZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div/div/h2/a");
	}

	public static By getBalancedZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div/div/h2/a");
	}

	public static By getSuperHeroZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@class, 'zn--idx-0')]/div[contains(@class, 'zn-top')]/div/div/div/h2/a");
	}

	public static By getRightPriority1ZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div/div/h2/a");
	}

	public static By getSingleColumnZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div/div/h2/a");
	}

	public static By getHero3ReverseZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'branding-header-content')]/div[contains(@class, 'zn-header')]/h2/a");
	}

	public static By getPriorityCenterZoneHeaderText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'zn-top')]/div/div/div/h2/a");
	}

	public static By getListHeadlinesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getListHeadlinesContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn-banner']/a/h2");
	}

	public static By getListHeadlinesImagesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-1']/ul/h2");
	}

	public static By getLeadHeadlinesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getLeadHeadlinesContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/article/a/h2");
	}

	public static By getListFullCardsContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-1']/ul/h2");
	}

	public static By getLeadHeadlinesNoDescContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[contains(@class, 'zn__containers')]/div[@class= 'column  zn__column--idx-0']/ul/h2");
	}

	public static By getLeadHeadlinesNoDescContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'zn-top']/div[contains(@class, 'zn-top__banner')]/div[@class= 'zn-banner']/a/h2");
	}

	public static By getLeadHeadlineswithImagesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[contains(@class, 'zn__containers')]/div[@class= 'column zn-left-fluid__cn zn__column--idx-1']/ul/h2");
	}

	public static By getLeadPipedHeadlinesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getLeadPipedHeadlinesNoDescContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getLeadPipedHeadlinesContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/ul/article/a");
	}

	public static By getLeadPipedHeadlinesNoDescContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn-banner')]/a/h2");
	}

	public static By getLeadHeadlineswithImagesNoDescContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-1']/ul/h2");
	}

	public static By getGridContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-1']/div[contains(@class, 'cn-grid-small')]/h2");
	}

	public static By getJumbotronContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-1']/div/h2");
	}

	public static By getCarouselStandardContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/div/h2");
	}

	public static By getCarouselStandardContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/div/div[contains(@class, 'carousel-medium-strip')]/div[contains(@class, 'owl-stage-outer')]/div/div[contains(@class, 'owl-item active')]/div/article/a/h2");
	}

	public static By getCarouselPaginatedHeadlinesImagesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/div/h2");
	}

	public static By getCarouselLargeContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-1']/div/h2");
	}

	public static By getCarouselPaginatedHeadlinesImagesContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div[@class= 'column zn__column--idx-0']/div/div[contains(@class, 'carousel-small-paginated')]/div[contains(@class, 'owl-stage-outer')]/div/div[contains(@class, 'owl-item active')]/article/a/h2");
	}

	public static By getCollectionPlayerContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-1']/div/h2");
	}

	public static By getFeaturedShowContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/div/h2");
	}

	public static By getFeaturedShowContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[contains(@class, 'l-container')]/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/div/article/a/h2");
	}

	public static By getVStripHeadlinesImagesContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getVStripHeadlinesImages11ContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-1']/ul/h2");
	}

	public static By getVStripHeadlinesImagesContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn-banner']/a/h2");
	}

	public static By getVStripHeadlinesImages34ContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-1']/ul/h2");
	}

	public static By getVStripFullCardsContainerTitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/h2");
	}

	public static By getVStripFullCardsContainerFirstCardBannerText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[contains(@id, '-zone-1')]/div[@class= 'l-container']/div[@class= 'zn__containers']/div[@class= 'column zn__column--idx-0']/ul/li/article/a/h2");
	}

	public static By getAppiaAD() {
		return WrapperMethods.locatorValue(Locators.CSS, ".appia-container");
	}

	public static By getYieldMoAd() {
		return WrapperMethods.locatorValue(Locators.CSS, ".ym_format6");
	}

	public static By getStyleFooterTopLinkcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-footer__top__link");
	}

	public static By getStyleHeaderSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS, ".utilities ul.social");
	}

	public static By getStyleHeaderFbIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".utilities ul.social li.social__link--facebook a");
	}

	public static By getStyleHeaderTwIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".utilities ul.social li.social__link--twitter a");
	}

	public static By getStyleHeaderInstIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".utilities ul.social li.social__link--instagram a");
	}

	public static By getPoliticsSearch() {
		return WrapperMethods.locatorValue(Locators.CSS, ".search__field");
	}

	public static List<WebElement> AppiaStylepageElement() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='appia-container js-appia adfuel-rendered']");
	}

	public static By getheadereditionpicsymbol() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.edition-picker__current-edition span.nav-section__expand-icon");
	}

	public static By getMobHeadereditionpicsymbol() {
		return WrapperMethods.locatorValue(Locators.CSS, "#nav-expanded span.nav-section__expand-icon");
	}

	public  static List<WebElement> getHeaderAllSections() {
		// return go().findElements(By.xpath("//header//a");
		return WrapperMethods.locateElements(Locators.CSS,
				".nav-menu-links__link,div.buckets.drawer a,nav.header-navigation__container a,div.menu a");
	}

	public static By navcolorstrip() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.nav__color-strip");
	}

	public static List<WebElement> getFooterAllSections() {
		return WrapperMethods.locateElements(Locators.CSS,
				"footer.l-footer a,div.footer-cnn-politics__container a,div#footer a");
	}

	public static By owlPrev() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='js-owl-carousel owl-carousel carousel--full carousel-large-strip owl-loaded owl-drag']//div[@class='owl-prev']");
	}

	public static By owlNext() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='js-owl-carousel owl-carousel carousel--full carousel-large-strip owl-loaded owl-drag']//div[@class='owl-next']");
	}

	public static By carouselpagination() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.owl-item.active div.media");
	}

	public static By carouselpaginationBackground() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.owl-item.active div.cd__content");
	}
	
	/**
	 * returns the footer copyright links
	 */
	public static Map<String, List<String>> getStyleFooterLastLineLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "//";

		map.put(path + "a[@data-analytics='footer_terms-of-use']", addValuesMap(map, "TERMS OF USE", "/terms"));
		map.put(path + "a[@data-analytics='footer_privacy-policy']", addValuesMap(map, "PRIVACY POLICY", "/privacy"));
		map.put(path + "a[@data-analytics='footer_adchoices']", addValuesMap(map, "ADCHOICES", "/style#"));
		return map;

	}
	public static List<String> addValuesMap(Map<String, List<String>> map, String val1, String val2) {
		List<String> v = new ArrayList<String>();
		v.add(val1);
		v.add(val2);
		return (v);
	}
	/**
	 * returns the copyright links : Turner Broadcasting System, Inc.
	 */
	public static Map<String, List<String>> getStyleFooterTurnBroadCastLink() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "//";

		map.put(path + "a[@data-analytics='footer_turner-broadcasting-system-inc']",
				addValuesMap(map, "Turner Broadcasting System, Inc.", "//www.turner.com"));
		return map;
	}
	
	/**
	 * *returns ABOUT Link : ABOUT
	 * 
	 * @return
	 */
	public static Map<String, List<String>> getStyleFooterAboutLink() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "//";

		map.put(path + "a[@class='m-footer__about__link']", addValuesMap(map, "ABOUT", "/style/about"));
		return map;
	}

}