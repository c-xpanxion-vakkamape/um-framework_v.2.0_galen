package um.testng.test.pom.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class VideoLandingPage {

	public static WebElement getVideoCarouselNext() {
		return WrapperMethods.locateElement(Locators.CSS,
				"div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-next']");
	}

	public static WebElement getVideoCarouselPrev() {
		return WrapperMethods.locateElement(Locators.CSS,
				"div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-prev']");
	}

	public static By getVideoSocialIcons() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.pg-body__social");
	}

	public static By getVideoSocialIconsElem1() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-email");
	}

	public static By getVideoSocialIconsElem2() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-facebook");
	}

	public static By getVideoSocialIconsElem3() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-twitter");
	}

	public static By getVideoSocialIconsElem4() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.m-share__bar div.gig-button-container-share");
	}

	public static By getVideoShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".gig-simpleShareUI");
	}

	public static By getVideoShareClose() {
		return WrapperMethods.locatorValue(Locators.CSS, ".gig-simpleShareUI-closeButton");
	}
	public static By gettheoPlayer() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//canvas[@class='theoplayer-vr']");
	}
	public static By gettheoPlayerPlayBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='vjs-play-control vjs-control  vjs-playing']");
	}
	
	public static By getReplayBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='video__end-slate js-video__end-slate video__end-slate--active']//a[@class='js-video__end-slate__replay-text video__end-slate__replay-text']");
	}

}
