package um.testng.test.pom.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class ProfilesPage extends BasePage {

	public static By getProfileName() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd__profile-name");
	}

	public static By getProfileTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd__profile-title");
	}

	public static By getProfileSocElem() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.social-description__follow-icon:nth-child(1) > a:nth-child(1)");
	}

	/**
	 * Waits for the page to finish loading before continuing.
	 */
	/*
	 * @Override protected void waitForLoad() { wait.until(new
	 * ExpectedCondition<WebElement>() {
	 * 
	 * @Override public static By apply() { if
	 * (isElementExists(By.cssSelector(".cd__profile-name")) {
	 * Reporter.log("<BR>PASSED: Profile Page is loaded successfully<BR>"); }
	 * return WrapperMethods.locatorValue(Locators.CSS,".cd__profile-name"); }
	 * }); }
	 */

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd__profile-name");
	}

	/**
	 * returns the follow button for the specified profile
	 */
	public static By getProfileFollow() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el-button");
	}

	/**
	 * returns the facebook link for the specified profile
	 */
	public static By getProfilePageFbLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".social-description__follow-icon--facebook a");
	}

	/**
	 * returns the twitter link for the specified profile
	 */
	public static By getProfilePageTwLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".social-description__follow-icon--twitter a");
	}

	/**
	 * returns the LinkedIn link for the specified profile
	 */
	public static By getProfilePageLILink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".social-description__follow-icon--linked-in a");
	}

	/**
	 * returns the Tumblr link for the specified profile
	 */
	public static By getProfilePageTumblrLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".social-description__follow-icon--tumblr a");
	}

	/**
	 * returns the Instagram link for the specified profile
	 */
	public static By getProfilePageInstaLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".social-description__follow-icon--instagram a");
	}

	/**
	 * Branding background for the profile Pages
	 */
	public static By getProfileBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper");
	}

	/**
	 * Branding Logo of the specified branding
	 */
	public static By getProfileBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	/**
	 * Short Bio of the Profile Page
	 */
	public static By getProfileShortBio() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd__description__paragraph");
	}

	/**
	 * Full Bio of the Profile Page
	 */
	public static By getProfileFulltBio() {
		return WrapperMethods.locatorValue(Locators.CSS, ".cd__text-body__paragraph");
	}

	/**
	 * Background Image of the Profile Page
	 */
	public static By getBackgroundImage() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	/**
	 * Slug of the Profile Page
	 */
	public static By getProfileSlug() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:url']");
	}

	/**
	 * Image of the Profile Page
	 */
	public static By getProfileImage() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:image']");
	}

}
