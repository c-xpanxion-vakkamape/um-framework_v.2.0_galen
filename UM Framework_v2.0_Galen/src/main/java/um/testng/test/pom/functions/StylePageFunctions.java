package um.testng.test.pom.functions;

import org.openqa.selenium.By;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.pom.elements.StylePage;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class StylePageFunctions extends SectionFrontsPageFunctions {

	public static void testStyleGalleryElements() {

			UMReporter.log(LogStatus.INFO,"<b>Info: Style Gallery Elements</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGallery()).isDisplayed(), "the Gallery is displayed",
					"the Gallery is not displayed");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemImg()).isDisplayed(),
					"the Image is displayed for the gallery item", "the Image is displayed for the gallery item");
			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemImgHref()).getAttribute("href").isEmpty(),
					"the Image href is displayed for the gallery item:  "
							+ WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemImgHref()).getAttribute("href"),
					"the Image href is not displayed for the gallery item");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContent()).isDisplayed(),
					"the Gallery's content is displayed:   " + WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContent()).getText(),
					"the Gallery's content is not displayed ");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGalleryContentTitle()).isDisplayed(),
					"the Gallery's content title is displayed:   " + WrapperMethods.getWebElement(SectionFrontsPage.getGalleryContentTitle()).getText(),
					"the Gallery's content title is not displayed ");
			if (!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryContentTitle()).getText().equals("GALLERY")) {
				WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryContentTitleHref()).getAttribute("href").isEmpty(),
						"the Gallery's content title href is correct:  "
								+ WrapperMethods.getWebElement(SectionFrontsPage.getGalleryContentTitleHref()).getAttribute("href"),
						"the Gallery's content title href is not correct");
			}
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContentHeadline()).isDisplayed(),
					"the Gallery's content headline is displayed : "
							+ WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContentHeadline()).getText(),
					"the Gallery's content headline is not displayed");
			WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContentHeadlineHref()).getAttribute("href").isEmpty(),
					"the Gallery's content headline href is correct:    "
							+ WrapperMethods.getWebElement(SectionFrontsPage.getGalleryItemContentHeadlineHref()).getAttribute("href"),
					"the Gallery's content headline href is not correct");
			WrapperMethods.isDisplayed(SectionFrontsPage.owlPrev(), "prev carousel is displayed",
					"prev carousel is not displayed");
			WrapperMethods.isDisplayed(SectionFrontsPage.owlNext(), "next carousel is displayed",
					"next carousel is not displayed");
			try {
				WrapperMethods.assertIsTrue(!WrapperMethods.getWebElement(SectionFrontsPage.getGalleryDot()).isEnabled(), "the red dot is highlighted",
						"the red dot is not highlighted");
			} catch (Exception e) {

			}
	}
	public static void validateGalleryOutbrain() {
//		try {
			
			String obfull16, obfull4, ob16, ob4;
			
			if (System.getProperty("site").equals("DOM")) {
				obfull16 = "AR_49";
				obfull4 = "AR_50";
				ob16 = "AR_51";
				ob4 = "AR_52";
			} else {
				obfull16 = "AR_8";
				obfull4 = "AR_9";
				ob16 = "AR_10";
				ob4 = "AR_11";
			}
			
			
			
			if (WrapperMethods.getWebElement(StylePage.getGalleryMedia()).getAttribute("data-eq-state")
					.contains("xsmall medium large")) {
				if (WrapperMethods.getWebElement(StylePage.getGalleryCarousel())
						.getAttribute("data-cut-format").contains("16:9")) {
					WrapperMethods.assertIsTrue(
							WrapperMethods.getWebElement(StylePage.getStyleOutbrain())
									.getAttribute("data-widget-id")
									.contains(obfull16),
							"Style Widget ID full 16:9 is correct"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"),
							"Style Widget ID full 16:9 is wrong"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"));
				} else if (WrapperMethods.getWebElement(StylePage.getGalleryCarousel())
						.getAttribute("data-cut-format").contains("4:3")) {
					WrapperMethods.assertIsTrue(
								WrapperMethods.getWebElement(StylePage.getStyleOutbrain())
									.getAttribute("data-widget-id")
									.contains(obfull4),
							"Style Widget ID full 4:3 is correct"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"),
							"Style Widget ID full 4:3 is wrong"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"));
				}
			} else {
				if (WrapperMethods.getWebElement(StylePage.getGalleryCarousel())
						.getAttribute("data-cut-format").contains("16:9")) {
					WrapperMethods.assertIsTrue(
							WrapperMethods.getWebElement(StylePage.getStyleOutbrain())
									.getAttribute("data-widget-id")
									.contains(ob16),
							"Style Widget ID body 16:9 is correct"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"),
							"Style Widget ID body 16:9 is wrong"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"));
				} else if (WrapperMethods.getWebElement(StylePage.getGalleryCarousel())
						.getAttribute("data-cut-format").contains("4:3")) {
					WrapperMethods.assertIsTrue(
							WrapperMethods.getWebElement(StylePage.getStyleOutbrain())
									.getAttribute("data-widget-id")
									.contains(ob4),
							"Style Widget ID body 4:3 is correct"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"),
							"Style Widget ID body 4:3 is wrong"
									+ WrapperMethods.getWebElement(StylePage.getStyleOutbrain()).getAttribute(
											"data-widget-id"));
				}
			}
//		} catch (Exception E) {
//			UMReporter.log(LogStatus.FAIL,"Error in accessing StylePage Gallery Elements "+ E.getMessage());
//		}
	}
	 public static void styleValidation(String baseurl,  String[] domData, int windowWidth) {

	     Boolean dev = MethodDef.chkDeviceHdr(windowWidth);
	     int i = 0;

	     for (String temp : domData) {

	            String[] headItems = temp.split("~");

	            UMReporter.log(LogStatus.PASS,"<b>Element under test " + headItems[0]+"</b>");

	            String newurl = baseurl + headItems[1];
	            MethodDef.navUrl(newurl);
	            UMReporter.log(LogStatus.PASS,newurl);
	            if (dev) {
	                  WrapperMethods.isDisplayed(StylePage.getHamFlyout(),
	                                "Hamburger Icon is present",
	                                "Hamburger Icon is not present");
	                  
	                  WrapperMethods.click( StylePage.getHamFlyout(), "Clicked the Hamburger menu", "Not able to click the Hamburger menu");
	            }

	            try {
	                  BasicStyleValidation(StylePage.getStyleLogo(),
	                		  StylePage.getCurrentSection());
	            } catch (Exception E) {
	            	UMReporter.log(LogStatus.FAIL, "Error in accessing section Elements for StylePage "
	                                              + headItems[0]);
	            }
	     	}

	    }
	 public static void BasicStyleValidation(By styleLogo, By currentSection) {

	    	WrapperMethods.isDisplayed( styleLogo,
	                  "The Style logo is displayed",
	                  "The Style logo is not displayed");

	     // Sub section Selection
	    	WrapperMethods.isDisplayed( currentSection,
	                  "The Current Section is highlighted",
	                  "The Current Section is not highlighted");

	    }
}
