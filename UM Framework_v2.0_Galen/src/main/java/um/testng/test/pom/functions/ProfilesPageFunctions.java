package um.testng.test.pom.functions;

import org.jboss.netty.handler.timeout.TimeoutException;
import org.testng.ITestContext;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.ProfilesPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class ProfilesPageFunctions {

	public void testprofilepage() {
		
		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}

		try {
			if (!System.getProperty("environment").equalsIgnoreCase("REF")) {
				
				WrapperMethods.textNotEmpty(ProfilesPage.getProfileName(),"Profile name");
				WrapperMethods.textNotEmpty(ProfilesPage.getProfileTitle(),"Profile title");
				WrapperMethods.verifyElement(ProfilesPage.getProfileSocElem(), "Profile social Element");		
			}
		} catch (TimeoutException e) {
			UMReporter.log(LogStatus.FAIL, "Error in testing the Profile Page elements");
		}
		
	
		
	}
	
}
