package um.testng.test.pom.functions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.ArticlePage;
import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.pom.elements.VideoLandingPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class VideoLandingPageFunctions {

	public void testVideolandingpage() {

		WebDriver driver = DriverFactory.getCurrentDriver();

		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			WebDriverWait waitvid = new WebDriverWait(driver, 300);

			waitvid.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					WebElement videoPlayer = null;
					try {
						videoPlayer = driver.findElement(By.xpath("//div[text()='Now Playing']"));
						return true;
					} catch (Exception e) {
						return false;
					}
				}
			});

			try {
				WrapperMethods.videoObjectCheck();
			} catch (Exception E) {
				UMReporter.log(LogStatus.INFO, E.getMessage());
				UMReporter.log(LogStatus.WARNING, "Video Flash player enabled - CVP player not tested");
			}
			CommomPageFunctions.testVideosDesktop();
			CommomPageFunctions.testVideosSocialIcons();

		}
	}
	
	public static void pinnedVideoValidations() {
		if (MethodDef.getJsonVideoPinned()) {
			try {
				commonPinnedVideoValidation();
				validatePinnedNowPlaying(ArticlePage.getVideoPinnedNow());
				validatePinnedHeader(ArticlePage.getVideoPinnedTrunc());
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error accessing Pinned Video");
			}
		} else {
			try {
				WrapperMethods.getWebElement(ArticlePage.getVideoElementPinned()).click();
				UMReporter.log(LogStatus.FAIL, "Pinned Video is displayed - But the Michonne config is turned off");
			} catch (Exception E) {
				UMReporter.log(LogStatus.PASS, "Pinned Video is not present as per configuration");
			}
		}
	}

	public static void commonPinnedVideoValidation() {
		try {
			ArticlePage.wait(25);
			WrapperMethods.scrollDown();
			 ArticlePage.wait(15);
			WrapperMethods.scrollDown();
			ArticlePage.wait(15);
			MethodDef.explicitWaitVisibility(ArticlePage.getVideoElementPinned(), 40, "Pinned Element is visible",
					"Pinned element is not visible");
			UMReporter.log(LogStatus.PASS, "Pinned Video is displayed correctly");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned video elements");
		}
	}

	public static void validatePinnedNowPlaying(By videoElementPinned) {
		try {
			MethodDef.explicitWaitVisibility(videoElementPinned, 10, "Pinned Video is available in the page",
					"Pinned Video is not available in the page");
			WrapperMethods.verifyElement(videoElementPinned, "Now Playing Element ");

			UMReporter.log(LogStatus.INFO, "Validating the CSS Property of Now Playing text");

			if (WrapperMethods.getWebElement(videoElementPinned).isDisplayed()) {
				WrapperMethods.verifyCssValue(videoElementPinned, "color", "rgba(227, 0, 0, 1)",
						"Now Playing in Red Colour");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned Now Playing");
		}
	}

	private static void validatePinnedHeader(By videoPinnedTrunc) {
		try {
			WrapperMethods.verifyElement(videoPinnedTrunc, "Truncated Header Element in Pinned Video");
			UMReporter.log(LogStatus.INFO, "Validating the CSS Property of truncated headline");

			if (WrapperMethods.getWebElement(videoPinnedTrunc).isDisplayed()) {
				WrapperMethods.verifyCssValue(videoPinnedTrunc, "max-height", "40px", "Max Height ");
				WrapperMethods.verifyCssValue(videoPinnedTrunc, "width", "200px", "width ");
				WrapperMethods.verifyCssValue(videoPinnedTrunc, "float", "left", "float ");
				WrapperMethods.verifyCssValue(videoPinnedTrunc, "word-wrap", "break-word", "word wrap ");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing basic pinned video header title");
		}
	}

	public static void pinnedVidLocation() {

		MethodDef.explicitWaitVisibility(ArticlePage.getVideoElementPinned(), 40, "Video pinned element is present",
				"Video pinned element is not present");
		UMReporter.log(LogStatus.INFO, "Validating the CSS Property controlling location of the pinned video");

		if (WrapperMethods.getWebElement(ArticlePage.getVideoElementPinned()).isDisplayed()) {
			WrapperMethods.verifyCssValue(ArticlePage.getVideoElementPinned(), "width", "300px", "Width");
			WrapperMethods.verifyCssValue(ArticlePage.getVideoElementPinned(), "height", "168.75px", "height");
			WrapperMethods.verifyCssValue(ArticlePage.getVideoElementPinned(), "left", "px", "left data");
			WrapperMethods.verifyCssValue(ArticlePage.getVideoElementPinned(), "top", "50px", "top");
			WrapperMethods.verifyCssValue(ArticlePage.getVideoElementPinned(), "position", "fixed", "position");
		}
	}

	public static void clickFirstEmbVideo() {

		MethodDef.explicitWaitVisibility(ArticlePage.getfirstEmvideo(), 40, "First Embedded video is present",
				"First embedded video is not present");
		WrapperMethods.click(ArticlePage.getfirstEmvideo(), "Embedded video");
		// article.wait(15);
		MethodDef.explicitWaitVisibility(ArticlePage.getEmVideoElementPlayerElem(), 40,
				"Embedded video element player is visible", "Embedded video element player is not visible");
		MethodDef.explicitWaitVisibility(ArticlePage.getEmVideoElementPlayerVid(), 40,
				"Embedded video element video player  is visible", "Embedded video  player is not visible");
	}

	public static void embVideoposition() {

		UMReporter.log(LogStatus.INFO, "Validating the CSS Property controlling location of the Opened Embedded Video");
		WrapperMethods.assertIsTrue(
				!WrapperMethods.getWebElement(ArticlePage.getEmVideoElementPlayerElem()).getCssValue("padding-bottom")
						.isEmpty()
						&& !WrapperMethods.getWebElement(ArticlePage.getEmVideoElementPlayerElem())
								.getCssValue("padding-bottom").equals("null"),
				"padding-bottom has data", "padding-bottom has no data");
		WrapperMethods.verifyCssValue(ArticlePage.getEmVideoElementPlayerElem(), "position", "relative", "position ");
	}
	
	public static void replayValidation() {
		WrapperMethods.isDisplayed(VideoLandingPage.gettheoPlayer(), "After Loaded the Page THEO Player Video is displayed","After Loaded the Page THEO Player Video is not displayed");
		WrapperMethods.isDisplayed(VideoLandingPage.gettheoPlayerPlayBtn(),"TEHO player video is playing","TEHO player video is not playing");
		WrapperMethods.waitForVisiblity(VideoLandingPage.getReplayBtn(), 120, "Replay button is visible after THEO player video has been finished", "Replay button is visible after THEO player video has been not finished");
		WrapperMethods.isDisplayed(VideoLandingPage.getReplayBtn(), "Replay button is visible after THEO player video has been finished", "Replay button is visible after THEO player video has been not finished");
	}

}
