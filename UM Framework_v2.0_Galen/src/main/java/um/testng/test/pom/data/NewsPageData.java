package um.testng.test.pom.data;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.TestDataProvider;
import um.testng.test.utilities.framework.db.CouchDocHelper;

import com.jayway.jsonpath.JsonPath;

public class NewsPageData {
	private static JSONObject newsDoc;
static{
	String uRL = ConfigProp.getPropertyValue("BaseURL");
	uRL = uRL + TestDataProvider.getData("URL");
	newsDoc = CouchDocHelper.fromURL(uRL);
//	System.out.println("before:::::::::::" + newsDoc);
}
public static JSONArray getNewsLinkBlk(){
//	System.out.println("after:::::::::::" + newsDoc);
	return JsonPath.read(newsDoc, "$.Data.bins.newsBullet");
}

}
