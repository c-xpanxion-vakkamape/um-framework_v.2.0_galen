package um.testng.test.pom.functions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltestpack.EnvironmentHelper;
import um.testng.test.pom.elements.HomePage;
import um.testng.test.pom.elements.PoliticsPage;
import um.testng.test.pom.elements.SectionFrontsPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class PoliticsPageFunctions {
	public static void testPoliticsHeaderElementsGeneral() {

		try {

			UMReporter.log(LogStatus.INFO, "Info: Politics Header Elements");
			WrapperMethods.verifyElement(PoliticsPage.getCNNRedLogo(), "the cnn red logo");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(PoliticsPage.getCnnRedlogo()).getAttribute("href").equals("/"),
					"the cnn red logo has the hyperlink", "the cnn red logo does not have the hyperlink");

			WrapperMethods.verifyElement(PoliticsPage.getPoliticslogo(), "the Politics logo ");
			WrapperMethods.textAttributeNotEmpty(PoliticsPage.getPoliticslogo(), "href", "politics logo hyperlink");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Politics Header elements");
		}
	}

	public static void headerTesting(Map<String, List<String>> footerLinks) {
		Map<String, List<String>> map;
		map = footerLinks;
		splitHrefValidation(map.entrySet());
	}

	public static void splitHrefValidation(Set<Entry<String, List<String>>> entrySet) {
		for (Entry<String, List<String>> entry : entrySet) {
			String path = entry.getKey();
			List<String> value = entry.getValue();
			String linktext = value.get(0);
			String href = value.get(1);
			MethodDef.validateLinkTextHref(path, linktext, href);
		}
	}

	public static Map<String, List<String>> politicsheaderLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = ".drawer li.bucket";

		/* if (!System.getProperty("Platform").equals("Perfecto")) { */
		/*
		 * map.put(path + " a[data-analytics='header_election-2016']",
		 * addValuesMap(map, "Election 2016", "/election"));
		 */
		// }

		map.put(path + " a[href='/specials/politics/president-donald-trump-45']",
				addValuesMap(map, "45", "/specials/politics/president-donald-trump-45"));
		map.put(path + " a[href='/specials/politics/congress-capitol-hill']",
				addValuesMap(map, "CONGRESS", "/specials/politics/congress-capitol-hill"));
		map.put(path + " a[href='/specials/politics/us-security']",
				addValuesMap(map, "SECURITY", "/specials/politics/us-security"));
		map.put(path + " a[href='/specials/politics/supreme-court-nine']",
				addValuesMap(map, "THE NINE", "/specials/politics/supreme-court-nine"));
		map.put(path + " a[href='/specials/politics/trumpmerica']",
				addValuesMap(map, "TRUMPMERICA", "/specials/politics/trumpmerica"));

		return map;
	}

	public static List<String> addValuesMap(Map<String, List<String>> map, String val1, String val2) {
		List<String> v = new ArrayList<String>();
		v.add(val1);
		v.add(val2);
		return (v);
	}

	public static void politicsFooter() {
		// Creating custom Page instances

		try {
			Reporter.log("Info: Testing Politics Page Footer Section");
			WrapperMethods.verifyElement(SectionFrontsPage.getPoliticsFooterLogo(), "The Politics Footer CNN Logo");
			WrapperMethods.verifyElement(SectionFrontsPage.getPoliticsFooterPoliticsLink(),
					"politics link in footer section");

			// PoliticsPageFunctions.footerTesting(wf.go(), error,
			// PoliticsPageFunctions.getPoliticsFooterCNNLink());
			PoliticsPageFunctions.footerTesting(getPoliticsFooterSectionLinks());
			PoliticsPageFunctions.footerTesting(PoliticsPageFunctions.getPoliticsFooterLastLineLinks());

			WrapperMethods.contains_Text(SectionFrontsPage.getPoliticsStyleFooterCopyright(), "CNN Sans � &",
					"CNN Sans � &' is added in Footer Copyright Text",
					"CNN Sans � &' is not added in Footer Copyright Text");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Politics Page Footer Section");

		}

	}

	public static Map<String, List<String>> getPoliticsFooterSectionLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = ".l-footer__buckets li";

		/*
		 * if (!System.getProperty("Platform").equals("Perfecto")) {
		 * map.put(path + " a[data-analytics='header_election-2016']",
		 * addValuesMap(map, "Election 2016", "/election")); }
		 */
		map.put(path + " a[href='/specials/politics/president-donald-trump-45']",
				addValuesMap(map, "45", "/specials/politics/president-donald-trump-45"));
		map.put(path + " a[href='/specials/politics/congress-capitol-hill']",
				addValuesMap(map, "CONGRESS", "/specials/politics/congress-capitol-hill"));
		map.put(path + " a[href='/specials/politics/us-security']",
				addValuesMap(map, "SECURITY", "/specials/politics/us-security"));
		map.put(path + " a[href='/specials/politics/supreme-court-nine']",
				addValuesMap(map, "THE NINE", "/specials/politics/supreme-court-nine"));
		map.put(path + " a[href='/specials/politics/trumpmerica']",
				addValuesMap(map, "TRUMPMERICA", "/specials/politics/trumpmerica"));

		return map;
	}

	public static Map<String, List<String>> getPoliticsFooterLastLineLinks() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		String path = "div.m-footer__copyright";

		map.put(path + " a[data-analytics='footer_turner-broadcasting-system-inc']",
				addValuesMap(map, "Turner Broadcasting System, Inc.", "//www.turner.com"));
		map.put(path + " a[href='/terms']", addValuesMap(map, "Terms of service", "/terms"));
		map.put(path + " a[href='/privacy']", addValuesMap(map, "Privacy guidelines", "/privacy"));

		return map;
	}

	public static void footerTesting(Map<String, List<String>> footerLinks) {
		Map<String, List<String>> map;
		map = footerLinks;
		splitHrefValidation(map.entrySet());
	}

	public static void politicsValidation(String[] domData) {

		int i = 0;

		for (String temp : domData) {

			String[] headItems = temp.split("~");

			Reporter.log("<br>Element under test " + headItems[0]);

			String newurl = EnvironmentHelper.getURL() + headItems[1];
			MethodDef.navUrl(newurl);

			if (!ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
				WrapperMethods.verifyElement(PoliticsPage.getHamFlyout(), "Hamburger Icon ");
				WrapperMethods.getWebElement(PoliticsPage.getHamFlyout()).click();
			}

			try {
				BasicPoliticsValidation(PoliticsPage.getCnnRedlogo(), PoliticsPage.getPoliticslogo(),
						PoliticsPage.getCurrentSection());
			} catch (Exception E) {
				UMReporter.log(LogStatus.FAIL, "Error in accessing section Elements for Politics " + headItems[0]);
			}
		}

	}

	public static void BasicPoliticsValidation(By cnnRedLogo, By politicsLogo, By currentSection) {
		// Logo
		WrapperMethods.verifyElement(cnnRedLogo, "The CNN logo ");

		WrapperMethods.verifyElement(politicsLogo, "The Politics logo ");

		// Sub section Selection
		WrapperMethods.verifyElement(currentSection, "The Current Section ");

	}

	public static void testEmailpolitcshome() throws IOException, InterruptedException {

		WrapperMethods.page_scrollDown();

		WrapperMethods.verifyElement(HomePage.emailtextboxx(), "Email Text box in politics home page ");

		WrapperMethods.getWebElement(HomePage.emailtextboxx()).sendKeys(RandomStringUtils.randomAlphanumeric(6),
				Keys.ENTER);
		Thread.sleep(5000);

		WrapperMethods.verifyElement(HomePage.emailerrormessage(),
				"error message is  displayed after entering invalid email address ");

		WrapperMethods.getWebElement(HomePage.emailtextboxx())
				.sendKeys(RandomStringUtils.randomAlphanumeric(6) + "@gmail.com", Keys.ENTER);
		Thread.sleep(5000);

		WrapperMethods.verifyElement(HomePage.emailsuccessmessage(),
				"Success message is  displayed after entering Valid email address ");

		WrapperMethods.verifyElement(HomePage.emailsuccessdesc(),
				"Success description is  displayed after entering valid email address ");

	}

	public static void PoliticsLinkedZones() throws IOException, InterruptedException {

		WrapperMethods.textAttributeNotEmpty(PoliticsPage.PoliticsPageLinkedZone(), "href",
				"Href Value for Politics Linked Zones");

		WrapperMethods.textAttributeNotEmpty(PoliticsPage.PoliticscontainerLinkedHeader(), "href",
				"Href Value for Politics Linked Container");

		WrapperMethods.verifyElement(PoliticsPage.PoliticsPageNONLinkedZone(), "Non Linked Zone Header ");
		WrapperMethods.verifyElement(PoliticsPage.PoliticscontainerNONLinkedHeader(), "Non Linked Container Header ");

	}
	
	public static void politicsFooterValidation() {

		WrapperMethods.scrollDown();
		WrapperMethods.isDisplayed(PoliticsPage.getCongress(), "In Footer section the subsection Congress is displayed", "In Footer section the subsection Congress is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.get45(), "In Footer section the subsection 45 is displayed", "In Footer section the subsection 45 is not displayedin");
			
		WrapperMethods.isDisplayed(PoliticsPage.getSecurity(), "In Footer section the subsection Security is displayed", "In Footer section the subsection Security is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.getTheNine(), "In Footer section the subsection TheNine is displayed", "In Footer section the subsection TheNine is not displayed");
			
		WrapperMethods.isDisplayed(PoliticsPage.getTrumpAmerica(), "In Footer section the subsection TrumpAmerica is displayed", "In Footer section the subsection TrumpAmerica is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.getFooterState(), "In Footer section the subsection State is displayed", "In Footer section the subsection State is not displayed");

		WrapperMethods.isDisplayed(PoliticsPage.getFooterFacebookShareIcon(), "In Footer section the FacebookShareIcon is displayed", "In Footer section the FacebookShareIcon is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.getFooterTwitterShareIcon(), "In Footer section the TwitterShareIcon is displayed", "In Footer section the TwitterShareIcon is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.getFooterInstagramShareIcon(), "In Footer section the InstagramShareIcon is displayed", "In Footer section the InstagramShareIcon is not displayed");
			
		WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(PoliticsPage.getCopyRights()).getText().equalsIgnoreCase("Turner Broadcasting System, Inc."), "'Turner Broadcasting System, Inc.' is displayed",
					"'Turner Broadcasting System, Inc.' is not displayed");
			
		WrapperMethods.isDisplayed(PoliticsPage.getPrivacyGuidlines(), "In Footer section the PrivacyGuidlines is displayed", "In Footer section the PrivacyGuidlines is not displayed");
		WrapperMethods.isDisplayed(PoliticsPage.getToS(), "In Footer section the Terms of services is displayed", "In Footer section the Terms of services is not displayed");
	}

}
