package um.testng.test.pom.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class SpecialPage extends BasePage {

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	/**
	 * Branding background for the Special Pages
	 */
	public static By getSpecialBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper");
	}

	/**
	 * Branding Image of the specified branding
	 */
	public static By getSpecialBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	/**
	 * Branding Logo of the specified branding
	 */
	public static By getSpecialBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	public static By getSpecialFb(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--facebook a");
	}

	public static By getSpecialTw(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--twitter a");
	}

	public static By getSpecialInst(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--instagram a");
	}

	public static By getSpecialDesc(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__description p");
	}

	public static By getSpecialSectName(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-nav-section-name a");
	}

	public static By getSpecialLogo(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	public static By getSpecialBackgroundImg(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	public static By getSpecialTitle(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:title']");
	}

	public static By getSpecialSeoKeyword(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='keywords']");
	}

	public static By getSpecialSlug(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:url']");
	}

	public static By getSpecialSeoDesc(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='description']");
	}

	public static By getSpecialCardImg(WebDriver d) {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:image']");
	}

	public static By getSpecialPageLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__top img.metadata-header__logo");
	}

	public static By getSpecialPageZone1() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-0-0']");
	}

	public static By getSpecialPageBlueLinkText() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='zone2']//div[contains(@class,'cd__wrapper')]/div[contains(@class,'cd__content')]/div[contains(@class,'cd__description')]/a/strong");
	}

	public static By getSpecialPageImage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='cd__wrapper']//img");
	}

}
