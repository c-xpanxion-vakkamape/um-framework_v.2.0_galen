package um.testng.test.pom.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class VideoLeafPage extends ArticlePage {

	static WebDriver driver = DriverFactory.getCurrentDriver();

	public static WebElement getVideoPlayElement() {
		return driver.findElement(By.cssSelector("div.media__video--responsive"));
	}

	public static By getVideoHead() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] h2");
	}

	public static By getVideoHeadDvc() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='el__video-collection__meta-wrapper cn-grid']/h2");
	}

	public static By getVideoSourceDvc() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[@class='video__metadata__source-name']");
	}

	public static WebElement getVideoSource() {
		return WrapperMethods.locateElement(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] span.metadata__source-name");
	}

	public static WebElement getVideoSrcdvc() {
		return WrapperMethods.locateElement(Locators.XPATH, "//span[@class='video__metadata__source-name']");
	}

	public static By getVideoEmbed() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.sharebar-video-embed-element div div");
	}
	public static By getVideoPlayElementBy() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video--responsive");
	}


	public static By getEmVideoElement() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video--thumbnail");
	}

	public static By getEmVideoElementPlayElemOB() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video--thumbnail");
	}

	public static By getEmVideoElementPlayClose() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[class^='el__video__close']");
	}

	// /

	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__video");
	}

	/**
	 * Video Play Button in the Video Leaf page
	 */
	public static By getVideoPlayElementNoAutoPlay() {
		return WrapperMethods.locatorValue(Locators.CSS, "large-media_0");
	}

	/**
	 * Show name of the video
	 */
	public static By getVideoShowName() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] p.metadata--show__name");
	}

	// Video Source
	/**
	 * Source of the video from which it is being played
	 */

	// Video Source URL
	/**
	 * Source URL
	 */
	public static By getVideoSourceUrl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata__source-name a");
	}

	/**
	 * Embedded Video Icon image in the article page with Embedded video
	 */

	/**
	 * Published Date of the video
	 */
	public static By getVideoDate() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata__data-added");
	}

	/**
	 * gigya social share bar in the Video Leaf page
	 */
	public static By getSocialContainer() {
		return WrapperMethods.locatorValue(Locators.CSS, "#gigyaShareBar_4_gig_containerParent");
	}

	/**
	 * Description of the video below the video
	 */
	public static By getVideoDescription() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] div.media__video-description");
	}

	/**
	 * Title of the Article with page top collections
	 */
	public static By getVideoCollTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cn-carousel-medium-strip h2");
	}

	/**
	 * checks the carousel of the video collections
	 */
	public static By getVideoCollCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cn-carousel-medium-strip div.owl-stage");
	}

	/**
	 * Previous button of the Video Collection Carousel
	 */
	public static By getVideoCollPrevCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cn-carousel-medium-strip div.owl-nav div.owl-prev");
	}

	/**
	 * Next button of the Video Collection Carousel
	 */
	public static By getVideoCollNextCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.cn-carousel-medium-strip div.owl-nav div.owl-next");
	}

	/**
	 * gets the active video in the carousel of the collections
	 */
	public static By getVideoCollVideoElem() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.carousel__content__item div.cd__wrapper div.media");
	}

	/**
	 * lists the active videos in the Video Collection
	 */
	public static List<WebElement> getVideoCollVideoElements() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='owl-stage']//div[@class='owl-item active']");
		// .cssSelector("div.cn-carousel-medium-strip div.owl-stage
		// div.owl-item.active");
		// .cssSelector("#cn-current_video_collection div.owl-stage
		// div.owl-item.active");
	}

	/**
	 * "Now playing" text is being displayed on the current playing video
	 */
	public static By getVideoCollNowPlay() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.media a div.media__over-text");
	}

	/**
	 * Total Time of the video which is visible for all the videos in the
	 * carousel
	 */
	public static By getVideoCollTotalTime() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.media a i.media__icon[aria-hidden='true']:not([style])");
	}

	/**
	 * Source of the Image
	 */
	public static By getVideoCollImgSrc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.media a img");
	}

	/**
	 * Headline of the video which is below the image
	 */
	public static By getVideoHeadLines() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.cd__content h3 a span.cd__headline-text");
	}

	// Branding
	public static By getVideoBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper");
	}

	public static By getVideoBrandingImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark");
	}

	public static By getVideoBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark img.pg__branding-logo");
	}

	public static By getVideoBrandingLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark a");
	}

	public static List<WebElement> getVideoCollVideoElemDevice() {
		/*
		 * return d.findElements(By .cssSelector(
		 * "#cn-current_video_collection div.owl-stage div.owl-item.active div.carousel__content__item div.cd__wrapper div.cd__content h3.cd__headline"
		 * );
		 */return WrapperMethods.locateElements(Locators.CSS,
				"div.cn-carousel-medium-strip div.cn__column[style='display: block;']");
		// .xpath("//div[contains(@class,'owl-stage-outer')]//div[contains(@class,'active')]");
	}

	public static By secondZoneAdContainer() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_rect_btf_01 iframe");
	}

	public static By getZone4() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-3-0']");
	}

	public static By getZone1DesktopAD() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@data-vr-zone='zone-0-0']//div[contains(@class,'ad--desktop')]//div[contains(@data-ad-id,'ad_rect_atf_01')]");
	}

	public static By getZone1TabAD() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@data-vr-zone='zone-0-0']//div[contains(@class,'ad--tablet')]//div[contains(@data-ad-id,'ad_rect_atf_01')]");
	}

	public static By getZone3Container1() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('otherCollections')//div[contains(@class,'zn__column--idx-0')]");
	}

	public static By getZone3Container2() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('otherCollections')//div[contains(@class,'zn__column--idx-1')]");
	}

	public static List<WebElement> getZone3Container1Card() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('otherCollections')//div[contains(@class,'zn__column--idx-0')]//article//a//img[contains(@class,'media__image')]");
	}

	public static List<WebElement> getZone3Container2Card() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"id('otherCollections')//div[contains(@class,'zn__column--idx-1')]//article//a//img[contains(@class,'media__image')]");
	}

	public static By getZone3Container2Layout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"id('otherCollections')//div[contains(@class,'zn__column--idx-1')]//div[contains(@class,'cn--idx-1')]");
	}

	public static By vidPlayButton() {
		return WrapperMethods.locatorValue(Locators.ID, "cvp_1");
	}

	public static By vidPlayButtonM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='vjs-big-play-button']");
	}

	public static List<WebElement> getVideoCollImgSrcs() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.media a img");
	}

	public static List<WebElement> getVideoCollTotalTimes() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.cn-carousel-medium-strip div.owl-stage div.owl-item.active div.media a i.media__icon[aria-hidden='true']:not([style='display: none;'])");
	}

	public static List<WebElement> getVideoCollTotalTimesMob() {
		return WrapperMethods.locateElements(Locators.CSS,
				"div.media a i.media__icon[aria-hidden='true']:not([style='display: none;'])");
	}

	public static List<WebElement> getVideoCollImgSrcsMob() {
		return WrapperMethods.locateElements(Locators.CSS, "div.media a img");
	}

	public static List<WebElement> getVideoCollNowPlays() {
		return WrapperMethods.locateElements(Locators.CSS, "a div.media__over-text");
	}

	public static By getCurrentVideoElem() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//a//div[@class='media__over-text']//ancestor::article[@data-video-id]");
	}

	public static By getVidLeftShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-side-of-rail .m-share");
	}

	public static By getVidShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-rail .gigya-sharebar");
	}

	public static By getEmailShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-rail .gigya-sharebar .gig-button-container-email");
	}

	public static By getFBShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-rail .gigya-sharebar .gig-button-container-facebook");
	}

	public static By getTWShare() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-rail .gigya-sharebar .gig-button-container-twitter");
	}

	/**
	 * returns VideoElement in the Article Page
	 */
	public static By getVideoElement() {
		return WrapperMethods.locatorValue(Locators.CSS, "[id*='cvp']");
	}

	/**
	 * returns VideoElement in the Article Page
	 */
	public static By getTheoVideoElement() {
		// return WrapperMethods.locatorValue(Locators.CSS, "[id*='Jj3Oj']");
		return WrapperMethods.locatorValue(Locators.CSS, ".el__featured-video");
	}

	public static By getVideoPinnedTrunc() {
		return WrapperMethods.locatorValue(Locators.CSS,
				"div.media__caption.el__storyelement__title.js-pinned-video-collection-title.fade-in > span.cd__headline-text");
	}

	public static By embeddedtextbox() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__video-collection__meta-wrapper[data-meta='desktop-collection'] input");
	}

	public static By get3rdSection() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-0-3']");
	}

	public static By getZone3() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-0-2']");
	}

	public static By getZone5() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-0-4']");
	}

	public static By getZone5Label() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@data-vr-zone='zone-0-4']//div/h2");
	}

	public static By getZone5Container() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@data-vr-zone='zone-0-4']//div[contains(@class,'zn__column--idx-0')]");
	}

	public static By getZone5ContainerLayout() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@data-vr-zone='zone-0-4']//div[contains(@class,'zn__column--idx-0')]//div[contains(@class,'cn--idx-0')]");
	}

	public static List<WebElement> getZone5ContainerCard() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@data-vr-zone='zone-0-4']//div[contains(@class,'zn__column--idx-0')]//article//a//img[contains(@class,'media__image')]");
	}

	public static List<WebElement> getZoneTopGradient() {
		return WrapperMethods.locateElements(Locators.CSS, ".zn-top");
	}

	public static List<WebElement> getZones() {
		return WrapperMethods.locateElements(Locators.CSS, "section.zn");
	}

	public static By getVideoSourceM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@id, 'js-video_source-')]");
	}

	public static By getVideoSourceUrlM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//span[contains(@id, 'js-video_source-')]/a");
	}

	public static By getVideoHeadM() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[contains(@id, 'js-mobile-video-headline-')]");
	}

	public static By getMoreBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='read-more-button']");
	}

	public static By getMoreTxt() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='read-more-link el__video-collection__see-more']");
	}

	public static List<WebElement> getVideosCarousel() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='cn__column carousel__content__item'][@style]");
	}

	public static By getPageLoadVR() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//nav[@class='logo flex-order--logo']");
	}

	public static By getVRPageCopyrights() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='legal__copyright']");
	}

	public static By getVRPageCopyrightsTB() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='legal__copyright']/a");
	}

	public static By getHomeLink() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='home-link']");
	}

	public static By getHomeLinkCNN() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='home-link']/a[@class='logo']");
	}

	public static By getLegalCopyRight() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//p[@class='legal__font-copyright']");
	}

	public static By getTos() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//ul[@class='legal__link-list']//li//a[text()='Terms of service']");
	}

	public static By getPrivGuid() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//ul[@class='legal__link-list']//li//a[text()='Privacy guidelines']");
	}

	public static By getShowMoreVRVid() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//a[@id='show-more-vr-videos']");
	}

	public static By getMoreVRVid() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h2[@class='zn-header__text']//a");
	}

	public static List<WebElement> getMoreVRVideos() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[@class='zn__containers']//div[@class='cd__wrapper']//img[@class='media__image media__image--responsive']");
	}

	public static By getZoneone() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-1']");
	}

	public static By getZoneoneHeadline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-1']//h3/a");
	}

	public static By getVRVideo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@id='player-fave-vr-360-video']");
	}

	public static By getVJSPlayBtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='vjs-big-play-button']");
	}

	public static By refArrow() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='vr_scroll']");
	}

	public static By getHdLn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//h1[@class='pg-headline']");
	}

	public static List<WebElement> getImages() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@id='vr-zone-5']//div[@class='zn__containers']//div[@class='column zn__column--idx-0']/div/div[*]//div[@class='media']");
	}

	public static List<WebElement> getImagesLink() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@id='vr-zone-5']//div[@class='zn__containers']//div[@class='column zn__column--idx-0']/div/div[*]//div[@class='media']/a");
	}

	public static List<WebElement> getImagesHeadlineLink() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//section[@id='vr-zone-5']//div[@class='zn__containers']//div[@class='column zn__column--idx-0']/div/div[*]//h3[@class='cd__headline']/a");
	}

	public static By getVRZone2() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-2']");
	}

	public static By getVRZone3() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-3']");
	}

	public static By getVRZone4() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-4']");
	}

	public static By getVRZone2Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-2']//h3/a");
	}

	public static By getVRZone3Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-3']//h3/a");
	}

	public static By getVRZone4Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//section[@id='vr-zone-4']//h3/a");
	}

	public static By getVRZone2BackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@id='vr-zone-2']/div/div[@class='zn-top__background']/a/div/img");
	}

	public static By getVRZone3BackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@id='vr-zone-3']/div/div[@class='zn-top__background']/a/div/img");
	}

	public static By getVRZone4BackgroundImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//section[@id='vr-zone-4']/div/div[@class='zn-top__background']/a/div/img");
	}

	public static List<WebElement> getVRPageAds() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[contains(@class,'ad ad')]");
	}

	public static By getPreloadad() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//video[@preload='metadata']");
	}

	public static By gettitle() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='el__video-collection__meta-wrapper']//h2[@class='media__video-headline']");
	}

	public static By getsecvideo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='owl-item active'][2]");
	}

	public static By getsecvideoDev() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='cn__column carousel__content__item'][2]");
	}

	public static String getTWContent() {
		return WrapperMethods.locateElement(Locators.CSS,("#status")).getText().toLowerCase();
	}
	
	public static List<WebElement>getVideo360Carousel() {
		return WrapperMethods.locateElements(Locators.XPATH, "//div[@class='owl-item active']//div[@class='media']//*[@class='media__icon icon-media-video360']");
	}

}
