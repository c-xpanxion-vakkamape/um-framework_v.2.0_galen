package um.testng.test.pom.functions;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.WeatherPage;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class WeatherPageFunctions {

	public static void weatherforecast() {
		UMReporter.log(LogStatus.INFO, "changing the location");
		WeatherPage.wait(5);
		WrapperMethods.sendkeys(WeatherPage.getTextBox(), "Maanti, Mongolia", "Entered the text ",
				"Not able to enter the text ");
		UMReporter.log(LogStatus.INFO, "Entered the city name - Maanti, Mongolia");
		WrapperMethods.waitForVisiblity(WeatherPage.getForecastdrop(), "Suggessions are showing",
				"Suggessions are not showing");
		WrapperMethods.click(WeatherPage.getForecastdrop(), "Maanti, Mongolia");
		WrapperMethods.click(WeatherPage.getForecast(), "Forecast ");
		WeatherPage.wait(5);
		WrapperMethods.verifyElement(WeatherPage.getDefaultButton(), "Make Default Button ");
		WrapperMethods.startwith_Text(WeatherPage.getCurrentLoc(), "", "Maanti, Mongolia");
	}

	public static void testExtendedForecast() {

		UMReporter.log(LogStatus.INFO, "Extended Forecast");
		WrapperMethods.clickJavaScript(WeatherPage.getFaren());

		WrapperMethods.verifyElement(WeatherPage.getExtendedForecastTitle(), "Extended Forecast title ");
		WrapperMethods.verifyElement(WeatherPage.getExtForecastDay(), "Extended Forecast Days ");
		WrapperMethods.verifyElement(WeatherPage.getWeatherIcon(), "weather conditions short icon ");

	}

	public static void weatherZip() {
		UMReporter.log(LogStatus.INFO, "<br>changing the location with Code");
		WrapperMethods.clear(WeatherPage.getTextBox());
		WrapperMethods.sendkeys(WeatherPage.getTextBox(), "75008", "Entered the code ", "Not able to enter the code ");
		WrapperMethods.waitForVisiblity(WeatherPage.getForecastdrop(), "Suggessions are showing",
				"Suggessions are not showing");
		WrapperMethods.click(WeatherPage.getForecastdrop(), "Maanti, Mongolia");
		WrapperMethods.click(WeatherPage.getForecast(), "Forecast ");
		WeatherPage.wait(5);
		WrapperMethods.verifyElement(WeatherPage.getDefaultButton(), "Make Default Button ");
		WrapperMethods.startwith_Text(WeatherPage.getCurrentLoc(), "", "Carrollton, TX");
	}

	public static void changeWeatherUnitType(String weatherunit) {
		UMReporter.log(LogStatus.INFO, weatherunit + " Unit is clicked");
		By unit = null;
		if (weatherunit.equals("celsius")) {
			unit = WeatherPage.getCelsi();
		} else {
			unit = WeatherPage.getFaren();
		}
		WrapperMethods.clickJavaScript(unit, "Clicked the UNIT", "Not able to click the UNIT");
		int opt = WrapperMethods.getWebElement(WeatherPage.getheaderTemp()).getAttribute(weatherunit).length();
		if (opt < 1) {
			UMReporter.log(LogStatus.FAIL, "Temperature value is not returned  " + opt);
			WrapperMethods.isDisplayed(WeatherPage.getTempRange(), "Temperature Range is displayed",
					"Temperature Range is not displayed");
			for (WebElement elem : WeatherPage.getTemCelsius()) {
				WrapperMethods.assertIsTrue(elem.getAttribute("data-temptype").equals(weatherunit),
						"All Temperature turned to " + weatherunit, "All Temperature didn't turn to " + weatherunit);
			}

		} else {
			UMReporter.log(LogStatus.PASS, "Temperature value is returned" + opt);
		}
	}

	/**
	 * Validating the Maps of the weather page
	 * 
	 * @param go
	 *            - WebDriver instance
	 * @param error
	 *            - Soft Error instance
	 * @param weather
	 *            - Weather page instance
	 */
	public static void testWeatherMaps() {

		try {

			UMReporter.log(LogStatus.INFO, "Weather Maps Validations: ");
			WrapperMethods.verifyElement(WeatherPage.getWeatherMapTitle(), "Weather Map title");

			if (System.getProperty("edition").equals("DOM")) {
				WrapperMethods.verifyElement(WeatherPage.getWeatherCurrentRadar(), "Weather Current Radar");
				WrapperMethods.assertIsTrue(!WeatherPage.getWeatherCurrentRadarSrc().getAttribute("src").isEmpty(),
						"The Current Radar Image source is displayed", "The Current Radar source is not displayed");
			}

			WrapperMethods.verifyElement(WeatherPage.getWeatherSatelliteMap(), "Weather Satellite Map");
			WrapperMethods.assertIsTrue(!WeatherPage.getWeatherSatelliteMapSrc().getAttribute("src").isEmpty(),
					"The Satellite Image source is displayed", "The Satellite Image source is not displayed");

			WrapperMethods.verifyElement(WeatherPage.getWeatherTemperatureMap(), "Weather Temperature Map");
			WrapperMethods.assertIsTrue(!WeatherPage.getWeatherTemperatureMapSrc().getAttribute("src").isEmpty(),
					"The Temperature Image source is displayed", "The Temperature Image source is not displayed");

			WrapperMethods.verifyElement(WeatherPage.getWeatherForecastMap(), "Weather Forecast Map");
			WrapperMethods.assertIsTrue(!WeatherPage.getWeatherForecastMapSrc().getAttribute("src").isEmpty(),
					"The Forecast Image source is displayed", "The Forecast Image source is not displayed");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the WeatherMap elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void weatherpageZipcodeValidation() {
		try {
			UMReporter.log(LogStatus.INFO, "Launched the Weather page");
			WrapperMethods.sendkeys(WeatherPage.getForecastsearch(), "20001", "Text entered", "Text not entered");
			MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "element loaded", "element not loaded");
			WrapperMethods.clickJavaScript(WeatherPage.getForecastdrop());
			WrapperMethods.click(WeatherPage.getForecast(), "Forecast ");
			WeatherPage.wait(5);
			WrapperMethods.assertIsTrue(WeatherPage.getCurrentLocation().getText().contains("Washington, DC"),
					"State name is displayed correcly based on zipcode 20001 :"
							+ WeatherPage.getCurrentLocation().getText(),
					"State name is not displayed correcly based on zipcode 20001");
			WrapperMethods.sendkeys(WeatherPage.getForecastsearch(), "30005", "Text entered", "Text not entered");
			MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "element loaded", "element not loaded");
			WrapperMethods.clickJavaScript(WeatherPage.getForecastdrop());
			WrapperMethods.click(WeatherPage.getForecast(), "Forecast ");
			WeatherPage.wait(5);
			WrapperMethods.assertIsTrue(WeatherPage.getCurrentLocation().getText().contains("Alpharetta, GA"),
					"State name is displayed correcly based on zipcode 30005 :"
							+ WeatherPage.getCurrentLocation().getText(),
					"State name is not displayed correcly based on zipcode 30005");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing the Weather page Zip code validations");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void TestWeatherPagepRefafterRefresh() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WrapperMethods.click(WeatherPage.getButtonTempC(), "C Celcious button");
		WrapperMethods.contains_Text_Attribute(WeatherPage.getCurrTemp(), "data-temptype", "celsius",
				"The Celsius is reflected in the current temperature",
				"The Celsius in not replected in the current temperature");
		String temp = WrapperMethods.getText(WeatherPage.getCurrTemp(), 20);

		driver.navigate().refresh();
		UMReporter.log(LogStatus.INFO, "The Page is refreshed");
		MethodDef.explicitWaitVisibility(WeatherPage.getCurrTemp(), 15, "Current Temperature is visible",
				"Current Temperature is not visible");
		WrapperMethods.contains_Text(WeatherPage.getCurrTemp(), temp, "The temperature persists after refresh",
				"The Temperature is not persisted after refresh");
		WrapperMethods.click(WeatherPage.getfarenheitbutton(), "F Farenheit button");

		WrapperMethods.contains_Text_Attribute(WeatherPage.getCurrTemp(), "data-temptype", "fahrenheit",
				"The fahrenheit is reflected in the current temperature",
				"The fahrenheit in not replected in the current temperature");
		String tempfaren = WrapperMethods.getText(WeatherPage.getCurrTemp(), 20);

		driver.navigate().refresh();
		UMReporter.log(LogStatus.INFO, "The Page is refreshed");
		MethodDef.explicitWaitVisibility(WeatherPage.getCurrTemp(), 15, "Current Temperature is visible",
				"Current Temperature is not visible");
		WrapperMethods.contains_Text(WeatherPage.getCurrTemp(), tempfaren, "The fahrenheit persists after refresh",
				"The fahrenheit is not persisted after refresh");
	}

	public static void weatherpageImageValidation() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		try {
			UMReporter.log(LogStatus.INFO, "Launched the Weather page");
			List<String> country = new ArrayList<String>();
			country.add("Aabenraa, Denmark");
			country.add("B0 Los Sitios, PR");
			country.add("C Gables, FL");
			country.add("D Hanis, TX");
			country.add("EJido Hermosillo, Mexico");
			country.add("F E Warren AFB, WY");

			for (int i = 0; i < 6; i++) {
				WrapperMethods.getWebElement(WeatherPage.getForecastsearch()).sendKeys(country.get(i));
				MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "Forecase Drop down is visisble",
						"Forecase Drop down is not visisble");
				WrapperMethods.clickJavaScript(WeatherPage.getForecastdrop());
				WrapperMethods.click(WeatherPage.getForecast(), "Get Forecase");
				UMReporter.log(LogStatus.INFO, "Entered Location " + country.get(i).toString() + "");
			}
			WrapperMethods.verifyElement(WeatherPage.getCurrentCondition(), "Current Condition");

			WrapperMethods.verifyElement(WeatherPage.getCurrentConditionCloudImg(), "Current Condition Image");

			WrapperMethods.verifyElement(WeatherPage.getCurrentConditionCloudText(), "Current Condition Image");

			for (int i = 0; i < WeatherPage.getExtForecastCloudImgs().size(); i++) {
				WrapperMethods.textNotEmpty(WeatherPage.getExtForecastCloudTexts().get(i),
						"Extended Forecast Date is displayed - "
								+ WeatherPage.getExtForecastCloudTexts().get(i).getText(),
						"Extended Forecast Date is not displayed");

				WrapperMethods.textNotEmpty(WeatherPage.getExtForecastCloudImgs().get(i),
						"Extended Forecast Image is displayed - "
								+ WeatherPage.getExtForecastCloudImgs().get(i).getText(),
						"Extended Forecast Image is not displayed");

				By sb = By.xpath("//ul[contains(@class,'cn cn-list-xs cn--idx-1 cn-container_')]/li[" + (i + 1)
						+ "]//div[@class='weather__conditions-short']/span[1]");
				WrapperMethods.verifyElement(sb, "Extended Forecast Text ");

				WrapperMethods.textNotEmpty(
						By.xpath("//ul[contains(@class,'cn cn-list-xs cn--idx-1 cn-container_')]/li[" + (i + 1)
								+ "]//div[@class='weather__conditions-short']/span[2]"),
						"Extended Forecast Text  is displayed - ", "Extended Forecast Text  is not displayed");

			}
			for (int i = 0; i < WeatherPage.getRecentLocationItems().size(); i++) {
				WrapperMethods.verifyElement(WeatherPage.getRecentLocationItems().get(i), (i + 1) + "st Location ");

				WrapperMethods.verifyElement(WeatherPage.getRecentLocImgs().get(i), "Recent Location Image ");
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Cloud Image validations");
		}

	}

	public static void weatherpageMakeDefaultValidation() {
		try {
			WrapperMethods.getWebElement(WeatherPage.getForecastsearch()).sendKeys("Australian, Canada");
			MethodDef.explicitWaitVisibility(WeatherPage.getForecastdrop(), "Forecase Drop down is visisble",
					"Forecase Drop down is not visisble");
			WrapperMethods.clickJavaScript(WeatherPage.getForecastdrop());
			WrapperMethods.getWebElement(WeatherPage.getForecast()).click();
			WrapperMethods.verifyElement(WeatherPage.getDefaultButton(), "Make Default button");
			WrapperMethods.startwith_Text(WeatherPage.getEnteredLoc(), "Australian, Canada");
			WrapperMethods.waitAndVerify(WeatherPage.getRecentLocationTitle(), "Recent Locations", 40,
					"Heading of recently searched Locations is displayed correctly ",
					"Heading of recently searched Locations is not displayed correctly ");
			WrapperMethods.verifyElement(WeatherPage.getRecentLocationLane(), "Recent Location Span ");
			WrapperMethods.clickJavaScript(WeatherPage.getDefaultButton());
			WrapperMethods.contains_Text_Attribute(WeatherPage.getDefaultButton(), "class", "default",
					"Changed Make default to 'Default'", "Not Changed the Make default lable to 'Default'");
			for (int i = 0; i < WeatherPage.getRecentLocationItems().size(); i++) {
				WrapperMethods.verifyElement(WeatherPage.getRecentLocationItems().get(i), (i + 1) + "st Location ");
				if (WeatherPage.getRecentLocationItems().get(i).getText().equals("Australian, Canada")
						&& WeatherPage.getRecentLocationItem().get(i).getAttribute("class").endsWith("default")) {
					UMReporter.log(LogStatus.PASS,
							"Entered Location is displayed !!" + WeatherPage.getRecentLocationItems().get(i).getText());
				}
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Error in Make default validations");
		}
	}

}