package um.testng.test.pom.elements;

import org.openqa.selenium.By;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class MoneyPage extends BasePage {

	public static By Skip() {
		return WrapperMethods.locatorValue(Locators.CSS, "div#skip>a");
	}

	public static By getmoneylogo() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//img[@alt='CNN Media logo']");
	}

}
