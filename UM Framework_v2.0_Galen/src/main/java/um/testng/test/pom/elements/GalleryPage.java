package um.testng.test.pom.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;
import um.testng.test.utilities.framework.*;

public class GalleryPage extends ArticlePage {

	public static By getTermsServiceConsentClose() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.user-msg--close");
	}

	public static By getGalleryPrev() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-nav > div.owl-prev");
	}

	/**
	 * gets the NEXT toggle in the gallery
	 */
	public static By getGalleryNext() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.owl-next");
	}

	public static By getGalleryEmIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.media__icon");
	}

	public static By getGalleryElement() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__gallery");
	}

	public static By getGalleryPrevOwl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-owl-carousel > div.owl-nav > div.owl-prev");
	}

	public static By getGalleryNextOwl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-owl-carousel > div.owl-nav > div.owl-next");
	}

	public static By getGalleryCarousel() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-owl-carousel");
	}

	public static By getGalleryPhotoCount() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery-showhide div.el__gallery-photocount");
	}

	public static By getGalleryPrevOwlNav() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.owl-carousel:nth-child(2) > div.owl-nav > div.owl-prev");
	}

	public static By getGalleryNextOwlNav() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.owl-carousel:nth-child(2) > div.owl-nav > div.owl-next");
	}

	/**
	 * Constructs the GalleryPage and gives the current driver instance.
	 * 
	 * @param driver
	 *            The driver currently controlling the window
	 */

	/**
	 * gets the title of the Gallery page
	 */
	public static By getGalleryHeaderTitle() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "pg-headline");
	}

	/**
	 * gets the published timestamp of the gallery
	 */
	public static By getGalleryTime() {
		return WrapperMethods.locatorValue(Locators.CSS, ".l-container p.update-time");
	}

	/**
	 * gets the NEXT toggle in the gallery
	 */

	/**
	 * gets the PREV toggle of the gallery carousel present below the gallery
	 * image
	 */
	public static By getGalleryOuterPrev() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-nav > div.owl-prev");
	}

	/**
	 * gets the Next toggle of the gallery carousel present below the gallery
	 * image
	 */
	public static By getGalleryOuterNext() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-nav > div.owl-next");
	}

	/**
	 * gets the title of the active image in the Gallery page
	 */
	public static By getGalleryTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active div span.el__storyelement__header");
	}

	/**
	 * @gets the caption title of the active image in the Gallery page
	 */
	public static By getGalleryImageCaptionHead() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active div span.media__caption-head");
	}

	/**
	 * @gets the caption content of the active image in the Gallery page
	 */
	public static By getGalleryImageCaptionContent() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div div.el__gallery_image-title span.el__storyelement__gray");
	}

	/**
	 * gets the total images present in the title below the image of the gallery
	 */
	public static By getGalleryTotalImages() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__storyelement__title span.el__storyelement__header");
	}

	/**
	 * Total images in film strip
	 */
	public static List<WebElement> getGalleryImages() {
		return WrapperMethods.locateElements(Locators.CSS, ".js-owl-filmstrip img");
	}

	/**
	 * gets the "Hide Caption" button in Gallery Page
	 */
	public static By getGalleryHideCaption() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery-showhide  div.el__gallery-caption");
	}

	/**
	 * gets the "Show Caption" button in Gallery Page
	 */
	public static By getGalleryShowCaption() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery-showhide  div.el__gallery-caption--closed");
	}

	/**
	 * gets the list of links present for the images present in the gallery
	 */
	public List<WebElement> getGalleryLinks() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//*[@class='pg-left-rail-short__body pg-side-of-rail']//a[boolean(@href) and starts-with(@href,'/') and contains(@href,'.htm')]");

	}

	/**
	 * waits till the element is found
	 * 
	 * @param element
	 *            - instance of webelement
	 * @return true or false depending on the visibility of the element
	 * @throws InterruptedException
	 */
	public boolean waitForVisible(WebElement element) {

		boolean continueLooking = true;
		boolean elementFound = true;
		int timeRunning = 0;

		while ((element == null || !element.isDisplayed()) && continueLooking) {
			BasePage.wait(1);
			timeRunning++;
			if (timeRunning > Constants.MAX_TIMEOUT) {
				continueLooking = false;
				elementFound = false;
			}
		}
		if (!elementFound) {
			System.out.println("Element not found!!");
		}
		return elementFound;
	}

	/**
	 * returns all links in the Page
	 */
	public List<WebElement> getAllLinks() {
		return WrapperMethods.locateElements(Locators.CSS, "a");
	}

	/**
	 * returns all image links in the Page
	 */
	public List<WebElement> getImageLinks() {
		return WrapperMethods.locateElements(Locators.CSS, "a>img");
		// return WrapperMethods.locateElements(Locators.XPATH,
		// "//article//a[img][boolean(@href)]");
	}

	/**
	 * returns all images in the Page
	 */
	public List<WebElement> getAllImages() {
		return WrapperMethods.locateElements(Locators.TAG_NAME, "img");
	}

	/**
	 * gets the gallery embedded icon in the article page
	 */

	/**
	 * gets the gallery embedded expandabale icon in the article page - For IE
	 * Browser
	 */
	public static By getGalleryEmExpandIE() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[class='el__gallery--teaseimage']");
	}

	/**
	 * gets the gallery embedded expandabale image in the article page - For IE
	 * Browser
	 */
	public static By getGalleryEmExpand() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".el__gallery--teaseimage > div:nth-child(2) > span:nth-child(2)");
		// return
		// WrapperMethods.locatorValue(Locators.XPATH,"//div[contains(@class,'el__gallery--teaseimage')]");
	}

	/**
	 * gets the PREV toggle in the Embedded Expandable Gallery after expanding
	 * in the article Page
	 */
	public static By getGalleryEmPrev() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".js__gallery--standard > div > div.js-owl-carousel > div.owl-nav > div.owl-prev");
	}

	/**
	 * gets the PREV toggle in the Embedded Expandable Gallery carousel after
	 * expanding in the article Page
	 */
	public static By getGalleryEmPrevOld() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.js-owl-carousel > div.owl-nav > div.owl-prev");
	}

	/**
	 * gets the Next toggle in the Embedded Expandable Gallery after expanding
	 * in the article Page
	 */
	public static By getGalleryEmNext() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".js__gallery--standard > div > div.js-owl-carousel > div.owl-nav > div.owl-next");
	}

	/**
	 * gets the Next toggle in the Embedded Expandable Gallery carousel after
	 * expanding in the article Page
	 */
	public static By getGalleryEmNextOld() {
		return WrapperMethods.locatorValue(Locators.CSS, "div.js-owl-carousel > div.owl-nav > div.owl-next");
	}

	/**
	 * gets the Close button in Embedded Expandable Gallery after expanding in
	 * the Gallery Page
	 */
	public static By getGalleryEmClose() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "js__gallery__close--standard");
	}

	/**
	 * gets the Embedded Expandable Gallery image in the Gallery Page
	 */
	public static By getEmGalElementPlayElemOB() {
		return WrapperMethods.locatorValue(Locators.CLASS_NAME, "el__gallery--teaseimage");
	}

	/**
	 * Branding background for the Gallery Pages
	 */
	public static By getGalleryBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark");
	}

	/**
	 * Branding Image of the specified branding
	 */
	public static By getGalleryBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark img.pg__branding-logo");
	}

	/**
	 * Branding Logo of the specified branding
	 */
	public static By getGalleryBrandingLink() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__branding--background-dark a");
	}

	public static By getGallerySlug() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:url']");
	}

	public static By getGalleryHeadline() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg-headline");
	}

	public static By getGalleryDescription() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-body__paragraph");
	}

	public static By getGallerySection() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[name='section']");
	}

	public static By getGallerySubSite() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:site_name']");
	}

	public static By getGalleryRatio() {
		return WrapperMethods.locatorValue(Locators.CSS, ".el__gallery--fullwidth div div.carousel--full");
	}

	public static By getGalleryImage1() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active.synced img.media__image");
	}

	public static By getGalleryImage1Headline() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery_image-title span.media__caption-head");
	}

	public static By getGalleryImage1Caption() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div.el__gallery_image-title span.el__storyelement__gray");
	}

	public static By getGalleryImage1Format() {
		return WrapperMethods.locatorValue(Locators.CSS,
				".owl-item.active div div.el__resize div.js-gallery-aspect-ratio-wrapper img");
	}

	public List<WebElement> getGalleryImagesList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class, 'owl-item') and not(contains(@class, 'cloned')]/img[contains(@class, 'media__image')]");
	}

	public List<WebElement> getGalleryImageHeadlinesList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class, 'owl-item') and not(contains(@class, 'cloned')]/div/div[contains(@class, 'el__gallery_image-title')]/span[contains(@class, 'media__caption-head')]");
	}

	public List<WebElement> getGalleryImageCaptionsList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class, 'owl-item') and not(contains(@class, 'cloned')]/div/div[contains(@class, 'el__gallery_image-title')]/span[contains(@class, 'el__storyelement__gray')]");
	}

	public List<WebElement> getGalleryImageFormatsList() {
		return WrapperMethods.locateElements(Locators.XPATH,
				"//div[contains(@class, 'owl-item') and not(contains(@class, 'cloned')]/div/div[contains(@class, 'el__resize')]/div[contains(@class, 'js-gallery-aspect-ratio-wrapper')]/img");
	}

	public static By getGalleryImage3() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active [alt='QAAuto Koala']");
	}

	public static By getGalleryImage3Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[3]/div[3]/span[1]");
	}

	public static By getGalleryImage3Caption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[3]/div[3]/span[2]");
	}

	public static By getGalleryImage3Format() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[1]/div/div[6]/div/div[1]/div/img");
	}

	public static By getGalleryImage4() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active [alt='QAAuto Hydrangeas']");
	}

	public static By getGalleryImage4Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[4]/div[3]/span[1]");
	}

	public static By getGalleryImage4Caption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[4]/div[3]/span[2]");
	}

	public static By getGalleryImage4Format() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[1]/div/div[7]/div/div[1]/div/img");
	}

	public static By getGalleryImage5() {
		return WrapperMethods.locatorValue(Locators.CSS, ".owl-item.active [alt='QAAuto Chrysanthemum']");
	}

	public static By getGalleryImage5Headline() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[5]/div[3]/span[1]");
	}

	public static By getGalleryImage5Caption() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[5]/div[3]/span[2]");
	}

	public static By getGalleryImage5Format() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//*[@id='large-media']/div/div/div/div[1]/div[1]/div/div[8]/div/div[1]/div/img");
	}

	public static By getGalleryMedia() {
		return WrapperMethods.locatorValue(Locators.ID, "large-media");
	}

	public static By getStyleOutbrain() {
		return WrapperMethods.locatorValue(Locators.ID, "outbrain_widget_1");
	}

	public static By adOverlay() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='js-owl-carousel owl-carousel carousel--full body owl-loaded owl-drag']");
	}

	public static By getGigyaBarMessenger() {
		return WrapperMethods.locatorValue(Locators.CSS, ".m-share__rail-top div.gig-button-container-messenger");
	}

	public static By getGalleryImage() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='owl-item synced active']/img");
	}

	public static By getGalleryPTImage() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[@class='owl-stage-outer owl-height']//div[@class='owl-item active']//img");
	}

	public static By getGalleryImageNew() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[@class='owl-item active synced']/img");
	}

	public static By getGalleryCloseButton() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'js__gallery__close')]");
	}

}
