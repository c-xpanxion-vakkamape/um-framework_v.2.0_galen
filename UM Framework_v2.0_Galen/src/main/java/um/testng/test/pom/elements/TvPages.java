package um.testng.test.pom.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class TvPages extends BasePage {

	static WebDriver driver = DriverFactory.getCurrentDriver();

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav__logo");
	}

	/**
	 * returns Current Section of the page
	 */
	public static By getCurrentSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".nav-section__name > a");

	}

	/**
	 * returns Current Sub Section of the page
	 */
	public static By getCurrentSubsection() {
		return WrapperMethods.locatorValue(Locators.CSS, "div[class=\"sections drawer\"]>ul>li[class$=selected]>a");

	}

	/**
	 * returns Current Sub Section Heading of the page
	 */
	public static By getCurrentSubsectHeading() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");

	}

	/**
	 * returns Current Sub Section Heading of special in the page
	 */
	public static By getCurrentSubsectHeadingSpl() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__title");

	}

	// TV Shows

	/**
	 * Domestic TV Shows Page Title
	 */
	public static By getTVShowsTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__top");
	}

	/**
	 * International TV Shows Page title
	 */
	public static By getTVShowsINTLTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	/**
	 * lists all the images of every show image in the TV Shows Page
	 */
	public static List<WebElement> getShowImages() {
		return driver.findElements(By.cssSelector(".zn__containers div.media"));
	}

	/**
	 * Lists the image hrefs of every show image in the TV Shows Page
	 */
	public static List<WebElement> getShowImagesHref() {
		return driver.findElements(By.cssSelector(".zn__containers div.media a"));
	}

	/**
	 * list of Show Headlines in the TV Shows Page
	 */
	public static List<WebElement> getShowHeadline() {
		return driver.findElements(By.cssSelector(".zn__containers div.cd__content span.cd__headline-text"));
	}

	/**
	 * list of Show Headlines Href in the TV Shows Page
	 */
	public static List<WebElement> getShowHeadlineHref() {
		return driver.findElements(By.cssSelector(".zn__containers div.cd__content h3 a"));
	}

	/**
	 * lists the description of each show in the TV Shows Page
	 */
	public static List<WebElement> getShowDescription() {
		return driver.findElements(By.cssSelector(".zn__containers div.cd__content div.cd__description"));
	}

	// TV Schedule
	/**
	 * Title of the TV Schedule Page
	 */
	public static By getTVScheduleTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, ".zn-header__text-page_header");
	}

	/**
	 * gets the Morning title in TV Schedule Page
	 */
	public static By getTVScheduleMorningLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tv-schedule-header__wrapper')]/div/ul/li[contains(@data-period,'Morning')]");
	}

	/**
	 * gets the Afternoon title in TV Schedule Page
	 */
	public static By getTVScheduleAfternoonLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tv-schedule-header__wrapper')]/div/ul/li[contains(@data-period,'Afternoon')]");
	}

	/**
	 * gets the Evening title in TV Schedule Page
	 */
	public static By getTVScheduleEveningLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tv-schedule-header__wrapper')]/div/ul/li[contains(@data-period,'Evening')]");
	}

	/**
	 * gets the Overnight title in TV Schedule Page
	 */
	public static By getTVScheduleOvernightLink() {
		return WrapperMethods.locatorValue(Locators.XPATH,
				"//div[contains(@class,'tv-schedule-header__wrapper')]/div/ul/li[contains(@data-period,'Overnight')]");
	}

	/**
	 * checks Drop down in TV Schedule Page
	 */
	public static By getTVScheduleDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-tv-schedule-drop-down");
	}

	/**
	 * North America menu item from the TV Schedule Drop down
	 */
	public static By getTVScheduleDropDownNA() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "North America");
	}

	/**
	 * TV Schedule Day menu item from the TV Schedule Drop down
	 */
	public static By getTVScheduleDayDropDown() {
		return WrapperMethods.locatorValue(Locators.CSS, ".js-tv-schedule-day-drop-down");
	}

	/**
	 * returns Europe menu item from the TV Schedule Drop down
	 */
	public static By getTVScheduleDropDownEu() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Europe");
	}

	/**
	 * returns Asia menu item from the TV Schedule Drop down
	 */
	public static By getTVScheduleDropDownAs() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Asia");
	}

	/**
	 * returns Americas menu item from the TV Schedule Drop down
	 */
	public static By getTVScheduleDropDownAm() {
		return WrapperMethods.locatorValue(Locators.LINKTEXT, "Americas");
	}

	/**
	 * List of the headlines of each show from TV schedule page
	 */
	public List<WebElement> getTVScheduleHeadlineText() {
		return driver
				.findElements(By.cssSelector("section.js-tv_schedule_day_0 div.l-container span.cd__headline-text"));
	}

	/**
	 * Lists the time of the shows from TV schedule page
	 */
	public List<WebElement> getTVScheduleTime() {
		return driver.findElements(By.cssSelector("section.js-tv_schedule_day_0 div.l-container div.cd__auxiliary"));
	}

	/**
	 * Lists the images of the shows from TV schedule page
	 */
	public List<WebElement> getTVScheduleImage() {
		return driver.findElements(By.cssSelector("section.js-tv_schedule_day_0 div.l-container div.media img"));
	}

	/**
	 * List of the headlines of each show from TV schedule page
	 */
	public List<WebElement> getValidTVScheduleHeadlineText() {
		return driver.findElements(By.xpath(
				"//section[contains(@class, 'js-tv_schedule_day_0')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div/ul/li/article/div/div[contains(@class, 'cd__content')]/h3/a/span[contains(@class, 'cd__headline-text')]"));
	}

	public List<WebElement> getInValidTVScheduleHeadlineText() {
		return driver.findElements(By.xpath(
				"//section[contains(@class, 'js-tv_schedule_day_0')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div/ul/li/article/div/div[contains(@class, 'cd__content')]/h3/span[contains(@class, 'cd__headline-text')]"));
	}

	/**
	 * Lists the time of the shows from TV schedule page
	 */
	public List<WebElement> getValidTVScheduleTime() {
		return driver.findElements(By.xpath(
				"//section[contains(@class, 'js-tv_schedule_day_0')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div/ul/li/article/div/div[contains(@class, 'cd__content')]/div[contains(@class, 'cd__auxiliary')]"));
	}

	/**
	 * Lists the images of the shows from TV schedule page
	 */
	public List<WebElement> getInValidTVScheduleImage() {
		return driver.findElements(By.xpath(
				"//section[contains(@class, 'js-tv_schedule_day_0')]/div[contains(@class, 'l-container')]/div[contains(@class, 'zn__containers')]/div/ul/li/article/div/div[contains(@class, 'media')]/img"));
	}

	// Show leaf page
	/**
	 * TV Show Logo in the Show page
	 */
	public static By getShowLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	/**
	 * Facebook Icon in the TV show page
	 */
	public static By getShowFbIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--facebook");
	}

	/**
	 * Facebook Icon Href in the TV show page
	 */
	public static By getShowFbIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--facebook a");
	}

	/**
	 * Twitter Icon in the TV show page
	 */
	public static By getShowTwIcon() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--twitter");
	}

	/**
	 * Twitter Icon Href in the TV show page
	 */
	public static By getShowTwIconHref() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__follow-icon--twitter  a");
	}

	// Branding : SHOW
	/**
	 * Branding background for the Show Pages
	 */
	public static By getShowbranding() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper");
	}

	/**
	 * Branding Image of the specified branding
	 */
	public static By getShowBrandingSrc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	/**
	 * Branding Logo of the specified branding
	 */
	public static By getShowBrandingLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	public static By getShowSlug() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:url']");
	}

	public static By getShowDesc() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__description p");
	}

	public static By getShowTitle() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:title']");
	}

	public static By getShowBranding() {
		return WrapperMethods.locatorValue(Locators.CSS, "#ad_mod_011ba0778");
	}

	public static By getShowNorthAmericaET() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__sub-text");
	}

	public static By getShowBackgroundImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".pg__background__image_wrapper img");
	}

	public static By getShowLogoImg() {
		return WrapperMethods.locatorValue(Locators.CSS, ".metadata-header__logo");
	}

	public static By getShowCardImg() {
		return WrapperMethods.locatorValue(Locators.CSS, "meta[property='og:image']");
	}

	public static By video360() {
		return WrapperMethods.locatorValue(Locators.CSS, ".fb_iframe_widget_fluid_desktop");
	}

	public List<WebElement> video360iframe() {
		return driver.findElements(
				By.cssSelector("div.fb-video.fb_iframe_widget.fb_iframe_widget_fluid_desktop span iframe"));
	}

	public static By video360Fbbtn() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[5]/div/div/div/div/div[3]/a");
	}

	public static By video360href() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_4']/div/div/div[1]/a");

	}

	public static By video360header() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_4']/div/div/div[1]/a/span");
	}

	public static By video360postedby() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_4']/div/div/div[2]");
	}

	public static By video360CNNhref() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_4']/div/div/div[2]/a");
	}

	public static By video360CNNtext() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[6]/div/div/div[2]/a/span");
	}

	public static By video360containsviews() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_4']/div/div/div[3]");
	}

	public static By video360Facebookicon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_3']/div/div/div/div/div[3]/a/i");
	}

	public static By video360FacebookHref() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_3']/div/div/div/div/div[3]/a");
	}

	public static By Video360icon() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//*[@id='u_0_6']");
	}

	public static By video360iFrame() {
		return WrapperMethods.locatorValue(Locators.XPATH, "//div[contains(@class,'fb_iframe_widget')]//iframe");
	}

	public static By video360iFrameSwitch() {
		return WrapperMethods.locatorValue(Locators.CSS, "iframe[title='fb:video Facebook Social Plugin']");
	}

	public static List<WebElement> weekdays() {
		return driver.findElements(
				By.xpath("//ul[@class='js-el-drop-down__list el-drop-down__list el-drop-down__list--open']//li"));
	}
}