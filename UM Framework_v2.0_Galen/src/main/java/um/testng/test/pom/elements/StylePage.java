package um.testng.test.pom.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.WrapperMethods;
import um.testng.test.utilities.framework.enums.Locators;

public class StylePage extends SectionFrontsPage {

	public StylePage(WebDriver driver) {
		super();
	}

	static WebDriver driver = DriverFactory.getCurrentDriver();

	/**
	 * Page Load Condition
	 */
	public static By getPageLoad(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo-style");
	}

	public static By getCurrentSection() {
		return WrapperMethods.locatorValue(Locators.CSS, ".buckets .open");
	}

	public static By getStyleLogo() {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo-style");
	}

	public static By getCnnRedLogo(WebDriver go) {
		return WrapperMethods.locatorValue(Locators.CSS, ".logo-cnn");
	}

	public List<WebElement> getSubsections(WebDriver d) {
		return d.findElements(By.xpath("//div[@class='buckets drawer']//li[*]//a"));
	}
}
