package um.testng.test.pom.functions;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.pom.elements.GalleryPage;
import um.testng.test.utilities.framework.ConfigProp;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class GalleryPageFunctions extends CommomPageFunctions {

	public void testGalleryPage() {

		boolean errorflagJavaScripts = false;
		boolean errorflagCarouselBtn = false;

		try {
			WrapperMethods.closeTermsOfService(GalleryPage.getTermsServiceConsentClose());
		} catch (Exception e) {

		}
		try {
			if (WrapperMethods.isDisplayed(GalleryPage.getGalleryNext())) {

				if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
						|| !ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {

					WrapperMethods.click(GalleryPage.getGalleryNext(), "Gallery next");
				} else {
					WrapperMethods.moveToElementClick(GalleryPage.getGalleryNext());
					UMReporter.log(LogStatus.PASS, "Gallery Carousel Next is verified");
				}
			} else {
				UMReporter.log(LogStatus.FAIL, "Gallery Carousel Next button is not displayed");
				errorflagCarouselBtn = true;
			}
			if (WrapperMethods.isDisplayed(GalleryPage.getGalleryPrev())) {
				if (ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
						|| !ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
					WrapperMethods.click(GalleryPage.getGalleryPrev(), "Gallery Previous");
				else
					WrapperMethods.moveToElementClick(GalleryPage.getGalleryPrev());
				UMReporter.log(LogStatus.PASS, "Gallery Carousel Prev is verified");
			} else {
				UMReporter.log(LogStatus.FAIL, "Gallery Carousel Prev button is not displayed");
				errorflagCarouselBtn = true;
			}
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, "Issues in the Gallery carousel buttons" + e.getMessage());
		}

	}

	public void testarticlegalpage(String browser, String machineType, String edition) {

		WrapperMethods.waitForVisiblity(GalleryPage.getGalleryElement(), 30, "Galley image/Elment loaded",
				"Gallery image/Element didn't load - unexcepted please check");

		WrapperMethods.waitForVisiblity(GalleryPage.getGalleryCarousel(), 30, "Gallery element Carousel is Present",
				"Gallery element Carousel  is not present");
		WrapperMethods.checkElementClickable(GalleryPage.getGalleryCarousel(), 30,
				"Gallery element Carousel is clickable", "Gallery element Carousel  is not clickable");

		WrapperMethods.waitForVisiblity(GalleryPage.getGalleryPrevOwl(), 30,
				"Gallery element Carousel previous link is Present",
				"Gallery element Carousel previous link is not Present");
		WrapperMethods.checkElementClickable(GalleryPage.getGalleryPrevOwl(), 30,
				"Gallery element Carousel previous link is clickable",
				"Gallery element Carousel Previous link is not clickable");

		WrapperMethods.waitForVisiblity(GalleryPage.getGalleryNextOwl(), 30,
				"Gallery element Carousel next link is Present", "Gallery element Carousel next link is not Present");
		WrapperMethods.checkElementClickable(GalleryPage.getGalleryNextOwl(), 30,
				"Gallery element Carousel next link is clickable",
				"Gallery element Carousel next link is not clickable");

		if (browser.startsWith("IE") || browser.equals("SAFARI")) {
			WrapperMethods.click(GalleryPage.getGalleryNextOwl(), "Next link in page top image ");
		} else {
			WrapperMethods.moveToElementClick(GalleryPage.getGalleryNextOwl());
		}

		String[] galimgs = WrapperMethods.getWebElement(GalleryPage.getGalleryPhotoCount()).getText().split(" ");
		int total = Integer.parseInt(galimgs[2]);

		int width = Integer.parseInt(ConfigProvider.getConfig("Res_Width"));
		if (width > 480 && total >= 15) {

			WrapperMethods.waitForVisiblity(GalleryPage.getGalleryPrevOwlNav(), 30,
					"Gallery element Navigation Bar Items Previous link is  Present",
					"Gallery element Navigation Bar Items  previous link is Present");
			WrapperMethods.checkElementClickable(GalleryPage.getGalleryPrevOwlNav(), 30,
					"Gallery element Navigation Bar Items Previous link  is clickable",
					"Gallery element Navigation Bar Items previous link is not clickable");

			WrapperMethods.waitForVisiblity(GalleryPage.getGalleryNextOwlNav(), 30,
					"Gallery element Navigation Bar Items Next  link is  Present",
					"Gallery element Navigation Bar Items nextlink is not Present");
			WrapperMethods.clickJavaScript(GalleryPage.getGalleryPrevOwlNav(),
					"clicked Gallery element Navigation Bar Items Previous link",
					"Not able to click Gallery element Navigation Bar Items Previous link");

		}
	}

	public static void testheader() {
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing On Article with Gallery Common Header</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGalleryHeaderTitle()).isDisplayed(),
					"Header Title is present", "Header Title is not present");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGalleryTime()).isDisplayed(),
					"Timestamp is present", "Timestamp is not present");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGigyaBar()).isDisplayed(),
					"Gigyabar is present", "Gigyabar is not present");

			/*
			 * WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(
			 * GalleryPage.getGigyaBarMessenger()).isDisplayed(),
			 * "Gigyabar Messenger icon is present",
			 * "Gigyabar Messenger Icon is not present");
			 */

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGigyaBarElem2()).isDisplayed(),
					"Gigyabar Facebook icon is present", "Gigyabar Facebook icon is not present");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGigyaBarElem3()).isDisplayed(),
					"Gigyabar Twitter icon is present", "Gigyabar Twitter icon is not present");
			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGigyaBarElem4()).isDisplayed(),
					"Gigyabar Share icon is present", "Gigyabar Share icon is not present");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Gallery Header element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void CheckFullwodthGAlleryImageSize() {
		try {

			WrapperMethods.assertIsTrue(
					WrapperMethods.getWebElement(GalleryPage.getGalleryPTImage()).getCssValue("width")
							.equalsIgnoreCase("780px")
							&& WrapperMethods.getWebElement(GalleryPage.getGalleryPTImage()).getCssValue("height")
									.contains("438"),
					"The Image Resolution is correct as expected",
					"The Image Resolution is not as expected - Please check");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Gallery Header element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void clickGalleryPrev() {
		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Gallery Prev</b>");

			WrapperMethods.clickJavaScript(GalleryPage.getGalleryPrev());
			GalleryPage.wait(10);
			String[] galimgs = WrapperMethods.getWebElement(GalleryPage.getGalleryPhotoCount()).getText().split(" ");
			int total = Integer.parseInt(galimgs[2]);
			WrapperMethods.assertIsTrue(Integer.parseInt(galimgs[0]) > 0 && Integer.parseInt(galimgs[2]) == total,
					"Current image index is correct", "Current image index is not correct");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "When Prev is clicked, error in accessing the element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void clickGalleryNext() {
		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Gallery Next</b>");

			WrapperMethods.clickJavaScript(GalleryPage.getGalleryNext());
			GalleryPage.wait(5);
			String[] filmimgs = WrapperMethods.getWebElement(GalleryPage.getGalleryPhotoCount()).getText().split(" ");
			// int totalgal = Integer.parseInt(galimgs[0]);
			int totalgal = GalleryPage.getGalleryImages().size();
			int current = Integer.parseInt(filmimgs[0]);
			int total = Integer.parseInt(filmimgs[2]);
			WrapperMethods.assertIsTrue(current <= total, "Current image index is correct" + current,
					"Current image index is not correct");
			WrapperMethods.assertIsTrue(totalgal == total, "Current image index is correct" + total,
					"Current image index is not correct");

			try {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(GalleryPage.getGalleryImage()).getCssValue("border-bottom-style")
								.equalsIgnoreCase("solid"),
						"The Red Line is availabe below the selected gallery image as expected",
						"The Red Line is not availabe below the selected gallery image - UnExpected");
			} catch (Exception e) {
				WrapperMethods.assertIsTrue(
						WrapperMethods.getWebElement(GalleryPage.getGalleryImageNew())
								.getCssValue("border-bottom-style").equalsIgnoreCase("solid"),
						"The Red Line is availabe below the selected gallery image as expected",
						"The Red Line is not availabe below the selected gallery image - UnExpected");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "When Next is clicked, error in accessing the element");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}

	}

	public static void galleryCaptionsOpenTest() {
		try {

			UMReporter.log(LogStatus.INFO, "<b>Info: Testing with Caption Open/ Reopened </b>");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(GalleryPage.getGalleryTitle()).getText().isEmpty(),
					"Gallery title bar is present", "Gallery title bar is not present");
			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(GalleryPage.getGalleryImageCaptionContent()).getText().isEmpty(),
					"Gallery caption is present", "Gallery caption is not present");
			String[] galimgs = WrapperMethods.getWebElement(GalleryPage.getGalleryPhotoCount()).getText().split(" ");
			int total = Integer.parseInt(galimgs[2]);

			WrapperMethods.assertIsTrue(GalleryPage.getGalleryImages().size() == total,
					"Gallery and FilmStrip has equal number of images" + GalleryPage.getGalleryImages().size() + total,
					"Gallery and FilmStrip don't have equal number of elements" + GalleryPage.getGalleryImages().size()
							+ total);
			WrapperMethods.assertIsTrue(Integer.parseInt(galimgs[0]) > 0 && Integer.parseInt(galimgs[0]) <= total,
					"Current image index is correct", "Current image index is not correct");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGalleryHideCaption()).isDisplayed(),
					"Gallery hide Caption is present", "Gallery hide Caption is not present");

		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Gallery Caption Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void hideCaption() {

		WrapperMethods.clickJavaScript(GalleryPage.getGalleryHideCaption());
		galleryCaptionsClosedTest();
	}

	public static void galleryCaptionsClosedTest() {
		try {
			UMReporter.log(LogStatus.INFO, "<b>Info: Testing On Captions Close</b>");

			WrapperMethods.assertIsTrue(WrapperMethods.getWebElement(GalleryPage.getGalleryShowCaption()).isDisplayed(),
					"Gallery show Caption is present", "Gallery Show Caption is not present");

			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(GalleryPage.getGalleryTitle()).getText().isEmpty(),
					"Gallery title bar is present", "Gallery title bar is not present");
			WrapperMethods.assertIsTrue(
					!WrapperMethods.getWebElement(GalleryPage.getGalleryImageCaptionContent()).isDisplayed(),
					"Gallery caption is hidden", "Gallery caption is not hidden");

			String[] galimgs = WrapperMethods.getWebElement(GalleryPage.getGalleryPhotoCount()).getText().split(" ");
			int total = Integer.parseInt(galimgs[2]);

			WrapperMethods.assertIsTrue(GalleryPage.getGalleryImages().size() == total,
					"Gallery and FilmStrip has equal number of images",
					"Gallery and FilmStrip don't have equal number of elements");
			WrapperMethods.assertIsTrue(Integer.parseInt(galimgs[0]) > 0 && Integer.parseInt(galimgs[0]) <= total,
					"Current image index is correct", "Current image index is not correct");
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in accessing Gallery Caption Elements");
			UMReporter.log(LogStatus.INFO, E.getMessage());
		}
	}

	public static void showCaption() {

		WrapperMethods.clickJavaScript(GalleryPage.getGalleryShowCaption());
		GalleryPage.wait(10);
		galleryCaptionsOpenTest();

	}
	
	public static void testGalleryCaptions() {
		try {
			if (!ConfigProp.getPropertyValue("Platform").equalsIgnoreCase("Mobile")) {
				GalleryPageFunctions.galleryCaptionsOpenTest();
				GalleryPageFunctions.hideCaption();
				GalleryPageFunctions.showCaption();
			} else if (ConfigProp.getPropertyValue("Platform").equalsIgnoreCase("Desktop")
					|| ConfigProp.getPropertyValue("Platform").equalsIgnoreCase("Tablet")) {
				GalleryPageFunctions.showCaption();
				GalleryPageFunctions.hideCaption();
			} else if (ConfigProp.getPropertyValue("Platform").equalsIgnoreCase("Desktop")
					|| ConfigProp.getPropertyValue("Platform").equalsIgnoreCase("Tablet")) {
				GalleryPageFunctions.showCaption();
				GalleryPageFunctions.hideCaption();
			} else {
				// if(!System.getProperty("deviceType").equals("Tab"))
				GalleryPageFunctions.showCaption();
				GalleryPageFunctions.hideCaption();
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error in Accessing Gallery Elements" + E.getMessage());
		}

	}

}
