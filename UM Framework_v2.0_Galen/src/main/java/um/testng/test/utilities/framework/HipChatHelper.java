package um.testng.test.utilities.framework;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

import um.testng.test.utilities.framework.enums.MessageFormat;

public class HipChatHelper {
	private String authKey;

	public HipChatHelper(String authKey) {
		this.authKey = authKey;
	}

	public void messageUser(String users, String message, boolean notify,
			MessageFormat format) {		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.authKey);
		headers.put("Content-Type", "application/json");
		JsonObject msgObj = new JsonObject();
		msgObj.addProperty("message", message);
		msgObj.addProperty("notify", String.valueOf(notify));
		msgObj.addProperty("message_format", format.getValue());
		
		String msgBody = msgObj.toString();
		
		System.out.println(msgBody);
		try {
			String[] arrayUsers = users.split(";");
			for (String user : arrayUsers) {
				String endPoint = "http://api.hipchat.com/v2/user/" + user + "/message";
			String responseString = HTTPHelper.sendPOST(endPoint, headers, msgBody);
			System.out.println(responseString);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void shareFile(String users, String message, String attachmentPath) {		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + this.authKey);
		String contentType = "multipart/related";
		JsonObject msgObj = new JsonObject();
		msgObj.addProperty("message", message);		
		String msgBody = msgObj.toString();
		
		System.out.println(msgBody);
		try {
			String[] arrayUsers = users.split(";");
			for (String user : arrayUsers) {
				String endPoint = "http://api.hipchat.com/v2/user/" + user + "/share/file";
				String responseString = HTTPHelper.multiPartPost(endPoint, contentType, headers, attachmentPath, msgBody);
				System.out.println(responseString);				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	/*public static void main(String[] args) {
		HipChatHelper hipchat = new HipChatHelper("api");  //<APIKEY>
		hipchat.messageUser("@KeerthiPandurangarao@turner.com", "What?", true, MessageFormat.TEXT);
	}*/
}
