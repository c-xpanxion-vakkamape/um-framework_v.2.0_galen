package um.testng.test.utilities.framework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DBConnection {
	public static Map<String, Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>> dbData = new HashMap<String, Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>>();
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	// static final String DB_URL =
	// "jdbc:mysql://10.146.201.35:3306/test_report_db";
	static final String DB_URL = "jdbc:mysql://localhost:3306/sample";
	// Database credentials
	static final String USER = "root";
	// static final String PASS = "Turner@1";
	static final String PASS = "mysql";

	public static void main(String a[]) throws SQLException {
		String qur = "select * from UMReport where testname='nbaHomepage' And OS='windows' AND Browser='Firefox' And Environment='qa'";
		// String qur= "select * from UMReport;";
		System.out.println(jsonGenerator(qur));
	}

	public static String jsonGenerator(String dbQuery) {
		Connection conn = null;
		Statement stmt = null;
		System.out.println("Connecting to database...");
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			if (conn != null) {
				stmt = conn.createStatement();
				String sql = dbQuery;
				ResultSet resultSet = stmt.executeQuery(sql);
				ResultSetMetaData rsmd = resultSet.getMetaData();
				int columnsNumber = rsmd.getColumnCount();
				String mapName = "UMReport";
				int j = 0;
				while (resultSet.next()) {
					j++;
					// for (int i = 1; i <= columnsNumber; i++) {
					String testName = resultSet.getString("TestName");
					String os = resultSet.getString("OS");
					String browser = resultSet.getString("Browser");
					String environment = resultSet.getString("Environment");
					String status = resultSet.getString("Status");
					String startTime = resultSet.getString("StartTime");
					String endTime = resultSet.getString("EndTime");
					String failedStep = resultSet.getString("FailedStep");
					String duration = resultSet.getString("Duration");
					// System.out.println(testName+" :TestName "+os+" :TestName "+browser+" :TestName "+environment+" :TestName "
					// +status+" :TestName "+startTime+" :TestName "+endTime+" :TestName "+failedStep+" :TestName "+duration+" :TestName --- J=="+j);
					addData(mapName, testName, os, browser, environment,
							status, startTime, endTime, failedStep, duration);
					// System.out.print(columnValue + " " +
					// rsmd.getColumnName(i));
					// }
				}
				// System.out.println(j);
				// System.out.println(dbData.size());
				// System.out.println("Hash Map values --- "+dbData);
				return convertMapToJSon(dbData);
			}
		} catch (SQLException ex) {
			System.out
					.println("An error occurred. Maybe user/password is invalid");
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return null;
	}

	// Map structure
	// Map<String, Map<String, Map<String, Map<String, Map<String,
	// ArrayList<Map<String, String>>>>>>>
	// Map<UMReport,Map<TestName, Map<OS, Map<Browser, Map<Envi,
	// ArrayList<Map<Key, Values>>>>>>>
	public static void addData(String mapName,String testName,String os,String browser,String environment,
			String status,String startTime,String endTime,String failedStep,String duration){
		//test map 
		Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>> testMap = new 
				HashMap<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>();
		//OS Map 
		Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>> osMap = new 
				HashMap<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>();
		//Browser Map
		Map<String, Map<String, ArrayList<Map<String, String>>>> browserMap = new 
				HashMap<String, Map<String, ArrayList<Map<String, String>>>>();
		//Environment Map
		Map<String, ArrayList<Map<String, String>>> envMap = new 
				HashMap<String, ArrayList<Map<String, String>>>();
		//Env based contents
		ArrayList<Map<String, String>> details = new ArrayList<Map<String, String>>();
		//adding UMReport Name 
		testMap = dbData.computeIfAbsent(mapName,testNm -> new 
				HashMap<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>());
		//adding test name 
		osMap = testMap.computeIfAbsent(testName, omNm->new 
				HashMap<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>());
		//adding OS Name 
		browserMap=osMap.computeIfAbsent(os, broNm-> new 
				HashMap<String, Map<String, ArrayList<Map<String, String>>>>());
		//adding browser Name
		envMap= browserMap.computeIfAbsent(browser, envNm-> new 
				HashMap<String, ArrayList<Map<String, String>>>());
		//adding environment Name
		details = envMap.computeIfAbsent(environment, deNm-> new ArrayList<Map<String, String>>());
		// Adding the contents such as status,time
		Map<String, String> contents = new HashMap<String, String>();
		contents.put("Status", status);
		contents.put("StartTime", startTime);
		contents.put("EndTime", endTime);
		contents.put("FailedStep", failedStep);
		contents.put("Duration", duration);
		details.add(contents);
//		System.out.println("DEtails -- "+details);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String convertMapToJSon(
			Map<String, Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>> dbData) {

		JSONObject umObject = new JSONObject();
		Iterator itUMReport = dbData.entrySet().iterator();
		String ReportName = null;
		// JSONObject testObj = new JSONObject();
		while (itUMReport.hasNext()) {
			Map.Entry umReportEntry = (Map.Entry) itUMReport.next();
			// System.out.println(umReportEntry.getKey()+" ---- "+umReportEntry.getValue());
			ReportName = (String) umReportEntry.getKey();
			Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>> testMap = (Map<String, Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>>) umReportEntry
					.getValue();
			Iterator itTest = testMap.entrySet().iterator();
			String testName = null;
			JSONObject testObj = new JSONObject();
			// JSONObject osObj = new JSONObject();
			// System.out.println("The size of the testname ---- "+testMap.size());
			while (itTest.hasNext()) {
				Map.Entry testEntry = (Map.Entry) itTest.next();
				// System.out.println(testEntry.getKey()+" :----: "+testEntry.getValue());
				testName = (String) testEntry.getKey();
				Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>> osMap = (Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>) testEntry
						.getValue();
				Iterator itOS = osMap.entrySet().iterator();
				String os = null;
				JSONObject osObj = new JSONObject();
				// JSONObject browserOj = new JSONObject();
				// System.out.println("The size of the OSname ---- "+osMap.size());
				while (itOS.hasNext()) {
					Map.Entry osEntry = (Map.Entry) itOS.next();
					// System.out.println(osEntry.getKey()+" :----: "+osEntry.getValue());
					os = (String) osEntry.getKey();
					Map<String, Map<String, ArrayList<Map<String, String>>>> browserMap = (Map<String, Map<String, ArrayList<Map<String, String>>>>) osEntry
							.getValue();
					Iterator itBrowser = browserMap.entrySet().iterator();
					String browser = null;
					JSONObject browserOj = new JSONObject();
					// JSONObject envOj = new JSONObject();
					// System.out.println("The size of the browsername ---- "+osMap.size());
					while (itBrowser.hasNext()) {
						Map.Entry browserEntry = (Map.Entry) itBrowser.next();
						// System.out.println(browserEntry.getKey()+" :----: "+browserEntry.getValue());
						browser = (String) browserEntry.getKey();
						Map<String, ArrayList<Map<String, String>>> envMap = (Map<String, ArrayList<Map<String, String>>>) browserEntry
								.getValue();
						Iterator itEnv = envMap.entrySet().iterator();
						String env = null;
						JSONObject envOj = new JSONObject();
						// JSONArray contArray = new JSONArray();
						// System.out.println("The size of the envname ---- "+envMap.size());
						while (itEnv.hasNext()) {
							JSONArray contArray = new JSONArray();
							Map.Entry envEntry = (Map.Entry) itEnv.next();
							// System.out.println(envEntry.getKey()+" :----: "+envEntry.getValue());
							env = (String) envEntry.getKey();
							ArrayList<Map<String, String>> contents = (ArrayList<Map<String, String>>) envEntry
									.getValue();
							// System.out.println("The contents are -- "+contents);
							// JSONObject valuesArray = new JSONObject();
							for (Map<String, String> content : contents) {
								JSONObject valuesArray = new JSONObject();
								System.out.println("The content is --"
										+ content.size());
								Iterator obj = content.entrySet().iterator();
								while (obj.hasNext()) {
									Map.Entry objEntry = (Map.Entry) obj.next();
									valuesArray.put(objEntry.getKey(),
											objEntry.getValue());
								}
								// System.out.println(valuesArray);
								contArray.add(valuesArray);
								// System.out.println("The content array is -- "+contArray);
								// envOj.put(env, contArray);
							}
							// contArray.add(valuesArray);
							// System.out.println("The content array is -- "+contArray);
							envOj.put(env, contArray);
						}
						// envOj.put(env, contArray);
						browserOj.put(browser, envOj);
					}
					osObj.put(os, browserOj);
				}
				testObj.put(testName, osObj);
			}
			umObject.put(ReportName, testObj);
		}
		// System.out.println("The json is "+umObject);
		return umObject.toJSONString();
	}

}
