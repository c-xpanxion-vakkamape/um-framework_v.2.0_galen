package um.testng.test.utilities.functions;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.utilities.framework.UMReporter;


public class CompareHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompareHelper.class.getName());
	private CompareHelper(){

	}
	public static void compare(String update,String response) throws java.text.ParseException{

		try {
			JSONParser parser=new JSONParser();
			Object objResponse = parser.parse(response);
			Object objUpdate = parser.parse(update);
			String classType = objUpdate.getClass().getSimpleName();
			switch (classType) {
			case "JSONArray":
				compare("",(JSONArray)objResponse,(JSONArray)objUpdate);
				break;
			case "JSONObject":
				compare((JSONObject)objResponse,(JSONObject)objUpdate);
				break;
			case "String":
				UMReporter.log(LogStatus.PASS,"String in compare string" + objResponse + objUpdate);
				break;				
			default:
				UMReporter.log(LogStatus.FAIL,"Unknown: " + classType);
				break;
			}
		} catch ( ParseException e) {
			LOGGER.error("Exception : ", e);
		}
	}



	@SuppressWarnings("unchecked")
	private static void compare(JSONObject response,JSONObject update) throws java.text.ParseException{
		System.out.println(response.toString());
		for (Iterator<String> updateItr = update.keySet().iterator(); updateItr.hasNext();) {
			String updateKey = updateItr.next();
			String responseKey = updateKey.substring(0,1).toLowerCase()+ updateKey.substring(1);
			System.out.println("The response key "+responseKey);
			Object updateVal = update.get(updateKey);
			if((responseKey.contains("unique")||(responseKey.contains("aspectRatio"))==false)){
				if(response.containsKey(responseKey)){
					Object responseVal = response.get(responseKey);
					String classType = responseVal.getClass().getSimpleName();
//					System.out.println(responseVal+"class type of "+classType);
					switch (classType) {
					case "JSONArray":
						compare(updateKey,(JSONArray)responseVal,(JSONArray)updateVal);
						break;
					case "JSONObject":
						compare((JSONObject)responseVal,(JSONObject)updateVal);
						break;
					case "Long":
						if(updateVal.equals(responseVal)){
							UMReporter.log(LogStatus.PASS,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "==" + (String.valueOf(responseVal)));
						}else if(String.valueOf(updateVal).equalsIgnoreCase(String.valueOf(responseVal))){
							UMReporter.log(LogStatus.PASS,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "==" + (String.valueOf(responseVal)));
						}else{
							UMReporter.log(LogStatus.FAIL,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "!=" + (String.valueOf(responseVal)));
						}
						break;
					case "Boolean":
						if(updateVal.equals(responseVal)){
							UMReporter.log(LogStatus.PASS,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "==" + (String.valueOf(responseVal)));
						}else{
							UMReporter.log(LogStatus.FAIL,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "!=" + (String.valueOf(responseVal)));
						}
						break;
					case "Double":
						if(updateVal.equals(responseVal)){
							UMReporter.log(LogStatus.PASS,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "==" + (String.valueOf(responseVal)));
						}else{
							UMReporter.log(LogStatus.FAIL,"The " + updateKey + " in the update body and response contains " + (String.valueOf(updateVal)) + "!=" + (String.valueOf(responseVal)));
						}
						break;
					default:
//						System.out.println("The update val"+updateVal+" and class type is "+updateVal.getClass().getSimpleName());
//						System.out.println("The Response val"+responseVal+" and class type is "+responseVal.getClass().getSimpleName());
						if(updateVal.equals(responseVal)){
							UMReporter.log(LogStatus.PASS,"The  " + updateKey + " in the update body and response contains " + (String)updateVal + "==" + (String)responseVal);
						}else{
							UMReporter.log(LogStatus.FAIL,"The  " + updateKey + " in the update body and response contains " + (String)updateVal + "!=" + (String)responseVal);
						}
						break;
					}
				}else{
					UMReporter.log(LogStatus.FAIL	,"The response key "+ responseKey+ " and update key "+ updateKey +" are not same ");
				}
			}
		}
	}

	private static void compare(String key,JSONArray response,JSONArray update) throws java.text.ParseException{
		for (int i = 0; i < update.size(); i++) {
			Object updateElem = update.get(i);
			Object responseElem = response.get(i);
			String classType = updateElem.getClass().getSimpleName();
			switch (classType) {
			case "JSONArray":
				compare(key,(JSONArray)responseElem,(JSONArray)updateElem);
				break;
			case "JSONObject":
				compare((JSONObject)responseElem,(JSONObject)updateElem);
				break;
			case "String":
				if(responseElem.equals(updateElem)){
					UMReporter.log(LogStatus.PASS,"The " + key + " in the update body and response contains " + (String)updateElem + "==" + (String)responseElem);
				}else{
					UMReporter.log(LogStatus.FAIL,"The " + key + " in the update body and response contains " + (String)updateElem + "!=" + (String)responseElem);
				}
				break;
			default:
				System.out.println("Default in compare jsonarray");
				break;
			}			
		}
	}
}