package um.testng.test.utilities.galen;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;
import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.framework.UMReporter;

public abstract class GalenTestBase {
public static WebDriver driver= DriverFactory.getCurrentDriver();

public static void checkLayout( String spec,
List<String> includedTags, String filename, String layout) throws IOException {
	try{
		
        String specPath=Paths.get("").toAbsolutePath().toString()+"//LayoutSpecs//"+spec;
        LayoutReport layoutReport = Galen.checkLayout(driver, specPath,includedTags);
        final String RELATIVE_LOC = Paths.get("").toAbsolutePath().toString();
        final String PROJECT_LOC = buildInput(RELATIVE_LOC);
        String galenFolder=UMReporter.reportPath+"/galen-html-reports";
        String userDirectory = PROJECT_LOC +galenFolder+"/";
        List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
        GalenTestInfo test = GalenTestInfo.fromString(filename);
        test.getReport().layout(layoutReport,
                     "check layout on " + layout );
        tests.add(test);
        System.out.println("@@@@ "+userDirectory);
        new HtmlReportBuilder().build(tests, userDirectory);//HtmlReportBuilder().build(tests, "galen-html-reports");
        System.out.println("$$$ "+layoutReport.errors());
        if (layoutReport.errors()>0 ) {
               
               UMReporter.log(LogStatus.FAIL, "Incorrect layout for the test case : "+filename );

        } else {
               
               UMReporter.log(LogStatus.PASS, "The Layout tests passed : "+filename);
               
        }
        
       
        UMReporter.log(LogStatus.INFO, "<font color=\"blue\">" + "<a href="        
                     + "\"" + userDirectory + "1-" + filename + ".html" + "\""
                     + " target=\"_blank\"" + ">" + "Galen Layout Result File"
                     + "</a></i></font><br/>");
}
catch(Exception e)
{
	System.out.println(e.getMessage());
}
       }

	
	private static String buildInput(String loc){
		System.out.println("%%% "+loc);
		if (loc.contains("target")){
			return loc.substring(0, loc.length() - 7);
		}
		else{
			return loc;
		}
	}
	public static class TestDevice{
		private final String name;
    	private final List<String> tags;
    public TestDevice(String name, List<String> tags) {
    	
        this.name = name;
        
        this.tags = tags;
    }
    public String getName() {
        return name;
 }
    public List<String> getTags() {
        return tags;
 }

 }

}