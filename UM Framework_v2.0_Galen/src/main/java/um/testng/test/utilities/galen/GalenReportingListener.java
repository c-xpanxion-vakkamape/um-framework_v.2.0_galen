package um.testng.test.utilities.galen;

import java.util.List;

import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.galen.GalenReportsContainer;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

public class GalenReportingListener implements IReporter {

    @Override
    public  void generateReport(List<XmlSuite> xmlSuites, List<ISuite> iSuites, String s) {
        System.out.println("Generating Galen Html reports");
        List<GalenTestInfo> tests = GalenReportsContainer.get().getAllTests();

        try {
        	String galenFolder=UMReporter.reportPath+"/galen-html-reports";
        	new HtmlReportBuilder().build(tests, galenFolder);
            //new HtmlReportBuilder().build(tests, "target/galen-html-reports");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
