package um.testng.test.utilities.framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import um.testng.test.alltestpack.EnvironmentHelper;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.utilities.functions.JsonValidater;
import um.testng.test.utilities.functions.jsonSimpleReader;
import com.google.common.base.CaseFormat;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.LogStatus;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class MethodDef {

	/**
	 * Logs Fail Message into Reporter log
	 * 
	 * @param string
	 */
	public static void failLog(String message, String string) {
		Reporter.log("<br><font color=\"red\">FAILED:" + string + "</font><br>");
		Reporter.log("<br> Error:" + message);
	}

	/**
	 * Logs Pass Message into Reporter log
	 * 
	 * @param string
	 */
	public static void passLog(String string) {
		Reporter.log("<br><font color=\"green\">PASSED:" + string + "</font>");
	}

	/**
	 * Checks and fails tests if any of the validation failed
	 * 
	 * @param error
	 */
	public static void resultLog(SoftAssert error) {
		if (error.hasError())
			Assert.fail(error.getErrors());
	}

	public static void brokenLinkValidation(String URL) {
		System.out.println("inside broken link");
		int response = getResponseCode(URL);
		if (response == 200) {
			System.out.println("working 200");
			// Reporter.log(" RESPONSE: " + response + "-URL is working");
			// System.out.println(URL + "-URL is working");
			// passLog(URL+"-URL is working");
			UMReporter.log(LogStatus.PASS, URL + "-URL is working");
		} else {
			System.out.println("not working NOT 200");
			// System.out.println(URL + "-URL is not working");
			// failLog(URL + "-URL is not working", "");
			UMReporter.log(LogStatus.FAIL, URL + "-URL is not working");
		}
	}
	
	public static void brokenLinkValidation(List<WebElement> elems) throws IOException {
		for (WebElement elem : elems) {
			String url = elem.getAttribute("href");
			if (url.startsWith("//")) {
				url = "http:" + url;
			} else if (url.startsWith("/") || url.equals("#")) {
				url = EnvironmentHelper.getURL() + url;
			}
			int response = WrapperMethods.getResponseCode(url);
			if (response == 200) {
				passLog(elem.getText() + " " + url);
				UMReporter.log(LogStatus.PASS, "RESPONSE: " + response + "-URL is working");
			} else if (response >= 400 && response <= 505) {
				UMReporter.log(LogStatus.FAIL, "RESPONSE:" + response + url + " URL Appears to be broken");
			} else {
				UMReporter.log(LogStatus.FAIL,
						"RESPONSE:" + response + url + " UnExpected Response - Handle it in script");
			}
		}
	}


	public static void brokenLinkValidationStrict(String URL) {
		if (!URL.isEmpty()) {
			int response = getResponseCode(URL);
			if (response == 200 || response == 401) {
				Reporter.log(" RESPONSE: " + response + "-URL is working" + response);
				System.out.println(URL + "-URL is working" + response);
			} else {
				System.out.println(URL + "-URL is not working" + response);
				org.testng.Assert.fail("you wandered onto the wrong URL" + response);
			}
		} else {
			System.out.println("-URL is EMPTY");
		}
	}

	public static int getResponseCode(String urlString) {
		try {
			URL u = new URL(urlString);
			HttpURLConnection h = (HttpURLConnection) u.openConnection();
			h.setRequestMethod("GET");
			h.connect();
			if (h.getResponseCode() == 301 || h.getResponseCode() == 302 || h.getResponseCode() == 307) {
				String newUrl = h.getHeaderField("Location");
				u = new URL(newUrl);
				h = (HttpURLConnection) u.openConnection();
				h.setRequestMethod("GET");
				h.connect();
			}
			return h.getResponseCode();

		} catch (MalformedURLException e) {
			Reporter.log("<BR>FAILED: MalformedURLException" + e.getMessage());
			System.out.println(e.getMessage());
			return -1;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	public static String logDriverInfo() {
		// System.out.println("" + elem.keySet());
		// System.out.println("OS" + elem.get("OS"));
		// System.out.println("Platform" + elem.get("Platform"));
		// String OS = elem.get("OS");
		String result = "";
		String Platform = ConfigProvider.getConfig("Platform");
		if (Platform.equals("Desktop")) {
			result = getIP();
		} else {
			try {
				String Plat = ConfigProvider.getConfig("Device_ID");
				System.out.println("Device ID" + Plat);
				String[] app_dev = ConfigProp.getPropertyValue("Devices").split(",");
				for (String dev : app_dev) {
					String[] devport = dev.split("~");
					// System.out.println("Device Name test" + devport[1]);
					if (devport[0].equalsIgnoreCase(Plat)) {
						System.out.println("Device Name" + devport[1]);
						result = devport[1];
					}
				}
			} catch (Exception e) {
				System.out.println("*************************************" + e.getMessage());
			}
		}
		return result;
	}

	private static String getIP() {
		WebDriver curr = DriverFactory.getCurrentDriver();
		String id = ((RemoteWebDriver) curr).getSessionId().toString();
		System.out.println(id);
		String ip = "";
		try {
			String hub = ConfigProp.getPropertyValue("SelHost").split("/wd/hub")[0];
			System.out.println("HUB" + hub);
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(hub + "/grid/api/testsession?session=" + id);
			HttpResponse response = client.execute(request);
			// System.out.println(response);
			JsonObject myjsonobject = extractObject(response);
			JsonElement urltest = myjsonobject.get("proxyId");
			// JsonElement urltest1 = myjsonobject.get("os");
			System.out.println(urltest.getAsString());
			ip = urltest.getAsString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ip;
	}

	private static JsonObject extractObject(HttpResponse resp) throws IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
		StringBuffer s = new StringBuffer();
		String line;
		while ((line = rd.readLine()) != null) {
			s.append(line);
		}
		rd.close();
		JsonParser parser = new JsonParser();
		JsonObject objToReturn = (JsonObject) parser.parse(s.toString());
		System.out.println(objToReturn.toString());
		System.out.println(objToReturn.get("proxyId"));
		return objToReturn;
	}

	/**
	 * Correct the supplied URL in case the domain detail is not given
	 * 
	 * @param url
	 * @return
	 */
	public static String correctUrl(String url) {
		String curl = (EnvironmentHelper.getURL() + url).substring(0, (EnvironmentHelper.getURL() + url).length());
		return curl;
	}

	public static void explicitWaitVisibility(By Byele, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			WebElement element = driver.findElement(Byele);
			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.PASS, Passmsg);
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	/**
	 * wait for visibility of a particular element for specified seconds
	 * 
	 * @param elem
	 *            Element to be visible
	 * @param waittime
	 *            Element to be visible
	 */
	public static void explicitWaitVisibility(By Byele, int waittime, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, waittime);
		try {
			WebElement element = driver.findElement(Byele);

			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.PASS, Passmsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	/**
	 * wait for visibility of a particular element for specified seconds
	 * 
	 * @param go
	 *            Webdriver Instance
	 * @param error
	 *            Soft Error Instance
	 * @param elem
	 *            Element to be visible
	 * @param waittime
	 *            Element to be visible
	 */
	public static void explicitWaitVisibilityNoError(By Byele, int waittime, String Passmsg, String Warningmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, waittime);
		try {
			WebElement element = driver.findElement(Byele);

			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.PASS, Passmsg);
		} catch (Exception e) {
			UMReporter.log(LogStatus.WARNING, Warningmsg);
		}
	}

	/**
	 * wait for visibility of a particular element for specified seconds
	 * 
	 * @param elem
	 *            Element to be visible
	 * @param waittime
	 *            Element to be visible
	 */
	public static void explicitWaitVisibilityCss(String path, int waittime, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, waittime);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(path)));
			UMReporter.log(LogStatus.PASS, Passmsg);
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	/**
	 * wait for visibility of a particular element for specified seconds
	 * 
	 * @param elem
	 *            Element to be visible
	 * @param waittime
	 *            Element to be visible
	 */
	public static void explicitWaitVisibilityXpath(String path, int waittime, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, waittime);

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));
			UMReporter.log(LogStatus.PASS, Passmsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	/**
	 * wait for visibility of a particular element (doesn't return error) -15
	 * seconds
	 * 
	 * @param go
	 *            Webdriver Instance
	 * @param error
	 *            Soft Error Instance
	 * @param elem
	 *            Element to be visible
	 */
	public static void explicitWaitVisibilityNoError(By Byele, String Passmsg, String Warningmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			WebElement element = driver.findElement(Byele);

			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.PASS, Passmsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, Warningmsg);
		}
	}

	public static void explicitWaitVisibilityNoErrorInfo(By Byele, String PassInfo, String WarningInfo) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			WebElement element = driver.findElement(Byele);

			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.INFO, PassInfo);

		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, PassInfo);
		}
	}

	/**
	 * wait for a particular element to be clickable -15 seconds
	 * 
	 * @param elem
	 *            Element under test
	 */
	public static void explicitWaitClickable(By Byele, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			WebElement element = driver.findElement(Byele);

			wait.until(ExpectedConditions.elementToBeClickable(element));
			UMReporter.log(LogStatus.PASS, Passmsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	/**
	 * Implicit wait
	 * 
	 * @param sec
	 *            Seconds to wait
	 */
	public static void implicitWait(int sec) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		try {
			driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Check if Terms and Service element is present and closes the same
	 * 
	 * @param go
	 *            webdriver
	 * @param error
	 *            soft error instance
	 * @param getTermsServiceConsentClose
	 *            element
	 */
	public static void cnnConsent(By getTermsServiceConsentClose) {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();

			explicitWaitVisibilityNoErrorInfo(getTermsServiceConsentClose, "The CNN consent is present",
					"The CNN consent is not present");
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", getTermsServiceConsentClose);
			UMReporter.log(LogStatus.INFO, "Cookie Consent close button was clicked");
		} catch (Exception E) {
			UMReporter.log(LogStatus.INFO, "Consent not present");
		}
	}

	/**
	 * Check if 15second auto close ad is present and closes the same
	 * 
	 * @param go
	 *            webdriver
	 * @param error
	 *            soft error instance
	 * @param skipAd
	 *            element
	 */
	public static void cnnCloseAd(By skipAd) {
		explicitWaitVisibilityNoErrorInfo(skipAd, "The CNN Close ad is displayed", "The CNN close ad is not displayed");
		WrapperMethods.click(skipAd, "skip Ad");
		UMReporter.log(LogStatus.INFO, "Ad Close button was clicked");
	}

	/**
	 * Gets current URL of the page webdriver is interacting with
	 * 
	 * @return
	 */
	public static String getURL(WebDriver go) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		return driver.getCurrentUrl();
	}

	public static String getURL() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		return driver.getCurrentUrl();
	}

	/*
	 * public static void brokenLinkValidation(List<WebElement> elems) throws
	 * IOException { for (WebElement elem : elems) { String url =
	 * elem.getAttribute("href"); if (url.startsWith("//")) { url = "http:" +
	 * url; } else if (url.startsWith("/") || url.equals("#")) { url =
	 * EnvironmentHelper.getURL() + url; } int response =
	 * WrapperMethods.getResponseCode(url); if (response == 200) {
	 * UMReporter.log(LogStatus.PASS, elem.getText() + " " + url);
	 * UMReporter.log(LogStatus.INFO, response + "-URL is working"); } else if
	 * (response >= 400 && response <= 505) { UMReporter.log(LogStatus.FAIL,
	 * "RESPONSE:" + response + " - " + url + " URL Appears to be broken");
	 * UMReporter.log(LogStatus.INFO, response + "-URL Appears to be broken"); }
	 * else { UMReporter.log(LogStatus.FAIL, "RESPONSE:" + response + " - " +
	 * url + " UnExpected Response - Handle it in script");
	 * UMReporter.log(LogStatus.INFO,
	 * "UnExpected Response - Handle it in script");
	 * 
	 * } } }
	 */

	/**
	 * Validated if the Link Text href has the expected URL link
	 * 
	 * @param go
	 *            webdriver instance
	 * @param error
	 *            error instance
	 * @param path
	 *            css locator
	 * @param linktext
	 *            link text to be validated against
	 * @param href
	 *            URL to be validated against
	 */
	public static void validateLinkTextHref(String path, String linktext, String href) {
		WebElement elem = WrapperMethods.findElementCss(path);
		Reporter.log("<br>Object under Test: " + linktext);

		if (!elem.getText().equals("")) {
			WrapperMethods.assertIsTrue(elem.getText().equalsIgnoreCase(linktext),
					"Link has the correct Text:  " + elem.getText(), "Link Text is wrong:  " + elem.getText());
		}

		if (!href.startsWith("//")) {
			String curl = EnvironmentHelper.getURL().substring(0, EnvironmentHelper.getURL().length() - 1) + href;
			WrapperMethods.assertIsTrue(curl.contains(elem.getAttribute("href")),
					"Link has the correct URL:  " + elem.getAttribute("href"),
					"Link HREF is wrong:  " + elem.getAttribute("href") + "Expected" + curl);
		} else {
			WrapperMethods.assertIsTrue(elem.getAttribute("href").contains(href),
					"Link has the correct URL:  " + elem.getAttribute("href"),
					"Link HREF is wrong:  " + elem.getAttribute("href"));
		}
	}

	/**
	 * Explicit wait on the visiblity of an element located by path
	 * 
	 * @param path
	 *            path to locate the element
	 */
	public static void explicitWaitVisibilityCss(String path, String PassMsg, String FailMsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(path)));
			UMReporter.log(LogStatus.PASS, PassMsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, FailMsg);
			UMReporter.log(LogStatus.INFO, e.getMessage());

		}

	}

	/**
	 * Explicit wait on the Presence of an element located by path
	 * 
	 * @param go
	 *            webdriver instance
	 * @param error
	 *            soft error instance
	 * @param path
	 *            path to locate the element
	 */
	public static void explicitWaitPresenceCss(By ele, String PassMsg, String FailMsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(ele));
			UMReporter.log(LogStatus.PASS, PassMsg);

		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, FailMsg);
			UMReporter.log(LogStatus.INFO, e.getMessage());
		}

	}

	public static void moveAndClick(String brow, By Byele, String Elementname) {
		if (System.getProperty("deviceType").equals("Desktop") && !brow.equals("SAFARI"))
			WrapperMethods.moveToElement(Byele);
		else
			WrapperMethods.click(Byele, Elementname);
	}

	/**
	 * Validated if the Link Text href has the expected URL link
	 * 
	 * @param go
	 *            webdriver instance
	 * @param error
	 *            error instance
	 * @param path
	 *            xpath locator
	 * @param linktext
	 *            link text to be validated against
	 * @param href
	 *            URL to be validated against
	 */
	public static void validateLinkTextHrefXpath(String path, String linktext, String href) {
		WebElement elem = WrapperMethods.findElementXpath(path);
		Reporter.log("<br>Object under Test: " + linktext);
		WrapperMethods.assertIsTrue(elem.getText().equals(linktext), "Link has the correct Text: " + elem.getText(),
				"Link Text is wrong: " + elem.getText());
		if (!href.startsWith("//")) {
			String curl = EnvironmentHelper.getURL().substring(0, EnvironmentHelper.getURL().length() - 1) + href;
			WrapperMethods.assertIsTrue(curl.contains(elem.getAttribute("href")),
					"Link has the correct URL: " + elem.getAttribute("href"),
					"Link HREF is wrong: " + elem.getAttribute("href") + "Expected" + curl);
		} else {
			WrapperMethods.assertIsTrue(elem.getAttribute("href").contains(href),
					"Link has the correct URL:  " + elem.getAttribute("href"),
					"Link HREF is wrong:  " + elem.getAttribute("href"));
		}
	}

	public static void vidPlayerBasicValidations(WebElement videoPlayer) throws InterruptedException {
		WebDriver driver = DriverFactory.getCurrentDriver();

		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		try {
			WrapperMethods.theoplayerelementpresent();
		} catch (Exception e) {
			UMReporter.log(LogStatus.WARNING, "Video is not auto-played");
			WrapperMethods.clickJavaScript(By.cssSelector("#player-large-media_0 div.vjs-big-play-button"));
			WrapperMethods.theoplayerelementpresent();
		}

		Boolean paused = (Boolean) jsExecutor.executeScript("return arguments[0].paused", videoPlayer);

		WrapperMethods.assertIsTrue(!paused, "Initially Video is not paused", "Initially Video is paused");

		Boolean muted = (Boolean) jsExecutor.executeScript("return arguments[0].muted", videoPlayer);

		WrapperMethods.assertIsTrue(!muted, "Initially Video is not muted", "Initially Video is not muted");

		try {
			Double duration = (Double) jsExecutor.executeScript("return arguments[0].duration", videoPlayer);

			WrapperMethods.assertIsTrue(duration > 0, "Vid length is not zero " + duration,
					"Vid length is zero" + duration);
		} catch (Exception e) {
			UMReporter.log(LogStatus.INFO, "Skipping duration for Video Landing Page");
		}
		jsExecutor.executeScript("arguments[0].pause()", videoPlayer);

		paused = (Boolean) jsExecutor.executeScript("return arguments[0].paused", videoPlayer);

		WrapperMethods.assertIsTrue(paused, "Pause button is working", "Pause button is not working");

		jsExecutor.executeScript("arguments[0].play()", videoPlayer);

		WrapperMethods.assertIsTrue(paused, "Play button is working", "Play button is not working");

		jsExecutor.executeScript("arguments[0].muted=true", videoPlayer);

		muted = (Boolean) jsExecutor.executeScript("return arguments[0].muted", videoPlayer);

		WrapperMethods.assertIsTrue(muted, "Video Mute is working", "Video Mute is not working");

		String source = (String) jsExecutor.executeScript("return arguments[0].currentSrc;", videoPlayer);

		WrapperMethods.assertIsTrue(!source.isEmpty(), "Video source is loaded", "Video source is not loaded");

		jsExecutor.executeScript("arguments[0].muted=false", videoPlayer);

		Double volume = (Double) jsExecutor.executeScript("return arguments[0].volume", videoPlayer);

		UMReporter.log(LogStatus.INFO, "Current Volume " + volume);
		Double volplus = volume + 0.2;
		String voladj = "return arguments[0].volume=" + volplus;
		UMReporter.log(LogStatus.INFO, "Adjusting volume to add " + volplus);

		Double volumechanged = (Double) jsExecutor.executeScript(voladj, videoPlayer);

		WrapperMethods.assertIsTrue(!(volumechanged == volume), "Volume change is working",
				"Volume change is not working");
	}

	public static WebElement getvideoObject() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		WebElement videoPlayer = null;
		try {
			videoPlayer = driver.findElement(By.xpath("//video[@preload='none']"));
			UMReporter.log(LogStatus.PASS, "Video element is loaded");
			System.out.println("---------------------------------------1");
		} catch (Exception e) {
			try {
				videoPlayer = driver.findElement(By.xpath("//video"));
				UMReporter.log(LogStatus.PASS, "Video element is loaded");
				System.out.println("---------------------------------------2");

			} catch (Exception ae) {
				try {
					videoPlayer = driver.findElement(By.xpath("//object"));
					UMReporter.log(LogStatus.PASS, "Video element is loaded");
					System.out.println("---------------------------------------3");

				} catch (Exception E) {
					/*
					 * MethodDef .failLog(E.getMessage(),
					 * "Video Element is not loaded"); throw E;
					 */
				}
			}
		}
		return videoPlayer;
	}

	/*
	 * public static void moveToElem(String brow, WebElement elem) { if
	 * (System.getProperty("deviceType").equals("Desktop") &&
	 * !brow.equals("SAFARI")) WrapperMethods.moveToElement(elem); }
	 */

	public static void isNotVisible(By Byele) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement element = driver.findElement(Byele);
		int elemYCoord = element.getLocation().getY();
		int y = ((Long) ((JavascriptExecutor) driver).executeScript("return window.pageYOffset;")).intValue();
		if (y < elemYCoord) {
			UMReporter.log(LogStatus.FAIL, "Element is visible in the viewport");
		} else {
			UMReporter.log(LogStatus.PASS, "Element is not visible in the viewport");
		}
	}

	public static void isVisible(By byele) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement element = driver.findElement(byele);
		int elemYCoord = element.getLocation().getY();
		int y = ((Long) ((JavascriptExecutor) driver).executeScript("return window.pageYOffset;")).intValue();
		if (y < elemYCoord)
			UMReporter.log(LogStatus.PASS, "Element is visible in the viewport");
		else {
			UMReporter.log(LogStatus.FAIL, "Element is not visible in the viewport");
		}

	}

	public static boolean ElementisClickable(By byele) {
		try {
			WebDriver driver = DriverFactory.getCurrentDriver();
			WebElement element = driver.findElement(byele);

			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void navUrl(String url) {
		WebDriver driver = DriverFactory.getCurrentDriver();

		if (url.startsWith("/")) {
			url = correctUrl(url);
			driver.get(url);
		} else {
			driver.get(url);
		}

		WrapperMethods.waitForPageLoaded(driver);

	}

	private static String getZonesUrl() {
		String Env = null;
		String Site = null;

		if (System.getProperty("edition").equals("DOM")) {
			Site = "domestic";
		} else {
			Site = "international";
		}
		if (System.getProperty("environment").equals("REF")) {
			Env = "ref";
		} else if (System.getProperty("environment").equals("ENABLE")) {
			Env = "enable";
		} else if (System.getProperty("environment").equals("TERRA")) {
			Env = "terra";
		} else if (System.getProperty("environment").equals("VIDEO")) {
			Env = "video";
		} else if (System.getProperty("environment").equals("SWEET")) {
			Env = "sweet";
		} else if (System.getProperty("environment").equals("POLITICS")) {
			Env = "politics";
		} else if (System.getProperty("environment").equals("STAGE")) {
			Env = "stage";
		} else
			Env = "prod";
		String uRL = "http://data.cnn.com/cfg/v1/" + Site + "/" + Env + "/zones.json";
		return uRL;
	}

	private static String getMichonneUrl() {
		String Env = null;
		String Site = null;

		if (System.getProperty("edition").equals("DOM")) {
			Site = "domestic";
		} else {
			Site = "international";
		}
		if (System.getProperty("environment").equals("REF")) {
			Env = "ref";
		} else if (System.getProperty("environment").equals("ENABLE")) {
			Env = "enable";
		} else if (System.getProperty("environment").equals("TERRA")) {
			Env = "terra";
		} else if (System.getProperty("environment").equals("VIDEO")) {
			Env = "video";
		} else if (System.getProperty("environment").equals("SWEET")) {
			Env = "sweet";
		} else if (System.getProperty("environment").equals("POLITICS")) {
			Env = "politics";
		} else if (System.getProperty("environment").equals("STAGE")) {
			Env = "stage";
		} else
			Env = "prod";
		String uRL = "http://data.cnn.com/cfg/v1/" + Site + "/" + Env + "/michonne.json";
		return uRL;
	}

	private static Boolean getJsonFromMichonne(String Tag) {
		String uRL = getMichonneUrl();

		JSONObject test;
		System.out.println("---------------------------------1");
		try {
			test = JsonValidater.readJsonFromUrl(uRL);
			System.out.println("---------------------------------4");

			String jsonText = test.toString();
			Boolean Test = jsonSimpleReader.parseCNNFeatures(jsonText, Tag);
			return Test;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Boolean getJsonReadMore() {
		return getJsonFromMichonne("enableReadMore");
	}

	public static Boolean getConfigBreakingNews() {
		return getJsonFromMichonne("enableBreakingNews");
	}

	public static Boolean getJsonVideoLandingLazyLoadFlag() {
		return getJsonFromVideosLandingPageIndex("lazyLoad");
	}

	public static String getJsonVideoLandingZones(String Dimension) {
		return getJsonFromVideoLandingZones(Dimension);
	}

	public static Boolean getJsonVideoLeafLazyLoadFlag() {
		return getJsonFromVideosLeafPageIndex("lazyLoad");
	}

	public static String getJsonVideoLeafZones(String Dimension) {
		return getJsonFromVideoLeafZones(Dimension);
	}

	public static Boolean getJsonVideoUnificationFlag() {
		return getJsonFromMichonne("enableVideoExperienceUnification");
	}

	public static Boolean getJsonVideoPinned() {
		return getJsonFromMichonne("enableVideoPinning");
	}

	private static Boolean getJsonFromVideosLandingPageIndex(String Tag) {
		String uRL = getZonesUrl();

		JSONObject test;
		try {
			test = JsonValidater.readJsonFromUrl(uRL);
			String jsonText = test.toString();
			Boolean Test = jsonSimpleReader.parseVideosLandingPageIndex(jsonText, Tag);
			return Test;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String getJsonFromVideoLandingZones(String Dimension) {
		String uRL = getZonesUrl();

		JSONObject test;
		try {
			test = JsonValidater.readJsonFromUrl(uRL);
			String jsonText = test.toString();
			String Test = jsonSimpleReader.parseVideoLandingIndexZones(jsonText, Dimension);
			return Test;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Boolean getJsonFromVideosLeafPageIndex(String Tag) {
		String uRL = getZonesUrl();

		JSONObject test;
		try {
			test = JsonValidater.readJsonFromUrl(uRL);
			String jsonText = test.toString();
			Boolean Test = jsonSimpleReader.parseVideosLeafPageIndex(jsonText, Tag);
			return Test;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String getJsonFromVideoLeafZones(String Dimension) {
		String uRL = getZonesUrl();

		JSONObject test;
		try {
			test = JsonValidater.readJsonFromUrl(uRL);
			String jsonText = test.toString();
			String Test = jsonSimpleReader.parseVideoLeafIndexZones(jsonText, Dimension);
			return Test;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Boolean chkDeviceHdr(int windowWidth) {
		int size = Integer.parseInt(ConfigProp.getPropertyValue("hdr_dev_viewport"));
		if (windowWidth < size)
			return true;
		else
			return false;
	}

	public static void perfectoLoad() {
		if (ConfigProp.getPropertyValue("Platform").equals("Perfecto"))
			WrapperMethods.waitForPageLoaded(DriverFactory.getCurrentDriver());
	}

	public static void splitHrefValidationXpath(Set<Entry<String, List<String>>> entrySet) {
		for (Entry<String, List<String>> entry : entrySet) {
			String path = entry.getKey();
			List<String> value = entry.getValue();
			String linktext = value.get(0);
			String href = value.get(1);
			validateLinkTextHrefXpath(path, linktext, href);
		}
	}

	public static void YoutubePlayervalidation(WebElement videoPlayer) {
		WebDriver go = DriverFactory.getCurrentDriver();
		JavascriptExecutor jsExecutor = (JavascriptExecutor) go;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Boolean paused = (Boolean) jsExecutor.executeScript("return arguments[0].paused", videoPlayer);

		Boolean muted = (Boolean) jsExecutor.executeScript("return arguments[0].muted", videoPlayer);

		WrapperMethods.assertIsTrue(!muted, "Initially Video is not muted", "Initially Video is not paused");

		try {
			Double duration = (Double) jsExecutor.executeScript("return arguments[0].duration", videoPlayer);

			WrapperMethods.assertIsTrue(duration > 0, "Vid length is not zero " + duration,
					"Vid length is zero" + duration);
		} catch (Exception e) {
			Reporter.log("Skipping duration for Video Landing Page");
		}

		jsExecutor.executeScript("arguments[0].pause()", videoPlayer);

		paused = (Boolean) jsExecutor.executeScript("return arguments[0].paused", videoPlayer);

		WrapperMethods.assertIsTrue(paused, "Pause button is working", "Pause button is not working");

		jsExecutor.executeScript("arguments[0].play()", videoPlayer);

		WrapperMethods.assertIsTrue(paused, "Play button is working", "Play button is not working");

		jsExecutor.executeScript("arguments[0].muted=true", videoPlayer);

		muted = (Boolean) jsExecutor.executeScript("return arguments[0].muted", videoPlayer);

		WrapperMethods.assertIsTrue(muted, "Video Mute is working", "Video Mute is not working");
		jsExecutor.executeScript("arguments[0].muted=false", videoPlayer);
	}

	public static void explicitWaitVisibility(WebElement element, String Passmsg, String Failmsg) {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			UMReporter.log(LogStatus.PASS, Passmsg);
		} catch (Exception e) {
			UMReporter.log(LogStatus.FAIL, Failmsg);
			UMReporter.log(LogStatus.WARNING, e.getMessage());
		}
	}

	public static void screenshot() {
		WebDriver go = DriverFactory.getCurrentDriver();
		Date screentime = new java.sql.Timestamp(System.currentTimeMillis());
		DateFormat format = new SimpleDateFormat("YY.MM.d_hh.mm.ss a,z");
		File scrFile = ((TakesScreenshot) go).getScreenshotAs(OutputType.FILE);
		char[] charArray = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
		String starttime_formatted = format.format(screentime);
		String failureImageFileName = starttime_formatted + "_" + RandomStringUtils.random(10, charArray)
				+ "_screenshot.png";
		try {
			FileUtils.copyFile(scrFile, new File(Constants.PROJECT_LOC + "/screenshots/" + failureImageFileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sc_env1 = "";
		String sc_env = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, System.getProperty("environment"));
		if (System.getProperty("environment").equals("PROD"))
			sc_env1 = "PROD";
		else
			sc_env1 = sc_env;
		UMReporter.log(LogStatus.INFO,
				"<font color=\"red\">><i>FAILURE SCREENSHOT " + "<a href = \"http://mssqacoreqa01.turner.com:8080/view/"
						+ sc_env1 + "/job/" + System.getProperty("JOB_NAME")
						+ "/ws/cnn-expanxion-automation/screenshots/" + failureImageFileName + "\""
						+ " target=\"_blank\"" + ">" + "Screenshot" + "</a></i></font><br/>");

	}
}