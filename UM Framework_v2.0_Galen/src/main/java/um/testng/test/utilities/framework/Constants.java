package um.testng.test.utilities.framework;

import java.nio.file.Paths;

/**
 * Holds values of constants for use throughout the project.
 * Provides a flexible way to change values without constant refactoring, as project needs change.
 */
public class Constants {
	
	// grabs the current working directory
	public static final String RELATIVE_LOC = Paths.get("").toAbsolutePath().toString();
	// gets the main project directory
	public static final String PROJECT_LOC = buildInput(RELATIVE_LOC);
	// gets the path to the input files for excel parsing
	public static final String INPUT_PATH = PROJECT_LOC + "/src/test/input_files/";
	// gives browsers to compare inputs to
	//public static final String DEFAULT_BROWSERS = "FIREFOX";
	public static final String DEFAULT_BROWSERS = "SAFARI,FIREFOX,CHROME,IE8,IE9,IE10,IE11";
	
	public static final int IMPLICIT_WAIT = 1;
	public static final int MAX_TIMEOUT = 30;
	
	public static final String prodURL = "http://www.cnn.com/";
	
	public static final String hub = ConfigProp.getPropertyValue("hubURL");
	
	// holds the address of the grid hub to be registered to
	public static final String gridHub = hub;//"http://mssselgrid01.turner.com:4444/wd/hub";
	public static final String appiumHub = "http://127.0.0.1:4723/wd/hub";
	public static final String perfectoHub = "https://turnergto.perfectomobile.com/nexperience/perfectomobile/wd/hub";
	//public static final String gridHub = "http://mssselgrid01.turner.com:4444/wd/hub";
	//public static final String gridHub = "http://mssqauitool-01.turner.com:4444/wd/hub";
	
	//public static final String gridHub = "http://127.0.0.1:4445/wd/hub";
	
	
	// adjusts working directory based on whether the project is launched from eclipse or maven
	private static String buildInput(String loc){
		if (loc.contains("target")){
			return loc.substring(0, loc.length() - 7);
		}
		else{
			return loc;
		}
	}
}


