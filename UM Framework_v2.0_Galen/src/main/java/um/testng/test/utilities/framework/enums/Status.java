package um.testng.test.utilities.framework.enums;

//public class CouchStatus {
//
//}

public enum Status {
	URL_INVALID {
		@Override
		public String getErrorMsg() {
			return "Provided URL is Invalid";
		}
	},
	URL_HP {
		@Override
		public String getErrorMsg() {
			return "Provided URL might be Home Page";
		}
	},
	NET_ERR {
		@Override
		public String getErrorMsg() {
			return "Unable to send Get Request. Network Error?";
		}
	},
	PARSE_EXCEPTION {
		@Override
		public String getErrorMsg() {
			return "Unable to Parse Couch Doc";
		}
	},
	STATIC_FILE {
		@Override
		public String getErrorMsg() {
			return "Is it an static file?";
		}
	};

	public abstract String getErrorMsg();
}