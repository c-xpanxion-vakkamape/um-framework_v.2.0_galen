package um.testng.test.utilities.framework;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailHelper {

	public static void sendMail(MailComposer composedMail) {
		// TODO Auto-generated method stub
		Properties props = new Properties();
		props.put("mail.smtp.auth", composedMail.getBolAuth());
		props.put("mail.smtp.host", composedMail.getsHost());
		props.put("mail.smtp.port", composedMail.getsPort());

		Session session = Session.getInstance(props);

		try {

			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(composedMail.getsFrom()));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(composedMail.getsTo()));
			message.setSubject(composedMail.getsSubject());
			// message.setText(composedMail.getsBody());

			// ///////////////////////////////////////////////

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(composedMail.getsBody(), "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// adds attachments

			String filePath = composedMail.getsAttachmentPath();
			if (!filePath.isEmpty()) {
				MimeBodyPart attachPart = new MimeBodyPart();
				try {
					attachPart.attachFile(filePath);
					// attachPart.setText(text);
//					attachPart.setFileName("test");
				} catch (IOException ex) {
					ex.printStackTrace();
				}

				multipart.addBodyPart(attachPart);
			}

			String fileContents = composedMail.getsAttachmentText();
			if (!fileContents.isEmpty()) {
				String fileName = composedMail.getsAttachmentTextName();
				if (fileName.isEmpty()) {
					fileName = "Report_" + getTS() + ".html";
				}
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.setText(fileContents);
				attachPart.setFileName(fileName);
				multipart.addBodyPart(attachPart);
			}

			// sets the multi-part as e-mail's content
			message.setContent(multipart);
			// /////////////////////////////////////////
			// message.setContent(composedMail.getsBody(), "text/html");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String getTS() {
		java.util.Date date = new java.util.Date();
		String ts = date.toString();
		ts = ts.replace(" ", "");
		ts = ts.replace(":", "");
		return ts;
	}

	public static void Send(String reportName, String attachmentPath, String attachmentText, String attachmentName) {
		MailComposer composedMail = new MailComposer();

		String mailContent = reportName;
		composedMail.setBolAuth(false);
		System.out.println("Inside send mail");
		composedMail.setsHost(ConfigProp.getPropertyValue("SMTPHost"));
		composedMail.setsPort(ConfigProp.getPropertyValue("SMTPPort"));
		String toMailIds = ConfigProp.getPropertyValue("ToMail").replace(";", ",");
		composedMail.setsTo(toMailIds);
		composedMail.setsFrom(ConfigProp.getPropertyValue("FromMail"));
		composedMail.setsSubject(ConfigProp.getPropertyValue("MailSubj"));
		composedMail.setsAttachmentPath(attachmentPath);
		composedMail.setsAttachmentText(attachmentText);
		composedMail.setsAttachmentTextName(attachmentName);
		composedMail.setsBody(mailContent);
		// System.out.println("Sending Mail");
		sendMail(composedMail);
		// System.out.println("Sent Mail");
	}
}
