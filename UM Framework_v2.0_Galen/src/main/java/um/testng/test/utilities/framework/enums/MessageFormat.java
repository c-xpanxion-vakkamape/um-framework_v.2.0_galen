package um.testng.test.utilities.framework.enums;

public enum MessageFormat {
HTML("html"),TEXT("text");
private String value;
private MessageFormat(String value){
	this.value = value;
}
public String getValue(){
	return this.value;
}
}
