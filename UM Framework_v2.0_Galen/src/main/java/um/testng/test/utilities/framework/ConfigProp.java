package um.testng.test.utilities.framework;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProp {
	public static Properties prop = new Properties();
	public static String filepath;
//	static {
//		try {
//			filepath = System.getProperty("ResFolder");
//			filepath = "resources";
//			if(filepath==null){
//				filepath = "Config.properties";
//				try(InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filepath)) {
//					prop.load(resourceStream);
//				}
//			}else{
//				System.out.println(filepath);
//				filepath = filepath + "/" + "Config.properties";
//				prop.load(new FileInputStream(filepath));
//			}
//			} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	static {
		try {
			filepath = System.getProperty("ResFolder");
			String[] serPath = {"resources", "resourcesIE"};
			for(int i=0; i<serPath.length; i++){
			
			filepath = serPath[i];
			if(filepath==null){
				filepath = "Config.properties";
				try(InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filepath)) {
					prop.load(resourceStream);
				}
			}else{
				System.out.println(filepath);
				filepath = filepath + "/" + "Config.properties";
				prop.load(new FileInputStream(filepath));
			}
			
		}
			} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getPropertyValue(String key) {
		return prop.getProperty(key);
	}
	
}
