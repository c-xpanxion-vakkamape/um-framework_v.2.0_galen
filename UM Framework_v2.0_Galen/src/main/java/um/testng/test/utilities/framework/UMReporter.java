package um.testng.test.utilities.framework;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class UMReporter   {	
	private static ExtentReports extentD;
	private static ExtentReports extentDwss;
	private static ExtentReports extentM;
	private static ExtentReports extentMwss;

	public ExtentTest testParent;
	public ExtentTest testParentWss;
	public static String reportPath;
	public static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	public static ThreadLocal<ExtentTest> testwss = new ThreadLocal<ExtentTest>();
	public static ThreadLocal<Boolean> isPassed = new ThreadLocal<Boolean>();
	
	public static int passCountD = 0;
	public static int failCountD = 0;
	public static int passCountM = 0;
	public static int failCountM = 0;
	
	public static Map<String,UMReporter> classReportMap = new HashMap<String,UMReporter>();
	
	public static void initReport(String suiteName) {
		String path = ConfigProp.getPropertyValue("SSPath");
		String fileName = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
		String history_Prerequisite = ConfigProp.getPropertyValue("History_Prerequisite");
		File reportConfig;
		if(history_Prerequisite.equalsIgnoreCase("Yes")){
			reportConfig = new File("./report_config_with_history.xml");
		}else{
			reportConfig = new File("./reportConfig.xml");
		}
		
//		new File(path + "Report_" + fileName).mkdirs(); 
		new File(path + "Report_").mkdirs(); //========= this is new - above is old
//		{
//		reportPath = path + "Report_" + fileName;
		reportPath = path + "Report_";//========= this is new - above is old
		System.out.println("The report path is ------ "+reportPath);		

		System.out.println("Into Init Report");
		String css = ".test-node-name .categoryDriver {background-color: #22b5e1; border-radius: 2px; color: #fff; font-size: 12px; margin-right: 3px; padding: 2px 4px; font-family: Roboto, Nunito, 'Source Sans Pro', Arial;font-weight: 400;line-height: 1.5;}"
				+ ".test-node-name .categoryDevice {background-color: #22b5e1; border-radius: 2px; color: #fff; font-size: 12px; margin-right: 3px; padding: 2px 4px; font-family: Roboto, Nunito, 'Source Sans Pro', Arial;font-weight: 400;line-height: 1.5;}"
				+ ".test-node-name .categoryBrowser {background-color: #22b5e1; border-radius: 2px; color: #fff; font-size: 12px; margin-right: 3px; padding: 2px 4px; font-family: Roboto, Nunito, 'Source Sans Pro', Arial;font-weight: 400;line-height: 1.5;}";
		// if (extent == null || extentwss==null) {
//		System.out.println("=======================================================================" + reportPath);
		if (suiteName.equalsIgnoreCase("Desktop")) {
			// if (extent == null){
			
//			extentD = new ExtentReports(reportPath + "/UM_Execution_Result_Desktop.htm", true);
//			extentDwss = new ExtentReports(reportPath + "/UM_Execution_Resultwss_Desktop.htm", true);
//			
//			extentD.loadConfig(reportConfig);
//			extentDwss.loadConfig(reportConfig);
			
			
			// if (extent == null){

			extentD = new ExtentReports(reportPath + "/UM_Execution_Result_Desktop.htm", true);
			extentDwss = new ExtentReports(reportPath + "/UM_Execution_Resultwss_Desktop.htm", true);
			
			  extentD.config().documentTitle("UM Hybrid Framework").reportName("Test Results Desktop")
              .reportHeadline("- UM Framework");
			  extentD.config().insertCustomStyles(css);
			  
			   extentDwss.config().documentTitle("UM Hybrid Framework with Screenshot")
               .reportName("Test Results Desktop").reportHeadline("-UM Framework");
			   extentDwss.config().insertCustomStyles(css);
			
			
			}
		if (!suiteName.equalsIgnoreCase("Desktop")) {
//			extentM = new ExtentReports(reportPath + "/UM_Execution_Result_Mobile.htm", true);
//			extentMwss = new ExtentReports(reportPath + "/UM_Execution_Resultwss_Mobile.htm", true);
//			extentM.loadConfig(reportConfig);
//			extentMwss.loadConfig(reportConfig);
			
			
			
			extentM = new ExtentReports(reportPath + "/UM_Execution_Result_Mobile.htm", true);
			extentMwss = new ExtentReports(reportPath + "/UM_Execution_Resultwss_Mobile.htm", true);
			//extentM.loadConfig(reportConfig);
			//extentMwss.loadConfig(reportConfig);
			
			 extentM.config().documentTitle("UM Hybrid Framework").reportName("Test Results Mobile")
             .reportHeadline("- UM Framework");
			 extentM.config().insertCustomStyles(css);
			 
			   extentMwss.config().documentTitle("UM Hybrid Framework with Screenshot")
               .reportName("Test Results Mobile").reportHeadline("-UM Framework");
			   extentMwss.config().insertCustomStyles(css);
			
			
		}
	}

	public UMReporter(String testName, String description, String suiteName) {
		initParent(testName, description, suiteName);		
	}

	public static void log(LogStatus logStatus, String stepName) {
		test.get().log(logStatus, stepName);
		String path = "";
		switch (logStatus) {
		case PASS:
			String SSValue = ConfigProvider.getConfig("Screenshot for Pass");
			if(SSValue.equalsIgnoreCase("YES")){
				path = WrapperMethods.captureScreen();
				testwss.get().log(logStatus, stepName + testwss.get().addScreenCapture(path));
			}else{
				testwss.get().log(logStatus, stepName);
			}
			break;
		case FAIL:
			path = WrapperMethods.captureScreen();
			testwss.get().log(logStatus, stepName + testwss.get().addScreenCapture(path));
			isPassed.set(false);
//			Assert.fail();
			break;
		case INFO:
		case SKIP:
		case WARNING:
			testwss.get().log(logStatus, stepName);
			break;
		case ERROR:
		case FATAL:
		case UNKNOWN:
			testwss.get().log(logStatus, stepName);
			isPassed.set(false);
			System.out.println(isPassed.get());
			Assert.fail();
			break;
		default:			
			break;
		}
		System.out.println(isPassed.get());
	}
	
	public void initParent(String testName, String description, String suiteName) {
		if ("Desktop".equalsIgnoreCase(suiteName)) {
			testParent = extentD.startTest(testName, description);
			testParentWss = extentDwss.startTest(testName, description);
		} else {
			testParent = extentM.startTest(testName, description);
			testParentWss = extentMwss.startTest(testName, description);

		}
	}

	public static void initTest(String testName,String suiteName) {
		String browser = ConfigProvider.getConfig("Browser");
		String os = ConfigProvider.getConfig("OS");
		String environment = System.getProperty("environment");
		String hw = null;
		if(ConfigProvider.getConfig("Res_Height")!=null ||ConfigProvider.getConfig("Res_Height").isEmpty())
			hw = ConfigProvider.getConfig("Res_Width")+"*"+ ConfigProvider.getConfig("Res_Height");
		isPassed.set(true);
		testName = testName + /*" <span class='categoryOS'>" + os + "</span> <span class='categoryBrowser '>" + browser + "</span> <span class='categoryEnvironment '>" + environment + "</span>";*/
				" <span class='categoryOS'>" + os + "</span>"+
				" <span class='categoryBrowser '>"+ browser + "</span>"+
				" <span class='categoryEnvironment '>"+ environment + "</span>"+
				" <span class='categoryheightwidth '>"+ hw + "</span>";
		if (!"Desktop".equalsIgnoreCase(suiteName)) {			
			String driver = ConfigProvider.getConfig("Driver").toUpperCase();
			testName = testName + " <span class='categoryDriver'>" + driver + "</span>";
			String deviceId = ConfigProvider.getConfig("Device_ID").toUpperCase();
			String device = "";
			String[] app_dev = ConfigProp.getPropertyValue("Devices").split(",");
			for (String dev : app_dev){
				String[] devport=dev.split("~");
				//System.out.println("Device Name test" + devport[1]);
				if(devport[0].equalsIgnoreCase(deviceId)){
//					System.out.println("Device Name" + devport[1]);
					device = devport[1];
				}
			}
			testName = testName + " <span class='categoryDriver'>" + device + "</span>";			
		}
		test.set(getextent().startTest(testName));
		testwss.set(getextentwss().startTest(testName));

	}

	public static void assignCatogory(String Catogory) {
		test.get().assignCategory(Catogory);
		testwss.get().assignCategory(Catogory);
	}

	public void endParent(String suiteName) {
		if ("Desktop".equalsIgnoreCase(suiteName)) {
			extentD.endTest(testParent);
			extentDwss.endTest(testParentWss);
		} else {
			extentM.endTest(testParent);
			extentMwss.endTest(testParentWss);

		}
	}

	public static void endReport(String suiteName) {
		if ("Desktop".equalsIgnoreCase(suiteName)) {
			extentD.flush();
			extentDwss.flush();
			extentD.close();
			extentDwss.close();
		} else {
			extentM.flush();
			extentMwss.flush();
			extentM.close();
			extentMwss.close();

		}
	}

	private static ExtentReports getextent() {
		switch (ConfigProvider.getConfig("Platform").toUpperCase()) {
		case "DESKTOP":
			return extentD;
		default:
			return extentM;
		}
	}

	private static ExtentReports getextentwss() {
		switch (ConfigProvider.getConfig("Platform").toUpperCase()) {
		case "DESKTOP":
			return extentDwss;
		default:
			return extentMwss;
		}
	}

	public void appendParent() {
		testParent.appendChild(test.get());
		testParentWss.appendChild(testwss.get());
//		System.out.println(isPassed.get());
		if(ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")){
			if(isPassed.get()){
				passCountD = passCountD + 1;
			}else{
				failCountD = failCountD + 1;
			}	
		}else{
		if(isPassed.get()){
			passCountM = passCountM + 1;
		}else{
			failCountM = failCountM + 1;
		}	}
		test.remove();
		testwss.remove();
		isPassed.remove();

	}

}
