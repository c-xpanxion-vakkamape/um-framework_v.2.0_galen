package um.testng.test.utilities.framework;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class XmlGenerator implements ExcelScanner {
	private static Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>> testsComb = new HashMap<String, Map<String, Map<String, ArrayList<Map<String, String>>>>>();
	private static XmlSuite suiteD, suiteM;

	private static XmlTest test;

	private static ArrayList<String> classesList = new ArrayList<>();

	private static XmlClass xmlClaz;
	private static List<XmlInclude> methodIncludes = new ArrayList<XmlInclude>();
	private static List<XmlClass> xmlClassesList = new ArrayList<XmlClass>();

	private static String basepkg = "um.testng.test.alltestpack";

	public static void main(String[] args) {
		xmlInit();
		ExcelReader xlReader = new ExcelReader(new XmlGenerator());
		xlReader.scanExcel();
		writeTestComb();
		suiteGen();
		String xmlD = getXMLD();
		writeToXMLD(xmlD);
		String xmlM = getXMLM();
		writeToXMLM(xmlM);
	}

	private static void xmlInit() {
		suiteD = new XmlSuite();
		suiteD.setParallel(XmlSuite.PARALLEL_TESTS);
		suiteD.setThreadCount(12);
		suiteD.setDataProviderThreadCount(12);

		suiteM = new XmlSuite();
		suiteM.setParallel(XmlSuite.PARALLEL_TESTS);
		suiteM.setThreadCount(12);
		suiteM.setDataProviderThreadCount(1);
	}

	private static void suiteGen() {
		for (Entry<String, Map<String, Map<String, ArrayList<Map<String, String>>>>> tests : testsComb.entrySet()) {
			String testName = tests.getKey();
			if (testName.equalsIgnoreCase("Browser")) {
				test = new XmlTest(suiteD);
				test.setName(testName);
				test.setThreadCount(12);
			} else {
				test = new XmlTest(suiteM);
				test.setName(testName);
				test.setThreadCount(1);
			}
			test.setParallel(XmlSuite.PARALLEL_METHODS);
			xmlClassesList = new ArrayList<XmlClass>();
			for (Entry<String, Map<String, ArrayList<Map<String, String>>>> classes : tests.getValue().entrySet()) {
				String className = classes.getKey();
				xmlClaz = new XmlClass(basepkg + "." + className);
				for (Entry<String, ArrayList<Map<String, String>>> methods : classes.getValue().entrySet()) {
					methodIncludes = xmlClaz.getIncludedMethods();
					String methodName = methods.getKey();
					methodIncludes.add(new XmlInclude(methodName));
				}
				xmlClassesList.add(xmlClaz);
			}
			test.setXmlClasses(xmlClassesList);
		}

	}
	
//	private static void writeTestComb(){
//		String serPath = "resources/TestComb.json";
//		FileOutputStream fileOut;
//		try {
//			fileOut = new FileOutputStream(serPath);
//		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
//		         out.writeObject(testsComb);
//		         out.close();
//		         fileOut.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	private static void writeTestComb(){
		String[] serPath = {"resources/TestComb.json", "resourcesIE/TestComb.json"};
		FileOutputStream fileOut;
		try {
			for(int i=0; i<serPath.length; i++){
			fileOut = new FileOutputStream(serPath[i]);
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(testsComb);
		         out.close();
		         fileOut.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	private static String getXMLD() {
		return suiteD.toXml();
	}

	private static String getXMLM() {
		return suiteM.toXml();
	}

	private static void writeToXMLD(String xml) {
		// System.out.println(xml);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("TestNgD.xml"), "utf-8"))) {
			writer.write(xml);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeToXMLM(String xml) {
		// System.out.println(xml);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("TestNgM.xml"), "utf-8"))) {
			writer.write(xml);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void assignSuiteName(String suiteName) {
		try{
			suiteD.setName("Desktop");
			suiteM.setName("Mobile");
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void assignGroupIncludes(String groupName) {
		test.addIncludedGroup(groupName);
	}

	@Override
	public void assignGroupExcludes(String groupName) {
		test.addExcludedGroup(groupName);

	}

	@Override
	public void addToClasses(String className) {
		classesList.add(className);
	}

	@Override
	public boolean checkClassList(String className) {
		return classesList.contains(className);
	}

	@Override
	public void setTestCombo(Map<String, Map<String, Map<String, ArrayList<Map<String, String>>>>> testsComb) {
		XmlGenerator.testsComb = testsComb;
	}

}
