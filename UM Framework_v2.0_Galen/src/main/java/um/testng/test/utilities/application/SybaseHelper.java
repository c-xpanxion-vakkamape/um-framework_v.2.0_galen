package um.testng.test.utilities.application;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import um.testng.test.utilities.framework.HTTPHelper;

public class SybaseHelper {

	public static ArrayList<Map<String, String>> getDataList(String queryType,
			String[] queryParams) {
		ArrayList<Map<String, String>> retData = null;
		String query = QueryRepo.getQuery(queryType,queryParams);
		if(query != null){
			String endPoint = "http://subscriberm1prod2.turner.com:8996/jsp/runQuery.jsp";
			try {
				query =  URLEncoder.encode(query,"UTF-8");				
	            String urlParameters = "query=" + query + "&submit=runQuery";
	            System.out.println(urlParameters);
				String xmlData = HTTPHelper.sendPOST(endPoint, urlParameters);
				System.out.println(xmlData);
				if(!((xmlData == "") || (xmlData == "timeout") || xmlData.contains("<root></root>") || xmlData.contains("<root />"))){
					retData = getDataList(xmlData);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return retData;

	}
	
	private static ArrayList<Map<String, String>> getDataList(String xmlStr){
		ArrayList<Map<String, String>> retData = new ArrayList<Map<String,String>>();
		Map <String, String> attrMap = null;
		int rootPos = xmlStr.indexOf("<root>");
		xmlStr = xmlStr.substring(rootPos);
		xmlStr = xmlStr.replace("&","&amp;");
		DocumentBuilder db;
		try {
			db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(xmlStr)));
			doc.getDocumentElement().normalize();
			 NodeList nodeList = doc.getElementsByTagName("child");
			 System.out.println("count 1" + nodeList.getLength());
			 for (int count = 0; count < nodeList.getLength(); count++) {
				 
					Node tempNode = nodeList.item(count);
				 
					// make sure it's element node.
					if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				 
			 
						if (tempNode.hasAttributes()) {
							attrMap = new HashMap<String,String>();
				 
							// get attributes names and values
							NamedNodeMap nodeMap = tempNode.getAttributes();
							System.out.println("count 2" + nodeMap.getLength());
							for (int i = 0; i < nodeMap.getLength(); i++) {
				 
								Node node = nodeMap.item(i);
								attrMap.put(node.getNodeName(), node.getNodeValue());
								System.out.println("attr name : " + node.getNodeName());
								System.out.println("attr value : " + node.getNodeValue());
				 
							}
							retData.add(attrMap);
				 
						}
					}
			 }
			 
			 
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retData;
	}

}
