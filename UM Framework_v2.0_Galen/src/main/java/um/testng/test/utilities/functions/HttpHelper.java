package um.testng.test.utilities.functions;


import static com.jayway.restassured.RestAssured.given;

import java.io.File;
import java.util.Map;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.MultiPartConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.utilities.framework.UMReporter;


public class HttpHelper {
	public static Response postRequest(String endPoint,ContentType contentType,String Auth,String content){
		Response response = null;
		try{
			response = given().
					contentType(contentType).
					header("Authorization", Auth).
				    body(content).
				when().
				    post(endPoint).
				then().
				extract().
			        response();	
			long time =  given().
					contentType(contentType).
					header("Authorization", Auth).
				    body(content).
				when().
				    post(endPoint).time();
			UMReporter.log(LogStatus.PASS,"The response time for the post request "+endPoint+" is -- "+time +" milliseconds");
			}catch(Exception e){
				UMReporter.log(LogStatus.FAIL,"Due to "+e.getMessage()+", the scenario got failed.");
	//			ReportHelper.logFail(""+e.printStackTrace());
		}
		return response;
	}
	
	public static Response getRequest(String endpoint,ContentType contentType,String Auth){
		Response response = null;
		try{
			response = given().
					contentType(contentType).
//					contentType(ContentType.)
					header("Authorization", Auth).	
						when().
								get(endpoint).
						then().								
						extract().
							response();
			long time =  given().
					contentType(contentType).
					header("Authorization", Auth).	
						when().
								get(endpoint).time();
			UMReporter.log(LogStatus.PASS,"The response time for the get request "+endpoint+" is -- "+time +" milliseconds");
			}catch(Exception e){
				/*StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionAsString = sw.toString();*/
				UMReporter.log(LogStatus.FAIL,"Due to "+e.getMessage()+", the scenario got failed.");
		}
		return response;
	}
	
	public static Response deleteRequest(String endPoint,ContentType contentType,String Auth,String content){
		Response response = null;
		try{
		 response = given().
								contentType(contentType).
								header("Authorization", Auth).
								body(content).
							when().
								delete(endPoint).
							then().								
							extract().
								response();
		 long time = given().
					contentType(contentType).
					header("Authorization", Auth).
					body(content).
				when().
					delete(endPoint).time();
		 UMReporter.log(LogStatus.PASS,"The response time for the delete request "+endPoint+" is -- "+time +" milliseconds");
			}catch(Exception e){
				UMReporter.log(LogStatus.FAIL,"Due to "+e.getMessage()+", the scenario got failed.");
		}
		return response;
	}
	
	public static Response sendRequest(String endPoint,ContentType contentType,String Auth){
		Response response = null;
		try{
		 response = given().
								contentType(contentType).
								header("Authorization", Auth).
							when().
							    post(endPoint).
							then().
							extract().
						        response();	
			}catch(Exception e){
				UMReporter.log(LogStatus.FAIL,"Due to "+e.getMessage()+", the scenario got failed.");
		}
		return response;
	}
	
	
	public static String sendPOST(String endPoint, Map<String, String> headers,
			String msgBody) {
		Response response = null;
		try{
//			String pathFile = PropertyHelper.getProp("ReportPath");
			System.out.println(msgBody);
			RestAssured.config = RestAssured.config().multiPartConfig(MultiPartConfig.multiPartConfig().defaultSubtype("related"));
			response = given().
									header("Authorization",headers.get("Authorization")).
//									contentType(headers.get("Content-Type")).								
//									body(msgBody).
									multiPart(new File(msgBody)).
								when().
									post(endPoint).
								then().
									extract(). 
									response();	
		}catch(Exception e){
			UMReporter.log(LogStatus.FAIL,"Due to "+e.getMessage()+", the scenario got failed.");
		}					
		return response.asString();
	}
	
}
