package um.testng.test.utilities.application;

public class QueryRepo {

	public static String getQuery(String queryType, String[] queryParams) {
		String retQuery = "";
		switch (queryType.toUpperCase()) {
		case "SCORESANDSCHEDULE":
			if(queryParams.length == 1){
				String strDate = queryParams[0];			
			retQuery = "" + 
					"SELECT distinct(gbi.broadcaster_id),g.game_id,gsl.period AS quarters, " +
						"convert(char(8), g.date_est, 112) as game_date, " +
						"convert(char(8),g.date_est,112)|| ' ' ||convert(char(5), isnull(g.time_est,'1900-01-01 00:00:00'), 18) AS game_date_time_est, " +				
						"rtrim(ht.team_city) AS h_team_city,rtrim(vt.team_city) AS v_team_city, " +
						"rtrim(ht.team_name) AS h_team_name,rtrim(vt.team_name) AS v_team_name, " +
			       		"rtrim(ht.abbreviation) AS h_abrv,rtrim(vt.abbreviation) AS v_abrv, " +
			  			"(SELECT CASE WHEN b.broadcaster_id = 173 THEN 'can' ELSE b.scope_id END " +
			  				"FROM [db_NBA_LiveStats].dbo.broadcaster b WHERE b.broadcaster_id = gbi.broadcaster_id) AS broadcaster_scope, " +
			       		"rtrim(gbi.broadcast_type_code) AS broadcast_type_code,  " +
			  			"(SELECT b.regional_display FROM [db_NBA_LiveStats].dbo.broadcaster b " +
			  				"WHERE b.broadcaster_id = gbi.broadcaster_id) AS broadcaster_display, " +
			       		"rtrim(gsl.game_status_text) AS status_text, gsl.game_Status as status_number, rtrim(gbi.media_type_code) AS media_type_code, " +
				   		"gsl.Visitor_Team_Pts AS Visitor_Score, gsl.Home_Team_Pts AS Home_Score " +
					"FROM [db_NBA_LiveStats].dbo.game g, [db_NBA_LiveStats].dbo.game_stats_live gsl, " +
			     		"[db_NBA_LiveStats].dbo.game_broadcast_info gbi, [db_nba_livestats].dbo.team_map ht,  " +
			     		"[db_nba_livestats].dbo.team_map vt " +
					"WHERE g.league_id in ('00','14','15','16') " +
						"AND g.game_id *= gsl.game_id " +
						"AND gbi.broadcaster_id != 0 " +
						"AND convert(char(8), g.date_est, 112) in ('" + strDate + "') " +
						"AND g.game_id *= gbi.game_id " +
						"AND g.visitor_team_id = vt.team_id " +
						"AND g.home_team_id = ht.team_id " +
						"AND gbi.broadcast_type_code = 'natl' " +
						"AND gbi.media_type_code = 'tv' " +
					"ORDER BY game_date_time_est, g.game_id";
			}else{
				retQuery = null;
			}
			break;

		default:
			break;
		}
		System.out.println(retQuery);
		return retQuery;
	}

}
