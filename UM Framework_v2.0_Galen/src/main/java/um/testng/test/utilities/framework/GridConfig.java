package um.testng.test.utilities.framework;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;

public class GridConfig {

//	public static void launchNodes() throws IOException {
//		System.out.println("Inside launch nodes");
//		ArrayList<Map<String, String>> test = ExcelReader.getTestDataIterator(
//				"GridConfig", "UM_Scenarios_Sheet");
//		for (Map<String, String> t : test) {
//			String port = t.get("port");
//			String bport = t.get("bport");
//			String browserName= t.get("BrowserName");
//			String platformName= t.get("PlatformName");
//			String deviceName = t.get("DeviceName");
//			String chromeDriverPort = t.get("ChromeDriverPort");
//			String deviceID = ConfigProp.getPropertyValue(deviceName);
//			System.out.println(port+"----"+bport+"----"+deviceID);
//			String Appium_HostName = ConfigProp.getPropertyValue("Appium_HostName");
//			String Appium_Grid_Port = ConfigProp.getPropertyValue("Appium_Grid_Port");
//			String Appium_Grid_HostName = ConfigProp.getPropertyValue("Appium_Grid_HostName");
//			String baseDir = System.getProperty("user.dir");
//			HashMap<String, String> hm = new HashMap<>();
//			hm.put("DeviceID", deviceID);
//			hm.put("Appium_HostName", Appium_HostName);
//			hm.put("AppiumPort", port);
//			hm.put("Appium_Grid_Port", Appium_Grid_Port);
//			hm.put("Appium_Grid_HostName", Appium_Grid_HostName);
//			hm.put("BrowserName", browserName);
//			hm.put("PlatformName", platformName);
////			hm.put("ChromeDriverPort", chromeDriverPort);
//			
//			File jsonFile = new File(baseDir+"\\resources\\GridConfig\\"+port+".json");
//			if(jsonFile.exists()){
//				jsonFile.delete();
//			}
//			ReadAndReplace.copyFileUsingJava7Files(baseDir+"/resources/GridConfig/template.json", baseDir+"\\resources\\GridConfig\\"+port+".json");
//			ReadAndReplace.replace(hm, baseDir+"\\resources\\GridConfig\\"+port+".json", baseDir+"\\resources\\GridConfig\\"+port+"_temp.json");
//			
//			String jsonPath ="\""+baseDir+"\\resources\\GridConfig\\"+port+".json\"";
//			String logPath ="\""+baseDir+"\\resources\\GridConfig\\Logs\\"+port+".txt\"";
//			String appiumJS = "\""+System.getenv("APPIUM_HOME")+"\\appium.js\"";
//			String node="\""+System.getenv("NODE")+"\\node.exe\"";
//			System.out.println(appiumJS);
//			System.out.println(node);		
//
//			String reset = null;
//			if(ConfigProp.getPropertyValue("Reset").equalsIgnoreCase("yes"))
//				reset ="--full-reset";
//			else
//				reset = "--no-reset";
//		
//			String cmdVal = "node "+appiumJS+" --nodeconfig "+jsonPath+" --log-timestamp --log "+logPath+" "+reset;
//			System.out.println(cmdVal);
//			CommandLine command = new CommandLine("cmd");
//			command.addArgument("/c");
//			command.addArgument("node");
//			command.addArgument(appiumJS);
//			command.addArgument("--port");
//			command.addArgument(port);
//			command.addArgument("--udid");
//			command.addArgument(deviceID);
//			command.addArgument("-bp");
//			command.addArgument(bport);
//			command.addArgument("--chromedriver-port");
//			command.addArgument(chromeDriverPort);
//			command.addArgument("--nodeconfig");
//			command.addArgument(jsonPath);
//			command.addArgument(reset);
//			command.addArgument("--log-timestamp");
//			command.addArgument("--log");
//			command.addArgument(logPath);
//			DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
//			DefaultExecutor executor = new DefaultExecutor();
//			executor.setExitValue(1);
//			try {
//				executor.execute(command, resultHandler);
//				System.out.println(" wiahws "+command.toString());
//				Thread.sleep(20000);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
	public static void launchNodes() throws IOException {
		System.out.println("Inside launch nodes");
		ArrayList<Map<String, String>> test = ExcelReader.getTestDataIterator(
				"GridConfig", "UM_Scenarios_Sheet");
		for (Map<String, String> t : test) {
			String port = t.get("port");
			String bport = t.get("bport");
			String browserName= t.get("BrowserName");
			String platformName= t.get("PlatformName");
			String deviceName = t.get("DeviceName");
			String chromeDriverPort = t.get("ChromeDriverPort");
			String deviceID = ConfigProp.getPropertyValue(deviceName);
			System.out.println(port+"----"+bport+"----"+deviceID);
			String Appium_HostName = ConfigProp.getPropertyValue("Appium_HostName");
			String Appium_Grid_Port = ConfigProp.getPropertyValue("Appium_Grid_Port");
			String Appium_Grid_HostName = ConfigProp.getPropertyValue("Appium_Grid_HostName");
			String baseDir = System.getProperty("user.dir");
			HashMap<String, String> hm = new HashMap<>();
			hm.put("DeviceID", deviceID);
			hm.put("Appium_HostName", Appium_HostName);
			hm.put("AppiumPort", port);
			hm.put("Appium_Grid_Port", Appium_Grid_Port);
			hm.put("Appium_Grid_HostName", Appium_Grid_HostName);
			hm.put("BrowserName", browserName);
			hm.put("PlatformName", platformName);
//			hm.put("ChromeDriverPort", chromeDriverPort);
			
			String[] serPath = {"resources", "resourcesIE"};
			
			for(int i=0 ;i<serPath.length; i++){
				File jsonFile = new File(baseDir+"\\"+serPath[i]+"\\GridConfig\\"+port+".json");
				if(jsonFile.exists()){
					jsonFile.delete();
				}
				ReadAndReplace.copyFileUsingJava7Files(baseDir+"/"+serPath[i]+"/GridConfig/template.json", baseDir+"\\"+serPath[i]+"\\GridConfig\\"+port+".json");
				ReadAndReplace.replace(hm, baseDir+"\\"+serPath[i]+"\\GridConfig\\"+port+".json", baseDir+"\\"+serPath[i]+"\\GridConfig\\"+port+"_temp.json");
				
				String jsonPath ="\""+baseDir+"\\"+serPath[i]+"\\GridConfig\\"+port+".json\"";
				String logPath ="\""+baseDir+"\\"+serPath[i]+"\\GridConfig\\Logs\\"+port+".txt\"";
			
			
			String appiumJS = "\""+System.getenv("APPIUM_HOME")+"\\appium.js\"";
			String node="\""+System.getenv("NODE")+"\\node.exe\"";
			System.out.println(appiumJS);
			System.out.println(node);		

			String reset = null;
			if(ConfigProp.getPropertyValue("Reset").equalsIgnoreCase("yes"))
				reset ="--full-reset";
			else
				reset = "--no-reset";
		
			String cmdVal = "node "+appiumJS+" --nodeconfig "+jsonPath+" --log-timestamp --log "+logPath+" "+reset;
			System.out.println(cmdVal);
			CommandLine command = new CommandLine("cmd");
			command.addArgument("/c");
			command.addArgument("node");
			command.addArgument(appiumJS);
			command.addArgument("--port");
			command.addArgument(port);
			command.addArgument("--udid");
			command.addArgument(deviceID);
			command.addArgument("-bp");
			command.addArgument(bport);
			command.addArgument("--chromedriver-port");
			command.addArgument(chromeDriverPort);
			command.addArgument("--nodeconfig");
			command.addArgument(jsonPath);
			command.addArgument(reset);
			command.addArgument("--log-timestamp");
			command.addArgument("--log");
			command.addArgument(logPath);
			DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
			DefaultExecutor executor = new DefaultExecutor();
			executor.setExitValue(1);
			
			try {
				executor.execute(command, resultHandler);
				System.out.println(" wiahws "+command.toString());
				Thread.sleep(20000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			}
		}
	}

	
	
	public static void main(String[] a) throws IOException{
		/*HashMap<String, String> hm = new HashMap<>();
		String port ="1112";
		hm.put("DeviceID", "5484865");
		hm.put("Appium_HostName", "45789632");
		hm.put("AppiumPort", port);
		hm.put("Appium_Grid_Port", "45:4545456");
		hm.put("Appium_Grid_HostName", "455454");
		System.out.println(System.getProperty("user.dir"));
		String baseDir = System.getProperty("user.dir");
		copyFileUsingJava7Files(baseDir+"/resources/GridConfig/template.json", baseDir+"\\resources\\GridConfig\\"+port+".json");
		replace(hm, baseDir+"\\resources\\GridConfig\\"+port+".json", baseDir+"\\resources\\GridConfig\\"+port+"_temp.json");*/
		
		System.out.println(System.getenv("APPIUM_HOME")+"\\appium.js");
	}
}
