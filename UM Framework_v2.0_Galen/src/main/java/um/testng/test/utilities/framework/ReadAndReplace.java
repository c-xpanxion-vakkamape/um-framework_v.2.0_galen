package um.testng.test.utilities.framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public class ReadAndReplace {

//	public static void main(String a[]) throws IOException{
	public static void updateXML(String jsonContents) throws IOException{
		
			File reportConfig = new File("./report_config.xml");
			if(reportConfig.exists()){
				reportConfig.delete();
				System.out.println("File deleted");
			}
			String jsonValue= jsonContents;
			copyFileUsingJava7Files("./report_config_with_history_base.xml", "./report_config_with_history.xml");
			replace(jsonValue,"./report_config_with_history.xml","./report_config_with_history_temp.xml","<JSONValue>");
	
	}
	
	public static void replace(String Environment,String oldFilePath,String newFilePath,String replaceText) throws FileNotFoundException{
		String oldFileName = oldFilePath;
		String tmpFileName = newFilePath;
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			br = new BufferedReader(new FileReader(oldFileName));
			bw = new BufferedWriter(new FileWriter(tmpFileName));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains(replaceText))
					line = line.replace(replaceText,Environment);
				bw.write(line+"\n");
			}
		} catch (Exception e) {
			return;
		} finally {
			try {
				if(br != null)
					br.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			try {
				if(bw != null)
					bw.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		File oldFile = new File(oldFileName);
		oldFile.delete();
		File newFile = new File(tmpFileName);
		newFile.renameTo(oldFile);
	}
	
	public static void replace(HashMap<String, String> userInputs, String oldFilePath, String tempFilePath) {
		BufferedReader br = null;
		BufferedWriter bw = null;
		try{
			br = new BufferedReader(new FileReader(oldFilePath));
			bw = new BufferedWriter(new FileWriter(tempFilePath));
			String line ;
			while ((line = br.readLine()) != null) {
				for(String string : userInputs.keySet()){
					if (line.contains(string))
						line = line.replace("<"+string+">",userInputs.get(string));
				}
				bw.write(line+"\n");
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(br != null)
					br.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			try {
				if(bw != null)
					bw.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		File oldFile = new File(oldFilePath);
		oldFile.delete();
		File newFile = new File(tempFilePath);
		newFile.renameTo(oldFile);
	}

	public static void copyFileUsingJava7Files(String source1, String dest1)
			throws IOException {
		File source= new File(source1);
		File dest = new File(dest1);
		Files.copy(source.toPath(), dest.toPath());
	}
}
