package um.testng.test.utilities.framework;

public class MailComposer {

	private String sHost,sPort,sFrom,sTo,sSubject,sBody,sAttachmentText,sAttachmentPath,sAttachmentTextName;
	private Boolean bolAuth;
	public String getsHost() {
		return sHost;
	}
	public void setsHost(String sHost) {
		this.sHost = sHost;
	}
	public String getsPort() {
		return sPort;
	}
	public void setsPort(String sPort) {
		this.sPort = sPort;
	}
	public String getsFrom() {
		return sFrom;
	}
	public void setsFrom(String sFrom) {
		this.sFrom = sFrom;
	}
	public String getsTo() {
		return sTo;
	}
	public void setsTo(String sTo) {
		this.sTo = sTo;
	}
	public String getsSubject() {
		return sSubject;
	}
	public void setsSubject(String sSubject) {
		this.sSubject = sSubject;
	}
	public String getsBody() {
		return sBody;
	}
	public void setsBody(String sBody) {
		this.sBody = sBody;
	}
	public Boolean getBolAuth() {
		return bolAuth;
	}
	public void setBolAuth(Boolean bolAuth) {
		this.bolAuth = bolAuth;
	}
	
	public String getsAttachmentPath(){
		if(sAttachmentPath!=null){
		return sAttachmentPath;
		}else{
			return "";
		}
	}
	
	public void setsAttachmentPath(String sAttachment){
		this.sAttachmentPath = sAttachment;
	}
	
	public String getsAttachmentText(){
		if(sAttachmentText!=null){
		return sAttachmentText;
		}else{
			return "";
		}
	}
	
	public void setsAttachmentText(String sAttachment){
		this.sAttachmentText = sAttachment;
	}

	public String getsAttachmentTextName(){
		if(sAttachmentTextName!=null){
		return sAttachmentTextName;
		}else{
			return "";
		}
	}
	
	public void setsAttachmentTextName(String sAttachment){
		this.sAttachmentTextName = sAttachment;
	}
}
