package um.testng.test.utilities.framework;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.google.common.base.CaseFormat;

public class SoftAssert {

	String errormessage = "";

	boolean errorFlag = false;

	WebDriver curDriver = null;
	int curWidth = 0;

	public SoftAssert(WebDriver driver) {
		errorFlag = false;
		errormessage = "";
		curDriver = driver;
	}

	public SoftAssert() {
		// This should only trigger on the initial valid checks!
		errorFlag = false;
		errormessage = "";
	}

	/**
	 * Used to trigger a failure at the end of a test method.
	 * 
	 * @return True if and only if there is an error
	 */
	public boolean hasError() {
		return errorFlag;
	}

	/**
	 * Returns the aggregated error messages for the test using this class.
	 * 
	 * @return All error messages for the current test
	 */
	public String getErrors() {
		if (curDriver == null) {
			return errormessage;
		} else {
			return errormessage + " (" + getCompName() + " - "
					+ getSizeAnalog(curWidth) + ")";
		}
	}

	public void assertTrue(boolean statement, String message) {
		String comments = "An Assertion Passed";
		try {
			Assert.assertTrue(statement, message);
			Reporter.log("<br>Passed Assertion " + comments + "<br>");
		} catch (AssertionError e) {
			errorFlag = true;
			Reporter.log("<br>Failed Assertion " + message + "<br>");
			addError(message);
		}
	}

	public void assertTrue(boolean statement, String errmessage,
			String passcomments) {
		try {
			Assert.assertTrue(statement, errmessage);
			MethodDef.passLog(passcomments);
		} catch (AssertionError e) {
			errorFlag = true;
			MethodDef.failLog(e.getMessage(), errmessage);
			addError(errmessage);
		}
	}

	public void assertFalse(boolean statement, String message) {
		try {
			Assert.assertFalse(statement, message);
		} catch (AssertionError e) {
			errorFlag = true;
			addError(message);
		}
	}

	public void assertTrue(boolean b) {
		assertTrue(b, "No error message defined");

	}

	public void assertNotNull(Object obj) {
		assertNotNull(obj, "No error message defined");
	}

	public void assertNotEquals(int size, int i) {
		try {
			Assert.assertNotEquals(size, i);
		} catch (AssertionError e) {
			errorFlag = true;
			addError(e.getMessage());
		}
	}

	// public void assertEquals(int width, int elementWidth) {
	// try{
	// Assert.assertEquals(width, elementWidth);
	// }
	// catch (AssertionError e){
	// if(!(Math.abs(width - elementWidth) <= Prefs.PIXEL_OFFSET)){
	// addError(e.getMessage());
	// errorFlag = true;
	// }
	// // Takes care of small browser discrepancies
	// // This can be removed if it is necessary to have exact sizes
	// }
	// }

	public void assertEquals(String lowerCase, String lowerCase2) {
		try {
			Assert.assertEquals(lowerCase, lowerCase2);
		} catch (AssertionError e) {
			errorFlag = true;
			addError(e.getMessage());
		}
	}

	public void assertNotNull(Object obj, String message) {
		try {
			Assert.assertNotNull(obj, message);
		} catch (AssertionError e) {
			errorFlag = true;
			addError(message);
		}
	}

	public void assertError(String string) {
		addError(string);
	}

	/**
	 * Adds error messages to the class as they occur.
	 * 
	 * @param toAdd
	 *            the exception thrown for the current test
	 * @throws IOException
	 */
	public void addError(String toAdd) {
		if (errormessage == "") {
			errormessage = toAdd;
		} else {
			errormessage += "; " + toAdd;
		}
		try {
			screenshot();
		} catch (Exception e) {
			Reporter.log("<BR> TEST <BR>" + e.getMessage());
		}
		errorFlag = true;
	}

	@SuppressWarnings("unchecked")
	private String getCompName() {
//		boolean arrayFlag = true;
//		Object osType = ((JavascriptExecutor) curDriver)
//				.executeScript("return navigator.userAgent;");
//		if (osType == null) {
//			return "No OS info returned!";
//		}
//		List<Object> array = null;
//		try {
//			array = (List<Object>) (osType);
//		} catch (Exception e) {
//			arrayFlag = false;
//		}
//		if (!arrayFlag) {
//			String readable = (String) (osType);
//			if (readable.contains("Windows NT 6.1")) {
//				return "Windows 7";
//			} else if (readable.contains("Windows NT 5.1")) {
//				return "Windows XP";
//			} else if (readable.contains("Windows NT 6.2")) {
//				return "Windows 8";
//			} else if (readable.contains("Macintosh")) {
//				return "Mac OS X 10.8.5";
//			}
//		} else {
//			String toreturn = "";
//			for (Object o : array) {
//				toreturn = toreturn + " " + (String) o;
//				if (((String) o).contains("Windows NT 6.2")) {
//					return "Windows 8";
//				} else if (((String) o).contains("Windows NT 6.1")) {
//					return "Windows 7";
//				} else if (((String) o).contains("Windows NT 5.1")) {
//					return "Windows XP";
//				} else if (((String) o).contains("Macintosh")) {
//					return "Mac OS X 10.8.5";
//				}
//			}
//			return toreturn;
//		}
//		return (String) (osType);
		return "UNKNOWN";
	}

	private String getSizeAnalog(int windowWidth) {
		int width = windowWidth;
		if (width < 500) {
			return "mobile resolution";
		} else if (width >= 500 && width < 1000) {
			return "Ipad (portrait) resolution";
		} else if (width >= 1000 && width < 1200) {
			return "Ipad (landscape) resolution";
		} else {
			return "desktop resolution";
		}
	}

	public void combineErrors(SoftAssert toCombine) {
		if (toCombine.hasError()) {
			this.assertError(toCombine.errormessage);
		}
	}

	public void screenshot() throws IOException {
		String sc_env1 = "";
		System.out.println("Screenshot is being taken due to an error... ");
		Date screentime = new java.sql.Timestamp(System.currentTimeMillis());
		DateFormat format = new SimpleDateFormat("YY.MM.d_hh.mm.ss a,z");
		System.getProperty("site").toLowerCase();
		String sc_env = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,
				System.getProperty("environment"));
		if (System.getProperty("environment").equals("PROD"))
			sc_env1 = "PROD";
		else
			sc_env1 = sc_env;
		String sc_site = System.getProperty("site");
		if (sc_site.equals("DOM"))
			sc_site = "Domestic";
		else
			sc_site = "International";
		String starttime_formatted = format.format(screentime);
		// path = "./target/screenshots/" + source.getName();

		// WebDriver augmentedDriver = new Augmenter().augment((WebDriver)
		// face().go());
		try {
			File scrFile = ((TakesScreenshot) curDriver)
					.getScreenshotAs(OutputType.FILE);
			String userDirectory = "./target/screenshots/" + scrFile.getName();
			// String failureImageFileName = Date.year() + Date.monthInYear() +
			// Date.dayInYear() + Date.hourInDay() + Date.minuteInHour() +
			// Date.secondInMinute() + "screenshot.png";
			char[] charArray = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
			String failureImageFileName = starttime_formatted + "_"
					+ RandomStringUtils.random(10, charArray)
					+ "_screenshot.png";
			FileUtils.copyFile(scrFile, new File("./target/screenshots/"
					+ scrFile.getName() + failureImageFileName));
			// Reporter.log("<font color=\"red\">><i>FAILURE SCREENSHOT "+
			// "<a href = \"http://mssqacoreqa01.turner.com:8080/view/"+sc_env1+"/job/"+System.getProperty("JOB_NAME")+"/ws/cnn-expanxion-automation/screenshots/"+failureImageFileName+"\""+" target=\"_blank\""+">"+"Screenshot"+"</a></i></font><br/>"
			// );
			Reporter.log("<font color=\"red\">><i>FAILURE SCREENSHOT "
					+ "<a href=" + "\"" + userDirectory + failureImageFileName
					+ "\"" + " target=\"_blank\"" + ">" + "Screenshot"
					+ "</a></i></font><br/>");
		} catch (Exception e) {
			Reporter.log("<br>Screenshot error" + e.getMessage() + "<br>");
		}
	}

}