package um.testng.test.utilities.framework.db;

import java.io.IOException;

import um.testng.test.utilities.framework.HTTPHelper;
import um.testng.test.utilities.framework.enums.Status;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.ParseException;

/**
 * Hello world!
 *
 */
public class CouchDocHelper {

	public static JSONObject fromURL(String pageURL) {

		String sURI, couchHost, docId;
		JSONObject couchDoc = new JSONObject();
		JSONObject docIdJ = new JSONObject();		
		couchHost = getCouchHost(pageURL);
		if ("Invalid".equalsIgnoreCase(couchHost)) {
			// System.out.println("Invalid URL. Terminating..." + "URL:" +
			// pageURL);
			couchDoc.put("Error", Status.URL_INVALID);
			return couchDoc;
		}
		sURI = getCouchURI(pageURL);
		sURI = sURI.trim();
		if (sURI.isEmpty() | "/".equals(sURI)) {
			if (pageURL.endsWith("nba.com") || pageURL.endsWith("nba.com/")) {
				couchDoc.put("Error", Status.URL_HP);
				return couchDoc;
			} else {
				couchDoc.put("Error", Status.URL_INVALID);
				return couchDoc;
				// System.out.println("URI from URL empty. Terminating..." +
				// "URL:" + pageURL);
			}
		}

		String endPoint = couchHost + "/external-content-db/_design/content/_view/by-uri?key=%22" + sURI + "%22";

		String response = "";
		try {
			response = HTTPHelper.sendGET(endPoint);

		} catch (IOException e) {
			// e.printStackTrace();
			couchDoc.put("Error", Status.NET_ERR);
			return couchDoc;

		}
		docIdJ = getDocId(response);
		if(docIdJ.containsKey("Error")){			
			couchDoc.put("Error", docIdJ.get("Error"));
		}else{
			docId = (String)docIdJ.get("Data");
			couchDoc = getDoc(couchHost, docId);
		}
		return couchDoc;
	}

	private static String getCouchHost(String pageURL) {
		String couchHost = "";

		if (pageURL.contains("dev.staging.nba.com")) {
			couchHost = "http://nba-dev-cms3-ext-proxy.56m.vgtf.net";
			// System.out.println("DEV Environment Detected.");
		} else if (pageURL.contains("ref.staging.nba.com")) {
			couchHost = "http://nba-ref-cms3-ext-proxy.56m.vgtf.net";
			// System.out.println("REF Environment Detected.");
		} else if (pageURL.contains("www.nba.com")) {
			couchHost = "http://nba-prod-cms3-ext-proxy.56m.vgtf.net";
			// System.out.println("PROD Environment Detected.");
		} else {
			couchHost = "Invalid";
			// System.out.println("Invalid Environment Detected.");
		}
		return couchHost;
	}

	private static String getCouchURI(String pageURL) {
		String sURI = "";
		String usageMsg = "";

		String mobileHash = (pageURL.split("#$").length > 1) ? pageURL.split("#$")[1].split("~")[0] : "";
		if (mobileHash.length() > 0) {
			sURI = mobileHash;
			// usageMsg = "Using mobile hash: ";
		} else {
			sURI = pageURL;
			// usageMsg = "Using supplied URL: ";
		}

		sURI = sURI.replace("//origin.", "//www.").replace("http://www.dev.staging.nba.com", "").replace("http://www.ref.staging.nba.com", "")
				.replace("http://www.nba.com", "").replace("http://nba.com", "").replace("/m/", "/").replace("index.json", "index.html").split("\\?")[0]
						.split("#")[0];

		System.out.println(usageMsg + sURI);
		return sURI;
	}

	private static JSONObject getDocId(String response) {
		JSONObject retObj = new JSONObject();
		response = response.trim();
		if (response.isEmpty()) {
			retObj.put("Error", Status.PARSE_EXCEPTION);
			return retObj;
		}
		String docId = "";
		JSONArray jsonArry = new JSONArray();
		JSONObject jsonObj;
		try {
			jsonObj = (JSONObject) JSONValue.parseWithException(response);
			if (jsonObj.containsKey("rows")) {
				jsonArry = (JSONArray) jsonObj.get("rows");
				if (jsonArry != null && jsonArry.size() > 0) {
					jsonObj = (JSONObject) jsonArry.get(jsonArry.size()-1);
					if (jsonObj.containsKey("id")) {
						docId = (String) jsonObj.get("id");
						retObj.put("Data", docId);
					} else {
						retObj.put("Error", Status.PARSE_EXCEPTION);
						// return retObj;
					}
				} else {
					retObj.put("Error", Status.STATIC_FILE);
					// return retObj;
				}
			} else {
				retObj.put("Error", Status.PARSE_EXCEPTION);
				// return retObj;
			}
		} catch (ParseException e) {
			retObj.put("Error", Status.PARSE_EXCEPTION);
			// return retObj;
			// e.printStackTrace();
		}
		return retObj;

	}

	private static JSONObject getDoc(String couchHost, String docId) {
		String endPoint = couchHost + "/external-content-db/" + docId;
		JSONObject retObj = new JSONObject();
		JSONObject jsonObj;
		String response;
		try {
			response = HTTPHelper.sendGET(endPoint);
			// System.out.println(response);
				jsonObj = (JSONObject) JSONValue.parseWithException(response);
				retObj.put("Data", jsonObj);
			} catch (ParseException e) {
				e.printStackTrace();
				retObj.put("Error", Status.PARSE_EXCEPTION);
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			retObj.put("Error", Status.NET_ERR);
		}
		return retObj;
	}
}
