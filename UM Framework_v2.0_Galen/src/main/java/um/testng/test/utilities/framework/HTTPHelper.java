package um.testng.test.utilities.framework;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.relevantcodes.extentreports.LogStatus;

public class HTTPHelper {
	private final static String USER_AGENT = "Mozilla/5.0";

	// HTTP GET request
	public static String sendGET(String endPoint) throws IOException {

		URL obj;
			obj = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");
		con.setConnectTimeout(5000);

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + endPoint);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}

		in.close();
		return response.toString();

	}
	
	//HTTP POST request
	public static String sendPOST(String endPoint,String urlParameters) throws IOException {

		URL obj;
			obj = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("POST");
		con.setConnectTimeout(5000);
		con.setDoOutput(true);

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		
		//send data
		try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
			 wr.writeBytes(urlParameters);
		     wr.flush();
		     wr.close();

			}

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + endPoint);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}

		in.close();
		return response.toString();

	}

	public static String sendPOST(String endPoint, Map<String,String> headers, String urlParameters) throws IOException {

		URL obj;
			obj = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("POST");
		con.setConnectTimeout(5000);
		con.setDoOutput(true);

		// add request header
//		con.setRequestProperty("User-Agent", USER_AGENT);
		
		for (Entry<String, String> header : headers.entrySet()) {
			con.setRequestProperty(header.getKey(),header.getValue());	
		}
		
		//send data
		try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
			 wr.writeBytes(urlParameters);
		     wr.flush();
		     wr.close();

			}

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + endPoint);
		System.out.println("Response Code : " + responseCode);

		String responseString = "";
		InputStream stream;
		if(responseCode == 200){
			stream = con.getInputStream();		
		}else{
			stream = con.getErrorStream();			
		}
		if(stream != null){
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		responseString = response.toString();
		}		
		con.disconnect();
		return responseString;

	}
	
	public static String multiPartPost(String endPoint, String contentType, Map<String, String> headers, String filePath, String msgText) throws ClientProtocolException, IOException{
		String responseString = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
//        HttpHost proxy = new HttpHost("127.0.0.1", 8888);
//        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
//        CloseableHttpClient httpclient = HttpClients.custom()
//                .setRoutePlanner(routePlanner)
//                .build();
        try {
            HttpPost httppost = new HttpPost(endPoint);
            String boundary = "-------------" + System.currentTimeMillis();
    		httppost.setHeader("Content-type", contentType + "; boundary=" + boundary);	
    		
    		for (Entry<String, String> header : headers.entrySet()) {
    			httppost.setHeader(header.getKey(),header.getValue());	
    		}
    		
            FileBody bin = new FileBody(new File(filePath));
            StringBody comment = new StringBody(msgText, ContentType.TEXT_PLAIN);

            HttpEntity reqEntity = MultipartEntityBuilder.create()
            		.setBoundary(boundary)
                    .addPart("metadata", comment)
            		.addPart("file", bin)
                    .build();


            httppost.setEntity(reqEntity);

        	
            System.out.println("executing request " + httppost.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {            	
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                	long length = resEntity.getContentLength();
                	System.out.println("Response content length: " + resEntity.getContentLength());
                	if(length != 0){                    
                    StringBuilder sb = new StringBuilder();
                    try {
                        BufferedReader reader = 
                               new BufferedReader(new InputStreamReader(resEntity.getContent()), 65728);
                        String line = null;

                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    }                    
                    catch (Exception e) { UMReporter.log(LogStatus.ERROR, "Error while posting data");		} 
                    responseString = sb.toString();

                    System.out.println("finalResult " + responseString);
                	}
                }
                EntityUtils.consume(resEntity);
//                responseString = resEntity.getContent().toString();
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }		
		return responseString;
	}
}
