package um.testng.test.utilities.functions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.json.JSONException;

public class JsonValidater {
	
	public static boolean isJSONValid(String test) {
		try{
			JSONParser parser = new JSONParser();
			parser.parse(test);
			return true;
		}catch(Exception e){
//			System.out.println(e.getMessage());
//			e.printStackTrace();
		}
		return false;
	}
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }

	  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
			System.out.println("---------------------------------2");

		  InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
			System.out.println("---------------------------------3");

	      JSONObject json = new JSONObject(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }

	  public static void main(String[] args) throws IOException, JSONException {
	    JSONObject json = readJsonFromUrl("https://graph.facebook.com/19292868552");
	    System.out.println(json.toString());
	    System.out.println(json.get("id"));
	  }


}
