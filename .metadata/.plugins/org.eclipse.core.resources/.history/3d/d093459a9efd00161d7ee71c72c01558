package um.testng.test.alltests;

import org.apache.poi.hslf.record.Environment;
import org.testng.ITestContext;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.alltestpack.EnvironmentHelper;
import um.testng.test.alltestpack.Prefs;
import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.functions.ArticlePageFunctions;
import um.testng.test.pom.functions.GalleryPageFunctions;
import um.testng.test.pom.functions.MoneyPageFunctions;
import um.testng.test.pom.functions.ProfilesPageFunctions;
import um.testng.test.pom.functions.ShowsPageFunctions;
import um.testng.test.pom.functions.VideoLandingPageFunctions;
import um.testng.test.pom.functions.VideoLeafPageFunctions;
import um.testng.test.pom.functions.sportsPageFunctions;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.TestDataProvider;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class BaseTestMethods {

	ArticlePageFunctions apf = new ArticlePageFunctions();
	GalleryPageFunctions gpf = new GalleryPageFunctions();
	MoneyPageFunctions mpf = new MoneyPageFunctions();
	VideoLandingPageFunctions vlpf = new VideoLandingPageFunctions();
	VideoLeafPageFunctions vpf = new VideoLeafPageFunctions();
	ProfilesPageFunctions ppf = new ProfilesPageFunctions();
	ShowsPageFunctions spf = new ShowsPageFunctions();
	sportsPageFunctions sprtpf = new sportsPageFunctions();

	public BaseTestMethods(boolean condi) {
		if (condi) {
			DriverFactory.driverInit();
			UMReporter.log(LogStatus.INFO,
					"OS-" + ConfigProvider.getConfig("OS") + "Browser-" + ConfigProvider.getConfig("Browser"));
		} else {
			UMReporter.log(LogStatus.INFO, "No browser interaction");
		}
	}

	public void testArticlePages(ITestContext ctx) {
		// Below code take the urls from excell
		// String secondUrl =TestDataProvider.getData("URLs");
		String url = EnvironmentHelper.getURL() + Prefs.ARTICLE_URL;
		WrapperMethods.enter_URL(EnvironmentHelper.getURL() + Prefs.ARTICLE_URL);
		WrapperMethods.checkallpageload();
		apf.testArticlePages(ctx);
	}

	public void testGalleryPages(ITestContext ctx) {
		if (ConfigProvider.getConfig("Environment").equalsIgnoreCase("REF")) {
			String url = EnvironmentHelper.getURL() + Prefs.GALLERY_URL;
			WrapperMethods.enter_URL(url);
			WrapperMethods.checkallpageload();
			gpf.testGalleryPage(ctx);
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");
		}
	}

	public void testMoneyPage(ITestContext ctx) {
		String url = Prefs.MONEY_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		mpf.testMoneyPage(ctx);
	}

	public void testVideos(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.videoLanding;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vlpf.testVideolandingpage(ctx);
	}

	public void testvideoleafpage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.videoUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vpf.testVideoleafgpage(ctx);
	}

	public void testarticleemvidpage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.articleEmVidUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		vpf.testarticleemvidpage(ctx);
	}

	public void testbrokenpage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.brokenUrl;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		apf.testbrokenpageurl(ctx);
	}

	public void testprofilepage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.profile_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		ppf.testprofilepage(ctx);
	}

	public void testshowpage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.shows_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		spf.testshowpage(ctx);
	}

	public void testspecialspage(ITestContext ctx) {
		String url = EnvironmentHelper.getURL() + Prefs.special_URL;
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		spf.testshowpage(ctx);
	}

	public void testsportpage(ITestContext ctx) {
		if (ConfigProvider.getConfig("Environment").equalsIgnoreCase("PROD")) {

			String url = Prefs.SPORTS_URL;
			WrapperMethods.enter_URL(url);
			WrapperMethods.checkallpageload();
			sprtpf.testsportpage(ctx);
		} else {
			UMReporter.log(LogStatus.SKIP,
					"Skipping this Test as this is not Applicable to this Environment or Browser or Device");

		}
	}

	public void testmorepage(ITestContext ctx) {
		
		
		String url = EnvironmentHelper.getURL() + TestDataProvider.getData("Data");
		WrapperMethods.enter_URL(url);
		WrapperMethods.checkallpageload();
		apf.testmorepage(ctx);
	}

}
