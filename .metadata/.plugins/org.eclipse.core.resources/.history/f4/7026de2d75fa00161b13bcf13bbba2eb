package um.testng.test.pom.functions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import um.testng.test.drivers.DriverFactory;
import um.testng.test.pom.elements.VideoLandingPage;
import um.testng.test.pom.elements.VideoLeafPage;
import um.testng.test.utilities.framework.ConfigProvider;
import um.testng.test.utilities.framework.MethodDef;
import um.testng.test.utilities.framework.UMReporter;
import um.testng.test.utilities.framework.WrapperMethods;

public class CommomPageFunctions {

	public static void testVideosDesktop() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 15);

		if (ConfigProvider.getConfig("Edition").equalsIgnoreCase("DOM")) {
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(
						By.cssSelector("div[class*='cn-featured']>div>div[class='owl-nav']>div[class='owl-next']")));
				WebElement lnknext = VideoLandingPage.getVideoCarouselNext();
				WebElement lnkprev = VideoLandingPage.getVideoCarouselPrev();
				if (lnknext.isDisplayed() == true && lnkprev.isDisplayed() == true) {
					try {
						if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")
								&& ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop"))
							actions.moveToElement(lnknext).click().perform();
						else
							lnknext.click();
						UMReporter.log(LogStatus.PASS, "Carousel links in Video Player is accessible");
					} catch (Exception e) {
						UMReporter.log(LogStatus.FAIL, "Carousel links in Video Player is not accessible");
					}
				} else {
					UMReporter.log(LogStatus.FAIL, "Carousel is not starting with first video in landing page");
				}
			} catch (Exception e) {
				UMReporter.log(LogStatus.FAIL, "Error in the Carousel elements in video landing page elements");
			}
		}
	}

	public static void testVideosSocialIcons() {

		try {
			UMReporter.log(LogStatus.INFO, "Testing social icons");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIcons(), "gigya social bar");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem1(), "gigya social element 1");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem2(), "gigya social element 2");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem3(), "gigya social element 3");
			WrapperMethods.verifyElement(VideoLandingPage.getVideoSocialIconsElem4(), "gigya social element 4");

			if (!ConfigProvider.getConfig("Browser").equalsIgnoreCase("SAFARI")) {
				WrapperMethods.click(VideoLandingPage.getVideoSocialIconsElem4(), "gigya share icon");
				MethodDef.explicitWaitVisibility(VideoLandingPage.getVideoShare(), "share block is visible after click"
						, "share block is not visible after click");
				WrapperMethods.verifyElement(VideoLandingPage.getVideoShare(), "share block");

				WrapperMethods.click(VideoLandingPage.getVideoShareClose(), "share block close");
				UMReporter.log(LogStatus.PASS, "Video Share elements passed");
			}
		} catch (Exception E) {
			UMReporter.log(LogStatus.FAIL, "Error accessing Video Share elements");
		}
	}

	public static void basicLandingPageElements() {
		WebDriver driver = DriverFactory.getCurrentDriver();

		Boolean vidPlayer = false;
		WebDriverWait wait = new WebDriverWait(driver, 15);

			wait.until(ExpectedConditions.visibilityOf(VideoLeafPage.getVideoPlayElement()));
			vidPlayer = true;
			
		if (vidPlayer)
			UMReporter.log(LogStatus.PASS, "Video Player is present in the page");
		else {
			UMReporter.log(LogStatus.FAIL, "Video Player is not present in the page");
		}

		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			
				WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHead()
						,"Video Title");
				
				WrapperMethods.assertIsTrue(VideoLeafPage.getVideoSource().getText().contains("Source: ")
						&&VideoLeafPage.getVideoSource().getText().length() > 8
						, "Source Element is not present / doesn't contains text"
						, "Source Element is present and contains text");
		} else {
			MethodDef.explicitWaitVisibility(VideoLeafPage.getVideoHeadDvc(),"Video header DVC is present"
					,"Video header DVC is not present");
			WrapperMethods.textNotEmpty(VideoLeafPage.getVideoHeadDvc()
					,"Video Title");
			
			WrapperMethods.assertIsTrue(VideoLeafPage.getVideoSrcdvc().getText().contains("Source: ")
					&& VideoLeafPage.getVideoSrcdvc().getText().length() > 8,
			"Source Element is not present / doesn't contains text",
			"Source Element is present and contains text");

		}
		/*
		 * error.assertTrue(leaf.getVideoDate(go).getText()
		 * .startsWith("Added on ") && leaf.getVideoDate(go).getText().length()
		 * > 35, "Date added Element is not present / doesn't contains text",
		 * "Date added Element is present and contains text");
		 */
		if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Desktop")) {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(VideoLeafPage.getVideoEmbed()));
				MethodDef.passLog("Embed Video is clickable");
			} catch (Exception E) {
				MethodDef.warnLog(E.getMessage(), "Error in accessing Embed Element - Check manually");
			}
		}

	}

}
